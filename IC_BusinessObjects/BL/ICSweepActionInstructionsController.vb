﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICSweepActionInstructionsController
        Public Shared Function AddSweepActionsInstructions(ByVal ICSweepActionInstruction As ICSweepActionInstructions, ByVal IsUpdate As Boolean,
                                                           ByVal UsersID As String, ByVal UsersName As String, ByVal ActionType As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim StrAction As String = Nothing

            If IsUpdate = False Then
                objICSweepActionInstruction.SweepActionCreationDate = ICSweepActionInstruction.SweepActionCreationDate
                objICSweepActionInstruction.SweepActionCreatedBy = ICSweepActionInstruction.SweepActionCreatedBy
            Else
                objICSweepActionInstruction.LoadByPrimaryKey(ICSweepActionInstruction.SweepActionInstructionID)
            End If
            objICSweepActionInstruction.FromAccountNo = ICSweepActionInstruction.FromAccountNo
            objICSweepActionInstruction.FromAccountBrCode = ICSweepActionInstruction.FromAccountBrCode
            objICSweepActionInstruction.FromAccountCurrency = ICSweepActionInstruction.FromAccountCurrency
            objICSweepActionInstruction.FromAccountType = ICSweepActionInstruction.FromAccountType

            objICSweepActionInstruction.ToAccountNo = ICSweepActionInstruction.ToAccountNo
            objICSweepActionInstruction.ToAccountBrCode = ICSweepActionInstruction.ToAccountBrCode
            objICSweepActionInstruction.ToAccountCurrency = ICSweepActionInstruction.ToAccountCurrency
            objICSweepActionInstruction.ToAccountType = ICSweepActionInstruction.ToAccountType

            objICSweepActionInstruction.SweepActionID = ICSweepActionInstruction.SweepActionID
            objICSweepActionInstruction.InstructionAmount = ICSweepActionInstruction.InstructionAmount
            objICSweepActionInstruction.IsProcessed = ICSweepActionInstruction.IsProcessed
            objICSweepActionInstruction.IsActive = ICSweepActionInstruction.IsActive
            objICSweepActionInstruction.IsApproved = ICSweepActionInstruction.IsApproved
            objICSweepActionInstruction.LastStatus = ICSweepActionInstruction.LastStatus
            objICSweepActionInstruction.CurrentStatus = ICSweepActionInstruction.CurrentStatus
            objICSweepActionInstruction.SweepActionFromDate = ICSweepActionInstruction.SweepActionFromDate
            objICSweepActionInstruction.SweepActionToDate = ICSweepActionInstruction.SweepActionToDate

            objICSweepActionInstruction.SweepActionFrequency = ICSweepActionInstruction.SweepActionFrequency
            objICSweepActionInstruction.SweepActionTitle = ICSweepActionInstruction.SweepActionTitle
            objICSweepActionInstruction.SweepActionTime = ICSweepActionInstruction.SweepActionTime
            objICSweepActionInstruction.SweepActionDayType = ICSweepActionInstruction.SweepActionDayType
            objICSweepActionInstruction.IsPaid = ICSweepActionInstruction.IsPaid

            objICSweepActionInstruction.SweepInstructionActivatedBy = ICSweepActionInstruction.SweepInstructionActivatedBy
            objICSweepActionInstruction.SweepInstructionApproveddBy = ICSweepActionInstruction.SweepInstructionApproveddBy

            objICSweepActionInstruction.GroupCode = ICSweepActionInstruction.GroupCode
            objICSweepActionInstruction.CompanyCode = ICSweepActionInstruction.CompanyCode
            objICSweepActionInstruction.ValueDate = ICSweepActionInstruction.ValueDate
            objICSweepActionInstruction.Save()


            StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
            StrAction += " from Account No [ " & objICSweepActionInstruction.FromAccountNo & " ] to Account No [ " & objICSweepActionInstruction.ToAccountNo & " ]"
            StrAction += " of amount [ " & objICSweepActionInstruction.InstructionAmount & " ] value date [ " & objICSweepActionInstruction.ValueDate & " ]"

            If IsUpdate = False Then
                StrAction += " added."
            Else
                StrAction += " updated."
            End If
            StrAction += " Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, UsersID, UsersName, ActionType)


            Result = True
            Return Result


        End Function
        Public Shared Function DeleteSweepActionInstruction(ByVal SweepActionInstructionID As String, ByVal UsersID As String, ByVal UsersName As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim StrAction As String = Nothing


            objICSweepActionInstruction.LoadByPrimaryKey(SweepActionInstructionID)
            objICSweepActionInstruction.MarkAsDeleted()
            objICSweepActionInstruction.Save()

            StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
            StrAction += " from Account No [ " & objICSweepActionInstruction.FromAccountNo & " ] to Account No [ " & objICSweepActionInstruction.ToAccountNo & " ]"
            StrAction += " of amount [ " & objICSweepActionInstruction.InstructionAmount & " ] value date [ " & objICSweepActionInstruction.ValueDate & " ]"
            StrAction += "deleted. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, UsersID, UsersName, "DELETE")


            Result = True
            Return Result

        End Function
        Public Shared Function DeleteSweepActionInstructionBySweepActionID(ByVal SweepActionID As String, ByVal UsersID As String, ByVal UsersName As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstructionForDelete As ICSweepActionInstructions
            Dim objICSweepActionInstructionColl As New ICSweepActionInstructionsCollection
            Dim StrAction As String = Nothing

            objICSweepActionInstructionColl.Query.Where(objICSweepActionInstructionColl.Query.SweepActionID = SweepActionID)
            If objICSweepActionInstructionColl.Query.Load Then
                For Each objICSweepActionInstruction As ICSweepActionInstructions In objICSweepActionInstructionColl
                    objICSweepActionInstructionForDelete = New ICSweepActionInstructions
                    objICSweepActionInstructionForDelete.LoadByPrimaryKey(objICSweepActionInstruction.SweepActionInstructionID)
                    objICSweepActionInstructionForDelete.MarkAsDeleted()
                    objICSweepActionInstructionForDelete.Save()

                    StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
                    StrAction += " from Account No [ " & objICSweepActionInstruction.FromAccountNo & " ] to Account No [ " & objICSweepActionInstruction.ToAccountNo & " ]"
                    StrAction += " of amount [ " & objICSweepActionInstruction.InstructionAmount & " ] value date [ " & objICSweepActionInstruction.ValueDate & " ]"
                    StrAction += "deleted. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
                    ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, UsersID, UsersName, "DELETE")


                  
                Next
                Result = True
                Return Result
            End If
           

        End Function
        Public Shared Function ActivateSweepActionInstruction(ByVal SweepActionInstructionID As String, ByVal UsersID As String, ByVal UsersName As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim StrAction As String = Nothing


            objICSweepActionInstruction.LoadByPrimaryKey(SweepActionInstructionID)
            objICSweepActionInstruction.LastStatus = objICSweepActionInstruction.CurrentStatus
            objICSweepActionInstruction.SweepInstructionActivatedBy = UsersID
            objICSweepActionInstruction.SweepActionInstructionActivationDateTime = Date.Now
            objICSweepActionInstruction.IsActive = True
            objICSweepActionInstruction.CurrentStatus = "Activated"
            objICSweepActionInstruction.Save()

            StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
            StrAction += " from Account No [ " & objICSweepActionInstruction.FromAccountNo & " ] to Account No [ " & objICSweepActionInstruction.ToAccountNo & " ]"
            StrAction += " of amount [ " & objICSweepActionInstruction.InstructionAmount & " ] value date [ " & objICSweepActionInstruction.ValueDate & " ]"
            StrAction += " activated. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, UsersID, UsersName, "ACTIVATE")


            Result = True
            Return Result

        End Function
        Public Shared Function ApprovedSweepActionInstruction(ByVal SweepActionInstructionID As String, ByVal UsersID As String, ByVal UsersName As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim StrAction As String = Nothing


            objICSweepActionInstruction.LoadByPrimaryKey(SweepActionInstructionID)
            objICSweepActionInstruction.LastStatus = objICSweepActionInstruction.CurrentStatus
            objICSweepActionInstruction.SweepInstructionApproveddBy = UsersID
            objICSweepActionInstruction.IsApproved = True
            objICSweepActionInstruction.SweepActionInstructionApprovalDateTime = Date.Now
            objICSweepActionInstruction.CurrentStatus = "APPROVED"
            objICSweepActionInstruction.Save()

            StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
            StrAction += " from Account No [ " & objICSweepActionInstruction.FromAccountNo & " ] to Account No [ " & objICSweepActionInstruction.ToAccountNo & " ]"
            StrAction += " of amount [ " & objICSweepActionInstruction.InstructionAmount & " ] value date [ " & objICSweepActionInstruction.ValueDate & " ]"
            StrAction += " approved. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, UsersID, UsersName, "APPROVED")


            Result = True
            Return Result

        End Function
        Public Shared Sub GetAllSweepActionInstructionBySweepActionID(ByVal PageNumber As Integer, ByVal PageSize As Integer,
                                                                        ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal SweepActionID As String)


            Dim qryObjICSweepActions As New ICSweepActionInstructionsQuery("qryObjICSweepActions")
            Dim ArrayAssignedAccounts As New ArrayList
            Dim dt As New DataTable
            qryObjICSweepActions.Select(qryObjICSweepActions.SweepActionInstructionID, qryObjICSweepActions.FromAccountNo, qryObjICSweepActions.ToAccountNo)
            qryObjICSweepActions.Select(qryObjICSweepActions.SweepActionFrequency, qryObjICSweepActions.IsActive, qryObjICSweepActions.IsApproved)
            qryObjICSweepActions.Select(qryObjICSweepActions.IsPaid, qryObjICSweepActions.SweepActionID, qryObjICSweepActions.ValueDate, qryObjICSweepActions.InstructionAmount)
            qryObjICSweepActions.Where(qryObjICSweepActions.SweepActionID = SweepActionID)
            qryObjICSweepActions.OrderBy(qryObjICSweepActions.SweepActionInstructionID.Descending)
            'If Not GroupCode.ToString = "" Then
            '    qryObjICSweepActions.Where(qryObjICSweepActions.GroupCode = GroupCode.ToString)
            'End If
            'If Not CompanyCode.ToString = "" Then
            '    qryObjICSweepActions.Where(qryObjICSweepActions.CompanyCode = CompanyCode.ToString)
            'End If
            'If dtAccountNo.Rows.Count > 0 Then
            '    For Each dr As DataRow In dtAccountNo.Rows
            '        ArrayAssignedAccounts.Add(dr("AccountNumber"))
            '    Next

            '    qryObjICSweepActions.Where((qryObjICSweepActions.ToAccountNo + "-" + qryObjICSweepActions.ToAccountBrCode + "-" + qryObjICSweepActions.ToAccountCurrency).In(ArrayAssignedAccounts))

            'End If
            'If dtFromAccountNo.Rows.Count > 0 Then
            '    ArrayAssignedAccounts = New ArrayList
            '    For Each dr As DataRow In dtAccountNo.Rows
            '        ArrayAssignedAccounts.Add(dr("AccountNumber"))
            '    Next

            '    qryObjICSweepActions.Where((qryObjICSweepActions.FromAccountNo + "-" + qryObjICSweepActions.FromAccountBrCode + "-" + qryObjICSweepActions.FromAccountCurrency).In(ArrayAssignedAccounts))
            'End If

            If Not PageNumber = 0 Then
                qryObjICSweepActions.es.PageNumber = PageNumber
                qryObjICSweepActions.es.PageSize = PageSize
                rg.DataSource = qryObjICSweepActions.LoadDataTable
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICSweepActions.es.PageNumber = 1
                qryObjICSweepActions.es.PageSize = PageSize
                rg.DataSource = qryObjICSweepActions.LoadDataTable
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        Public Shared Function GetSweepInstructionsCountAgainstSweepAction(ByVal SweepActionID As String) As Integer
            Dim qryObjICSweepActionInstructions As New ICSweepActionInstructionsQuery("qryObjICSweepActionInstructions")
            qryObjICSweepActionInstructions.Select(qryObjICSweepActionInstructions.SweepActionInstructionID.Count.As("Count"))
            qryObjICSweepActionInstructions.Where(qryObjICSweepActionInstructions.SweepActionID = SweepActionID)



            Return CInt(qryObjICSweepActionInstructions.LoadDataTable(0)(0))
        End Function
        Public Shared Sub AutoProcessAllSweepActionInstruction()
            Dim objICSweepActionInstructionColl As New ICSweepActionInstructionsCollection
            Dim objICSweepActionInstruction As ICSweepActionInstructions
            Dim objTxnStatus As CBUtilities.TransactionStatus
            Dim FromAccountType, ToAccountType As CBUtilities.AccountType
            Dim TransactionMessage As String = Nothing
            Dim StrAction As String = Nothing
            Dim AccountBalance As Double = 0
            Dim InstructionAmount As Double = 0
            Try
                If CBUtilities.GetEODStatus().ToString = "1" Then
                    Exit Sub

                End If
            Catch ex As Exception
                Exit Sub
            End Try



            objICSweepActionInstructionColl.Query.Where(objICSweepActionInstructionColl.Query.IsActive = True And objICSweepActionInstructionColl.Query.IsApproved = True)
            objICSweepActionInstructionColl.Query.Where(objICSweepActionInstructionColl.Query.ValueDate.Date <= Date.Now.Date And objICSweepActionInstructionColl.Query.IsPaid = False)
            objICSweepActionInstructionColl.Query.OrderBy(objICSweepActionInstructionColl.Query.SweepActionInstructionID.Ascending)
            If objICSweepActionInstructionColl.Query.Load Then
                Dim CurerentTime As DateTime = Date.Now.Hour & ":" & Date.Now.Minute
                For Each objICSweepInstruction As ICSweepActionInstructions In objICSweepActionInstructionColl
                    Dim SweepActionTime As DateTime = CDate(objICSweepInstruction.SweepActionTime).Hour & ":" & CDate(objICSweepInstruction.SweepActionTime).Minute
                    If CurerentTime >= SweepActionTime Then
                        objICSweepActionInstruction = New ICSweepActionInstructions
                        objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
                        If objICSweepActionInstruction.InstructionAmount Is Nothing Then

                            Try
                                AccountBalance = CBUtilities.BalanceInquiry(objICSweepActionInstruction.FromAccountNo, CBUtilities.AccountType.RB, "Sweep Action Instruction Balance", objICSweepActionInstruction.SweepActionInstructionID, objICSweepActionInstruction.FromAccountBrCode)
                                If AccountBalance = -1 Then
                                    StrAction = Nothing
                                    StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] skipped due to failure in client account balance inquiry."
                                    ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
                                    Continue For
                                Else
                                    InstructionAmount = AccountBalance
                                End If
                            Catch ex As Exception
                                StrAction = Nothing
                                StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] skipped due " & ex.Message.ToString
                                ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
                                Continue For
                            End Try

                        Else

                            InstructionAmount = objICSweepActionInstruction.InstructionAmount
                        End If
                        UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, "PENDING CB", "Auto Process")
                        Try
                            If objICSweepInstruction.FromAccountType = "RB" Then
                                FromAccountType = CBUtilities.AccountType.RB
                            ElseIf objICSweepActionInstruction.FromAccountType = "GL" Then
                                FromAccountType = CBUtilities.AccountType.RB
                            End If
                            If objICSweepInstruction.ToAccountType = "RB" Then
                                ToAccountType = CBUtilities.AccountType.RB
                            ElseIf objICSweepActionInstruction.ToAccountType = "GL" Then
                                ToAccountType = CBUtilities.AccountType.RB
                            End If
                            objTxnStatus = New CBUtilities.TransactionStatus
                            objTxnStatus = CBUtilities.OpenEndedFundsTransfer(objICSweepInstruction.FromAccountNo, objICSweepInstruction.FromAccountBrCode, objICSweepInstruction.FromAccountCurrency, Nothing, Nothing, Nothing, FromAccountType, objICSweepActionInstruction.ToAccountNo, objICSweepActionInstruction.ToAccountBrCode, objICSweepActionInstruction.ToAccountCurrency, Nothing, Nothing, Nothing, ToAccountType, InstructionAmount, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID)
                            If objTxnStatus = CBUtilities.TransactionStatus.Successfull Then
                                objICSweepActionInstruction = New ICSweepActionInstructions
                                objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
                                objICSweepActionInstruction.IsPaid = True
                                objICSweepActionInstruction.IsProcessed = True
                                objICSweepActionInstruction.SweepActionInstructionPaidDateTime = Date.Now
                                AddSweepActionsInstructions(objICSweepActionInstruction, True, Nothing, Nothing, "UPDATE")
                                UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, "PAID", "Auto Process")
                                StrAction = Nothing
                                StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] masrked as paid."
                                ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
                            Else
                                TransactionMessage = CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(objTxnStatus)
                                objICSweepActionInstruction = New ICSweepActionInstructions
                                objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
                                UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, objICSweepActionInstruction.LastStatus, "Auto Process")
                                StrAction = Nothing
                                StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] is skipped due to " & TransactionMessage
                                ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
                            End If
                        Catch ex As Exception
                            objICSweepActionInstruction = New ICSweepActionInstructions
                            objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
                            StrAction = Nothing
                            StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] is skipped due to " & ex.Message.ToString
                            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
                            UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, objICSweepActionInstruction.LastStatus, "Auto Process")
                        End Try
                    End If
                Next
            End If
        End Sub

        'Public Shared Sub AutoProcessAllSweepActionInstruction()
        '    Dim objICSweepActionInstructionColl As New ICSweepActionInstructionsCollection
        '    Dim objICSweepActionInstruction As ICSweepActionInstructions
        '    Dim objTxnStatus As CBUtilities.TransactionStatus
        '    Dim FromAccountType, ToAccountType As CBUtilities.AccountType
        '    Dim TransactionMessage As String = Nothing
        '    Dim StrAction As String = Nothing
        '    objICSweepActionInstructionColl.Query.Where(objICSweepActionInstructionColl.Query.IsActive = True And objICSweepActionInstructionColl.Query.IsApproved = True)
        '    objICSweepActionInstructionColl.Query.Where(objICSweepActionInstructionColl.Query.ValueDate.Date <= Now.Date And objICSweepActionInstructionColl.Query.IsPaid = False)
        '    objICSweepActionInstructionColl.Query.OrderBy(objICSweepActionInstructionColl.Query.SweepActionInstructionID.Ascending)
        '    If objICSweepActionInstructionColl.Query.Load Then
        '        Dim CurerentTime As DateTime = Date.Now.Hour & ":" & Date.Now.Minute
        '        For Each objICSweepInstruction As ICSweepActionInstructions In objICSweepActionInstructionColl
        '            Dim SweepActionTime As DateTime = CDate(objICSweepInstruction.SweepActionTime).Hour & ":" & CDate(objICSweepInstruction.SweepActionTime).Minute
        '            If CurerentTime >= SweepActionTime Then
        '                objICSweepActionInstruction = New ICSweepActionInstructions
        '                objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
        '                UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, "PENDING CB", "Auto Process")
        '                Try
        '                    If objICSweepInstruction.FromAccountType = "RB" Then
        '                        FromAccountType = CBUtilities.AccountType.RB
        '                    ElseIf objICSweepActionInstruction.FromAccountType = "GL" Then
        '                        FromAccountType = CBUtilities.AccountType.RB
        '                    End If
        '                    If objICSweepInstruction.ToAccountType = "RB" Then
        '                        ToAccountType = CBUtilities.AccountType.RB
        '                    ElseIf objICSweepActionInstruction.ToAccountType = "GL" Then
        '                        ToAccountType = CBUtilities.AccountType.RB
        '                    End If
        '                    objTxnStatus = New CBUtilities.TransactionStatus
        '                    objTxnStatus = CBUtilities.OpenEndedFundsTransfer(objICSweepInstruction.FromAccountNo, objICSweepInstruction.FromAccountBrCode, objICSweepInstruction.FromAccountCurrency, Nothing, Nothing, Nothing, FromAccountType, objICSweepActionInstruction.ToAccountNo, objICSweepActionInstruction.ToAccountBrCode, objICSweepActionInstruction.ToAccountCurrency, Nothing, Nothing, Nothing, ToAccountType, objICSweepActionInstruction.InstructionAmount, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID)
        '                    If objTxnStatus = CBUtilities.TransactionStatus.Successfull Then
        '                        objICSweepActionInstruction = New ICSweepActionInstructions
        '                        objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
        '                        objICSweepActionInstruction.IsPaid = True
        '                        objICSweepActionInstruction.IsProcessed = True
        '                        objICSweepActionInstruction.SweepActionInstructionPaidDateTime = Date.Now
        '                        AddSweepActionsInstructions(objICSweepActionInstruction, True, Nothing, Nothing, "UPDATE")
        '                        UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, "PAID", "Auto Process")
        '                        StrAction = Nothing
        '                        StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] masrked as paid."
        '                        ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
        '                    Else
        '                        TransactionMessage = CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(objTxnStatus)
        '                        objICSweepActionInstruction = New ICSweepActionInstructions
        '                        objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
        '                        UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, objICSweepActionInstruction.LastStatus, "Auto Process")
        '                        StrAction = Nothing
        '                        StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] is skipped due to " & TransactionMessage
        '                        ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
        '                    End If
        '                Catch ex As Exception
        '                    objICSweepActionInstruction = New ICSweepActionInstructions
        '                    objICSweepActionInstruction.LoadByPrimaryKey(objICSweepInstruction.SweepActionInstructionID)
        '                    StrAction = Nothing
        '                    StrAction = "Sweep Action Instruction With ID: [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] is skipped due to " & ex.Message.ToString
        '                    ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, "UPDATE")
        '                    UpdateSweepInstructionStatus(objICSweepInstruction.SweepActionInstructionID, objICSweepInstruction.CurrentStatus, objICSweepActionInstruction.LastStatus, "Auto Process")
        '                End Try
        '            End If
        '        Next
        '    End If
        'End Sub

        Public Shared Function UpdateSweepInstructionStatus(ByVal SweepActionInstructionID As String, ByVal FromStatus As String, ByVal ToStatus As String, ByVal ActionType As String) As Boolean

            Dim Result As Boolean = False
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim StrAction As String = Nothing


            objICSweepActionInstruction.LoadByPrimaryKey(SweepActionInstructionID)
            objICSweepActionInstruction.LastStatus = FromStatus

            objICSweepActionInstruction.CurrentStatus = ToStatus
            objICSweepActionInstruction.Save()

            StrAction = "Sweep Action Instruction With ID [ " & objICSweepActionInstruction.SweepActionInstructionID & " ] of Sweep Action with ID [ " & objICSweepActionInstruction.SweepActionID & " ]"
            StrAction += " status updated from [ " & FromStatus & " ] to status [ " & ToStatus & " ]"
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action Instruction", objICSweepActionInstruction.SweepActionInstructionID.ToString, Nothing, Nothing, ActionType)


            Result = True
            Return Result

        End Function
    End Class
End Namespace
