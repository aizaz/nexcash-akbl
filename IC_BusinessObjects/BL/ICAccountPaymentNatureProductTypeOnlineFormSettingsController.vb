﻿
Namespace IC
    Public Class ICAccountPaymentNatureProductTypeOnlineFormSettingsController
        Public Shared Function GetAccountsPaymentNatureProductTypeOnlineFormSettingsByAccountsPaymentNatureProductTypeCode(ByVal AccountsPaymentNatureProductTypeCode As String) As ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection
            Dim objAPNPTOnlineFormSettingsColl As New IC.ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection

            objAPNPTOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600
            objAPNPTOnlineFormSettingsColl.Query.Where(objAPNPTOnlineFormSettingsColl.Query.AccountPaymentNatureProductTypeCode = AccountsPaymentNatureProductTypeCode)
            objAPNPTOnlineFormSettingsColl.Query.OrderBy(objAPNPTOnlineFormSettingsColl.Query.FieldName.Ascending)
            objAPNPTOnlineFormSettingsColl.Query.Load()
            Return objAPNPTOnlineFormSettingsColl
        End Function
        Public Shared Function AddAPNPTOnlineFormSetting(ByVal cOnlineFormSetting As ICAccountPaymentNatureProductTypeOnlineFormSettings) As String
            Dim objICAPNPTOnlineFormSetting As New ICAccountPaymentNatureProductTypeOnlineFormSettings
            objICAPNPTOnlineFormSetting.es.Connection.CommandTimeout = 3600

            objICAPNPTOnlineFormSetting.AccountPaymentNatureProductTypeCode = cOnlineFormSetting.AccountPaymentNatureProductTypeCode
            objICAPNPTOnlineFormSetting.FieldID = cOnlineFormSetting.FieldID
            objICAPNPTOnlineFormSetting.FieldName = cOnlineFormSetting.FieldName
            objICAPNPTOnlineFormSetting.FixLength = cOnlineFormSetting.FixLength
            objICAPNPTOnlineFormSetting.IsRequired = cOnlineFormSetting.IsRequired
            objICAPNPTOnlineFormSetting.MustRequired = cOnlineFormSetting.MustRequired
            objICAPNPTOnlineFormSetting.IsVisible = cOnlineFormSetting.IsVisible

            objICAPNPTOnlineFormSetting.Save()
            Return objICAPNPTOnlineFormSetting.AccountPaymentNatureProductTypeOnlineFormSettingID
        End Function
        Public Shared Sub DeleteAPNPTOnlineFormSetting(ByVal AccountPaymentNatureProductTypeOnlineFormSettingID As String)
            Dim objICAPNPTTemplate As New ICAccountPaymentNatureProductTypeOnlineFormSettings
            objICAPNPTTemplate.es.Connection.CommandTimeout = 3600
            If objICAPNPTTemplate.LoadByPrimaryKey(AccountPaymentNatureProductTypeOnlineFormSettingID) Then
                objICAPNPTTemplate.MarkAsDeleted()
            End If
            objICAPNPTTemplate.Save()
        End Sub
    End Class
End Namespace