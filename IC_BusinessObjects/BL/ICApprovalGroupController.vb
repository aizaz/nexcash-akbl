﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports FtpLib
Imports System
Imports System.IO
Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals


Namespace IC

    Public Class ICApprovalGroupController
        Dim CompanyName As String = ""


        Public Shared Sub GetApprovalGroupgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal GroupCode As String, ByVal CompanyCode As String)

            Dim qryApprovalGroup As New ICApprovalGroupManagementQuery("qryApprovalGroup")
            Dim qryCompany As New ICCompanyQuery("qryCompany")

            If Not pagenumber = 0 Then

                qryApprovalGroup.Select(qryApprovalGroup.ApprovalGroupID, qryApprovalGroup.GroupName, qryApprovalGroup.CompanyCode, qryApprovalGroup.IsActive)
                qryApprovalGroup.Select(qryCompany.CompanyName)

                qryApprovalGroup.LeftJoin(qryCompany).On(qryApprovalGroup.CompanyCode = qryCompany.CompanyCode)
                qryApprovalGroup.Where(qryCompany.IsActive = True And qryCompany.IsApprove = True And qryCompany.IsPaymentsAllowed = True)
                If GroupCode <> "0" Then
                    qryApprovalGroup.Where(qryCompany.GroupCode = GroupCode)
                End If
                If CompanyCode <> "0" Then
                    qryApprovalGroup.Where(qryCompany.CompanyCode = CompanyCode)
                End If

                rg.DataSource = qryApprovalGroup.LoadDataTable()

                If DoDataBind = True Then

                    rg.DataBind()
                End If

            Else

                qryApprovalGroup.es.PageNumber = 1
                qryApprovalGroup.es.PageSize = pagesize

                rg.DataSource = qryApprovalGroup.LoadDataTable()


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub


        Public Shared Sub DeleteApprovalGroup(ByVal ApprovalGroupID As String, ByVal UserID As String, ByVal UserName As String)
            Dim ICApprovalGroup As New ICApprovalGroupManagement
            Dim prevICApprovalGroup As New ICApprovalGroupManagement
            Dim PrevAt As String = ""
            ICApprovalGroup.es.Connection.CommandTimeout = 3600
            prevICApprovalGroup.es.Connection.CommandTimeout = 3600
            Dim ActiveMsg As String = ""

            ICApprovalGroup.LoadByPrimaryKey(ApprovalGroupID)

            PrevAt = "Approval Group [ID:  " & ICApprovalGroup.ApprovalGroupID.ToString() & " ; Name:  " & ICApprovalGroup.GroupName.ToString() & " ; Client Name:  " & ICApprovalGroup.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ; IsActive:  " & ICApprovalGroup.IsActive.ToString() & "] "

            ICApprovalGroup.MarkAsDeleted()
            ICApprovalGroup.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Group", prevICApprovalGroup.ApprovalGroupID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub



        Public Shared Function GetAllActiveGroups() As ICGroupCollection
            Dim ICGroupColl As New ICGroupCollection
            ICGroupColl.es.Connection.CommandTimeout = 3600
            ICGroupColl.Query.Where(ICGroupColl.Query.IsActive = True)
            ICGroupColl.Query.OrderBy(ICGroupColl.Query.GroupName.Ascending)
            ICGroupColl.Query.Load()
            Return ICGroupColl
        End Function


        Public Shared Function GetAllActiveApproveIspaymentAllowedCompaniesByApprovlGroupCode(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection

            ICComapnyColl.es.Connection.CommandTimeout = 3600
            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode)
            ICComapnyColl.Query.Where(ICComapnyColl.Query.IsActive = True And ICComapnyColl.Query.IsApprove = True And ICComapnyColl.Query.IsPaymentsAllowed = True)
            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function

        Public Shared Function GetAllActiveApprovalGroupsByCompanyCode(ByVal CompanyCode As String) As ICApprovalGroupManagementCollection

            Dim CollObjICAppGrp As New ICApprovalGroupManagementCollection



            CollObjICAppGrp.Query.Where(CollObjICAppGrp.Query.CompanyCode = CompanyCode And CollObjICAppGrp.Query.IsActive = True)
            CollObjICAppGrp.Query.OrderBy(CollObjICAppGrp.Query.GroupName.Ascending)
            CollObjICAppGrp.Query.Load()
            Return CollObjICAppGrp




        End Function
        Public Shared Sub AddApprovalGroup(ByVal cApprovalGroup As ICApprovalGroupManagement, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICApprovalGroup As New ICApprovalGroupManagement
            Dim prevICApprovalGroup As New ICApprovalGroupManagement
            Dim IsActiveText As String
            Dim CurrentAt As String
            Dim PrevAt As String

            ICApprovalGroup.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICApprovalGroup.CreatedBy = cApprovalGroup.CreatedBy
                ICApprovalGroup.CreateDate = cApprovalGroup.CreateDate

            Else
                ICApprovalGroup.LoadByPrimaryKey(cApprovalGroup.ApprovalGroupID)
                prevICApprovalGroup.LoadByPrimaryKey(cApprovalGroup.ApprovalGroupID)
                ICApprovalGroup.CreatedBy = ICApprovalGroup.CreatedBy
                ICApprovalGroup.CreateDate = cApprovalGroup.CreateDate
            End If
            ICApprovalGroup.GroupName = cApprovalGroup.GroupName
            ICApprovalGroup.IsActive = cApprovalGroup.IsActive
            ICApprovalGroup.CompanyCode = cApprovalGroup.CompanyCode


            If ICApprovalGroup.IsActive = True Then
                ICApprovalGroup.IsActive = True
                IsActiveText = True
            Else
                IsActiveText = False
                ICApprovalGroup.IsActive = False
            End If

            ICApprovalGroup.Save()

            If (isUpdate = False) Then
                CurrentAt = "Approval Group : [Code:  " & ICApprovalGroup.ApprovalGroupID.ToString() & " ; Name:  " & ICApprovalGroup.GroupName.ToString() & " ; IsActive:  " & ICApprovalGroup.IsActive.ToString() & "]" & " ; Name:  " & ICApprovalGroup.UpToICCompanyByCompanyCode.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Approval Group", ICApprovalGroup.ApprovalGroupID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "Approval Group : [Code:  " & ICApprovalGroup.ApprovalGroupID.ToString() & " ; Name:  " & ICApprovalGroup.GroupName.ToString() & " ; IsActive:  " & ICApprovalGroup.IsActive.ToString() & "]" & " ; Name:  " & ICApprovalGroup.UpToICCompanyByCompanyCode.ToString() & "]"
                PrevAt = "<br />Group : Previous Values [Code:  " & prevICApprovalGroup.ApprovalGroupID.ToString() & " ; Name:  " & prevICApprovalGroup.GroupName.ToString() & " ; IsActive:  " & prevICApprovalGroup.IsActive.ToString() & "]" & " ; Name:  " & prevICApprovalGroup.UpToICCompanyByCompanyCode.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", " Approval Group", ICApprovalGroup.ApprovalGroupID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub


    End Class

End Namespace
