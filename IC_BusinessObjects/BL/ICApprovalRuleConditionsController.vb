﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICApprovalRuleConditionsController

        Public Shared Function AddApprovalRuleCondtions(ByVal objICAPPRuleCond As ICApprovalRuleConditions, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String) As Integer

            Dim objICApprovalCondSave As New ICApprovalRuleConditions
            Dim objICApprovalRuleCondPrev As New ICApprovalRuleConditions
            Dim ActionString As String = ""


            If IsUpdate = False Then

                objICApprovalCondSave.CreatedBy = objICAPPRuleCond.CreatedBy
                objICApprovalCondSave.CreatedDate = objICAPPRuleCond.CreatedDate
                objICApprovalCondSave.Creator = objICAPPRuleCond.Creator
                objICApprovalCondSave.CreationDate = objICAPPRuleCond.CreationDate
            Else
                objICApprovalCondSave.LoadByPrimaryKey(objICAPPRuleCond.ApprovalRuleConditionID)
                objICApprovalRuleCondPrev.LoadByPrimaryKey(objICAPPRuleCond.ApprovalRuleConditionID)
                objICApprovalCondSave.CreatedBy = objICAPPRuleCond.CreatedBy
                objICApprovalCondSave.CreatedDate = objICAPPRuleCond.CreatedDate
            End If
            objICApprovalCondSave.ApprovalRuleID = objICAPPRuleCond.ApprovalRuleID
            objICApprovalCondSave.SelectionCriteria = objICAPPRuleCond.SelectionCriteria
            objICApprovalCondSave.ConditionType = objICAPPRuleCond.ConditionType
            objICApprovalCondSave.ApprovalGroupID = objICAPPRuleCond.ApprovalGroupID
            objICApprovalCondSave.ApprovalRuleID = objICAPPRuleCond.ApprovalRuleID
            objICApprovalCondSave.Save()

            If IsUpdate = False Then
                ActionString = "Approval Rule Condition [ " & objICApprovalCondSave.ApprovalRuleID & " ], type [ " & objICApprovalCondSave.ConditionType & " ]"
                ActionString += " selection criteria [ " & objICApprovalCondSave.SelectionCriteria & " ] for approval rule [ " & objICApprovalCondSave.ApprovalRuleID & " ] [ " & objICApprovalCondSave.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName & " ]"
                ActionString += " for approval group [ " & objICApprovalCondSave.ApprovalGroupID & " ][ " & objICApprovalCondSave.UpToICApprovalGroupManagementByApprovalGroupID.GroupName & " ] "
                ActionString += " added."
                ICUtilities.AddAuditTrail(ActionString, "Approval Rule Condition", objICApprovalCondSave.ApprovalRuleConditionID, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpdate = True Then
                ActionString = "Approval Rule Condition [ " & objICApprovalCondSave.ApprovalRuleID & " ], updated from :"
                ActionString += "type from [ " & objICApprovalRuleCondPrev.ConditionType & " ] to  [ " & objICApprovalCondSave.ConditionType & " ]"
                ActionString += " selection criteria from [ " & objICApprovalCondSave.SelectionCriteria & " ] to [ " & objICApprovalCondSave.SelectionCriteria & " ]"
                ActionString += " approval rule from [ " & objICApprovalRuleCondPrev.ApprovalRuleID & " ] [ " & objICApprovalRuleCondPrev.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName & " ]"
                ActionString += " to [ " & objICApprovalCondSave.ApprovalRuleID & " ] [ " & objICApprovalCondSave.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName & " ]"
                ActionString += "approval group from [ " & objICApprovalRuleCondPrev.ApprovalGroupID & " ][ " & objICApprovalRuleCondPrev.UpToICApprovalGroupManagementByApprovalGroupID.GroupName & " ] "
                ActionString += " to[ " & objICApprovalCondSave.ApprovalGroupID & " ][ " & objICApprovalCondSave.UpToICApprovalGroupManagementByApprovalGroupID.GroupName & " ] "
                ICUtilities.AddAuditTrail(ActionString.ToString, "Approval Rule Condition", objICApprovalCondSave.ApprovalRuleConditionID, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


            Return objICApprovalCondSave.ApprovalRuleConditionID


        End Function

        Public Shared Sub GetAllApprovalRuleConditionsByApprovalRuleIDAndType(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal ApprovalRuleID As String, ByVal ConditionType As String)


            Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRuleCond")
            Dim qryObjICAppRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppRuleCondUsers")
            Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")

            qryObjICAppRuleCond.Select(qryObjICAppRuleCond.ConditionType, qryObjICAppRuleCond.SelectionCriteria, qryObjICAppRuleCond.ApprovalRuleConditionID)

            qryObjICAppRuleCond.Select(qryObjICAppRuleCond.ApprovalRuleID, qryObjICApprovalGroup.GroupName)
            qryObjICAppRuleCond.Select(qryObjICAppRuleCondUsers.[Select](qryObjICAppRuleCondUsers.UserCount.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'")).Where(qryObjICAppRuleCondUsers.ApprovaRuleCondID = qryObjICAppRuleCond.ApprovalRuleConditionID).As("UserCount"))



            qryObjICAppRuleCond.LeftJoin(qryObjICApprovalGroup).On(qryObjICAppRuleCond.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
            qryObjICAppRuleCond.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID And qryObjICAppRuleCond.ConditionType = ConditionType)
            qryObjICAppRuleCondUsers.es.Top = 1
            dt = qryObjICAppRuleCond.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICAppRuleCond.es.PageNumber = PageNumber
                qryObjICAppRuleCond.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAppRuleCond.es.PageNumber = 1
                qryObjICAppRuleCond.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub

        Public Shared Function GetApprovalRuleConditionsByApprovalRuleID(ByVal ApprovalRuleID As String) As ICApprovalRuleConditionsCollection
            Dim collObjICAPPRuleCond As New ICApprovalRuleConditionsCollection



            collObjICAPPRuleCond.Query.Where(collObjICAPPRuleCond.Query.ApprovalRuleID = ApprovalRuleID)
            collObjICAPPRuleCond.Query.OrderBy(collObjICAPPRuleCond.Query.ApprovalRuleID.Ascending)
            collObjICAPPRuleCond.Query.Load()



            Return collObjICAPPRuleCond
        End Function
        Public Shared Sub DeleteApprovalRuleCondition(ByVal ApprovalRuleConditionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICApprovalRuleCond As New IC.ICApprovalRuleConditions
            Dim ActionString As String = ""

            objICApprovalRuleCond.LoadByPrimaryKey(ApprovalRuleConditionID)
            objICApprovalRuleCond.MarkAsDeleted()
            ActionString = "Approval Rule Condition : [ " & objICApprovalRuleCond.ApprovalRuleConditionID & " ], for approval ruule [ "
            ActionString += "" & objICApprovalRuleCond.ApprovalRuleID & " ] [ " & objICApprovalRuleCond.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName & " ]"
            ActionString += " [ " & objICApprovalRuleCond.UpToICApprovalRuleByApprovalRuleID.CompanyCode & " ][ " & objICApprovalRuleCond.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName & " ]"""
            ActionString += " [ " & objICApprovalRuleCond.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ " & objICApprovalRuleCond.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ]"
            ActionString += " deleted."
            ICUtilities.AddAuditTrail(ActionString, "Approval Rule Condition", objICApprovalRuleCond.ApprovalRuleID, UsersID.ToString, UsersName.ToString, "DELETE")
            objICApprovalRuleCond.Save()
        End Sub
        Public Shared Function GetAllApprovalRuleConditionsByApprovalGroupIDAndConditionType(ByVal ApprovalGroupID As String, ByVal ConditionType As ArrayList) As DataTable


            Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRuleCond")
            Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")

            qryObjICAppRuleCond.Select(qryObjICAppRuleCond.ConditionType, qryObjICAppRuleCond.SelectionCriteria, qryObjICAppRuleCond.ApprovalRuleConditionID, qryObjICAppRuleCond.ApprovalGroupID)



            qryObjICAppRuleCond.InnerJoin(qryObjICApprovalGroup).On(qryObjICAppRuleCond.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
            qryObjICAppRuleCond.Where(qryObjICAppRuleCond.ApprovalGroupID = ApprovalGroupID And qryObjICAppRuleCond.SelectionCriteria.In(ConditionType))

            dt = qryObjICAppRuleCond.LoadDataTable

            Return dt
        End Function
    End Class
End Namespace
