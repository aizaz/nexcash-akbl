﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionAccountsController
        Public Shared Sub AddCollectionAccounts(ByVal ICCollectionAccount As ICCollectionAccounts, ByVal objICCollectionAccountsFrom As ICCollectionAccounts, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String)
            Dim objICCollectionAccounts As New ICCollectionAccounts
            objICCollectionAccounts.es.Connection.CommandTimeout = 3600

            If IsUpDate = False Then

                objICCollectionAccounts.CreateBy = ICCollectionAccount.CreateBy
                objICCollectionAccounts.CreateDate = ICCollectionAccount.CreateDate
            Else
                objICCollectionAccounts.LoadByPrimaryKey(objICCollectionAccountsFrom.AccountNumber, objICCollectionAccountsFrom.BranchCode, objICCollectionAccountsFrom.Currency)
            End If
            objICCollectionAccounts.AccountNumber = ICCollectionAccount.AccountNumber
            objICCollectionAccounts.BranchCode = ICCollectionAccount.BranchCode
            objICCollectionAccounts.Currency = ICCollectionAccount.Currency
            objICCollectionAccounts.AccountTitle = ICCollectionAccount.AccountTitle
            objICCollectionAccounts.CompanyCode = ICCollectionAccount.CompanyCode
            objICCollectionAccounts.IsActive = ICCollectionAccount.IsActive
            objICCollectionAccounts.Save()

            If IsUpDate = False Then
                ICUtilities.AddAuditTrail("Collection Account [ " & objICCollectionAccounts.AccountNumber.ToString() & " ], Branch Code [ " & objICCollectionAccounts.BranchCode & " ], Currency [ " & objICCollectionAccounts.Currency & " ] Added", "Collection Account", objICCollectionAccounts.AccountNumber.ToString + "-" + objICCollectionAccounts.BranchCode + "-" + objICCollectionAccounts.Currency, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpDate = True Then
                ICUtilities.AddAuditTrail(ActionString.ToString, "Collection Account", objICCollectionAccounts.AccountNumber.ToString(), UsersID.ToString, UsersName.ToString, "UPDATE")
            End If

        End Sub

        Public Shared Sub DeleteCollectionAccount(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICCollectionAccount As New ICCollectionAccounts
            objICCollectionAccount.LoadByPrimaryKey(AccountNumber, BranchCode, Currency)
            objICCollectionAccount.MarkAsDeleted()
            ICUtilities.AddAuditTrail("Collection Account : [ " & AccountNumber.ToString & " ], Branch Code [ " & BranchCode & " ], Currency [ " & Currency & " ] deleted.", "Collection Account", AccountNumber.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            objICCollectionAccount.Save()
        End Sub

        Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String)


            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICCollectionAccounts.Select(qryObjICCollectionAccounts.AccountNumber, qryObjICCollectionAccounts.AccountTitle, qryObjICCollectionAccounts.BranchCode, qryObjICCollectionAccounts.Currency, qryObjICCollectionAccounts.IsActive)
            qryObjICCollectionAccounts.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICCollectionAccounts.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)

            qryObjICCollectionAccounts.GroupBy(qryObjICCollectionAccounts.AccountNumber, qryObjICCollectionAccounts.BranchCode, qryObjICCollectionAccounts.Currency, qryObjICCollectionAccounts.AccountTitle, qryObjICCollectionAccounts.IsActive)

            If Not GroupCode.ToString = "" Then
                qryObjICCollectionAccounts.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode.ToString = "" Then
                qryObjICCollectionAccounts.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If
            dt = qryObjICCollectionAccounts.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICCollectionAccounts.es.PageNumber = PageNumber
                qryObjICCollectionAccounts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICCollectionAccounts.es.PageNumber = 1
                qryObjICCollectionAccounts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub

        Public Shared Function GetAllCollectionAccountsByCompanyCode(ByVal CompanyCode As String) As ICCollectionAccountsCollection
            Dim collICCollectionAccounts As New ICCollectionAccountsCollection
            collICCollectionAccounts.es.Connection.CommandTimeout = 3600
            collICCollectionAccounts.Query.Where(collICCollectionAccounts.Query.CompanyCode = CompanyCode And collICCollectionAccounts.Query.IsActive = True)
            collICCollectionAccounts.Query.OrderBy(collICCollectionAccounts.Query.AccountNumber.Ascending)
            collICCollectionAccounts.Query.Load()
            Return collICCollectionAccounts
        End Function


    End Class
End Namespace