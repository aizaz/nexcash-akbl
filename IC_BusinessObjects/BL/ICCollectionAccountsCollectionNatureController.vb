﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionAccountsCollectionNatureController
        Public Shared Function AddCollectionAccountCollectionNature(ByVal ICCollAccountCollectionNature As ICCollectionAccountsCollectionNature, ByVal ActionText As String, ByVal UserID As String, ByVal UserName As String) As String
            Dim objCollectionAccountCollectionNature As New ICCollectionAccountsCollectionNature
            objCollectionAccountCollectionNature.es.Connection.CommandTimeout = 3600
            objCollectionAccountCollectionNature.CreateBy = ICCollAccountCollectionNature.CreateBy
            objCollectionAccountCollectionNature.CreateDate = ICCollAccountCollectionNature.CreateDate
            objCollectionAccountCollectionNature.AccountNumber = ICCollAccountCollectionNature.AccountNumber
            objCollectionAccountCollectionNature.BranchCode = ICCollAccountCollectionNature.BranchCode
            objCollectionAccountCollectionNature.Currency = ICCollAccountCollectionNature.Currency
            objCollectionAccountCollectionNature.CollectionNatureCode = ICCollAccountCollectionNature.CollectionNatureCode
            objCollectionAccountCollectionNature.IsActive = True
            objCollectionAccountCollectionNature.IsApproved = True
            objCollectionAccountCollectionNature.Save()

            ICUtilities.AddAuditTrail(ActionText.ToString(), "Collection Account Collection Nature", objCollectionAccountCollectionNature.AccountNumber & "-" & objCollectionAccountCollectionNature.BranchCode & "-" & objCollectionAccountCollectionNature.Currency & "-" & objCollectionAccountCollectionNature.CollectionNatureCode, UserID.ToString(), UserName.ToString(), "ADD")

            Return objCollectionAccountCollectionNature.AccountNumber & ";" & objCollectionAccountCollectionNature.BranchCode & ";" & objCollectionAccountCollectionNature.Currency & ";" & objCollectionAccountCollectionNature.CollectionNatureCode
        End Function

        Public Shared Sub SetgvCollectionAccountCollectionNatures(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")
            Dim qryCollectionNature As New ICCollectionNatureQuery("pn")
            Dim qryCollectionAccount As New ICCollectionAccountsQuery("acc")
            Dim qryAccountCollectionNatures As New ICCollectionAccountsCollectionNatureQuery("acccpn")

            If Not pagenumber = 0 Then

            Else
                qryAccountCollectionNatures.es.PageNumber = 1
                qryAccountCollectionNatures.es.PageSize = pagesize
            End If


            qryAccountCollectionNatures.Select(qryAccountCollectionNatures.AccountNumber, qryAccountCollectionNatures.BranchCode, qryAccountCollectionNatures.Currency, qryAccountCollectionNatures.CollectionNatureCode, qryCollectionNature.CollectionNatureName, qryCollectionAccount.AccountTitle, qryGroup.GroupName, qryCompany.CompanyName)
            qryAccountCollectionNatures.InnerJoin(qryCollectionAccount).On(qryAccountCollectionNatures.AccountNumber = qryCollectionAccount.AccountNumber And qryAccountCollectionNatures.BranchCode = qryCollectionAccount.BranchCode And qryAccountCollectionNatures.Currency = qryCollectionAccount.Currency)
            qryAccountCollectionNatures.InnerJoin(qryCollectionNature).On(qryAccountCollectionNatures.CollectionNatureCode = qryCollectionNature.CollectionNatureCode)
            qryAccountCollectionNatures.InnerJoin(qryCompany).On(qryCollectionAccount.CompanyCode = qryCompany.CompanyCode)
            qryAccountCollectionNatures.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)

            If Not GroupCode.ToString() = "0" Then
                qryAccountCollectionNatures.Where(qryGroup.GroupCode = GroupCode)
            End If
            If Not CompanyCode.ToString() = "0" Then
                qryAccountCollectionNatures.Where(qryCompany.CompanyCode = CompanyCode)
            End If
            qryAccountCollectionNatures.OrderBy(qryGroup.GroupName.Ascending, qryCompany.CompanyName.Ascending, qryAccountCollectionNatures.AccountNumber.Ascending, qryCollectionNature.CollectionNatureName.Ascending)
            rg.DataSource = qryAccountCollectionNatures.LoadDataTable()
            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub

        Public Shared Sub DeleteCollectionAccountCollectionNature(ByVal ICCollectionAccountCollectionNature As ICCollectionAccountsCollectionNature, ByVal ActionText As String, ByVal UserID As String, ByVal UserName As String)
            Dim objCollectionAccountCollectionNature As New ICCollectionAccountsCollectionNature
            objCollectionAccountCollectionNature.es.Connection.CommandTimeout = 3600

            objCollectionAccountCollectionNature.LoadByPrimaryKey(ICCollectionAccountCollectionNature.AccountNumber, ICCollectionAccountCollectionNature.BranchCode, ICCollectionAccountCollectionNature.Currency, ICCollectionAccountCollectionNature.CollectionNatureCode)
            objCollectionAccountCollectionNature.MarkAsDeleted()
            objCollectionAccountCollectionNature.Save()

            ICUtilities.AddAuditTrail(ActionText.ToString(), "Collection Account Collection Nature", ICCollectionAccountCollectionNature.AccountNumber & ";" & ICCollectionAccountCollectionNature.BranchCode & ";" & ICCollectionAccountCollectionNature.Currency & ";" & ICCollectionAccountCollectionNature.CollectionNatureCode, UserID.ToString(), UserName.ToString(), "DELETE")
        End Sub

        Public Shared Function GetCollectionAccountCollectionNatureByCompanyCode(ByVal CompanyCode As String) As DataTable
            Dim qryObjICCollAccountCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountCollNature")
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICCollAccountCollNature.Select((qryObjICCollAccountCollNature.AccountNumber + "-" + qryObjICCollectionNature.CollectionNatureName).As("CollectionAccountAndCollectionNature"))
            qryObjICCollAccountCollNature.Select((qryObjICCollAccountCollNature.AccountNumber + "-" + qryObjICCollAccountCollNature.BranchCode + "-" + qryObjICCollAccountCollNature.Currency + "-" + qryObjICCollAccountCollNature.CollectionNatureCode).As("CollectionAccountCollectionNature"))
            qryObjICCollAccountCollNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICCollAccountCollNature.InnerJoin(qryObjICCollectionNature).On(qryObjICCollAccountCollNature.CollectionNatureCode = qryObjICCollectionNature.CollectionNatureCode)
            qryObjICCollAccountCollNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            qryObjICCollAccountCollNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)

            dt = qryObjICCollAccountCollNature.LoadDataTable
            Return dt

        End Function

    End Class
End Namespace