﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICDDInstrumentsController
        Public Shared Sub AddDDInstrumentSeriesFromDDMasterSeries(ByVal DDInstruments As ICDDInstruments, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objICDDInstruments As New ICDDInstruments
            Dim prevobjICDDInstruments As New ICDDInstruments
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""

            objICDDInstruments.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objICDDInstruments.CreatedBy = DDInstruments.CreatedBy
                objICDDInstruments.CreatedDate = DDInstruments.CreatedDate
                objICDDInstruments.IsUsed = DDInstruments.IsUsed
                objICDDInstruments.Creater = DDInstruments.Creater
                objICDDInstruments.CreationDate = DDInstruments.CreationDate
            Else
                objICDDInstruments.LoadByPrimaryKey(DDInstruments.DDMasterSeriesID)
                prevobjICDDInstruments.LoadByPrimaryKey(DDInstruments.DDMasterSeriesID)
                objICDDInstruments.CreatedBy = DDInstruments.CreatedBy
                objICDDInstruments.CreatedDate = DDInstruments.CreatedDate
            End If

            objICDDInstruments.PreFix = DDInstruments.PreFix
            objICDDInstruments.DDMasterSeriesID = DDInstruments.DDMasterSeriesID
            objICDDInstruments.DDInstrumentNumber = DDInstruments.DDInstrumentNumber


            objICDDInstruments.Save()

            If (isUpdate = False) Then
                CurrentAt = "DDInstruments [Code:  " & objICDDInstruments.DDInstrumentsID.ToString() & " ; DDMasterSeriesID: " & objICDDInstruments.DDMasterSeriesID.ToString() & " ; Prefix:  " & objICDDInstruments.PreFix.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "DDInstruments", objICDDInstruments.DDInstrumentsID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "DDInstruments : Current Values [Code:  " & objICDDInstruments.DDInstrumentsID.ToString() & " ; DDMasterSeriesID: " & objICDDInstruments.DDMasterSeriesID.ToString() & " ; Prefix:  " & objICDDInstruments.PreFix.ToString() & "]"
                PrevAt = "<br />DDInstruments : Previous Values [Code:  " & prevobjICDDInstruments.DDInstrumentsID.ToString() & " ; DDMasterSeriesID: " & prevobjICDDInstruments.DDMasterSeriesID.ToString() & " ; Prefix:  " & prevobjICDDInstruments.PreFix.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "DDInstruments", objICDDInstruments.DDInstrumentsID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub

        Public Shared Sub DeleteAddedDDInstrumentSeriesByDDMasterSeriesID(ByVal DDMasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim objICDDInstrument As New ICDDInstruments

            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600
            objICDDInstrument.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID)
            objICDDInstrumentColl.Query.Load()

            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstrumentInColl As ICDDInstruments In objICDDInstrumentColl
                    objICDDInstrument.LoadByPrimaryKey(objICDDInstrumentInColl.DDInstrumentsID)
                    objICDDInstrument.MarkAsDeleted()
                    objICDDInstrument.Save()
                    ICUtilities.AddAuditTrail("Instrument No: " & objICDDInstrumentInColl.DDInstrumentsID.ToString & " ; DD Master Series No: " & objICDDInstrumentInColl.DDMasterSeriesID.ToString & " ; Prefix: " & objICDDInstrumentInColl.PreFix.ToString & " deleted", "DDInstruments", objICDDInstrumentInColl.DDInstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub

        Public Shared Sub DeleteUpdatedDDInstrumentSeriesByDDMasterSeriesID(ByVal DDMasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal TopOrder As Integer)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim objICDDInstrument As New ICDDInstruments

            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600
            objICDDInstrument.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID)
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber.Descending)
            objICDDInstrumentColl.Query.es.Top = TopOrder
            objICDDInstrumentColl.Query.Load()
            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstrumentInColl As ICDDInstruments In objICDDInstrumentColl
                    objICDDInstrument.LoadByPrimaryKey(objICDDInstrumentInColl.DDInstrumentsID)
                    objICDDInstrument.MarkAsDeleted()
                    objICDDInstrument.Save()
                    ICUtilities.AddAuditTrail("Instrument No: " & objICDDInstrumentInColl.DDInstrumentsID.ToString & " of Master Series with ID: " & objICDDInstrumentInColl.DDMasterSeriesID.ToString & " deleted on update.", "Instrument", objICDDInstrumentInColl.DDInstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub

        Public Shared Function GetLastDDInstrumentOfDDMasterSeries(ByVal DDMasterSeriesID As String) As Integer
            Dim qryDDInstrument As New ICDDInstrumentsQuery("qryDDInstrument")
            Dim dt As New DataTable

            qryDDInstrument.Select(qryDDInstrument.DDInstrumentNumber)
            qryDDInstrument.Where(qryDDInstrument.DDMasterSeriesID = DDMasterSeriesID)
            qryDDInstrument.OrderBy(qryDDInstrument.DDInstrumentNumber.Descending)
            qryDDInstrument.es.Top = 1
            dt = qryDDInstrument.LoadDataTable
            Return CInt(dt.Rows(0)(0))
        End Function

        'Public Shared Function GetFirstUnAssignedInstrumentOfDDInstrumentSeries() As String
        '    Dim qryObjICDDInstrument As New ICDDInstrumentsQuery("qryObjICDDInstrument")
        '    Dim dt As New DataTable
        '    Dim StrInstrumentNumber As String = Nothing

        '    qryObjICDDInstrument.Select((qryObjICDDInstrument.PreFix.Cast(EntitySpaces.DynamicQuery.esCastType.String) + "-" + qryObjICDDInstrument.DDInstrumentNumber.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("DDInstrumentNumber"))
        '    qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull)
        '    qryObjICDDInstrument.GroupBy(qryObjICDDInstrument.PreFix, qryObjICDDInstrument.DDInstrumentNumber)
        '    qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.DDInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    qryObjICDDInstrument.es.Top = 1
        '    dt = qryObjICDDInstrument.LoadDataTable
        '    If dt.Rows.Count > 0 Then
        '        StrInstrumentNumber = CStr(dt.Rows(0)(0))
        '    End If
        '    Return StrInstrumentNumber
        'End Function
        Public Shared Function GetFirstUnAssignedInstrumentOfDDInstrumentSeriesByPrintLocation(ByVal PrintLocationCode As String) As String
            Dim qryObjICDDInstrument As New ICDDInstrumentsQuery("qryObjICDDInstrument")
            Dim dt As New DataTable
            Dim StrInstrumentNumber As String = Nothing

            qryObjICDDInstrument.Select((qryObjICDDInstrument.PreFix.Cast(EntitySpaces.DynamicQuery.esCastType.String) + "-" + qryObjICDDInstrument.DDInstrumentNumber.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("DDInstrumentNumber"))
            qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull And qryObjICDDInstrument.OfficeCode = PrintLocationCode)
            qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.DDMasterSeriesID.Ascending, qryObjICDDInstrument.PreFix.Ascending, qryObjICDDInstrument.DDInstrumentNumber.Ascending)
            qryObjICDDInstrument.es.Top = 1
            dt = qryObjICDDInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                StrInstrumentNumber = CStr(dt.Rows(0)(0))
            End If
            Return StrInstrumentNumber
        End Function
        Public Shared Sub MarkInstrumentNumberAssignedByInstructionIDAndDDInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal PrintLocationCode As String)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber = InstrumentNumber And objICDDInstrumentColl.Query.PreFix = Prefix And objICDDInstrumentColl.Query.OfficeCode = PrintLocationCode)
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstrument As ICDDInstruments In objICDDInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICDDInstrument.PreFix & "-" & objICDDInstrument.DDInstrumentNumber & " ] assigned to instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is assigned by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICDDInstrument.IsAssigned = True
                    objICDDInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "DDInstruments", objICDDInstrument.DDInstrumentsID.ToString, UsersID, UsersName, "ASSIGN")
                Next
            End If
        End Sub
        Public Shared Sub MarkInstrumentNumberIsUsedByInstructionIDAndDDInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal PrintLocationCode As String)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber = InstrumentNumber And objICDDInstrumentColl.Query.PreFix = Prefix And objICDDInstrumentColl.Query.OfficeCode = PrintLocationCode)
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstrument As ICDDInstruments In objICDDInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICDDInstrument.PreFix & "-" & objICDDInstrument.DDInstrumentNumber & " ] used by instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is printed by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICDDInstrument.IsUsed = True
                    objICDDInstrument.UsedOn = Date.Now
                    objICDDInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "DDInstruments", objICDDInstrument.DDInstrumentsID.ToString, UsersID, UsersName, "PRINT")
                Next
            End If
        End Sub
        Public Shared Sub MarkInstrumentNumberIsCancelledByInstructionIDAndDDInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber = InstrumentNumber And objICDDInstrumentColl.Query.PreFix = Prefix)
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstrument As ICDDInstruments In objICDDInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICDDInstrument.PreFix & "-" & objICDDInstrument.DDInstrumentNumber & " ] unassigned by instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is unassigned by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICDDInstrument.IsUsed = False
                    objICDDInstrument.IsAssigned = Nothing
                    objICDDInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "DDInstruments", objICDDInstrument.DDInstrumentsID.ToString, UsersID, UsersName, "UNASSIGN")
                Next
            End If
        End Sub
        Public Shared Function IsDDInstrumentNumberExistsByPrefixAndInstrumentNo(ByVal InstrumentNumber As String, ByVal Prefix As String) As Boolean
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber = InstrumentNumber And objICDDInstrumentColl.Query.PreFix = Prefix)
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICDDInstrumentColl.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsDDInstrumentNumberAssignedOrPrintedByPrefixAndInstrumentNoAndInstructionID(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal PrintLocationCode As String) As Boolean
            Dim qryObjICDDInstrument As New ICDDInstrumentsQuery("qryObjICDDInstrument")
            Dim dt As New DataTable
            qryObjICDDInstrument.Select(qryObjICDDInstrument.PreFix, qryObjICDDInstrument.DDInstrumentNumber)
            qryObjICDDInstrument.Where(qryObjICDDInstrument.PreFix = Prefix And qryObjICDDInstrument.DDInstrumentNumber = InstrumentNumber And qryObjICDDInstrument.OfficeCode = PrintLocationCode)
            qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = True Or qryObjICDDInstrument.IsAssigned = True)
            dt = qryObjICDDInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        End Function
        ''22-10-2013
        Public Shared Sub GetAllAssignedDDSubSetsToPrintLocationsForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal OfficeCode As String, ByVal POMasterSereisID As String)

            Dim qryObjICDDInstrument As New ICDDInstrumentsQuery("qryObjICDDInstrument")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICDDInstrumentCount As New ICDDInstrumentsQuery("qryObjICDDInstrumentCount")
            Dim qryObjICDDInstrumentUsedCount As New ICDDInstrumentsQuery("qryObjICDDInstrumentUsedCount")
            Dim qryObjICDDInstrumentUnUsedCount As New ICDDInstrumentsQuery("qryObjICDDInstrumentUnUsedCount")
            Dim qryObjICDDMasterSeries As New ICDDMasterSeriesQuery("qryObjICDDMasterSeries")
            Dim dt As New DataTable



            qryObjICDDInstrument.Select(qryObjICDDInstrument.DDMasterSeriesID, qryObjICDDInstrument.SubsetFrom, qryObjICDDInstrument.SubsetTo, qryObjICDDInstrument.OfficeCode)
            qryObjICDDInstrument.Select((qryObjICDDMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String) + " - " + qryObjICDDMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("DDMasterSeries"))
            qryObjICDDInstrument.Select((qryObjICOffice.OfficeCode + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICDDInstrument.Select(qryObjICDDInstrumentCount.DDInstrumentNumber.Count.As("Series"))
            qryObjICDDInstrument.Select((qryObjICDDInstrumentUsedCount.[Select](qryObjICDDInstrumentUsedCount.DDInstrumentNumber.Count).Where((qryObjICDDInstrumentUsedCount.IsAssigned = True Or qryObjICDDInstrumentUsedCount.IsUsed = True) And qryObjICDDInstrumentUsedCount.DDMasterSeriesID = qryObjICDDInstrument.DDMasterSeriesID And qryObjICDDInstrumentUsedCount.OfficeCode = qryObjICDDInstrument.OfficeCode And qryObjICDDInstrumentUsedCount.SubsetFrom = qryObjICDDInstrument.SubsetFrom And qryObjICDDInstrumentUsedCount.SubsetTo = qryObjICDDInstrument.SubsetTo).As("Used")))
            qryObjICDDInstrument.Select((qryObjICDDInstrumentUnUsedCount.[Select](qryObjICDDInstrumentUnUsedCount.DDInstrumentNumber.Count).Where(qryObjICDDInstrumentUnUsedCount.IsUsed = False And qryObjICDDInstrumentUnUsedCount.IsAssigned.IsNull And qryObjICDDInstrumentUnUsedCount.DDMasterSeriesID = qryObjICDDInstrument.DDMasterSeriesID And qryObjICDDInstrumentUnUsedCount.OfficeCode = qryObjICDDInstrument.OfficeCode And qryObjICDDInstrumentUnUsedCount.SubsetFrom = qryObjICDDInstrument.SubsetFrom And qryObjICDDInstrumentUnUsedCount.SubsetTo = qryObjICDDInstrument.SubsetTo)).As("UnUsed"))
            qryObjICDDInstrument.InnerJoin(qryObjICOffice).On(qryObjICDDInstrument.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICDDInstrument.InnerJoin(qryObjICDDInstrumentCount).On(qryObjICDDInstrument.DDInstrumentsID = qryObjICDDInstrumentCount.DDInstrumentsID)
            qryObjICDDInstrument.InnerJoin(qryObjICDDMasterSeries).On(qryObjICDDInstrument.DDMasterSeriesID = qryObjICDDMasterSeries.DDMasterSeriesID)

            If Not OfficeCode = "" Then
                qryObjICDDInstrument.Where(qryObjICOffice.OfficeID = OfficeCode)
            End If
            If Not POMasterSereisID = "" Then
                qryObjICDDInstrument.Where(qryObjICDDInstrument.DDMasterSeriesID = POMasterSereisID)
            End If


            qryObjICDDInstrument.GroupBy(qryObjICDDInstrument.DDMasterSeriesID, qryObjICDDInstrument.SubsetFrom, qryObjICDDInstrument.SubsetTo, qryObjICDDInstrument.OfficeCode, qryObjICDDMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICDDMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICOffice.OfficeCode, qryObjICOffice.OfficeName)
            qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.DDMasterSeriesID.Descending)
            dt = qryObjICDDInstrument.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICDDInstrument.es.PageNumber = PageNumber
                qryObjICDDInstrument.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICDDInstrument.es.PageNumber = 1
                qryObjICDDInstrument.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function IsDDInstrumentIsUsedOrAssignedAgainstSubSetIDAndOfficeID(ByVal DDMasterSeriesID As String, ByVal OfficeID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String) As Boolean
            Dim objIDDCInstrumentColl As New ICDDInstrumentsCollection
            Dim Result As Boolean = False

            objIDDCInstrumentColl.es.Connection.CommandTimeout = 3600

            objIDDCInstrumentColl.Query.Where(objIDDCInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID And objIDDCInstrumentColl.Query.OfficeCode = OfficeID)
            objIDDCInstrumentColl.Query.Where(objIDDCInstrumentColl.Query.SubsetFrom = SubSetFrom And objIDDCInstrumentColl.Query.SubsetTo = SubSetTo)

            If objIDDCInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICDDInstruments In objIDDCInstrumentColl
                    If Not objICInstrument.IsUsed = False Or Not objICInstrument.IsAssigned Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If
            Return Result
        End Function
        Public Shared Sub DeleteDDInstrumentsAgainstOfficeID(ByVal DDMasterSeriesID As String, ByVal OfficeID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objIDDCInstrumentColl As New ICDDInstrumentsCollection
            Dim objIDDCInstrumentCollForDelete As New ICDDInstrumentsCollection
            Dim objICOffice As New ICOffice
            Dim Result As Boolean = False

            objIDDCInstrumentColl.es.Connection.CommandTimeout = 3600

            objIDDCInstrumentColl.Query.Where(objIDDCInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID And objIDDCInstrumentColl.Query.OfficeCode = OfficeID)
            objIDDCInstrumentColl.Query.Where(objIDDCInstrumentColl.Query.SubsetFrom = SubSetFrom And objIDDCInstrumentColl.Query.SubsetTo = SubSetTo)

            If objIDDCInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICDDInstruments In objIDDCInstrumentColl
                    objICInstrument.SubsetTo = Nothing
                    objICInstrument.SubsetFrom = Nothing
                    objICInstrument.OfficeCode = Nothing
                    objIDDCInstrumentCollForDelete.Add(objICInstrument)
                Next
                If objIDDCInstrumentCollForDelete.Count > 0 Then
                    objIDDCInstrumentCollForDelete.Save()
                    objICOffice.LoadByPrimaryKey(OfficeID)
                    ICUtilities.AddAuditTrail("DD Sub Set with ID [ " & DDMasterSeriesID & " ] unassigned from office [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & SubSetFrom & " ] to [ " & SubSetTo & " ] .", "DD Sub Set", objICOffice.OfficeID, UsersID.ToString, UsersName.ToString, "UNASSIGN")
                End If
            End If

        End Sub
        Public Shared Function GetSingleDDInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(ByVal DDMasterSeriesID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String, ByVal OfficeCode As String) As ICDDInstruments
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID, objICDDInstrumentColl.Query.DDInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.OfficeCode = OfficeCode)
            objICDDInstrumentColl.Query.Load()
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDMasterSeriesID.Ascending)
            Return objICDDInstrumentColl(0)
        End Function
        Public Shared Function IsDDInstrumentSeriesIsAssignedOrPrintedForSubSet(ByVal SubSetFrom As Long, ByVal SubSetTo As Long, ByVal DDMasterSeriesID As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrumentColl As New ICDDInstrumentsCollection
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID And objICInstrumentColl.Query.DDInstrumentNumber.Between(SubSetFrom, SubSetTo))
            If objICInstrumentColl.Query.Load() Then
                For Each objICPOInstrument As ICDDInstruments In objICInstrumentColl
                    If Not objICPOInstrument.OfficeCode Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If

            Return Result
        End Function
        Public Shared Function GetDDInstrumentsForSubSetByDDMasterSeriesIDAndSubSetRange(ByVal DDMasterSeriesID As String, ByVal SubSetFrom As Long, ByVal SubSetTo As Long) As ICDDInstrumentsCollection
            Dim objICDDInstrumentcoll As New ICDDInstrumentsCollection
            objICDDInstrumentcoll.Query.Where(objICDDInstrumentcoll.Query.DDMasterSeriesID = DDMasterSeriesID And objICDDInstrumentcoll.Query.DDInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICDDInstrumentcoll.Query.Load()
            objICDDInstrumentcoll.Query.OrderBy(objICDDInstrumentcoll.Query.DDInstrumentsID.Ascending)
            Return objICDDInstrumentcoll
        End Function
        Public Shared Function IsDDInstrumentsExistForUpdateOfDDSubSet(ByVal TopOrder As Integer, ByVal objICDDInstrument As ICDDInstruments, ByVal SubSetTo As String, ByVal IsExtended As Boolean) As Boolean
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim dt As New DataTable
            Dim Result As Boolean = False
            objICDDInstrumentColl.es.Connection.CommandTimeout = 3600


            If IsExtended = False Then
                objICDDInstrumentColl.Query.Select(objICDDInstrumentColl.Query.DDInstrumentNumber.Count)
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber.Between(objICDDInstrument.SubsetTo + 1, SubSetTo))
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = objICDDInstrument.DDMasterSeriesID)
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.OfficeCode = objICDDInstrument.OfficeCode And objICDDInstrumentColl.Query.IsAssigned.IsNull And objICDDInstrumentColl.Query.IsUsed = False)
                objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber.Descending)
                objICDDInstrumentColl.Query.es.Top = TopOrder
                objICDDInstrumentColl.Query.Load()
                dt = objICDDInstrumentColl.Query.LoadDataTable
                If objICDDInstrumentColl.Query.Load Then
                    If CLng(dt.Rows(0)(0) >= TopOrder) Then
                        Result = True
                    End If
                End If
            ElseIf IsExtended = True Then
                objICDDInstrumentColl.Query.Select(objICDDInstrumentColl.Query.DDInstrumentNumber.Count)
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber.Between(SubSetTo + 1, objICDDInstrument.SubsetTo))
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.IsUsed = False And objICDDInstrumentColl.Query.IsAssigned.IsNull And objICDDInstrumentColl.Query.OfficeCode.IsNull)
                objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = objICDDInstrument.DDMasterSeriesID)
                objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber.Descending)
                objICDDInstrumentColl.Query.es.Top = TopOrder
                dt = objICDDInstrumentColl.Query.LoadDataTable
                If objICDDInstrumentColl.Query.Load Then
                    If CLng(dt.Rows(0)(0) >= TopOrder) Then
                        Result = True
                    End If
                End If
            End If
            Return Result

        End Function
        'Public Shared Function IsDDInstrumentsExistForUpdateOfDDSubSet(ByVal TopOrder As Integer, ByVal objICDDInstrument As ICDDInstruments, ByVal SubSetTo As String, ByVal IsExtended As Boolean) As Boolean
        '    Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
        '    Dim RequiredLeavesCount As Long
        '    Dim objICInstrument As ICDDInstruments
        '    Dim dt As New DataTable
        '    Dim Result As Boolean = False
        '    objICDDInstrumentColl.es.Connection.CommandTimeout = 3600


        '    If IsExtended = False Then
        '        'objICDDInstrumentColl.Query.Select(objICDDInstrumentColl.Query.DDInstrumentNumber.Count)
        '        objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = objICDDInstrument.DDMasterSeriesID)
        '        objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.OfficeCode = objICDDInstrument.OfficeCode And objICDDInstrumentColl.Query.IsUsed = False And objICDDInstrumentColl.Query.IsAssigned.IsNull)
        '        objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber.Descending)
        '        objICDDInstrumentColl.Query.es.Top = TopOrder
        '        objICDDInstrumentColl.Query.Load()
        '        dt = objICDDInstrumentColl.Query.LoadDataTable
        '        If objICDDInstrumentColl.Query.Load Then

        '            For i = 0 To TopOrder - 1
        '                objICInstrument = New ICDDInstruments
        '                objICInstrument = objICDDInstrumentColl(i)
        '                If objICInstrument.IsUsed = False And objICInstrument.IsAssigned Is Nothing Then
        '                    RequiredLeavesCount = RequiredLeavesCount + 1
        '                End If
        '            Next
        '            If RequiredLeavesCount >= TopOrder Then
        '                Result = True
        '            End If

        '        End If
        '    ElseIf IsExtended = True Then
        '        objICDDInstrumentColl.Query.Select(objICDDInstrumentColl.Query.DDInstrumentNumber.Count)
        '        objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.IsUsed = False And objICDDInstrumentColl.Query.IsAssigned.IsNull And objICDDInstrumentColl.Query.OfficeCode.IsNull)
        '        objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = objICDDInstrument.DDMasterSeriesID And objICDDInstrumentColl.Query.SubsetFrom.IsNull)
        '        objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.SubsetTo.IsNull)
        '        objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentNumber.Descending)
        '        objICDDInstrumentColl.Query.es.Top = TopOrder
        '        dt = objICDDInstrumentColl.Query.LoadDataTable
        '        If objICDDInstrumentColl.Query.Load Then
        '            If CLng(dt.Rows(0)(0) >= TopOrder) Then
        '                Result = True
        '            End If
        '        End If
        '    End If
        '    Return Result

        'End Function
        Public Shared Sub UpDateDDInstrumentsForUpdateOfSubSet(ByVal TopOrder As Integer, ByVal objICDDInstrument As ICDDInstruments, ByVal SubSetTo As String, ByVal UsersID As String, ByVal UsersName As String, ByVal IsExtended As Boolean)
            Dim objICInstrumentColl As New ICDDInstrumentsCollection
            Dim objICInstrumentCollSave As New ICDDInstrumentsCollection
            Dim objInstrumentForUpDate As New ICDDInstruments
            Dim objICOffice As New ICOffice
            Dim dt As New DataTable
            Dim Result As Boolean = False
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objInstrumentForUpDate.es.Connection.CommandTimeout = 3600
            If IsExtended = False Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.DDInstrumentNumber.Between(objICDDInstrument.SubsetTo + 1, SubSetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.OfficeCode = objICDDInstrument.OfficeCode And objICInstrumentColl.Query.IsUsed = False And objICInstrumentColl.Query.IsAssigned.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.DDInstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    For Each objInstrument As ICDDInstruments In objICInstrumentColl
                        objInstrumentForUpDate = New ICDDInstruments
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.DDInstrumentsID.ToString)
                        objInstrumentForUpDate.SubsetFrom = Nothing
                        objInstrumentForUpDate.SubsetTo = Nothing
                        objInstrumentForUpDate.OfficeCode = Nothing
                        objICInstrumentCollSave.Add(objInstrumentForUpDate)
                    Next
                    If objICInstrumentCollSave.Count > 0 Then
                        objICInstrumentCollSave.Save()
                        objICOffice.LoadByPrimaryKey(objICDDInstrument.OfficeCode)
                        ICUtilities.AddAuditTrail("DD Sub Set of master series with ID [ " & objICDDInstrument.DDMasterSeriesID & " ] unassigned from [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & objICDDInstrument.SubsetFrom & " ] to [ " & objICDDInstrument.SubsetTo - TopOrder & " ]. ", "DD Sub Set", objICOffice.OfficeID.ToString, UsersID.ToString, UsersName.ToString, "UNASSIGN")

                    End If
                End If
            ElseIf IsExtended = True Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.DDInstrumentNumber.Between(SubSetTo + 1, objICDDInstrument.SubsetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.IsUsed = False And objICInstrumentColl.Query.DDMasterSeriesID = objICDDInstrument.DDMasterSeriesID)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubsetFrom.IsNull And objICInstrumentColl.Query.SubsetTo.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.DDInstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    For Each objInstrument As ICDDInstruments In objICInstrumentColl
                        objInstrumentForUpDate = New ICDDInstruments
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.DDInstrumentsID.ToString)
                        objInstrumentForUpDate.SubsetFrom = objICDDInstrument.SubsetFrom
                        objInstrumentForUpDate.SubsetTo = objICDDInstrument.SubsetTo
                        objInstrumentForUpDate.OfficeCode = objICDDInstrument.OfficeCode
                        objICInstrumentCollSave.Add(objInstrumentForUpDate)
                    Next
                    If objICInstrumentCollSave.Count > 0 Then
                        objICInstrumentCollSave.Save()
                        objICOffice.LoadByPrimaryKey(objICDDInstrument.OfficeCode)
                        ICUtilities.AddAuditTrail("DD Sub Set of master series with ID [ " & objICDDInstrument.DDMasterSeriesID & " ] assigned to [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & objICDDInstrument.SubsetFrom & " ] to [ " & objICDDInstrument.SubsetTo & " ]. ", "PO Sub Set", objICOffice.OfficeID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")

                    End If
                End If
            End If


        End Sub
        Public Shared Sub UpDateSubSetRangeOnUpDate(ByVal DDMasterSeriesID As String, ByVal SubSetFrom As Long, ByVal SubSetTo As Long, ByVal OfficeCode As String, ByVal objICDDInstrument As ICDDInstruments)
            Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
            Dim objICDDInstrumentForUpdate As New ICDDInstruments
            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID = DDMasterSeriesID And objICDDInstrumentColl.Query.DDInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.OfficeCode = OfficeCode)
            objICDDInstrumentColl.Query.Load()
            objICDDInstrumentColl.Query.OrderBy(objICDDInstrumentColl.Query.DDInstrumentsID.Ascending)

            If objICDDInstrumentColl.Query.Load Then
                For Each objICDDInstruments As ICDDInstruments In objICDDInstrumentColl
                    objICDDInstrumentForUpdate = New ICDDInstruments
                    objICDDInstrumentForUpdate.LoadByPrimaryKey(objICDDInstruments.DDInstrumentsID.ToString)
                    objICDDInstrumentForUpdate.SubsetFrom = SubSetFrom
                    objICDDInstrumentForUpdate.SubsetTo = SubSetTo
                    objICDDInstrumentForUpdate.Save()
                Next
            End If
        End Sub
        Public Shared Function IsDDInstrumentNoIsAssignedToSpecificPrintLocationByInstrumentNoAndOfficeCode(ByVal InstrumentNo As String, ByVal Prefix As String, ByVal OfficeCode As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrument As New ICDDInstruments
            Dim objICInstrumentColl As New ICDDInstrumentsCollection

            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.DDInstrumentNumber = InstrumentNo And objICInstrumentColl.Query.OfficeCode = OfficeCode And objICInstrumentColl.Query.PreFix = Prefix)
            If objICInstrumentColl.Query.Load Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function GetTotalUnUSedDDInstrumentCountByPrintLocationCode(ByVal PrintLocationCode As String) As Integer
            Dim qryObjICDDInstrument As New ICDDInstrumentsQuery("qryObjICDDInstrument")
            Dim dt As New DataTable

            qryObjICDDInstrument.Select(qryObjICDDInstrument.DDInstrumentNumber.Count.As("Count"))
            qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.OfficeCode = PrintLocationCode.ToString And qryObjICDDInstrument.IsAssigned.IsNull)
            dt = qryObjICDDInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return CInt(dt.Rows(0)(0))
            Else
                Return 0
            End If
        End Function
    End Class
End Namespace
