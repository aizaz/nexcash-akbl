﻿Namespace IC
    Public Class ICTemplateFieldController
        Public Shared Function GetTemplateFieldsTemplateID(ByVal TemplateID As String) As ICTemplateFieldsCollection
            Dim objICTemplateFieldListColl As New IC.ICTemplateFieldsCollection

            objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = TemplateID)
            objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.FieldOrder.Ascending)
            objICTemplateFieldListColl.Query.Load()
            Return objICTemplateFieldListColl
        End Function
        Public Shared Sub InsertTemplateField(ByVal cTemplateField As ICTemplateFields)
            Dim objICTemplateFields As New ICTemplateFields
            objICTemplateFields.es.Connection.CommandTimeout = 3600
            

            objICTemplateFields.FieldID = cTemplateField.FieldID
            objICTemplateFields.FieldType = cTemplateField.FieldType
            objICTemplateFields.FieldOrder = cTemplateField.FieldOrder
            objICTemplateFields.FieldName = cTemplateField.FieldName
            objICTemplateFields.TemplateID = cTemplateField.TemplateID
            objICTemplateFields.FixLength = cTemplateField.FixLength
            'objICTemplateFields.IsRequired = cTemplateField.IsRequired
            'objICTemplateFields.MustRequired = cTemplateField.MustRequired
            objICTemplateFields.IsActive = True
            objICTemplateFields.CreateBy = cTemplateField.CreateBy
            objICTemplateFields.CreateDate = cTemplateField.CreateDate
            objICTemplateFields.Save()
        End Sub
        Public Shared Sub UpdateTemplateField(ByVal FieldID As String, ByVal TemplateID As String, ByVal cTemplateField As ICTemplateFields)
            Dim objICTemplateFields As New ICTemplateFields
            objICTemplateFields.es.Connection.CommandTimeout = 3600
            objICTemplateFields.LoadByPrimaryKey(FieldID, TemplateID)

            objICTemplateFields.FieldOrder = cTemplateField.FieldOrder
            objICTemplateFields.FixLength = cTemplateField.FixLength
            'objICTemplateFields.IsRequired = cTemplateField.IsRequired
            'objICTemplateFields.MustRequired = cTemplateField.MustRequired
            objICTemplateFields.IsActive = True
            objICTemplateFields.Save()
        End Sub
        Public Shared Sub UpdateFieldOrderByFieldID(ByVal FieldOrderFrom As String, ByVal FieldOrderTo As String, ByVal TemplateID As String, ByVal FieldID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICTemplateFields As New ICTemplateFields
            objICTemplateFields.es.Connection.CommandTimeout = 3600
            If objICTemplateFields.LoadByPrimaryKey(FieldID, TemplateID) Then
                objICTemplateFields.FieldOrder = FieldOrderTo
                objICTemplateFields.Save()
                ICUtilities.AddAuditTrail("File upload template's field " & objICTemplateFields.FieldName & "  order has been chnaged from " & FieldOrderFrom & " to " & FieldOrderTo & " .", "File Upload Template", objICTemplateFields.FieldID.ToString & "-" & objICTemplateFields.TemplateID.ToString, UsersID, UsersName, "UPDATE")
            End If
        End Sub
        'Public Shared Function GetTemplateFieldsByTemplateIDAndFieldType(ByVal TemplateID As String, ByVal FieldType As String) As ICTemplateFieldsCollection
        '    Dim objICTemplateFieldListColl As New IC.ICTemplateFieldsCollection

        '    objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600
        '    objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = TemplateID And objICTemplateFieldListColl.Query.FieldType = FieldType)
        '    objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.FieldOrder.Ascending)
        '    objICTemplateFieldListColl.Query.Load()
        '    Return objICTemplateFieldListColl
        'End Function
        Public Shared Function GetTemplateFieldsByTemplateIDAndFieldType(ByVal TemplateID As String, ByVal FieldType As String) As ICTemplateFieldsCollection
            Dim objICTemplateFieldListColl As New IC.ICTemplateFieldsCollection

            objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = TemplateID)

            If FieldType = "NonFlexi" Then
                objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.FieldType = "NonFlexi" Or objICTemplateFieldListColl.Query.FieldType = "DetailField")
            Else
                objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.FieldType = FieldType)
            End If
            objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.FieldOrder.Ascending)
            objICTemplateFieldListColl.Query.Load()
            Return objICTemplateFieldListColl
        End Function
    End Class
End Namespace
