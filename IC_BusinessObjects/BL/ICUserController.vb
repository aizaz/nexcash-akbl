﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICUserController
        Public Shared Sub AddICUser(ByVal ICUser As ICUser, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String, ByVal UserType As String)
            Dim objICUser As New ICUser
            objICUser.es.Connection.CommandTimeout = 3600


            If IsUpdate = False Then
                objICUser.CreateBy = ICUser.CreateBy
                objICUser.CreateDate = ICUser.CreateDate
                objICUser.Creater = ICUser.Creater
                objICUser.CreationDate = ICUser.CreationDate
            Else
                objICUser.LoadByPrimaryKey(ICUser.UserID)
                objICUser.CreateBy = ICUser.CreateBy
                objICUser.CreateDate = ICUser.CreateDate
            End If
            objICUser.UserID = ICUser.UserID
            objICUser.UserName = ICUser.UserName
            objICUser.DisplayName = ICUser.DisplayName
            objICUser.PhoneNo1 = ICUser.PhoneNo1
            objICUser.PhoneNo2 = ICUser.PhoneNo2
            'objICUser.Password = ICUser.Password
            objICUser.CellNo = ICUser.CellNo
            objICUser.Email = ICUser.Email
            objICUser.UserType = ICUser.UserType
            objICUser.OfficeCode = ICUser.OfficeCode
            objICUser.Location = ICUser.Location
            objICUser.Department = ICUser.Department
            objICUser.Is2FARequiredOnApproval = ICUser.Is2FARequiredOnApproval
            objICUser.Is2FARequiredOnLogin = ICUser.Is2FARequiredOnLogin
            objICUser.IsTwoFAVIASMSAllowForLogin = ICUser.IsTwoFAVIASMSAllowForLogin
            objICUser.IsTwoFAVIAEmailAllowForLogin = ICUser.IsTwoFAVIAEmailAllowForLogin
            objICUser.IsTwoFAVIASMSAllowForApproval = ICUser.IsTwoFAVIASMSAllowForApproval
            objICUser.IsTwoFAVIAEmailAllowForApproval = ICUser.IsTwoFAVIAEmailAllowForApproval
            objICUser.IsApproved = False
            objICUser.IsActive = ICUser.IsActive
            objICUser.AuthenticatedVia = ICUser.AuthenticatedVia
            objICUser.EmployeeCode = ICUser.EmployeeCode
            objICUser.ClearingBranchCode = ICUser.ClearingBranchCode
            objICUser.Save()
            If UserType = "Bank User" Then
                If IsUpdate = False Then
                    ICUtilities.AddAuditTrail("Bank User: [ " & objICUser.UserName.ToString() & " ] [ " & objICUser.DisplayName & " ], Authentication Mode [ " & objICUser.AuthenticatedVia & " ] , of Branch [ " & objICUser.UpToICOfficeByOfficeCode.OfficeName & " ] [ " & objICUser.UpToICOfficeByOfficeCode.OfficeCode & " ] , Clearing Branch Code [ " & objICUser.ClearingBranchCode & " ], Email [ " & objICUser.Email & " ], Phone No. 1[ " & objICUser.PhoneNo1 & " ], Phone No. 2[ " & objICUser.PhoneNo2 & " ], Cell No. [ " & objICUser.CellNo & " ], location [ " & objICUser.Location & " ], Employee Code [ " & objICUser.EmployeeCode & " ] department [ " & objICUser.Department & " ], 2FA required on login [ " & objICUser.Is2FARequiredOnLogin & " ], 2FA for login VIA SMS [ " & objICUser.IsTwoFAVIASMSAllowForLogin & " ], 2FA for login VIA Email [ " & objICUser.IsTwoFAVIAEmailAllowForLogin & " ],2FA required on approval [ " & objICUser.Is2FARequiredOnApproval & " ], 2FA for approval VIA SMS [ " & objICUser.IsTwoFAVIASMSAllowForApproval & " ], 2FA for approval VIA Email [ " & objICUser.IsTwoFAVIAEmailAllowForApproval & " ] added", "Bank User", objICUser.UserID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
                ElseIf IsUpdate = True Then
                    ICUtilities.AddAuditTrail(ActionString.ToString(), "Bank User", objICUser.UserID.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")
                End If

            ElseIf UserType = "Client User" Then
                If IsUpdate = False Then
                    ICUtilities.AddAuditTrail("Company User: [ " & objICUser.UserName.ToString() & " ] [ " & objICUser.DisplayName & " ] , Authentication Mode [ " & objICUser.AuthenticatedVia & " ], of Client [ " & objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.CompanyName & " ], for location [ " & objICUser.UpToICOfficeByOfficeCode.OfficeName & " ] [ " & objICUser.UpToICOfficeByOfficeCode.OfficeCode & " ] , Email [ " & objICUser.Email & " ], Phone No. 1[ " & objICUser.PhoneNo1 & " ], Phone No. 2[ " & objICUser.PhoneNo2 & " ], Cell No. [ " & objICUser.CellNo & " ], location [ " & objICUser.Location & " ], department [ " & objICUser.Department & " ], 2FA required on login [ " & objICUser.Is2FARequiredOnLogin & " ], 2FA for login VIA SMS [ " & objICUser.IsTwoFAVIASMSAllowForLogin & " ], 2FA for login VIA Email [ " & objICUser.IsTwoFAVIAEmailAllowForLogin & " ],2FA required on approval [ " & objICUser.Is2FARequiredOnApproval & " ], 2FA for approval VIA SMS [ " & objICUser.IsTwoFAVIASMSAllowForApproval & " ], 2FA for approval VIA Email [ " & objICUser.IsTwoFAVIAEmailAllowForApproval & " ] added", "Client User", objICUser.UserID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
                ElseIf IsUpdate = True Then
                    ICUtilities.AddAuditTrail(ActionString.ToString(), "Client User", objICUser.UserID.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")
                End If

            End If

        End Sub
        Public Shared Sub GetUserByOfficeCode(ByVal Officecode As String, ByVal UserType As String, ByVal UserName As String, ByVal DisplayName As String, ByVal Email As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal GroupCode As String, ByVal CompanyCode As String)
            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")


            Dim dt As New DataTable
            qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.Email, qryObjICUser.PhoneNo1, qryObjICUser.UserType)
            qryObjICUser.Select(qryObjICUser.IsActive, qryObjICUser.IsApproved)
            If UserType = "Bank User" Then
                If Not Officecode.ToString() = "0" Then
                    qryObjICUser.Where(qryObjICUser.OfficeCode = Officecode.ToString())
                End If

                If Not UserType = "" Then
                    qryObjICUser.Where(qryObjICUser.UserType = UserType.ToString())
                Else
                    qryObjICUser.Where(qryObjICUser.UserType = UserType.ToString())
                End If


                If Not UserName.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.UserName = UserName.ToString())
                End If


                If Not DisplayName.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.DisplayName = DisplayName.ToString())
                End If



                If Not Email.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.Email = Email.ToString())
                End If
            ElseIf UserType = "Client User" Then

                qryObjICUser.LeftJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
                qryObjICUser.LeftJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
                qryObjICUser.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
                If Not Officecode.ToString() = "0" Then
                    qryObjICUser.Where(qryObjICUser.OfficeCode = Officecode.ToString())
                End If
                If Not UserType = "" Then
                    qryObjICUser.Where(qryObjICUser.UserType = UserType.ToString())
                End If
                If Not UserName.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.UserName = UserName.ToString())
                End If
                If Not DisplayName.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.DisplayName = DisplayName.ToString())
                End If
                If Not Email.ToString() = "" Then
                    qryObjICUser.Where(qryObjICUser.Email = Email.ToString())
                End If
                If Not GroupCode.ToString = "0" Then
                    qryObjICUser.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
                End If
                If Not CompanyCode.ToString = "0" Then
                    qryObjICUser.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
                End If
            End If
            qryObjICUser.OrderBy(qryObjICUser.UserName.Descending, qryObjICUser.DisplayName.Descending, qryObjICUser.Email.Descending, qryObjICUser.PhoneNo1.Descending, qryObjICUser.UserType.Descending)
            dt = qryObjICUser.LoadDataTable()



            If Not pagenumber = 0 Then
                qryObjICUser.es.PageNumber = pagenumber

                qryObjICUser.es.PageSize = pagesize


                rg.DataSource = dt


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else
                qryObjICUser.es.PageNumber = 1
                qryObjICUser.es.PageSize = pagesize


                rg.DataSource = dt

                If DoDataBind Then


                    rg.DataBind()

                End If
            End If

        End Sub

        Public Shared Function GetClient() As ICGroupCollection
            Dim collGroup As New ICGroupCollection
            collGroup.es.Connection.CommandTimeout = 3600

            collGroup.Query.Select(collGroup.Query.GroupCode, collGroup.Query.GroupName)
            collGroup.Query.Where(collGroup.Query.IsActive = True)
            collGroup.Query.OrderBy(collGroup.Query.GroupName.Ascending)
            collGroup.Query.Load()

            Return collGroup
        End Function
        Public Shared Function GetAllUsersByUserTypeAndReportID(ByVal UserType As String, ByVal ReportID As String, ByVal CompanyCode As String) As DataTable

            Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")
            Dim qryMISReportUsers As New ICMISReportUsersQuery("rpu")
            'Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")
            Dim qryObjICOfficeUsers As New ICOfficeQuery("qryObjICOfficeUsers")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")

            qryObjICUsers.Select(qryObjICUsers.UserID, (qryObjICUsers.UserName + "-" + qryObjICUsers.DisplayName).As("UserName"))
            qryObjICUsers.Where(qryObjICUsers.UserType = UserType.ToString())

            If ReportID <> "0" Then
                If CompanyCode <> "0" Then
                    qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
                    qryObjICUsers.InnerJoin(qryObjICCompany).On(qryObjICOfficeUsers.CompanyCode = qryObjICCompany.CompanyCode)
                    qryObjICUsers.Where(qryObjICCompany.CompanyCode = CompanyCode)
                    qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
                    qryObjICUsers.Where(qryObjICUsers.UserID.NotIn(qryMISReportUsers.[Select](qryMISReportUsers.UserID).Where(qryMISReportUsers.ReportID = ReportID)))

                Else
                    qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
                    qryObjICUsers.InnerJoin(qryObjICBank).On(qryObjICOfficeUsers.BankCode = qryObjICBank.BankCode)
                    qryObjICUsers.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
                    qryObjICUsers.Where(qryObjICUsers.UserID.NotIn(qryMISReportUsers.[Select](qryMISReportUsers.UserID).Where(qryMISReportUsers.ReportID = ReportID)))

                End If
                'qryMISReportUsers.Where(qryMISReportUsers.ReportID = ReportID)
            Else
                If CompanyCode <> "0" Then
                    qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
                    qryObjICUsers.InnerJoin(qryObjICCompany).On(qryObjICOfficeUsers.CompanyCode = qryObjICCompany.CompanyCode)
                    qryObjICUsers.Where(qryObjICCompany.CompanyCode = CompanyCode)
                    qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
                Else
                    qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
                    qryObjICUsers.InnerJoin(qryObjICBank).On(qryObjICOfficeUsers.BankCode = qryObjICBank.BankCode)
                    qryObjICUsers.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
                End If
            End If

            qryObjICUsers.OrderBy(qryObjICUsers.UserName.Ascending)
            qryObjICUsers.Load()

            Return qryObjICUsers.LoadDataTable

        End Function
        ''Before Farhan modified 25052015
        'Public Shared Function GetAllUsersByUserTypeAndReportID(ByVal UserType As String, ByVal ReportID As String, ByVal CompanyCode As String) As DataTable
        '    Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")
        '    Dim qryMISReportUsers As New ICMISReportUsersQuery("rpu")
        '    'Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")
        '    Dim qryObjICOfficeUsers As New ICOfficeQuery("qryObjICOfficeUsers")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")

        '    qryObjICUsers.Select(qryObjICUsers.UserID, qryObjICUsers.UserName)
        '    qryObjICUsers.Where(qryObjICUsers.UserType = UserType.ToString())

        '    If ReportID <> "0" Then
        '        If CompanyCode <> "0" Then
        '            qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
        '            qryObjICUsers.InnerJoin(qryObjICCompany).On(qryObjICOfficeUsers.CompanyCode = qryObjICCompany.CompanyCode)
        '            qryObjICUsers.Where(qryObjICCompany.CompanyCode = CompanyCode)
        '            qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
        '            qryObjICUsers.Where(qryObjICUsers.UserID.NotIn(qryMISReportUsers.[Select](qryMISReportUsers.UserID).Where(qryMISReportUsers.ReportID = ReportID)))

        '        Else
        '            qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
        '            qryObjICUsers.InnerJoin(qryObjICBank).On(qryObjICOfficeUsers.BankCode = qryObjICBank.BankCode)
        '            qryObjICUsers.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
        '            qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
        '            qryObjICUsers.Where(qryObjICUsers.UserID.NotIn(qryMISReportUsers.[Select](qryMISReportUsers.UserID).Where(qryMISReportUsers.ReportID = ReportID)))

        '        End If
        '        'qryMISReportUsers.Where(qryMISReportUsers.ReportID = ReportID)
        '    Else
        '        If CompanyCode <> "0" Then
        '            qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
        '            qryObjICUsers.InnerJoin(qryObjICCompany).On(qryObjICOfficeUsers.CompanyCode = qryObjICCompany.CompanyCode)
        '            qryObjICUsers.Where(qryObjICCompany.CompanyCode = CompanyCode)
        '            qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
        '        Else
        '            qryObjICUsers.InnerJoin(qryObjICOfficeUsers).On(qryObjICUsers.OfficeCode = qryObjICOfficeUsers.OfficeID)
        '            qryObjICUsers.InnerJoin(qryObjICBank).On(qryObjICOfficeUsers.BankCode = qryObjICBank.BankCode)
        '            qryObjICUsers.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
        '            qryObjICUsers.Where(qryObjICUsers.IsActive = True And qryObjICUsers.IsApproved = True)
        '        End If
        '    End If
        '    qryObjICUsers.OrderBy(qryObjICUsers.UserName.Ascending)
        '    qryObjICUsers.Load()

        '    Return qryObjICUsers.LoadDataTable

        'End Function
        

        Public Shared Function GetAllUsersByBankBranchUsers(ByVal UserType As String) As DataTable
            Dim qryUsers As New ICUserQuery("qryusr")
            Dim qryoffice As New ICOfficeQuery("qryoff")
            Dim qryEmail As New ICEmailSettingsQuery("eml")

            qryUsers.Select(qryUsers.UserID, qryUsers.UserName, qryUsers.OfficeCode)
            qryUsers.Where(qryUsers.UserType = UserType)
            qryUsers.Where(qryUsers.UserID.NotIn(qryEmail.[Select](qryEmail.UserID)))
            qryUsers.InnerJoin(qryoffice).On(qryUsers.OfficeCode = qryoffice.OfficeID)

            qryUsers.OrderBy(qryUsers.UserName.Ascending)
            qryUsers.Load()

            Return qryUsers.LoadDataTable()
        End Function
        Public Shared Function GetAllUsersByUserType(ByVal UserType As String, ByVal UserID As String) As ICUserCollection
            Dim collUsers As New ICUserCollection
            Dim qryEmail As New ICEmailSettingsQuery("email")
            collUsers.es.Connection.CommandTimeout = 3600

            collUsers.Query.Select(collUsers.Query.UserID, collUsers.Query.UserName)
            collUsers.Query.Where(collUsers.Query.UserType = UserType.ToString())
            '    collUsers.Query.Where(collUsers.Query.UserID.NotIn(qryEmail.[Select](qryEmail.UserID).Where(qryEmail.UserID = UserID)))
            collUsers.Query.OrderBy(collUsers.Query.UserName.Ascending)
            collUsers.Query.Load()

            Return collUsers

        End Function
        Public Shared Sub DeleteICUser(ByVal UserID As String, ByVal UsersID As String, ByVal USersName As String, ByVal UserType As String)
            Dim objICUser As New ICUser
            Dim objICUserForAT As New ICUser
            Dim DisplayName As String = ""

            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(UserID)
            objICUserForAT.LoadByPrimaryKey(UserID)
            DisplayName = objICUser.DisplayName.ToString
            objICUser.MarkAsDeleted()
            objICUser.Save()
            If UserType.ToString = "Bank User" Then
                ICUtilities.AddAuditTrail("Bank User [ " & objICUserForAT.UserName.ToString() & " ] , of Branch [ " & objICUserForAT.UpToICOfficeByOfficeCode.OfficeName & " ] [ " & objICUserForAT.UpToICOfficeByOfficeCode.OfficeCode & " ] , Email [ " & objICUserForAT.Email & " ], Phone No.[ " & objICUserForAT.PhoneNo1 & " ], Cell No. [ " & objICUserForAT.PhoneNo2 & " ], location [ " & objICUserForAT.Location & " ], department [ " & objICUserForAT.Department & " ], 2FA required on login [ " & objICUserForAT.Is2FARequiredOnLogin & " ], 2FA for login VIA SMS [ " & objICUserForAT.IsTwoFAVIASMSAllowForLogin & " ], 2FA for login VIA Email [ " & objICUserForAT.IsTwoFAVIAEmailAllowForLogin & " ],2FA required on approval [ " & objICUserForAT.Is2FARequiredOnApproval & " ], 2FA for approval VIA SMS [ " & objICUserForAT.IsTwoFAVIASMSAllowForApproval & " ], 2FA for approval VIA Email [ " & objICUserForAT.IsTwoFAVIAEmailAllowForApproval & " ] Deleted", "Bank User", UserID.ToString(), UsersID.ToString, USersName.ToString(), "DELETE")
            ElseIf UserType.ToString = "Client User" Then
                ICUtilities.AddAuditTrail("Client User [ " & objICUserForAT.UserName.ToString() & " ] , of Client [ " & objICUserForAT.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.CompanyName & " ], for location [ " & objICUserForAT.UpToICOfficeByOfficeCode.OfficeName & " ] [ " & objICUserForAT.UpToICOfficeByOfficeCode.OfficeCode & " ] , Email [ " & objICUserForAT.Email & " ], Phone No.[ " & objICUserForAT.PhoneNo1 & " ], Cell No. [ " & objICUserForAT.PhoneNo2 & " ], location [ " & objICUserForAT.Location & " ], department [ " & objICUserForAT.Department & " ], 2FA required on login [ " & objICUserForAT.Is2FARequiredOnLogin & " ], 2FA for login VIA SMS [ " & objICUserForAT.IsTwoFAVIASMSAllowForLogin & " ], 2FA for login VIA Email [ " & objICUserForAT.IsTwoFAVIAEmailAllowForLogin & " ],2FA required on approval [ " & objICUserForAT.Is2FARequiredOnApproval & " ], 2FA for approval VIA SMS [ " & objICUserForAT.IsTwoFAVIASMSAllowForApproval & " ], 2FA for approval VIA Email [ " & objICUserForAT.IsTwoFAVIAEmailAllowForApproval & " ] Deleted", "Client User", UserID.ToString(), UsersID.ToString, USersName.ToString(), "DELETE")
            End If


        End Sub
        Public Shared Sub ApproveUser(ByVal UserID As Integer, ByVal ApproveBy As String, ByVal IsApproved As Boolean, ByVal UsersName As String, ByVal UserType As String)
            Dim objICUser As New ICUser
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(UserID)
            objICUser.ApprovedBy = ApproveBy
            objICUser.ApprovedOn = Now
            objICUser.IsApproved = IsApproved
            objICUser.Save()
            If UserType.ToString = "Bank User" Then
                If IsApproved = True Then
                    ICUtilities.AddAuditTrail("Bank User : " & objICUser.UserName.ToString() & " Approve", "Bank User", objICUser.UserID.ToString(), objICUser.ApprovedBy.ToString, UsersName.ToString(), "APPROVE")
                ElseIf IsApproved = False Then
                    ICUtilities.AddAuditTrail("Bank User : " & objICUser.UserName.ToString() & " Not Approve", "Bank User", objICUser.UserID.ToString(), objICUser.ApprovedBy.ToString, UsersName.ToString(), "NOT APPROVE")
                End If
            ElseIf UserType.ToString = "Client User" Then
                If IsApproved = True Then
                    ICUtilities.AddAuditTrail("Client User : " & objICUser.UserName.ToString() & " Approve", "Client User", objICUser.UserID.ToString(), objICUser.ApprovedBy.ToString, UsersName.ToString(), "APPROVE")
                ElseIf IsApproved = False Then
                    ICUtilities.AddAuditTrail("Client User : " & objICUser.UserName.ToString() & " Not Approve", "Client User", objICUser.UserID.ToString(), objICUser.ApprovedBy.ToString, UsersName.ToString(), "NOT APPROVE")
                End If
            End If


        End Sub



        Public Shared Function GetAllUsersByUserGroupCode(ByVal GroupCode As String, ByVal UserType As String, ByVal SettingName As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String) As DataTable

            Dim qryUsers As New ICUserQuery("qryusr")
            Dim qryoffice As New ICOfficeQuery("qryoff")
            Dim qryemail As New ICEmailSettingsQuery("eml")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            qryUsers.Select(qryUsers.UserID, (qryUsers.UserName + "-" + qryUsers.DisplayName).As("UserName"), qryUsers.OfficeCode, qryoffice.OfficeID)
            qryUsers.InnerJoin(qryoffice).On(qryUsers.OfficeCode = qryoffice.OfficeID)
            qryUsers.InnerJoin(qryObjICCompany).On(qryoffice.CompanyCode = qryObjICCompany.CompanyCode)
            qryUsers.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryUsers.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            qryUsers.Where(qryUsers.UserType = UserType)
            qryUsers.Where(qryUsers.IsActive = True And qryUsers.IsApproved = True)

            If SettingName = "EmailSettings" Then
                If AccntNumber.ToString() <> "0" And BranchCode.ToString() <> "0" And Currency.ToString() <> "0" And PayNatureCode.ToString <> "0" And TemplateID.ToString <> "0" Then
                    qryUsers.Where(qryUsers.UserID.NotIn(qryemail.[Select](qryemail.UserID).Where(qryemail.AccountNumber = AccntNumber And qryemail.BranchCode = BranchCode And qryemail.Currency = Currency And qryemail.PaymentNatureCode = PayNatureCode)))
                End If
            End If

            qryUsers.OrderBy(qryUsers.UserName.Ascending)
            qryUsers.Load()
            Return qryUsers.LoadDataTable()

        End Function

        ''Before Farhan Modified 25052015

        'Public Shared Function GetAllUsersByUserGroupCode(ByVal GroupCode As String, ByVal UserType As String, ByVal SettingName As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String) As DataTable

        '    Dim qryUsers As New ICUserQuery("qryusr")
        '    Dim qryoffice As New ICOfficeQuery("qryoff")
        '    Dim qryemail As New ICEmailSettingsQuery("eml")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    qryUsers.Select(qryUsers.UserID, qryUsers.UserName, qryUsers.OfficeCode, qryoffice.OfficeID)
        '    qryUsers.InnerJoin(qryoffice).On(qryUsers.OfficeCode = qryoffice.OfficeID)
        '    qryUsers.InnerJoin(qryObjICCompany).On(qryoffice.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryUsers.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryUsers.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
        '    qryUsers.Where(qryUsers.UserType = UserType)
        '    qryUsers.Where(qryUsers.IsActive = True And qryUsers.IsApproved = True)

        '    If SettingName = "EmailSettings" Then
        '        If AccntNumber.ToString() <> "0" And BranchCode.ToString() <> "0" And Currency.ToString() <> "0" And PayNatureCode.ToString <> "0" And TemplateID.ToString <> "0" Then
        '            qryUsers.Where(qryUsers.UserID.NotIn(qryemail.[Select](qryemail.UserID).Where(qryemail.AccountNumber = AccntNumber And qryemail.BranchCode = BranchCode And qryemail.Currency = Currency And qryemail.PaymentNatureCode = PayNatureCode)))
        '        End If
        '    End If

        '    qryUsers.OrderBy(qryUsers.UserName.Ascending)
        '    qryUsers.Load()
        '    Return qryUsers.LoadDataTable()

        'End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBank() As DataTable




            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")

            Dim dt As New DataTable



            qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
            qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICUser.Where(qryObjICBank.IsPrincipal = True)
            dt = qryObjICUser.LoadDataTable()
            Return dt





        End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankWithUserAndDisplayName() As DataTable




            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")

            Dim dt As New DataTable



            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"))
            qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICUser.Where(qryObjICBank.IsPrincipal = True)
            dt = qryObjICUser.LoadDataTable()
            Return dt





        End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankWithUserAndDisplayNameForUserLimit(ByVal IsUpdate As Boolean) As DataTable




            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICApplimit As New ICLimitForApprovalQuery("qryObjICApplimit")

            Dim dt As New DataTable



            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"))
            qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            If IsUpdate = False Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICApplimit.[Select](qryObjICApplimit.UserID)))

            End If
            qryObjICUser.Where(qryObjICBank.IsPrincipal = True)
            dt = qryObjICUser.LoadDataTable()
            Return dt





        End Function


        Public Shared Function GetAllUsersByBankBranchUsersforFTPSettings(ByVal UserType As String) As DataTable
            Dim qryUsers As New ICUserQuery("qryusr")
            Dim qryoffice As New ICOfficeQuery("qryoff")
            Dim qryFTP As New ICFTPSettingsQuery("ftp")
            Dim dt As DataTable

            qryUsers.Select(qryUsers.UserID, (qryUsers.UserName + "-" + qryUsers.DisplayName).As("UserName"), qryUsers.OfficeCode)
            qryUsers.Where(qryUsers.UserType = UserType)
            qryUsers.InnerJoin(qryoffice).On(qryUsers.OfficeCode = qryoffice.OfficeID)

            qryUsers.OrderBy(qryUsers.UserName.Ascending)
            'qryUsers.Load() 

            dt = qryUsers.LoadDataTable()

            ' Return qryUsers.LoadDataTable() 

            Return dt
        End Function


        ''Farhan modified 25052015
        'Public Shared Function GetAllUsersByBankBranchUsersforFTPSettings(ByVal UserType As String) As DataTable
        '    Dim qryUsers As New ICUserQuery("qryusr")
        '    Dim qryoffice As New ICOfficeQuery("qryoff")
        '    Dim qryFTP As New ICFTPSettingsQuery("ftp")
        '    Dim dt As DataTable

        '    qryUsers.Select(qryUsers.UserID, qryUsers.UserName, qryUsers.OfficeCode)
        '    qryUsers.Where(qryUsers.UserType = UserType)
        '    qryUsers.InnerJoin(qryoffice).On(qryUsers.OfficeCode = qryoffice.OfficeID)

        '    qryUsers.OrderBy(qryUsers.UserName.Ascending)
        '    'qryUsers.Load() 

        '    dt = qryUsers.LoadDataTable()

        '    ' Return qryUsers.LoadDataTable() 

        '    Return dt
        'End Function
        Public Shared Function GetAllActiveAndApproveUsersByGroupCode(ByVal GroupCode As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.DisplayName)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

            qryObjICUser.Where(qryObjICGroup.GroupCode = GroupCode.ToString)

            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function
        Public Shared Function GetAllActiveAndApproveUsersByGroupCodeForUserLimit(ByVal GroupCode As String, ByVal IsUpdate As Boolean) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICAppLimit As New ICLimitForApprovalQuery("qryObjICAppLimit")
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.DisplayName)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

            qryObjICUser.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            If IsUpdate = False Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICAppLimit.[Select](qryObjICAppLimit.UserID)))
            End If
            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function
        'Before Farhan Modefied 25052015
        'Public Shared Function GetAllActiveAndApproveUsersByGroupCode(ByVal GroupCode As String) As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryUser")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim dt As New DataTable

        '    qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
        '    qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
        '    qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

        '    qryObjICUser.Where(qryObjICGroup.GroupCode = GroupCode.ToString)

        '    dt = qryObjICUser.LoadDataTable()
        '    Return dt

        'End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBank(ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryemail As New ICEmailSettingsQuery("eml")
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICUser.Where(qryObjICBank.IsPrincipal = True And qryObjICUser.UserType = "Bank User")
            qryObjICUser.Where(qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            If AccntNumber.ToString() <> "0" And BranchCode.ToString() <> "0" And Currency.ToString() <> "0" And PayNatureCode.ToString <> "0" And TemplateID.ToString <> "0" Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryemail.[Select](qryemail.UserID).Where(qryemail.AccountNumber = AccntNumber And qryemail.BranchCode = BranchCode And qryemail.Currency = Currency And qryemail.PaymentNatureCode = PayNatureCode)))
            End If

            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function
        'Before Farhan Modefied 25052015
        'Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBank(ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String) As DataTable
        '    Dim qryObjICUser As New ICUserQuery("qryUser")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim qryemail As New ICEmailSettingsQuery("eml")
        '    Dim dt As New DataTable

        '    qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
        '    qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
        '    qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
        '    qryObjICUser.Where(qryObjICBank.IsPrincipal = True And qryObjICUser.UserType = "Bank User")
        '    qryObjICUser.Where(qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
        '    If AccntNumber.ToString() <> "0" And BranchCode.ToString() <> "0" And Currency.ToString() <> "0" And PayNatureCode.ToString <> "0" And TemplateID.ToString <> "0" Then
        '        qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryemail.[Select](qryemail.UserID).Where(qryemail.AccountNumber = AccntNumber And qryemail.BranchCode = BranchCode And qryemail.Currency = Currency And qryemail.PaymentNatureCode = PayNatureCode)))
        '    End If

        '    dt = qryObjICUser.LoadDataTable()
        '    Return dt

        'End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankforFTPSettings() As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")

            Dim dt As New DataTable


            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.DisplayName, qryObjICBank.IsPrincipal)



            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICUser.Where(qryObjICBank.IsPrincipal = True)
            qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function

        'Before Farhan Modefied 25052015
        'Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankforFTPSettings() As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryUser")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")

        '    Dim dt As New DataTable


        '    qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICBank.IsPrincipal)



        '    qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
        '    qryObjICUser.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
        '    qryObjICUser.Where(qryObjICBank.IsPrincipal = True)
        '    qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
        '    dt = qryObjICUser.LoadDataTable()
        '    Return dt

        'End Function
        Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankBranchforClearingLmts(ByVal OfficeCode As String, ByVal ClearingLmt As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryClearingLmt As New ICLimitsForClearingQuery("qryClearingLmt")
            Dim dt As New DataTable()

            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"))
            qryObjICUser.Where(qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.OfficeCode = OfficeCode)
            If ClearingLmt = 0 Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryClearingLmt.[Select](qryClearingLmt.UserID)))
            End If
            dt = qryObjICUser.LoadDataTable()

            Return dt
        End Function
        ''Farhan 25052015
        'Public Shared Function GetAllActiveAndApproveUsersOfPrincipalBankBranchforClearingLmts(ByVal OfficeCode As String, ByVal ClearingLmt As String) As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryUser")
        '    Dim qryClearingLmt As New ICLimitsForClearingQuery("qryClearingLmt")
        '    Dim dt As New DataTable()

        '    qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName)
        '    qryObjICUser.Where(qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
        '    qryObjICUser.Where(qryObjICUser.UserType = "Bank User" And qryObjICUser.OfficeCode = OfficeCode)
        '    If ClearingLmt = 0 Then
        '        qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryClearingLmt.[Select](qryClearingLmt.UserID)))
        '    End If
        '    dt = qryObjICUser.LoadDataTable()

        '    Return dt
        'End Function

        'Created function for getting client users tagged with company for checklist filling in add page uses in Account Management Module by JAWWAD 11-03-2014
        Public Shared Function GetAllActiveAndApproveUsersByCompanyCodeForTaggingAccountBalance(ByVal CompanyCode As String, ByVal IsUpdate As Boolean, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICTaggedUser As New ICTaggedClientUserForAccountQuery("qryObjICTaggedUser")
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.DisplayName)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            'qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

            qryObjICUser.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            If IsUpdate = True Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICTaggedUser.[Select](qryObjICTaggedUser.UserID).Where(qryObjICTaggedUser.AccountNumber = AccountNo And qryObjICTaggedUser.BranchCode = BranchCode And qryObjICTaggedUser.Currency = Currency)))
            End If
            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function
        ''Modified 25052015
        'Public Shared Function GetAllActiveAndApproveUsersByCompanyCodeForTaggingAccountBalance(ByVal CompanyCode As String, ByVal IsUpdate As Boolean, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryUser")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim qryObjICTaggedUser As New ICTaggedClientUserForAccountQuery("qryObjICTaggedUser")
        '    Dim dt As New DataTable

        '    qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
        '    qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
        '    qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
        '    'qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

        '    qryObjICUser.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    If IsUpdate = True Then
        '        qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICTaggedUser.[Select](qryObjICTaggedUser.UserID).Where(qryObjICTaggedUser.AccountNumber = AccountNo And qryObjICTaggedUser.BranchCode = BranchCode And qryObjICTaggedUser.Currency = Currency)))
        '    End If
        '    dt = qryObjICUser.LoadDataTable()
        '    Return dt

        'End Function


        Public Shared Function CheckDuplicateEmailAddress(ByVal EmailAddress As String, ByVal UserID As String) As String
            Dim strResult As String = ""
            Dim collUser As New ICUserCollection

            If UserID = 0 Then
                collUser.Query.Where(collUser.Query.Email.Trim = EmailAddress.Trim)
            Else
                collUser.Query.Where(collUser.Query.Email.Trim = EmailAddress.Trim And collUser.Query.UserID <> UserID)
            End If

            If collUser.Query.Load() Then
                strResult = "Email address already exist"
                Return strResult
                Exit Function
            End If

            strResult = "OK"
            Return strResult

        End Function

        Public Shared Function CheckDuplicateMobileNumber(ByVal MobileNumber As String, ByVal UserID As String) As String
            Dim strResult As String = ""
            Dim collUser As New ICUserCollection

            If UserID = 0 Then
                collUser.Query.Where(collUser.Query.CellNo.Trim = MobileNumber.ToString().Trim)
            Else
                collUser.Query.Where(collUser.Query.CellNo.Trim = MobileNumber.ToString().Trim And collUser.Query.UserID <> UserID)
            End If

            If collUser.Query.Load() Then
                strResult = "Mobile number already exist"
                Return strResult
                Exit Function
            End If

            strResult = "OK"
            Return strResult

        End Function


    End Class
End Namespace
