﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICCollectionMISReportUsersController

        Public Shared Sub AddMISReportUsers(ByVal Report As ICCollectionMISReportUsers, ByVal CurrentAt As String, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objReportUsers As New ICCollectionMISReportUsers
            Dim prevobjReportUsers As New ICCollectionMISReportUsers
            Dim objUser As New ICUser
            Dim User_Name As String = ""
            'Dim CurrentAt As String
            '  Dim PrevAt As String
            Dim IsApprovedText As String

            If (isUpdate = False) Then
                objReportUsers.CreatedBy = Report.CreatedBy
                objReportUsers.CreatedDate = Report.CreatedDate
                prevobjReportUsers.CreatedDate = Report.CreatedDate
                objReportUsers.Creater = Report.Creater
                objReportUsers.CreationDate = Report.CreationDate
            Else
                objReportUsers.LoadByPrimaryKey(Report.ReportID)
                prevobjReportUsers.LoadByPrimaryKey(Report.ReportID.ToString())
                objReportUsers.CreatedBy = Report.CreatedBy
                objReportUsers.CreatedDate = Report.CreatedDate

            End If

            objReportUsers.ReportID = Report.ReportID
            objReportUsers.UserID = Report.UserID

            objReportUsers.IsApproved = False

            If objReportUsers.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            objReportUsers.Save()



            If (isUpdate = False) Then
                '   CurrentAt = "MISReportUsers : [Code:  " & objReportUsers.ReportUserID.ToString() & " ; ReportID:  " & objReportUsers.ReportID.ToString() & " ; UserID:  " & objReportUsers.UserID.ToString() & " ; UserName:  " & User_Name.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Collection MIS Report Users", objReportUsers.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            Else
                ' CurrentAt = "MISReportUsers : Current Values  [Code:  " & objReportUsers.ReportUserID.ToString() & " ; ReportID:  " & objReportUsers.ReportID.ToString() & " ; UserID:  " & objReportUsers.UserID.ToString() & "]"
                '   PrevAt = "<br /> MISReportUsers : Previous Values  [Code:  " & prevobjReportUsers.ReportUserID.ToString() & " ; ReportID:  " & prevobjReportUsers.ReportID.ToString() & " ; UserID:  " & prevobjReportUsers.UserID.ToString() & "]"
                '  PrevAt = "<br /> Previous Report " & "[ReportID:  " & objReportUsers.ReportID.ToString() & " ; ReportName:  " & txtReportName.Text.ToString() & "] is tagged with User [" & " UserID:  " & objReportUsers.UserID.ToString() & " ; UserName:  " & lit.Text.ToString() & "]"

                ICUtilities.AddAuditTrail(CurrentAt & " Updated ", "Collection MIS Report Users", objReportUsers.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            End If

        End Sub

        Public Shared Sub ApproveReportUsers(ByVal User As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objReportUsers As New ICCollectionMISReportUsers
            Dim prevobjReportUsers As New ICCollectionMISReportUsers

            Dim CurrentAt As String = ""
            '   Dim PrevAt As String = ""
            Dim IsApprovedText As String

            objReportUsers.LoadByPrimaryKey(User)
            objReportUsers.IsApproved = IsApproved
            objReportUsers.ApprovedBy = UserID
            objReportUsers.ApprovedOn = Date.Now


            If objReportUsers.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            objReportUsers.Save()

            CurrentAt = "Collection MIS Report Users : Current Values [Code:  " & objReportUsers.ReportUserID.ToString() & " ; Report ID:  " & objReportUsers.ReportID.ToString() & " ; User ID:  " & objReportUsers.UserID.ToString() & " ; Is Approved: " & IsApprovedText.ToString() & "]"
            ' PrevAt = "<br />MISReportUsers : Previous Values [Code:  " & prevobjReportUsers.ReportUserID.ToString() & " ; ReportID:  " & prevobjReportUsers.ReportID.ToString() & " ; UserID:  " & prevobjReportUsers.UserID.ToString() & " ; IsApproved: " & prevobjReportUsers.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Collection MIS Report Users", objReportUsers.ReportUserID.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

        End Sub


        Public Shared Sub DeleteReportUsers(ByVal ReportUser As String, ByVal UserID As String, ByVal UserName As String)
            Dim objReportUsers As New ICCollectionMISReportUsers
            Dim PrevAt As String = ""

            objReportUsers.LoadByPrimaryKey(ReportUser)

            PrevAt = "Collection MIS Report Users [Code:  " & objReportUsers.ReportUserID.ToString() & " ; Report ID:  " & objReportUsers.ReportID.ToString() & " ; User ID:  " & objReportUsers.UserID.ToString() & " ; Is Approved: " & objReportUsers.IsApproved.ToString() & "]"
            objReportUsers.MarkAsDeleted()
            objReportUsers.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Collection MIS Report Users", ReportUser, UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub


        Public Shared Sub GetgvAssignedUsersToReport(ByVal ReportID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryICUser As New ICUserQuery("usr")
            Dim qryReportUser As New ICCollectionMISReportUsersQuery("report")
            Dim qryOffice As New ICOfficeQuery("office")

            If Not pagenumber = 0 Then
                qryReportUser.Select(qryReportUser.ReportUserID, qryICUser.UserID, qryICUser.UserName, qryICUser.DisplayName, qryICUser.Email, qryICUser.UserType, qryReportUser.IsApproved)
                qryReportUser.Select(qryOffice.Description)
                qryReportUser.InnerJoin(qryICUser).On(qryReportUser.UserID = qryICUser.UserID)
                qryReportUser.InnerJoin(qryOffice).On(qryICUser.OfficeCode = qryOffice.OfficeID)
                qryReportUser.Where(qryReportUser.ReportID = ReportID.ToString())

                rg.DataSource = qryReportUser.LoadDataTable()

                If DoDataBind Then
                    rg.DataBind()
                End If


            End If

        End Sub

        Public Shared Sub UpdateTaggesReportUsers(ByVal MISReportID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal ALCurrentUsers As ArrayList)
            Dim objICMisReportUsersColl As New ICCollectionMISReportUsersCollection
            Dim CurrentAt As String = Nothing
            If ALCurrentUsers.Count > 0 Then
                objICMisReportUsersColl.Query.Where(objICMisReportUsersColl.Query.UserID.NotIn(ALCurrentUsers) And objICMisReportUsersColl.Query.ReportID = MISReportID)
                objICMisReportUsersColl.Query.Load()
            Else
                objICMisReportUsersColl.Query.Where(objICMisReportUsersColl.Query.ReportID = MISReportID)
                objICMisReportUsersColl.Query.Load()
            End If

            For Each objICMisReportUSer As ICCollectionMISReportUsers In objICMisReportUsersColl
                objICMisReportUSer.CreatedBy = UsersID
                objICMisReportUSer.CreatedDate = Date.Now
                objICMisReportUSer.IsApproved = False
                CurrentAt = Nothing
                Dim collMISReport As New ICCollectionMISReport
                collMISReport.LoadByPrimaryKey(MISReportID)
                CurrentAt = "Collection MIS Report Users" & "[Report ID:  " & objICMisReportUSer.ReportID.ToString() & " ; Report Name:  " & collMISReport.ReportName & "] is tagged with User [" & " User ID:  " & objICMisReportUSer.UserID & " ; User Name:  " & objICMisReportUSer.UpToICUserByUserID.UserName & "] on Update."
                ICUtilities.AddAuditTrail(CurrentAt, "Collection MIS Report Users", objICMisReportUSer.ReportUserID.ToString, UsersID, UsersName, "UPDATE")
                objICMisReportUSer.Save()
            Next
        End Sub

    End Class
End Namespace
