﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionNatureController

        Public Shared Sub AddCollectionNature(ByVal CollectionNature As IC.ICCollectionNature, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objCollectionNature As New ICCollectionNature
            Dim prevICCollectionNature As New ICCollectionNature
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""
            Dim IsActiveText As String

            objCollectionNature.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then

                objCollectionNature.CreatedBy = CollectionNature.CreatedBy
                objCollectionNature.CreatedDate = CollectionNature.CreatedDate

            Else
                objCollectionNature.LoadByPrimaryKey(CollectionNature.CollectionNatureCode)
                prevICCollectionNature.LoadByPrimaryKey(CollectionNature.CollectionNatureCode)
            End If
            objCollectionNature.CollectionNatureCode = CollectionNature.CollectionNatureCode
            objCollectionNature.CollectionNatureName = CollectionNature.CollectionNatureName
            objCollectionNature.CompanyCode = CollectionNature.CompanyCode
            objCollectionNature.IsActive = CollectionNature.IsActive

            If objCollectionNature.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            objCollectionNature.Save()

            If (isUpdate = False) Then
                CurrentAt = "Collection Nature : [Collection Code:  " & objCollectionNature.CollectionNatureCode.ToString() & " ; Collection Name:  " & objCollectionNature.CollectionNatureName.ToString() & " ; Collection Company: " & objCollectionNature.CompanyCode.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Collection Nature", objCollectionNature.CollectionNatureCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "Collection Nature : Current Values [Collection Code:  " & objCollectionNature.CollectionNatureCode.ToString() & " ; Collection Name:  " & objCollectionNature.CollectionNatureName.ToString() & " ; Collection Company: " & objCollectionNature.CompanyCode.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                PrevAt = "<br />Collection Nature : Previous Values [Collection Code:  " & prevICCollectionNature.CollectionNatureCode.ToString() & " ; Collection Name:  " & prevICCollectionNature.CollectionNatureName.ToString() & " ; Collection Company: " & objCollectionNature.CompanyCode.ToString() & " ; IsActive:  " & prevICCollectionNature.IsActive.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Collection Nature", objCollectionNature.CollectionNatureCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If
        End Sub

        Public Shared Sub DeleteCollectionNature(ByVal CollectionNatureCode As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICCollectionNature As New ICCollectionNature
            Dim prevICCollectionNature As New ICCollectionNature
            Dim PrevAt As String = ""
            objICCollectionNature.es.Connection.CommandTimeout = 3600
            prevICCollectionNature.es.Connection.CommandTimeout = 3600

            objICCollectionNature.LoadByPrimaryKey(CollectionNatureCode)
            prevICCollectionNature.LoadByPrimaryKey(CollectionNatureCode)

            PrevAt = "CollectionNature : [Code:  " & objICCollectionNature.CollectionNatureCode.ToString() & " ; Name:  " & objICCollectionNature.CollectionNatureName.ToString() & " ; Collection Company: " & objICCollectionNature.CompanyCode.ToString() & " ; IsActive:  " & objICCollectionNature.IsActive.ToString() & "] "

            objICCollectionNature.MarkAsDeleted()
            objICCollectionNature.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Collection Nature", prevICCollectionNature.CollectionNatureCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub GetCollectionNaturesgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            If Not pagenumber = 0 Then
                Dim collICCollectionNatures As New ICCollectionNatureCollection
                collICCollectionNatures.LoadAll()
                collICCollectionNatures.Query.OrderBy(collICCollectionNatures.Query.CollectionNatureName.Ascending)
                rg.DataSource = collICCollectionNatures
                If DoDataBind Then
                    rg.DataBind()
                End If

            Else
                Dim collICCollectionNatures As New ICCollectionNatureCollection
                collICCollectionNatures.LoadAll()
                collICCollectionNatures.Query.es.PageNumber = 1
                collICCollectionNatures.Query.es.PageSize = pagesize
                collICCollectionNatures.Query.OrderBy(collICCollectionNatures.Query.CollectionNatureName.Ascending)
                rg.DataSource = collICCollectionNatures
                If DoDataBind Then
                    rg.DataBind()
                End If
            End If
        End Sub


        Public Shared Function GetAllActiveCollectionNatures(ByVal CompanyCode As String) As ICCollectionNatureCollection
            Dim ICCollectionNature As New ICCollectionNatureCollection
            ICCollectionNature.es.Connection.CommandTimeout = 3600

            ICCollectionNature.Query.Where(ICCollectionNature.Query.CompanyCode = CompanyCode.ToString(), ICCollectionNature.Query.IsActive = True)
            ICCollectionNature.Query.OrderBy(ICCollectionNature.Query.CollectionNatureName.Ascending)
            ICCollectionNature.Query.Load()

            Return ICCollectionNature
        End Function

    End Class
End Namespace
