﻿Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Common.Utilities
Imports Telerik.Web.UI
Namespace IC

    Public Class ICRoleController
        Public Shared Sub AddICRole(ByVal ICRole As ICRole, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objICRole As New ICRole
            Dim objICRoleFrom As New ICRole
            Dim RoleCurrentProperties As String
            Dim RolePreviousProperties As String
            Dim IsActiveProperty As String = ""
            Dim IsApprovedProperty As String = ""
            'Dim Result As Boolean = False
            objICRole.es.Connection.CommandTimeout = 3600
            objICRoleFrom.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then
                objICRole.CreateBy = ICRole.CreateBy
                objICRole.CreateDate = ICRole.CreateDate
                objICRole.Creater = ICRole.Creater
                objICRole.CreationDate = ICRole.CreationDate
                objICRole.IsApproved = False
            Else
                objICRole.LoadByPrimaryKey(ICRole.RoleID)
                objICRoleFrom.LoadByPrimaryKey(ICRole.RoleID)
                objICRole.CreateBy = ICRole.CreateBy
                objICRole.CreateDate = ICRole.CreateDate
            End If
            objICRole.RoleID = ICRole.RoleID
            objICRole.RoleName = ICRole.RoleName.ToString()
            objICRole.RoleType = ICRole.RoleType
            objICRole.Description = ICRole.Description
            objICRole.IsApproved = True
            objICRole.IsActive = ICRole.IsActive
            objICRole.Save()

            If isUpdate = True Then

                RoleCurrentProperties = "Role Current values at: ID: " & objICRole.RoleID & ", Name: " & objICRole.RoleName.ToString & ", Description: " & objICRole.Description.ToString & ", IsActive: " & objICRole.IsActive & " ."
                RolePreviousProperties = " Role Previous values at: ID: " & objICRoleFrom.RoleID & ", Name: " & objICRoleFrom.RoleName.ToString & ", Description: " & objICRoleFrom.Description.ToString & ", IsActive: " & objICRoleFrom.IsActive & " ."
            End If



            If isUpdate = False Then
                ICUtilities.AddAuditTrail("Role : ID " & objICRole.RoleID & " ,Name : " & objICRole.RoleName.ToString & " , Description: " & objICRole.Description.ToString & ", IsActive: " & objICRole.IsActive & " Added.", "Role", objICRole.RoleID, UserID.ToString, UserName.ToString(), "ADD")
            ElseIf isUpdate = True Then
                ICUtilities.AddAuditTrail(RoleCurrentProperties & RolePreviousProperties & " Updated ", "Role", objICRole.RoleID.ToString, UserID.ToString(), UserName.ToString(), "UPDATE")
            End If



        End Sub

        Public Shared Sub DeleteICRole(ByVal RoleID As Integer, ByVal PortalID As Integer)

            Dim CtrlRole As New RoleController
            CtrlRole.DeleteRole(RoleID, PortalID)

            DataCache.ClearPortalCache(0, True)


        End Sub
        Public Shared Sub DeleteRoleFromIC(ByVal RoleID As String, ByVal UserID As String, ByVal UserName As String)

            Dim objICRole As New ICRole
            Dim RoleName As String = ""
            Dim RoleType As String = ""

            objICRole.es.Connection.CommandTimeout = 3600

            If objICRole.LoadByPrimaryKey(RoleID) Then
                RoleName = objICRole.RoleName.ToString
                RoleType = objICRole.RoleType.ToString

                objICRole.MarkAsDeleted()
                objICRole.Save()
                ICUtilities.AddAuditTrail("Role with ID : " & RoleID.ToString & " ,Name: " & RoleName.ToString & " and Role Type : " & RoleType.ToString & " deleted.", "Role", RoleID.ToString, UserID.ToString, UserName.ToString, "DELETE")
            End If

           

        End Sub
        Public Shared Sub ApproveICRole(ByVal RoleID As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objICRole As New ICRole
            objICRole.es.Connection.CommandTimeout = 3600

            objICRole.LoadByPrimaryKey(RoleID)
            objICRole.IsApproved = IsApproved
            objICRole.ApprovedBy = UserID
            objICRole.ApprovedDate = Date.Now
            objICRole.Save()

            If IsApproved = True Then
                ICUtilities.AddAuditTrail("Role : " & objICRole.RoleName.ToString() & " Approved", "Role", objICRole.RoleID.ToString(), UserID.ToString, UserName.ToString(), "APPROVE")

            Else
                ICUtilities.AddAuditTrail("Role: " & objICRole.RoleName.ToString() & " Not Approved", "Role", objICRole.RoleID.ToString(), UserID.ToString(), UserName.ToString(), "UNAPPROVE")

            End If

        End Sub
        Public Shared Function GetRoleByID(ByVal RoleID As Integer, ByVal PortalID As Integer) As RoleInfo

            Dim InfoRole As New RoleInfo
            Dim cRole As New RoleController
            InfoRole = cRole.GetRole(RoleID, PortalID)
            Return InfoRole


        End Function
        Public Shared Function GetAllICRolesByRoleType(ByVal RoleType As String) As ICRoleCollection
            Dim objICRoleColl As New ICRoleCollection
            objICRoleColl.es.Connection.CommandTimeout = 3600

            If RoleType.ToString() <> "" Then
                objICRoleColl.Query.Where(objICRoleColl.Query.RoleType = RoleType.ToString())

            End If

            objICRoleColl.Query.OrderBy(objICRoleColl.Query.RoleName.Ascending)
            objICRoleColl.Query.Load()
            Return objICRoleColl


        End Function
        '' For Rad Grid Changed Function
        Public Shared Sub GetAllICRolesForRadGrid(ByVal RoleType As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim objICRoleColl As New ICRoleCollection
            objICRoleColl.es.Connection.CommandTimeout = 3600
            objICRoleColl.Query.Select(objICRoleColl.Query.RoleID, objICRoleColl.Query.RoleName, objICRoleColl.Query.RoleType, objICRoleColl.Query.IsActive)
            objICRoleColl.Query.Where(objICRoleColl.Query.RoleType = RoleType.ToString)
            objICRoleColl.Query.OrderBy(objICRoleColl.Query.RoleName.Descending, objICRoleColl.Query.RoleType.Descending)
            objICRoleColl.Query.Load()
            If Not pagenumber = 0 Then
                objICRoleColl.Query.es.PageNumber = pagenumber

                objICRoleColl.Query.es.PageSize = pagesize


                rg.DataSource = objICRoleColl


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else


                objICRoleColl.Query.es.PageNumber = 1
                objICRoleColl.Query.es.PageSize = pagesize


                rg.DataSource = objICRoleColl

                If DoDataBind Then


                    rg.DataBind()

                End If




            End If





        End Sub

        Public Shared Function GetAllICRolesByRoleTypeActiveAndApprove(ByVal RoleType As String) As ICRoleCollection
            Dim objICRoleColl As New ICRoleCollection
            objICRoleColl.es.Connection.CommandTimeout = 3600
            objICRoleColl.Query.Select(objICRoleColl.Query.RoleID, objICRoleColl.Query.RoleName)

            If Not RoleType.ToString = "" Then
                objICRoleColl.Query.Where(objICRoleColl.Query.RoleType = RoleType And objICRoleColl.Query.IsActive = True)
            Else
                objICRoleColl.Query.Where(objICRoleColl.Query.IsActive = True)
            End If
            objICRoleColl.Query.OrderBy(objICRoleColl.Query.RoleName.Ascending)
            objICRoleColl.Query.Load()
            Return objICRoleColl


        End Function

    End Class
End Namespace

