﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICBankController

        Public Shared Sub AddBank(ByVal cBank As IC.ICBank, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICBank As New IC.ICBank
            Dim prevICBank As New IC.ICBank
            Dim IsActiveText As String
            Dim IsIBFTText As String
            Dim IsDDEnabledText As String
            Dim IsPrincipalText As String
            Dim IsApprovedText As String
            Dim IsDDPreferredText As String
            Dim CurrentAt As String
            Dim PrevAt As String

            ICBankController.ISDDPreferedBankPresent()
            ICBank.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICBank.CreatedBy = cBank.CreatedBy
                ICBank.CreateDate = cBank.CreateDate
                ICBank.Creater = cBank.Creater
                ICBank.CreationDate = cBank.CreationDate
            Else
                ICBank.LoadByPrimaryKey(cBank.BankCode)
                prevICBank.LoadByPrimaryKey(cBank.BankCode.ToString())
                ICBank.CreatedBy = cBank.CreatedBy
                ICBank.CreateDate = cBank.CreateDate
            End If
            ICBank.BankCode = cBank.BankCode
            ICBank.BankName = cBank.BankName
            ICBank.Decription = cBank.Decription
            ICBank.WebSite = cBank.WebSite
            ICBank.BankID = cBank.BankID.ToString()
            ICBank.Phone = cBank.Phone
            ICBank.BankIMD = cBank.BankIMD
            ICBank.IsIBFT = cBank.IsIBFT
            ICBank.IsRTGS = cBank.IsRTGS
            ICBank.IsPrincipal = cBank.IsPrincipal
            ICBank.IsActive = cBank.IsActive
            ICBank.IsDDEnabled = cBank.IsDDEnabled
            ICBank.IsDDPreferred = cBank.IsDDPreferred


            ICBank.IsApproved = False

            If ICBank.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If ICBank.IsIBFT = True Then
                IsIBFTText = True
            Else
                IsIBFTText = False
            End If

            If ICBank.IsDDEnabled = True Then
                IsDDEnabledText = True
            Else
                IsDDEnabledText = False
            End If

            If ICBank.IsPrincipal = True Then
                IsPrincipalText = "Yes"
            Else
                IsPrincipalText = "No"
            End If

            If ICBank.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            If ICBank.IsDDPreferred = True Then
                IsDDPreferredText = True
            Else
                IsDDPreferredText = False
            End If

            ICBank.Save()

            If (isUpdate = False) Then

                If ICBank.IsPrincipal = True Then
                    CurrentAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                Else
                    CurrentAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsDDEnabled:  " & ICBank.IsDDEnabled.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & " ; IsDDPreferred:  " & ICBank.IsDDPreferred.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                End If

            Else
                If ICBank.IsPrincipal = True Then
                    CurrentAt = "Bank : Current Values [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & IsIBFTText.ToString() & " ; IsPrincipal:  " & IsPrincipalText.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                    PrevAt = "<br />Bank : Previous Values [Code:  " & prevICBank.BankCode.ToString() & " ; Name:  " & prevICBank.BankName.ToString() & " ; Description:  " & prevICBank.Decription.ToString() & " ; IMD:  " & prevICBank.BankIMD.ToString() & " ; Phone:  " & prevICBank.Phone.ToString() & " ; Website:  " & prevICBank.WebSite.ToString() & " ; IsIBFT:  " & prevICBank.IsIBFT.ToString() & " ; IsPrincipal:  " & prevICBank.IsPrincipal.ToString() & " ; IsActive:  " & prevICBank.IsActive.ToString() & " ; IsApproved:  " & prevICBank.IsApproved.ToString() & "] "
                    ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                Else
                    CurrentAt = "Bank : Current Values [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & IsIBFTText.ToString() & " ; IsDDEnabled:  " & IsDDEnabledText.ToString() & " ; IsPrincipal:  " & IsPrincipalText.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & " ; IsDDPreferred:  " & IsDDPreferredText.ToString() & "]"
                    PrevAt = "<br />Bank : Previous Values [Code:  " & prevICBank.BankCode.ToString() & " ; Name:  " & prevICBank.BankName.ToString() & " ; Description:  " & prevICBank.Decription.ToString() & " ; IMD:  " & prevICBank.BankIMD.ToString() & " ; Phone:  " & prevICBank.Phone.ToString() & " ; Website:  " & prevICBank.WebSite.ToString() & " ; IsIBFT:  " & prevICBank.IsIBFT.ToString() & " ; IsDDEnabled:  " & prevICBank.IsDDEnabled.ToString() & " ; IsPrincipal:  " & prevICBank.IsPrincipal.ToString() & " ; IsActive:  " & prevICBank.IsActive.ToString() & " ; IsApproved:  " & prevICBank.IsApproved.ToString() & " ; IsDDPreferred:  " & prevICBank.IsDDPreferred.ToString() & "] "
                    ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                End If
            End If

        End Sub
        Public Shared Function CheckIsPrincipal() As Boolean
            Dim icBank As New ICBankCollection

            icBank.es.Connection.CommandTimeout = 3600

            icBank.Query.Where(icBank.Query.IsPrincipal = True And icBank.Query.IsActive = True And icBank.Query.IsApproved = True)

            If icBank.Query.Load() = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Sub deleteBank(ByVal BankCode As String, ByVal UserID As String, ByVal UserName As String)
            Dim ICBank As New ICBank
            Dim PrevAt As String

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.LoadByPrimaryKey(BankCode)
            If ICBank.IsPrincipal = True Then
                PrevAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & "]"
            Else
                PrevAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsDDEnabled:  " & ICBank.IsDDEnabled.ToString() & " ; IsDDPreferred: " & ICBank.IsDDPreferred.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & "]"
            End If
            ICBank.MarkAsDeleted()
            ICBank.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub
        ' Aizaz Ahmed [Dated: 19-Feb-2013] 
        Public Shared Function GetBankByBankCode(ByVal BankCode As String) As ICBank
            Dim objICBank As New ICBank
            objICBank.Query.Where(objICBank.Query.BankCode = BankCode.ToString() And objICBank.Query.IsActive = True And objICBank.Query.IsApproved = True)

            If objICBank.Query.Load() Then
                Return objICBank
            Else
                Return Nothing
            End If
        End Function


        Public Shared Sub ApproveBank(ByVal BankCode As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim ICBank As New ICBank
            Dim CurrentAt As String

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.LoadByPrimaryKey(BankCode)
            ICBank.IsApproved = IsApproved
            ICBank.Approvedby = UserID
            ICBank.ApprovedOn = Date.Now
            ICBank.Save()

            If ICBank.IsPrincipal = True Then
                CurrentAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")
            Else
                CurrentAt = "Bank : [Code:  " & ICBank.BankCode.ToString() & " ; Name:  " & ICBank.BankName.ToString() & " ; Description:  " & ICBank.Decription.ToString() & " ; IMD:  " & ICBank.BankIMD.ToString() & " ; Phone:  " & ICBank.Phone.ToString() & " ; Website:  " & ICBank.WebSite.ToString() & " ; IsIBFT:  " & ICBank.IsIBFT.ToString() & " ; IsDDEnabled:  " & ICBank.IsDDEnabled.ToString() & " ; IsPrincipal:  " & ICBank.IsPrincipal.ToString() & " ; IsActive:  " & ICBank.IsActive.ToString() & " ; IsApproved:  " & ICBank.IsApproved.ToString() & " ; IsDDPreferred:  " & ICBank.IsDDPreferred.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Bank", ICBank.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")
            End If
        End Sub
        Public Shared Function IsBankExistByBankCode(ByVal BankCode As String) As Boolean
            Dim ICBank As New ICBank

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.Query.Where(ICBank.Query.BankCode = BankCode And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
            If ICBank.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsBankExistByBankName(ByVal BankName As String) As Boolean
            Dim FRCBank As New ICBank

            FRCBank.es.Connection.CommandTimeout = 3600
            FRCBank.Query.Where(FRCBank.Query.BankName.Trim.ToLower = BankName And FRCBank.Query.IsActive = True And FRCBank.Query.IsApproved = True)
            If FRCBank.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function GetBankByName(ByVal BankName As String) As ICBank
            Dim ICBank As New ICBank

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.Query.Where(ICBank.Query.BankName.Trim.ToLower = BankName.Trim.ToLower And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
            If ICBank.Query.Load() Then
                Return ICBank
            Else
                Return Nothing
            End If
        End Function
        'Public Shared Function GetPrincipalBankNameByBankIMD(ByVal BankIMD As String) As String
        '    Dim ICBank As New ICBank

        '    ICBank.es.Connection.CommandTimeout = 3600
        '    ICBank.Query.Where(ICBank.Query.BankIMD.Trim.ToLower = BankIMD.Trim.ToLower And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
        '    If ICBank.Query.Load() Then
        '        Return ICBank.BankName.ToString
        '    Else
        '        Return "-"
        '    End If
        'End Function
        Public Shared Function GetPrincipalBankNameByBankIMD(ByVal BankIMD As String) As String
            Dim objICBankColl As New ICBankCollection
            Dim objICBank As New ICBank
            Dim BankName As String = ""
            objICBankColl.es.Connection.CommandTimeout = 3600
            objICBankColl.Query.Where(objICBankColl.Query.BankIMD.Trim.ToLower = BankIMD.Trim.ToLower And objICBankColl.Query.IsActive = True And objICBankColl.Query.IsApproved = True)
            objICBankColl.Query.OrderBy(objICBankColl.Query.BankCode.Ascending)
            objICBankColl.Query.Load()
            If objICBankColl.Query.Load() Then
                objICBank = objICBankColl(0)
                Return objICBank.BankName.ToString
            Else
                Return "-"
            End If
        End Function
        Public Shared Function GetBank() As ICBankCollection
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            collICBank.LoadAll()
            Return collICBank
        End Function
        Public Shared Function GetNonPrincipleBank() As ICBankCollection
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsPrincipal = False And collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            collICBank.Query.Load()
            Return collICBank
        End Function
        Public Shared Function GetPrincipleBank() As ICBank
            Dim ICBank As New ICBank

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.Query.Where(ICBank.Query.IsPrincipal = True And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
            ICBank.Query.Load()
            Return ICBank
        End Function
        Public Shared Function GetBankTransferAllowed(ByVal BankCode As String) As String
            Dim ICBank As New ICBank

            ICBank.es.Connection.CommandTimeout = 3600

            Dim TrasferType As String = ""
            If ICBank.LoadByPrimaryKey(BankCode) Then

                If ICBank.IsIBFT = True And ICBank.IsRTGS = True Then
                    TrasferType = "Both"
                ElseIf ICBank.IsIBFT = True And ICBank.IsRTGS = False Then
                    TrasferType = "IBFT"
                ElseIf ICBank.IsIBFT = False And ICBank.IsRTGS = True Then
                    TrasferType = "RTGS"
                ElseIf ICBank.IsIBFT = False And ICBank.IsRTGS = False Then
                    TrasferType = "None"
                End If
            End If
            Return TrasferType
        End Function
        Public Shared Function GetAllApprovedAndActiveBanks() As DataTable
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            Return collICBank.Query.LoadDataTable()
        End Function
        Public Shared Function GetAllApprovedActiveNonPrincipalBanksAndIBFTEnabledBanks() As DataTable
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.Where(collICBank.Query.IsPrincipal = False And collICBank.Query.IsIBFT = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            Return collICBank.Query.LoadDataTable()
        End Function
        Public Shared Function GetAllApprovedActivePrincipalBanks() As DataTable
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True And collICBank.Query.IsPrincipal = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            Return collICBank.Query.LoadDataTable()
        End Function
        Public Shared Function GetAllApprovedActiveNonPrincipalBanks() As DataTable
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True And collICBank.Query.IsPrincipal = False)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            Return collICBank.Query.LoadDataTable()
        End Function


        Public Shared Function IsPrincipalBank(ByVal BankCode As String, ByVal BankName As String) As Boolean
            Dim ICBank As New ICBank
            ICBank.es.Connection.CommandTimeout = 3600
            If BankCode <> "" Then
                ICBank.Query.Where(ICBank.Query.BankCode = BankCode And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
                If ICBank.Query.Load() Then
                    If ICBank.IsPrincipal = True Then
                        Return True
                        Exit Function
                    Else
                        Return False
                        Exit Function
                    End If
                End If
            End If
           

            ICBank = New ICBank
            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.Query.Where(ICBank.Query.BankName.Trim.ToLower = BankName And ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
            If ICBank.Query.Load() Then
                If ICBank.IsPrincipal = True Then
                    Return True
                    Exit Function
                Else
                    Return False
                    Exit Function
                End If
            End If
        End Function
        Public Shared Function GetNonPrincipleRTGSBanks() As ICBankCollection
            Dim collICBank As New ICBankCollection

            collICBank.es.Connection.CommandTimeout = 3600
            collICBank.Query.Where(collICBank.Query.IsPrincipal = False And collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True And collICBank.Query.IsRTGS = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            collICBank.Query.Load()
            Return collICBank
        End Function

        Public Shared Sub GetBankgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collBank As New ICBankCollection

            If Not pagenumber = 0 Then
                collBank.Query.Select(collBank.Query.BankCode, collBank.Query.BankName, collBank.Query.IsActive, collBank.Query.IsApproved)
                collBank.Query.OrderBy(collBank.Query.BankCode.Descending, collBank.Query.BankName.Descending)
                collBank.Query.Load()
                rg.DataSource = collBank

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collBank.Query.es.PageNumber = 1
                collBank.Query.es.PageSize = pagesize
                collBank.Query.OrderBy(collBank.Query.BankName.Ascending)
                collBank.Query.Load()
                rg.DataSource = collBank


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub
        'Public Shared Function GetBankNameByBankCode(ByVal BankCode As String) As String
        '    Dim BankName As String = ""
        '    Dim objICBank As New ICBank

        '    If objICBank.LoadByPrimaryKey(BankCode.ToString()) Then
        '        BankName = objICBank.BankName.ToString()
        '    Else
        '        BankName = ""
        '    End If
        '    Return BankName.ToString()
        'End Function
        Public Shared Function GetBankNameByBankCode(ByVal BankCode As String) As String
            Dim BankName As String = ""
            Dim collICBank As New ICBankCollection

            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.Where(collICBank.Query.BankCode = BankCode)
            If collICBank.Query.Load() Then
                BankName = collICBank(0).BankName.ToString()
            Else
                BankName = ""
            End If
            Return BankName.ToString()
        End Function

        Public Shared Function ISDDPreferedBankPresent() As Boolean
            Dim collicBank As New ICBankCollection

            collicBank.Query.Where(collicBank.Query.IsDDPreferred = True)
            If collicBank.Query.Load() = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Sub SetAllDDPreferedAsFalse()
            Dim objBank As New ICBank
            Dim collicBank As New ICBankCollection
            If collicBank.LoadAll() Then
                For Each objBank In collicBank
                    objBank.IsDDPreferred = False
                Next
                collicBank.Save()
            End If
        End Sub

        Public Shared Function IsBankExistByBankCodeForBulkUpLoad() As DataTable
            Dim objICBankQuery As New ICBankQuery("objICBankQuery")
            Dim dt As New DataTable
            objICBankQuery.Where(objICBankQuery.IsActive = True And objICBankQuery.IsApproved = True)
            objICBankQuery.OrderBy(objICBankQuery.BankCode.Ascending)
            dt = objICBankQuery.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllActiveApprovedIBFTEnabledBanks() As DataTable
            Dim collICBank As New ICBankCollection
            collICBank.Query.Where(collICBank.Query.IsActive = True And collICBank.Query.IsApproved = True)
            collICBank.Query.Where(collICBank.Query.IsIBFT = True)
            collICBank.Query.OrderBy(collICBank.Query.BankName.Ascending)
            Return collICBank.Query.LoadDataTable()
        End Function


        Public Shared Function IsActiveApprovedNonPrincipalAndIBFTEnabledBankExistByBankCode(ByVal BankCode As String) As Boolean
            Dim ICBank As New ICBank
            ICBank.Query.Where(ICBank.Query.BankCode = BankCode)
            ICBank.Query.Where(ICBank.Query.IsPrincipal = False And ICBank.Query.IsIBFT = True)
            ICBank.Query.Where(ICBank.Query.IsActive = True And ICBank.Query.IsApproved = True)
            If ICBank.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace
