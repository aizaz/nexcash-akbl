﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC


    Public Class ICPdcController

        'CREATE THIS CLASS FOR IC_Pdc TABLE 
        'for adding, updating and deleting of cheques By JAWWAD 11-03-2014


        'created method for adding and updating value in db field of IC_Pdc TABLE
        Public Shared Sub AddPdc(ByVal cPdc As IC.ICPdc, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICPdc As New IC.ICPdc
            Dim prevICPdc As New IC.ICPdc


            ICPdc.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICPdc.CreateBy = cPdc.CreateBy
                ICPdc.CreateDate = cPdc.CreateDate
                ICPdc.Creater = cPdc.Creater
                ICPdc.CreationDate = cPdc.CreationDate
            Else

                ICPdc.LoadByPrimaryKey(cPdc.ChequeId)
                prevICPdc.LoadByPrimaryKey(cPdc.ChequeId)
                'prevICPdc.LoadByPrimaryKey(cPdc.ChequeId.ToString())
                ICPdc.CreateBy = cPdc.CreateBy
                ICPdc.CreateDate = cPdc.CreateDate

            End If

            ICPdc.AccountNumber = cPdc.AccountNumber
            ICPdc.BranchCode = cPdc.BranchCode
            ICPdc.Currency = cPdc.Currency
            '  ICPdc.ChequeId = cPdc.ChequeId
            ICPdc.ChequeNo = cPdc.ChequeNo
            ICPdc.BankCode = cPdc.BankCode
            ICPdc.Presentment = cPdc.Presentment
            ICPdc.PostingDate = cPdc.PostingDate
            ICPdc.CityCode = cPdc.CityCode
            ICPdc.TransactionType = cPdc.TransactionType
            ICPdc.ClearingType = cPdc.ClearingType
            ICPdc.TransactionDetails = cPdc.TransactionDetails
            ICPdc.AddtionalDetails = cPdc.AddtionalDetails


            ICPdc.Save()

            If (isUpdate = False) Then
                CurrentAt = " ID : [  " & ICPdc.ChequeId & " ]  Cheque Number: [ " & ICPdc.ChequeNo & "] Account Number: [" & ICPdc.AccountNumber & "] Branch Code: [" & ICPdc.BranchCode & "] Currency: [" & ICPdc.Currency & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Post Dated Cheque", ICPdc.ChequeId.ToString, UserID, UserName, "ADD")
            Else

                CurrentAt = "Cheque : Current Values [ID: " & ICPdc.ChequeId & "; Cheque No:  " & ICPdc.ChequeNo & "; Account Number: " & ICPdc.AccountNumber & "; Branch Code: " & ICPdc.BranchCode & "; Currency: " & ICPdc.Currency & " ]"
                PrevAt = "<br />Cheque : Previous Values [ID: " & ICPdc.ChequeId & "; Cheque No:  " & prevICPdc.ChequeNo & "; Account Number: " & prevICPdc.AccountNumber & "; Branch Code: " & prevICPdc.BranchCode & "; Currency: " & prevICPdc.Currency & " ]"
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Post Dated Cheque", ICPdc.ChequeId.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If


        End Sub


        'created method for loading gv in view page
        Public Shared Sub GetPdcgv(ByVal ChequeId As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


            Dim collPdc As New ICPdcCollection

            If Not pagenumber = 0 Then
                collPdc.Query.Select(collPdc.Query.ChequeId, collPdc.Query.ChequeNo, collPdc.Query.TransactionType, collPdc.Query.ClearingType)
                collPdc.Query.Select(collPdc.Query.PostingDate)
                collPdc.Query.OrderBy(collPdc.Query.ChequeNo.Ascending)
                collPdc.Query.Load()
                rg.DataSource = collPdc

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collPdc.Query.es.PageNumber = 1
                collPdc.Query.es.PageSize = pagesize
                collPdc.Query.OrderBy(collPdc.Query.ChequeNo.Ascending)
                collPdc.Query.Load()
                rg.DataSource = collPdc


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub


        'created method for loading chq in drop down after select index change
        Public Shared Sub GetgvPdc(ByVal ChequeId As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryPdc As New ICPdcQuery("office")
            ' Dim qryBank As New ICBankQuery("bank")
            Dim dtPdc As New DataTable



            Dim collOffice As New ICPdcCollection

            If Not pagenumber = 0 Then
                qryPdc.Select(qryPdc.ChequeId, qryPdc.ChequeNo, qryPdc.PostingDate)
                qryPdc.Select(qryPdc.TransactionType, qryPdc.ClearingType)
                If ChequeId.ToString() <> "0" Then
                    qryPdc.Where(qryPdc.ChequeNo = ChequeId)
                End If
                ' qryPdc.InnerJoin(qryBank).On(qryPdc.ChequeId = qryBank.ChequeId)
                'qryPdc.OrderBy(qryPdc.ChequeNo.Ascending)
                dtPdc = qryPdc.LoadDataTable()
                rg.DataSource = dtPdc

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else
                qryPdc.es.PageNumber = pagenumber
                qryPdc.es.PageSize = pagesize

                qryPdc.Select(qryPdc.ChequeId, qryPdc.ChequeNo, qryPdc.PostingDate)
                qryPdc.Select(qryPdc.TransactionType, qryPdc.ClearingType)
                If ChequeId.ToString() <> "0" Then
                    qryPdc.Where(qryPdc.ChequeNo = ChequeId)
                End If
                'qryPdc.InnerJoin(qryBank).On(qryPdc.ChequeId = qryBank.ChequeId)
                'qryPdc.OrderBy(qryPdc.ChequeNo.Ascending)
                dtPdc = qryPdc.LoadDataTable()
                rg.DataSource = dtPdc


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub


        'created method for deleting cheque
        Public Shared Sub DeletePdc(ByVal ChequeId As String, ByVal UserID As String, ByVal UserName As String)

            Dim icPdc As New ICPdc
            Dim CountryName As String = ""
            Dim CurrentAt As String

            icPdc.es.Connection.CommandTimeout = 3600

            icPdc.LoadByPrimaryKey(ChequeId)
            CountryName = icPdc.ChequeId.ToString()

            CurrentAt = "ID : [ " & icPdc.ChequeId.ToString() & " ] Cheque Number: [" & icPdc.ChequeNo.ToString() & "] Account Number: [" & icPdc.AccountNumber.ToString() & "] Branch Code: [" & icPdc.BranchCode.ToString() & "] Currency: [" & icPdc.Currency.ToString() & "]"

            icPdc.MarkAsDeleted()
            icPdc.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Post Dated Cheque", icPdc.ChequeId.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")



        End Sub


        'created method for loading chq in drop down
        Public Shared Function GetPdc() As IC.ICPdcCollection

            Dim collPdc As New IC.ICPdcCollection

            collPdc.es.Connection.CommandTimeout = 3600

            collPdc.Query.Select(collPdc.Query.ChequeNo)
            collPdc.Query.OrderBy(collPdc.Query.ChequeNo.Ascending)
            collPdc.Query.Load()
            Return collPdc


        End Function


        'created function for matching presentment date and todays date for notification section of this module
        Public Shared Function PDCNotification() As DataTable

            Dim collPdc As New IC.ICPdcCollection
            Dim dt As New DataTable

            collPdc.es.Connection.CommandTimeout = 3600
            collPdc.Query.Select(collPdc.Query.ChequeNo)
            collPdc.Query.Where(collPdc.Query.Presentment.Date = Date.Now.Date)
            collPdc.Query.Where(collPdc.Query.NotificationSendDate.IsNull)

            If collPdc.Query.Load() Then
                Return collPdc.Query.LoadDataTable
            Else
                Return dt
            End If


        End Function

        'created this function for PDC notification section (checks the presentment date and today date)  for sending email and sms to the clearing user.
        Public Shared Sub AutoSendEmailForPDCClearingUser()

            'Dim collPdc As New IC.ICPdcCollection
            'collPdc = ICPdcController.PDCNotification()
            'If collPdc.Count > 0 Then
            EmailUtilities.PdcNotificationForEmail()
            SMSUtilities.PdcNotificationForSms()

            'End If


            'If collPdc Is Nothing Then

            '    'MsgBox("Data Not Matches", MsgBoxStyle.Information, "Error")
            '    Label1.Text = "Data Not Matches"
            'Else
            '    'MsgBox("Data Matches", MsgBoxStyle.Information, "Error")
            '    Label1.Text = "Data Matches"

            'End If





        End Sub

        'created function for filling cheq no in Email Utilities for send email notification to user.
        'Public Shared Function PDCNotificationforFetchingWithCHqNo() As DataTable

        '    Dim collPdc As New IC.ICPdcQuery

        '    'collPdc.CommandTimeout = 3600

        '    collPdc.Select(collPdc.ChequeNo)
        '    collPdc.Where(collPdc.Presentment.Date = Date.Now.Date)

        '    Return collPdc.LoadDataTable()



        'End Function

        Public Shared Function GetNotificationSendDateArrayList(ByVal chqArry As ArrayList) As IC.ICPdcCollection

            Dim qryObjICPdc As New ICPdcCollection


            qryObjICPdc.Query.Where(qryObjICPdc.Query.ChequeNo.In(chqArry) And qryObjICPdc.Query.NotificationSendDate.IsNull)
            qryObjICPdc.Query.Load()

            Return qryObjICPdc

        End Function
        Public Shared Sub UpdatePDCNotificationSentDate()
            Dim dt As New DataTable
            Dim objICPDCColl As New ICPdcCollection
            Dim ArrPDCChq As New ArrayList
            dt = ICBO.IC.ICPdcController.PDCNotification
            For Each dr As DataRow In dt.Rows
                ArrPDCChq.Add(dr("ChequeNo").ToString)
            Next
            If ArrPDCChq.Count > 0 Then
                objICPDCColl = GetNotificationSendDateArrayList(ArrPDCChq)
                For Each objICPDCCheque As ICPdc In objICPDCColl
                    objICPDCCheque.NotificationSendDate = Date.Now
                    objICPDCCheque.Save()
                Next
            End If
        End Sub
    End Class

End Namespace