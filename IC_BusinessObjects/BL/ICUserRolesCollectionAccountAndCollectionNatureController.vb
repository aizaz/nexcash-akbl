﻿Namespace IC
    Public Class ICUserRolesCollectionAccountAndCollectionNatureController
        Public Shared Function GetCollectionAccountCollectionNatureTaggedWithUser(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")

            Dim dt As DataTable
            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesCollAPNature.Select((qryObjICUserRolesCollAPNature.AccountNumber + "-" + qryObjICUserRolesCollAPNature.BranchCode + "-" + qryObjICUserRolesCollAPNature.Currency).As("AccountNumber"), qryObjICUserRolesCollAPNature.CollectionNatureCode.As("CollectionNature"))
            'qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True And qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.GroupBy(qryObjICUserRolesCollAPNature.AccountNumber, qryObjICUserRolesCollAPNature.BranchCode, qryObjICUserRolesCollAPNature.Currency, qryObjICUserRolesCollAPNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionAccountCollectionNatureTaggedWithUserForDropDown(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select((qryObjICUserRolesCollAPNature.AccountNumber + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("CollAccountAndNature"))
            qryObjICUserRolesCollAPNature.Select((qryObjICUserRolesCollAPNature.AccountNumber + "-" + qryObjICUserRolesCollAPNature.BranchCode + "-" + qryObjICUserRolesCollAPNature.Currency + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("Value"))
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            'qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionAccounts.IsActive = True And qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionAccountCollectionNatureTaggedWithUserForDropDownByCompanyCode(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal CompanyCode As String) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICCollNature As New ICCollectionNatureQuery("qryObjICCollNature")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select((qryObjICUserRolesCollAPNature.AccountNumber + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("CollAccountAndNature"))
            qryObjICUserRolesCollAPNature.Select((qryObjICUserRolesCollAPNature.AccountNumber + "-" + qryObjICUserRolesCollAPNature.BranchCode + "-" + qryObjICUserRolesCollAPNature.Currency + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("Value"))
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollNature).On(qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            'qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionAccounts.IsActive = True And qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICCollNature.IsActive = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICCompany.CompanyCode = CompanyCode)
            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionNatureTaggedWithUserForDropDown(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select((qryObjICCollectionNature.CollectionNatureName + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("CollAccountAndNature"))
            qryObjICUserRolesCollAPNature.Select(qryObjICUserRolesCollAPNature.CollectionNatureCode.As("Value"))
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionNature).On(qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollectionNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            'qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionAccounts.IsActive = True And qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionGroupTaggedWithUserForDropDown(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollAccountsCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountsCollNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select(qryObjICGroup.GroupCode, qryObjICGroup.GroupName)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollAccountsCollNature).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollAccountsCollNature.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollAccountsCollNature.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollAccountsCollNature.Currency And qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollAccountsCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountsCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountsCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountsCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)

            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICGroup.IsActive = True)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionCompanyTaggedWithUserForDropDownByGroupCode(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal GroupCode As String) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollAccountsCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountsCollNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollAccountsCollNature).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollAccountsCollNature.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollAccountsCollNature.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollAccountsCollNature.Currency And qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollAccountsCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountsCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountsCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountsCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCompany.GroupCode = GroupCode)
            qryObjICUserRolesCollAPNature.Where(qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        
        Public Shared Function GetCollectionNatureTaggedWithUserForDropDownByCompanyCode(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal CompanyCode As String) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollAccountsCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountsCollNature")
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select((qryObjICCollectionNature.CollectionNatureName + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("CollAccountAndNature"))
            qryObjICUserRolesCollAPNature.Select(qryObjICUserRolesCollAPNature.CollectionNatureCode.As("Value"))
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollAccountsCollNature).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollAccountsCollNature.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollAccountsCollNature.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollAccountsCollNature.Currency And qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollAccountsCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountsCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountsCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountsCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionNature).On(qryObjICCollAccountsCollNature.CollectionNatureCode = qryObjICCollectionNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionAccounts.IsActive = True And qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICCollectionNature.CompanyCode = CompanyCode)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionNatureTaggedWithUserForDropDownByCompanyCodeInvoiceDBAvailable(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal CompanyCode As String) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollAccountsCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountsCollNature")
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select((qryObjICCollectionNature.CollectionNatureName + "-" + qryObjICUserRolesCollAPNature.CollectionNatureCode).As("CollAccountAndNature"))
            qryObjICUserRolesCollAPNature.Select(qryObjICUserRolesCollAPNature.CollectionNatureCode.As("Value"))
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollAccountsCollNature).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollAccountsCollNature.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollAccountsCollNature.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollAccountsCollNature.Currency And qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollAccountsCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountsCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountsCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountsCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionNature).On(qryObjICCollAccountsCollNature.CollectionNatureCode = qryObjICCollectionNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))
            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionAccounts.IsActive = True And qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICCollectionNature.CompanyCode = CompanyCode)

            qryObjICUserRolesCollAPNature.Where(qryObjICCollectionNature.IsInvoiceDBAvailable = True)
            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetCollectionCompanyTaggedWithUserForDropDown(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesCollAPNature As New ICUserRolesCollectionAccountsAndCollectionNatureQuery("qryObjICUserRolesCollAPNature")
            Dim qryObjICCollAccountsCollNature As New ICCollectionAccountsCollectionNatureQuery("qryObjICCollAccountsCollNature")
            Dim qryObjICCollectionAccounts As New ICCollectionAccountsQuery("qryObjICCollectionAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim dt As DataTable
            qryObjICUserRolesCollAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollAccountsCollNature).On(qryObjICUserRolesCollAPNature.AccountNumber = qryObjICCollAccountsCollNature.AccountNumber And qryObjICUserRolesCollAPNature.BranchCode = qryObjICCollAccountsCollNature.BranchCode And qryObjICUserRolesCollAPNature.Currency = qryObjICCollAccountsCollNature.Currency And qryObjICUserRolesCollAPNature.CollectionNatureCode = qryObjICCollAccountsCollNature.CollectionNatureCode)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCollectionAccounts).On(qryObjICCollAccountsCollNature.AccountNumber = qryObjICCollectionAccounts.AccountNumber And qryObjICCollAccountsCollNature.BranchCode = qryObjICCollectionAccounts.BranchCode And qryObjICCollAccountsCollNature.Currency = qryObjICCollectionAccounts.Currency)
            qryObjICUserRolesCollAPNature.InnerJoin(qryObjICCompany).On(qryObjICCollectionAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.UserID = UserID.ToString)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.IsApproved = True)
            qryObjICUserRolesCollAPNature.Where(qryObjICUserRolesCollAPNature.RoleID.In(ArrayList))

            qryObjICUserRolesCollAPNature.Where(qryObjICCompany.IsCollectionAllowed = True And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)


            qryObjICUserRolesCollAPNature.es.Distinct = True
            dt = qryObjICUserRolesCollAPNature.LoadDataTable
            Return dt
        End Function
        '' Farah 02-10-2014
        Public Shared Function GetAllUserRolesCollectionAccountNatureByRoleIDAndUserID(ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesCollectionAccountsAndCollectionNatureCollection
            Dim objICUserRolesCollectionAccNatureColl As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            objICUserRolesCollectionAccNatureColl.LoadAll()
            If RoleID <> "0" Then
                objICUserRolesCollectionAccNatureColl.Query.Where(objICUserRolesCollectionAccNatureColl.Query.RoleID = RoleID.ToString)
            End If
            If UsersId <> "0" Then
                objICUserRolesCollectionAccNatureColl.Query.Where(objICUserRolesCollectionAccNatureColl.Query.UserID = UsersId.ToString)
            End If

            objICUserRolesCollectionAccNatureColl.Query.Load()
            Return objICUserRolesCollectionAccNatureColl
        End Function

        Public Shared Function GetAllUserRolesbyCollectionAccountCollectionNature(ByVal CollectionNatureCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesCollectionAccountsAndCollectionNatureCollection
            Dim collICUserRolesCollectionAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            collICUserRolesCollectionAccNature.es.Connection.CommandTimeout = 3600
            collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.CollectionNatureCode = CollectionNatureCode.ToString And collICUserRolesCollectionAccNature.Query.AccountNumber = AccountNumber.ToString)
            collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.BranchCode = BranchCode.ToString And collICUserRolesCollectionAccNature.Query.Currency = Currency.ToString)
            If UsersId <> "0" Then
                collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.UserID = UsersId.ToString)
            End If
            If RoleID <> "0" Then
                collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.RoleID = RoleID.ToString)
            End If
            collICUserRolesCollectionAccNature.Query.Load()
            Return collICUserRolesCollectionAccNature

        End Function
    End Class
End Namespace
