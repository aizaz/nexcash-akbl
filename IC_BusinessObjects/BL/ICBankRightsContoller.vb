﻿
Imports DotNetNuke.Security
Imports DotNetNuke.Data
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Tabs

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data
Imports DotNetNuke.Common
Imports DotNetNuke.Common.Utilities
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Services.Exceptions
Namespace IC
    Public Class ICBankRightsContoller

        '' Javed 25 November 2012
        Public Shared Sub AddRoleRight(ByVal cRoleRights As ICRoleRights, ByVal isUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICBankRoleRights As New ICRoleRights
            Dim objICRole As New ICRole
            objICRole.es.Connection.CommandTimeout = 3600
            objICBankRoleRights.es.Connection.CommandTimeout = 3600
            If (isUpdate = True) Then

                objICBankRoleRights.LoadByPrimaryKey(cRoleRights.RoleRightID)

            End If
            objICRole.LoadByPrimaryKey(cRoleRights.RoleID.ToString)
            objICBankRoleRights.RoleID = cRoleRights.RoleID
            objICBankRoleRights.RightName = cRoleRights.RightName
            objICBankRoleRights.RightValue = cRoleRights.RightValue
            objICBankRoleRights.RightDetailName = cRoleRights.RightDetailName
            objICBankRoleRights.CreatedBy = cRoleRights.CreatedBy
            objICBankRoleRights.CreatedDate = cRoleRights.CreatedDate
            objICBankRoleRights.Creater = cRoleRights.Creater
            objICBankRoleRights.CreationDate = cRoleRights.CreationDate
            objICBankRoleRights.IsApproved = cRoleRights.IsApproved
            objICBankRoleRights.Save()

            ICUtilities.AddAuditTrail("Role [ " & objICBankRoleRights.UpToICRoleByRoleID.RoleName & " ] allowed access to page [ " & objICBankRoleRights.RightName & " ] for right [ " & objICBankRoleRights.RightDetailName & " ].", "Role Right", objICBankRoleRights.RoleRightID.ToString, UsersID.ToString(), UsersName.ToString(), "ADD")

        End Sub


        'Public Shared Sub AddRoleRight(ByVal cBankRoleRights As ICBankRoleRights, ByVal isUpdate As Boolean)

        '    Dim objICBankRoleRights As New ICBankRoleRights
        '    objICBankRoleRights.es.Connection.CommandTimeout = 3600
        '    If (isUpdate = True) Then

        '        objICBankRoleRights.LoadByPrimaryKey(cBankRoleRights.BankRoleRightsID)
        '    End If

        '    objICBankRoleRights.RoleID = cBankRoleRights.RoleID
        '    objICBankRoleRights.RightName = cBankRoleRights.RightName
        '    objICBankRoleRights.CanView = cBankRoleRights.CanView
        '    objICBankRoleRights.CanAdd = cBankRoleRights.CanAdd
        '    objICBankRoleRights.CanEdit = cBankRoleRights.CanEdit
        '    objICBankRoleRights.CanDelete = cBankRoleRights.CanDelete
        '    objICBankRoleRights.CanApprove = cBankRoleRights.CanApprove

        '    objICBankRoleRights.Save()



        'End Sub
        Public Shared Function GetRights() As ICBankRightsCollection
            Dim objICRights As New ICBankRightsCollection
            objICRights.es.Connection.CommandTimeout = 3600

            ' objICRights.Query.Where(objICRights.Query.RightType <> "Client Right")
            objICRights.Query.OrderBy(objICRights.Query.RightName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICRights.Query.Load()


            Return objICRights
        End Function
        Public Shared Function GetCompanyRights() As ICBankRightsCollection
            Dim objICRights As New ICBankRightsCollection
            objICRights.es.Connection.CommandTimeout = 3600

            'objICRights.Query.Where(objICRights.Query.RightType = "Client Right")
            objICRights.Query.OrderBy(objICRights.Query.RightName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICRights.Query.Load()


            Return objICRights
        End Function
        Public Shared Sub deleteRights(ByVal RightsCode As String)

            Dim objICBank As New ICBankRights
            objICBank.es.Connection.CommandTimeout = 3600
            objICBank.LoadByPrimaryKey(RightsCode)
            objICBank.MarkAsDeleted()
            objICBank.Save()



        End Sub
        Public Shared Sub AddRightToRole(ByVal RoleID As String, ByVal TabID As String, ByVal RightName As String, ByVal UsersID As String, ByVal UsersName As String)

            Dim tpController As TabPermissionController
            Dim tabColl As New ArrayList
            Dim tpInfo As TabPermissionInfo
            Dim tc As New TabController
            Dim tInfo As New TabInfo
            Dim objICRole As New ICRole
            Dim ParentID As String = ""
            Dim i As Integer = 0
            Dim lvl As Integer = 0
            Dim PortalID As String = ""


            objICRole.es.Connection.CommandTimeout = 3600
            objICRole.LoadByPrimaryKey(RoleID.ToString)
            ' Get Parent ID of the Tab.
            tInfo = tc.GetTab(TabID, 0, True)
            ParentID = tInfo.ParentId.ToString()
            PortalID = tInfo.PortalID
            'Set View Permission to the Tab.
            tpInfo = New TabPermissionInfo
            tpInfo.AllowAccess = True
            tpInfo.PermissionID = 3
            tpInfo.RoleID = RoleID
            tpInfo.TabID = TabID

            tpController = New TabPermissionController
            Dim colltabPermission As DotNetNuke.Security.Permissions.TabPermissionCollection
            colltabPermission = TabPermissionController.GetTabPermissions(TabID, 0)
            Dim IsTabAdded As Boolean = False

            '
            For Each chktpInfo As TabPermissionInfo In colltabPermission
                If chktpInfo.RoleID = RoleID Then
                    'do nothing
                    IsTabAdded = True
                End If
            Next

            If IsTabAdded = False Then
                tpController.AddTabPermission(tpInfo)
            End If



            lvl = tInfo.Level
            tInfo = New TabInfo
            tInfo = tc.GetTab(ParentID, 0, True)

            For i = 0 To lvl
                If i > 0 Then
                    ParentID = tInfo.ParentId
                    tInfo = New TabInfo
                    tInfo = tc.GetTab(ParentID, 0, True)
                End If
                If ParentID <> "-1" Then
                    'Set View Permission to the Tab's Parent.
                    tpInfo = New TabPermissionInfo
                    tpInfo.AllowAccess = True
                    tpInfo.PermissionID = 3
                    tpInfo.RoleID = RoleID
                    tpInfo.TabID = ParentID

                    tpController = New TabPermissionController
                    Dim permissioncoll As DotNetNuke.Security.Permissions.TabPermissionCollection
                    permissioncoll = TabPermissionController.GetTabPermissions(ParentID, 0)
                    Dim isparentadded As Boolean = False
                    For Each tpInfo2 As TabPermissionInfo In permissioncoll
                        If tpInfo2.RoleID = RoleID Then
                            'do nothing
                            isparentadded = True
                        End If
                    Next
                    If isparentadded = False Then
                        tpController.AddTabPermission(tpInfo)
                    End If
                End If
               
            Next
            DataCache.ClearTabPermissionsCache(PortalID)
            ICUtilities.AddAuditTrail("Tab Permission : Set [ " & RightName.ToString & " ] Tab Permission To Role [ " & objICRole.RoleName.ToString & " ].", "Tab Permission", objICRole.RoleID.ToString, UsersID.ToString, UsersName.ToString(), "ADD")
        End Sub
        Public Shared Sub DeleteRightToRole(ByVal RoleID As String, ByVal TabID As String)

            Dim tpController As New TabPermissionController
            Dim tpInfo As TabPermissionInfo
            Dim tc As New TabController
            Dim tInfo, pinfo As TabInfo
            Dim ParentID As String = ""
            Dim i As Integer = 0
            Dim permissionProvider As DotNetNuke.Security.Permissions.PermissionProvider = DotNetNuke.Security.Permissions.PermissionProvider.Instance()
            Dim permissioncoll As DotNetNuke.Security.Permissions.TabPermissionCollection

            tInfo = tc.GetTab(TabID, 0, True)
            permissioncoll = tInfo.TabPermissions
            For Each tpInfo In permissioncoll
                If tpInfo.RoleID = RoleID Then
                    permissioncoll.Remove(tpInfo)
                    Exit For
                End If
            Next

            permissionProvider.SaveTabPermissions(tInfo)

            pinfo = tc.GetTab(tInfo.ParentId, 0, True)
            For i = 0 To tInfo.Level
                If i > 0 Then
                    pinfo = tc.GetTab(pinfo.ParentId, 0, True)
                End If
                If Not pinfo Is Nothing Then
                    permissioncoll = pinfo.TabPermissions
                    For Each tpInfo In permissioncoll
                        If tpInfo.RoleID = RoleID Then
                            permissioncoll.Remove(tpInfo)
                            Exit For
                        End If
                    Next
                    permissionProvider.SaveTabPermissions(pinfo)
                End If
            Next
            DataCache.ClearTabPermissionsCache(tInfo.PortalID)
        End Sub
        ''Javed Work 25 November 2012
        Public Shared Function GetTabIDByRightName(ByVal RightName As String) As Integer
            Dim objICRightsColl As New ICRightsCollection
            Dim dt As New DataTable

            objICRightsColl.es.Connection.CommandTimeout = 3600
            objICRightsColl.Query.Select(objICRightsColl.Query.TabID)
            objICRightsColl.Query.Where(objICRightsColl.Query.RightName = RightName.ToString)
            objICRightsColl.Query.Load()
            objICRightsColl.Query.es.Distinct = True
            dt = objICRightsColl.Query.LoadDataTable()

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0)
            Else
                Return 0
            End If
        End Function
    End Class
End Namespace

