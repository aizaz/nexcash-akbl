﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICSingleInstructionFormSettingController
        Public Shared Sub GetAllSingleInstructionFormSettingsForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal Pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryObjICOnlineFormSettings As New ICOnlineFormSettingsQuery("qryObjICOnlineFormSettings")
            Dim qryObjICAccountsPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountsPNature")
            Dim qryObjICPNature As New ICPaymentNatureQuery("qryObjICPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim dt As New DataTable

            qryObjICOnlineFormSettings.Select(qryObjICOnlineFormSettings.AccountNumber, qryObjICOnlineFormSettings.BranchCode, qryObjICOnlineFormSettings.Currency)
            qryObjICOnlineFormSettings.Select(qryObjICOnlineFormSettings.PaymentNatureCode, qryObjICOnlineFormSettings.ProductTypeCode)
            qryObjICOnlineFormSettings.Select(qryObjICPNature.PaymentNatureName, qryObjICProductType.ProductTypeName)
            qryObjICOnlineFormSettings.Select(qryObjICCompany.CompanyName, qryObjICCompany.CompanyCode, qryObjICOnlineFormSettings.IsApproved)

            qryObjICOnlineFormSettings.InnerJoin(qryObjICAccountsPNature).On(qryObjICOnlineFormSettings.AccountNumber = qryObjICAccountsPNature.AccountNumber And qryObjICOnlineFormSettings.BranchCode = qryObjICAccountsPNature.BranchCode And qryObjICOnlineFormSettings.Currency = qryObjICAccountsPNature.Currency And qryObjICOnlineFormSettings.PaymentNatureCode = qryObjICAccountsPNature.PaymentNatureCode)
            qryObjICOnlineFormSettings.InnerJoin(qryObjICPNature).On(qryObjICAccountsPNature.PaymentNatureCode = qryObjICPNature.PaymentNatureCode)
            qryObjICOnlineFormSettings.InnerJoin(qryObjICProductType).On(qryObjICOnlineFormSettings.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICOnlineFormSettings.InnerJoin(qryObjICAccounts).On(qryObjICAccountsPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountsPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountsPNature.Currency = qryObjICAccounts.Currency)
            qryObjICOnlineFormSettings.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICOnlineFormSettings.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICOnlineFormSettings.GroupBy(qryObjICOnlineFormSettings.AccountNumber, qryObjICOnlineFormSettings.BranchCode, qryObjICOnlineFormSettings.Currency)
            qryObjICOnlineFormSettings.GroupBy(qryObjICOnlineFormSettings.PaymentNatureCode, qryObjICOnlineFormSettings.ProductTypeCode)
            qryObjICOnlineFormSettings.GroupBy(qryObjICPNature.PaymentNatureName, qryObjICProductType.ProductTypeName)
            qryObjICOnlineFormSettings.GroupBy(qryObjICCompany.CompanyName, qryObjICCompany.CompanyCode, qryObjICOnlineFormSettings.IsApproved)
            qryObjICOnlineFormSettings.OrderBy(qryObjICCompany.CompanyName.Descending, qryObjICCompany.CompanyCode.Descending)
            If Not GroupCode = "" Then
                qryObjICOnlineFormSettings.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode = "" Then
                qryObjICOnlineFormSettings.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If


            qryObjICOnlineFormSettings.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            dt = qryObjICOnlineFormSettings.LoadDataTable()
            If Not PageNumber = 0 Then
                qryObjICOnlineFormSettings.es.PageNumber = PageNumber
                qryObjICOnlineFormSettings.es.PageSize = Pagesize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICOnlineFormSettings.es.PageNumber = 1
                qryObjICOnlineFormSettings.es.PageSize = Pagesize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If



        End Sub
        Public Shared Sub AddSingleInstructionFormSettings(ByVal objICOnlineFormSetting As ICOnlineFormSettings, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim objOnlineFormSettingForSave As New ICOnlineFormSettings
            Dim objICFieldsList As New ICFieldsList
            objICFieldsList.LoadByPrimaryKey(objICOnlineFormSetting.FieldID)
            objOnlineFormSettingForSave.es.Connection.CommandTimeout = 3600


           
            objOnlineFormSettingForSave.CreatedBy = objICOnlineFormSetting.CreatedBy
            objOnlineFormSettingForSave.CreatedDate = objICOnlineFormSetting.CreatedDate

            objOnlineFormSettingForSave.Creater = objICOnlineFormSetting.Creater
            objOnlineFormSettingForSave.CreationDate = objICOnlineFormSetting.CreationDate

            objOnlineFormSettingForSave.AccountNumber = objICOnlineFormSetting.AccountNumber
            objOnlineFormSettingForSave.BranchCode = objICOnlineFormSetting.BranchCode
            objOnlineFormSettingForSave.Currency = objICOnlineFormSetting.Currency
            objOnlineFormSettingForSave.PaymentNatureCode = objICOnlineFormSetting.PaymentNatureCode
            objOnlineFormSettingForSave.ProductTypeCode = objICOnlineFormSetting.ProductTypeCode
            objOnlineFormSettingForSave.CompanyCode = objICOnlineFormSetting.CompanyCode
            objOnlineFormSettingForSave.FieldID = objICOnlineFormSetting.FieldID
            objOnlineFormSettingForSave.IsRequired = objICOnlineFormSetting.IsRequired
            objOnlineFormSettingForSave.IsVisible = objICOnlineFormSetting.IsVisible
            objOnlineFormSettingForSave.IsApproved = False
            objOnlineFormSettingForSave.IsActive = True
            objOnlineFormSettingForSave.Save()
            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSettingForSave.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSettingForSave.AccountNumber & ", Branch Code: " & objOnlineFormSettingForSave.BranchCode & ", Currency " & objOnlineFormSettingForSave.Currency & ", Payment nature " & objOnlineFormSettingForSave.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSettingForSave.ProductTypeCode & ", Field [ " & objICFieldsList.FieldName & " ] added", "Single Instruction Form Settings", objOnlineFormSettingForSave.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
                'ElseIf IsUpdate = True Then
                '    ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSettingForSave.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSettingForSave.AccountNumber & ", Branch Code: " & objOnlineFormSettingForSave.BranchCode & ", Currency " & objOnlineFormSettingForSave.Currency & ", Payment nature " & objOnlineFormSettingForSave.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSettingForSave.ProductTypeCode & " updated", "Single Instruction Form Settings", objOnlineFormSettingForSave.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString)
            End If

        End Sub
        Public Shared Sub DeleteOnlineFormSettings(ByVal OnlineFormSettingsID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objOnlineFormSettings As New ICOnlineFormSettings
            Dim objOnlineFormSettingForDelete As New ICOnlineFormSettings
            Dim objICFieldsList As New ICFieldsList

            objOnlineFormSettingForDelete.es.Connection.CommandTimeout = 3600
            objOnlineFormSettings.LoadByPrimaryKey(OnlineFormSettingsID.ToString)
            objICFieldsList.LoadByPrimaryKey(objOnlineFormSettings.FieldID)
            objOnlineFormSettingForDelete.LoadByPrimaryKey(OnlineFormSettingsID.ToString)
            objOnlineFormSettingForDelete.MarkAsDeleted()
            objOnlineFormSettingForDelete.Save()
            ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSettings.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSettings.AccountNumber & ", Branch Code: " & objOnlineFormSettings.BranchCode & ", Currency " & objOnlineFormSettings.Currency & ", Payment nature " & objOnlineFormSettings.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSettings.ProductTypeCode & ", Filed [ " & objICFieldsList.FieldName & " ] deleted", "Single Instruction Form Settings", objOnlineFormSettings.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
        End Sub
        Public Shared Function GetOnlineFormSettingsByAPNPTypeAndCompanyCode(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal CompanyCode As String) As ICOnlineFormSettingsCollection
            Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
            objOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.OnlineFormSettingsID, objOnlineFormSettingsColl.Query.AccountNumber)
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.BranchCode, objOnlineFormSettingsColl.Query.Currency)
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.ProductTypeCode, objOnlineFormSettingsColl.Query.CompanyCode)
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.FieldID, objOnlineFormSettingsColl.Query.IsVisible)
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.IsRequired)
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.IsRequired.Case.When(objOnlineFormSettingsColl.Query.IsRequired = True).Then("Yes").Else("No").End.As("IsRequired"))
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.IsVisible.Case.When(objOnlineFormSettingsColl.Query.IsVisible = True).Then("Yes").Else("No").End.As("IsVisible"))
            objOnlineFormSettingsColl.Query.Select(objOnlineFormSettingsColl.Query.IsApproved.Case.When(objOnlineFormSettingsColl.Query.IsApproved = True).Then("True").Else("False").End.As("IsApproved"))
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.AccountNumber = AccountNumber.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.BranchCode = BranchCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.Currency = Currency.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.PaymentNatureCode = PaymentNatureCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.CompanyCode = CompanyCode.ToString)
            objOnlineFormSettingsColl.Query.Load()
            Return objOnlineFormSettingsColl
        End Function
        Public Shared Sub GetOnlineFormSettingsAssignedFieldsForRadGrid(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryObjOnlineFormSettings As New ICOnlineFormSettingsQuery("qryObjOnlineFormSettings")
            Dim objObjFieldsList As New ICFieldsListQuery("objObjFieldsList")
            Dim dt As New DataTable
            qryObjOnlineFormSettings.Select(objObjFieldsList.FieldName, qryObjOnlineFormSettings.FieldID)
            qryObjOnlineFormSettings.Select(qryObjOnlineFormSettings.IsApproved.Case.When(qryObjOnlineFormSettings.IsApproved = True).Then("True").Else("False").End.Cast(EntitySpaces.DynamicQuery.esCastType.String).As("IsApproved"))
            qryObjOnlineFormSettings.Select(qryObjOnlineFormSettings.IsRequired.Case.When(qryObjOnlineFormSettings.IsRequired = True).Then("Yes").Else("No").End.As("IsRequired"))
            qryObjOnlineFormSettings.Select(qryObjOnlineFormSettings.IsVisible.Case.When(qryObjOnlineFormSettings.IsVisible = True).Then("Yes").Else("No").End.As("IsVisible"))
            qryObjOnlineFormSettings.InnerJoin(objObjFieldsList).On(qryObjOnlineFormSettings.FieldID = objObjFieldsList.FieldID)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.AccountNumber = AccountNumber.ToString)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.BranchCode = BranchCode.ToString)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.Currency = Currency.ToString)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.PaymentNatureCode = PaymentNatureCode.ToString)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.ProductTypeCode = ProductTypeCode.ToString)
            qryObjOnlineFormSettings.Where(qryObjOnlineFormSettings.CompanyCode = CompanyCode.ToString)
            dt = qryObjOnlineFormSettings.LoadDataTable
            If Not PageNumber = 0 Then
                qryObjOnlineFormSettings.es.PageNumber = PageNumber
                qryObjOnlineFormSettings.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjOnlineFormSettings.es.PageNumber = 1
                qryObjOnlineFormSettings.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        Public Shared Function GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal CompanyCode As String) As ICOnlineFormSettings
            Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
            objOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600

            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.AccountNumber = AccountNumber.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.BranchCode = BranchCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.Currency = Currency.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.PaymentNatureCode = PaymentNatureCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.CompanyCode = CompanyCode.ToString)
            objOnlineFormSettingsColl.Query.Load()
            Return objOnlineFormSettingsColl(0)
        End Function
        'Public Sub DeleteOnlineFormSettingsByAPNPTypeCode(ByVal ArrayParam As String(), ByVal UsersID As String, ByVal UsersName As String)
        '    Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
        '    Dim objOnlineFormSettingForDelete As ICOnlineFormSettings
        '    objOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600


        '    objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.AccountNumber = ArrayParam(0).ToString)
        '    objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.BranchCode = ArrayParam(1))
        '    objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.Currency = ArrayParam(2))
        '    objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.PaymentNatureCode = ArrayParam(3))
        '    objOnlineFormSettingsColl.Query.Where(objOnlineFormSettingsColl.Query.ProductTypeCode = ArrayParam(4))
        '    If objOnlineFormSettingsColl.Query.Load Then
        '        For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl
        '            objOnlineFormSettingForDelete = New ICOnlineFormSettings
        '            objOnlineFormSettingForDelete.es.Connection.CommandTimeout = 3600
        '            objOnlineFormSettingForDelete.LoadByPrimaryKey(objOnlineFormSetting.OnlineFormSettingsID)
        '            objOnlineFormSettingForDelete.MarkAsDeleted()
        '            objOnlineFormSettingForDelete.Save()
        '            ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSetting.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSetting.AccountNumber & ", Branch Code: " & objOnlineFormSetting.BranchCode & ", Currency " & objOnlineFormSetting.Currency & ", Payment nature " & objOnlineFormSetting.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSetting.ProductTypeCode & " deleted", "Single Instruction Form Settings", objOnlineFormSetting.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString)
        '        Next
        '    End If
        'End Sub
        Public Shared Sub AproveOnlineFormSettings(ByVal FeildID As String, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal CompanyCode As String, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objOnlineFormSettingColl As New ICOnlineFormSettingsCollection
            Dim objOnlineFormSettingForApprove As ICOnlineFormSettings
            objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
            Dim objICFieldList As ICFieldsList
            objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.FieldID = FeildID And objOnlineFormSettingColl.Query.AccountNumber = AccountNo And objOnlineFormSettingColl.Query.BranchCode = BranchCode And objOnlineFormSettingColl.Query.Currency = Currency And objOnlineFormSettingColl.Query.PaymentNatureCode = PaymentNatureCode And objOnlineFormSettingColl.Query.ProductTypeCode = ProductTypeCode And objOnlineFormSettingColl.Query.CompanyCode = CompanyCode)
            If objOnlineFormSettingColl.Query.Load Then
                For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl
                    objICFieldList = New ICFieldsList
                    objICFieldList.LoadByPrimaryKey(objOnlineFormSetting.FieldID.ToString)
                    objOnlineFormSettingForApprove = New ICOnlineFormSettings
                    objOnlineFormSettingForApprove.es.Connection.CommandTimeout = 3600
                    objOnlineFormSettingForApprove.LoadByPrimaryKey(objOnlineFormSetting.OnlineFormSettingsID)
                    objOnlineFormSettingForApprove.IsApproved = IsApproved
                    objOnlineFormSettingForApprove.ApprovedDate = Date.Now
                    objOnlineFormSettingForApprove.ApprovedBy = UsersID
                    objOnlineFormSettingForApprove.Save()
                    ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSetting.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSetting.AccountNumber & ", Branch Code: " & objOnlineFormSetting.BranchCode & ", Currency " & objOnlineFormSetting.Currency & ", Payment nature " & objOnlineFormSetting.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSetting.ProductTypeCode & ", Field [ " & objICFieldList.FieldName & " ] approved", "Single Instruction Form Settings", objOnlineFormSetting.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString, "APPROVE")
                Next
            End If
        End Sub
        'Public Shared Sub AproveOnlineFormSettings(ByVal FeildID As String, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String)

        '    Dim objOnlineFormSettingColl As New ICOnlineFormSettingsCollection
        '    Dim objOnlineFormSettingForApprove As ICOnlineFormSettings
        '    objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
        '    objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.FieldID = FeildID)
        '    If objOnlineFormSettingColl.Query.Load Then
        '        For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl
        '            objOnlineFormSettingForApprove = New ICOnlineFormSettings
        '            objOnlineFormSettingForApprove.es.Connection.CommandTimeout = 3600
        '            objOnlineFormSettingForApprove.LoadByPrimaryKey(objOnlineFormSetting.OnlineFormSettingsID)
        '            objOnlineFormSettingForApprove.IsApproved = IsApproved
        '            objOnlineFormSettingForApprove.ApprovedDate = Date.Now
        '            objOnlineFormSettingForApprove.ApprovedBy = UsersID
        '            objOnlineFormSettingForApprove.Save()
        '            ICUtilities.AddAuditTrail("Single instruction form setting for company: " & objOnlineFormSetting.UpToICCompanyByCompanyCode.CompanyName & " against account number : " & objOnlineFormSetting.AccountNumber & ", Branch Code: " & objOnlineFormSetting.BranchCode & ", Currency " & objOnlineFormSetting.Currency & ", Payment nature " & objOnlineFormSetting.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & ", Product type: " & objOnlineFormSetting.ProductTypeCode & " approved", "Single Instruction Form Settings", objOnlineFormSetting.OnlineFormSettingsID.ToString, UsersID.ToString, UsersName.ToString)
        '        Next
        '    End If
        'End Sub
    End Class
End Namespace
