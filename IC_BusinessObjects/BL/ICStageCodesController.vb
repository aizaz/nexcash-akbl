﻿Namespace IC
    Public Class ICStageCodesController
        Public Shared Function GetStageCodeDescription(ByVal StageCode As String) As String
            Dim collObjICStageCode As New ICStageCodesCollection


            collObjICStageCode.Query.Where(collObjICStageCode.Query.StageCode = StageCode)
            collObjICStageCode.Query.OrderBy(collObjICStageCode.Query.StageCode.Ascending)
            collObjICStageCode.Query.Load()
            If collObjICStageCode.Count > 0 Then
                Return collObjICStageCode(0).StageDescription.ToString
            Else
                Return "N/A"
            End If
        End Function
    End Class
End Namespace
