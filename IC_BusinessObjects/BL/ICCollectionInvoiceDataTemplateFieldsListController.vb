﻿Imports Telerik.Web.UI

Namespace IC
    Public Class ICCollectionInvoiceDataTemplateFieldsListController

        

        Public Shared Sub AddTemplateField(ByVal objICTemplateFieldsList As ICCollectionInvoiceDataTemplateFields, ByVal TemplateName As String, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTemplateFieldListFoSave As New ICCollectionInvoiceDataTemplateFields
            objICTemplateFieldListFoSave.es.Connection.CommandTimeout = 3600

            If (IsUpdate = True) Then
                objICTemplateFieldListFoSave.LoadByPrimaryKey(objICTemplateFieldsList.CollectionFieldID)
            End If

            objICTemplateFieldListFoSave.FieldName = objICTemplateFieldsList.FieldName
            objICTemplateFieldListFoSave.FieldType = objICTemplateFieldsList.FieldType
            objICTemplateFieldListFoSave.FieldOrder = objICTemplateFieldsList.FieldOrder
            objICTemplateFieldListFoSave.FieldLength = objICTemplateFieldsList.FieldLength
            objICTemplateFieldListFoSave.FieldDBType = objICTemplateFieldsList.FieldDBType
            objICTemplateFieldListFoSave.CollectionInvoiceTemplateID = objICTemplateFieldsList.CollectionInvoiceTemplateID
            objICTemplateFieldListFoSave.CollectionFieldID = objICTemplateFieldsList.CollectionFieldID
            objICTemplateFieldListFoSave.IsReference = objICTemplateFieldsList.IsReference

            objICTemplateFieldListFoSave.CreateBy = objICTemplateFieldsList.CreateBy
            objICTemplateFieldListFoSave.CreateDate = objICTemplateFieldsList.CreateDate


            objICTemplateFieldListFoSave.Save()


            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Invoice DB Template field :" & objICTemplateFieldListFoSave.FieldName & " type " & objICTemplateFieldListFoSave.FieldType & " assigned to template : " & TemplateName & " .", "Invoice Data Template Field", objICTemplateFieldListFoSave.CollectionInvoiceTemplateID.ToString + "" + objICTemplateFieldListFoSave.CollectionFieldID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpdate = True Then
                ICUtilities.AddAuditTrail("Invoice DB Template field :" & objICTemplateFieldListFoSave.FieldName & " type " & objICTemplateFieldListFoSave.FieldType & " assigned to template : " & TemplateName & " after update .", "Invoice Data Template Field", objICTemplateFieldListFoSave.CollectionInvoiceTemplateID.ToString + "" + objICTemplateFieldListFoSave.CollectionFieldID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If
        End Sub






        Public Shared Function CheckInvoiceStatusAgainstTemplate(ByVal TemplateCode As String) As Boolean
            Dim collICFiles As New ICFilesCollection


            Dim Result As Boolean = False

            collICFiles.Query.Select(collICFiles.Query.Status)
            collICFiles.Query.Where(collICFiles.Query.FileUploadTemplateID = TemplateCode.ToString And collICFiles.Query.Status = "Pending Read" And collICFiles.Query.FileType = "Collection Invoice")
            If collICFiles.Query.Load() Then
                Result = True
            Else
                Result = False
            End If

            Return Result
        End Function

        Public Shared Sub DeleteTemplateFieldForUpdate(ByVal TemplateFieldCode As String, ByVal TemplateName As String, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTemplateFieldListColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim ActionString As String = ""
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.CollectionInvoiceTemplateID = TemplateFieldCode.ToString)
            objICTemplateFieldListColl.Query.Load()
            If objICTemplateFieldListColl.Query.Load Then
                ActionString = "Following invoice data template fields for template [ " & objICTemplateFieldListColl(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & " ] are deleted : "
                For Each objICTempFieldForDelete As ICCollectionInvoiceDataTemplateFields In objICTemplateFieldListColl
                    ActionString += " FieldName [ " & objICTempFieldForDelete.FieldName & " ] Field Type [" & objICTempFieldForDelete.FieldType & "] Field Order [" & objICTempFieldForDelete.FieldOrder & "],"
                Next
                objICTemplateFieldListColl.MarkAllAsDeleted()
                objICTemplateFieldListColl.Save()
                ActionString = ActionString.Remove(ActionString.Length - 1, 1)
                ICUtilities.AddAuditTrail(ActionString, "Invoice Data Template", TemplateFieldCode, UsersID.ToString, UsersName.ToString, "DELETE")
            End If




        End Sub

        Public Shared Sub DeleteTemplateFieldByFieldID(ByVal FieldID As String, ByVal FileUploadTemplateCode As String, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTemplateFieldListColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim ActionString As String = ""
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.CollectionFieldID = FieldID.ToString)
            objICTemplateFieldListColl.Query.Load()
            If objICTemplateFieldListColl.Query.Load Then
                ActionString = "Following invoice data template fields for template [ " & objICTemplateFieldListColl(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & " ] are deleted : "
                For Each objICTempFieldForDelete As ICCollectionInvoiceDataTemplateFields In objICTemplateFieldListColl
                    ActionString += " FieldName [ " & objICTempFieldForDelete.FieldName & " ] Field Type [" & objICTempFieldForDelete.FieldType & "] Field Order [" & objICTempFieldForDelete.FieldOrder & "],"
                Next
                objICTemplateFieldListColl.MarkAllAsDeleted()
                objICTemplateFieldListColl.Save()
                ActionString = ActionString.Remove(ActionString.Length - 1, 1)
                ICUtilities.AddAuditTrail(ActionString, "Invoice Data Template", FileUploadTemplateCode, UsersID.ToString, UsersName.ToString, "DELETE")
            End If




        End Sub

        Public Shared Sub UpdateInvoiceDBTemplatesFieldOrder(ByVal FileUploadTemplateCode As String, ByVal UserID As String, ByVal UserName As String, ByVal FiedType As String)
            Dim objICTemplateFieldListColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim FieldOrderCount As Integer = 0
            Dim ActionString As String = ""
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.CollectionInvoiceTemplateID = FileUploadTemplateCode And objICTemplateFieldListColl.Query.FieldType = FiedType)
            objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.CollectionFieldID.Ascending)

            If objICTemplateFieldListColl.Query.Load Then
                ActionString = "Following invoice data template field's Order for template [ " & objICTemplateFieldListColl(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & " ] are updated : "
                For Each objICTempFieldForDelete As ICCollectionInvoiceDataTemplateFields In objICTemplateFieldListColl
                    FieldOrderCount = FieldOrderCount + 1
                    ActionString += " FieldName [ " & objICTempFieldForDelete.FieldName & " ] Field Type [" & objICTempFieldForDelete.FieldType & "] Field Order from [" & objICTempFieldForDelete.FieldOrder & "] to [ " & FieldOrderCount & " ],"
                    objICTempFieldForDelete.FieldOrder = FieldOrderCount
                    objICTempFieldForDelete.Save()
                Next


                ActionString = ActionString.Remove(ActionString.Length - 1, 1)
                ICUtilities.AddAuditTrail(ActionString, "Invoice Data Template", FileUploadTemplateCode, UserID.ToString, UserName.ToString, "UPDATE")
            End If
        End Sub
        Public Shared Sub GetAllTemplateFieldByTemplateIDForRadGrid(ByVal TemplateID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


            Dim qryObjICTemplateFieldList As New ICCollectionInvoiceDataTemplateFieldsQuery("qryObjICTemplateFieldList")

            Dim dt As New DataTable
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.CollectionFieldID)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldName)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldLength)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldOrder)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsReference)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldDBType, qryObjICTemplateFieldList.FieldType)

            qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.CollectionInvoiceTemplateID = TemplateID)


            qryObjICTemplateFieldList.OrderBy(qryObjICTemplateFieldList.FieldType.Descending, qryObjICTemplateFieldList.FieldOrder.Ascending)
            dt = qryObjICTemplateFieldList.LoadDataTable

            If Not PageNumber = 0 Then
                qryObjICTemplateFieldList.es.PageNumber = PageNumber
                qryObjICTemplateFieldList.es.PageSize = PageSize

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                qryObjICTemplateFieldList.es.PageNumber = 1
                qryObjICTemplateFieldList.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub




        Public Shared Sub GetFieldListForRadGridByFieldType(ByVal rg As RadGrid, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal FieldType As String, ByVal FileUpload As Boolean)
            Dim objICFieldsListColl As New ICCollectionFieldsListCollection
            Dim dt As New DataTable
            objICFieldsListColl.es.Connection.CommandTimeout = 3600

            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.FieldName, objICFieldsListColl.Query.FieldDBLength, objICFieldsListColl.Query.CollectionFieldID, objICFieldsListColl.Query.MustRequired)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired.Case.When(objICFieldsListColl.Query.MustRequired = True).Then("False").Else("True").End().As("IsEnabled"))
            objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldType = FieldType.ToString)

            objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldName.Ascending)
            dt = objICFieldsListColl.Query.LoadDataTable

            If Not PageNumber = 0 Then
                objICFieldsListColl.Query.es.PageNumber = PageNumber
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICFieldsListColl.Query.es.PageNumber = 1
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(ByVal TemplateID As String) As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim collICCollectionInvoiceTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection


            collICCollectionInvoiceTemplateFields.Query.Where(collICCollectionInvoiceTemplateFields.Query.CollectionInvoiceTemplateID = TemplateID.ToString() And collICCollectionInvoiceTemplateFields.Query.IsReference = True)

            collICCollectionInvoiceTemplateFields.Query.OrderBy(collICCollectionInvoiceTemplateFields.Query.FieldOrder.Ascending)
            collICCollectionInvoiceTemplateFields.Query.Load()

            Return collICCollectionInvoiceTemplateFields
        End Function

       

        Public Shared Function GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(ByVal TemplateID As String, ByVal FieldType As String) As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim collICCollectionInvoiceTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection
            collICCollectionInvoiceTemplateFields.es.Connection.CommandTimeout = 3600

            collICCollectionInvoiceTemplateFields.Query.Where(collICCollectionInvoiceTemplateFields.Query.CollectionInvoiceTemplateID = TemplateID.ToString())
            If FieldType.ToString() = "Header" Then
                collICCollectionInvoiceTemplateFields.Query.Where(collICCollectionInvoiceTemplateFields.Query.FieldType = "NonFlexi")
            ElseIf FieldType.ToString() = "Detail" Then
                collICCollectionInvoiceTemplateFields.Query.Where(collICCollectionInvoiceTemplateFields.Query.FieldType <> "NonFlexi")
            End If
            collICCollectionInvoiceTemplateFields.Query.OrderBy(collICCollectionInvoiceTemplateFields.Query.FieldOrder.Ascending)
            collICCollectionInvoiceTemplateFields.Query.Load()

            Return collICCollectionInvoiceTemplateFields
        End Function

        Public Shared Function GetTemplateFieldsTemplateID(ByVal TemplateID As String) As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim collICAPNFUTemplate As New ICCollectionInvoiceDataTemplateFieldsCollection
            collICAPNFUTemplate.es.Connection.CommandTimeout = 3600

            collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.CollectionInvoiceTemplateID = TemplateID.ToString())
            collICAPNFUTemplate.Query.OrderBy(collICAPNFUTemplate.Query.FieldType.Descending, collICAPNFUTemplate.Query.FieldOrder.Ascending)
            collICAPNFUTemplate.Query.Load()

            Return collICAPNFUTemplate
        End Function
        'Public Shared Function GetTemplateFieldsByInvoiceTemplateOfDualFile(ByVal TemplateID As String, ByVal FieldType As String) As ICCollectionInvoiceDataTemplateFieldsCollection
        '    Dim collICAPNFUTemplate As New ICCollectionInvoiceDataTemplateFieldsCollection


        '    collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.CollectionInvoiceTemplateID = TemplateID.ToString())
        '    If FieldType.ToString() = "Header" Then
        '        collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType = "NonFlexi")
        '    ElseIf FieldType.ToString() = "Detail" Then
        '        collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType <> "Detail")
        '    End If
        '    collICAPNFUTemplate.Query.OrderBy(collICAPNFUTemplate.Query.FieldOrder.Ascending)
        '    collICAPNFUTemplate.Query.Load()

        '    Return collICAPNFUTemplate
        'End Function

        Public Shared Function GetTemplateFieldsByInvoiceTemplateOfDualFile(ByVal TemplateID As String, ByVal FieldType As String) As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim collICAPNFUTemplate As New ICCollectionInvoiceDataTemplateFieldsCollection


            collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.CollectionInvoiceTemplateID = TemplateID.ToString())
            If FieldType.ToString() = "Header" Then
                collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType = "NonFlexi")
            ElseIf FieldType.ToString() = "Detail" Then
                'collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType <> "Detail")
                collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType = "Detail" Or (collICAPNFUTemplate.Query.FieldType = "NonFlexi" And collICAPNFUTemplate.Query.IsReference = True))
            End If
            collICAPNFUTemplate.Query.OrderBy(collICAPNFUTemplate.Query.FieldOrder.Ascending)
            collICAPNFUTemplate.Query.Load()

            Return collICAPNFUTemplate
        End Function

        Public Shared Function CheckCollectionFieldDBLength(ByVal FieldID As String, ByVal FieldContent As String) As Boolean
            Dim objICFieldsList As New ICCollectionInvoiceDataTemplateFields
            Dim FieldContentLength As Integer = 0
            FieldContentLength = FieldContent.Length
            If objICFieldsList.LoadByPrimaryKey(FieldID) Then
                If objICFieldsList.FieldDBType = "Numeric" Then
                    Dim Amount As String = ""
                    Amount = FieldContent
                    If Amount.Contains(".") Then
                        Dim str As String()
                        str = Amount.Split(".")
                        Amount = ""
                        Amount = str(0) & str(1)
                    End If
                    If Amount.Length > objICFieldsList.FieldLength Then
                        Return False
                    Else
                        Return True
                    End If

                Else

                    If FieldContentLength > objICFieldsList.FieldLength Then
                        Return False
                    Else
                        Return True
                    End If

                End If
            Else
                Return False
            End If
        End Function

        Public Shared Function GetFieldDBTypebyFieldID(ByVal FieldID As String) As String
            'Dim objICFieldsList As New ICCollectionFieldsList
            'Dim FieldContentLength As Integer = 0

            'objICFieldsList.es.Connection.CommandTimeout = 3600

            'If objICFieldsList.LoadByPrimaryKey(FieldID) Then
            '    If objICFieldsList.Query.Load() Then
            '        Return objICFieldsList.FieldDBType
            '    Else
            '        Return ""
            '    End If
            'End If
               
        End Function



        

        Public Shared Function GetAllFieldIDByTemplateIDForUpdateCollNature(ByVal TemplateID As String) As DataTable
            Dim qryObjICTemplateFieldList As New ICCollectionInvoiceDataTemplateFieldsQuery("qryObjICTemplateFieldList")
            Dim dt As New DataTable

            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.CollectionFieldID)
            qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.CollectionInvoiceTemplateID = TemplateID)
            dt = qryObjICTemplateFieldList.LoadDataTable
            Return dt
        End Function
        Public Shared Function CheckCollectionFieldDBType(ByVal FieldID As String, ByVal FieldContent As String, ByVal FieldName As String, ByVal FieldType As String) As String
            Try

                Dim objICFieldsList As New ICCollectionInvoiceDataTemplateFields
                objICFieldsList.LoadByPrimaryKey(FieldID)
                Dim Result As Integer
                Dim ResultDouble As Double
                Dim CheckResult As String = "OK"




                If FieldContent <> "" Then
                    If objICFieldsList.FieldDBType = "Integer" Then
                        If FieldContent.Length > objICFieldsList.FieldLength Then
                            CheckResult = Nothing
                            CheckResult = "Invalid field length"
                            Return CheckResult
                            Exit Function
                        End If
                        If IsNumeric(FieldContent) = False Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function

                        End If
                        If Integer.TryParse(FieldContent, Result) = False Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function
                        End If
                        If CDbl(FieldContent) <= 0 Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function
                        End If

                    ElseIf objICFieldsList.FieldDBType = "Numeric" Then
                        If FieldContent.Length > objICFieldsList.FieldLength Then
                            CheckResult = Nothing
                            CheckResult = " Invalid field length"
                            Return CheckResult
                            Exit Function
                        End If
                        If IsNumeric(FieldContent) = False Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function

                        End If
                        If Double.TryParse(FieldContent, ResultDouble) = False Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function
                        End If
                        If CDbl(FieldContent) <= 0 Then
                            CheckResult = Nothing
                            CheckResult = " is not in correct format."
                            Return CheckResult
                            Exit Function
                        End If

                    End If
                End If

                Return CheckResult
            Catch ex As Exception
                Return " is not in correct format."
            End Try
        End Function
        Public Shared Function GetFieldsByTemplateIDandDataType(ByVal TemplateID As String, ByVal DataType As String) As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim collInvoiceTempFields As New ICCollectionInvoiceDataTemplateFieldsCollection
            collInvoiceTempFields.Query.Where(collInvoiceTempFields.Query.CollectionInvoiceTemplateID = TemplateID)
            collInvoiceTempFields.Query.Where(collInvoiceTempFields.Query.FieldDBType = DataType)
            collInvoiceTempFields.Query.Load()
            Return collInvoiceTempFields
        End Function


        Public Shared Sub GetAllTemplateFieldByTemplateIDForCollNature(ByVal TemplateID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim collICTemplateFieldList As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim dt As New DataTable

            collICTemplateFieldList.Query.Select(collICTemplateFieldList.Query.CollectionFieldID, collICTemplateFieldList.Query.FieldOrder, collICTemplateFieldList.Query.FieldName, collICTemplateFieldList.Query.IsReference, collICTemplateFieldList.Query.FieldDBType)
            collICTemplateFieldList.Query.Where(collICTemplateFieldList.Query.CollectionInvoiceTemplateID = TemplateID)

            collICTemplateFieldList.Query.OrderBy(collICTemplateFieldList.Query.FieldOrder.Ascending)
            dt = collICTemplateFieldList.Query.LoadDataTable

            If Not PageNumber = 0 Then
                collICTemplateFieldList.Query.es.PageNumber = PageNumber
                collICTemplateFieldList.Query.es.PageSize = PageSize

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                collICTemplateFieldList.Query.es.PageNumber = 1
                collICTemplateFieldList.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
    End Class
End Namespace
