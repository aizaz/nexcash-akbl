﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICAmendmentRightsController
        Public Shared Sub GetAllAmendmentRightsForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal ProductTypeCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As Radgrid)
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjAmendmentRights As New ICAmendmentsRightsManagementQuery("qryObjAmendmentRights")
            Dim dt As New DataTable


            qryObjAmendmentRights.Select(qryObjAmendmentRights.CompanyCode, qryObjAmendmentRights.ProductTypeCode, qryObjICGroup.GroupName)
            qryObjAmendmentRights.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
            qryObjAmendmentRights.InnerJoin(qryObjICCompany).On(qryObjAmendmentRights.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjAmendmentRights.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjAmendmentRights.InnerJoin(qryObjICProductType).On(qryObjAmendmentRights.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjAmendmentRights.GroupBy(qryObjAmendmentRights.CompanyCode, qryObjAmendmentRights.ProductTypeCode, qryObjICGroup.GroupName)
            qryObjAmendmentRights.GroupBy(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)

            qryObjAmendmentRights.OrderBy(qryObjICGroup.GroupName.Ascending)
            If Not GroupCode = "" Then
                qryObjAmendmentRights.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode = "" Then
                qryObjAmendmentRights.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If
            If Not ProductTypeCode = "" Then
                qryObjAmendmentRights.Where(qryObjICProductType.ProductTypeCode = ProductTypeCode.ToString)
            End If
           

            dt = qryObjAmendmentRights.LoadDataTable

            If Not PageNumber = 0 Then
                qryObjAmendmentRights.es.PageNumber = PageNumber
                qryObjAmendmentRights.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjAmendmentRights.es.PageNumber = 1
                qryObjAmendmentRights.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If





        End Sub
        Public Shared Sub AddAmendRightsManagement(ByVal objICAmendrights As ICAmendmentsRightsManagement, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAmendRightsForSave As New ICAmendmentsRightsManagement
            Dim StrAction As String = Nothing

            objICAmendRightsForSave.es.Connection.CommandTimeout = 3600
            objICAmendRightsForSave.FieldID = objICAmendrights.FieldID
            objICAmendRightsForSave.CompanyCode = objICAmendrights.CompanyCode
            objICAmendRightsForSave.ProductTypeCode = objICAmendrights.ProductTypeCode
            objICAmendRightsForSave.IsAmendmentAllow = objICAmendrights.IsAmendmentAllow
            objICAmendRightsForSave.IsBulkAmendmentAllow = objICAmendrights.IsBulkAmendmentAllow
            objICAmendRightsForSave.CreatedDate = objICAmendrights.CreatedDate
            objICAmendRightsForSave.CreatedBy = objICAmendrights.CreatedBy
            objICAmendRightsForSave.CanView = objICAmendrights.CanView
            objICAmendRightsForSave.Save()
            If IsUpdate = False Then
                StrAction = Nothing
                StrAction += "Amend rights for company [ " & objICAmendRightsForSave.UpToICCompanyByCompanyCode.CompanyName & " ] "
                StrAction += ", for product type [ " & objICAmendRightsForSave.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]"
                StrAction += " [ " & objICAmendRightsForSave.ProductTypeCode & " ] added. Rights assigned on field [ " & objICAmendRightsForSave.UpToICFieldsListByFieldID.FieldName & " ] Can View set to "
                If objICAmendRightsForSave.CanView = True Then
                    StrAction += " [ Yes ], "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend set to "
                If objICAmendRightsForSave.IsAmendmentAllow = True Then
                    StrAction += " [ Yes ] , "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend in bulk set to"
                If objICAmendRightsForSave.IsBulkAmendmentAllow = True Then
                    StrAction += " [ Yes ]."
                Else
                    StrAction += " [ No ]."
                End If

                ICUtilities.AddAuditTrail(StrAction, "Amendment Rights Management", objICAmendRightsForSave.AmendmentsRightsID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpdate = True Then
               StrAction = Nothing
                StrAction += "Amend rights for company [ " & objICAmendRightsForSave.UpToICCompanyByCompanyCode.CompanyName & " ] "
                StrAction += ", for product type [ " & objICAmendRightsForSave.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]"
                StrAction += " [ " & objICAmendRightsForSave.ProductTypeCode & " ] added after delete on update. Rights assigned on field [ " & objICAmendRightsForSave.UpToICFieldsListByFieldID.FieldName & " ] Can View set to "
                If objICAmendRightsForSave.CanView = True Then
                    StrAction += " [ Yes ], "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend set to"
                If objICAmendRightsForSave.IsAmendmentAllow = True Then
                    StrAction += " [ Yes ], "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend in bulk set to "
                If objICAmendRightsForSave.IsBulkAmendmentAllow = True Then
                    StrAction += " [ Yes]."
                Else
                    StrAction += " [ No ]."
                End If
                ICUtilities.AddAuditTrail(StrAction, "Amendment Rights Management", objICAmendRightsForSave.AmendmentsRightsID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If
        End Sub
        Public Shared Sub DeleteamendRightsManagement(ByVal objICAmendmentRights As ICAmendmentsRightsManagement, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAmendRightsMAnagementForDelete As New ICAmendmentsRightsManagement
            objICAmendRightsMAnagementForDelete.es.Connection.CommandTimeout = 3600
            If objICAmendRightsMAnagementForDelete.LoadByPrimaryKey(objICAmendmentRights.AmendmentsRightsID) Then
                objICAmendRightsMAnagementForDelete.MarkAsDeleted()
                objICAmendRightsMAnagementForDelete.Save()
                Dim StrAction As String = Nothing
                StrAction = Nothing
                StrAction += "Amend rights for company [ " & objICAmendmentRights.UpToICCompanyByCompanyCode.CompanyName & " ] "
                StrAction += ", for product type [ " & objICAmendmentRights.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]"
                StrAction += " [ " & objICAmendmentRights.ProductTypeCode & " ] deleted for update. Rights assigned on field was [ " & objICAmendmentRights.UpToICFieldsListByFieldID.FieldName & " ] Can View "
                If objICAmendmentRights.CanView = True Then
                    StrAction += " [ Yes ], "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend "
                If objICAmendmentRights.IsAmendmentAllow = True Then
                    StrAction += " [ Yes ], "
                Else
                    StrAction += " [ No ], "
                End If
                StrAction += "can amend in bulk "
                If objICAmendmentRights.IsBulkAmendmentAllow = True Then
                    StrAction += " [ Yes]."
                Else
                    StrAction += " [ No ]."
                End If

                'ICUtilities.AddAuditTrail("Amend rights Can View [ " & objICAmendmentRights.CanView & " ], can amend [ " & objICAmendmentRights.IsAmendmentAllow & " ] and can amend in bulk [ " & objICAmendmentRights.IsBulkAmendmentAllow & " assigned on field [ " & objICAmendmentRights.UpToICFieldsListByFieldID.FieldName & " ] for company [ " & objICAmendmentRights.UpToICCompanyByCompanyCode.CompanyName & " ] for product type [ " & objICAmendmentRights.UpToICProductTypeByProductTypeCode.ProductTypeName & " ] [ " & objICAmendmentRights.ProductTypeCode & " ] deleted for update.", "Amendment Rights Management", objICAmendmentRights.AmendmentsRightsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                ICUtilities.AddAuditTrail(StrAction, "Amendment Rights Management", objICAmendmentRights.AmendmentsRightsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub

        Public Shared Function GetAllAmendRightsCollectionByCompanyCodeAndProductTypeCode(ByVal ProductTypeCode As String, ByVal Companycode As String) As ICAmendmentsRightsManagementCollection
            Dim objICAmendmentRightsColl As New ICAmendmentsRightsManagementCollection
            objICAmendmentRightsColl.es.Connection.CommandTimeout = 3600

            objICAmendmentRightsColl.Query.Where(objICAmendmentRightsColl.Query.CompanyCode = Companycode.ToString And objICAmendmentRightsColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objICAmendmentRightsColl.Query.Load()

            Return objICAmendmentRightsColl
        End Function
        Public Shared Function GetAllAmendRightssingleObjectByCompanyCodeAndProductTypeCode(ByVal ProductTypeCode As String, ByVal Companycode As String) As ICAmendmentsRightsManagement
            Dim objICAmendmentRightsColl As New ICAmendmentsRightsManagementCollection
            objICAmendmentRightsColl.es.Connection.CommandTimeout = 3600

            objICAmendmentRightsColl.Query.Where(objICAmendmentRightsColl.Query.CompanyCode = Companycode.ToString And objICAmendmentRightsColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objICAmendmentRightsColl.Query.Load()
            Return objICAmendmentRightsColl(0)
        End Function
        Public Shared Sub GetAllAmendRightAssignedByCompanyCodeAndProductTypeCodeForRadGrid(ByVal ProductTypeCode As String, ByVal Companycode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
            Dim qryObjICAmendmentRights As New ICAmendmentsRightsManagementQuery("qryObjICAmendmentRights")
            Dim qryObjFieldList As New ICFieldsListQuery("qryObjFieldList")
            Dim dt As New DataTable


            qryObjICAmendmentRights.Select(qryObjFieldList.FieldName, qryObjICAmendmentRights.FieldID)
            qryObjICAmendmentRights.Select(qryObjICAmendmentRights.IsAmendmentAllow.Case.When(qryObjICAmendmentRights.IsAmendmentAllow = True).Then("Yes").Else("No").End.As("IsAmendmentAllow"))
            qryObjICAmendmentRights.Select(qryObjICAmendmentRights.IsBulkAmendmentAllow.Case.When(qryObjICAmendmentRights.IsBulkAmendmentAllow = True).Then("Yes").Else("No").End.As("IsBulkAmendmentAllow"))
            qryObjICAmendmentRights.Select(qryObjICAmendmentRights.CanView.Case.When(qryObjICAmendmentRights.CanView = True).Then("Yes").Else("No").End.As("CanView"))
            qryObjICAmendmentRights.InnerJoin(qryObjFieldList).On(qryObjICAmendmentRights.FieldID = qryObjFieldList.FieldID)
            qryObjICAmendmentRights.Where(qryObjICAmendmentRights.CompanyCode = Companycode And qryObjICAmendmentRights.ProductTypeCode = ProductTypeCode)

            dt = qryObjICAmendmentRights.LoadDataTable



            If Not PageNumber = 0 Then
                qryObjICAmendmentRights.es.PageNumber = PageNumber
                qryObjICAmendmentRights.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAmendmentRights.es.PageNumber = 1
                qryObjICAmendmentRights.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If







        End Sub
        Public Shared Function GetAllAmendmentAllowedFieldsByCompanyCodeAndProductTypeCode(ByVal CompanyCode As String, ByVal ProductTypeCode As String, ByVal AllowBulk As Boolean) As ICAmendmentsRightsManagementCollection
            Dim objICAmenmentRightColl As New ICAmendmentsRightsManagementCollection
            objICAmenmentRightColl.es.Connection.CommandTimeout = 3600

            objICAmenmentRightColl.Query.Select(objICAmenmentRightColl.Query.FieldID, objICAmenmentRightColl.Query.CanView, objICAmenmentRightColl.Query.IsAmendmentAllow, objICAmenmentRightColl.Query.IsBulkAmendmentAllow)
            objICAmenmentRightColl.Query.Where(objICAmenmentRightColl.Query.CompanyCode = CompanyCode And objICAmenmentRightColl.Query.ProductTypeCode = ProductTypeCode)
            If AllowBulk = True Then
                objICAmenmentRightColl.Query.Where(objICAmenmentRightColl.Query.IsBulkAmendmentAllow = True)
            ElseIf AllowBulk = False Then
                objICAmenmentRightColl.Query.Where(objICAmenmentRightColl.Query.IsAmendmentAllow = True)
            End If
            objICAmenmentRightColl.Query.Load()
            Return objICAmenmentRightColl
        End Function
    End Class
End Namespace
