﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class ICFlexiFieldsController
        Public Shared Sub AddFlexiField(ByVal objFlexiField As ICFlexiFields, ByVal isUpdate As Boolean, ByVal Action As String, ByVal UserID As String, ByVal UserName As String)
            Dim ICFlexiField As New ICFlexiFields


            ICFlexiField.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICFlexiField.CreatedBy = objFlexiField.CreatedBy
                ICFlexiField.CreatedOn = objFlexiField.CreatedOn
            Else
                ICFlexiField.LoadByPrimaryKey(objFlexiField.FieldID)
            End If
            ICFlexiField.FieldTitle = objFlexiField.FieldTitle
            ICFlexiField.FieldType = objFlexiField.FieldType
            ICFlexiField.ValExp = objFlexiField.ValExp
            ICFlexiField.CompanyCode = objFlexiField.CompanyCode
            ICFlexiField.ListOfValues = objFlexiField.ListOfValues
            ICFlexiField.IsRequired = objFlexiField.IsRequired
            ICFlexiField.IsEnabled = objFlexiField.IsEnabled
            ICFlexiField.FieldOrder = objFlexiField.FieldOrder
            ICFlexiField.FieldLength = objFlexiField.FieldLength
            ICFlexiField.HelpText = objFlexiField.HelpText
            ICFlexiField.DefaultValue = objFlexiField.DefaultValue
            ICFlexiField.IsActive = objFlexiField.IsActive
            ICFlexiField.Save()

            ICUtilities.AddAuditTrail(Action.ToString(), "Flexi Field", ICFlexiField.FieldID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
        End Sub
        Public Shared Sub DeleteFlexiField(ByVal objFlexiField As ICFlexiFields, ByVal Action As String, ByVal UserID As String, ByVal UserName As String)
            Dim ICFlexiField As New ICFlexiFields
            Dim collFlexiField As New ICFlexiFieldsCollection
            ICFlexiField.es.Connection.CommandTimeout = 3600
            ICFlexiField.LoadByPrimaryKey(objFlexiField.FieldID)
            ICFlexiField.MarkAsDeleted()
            ICFlexiField.Save()
            ICFlexiField = New ICFlexiFields
            collFlexiField.Query.Where(collFlexiField.Query.FieldOrder > objFlexiField.FieldOrder)
            If collFlexiField.Query.Load() Then
                For Each ICFlexiField In collFlexiField
                    ICFlexiField.FieldOrder = ICFlexiField.FieldOrder - 1
                    ICFlexiField.Save()
                Next
            End If
            ICUtilities.AddAuditTrail(Action.ToString(), "Flexi Field", objFlexiField.FieldID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
        End Sub
        Public Shared Function GetFlexiFieldsByCompanyCode(ByVal CompanyCode As String) As ICFlexiFieldsCollection
            Dim collICFlexiFields As New ICFlexiFieldsCollection
            collICFlexiFields.es.Connection.CommandTimeout = 3600
            collICFlexiFields.Query.Where(collICFlexiFields.Query.CompanyCode = CompanyCode.ToString())
            collICFlexiFields.Query.OrderBy(collICFlexiFields.Query.FieldOrder.Ascending)
            collICFlexiFields.Query.Load()
            Return collICFlexiFields
        End Function
        Public Shared Sub SetgvFlexiFields(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")
            Dim qryFlexiFields As New ICFlexiFieldsQuery("ff")

            If Not pagenumber = 0 Then

            Else
                qryFlexiFields.es.PageNumber = 1
                qryFlexiFields.es.PageSize = pagesize
            End If


            qryFlexiFields.Select(qryGroup.GroupName, qryCompany.CompanyName, qryFlexiFields.FieldID, qryFlexiFields.FieldTitle, qryFlexiFields.FieldOrder, qryFlexiFields.FieldLength, qryFlexiFields.FieldType, qryFlexiFields.IsRequired, qryFlexiFields.IsEnabled, qryFlexiFields.IsActive)
            qryFlexiFields.InnerJoin(qryCompany).On(qryFlexiFields.CompanyCode = qryCompany.CompanyCode)
            qryFlexiFields.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)

            If Not GroupCode.ToString() = "0" Then
                qryFlexiFields.Where(qryGroup.GroupCode = GroupCode)
            End If
            If Not CompanyCode.ToString() = "0" Then
                qryFlexiFields.Where(qryCompany.CompanyCode = CompanyCode)
            End If
            qryFlexiFields.OrderBy(qryGroup.GroupName.Ascending, qryCompany.CompanyName.Ascending, qryFlexiFields.FieldOrder.Ascending)
            rg.DataSource = qryFlexiFields.LoadDataTable()
            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub
        Public Shared Function GetFlexiFieldOrderByCompanyCode(ByVal CompanyCode As String) As Integer
            Dim objICFlexiFields As New ICFlexiFields
            objICFlexiFields.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            objICFlexiFields.Query.Select(objICFlexiFields.Query.FieldOrder.Max())
            objICFlexiFields.Query.Where(objICFlexiFields.Query.CompanyCode = CompanyCode.ToString())
            objICFlexiFields.Query.OrderBy(objICFlexiFields.Query.FieldOrder.Ascending)
            dt = objICFlexiFields.Query.LoadDataTable()
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item(0).ToString() = "" Then
                    Return 1
                Else
                    Return CInt(dt.Rows(0).Item(0).ToString()) + 1
                End If
            Else
                Return 1
            End If
        End Function
        Public Shared Sub GetFlexiFieldOrderByCompanyCodeForRadGrid(ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
            Dim objICFlexiFieldsColl As New ICFlexiFieldsCollection
            objICFlexiFieldsColl.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            objICFlexiFieldsColl.Query.Select(objICFlexiFieldsColl.Query.FieldTitle, objICFlexiFieldsColl.Query.FieldType.Coalesce("'-'"), objICFlexiFieldsColl.Query.FieldLength.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            objICFlexiFieldsColl.Query.Select(objICFlexiFieldsColl.Query.FieldID)
            objICFlexiFieldsColl.Query.Select(objICFlexiFieldsColl.Query.IsRequired)
            objICFlexiFieldsColl.Query.Where(objICFlexiFieldsColl.Query.CompanyCode = CompanyCode.ToString())
            objICFlexiFieldsColl.Query.OrderBy(objICFlexiFieldsColl.Query.FieldOrder.Ascending)
            dt = objICFlexiFieldsColl.Query.LoadDataTable()

            If Not PageNumber = 0 Then
                objICFlexiFieldsColl.Query.es.PageNumber = PageNumber
                objICFlexiFieldsColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICFlexiFieldsColl.Query.es.PageNumber = 1
                objICFlexiFieldsColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function CheckDuplicate(ByVal cFlexiFields As ICFlexiFields) As Boolean
            Dim rslt As Boolean = False
            Dim collFlexiFields As New ICFlexiFieldsCollection
            Dim collTemplateFields As New ICTemplateFieldsCollection
            collFlexiFields.es.Connection.CommandTimeout = 3600
            collFlexiFields.Query.Select(collFlexiFields.Query.FieldID)
            collFlexiFields.Query.Where(collFlexiFields.Query.FieldTitle.Trim.ToLower = cFlexiFields.FieldTitle.Trim.ToLower)
            If collFlexiFields.Query.Load() Then
                rslt = True
            End If
            collTemplateFields.Query.Select(collTemplateFields.Query.FieldID)
            collTemplateFields.Query.Where(collTemplateFields.Query.FieldName.Trim.ToLower = cFlexiFields.FieldTitle.Trim.ToLower)
            If collTemplateFields.Query.Load() Then
                rslt = True
            End If
            Return rslt
        End Function
    End Class
End Namespace
