﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICLimitsForClearingController

        Public Shared Sub AddClearingLimits(ByVal ClearingLmts As ICLimitsForClearing, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objClearingLmts As New ICLimitsForClearing
            Dim prevobjClearingLmts As New ICLimitsForClearing
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""

            objClearingLmts.es.Connection.CommandTimeout = 3600
            prevobjClearingLmts.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then

                objClearingLmts.CreatedBy = ClearingLmts.CreatedBy
                objClearingLmts.CreatedDate = ClearingLmts.CreatedDate
                objClearingLmts.Creater = ClearingLmts.Creater
                objClearingLmts.CreationDate = ClearingLmts.CreationDate
            Else
                objClearingLmts.LoadByPrimaryKey(ClearingLmts.LimitsForClearingID.ToString())
                prevobjClearingLmts.LoadByPrimaryKey(ClearingLmts.LimitsForClearingID.ToString())
                objClearingLmts.CreatedBy = ClearingLmts.CreatedBy
                objClearingLmts.CreatedDate = ClearingLmts.CreatedDate
            End If

            objClearingLmts.MakerLimits = ClearingLmts.MakerLimits.ToString()
            objClearingLmts.ApprovalLimits = ClearingLmts.ApprovalLimits.ToString()
            objClearingLmts.UserID = ClearingLmts.UserID.ToString()
            objClearingLmts.Save()

            If (isUpdate = False) Then
                CurrentAt = "User Clearing Limits : [Code:  " & objClearingLmts.LimitsForClearingID.ToString() & " ; UserID:  " & objClearingLmts.UserID.ToString() & " ; Maker Limit:  " & objClearingLmts.MakerLimits.ToString() & " ; Approval Limit:  " & objClearingLmts.ApprovalLimits.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Clearing Limits", objClearingLmts.LimitsForClearingID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else

                CurrentAt = "User Clearing Limits : Current Values [Code:  " & objClearingLmts.LimitsForClearingID.ToString() & " ; UserID:  " & objClearingLmts.UserID.ToString() & " ; Maker Limit:  " & objClearingLmts.MakerLimits.ToString() & " ; Approval Limit:  " & objClearingLmts.ApprovalLimits.ToString() & "]"
                PrevAt = "<br />User Clearing Limits : Previous Values [Code:  " & prevobjClearingLmts.LimitsForClearingID.ToString() & " ; UserID:  " & prevobjClearingLmts.UserID.ToString() & " ; Maker Limit:  " & prevobjClearingLmts.MakerLimits.ToString() & " ; Approval Limit:  " & prevobjClearingLmts.ApprovalLimits.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Clearing Limits", objClearingLmts.LimitsForClearingID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub

        Public Shared Sub DeleteAllRecordsByUserID(ByVal UserID As String)
            Dim collClearingLmts As New ICLimitsForClearingCollection

            collClearingLmts.Query.Where(collClearingLmts.Query.UserID = UserID.ToString())
            If collClearingLmts.Query.Load() Then
                collClearingLmts.MarkAllAsDeleted()
                collClearingLmts.Save()
            End If

        End Sub
        Public Shared Sub DeleteClearingLimits(ByVal ClearingLmts As String, ByVal UserID As String, ByVal UserName As String)
            Dim objClearingLmts As New ICLimitsForClearing
            Dim prevobjClearingLmts As New ICLimitsForClearing
            Dim CurrentAt As String = ""
            objClearingLmts.es.Connection.CommandTimeout = 3600
            prevobjClearingLmts.es.Connection.CommandTimeout = 3600

            objClearingLmts.LoadByPrimaryKey(ClearingLmts)
            prevobjClearingLmts.LoadByPrimaryKey(ClearingLmts)

            CurrentAt = "Clearing Limits: [Code:  " & objClearingLmts.LimitsForClearingID.ToString() & " ; UserID:  " & objClearingLmts.UserID.ToString() & " ; Maker Limit:  " & objClearingLmts.MakerLimits.ToString() & " ; Approval Limit:  " & objClearingLmts.ApprovalLimits.ToString() & "] "

            objClearingLmts.MarkAsDeleted()
            objClearingLmts.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Clearing Limits", prevobjClearingLmts.LimitsForClearingID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub GetClearingLmtgv(ByVal OfficeCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            If Not pagenumber = 0 Then
                Dim qryBankBranch As New ICOfficeQuery("qryBankBranch ")
                Dim qryUser As New ICUserQuery("qryUser")
                Dim qryClearingLmts As New ICLimitsForClearingQuery("qryClearingLmts")
                Dim dt As DataTable

                qryClearingLmts.Select(qryClearingLmts.LimitsForClearingID, qryBankBranch.OfficeName, qryClearingLmts.MakerLimits, qryClearingLmts.ApprovalLimits, qryUser.UserName, qryUser.DisplayName, qryUser.UserType)
                qryClearingLmts.InnerJoin(qryUser).On(qryUser.UserID = qryClearingLmts.UserID)
                qryClearingLmts.InnerJoin(qryBankBranch).On(qryBankBranch.OfficeID = qryUser.OfficeCode)
                qryClearingLmts.Where(qryUser.UserType = "Bank User" And qryUser.OfficeCode = OfficeCode)

                dt = qryClearingLmts.LoadDataTable()
                rg.DataSource = dt
                If DoDataBind Then
                    rg.DataBind()
                End If
            End If
        End Sub

        'Public Shared Sub DeleteAllRecordsByUserID(ByVal UserID As String)
        '    Dim collClearingLmts As New ICLimitsForClearingCollection

        '    collClearingLmts.Query.Where(collClearingLmts.Query.UserID = UserID.ToString())
        '    If collClearingLmts.Query.Load() Then
        '        collClearingLmts.MarkAllAsDeleted()
        '        collClearingLmts.Save()
        '    End If

        'End Sub

        ' Aizaz Ahmed Work [Dated: 19-Feb-2013]
        Public Shared Function GetClearingLimitsByUserIDAndLimitType(ByVal UserID As String, ByVal LimitType As String) As Double
            Dim Limit As Double = 0
            Dim objLimitsForClearing As New ICLimitsForClearing

            objLimitsForClearing.Query.Where(objLimitsForClearing.Query.UserID = UserID.ToString())
            If objLimitsForClearing.Query.Load() Then
                If LimitType.ToString() = "Maker" Then
                    Limit = objLimitsForClearing.MakerLimits
                ElseIf LimitType.ToString() = "Approver" Then
                    Limit = objLimitsForClearing.ApprovalLimits
                End If
            Else
                Limit = 0
            End If
            Return Limit
        End Function
    End Class
End Namespace

