﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICUserLimitsController
        Public Shared Sub AddICLimitsForApproval(ByVal objICLimitsForApproval As ICLimitForApproval, ByVal UsersID As String, ByVal UsersName As String, ByVal IsUpdate As Boolean, ByVal ActionString As String)
            Dim objICLimitsForApprovalSave As New ICLimitForApproval
            objICLimitsForApprovalSave.es.Connection.CommandTimeout = 3600


            If IsUpdate = False Then
                objICLimitsForApprovalSave.CreatedBy = objICLimitsForApproval.CreatedBy
                objICLimitsForApprovalSave.CreatedDate = objICLimitsForApproval.CreatedDate
                objICLimitsForApprovalSave.Creater = objICLimitsForApproval.Creater
                objICLimitsForApprovalSave.CreationDate = objICLimitsForApproval.CreationDate
            Else
                objICLimitsForApprovalSave.LoadByPrimaryKey(objICLimitsForApproval.LimitsForApprovalID)
                objICLimitsForApprovalSave.CreatedBy = objICLimitsForApproval.CreatedBy
                objICLimitsForApprovalSave.CreatedDate = objICLimitsForApproval.CreatedDate
            End If
            objICLimitsForApprovalSave.AccountNumber = objICLimitsForApproval.AccountNumber
            objICLimitsForApprovalSave.BranchCode = objICLimitsForApproval.BranchCode
            objICLimitsForApprovalSave.Currency = objICLimitsForApproval.Currency
            objICLimitsForApprovalSave.UserID = objICLimitsForApproval.UserID
            objICLimitsForApprovalSave.LimitsForPrimaryApproval = objICLimitsForApproval.LimitsForPrimaryApproval
            objICLimitsForApprovalSave.LimitsForSecondaryApproval = objICLimitsForApproval.LimitsForSecondaryApproval
            objICLimitsForApprovalSave.IsPrimaryApprover = objICLimitsForApproval.IsPrimaryApprover
            objICLimitsForApprovalSave.IsSecondaryApprover = objICLimitsForApproval.IsSecondaryApprover
            objICLimitsForApprovalSave.UserSignatureImage = objICLimitsForApproval.UserSignatureImage
            objICLimitsForApprovalSave.FileSize = objICLimitsForApproval.FileSize
            objICLimitsForApprovalSave.FileType = objICLimitsForApproval.FileType

            objICLimitsForApprovalSave.BankPrimaryApprovalLimit = objICLimitsForApproval.BankPrimaryApprovalLimit
            objICLimitsForApprovalSave.BankSecondaryApprovalLimit = objICLimitsForApproval.BankSecondaryApprovalLimit


            objICLimitsForApprovalSave.Save()
            If IsUpdate = False Then
                'ICUtilities.AddAuditTrail("Limits for approval of user : " & objICLimitsForApprovalSave.UpToICUserByUserID.DisplayName & " against account number : " & objICLimitsForApprovalSave.AccountNumber & " of company : " & objICLimitsForApprovalSave.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.CompanyName & " added.", "Limits For Approval", objICLimitsForApprovalSave.LimitsForApprovalID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
                ICUtilities.AddAuditTrail(ActionString, "Limits For Approval", objICLimitsForApprovalSave.LimitsForApprovalID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            Else
                ICUtilities.AddAuditTrail(ActionString, "Limits For Approval", objICLimitsForApprovalSave.LimitsForApprovalID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


        End Sub
        Public Shared Sub GetAllUSerLimitsDefinedAgainstUSerForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCde As String, ByVal CompanyCode As String, ByVal UserType As String)


            Dim qryObjICUserLimits As New ICLimitForApprovalQuery("qryObjICUserLimits")
            'Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            'Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            'Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim dt As New DataTable


            qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForApprovalID, qryObjICUserLimits.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.UserType, qryObjICUserLimits.AccountNumber.Coalesce("'-'"))
            qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForPrimaryApproval)
            qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForSecondaryApproval)
            qryObjICUserLimits.Select(qryObjICUserLimits.BankPrimaryApprovalLimit)
            qryObjICUserLimits.Select(qryObjICUserLimits.BankSecondaryApprovalLimit)



            'qryObjICUserLimits.LeftJoin(qryObjICAccounts).On(qryObjICUserLimits.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserLimits.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserLimits.Currency = qryObjICAccounts.Currency)

            'qryObjICUserLimits.LeftJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            'qryObjICUserLimits.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUserLimits.LeftJoin(qryObjICUser).On(qryObjICUserLimits.UserID = qryObjICUser.UserID)
            'qryObjICUserLimits.LeftJoin(qryObjICAccounts).On(qryObjICUserLimits.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserLimits.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserLimits.Currency = qryObjICAccounts.Currency)

            'qryObjICUserLimits.LeftJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            'qryObjICUserLimits.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            'qryObjICUserLimits.GroupBy(qryObjICUserLimits.LimitsForApprovalID, qryObjICUserLimits.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.UserType, qryObjICCompany.CompanyName, qryObjICUserLimits.AccountNumber)
            'qryObjICUserLimits.GroupBy(qryObjICUserLimits.LimitsForPrimaryApproval, qryObjICUserLimits.LimitsForSecondaryApproval, qryObjICUserLimits.BankPrimaryApprovalLimit, qryObjICUserLimits.BankSecondaryApprovalLimit)

            qryObjICUserLimits.OrderBy(qryObjICUserLimits.LimitsForApprovalID.Ascending)

            'If Not GroupCde = "" Then
            '    qryObjICUserLimits.Where(qryObjICGroup.GroupCode = GroupCde.ToString)
            'End If
            'If Not CompanyCode = "" Then
            '    qryObjICUserLimits.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            'End If
            If Not UserType = "" Then
                qryObjICUserLimits.Where(qryObjICUser.UserType = UserType.ToString)
            End If



            dt = qryObjICUserLimits.LoadDataTable

            If Not PageNumber = 0 Then
                qryObjICUserLimits.es.PageNumber = PageNumber
                qryObjICUserLimits.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICUserLimits.es.PageNumber = 1
                qryObjICUserLimits.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If




        End Sub
        ''Before Farhan modified
        'Public Shared Sub GetAllUSerLimitsDefinedAgainstUSerForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCde As String, ByVal CompanyCode As String, ByVal UserType As String)




        '    Dim qryObjICUserLimits As New ICLimitForApprovalQuery("qryObjICUserLimits")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim qryObjICUser As New ICUserQuery("qryObjICUser")
        '    Dim dt As New DataTable


        '    qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForApprovalID, qryObjICUserLimits.UserID, qryObjICUser.UserName, qryObjICUser.UserType, qryObjICCompany.CompanyName.Coalesce("'-'"), qryObjICUserLimits.AccountNumber.Coalesce("'-'"))
        '    qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForPrimaryApproval)
        '    qryObjICUserLimits.Select(qryObjICUserLimits.LimitsForSecondaryApproval)
        '    qryObjICUserLimits.Select(qryObjICUserLimits.BankPrimaryApprovalLimit)
        '    qryObjICUserLimits.Select(qryObjICUserLimits.BankSecondaryApprovalLimit)



        '    qryObjICUserLimits.LeftJoin(qryObjICAccounts).On(qryObjICUserLimits.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserLimits.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserLimits.Currency = qryObjICAccounts.Currency)

        '    qryObjICUserLimits.LeftJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICUserLimits.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICUserLimits.LeftJoin(qryObjICUser).On(qryObjICUserLimits.UserID = qryObjICUser.UserID)
        '    'qryObjICUserLimits.LeftJoin(qryObjICAccounts).On(qryObjICUserLimits.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserLimits.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserLimits.Currency = qryObjICAccounts.Currency)

        '    'qryObjICUserLimits.LeftJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    'qryObjICUserLimits.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICUserLimits.GroupBy(qryObjICUserLimits.LimitsForApprovalID, qryObjICUserLimits.UserID, qryObjICUser.UserName, qryObjICUser.UserType, qryObjICCompany.CompanyName, qryObjICUserLimits.AccountNumber)
        '    qryObjICUserLimits.GroupBy(qryObjICUserLimits.LimitsForPrimaryApproval, qryObjICUserLimits.LimitsForSecondaryApproval, qryObjICUserLimits.BankPrimaryApprovalLimit, qryObjICUserLimits.BankSecondaryApprovalLimit)

        '    qryObjICUserLimits.OrderBy(qryObjICUserLimits.AccountNumber.Ascending)

        '    If Not GroupCde = "" Then
        '        qryObjICUserLimits.Where(qryObjICGroup.GroupCode = GroupCde.ToString)
        '    End If
        '    If Not CompanyCode = "" Then
        '        qryObjICUserLimits.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If Not UserType = "" Then
        '        qryObjICUserLimits.Where(qryObjICUser.UserType = UserType.ToString)
        '    End If

        '    qryObjICUserLimits.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)

        '    dt = qryObjICUserLimits.LoadDataTable

        '    If Not PageNumber = 0 Then
        '        qryObjICUserLimits.es.PageNumber = PageNumber
        '        qryObjICUserLimits.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICUserLimits.es.PageNumber = 1
        '        qryObjICUserLimits.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If




        'End Sub
        Public Shared Sub DeleteICUserLimt(ByVal objICUserLimit As ICLimitForApproval, ByVal UsersID As String, ByVal UsersName As String, ByVal StringAction As String)
            Dim objICLimitOfApprovalForDelete As New ICLimitForApproval
            objICLimitOfApprovalForDelete.es.Connection.CommandTimeout = 3600

            If objICLimitOfApprovalForDelete.LoadByPrimaryKey(objICUserLimit.LimitsForApprovalID) Then
                objICLimitOfApprovalForDelete.MarkAsDeleted()
                objICLimitOfApprovalForDelete.Save()
                ICUtilities.AddAuditTrail(StringAction, "Limits For Approval", objICUserLimit.LimitsForApprovalID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub
        Public Shared Function GetAllLimitsForApprovalByUserID(ByVal UserID As String) As ICLimitForApprovalCollection
            Dim objICLimitsForAppColl As New ICLimitForApprovalCollection
            objICLimitsForAppColl.es.Connection.CommandTimeout = 3600
            objICLimitsForAppColl.Query.Where(objICLimitsForAppColl.Query.UserID = UserID.ToString)
            objICLimitsForAppColl.Query.OrderBy(objICLimitsForAppColl.Query.LimitsForApprovalID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICLimitsForAppColl.Query.Load()
            Return objICLimitsForAppColl
        End Function
        Public Shared Function GetSingleObjectLimitsForApprovalByUserID(ByVal UserID As String) As ICLimitForApproval
            Dim objICLimitsForAppColl As New ICLimitForApprovalCollection
            objICLimitsForAppColl.es.Connection.CommandTimeout = 3600
            objICLimitsForAppColl.Query.Where(objICLimitsForAppColl.Query.UserID = UserID.ToString)
            objICLimitsForAppColl.Query.OrderBy(objICLimitsForAppColl.Query.LimitsForApprovalID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICLimitsForAppColl.Query.Load()
            Return objICLimitsForAppColl(0)
        End Function
        Public Shared Sub BindRadGridForAccountsByGroupCode(ByVal GroupCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
            Dim objICAccnts As New ICAccountsQuery("qryICAccnts")
            Dim objICCompany As New ICCompanyQuery("qryICCompany")
            Dim dt As New DataTable
            objICAccnts.Select(objICCompany.CompanyName, objICAccnts.AccountNumber, objICAccnts.BranchCode, objICAccnts.Currency)
            objICAccnts.InnerJoin(objICCompany).On(objICAccnts.CompanyCode = objICCompany.CompanyCode)
            objICAccnts.Where(objICCompany.GroupCode = GroupCode And objICAccnts.IsActive = True And objICAccnts.IsApproved = True And objICAccnts.IsActive = True)
            dt = objICAccnts.LoadDataTable
            If Not PageNumber = 0 Then
                objICAccnts.es.PageNumber = PageNumber
                objICAccnts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICAccnts.es.PageNumber = 1
                objICAccnts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetUserLimitByAccountNumberAndUserID(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal UserID As String, ByVal PrimaryLimitType As Boolean) As Double
            Dim qryICUserLimit As New ICLimitForApprovalQuery("qryICUserLimit")
            Dim Amount As Double = 0
            Dim dt As New DataTable
            If PrimaryLimitType = True Then
                qryICUserLimit.Select(qryICUserLimit.LimitsForPrimaryApproval.Coalesce("'0'"))
            ElseIf PrimaryLimitType = False Then
                qryICUserLimit.Select(qryICUserLimit.LimitsForSecondaryApproval.Coalesce("'0'"))
            End If
            qryICUserLimit.Where(qryICUserLimit.AccountNumber = AccountNumber And qryICUserLimit.BranchCode = BranchCode And qryICUserLimit.Currency = Currency)
            qryICUserLimit.Where(qryICUserLimit.UserID = UserID)
            qryICUserLimit.Load()
            dt = qryICUserLimit.LoadDataTable
            If dt.Rows.Count > 0 Then
                Amount = CDbl(dt.Rows(0)(0))
            End If
            Return Amount
        End Function
        ' Aizaz Ahmed Work [Dated: 05-Mar-2013]
        Public Shared Function GetSignaturesOfAccount(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable
            Dim qryICUserLimit As New ICLimitForApprovalQuery("ul")
            Dim qryICUsers As New ICUserQuery("usr")

            qryICUserLimit.Select(qryICUserLimit.LimitsForApprovalID, qryICUserLimit.UserID, qryICUsers.DisplayName)
            qryICUserLimit.InnerJoin(qryICUsers).On(qryICUserLimit.UserID = qryICUsers.UserID)
            qryICUserLimit.Where(qryICUserLimit.AccountNumber = AccountNumber.ToString() And qryICUserLimit.BranchCode = BranchCode.ToString() And qryICUserLimit.Currency = Currency.ToString())

            
            Return qryICUserLimit.LoadDataTable()
        End Function
        Public Shared Function GetSignaturesOfAccountByApproverIDs(ByVal ArrayListUserIDS As ArrayList) As DataTable
            Dim qryICUserLimit As New ICLimitForApprovalQuery("ul")
            Dim qryICUsers As New ICUserQuery("usr")

            qryICUserLimit.Select(qryICUserLimit.UserID, qryICUsers.DisplayName, qryICUsers.UserName)
            qryICUserLimit.InnerJoin(qryICUsers).On(qryICUserLimit.UserID = qryICUsers.UserID)
            qryICUserLimit.Where(qryICUserLimit.UserID.In(ArrayListUserIDS))
            'qryICUserLimit.Where(qryICUserLimit.UserID.Distinct = True)

            qryICUserLimit.es.Distinct = True
            Return qryICUserLimit.LoadDataTable()
        End Function
        Public Shared Function GetBankUserPrimaryOrSecondaryApprovalLimitByUserID(ByVal UserID As String, ByVal PrimaryLimit As Boolean) As Double
            Dim qryObjICLimitsForApproval As New ICLimitForApprovalQuery("objICLimitsForApprovalQuery")
            Dim dt As New DataTable
            If PrimaryLimit = True Then
                qryObjICLimitsForApproval.Select(qryObjICLimitsForApproval.BankPrimaryApprovalLimit.Case.When(qryObjICLimitsForApproval.BankPrimaryApprovalLimit.IsNull).Then(0).Else(qryObjICLimitsForApproval.BankPrimaryApprovalLimit).End.As("Limit"))
                qryObjICLimitsForApproval.Where(qryObjICLimitsForApproval.BankPrimaryApprovalLimit.IsNotNull)
            ElseIf PrimaryLimit = False Then
                qryObjICLimitsForApproval.Select(qryObjICLimitsForApproval.BankSecondaryApprovalLimit.Case.When(qryObjICLimitsForApproval.BankSecondaryApprovalLimit.IsNull).Then(0).Else(qryObjICLimitsForApproval.BankSecondaryApprovalLimit).End.As("Limit"))
                qryObjICLimitsForApproval.Where(qryObjICLimitsForApproval.BankSecondaryApprovalLimit.IsNotNull)
            End If
            qryObjICLimitsForApproval.Where(qryObjICLimitsForApproval.UserID = UserID)
            qryObjICLimitsForApproval.OrderBy(qryObjICLimitsForApproval.LimitsForApprovalID.Ascending)
            dt = qryObjICLimitsForApproval.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return CDbl(dt(0)(0))
            Else
                Return 0
            End If
        End Function
        Public Shared Function GetBankApprovalLimitsByUserID(ByVal UserID As String, ByVal LimitType As String) As Double
            Dim qryObjICLimitsForApproval As New ICLimitForApprovalQuery("qryObjICLimitsForApproval")
            Dim ApprovalAmount As Double = 0
            Dim dt As New DataTable

            If LimitType = "Primary" Then
                qryObjICLimitsForApproval.Select(qryObjICLimitsForApproval.BankPrimaryApprovalLimit.Coalesce("'0'"))
            ElseIf LimitType = "Secondary" Then
                qryObjICLimitsForApproval.Select(qryObjICLimitsForApproval.BankSecondaryApprovalLimit.Coalesce("'0'"))
            End If
            qryObjICLimitsForApproval.Where(qryObjICLimitsForApproval.UserID = UserID)
            qryObjICLimitsForApproval.OrderBy(qryObjICLimitsForApproval.LimitsForApprovalID.Ascending)
            dt = qryObjICLimitsForApproval.LoadDataTable
            Return CDbl(dt(0)(0))

        End Function
        Public Shared Function GetApprovalLimitIDByUserID(ByVal UserID As String) As Integer
            Dim qryObjICLimitsForApproval As New ICLimitForApprovalQuery("qryObjICLimitsForApproval")
            qryObjICLimitsForApproval.Select(qryObjICLimitsForApproval.LimitsForApprovalID)
            qryObjICLimitsForApproval.Where(qryObjICLimitsForApproval.UserID = UserID)
            qryObjICLimitsForApproval.es.Top = 1
            Return CInt(qryObjICLimitsForApproval.LoadDataTable(0)(0))
        End Function
    End Class
End Namespace
