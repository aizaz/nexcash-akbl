﻿Namespace IC
    Public Class ICApprovalStageController
        Public Shared Function GetAllApprovalStages() As ICApprovalStageCollection
            Dim collICApprovalStage As New ICApprovalStageCollection
            collICApprovalStage.es.Connection.CommandTimeout = 3600
            collICApprovalStage.Query.OrderBy(collICApprovalStage.Query.ApprovalStagesID.Ascending)
            collICApprovalStage.Query.Load()
            Return collICApprovalStage
        End Function

    End Class
End Namespace

