﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class ICWorkingDaysController
        Public Shared Sub AddWorkingDay(ByVal cWorkingDay As IC.ICWorkingDays, ByVal UserID As String, ByVal UserName As String)

            Dim objWorkingDay As New IC.ICWorkingDays
            Dim strAction As String = ""
            objWorkingDay.DayName = cWorkingDay.DayName
            objWorkingDay.OrderDay = cWorkingDay.OrderDay
            objWorkingDay.Save()

            strAction = "Working Day : [Name: " & objWorkingDay.DayName.ToString() & "]"
            ICUtilities.AddAuditTrail(strAction & " Added ", "Working Day", objWorkingDay.DayName.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
        End Sub

        Public Shared Function DeleteWorkingDay(ByVal WorkingDayName As String, ByVal UserID As String, ByVal UserName As String) As Boolean

            Dim objWorkingDay As New IC.ICWorkingDays
            Dim strAction As String = ""

            If objWorkingDay.LoadByPrimaryKey(WorkingDayName.ToString()) Then
                strAction = "Working Day : [Name: " & objWorkingDay.DayName.ToString() & "]"
                objWorkingDay.MarkAsDeleted()
                objWorkingDay.Save()
                ICUtilities.AddAuditTrail(strAction & " Deleted ", "Working Day", WorkingDayName.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsWorkingDay(ByVal DayName As String) As Boolean
            Dim qryWorkingDays As New ICWorkingDays
            qryWorkingDays.Query.Where(qryWorkingDays.Query.DayName.ToLower = DayName.ToLower)
            If qryWorkingDays.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function GetWorkingDays() As DataTable
            Dim qryWorkingDays As New ICWorkingDaysQuery
            qryWorkingDays.OrderBy(qryWorkingDays.OrderDay.Ascending)
            Return qryWorkingDays.LoadDataTable()
        End Function
        Public Shared Function GetWorkingDayForValueDate(ByVal ReqDate As Date) As Date
            Dim qryWorkingDays As New ICWorkingDaysQuery("qryWorkingDays")

            qryWorkingDays.Where(qryWorkingDays.DayName.ToLower = ReqDate.ToString("dddd").ToLower)
            If qryWorkingDays.Load Then
                GetWorkingDayForValueDate(ReqDate.Date.AddDays(1).Date)
            Else
                Return ReqDate
            End If
            Return ReqDate
        End Function
    End Class
End Namespace

