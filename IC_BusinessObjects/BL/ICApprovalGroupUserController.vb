﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICApprovalGroupUserController
        Public Shared Sub AddApprovalGroupUsers(ByVal objICAppGroupUsers As ICApprovalGroupUsers, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objICAppGroupUsersSave As New ICApprovalGroupUsers

            

            If (isUpdate = False) Then
                objICAppGroupUsersSave.CreatedBy = objICAppGroupUsers.CreatedBy
                objICAppGroupUsersSave.CreatedDate = objICAppGroupUsers.CreatedDate
                objICAppGroupUsersSave.Creator = objICAppGroupUsers.Creator
                objICAppGroupUsersSave.CreationDate = objICAppGroupUsers.CreationDate
            Else
                objICAppGroupUsersSave.LoadByPrimaryKey(objICAppGroupUsers.UserApprovalGroupID)

                objICAppGroupUsersSave.CreatedBy = objICAppGroupUsers.CreatedBy
                objICAppGroupUsersSave.CreatedDate = objICAppGroupUsers.CreatedDate
            End If
            objICAppGroupUsersSave.ApprovalGroupID = objICAppGroupUsers.ApprovalGroupID
            objICAppGroupUsersSave.UserID = objICAppGroupUsers.UserID


            objICAppGroupUsersSave.Save()


            CurrentAt = "User [ " & objICAppGroupUsers.UserID & " ] [ " & objICAppGroupUsers.UpToICUserByUserID.UserName & " ]  for Approval Group"
            CurrentAt += " [ " & objICAppGroupUsers.ApprovalGroupID & " ] [ " & objICAppGroupUsers.UpToICApprovalGroupManagementByApprovalGroupID.GroupName & " ]"
            CurrentAt += " added."
            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Tag User Approval Group", objICAppGroupUsersSave.UserApprovalGroupID, UserID.ToString(), UserName.ToString(), "ADD")



        End Sub
        Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)


            Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICApprovalGroupUsers As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUsers")
            Dim dt As New DataTable



            qryObjICApprovalGroupUsers.Select(qryObjICApprovalGroup.GroupName, qryObjICCompany.CompanyName, qryObjICApprovalGroup.IsActive)
            qryObjICApprovalGroupUsers.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUsers.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
            qryObjICApprovalGroupUsers.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICApprovalGroupUsers.Where(qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICCompany.IsPaymentsAllowed = True)
            qryObjICApprovalGroupUsers.Where(qryObjICApprovalGroup.IsActive = True)

            qryObjICApprovalGroupUsers.es.Distinct = Tru
            dt = qryObjICApprovalGroupUsers.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICApprovalGroup.es.PageNumber = PageNumber
                qryObjICApprovalGroup.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICApprovalGroup.es.PageNumber = 1
                qryObjICApprovalGroup.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        Public Shared Function GetAllUsersByCompanyCodeForTaggingAccountBalance(ByVal CompanyCode As String, ByVal IsUpdate As Boolean, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryUser")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICTaggedUser As New ICTaggedClientUserForAccountQuery("qryObjICTaggedUser")
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
            qryObjICUser.InnerJoin(qryObjICOffice).On(qryObjICUser.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            'qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

            qryObjICUser.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            If IsUpdate = True Then
                qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICTaggedUser.[Select](qryObjICTaggedUser.UserID).Where(qryObjICTaggedUser.AccountNumber = AccountNo And qryObjICTaggedUser.BranchCode = BranchCode And qryObjICTaggedUser.Currency = Currency)))
            End If
            dt = qryObjICUser.LoadDataTable()
            Return dt

        End Function
        Public Shared Function GetApprovalGroupTaggedUserByApprovalGroupID(ByVal ApprovalGroupID As String) As ICApprovalGroupUsersCollection
            Dim collObjICApprovalGroupUser As New ICApprovalGroupUsersCollection



            collObjICApprovalGroupUser.Query.Where(collObjICApprovalGroupUser.Query.ApprovalGroupID = ApprovalGroupID)
            collObjICApprovalGroupUser.Query.OrderBy(collObjICApprovalGroupUser.Query.UserApprovalGroupID.Ascending)
            collObjICApprovalGroupUser.Query.Load()
            Return collObjICApprovalGroupUser

        End Function
        Public Shared Function GetApprovalGroupTaggedUserByNotInCondition(ByVal ApprovalGroupID As String, ByVal CondtionType As String, ByVal ApprovalRuleID As String) As ICApprovalGroupUsersCollection
            Dim collObjICApprovalGroupUser As New ICApprovalGroupUsersCollection
            Dim qryObjICAppGroupCondUSer As New ICApprovalRuleConditionUsersQuery("qryObjICAppGroupCondUSer")
            Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRule")


            qryObjICAppGroupCondUSer.Select(qryObjICAppGroupCondUSer.UserID)
            qryObjICAppGroupCondUSer.InnerJoin(qryObjICAppRuleCond).On(qryObjICAppGroupCondUSer.ApprovaRuleCondID = qryObjICAppRuleCond.ApprovalRuleConditionID)
            qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ConditionType = CondtionType And qryObjICAppRuleCond.ApprovalGroupID = ApprovalGroupID)
            qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID)


            collObjICApprovalGroupUser.Query.Where(collObjICApprovalGroupUser.Query.ApprovalGroupID = ApprovalGroupID)
            collObjICApprovalGroupUser.Query.Where(collObjICApprovalGroupUser.Query.UserID.NotIn(qryObjICAppGroupCondUSer))
            collObjICApprovalGroupUser.Query.OrderBy(collObjICApprovalGroupUser.Query.UserApprovalGroupID.Ascending)

            collObjICApprovalGroupUser.Query.Load()
            Return collObjICApprovalGroupUser

        End Function
        Public Shared Function GetApprovalGroupTaggedUserByApprovalGroupIDDataTable(ByVal ApprovalGroupID As String) As DataTable
            Dim qryObjICApprovalGroup As New ICApprovalGroupUsersQuery("qryObjICApprovalGroup")
            Dim qryObjICUSer As New ICUserQuery("qryObjICUSer")

            qryObjICApprovalGroup.Select(qryObjICUSer.UserID, (qryObjICUSer.UserName + "-" + qryObjICUSer.DisplayName).As("UserName"))
            qryObjICApprovalGroup.InnerJoin(qryObjICUSer).On(qryObjICApprovalGroup.UserID = qryObjICUSer.UserID)
            qryObjICApprovalGroup.Where(qryObjICApprovalGroup.ApprovalGroupID = ApprovalGroupID)
            qryObjICApprovalGroup.OrderBy(qryObjICUSer.UserName.Ascending)
            Return qryObjICApprovalGroup.LoadDataTable

        End Function


        Public Shared Sub GetTaggedApprovalGroupUserForRadGrid(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal ApprovalGroupID As String)
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryObjICApprovalGroupUser As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUser")

            Dim dt As New DataTable

            qryObjICApprovalGroupUser.Select(qryObjICApprovalGroupUser.UserApprovalGroupID, qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICApprovalGroupUser.ApprovalGroupID, qryObjICUser.UserType)
            qryObjICApprovalGroupUser.InnerJoin(qryObjICUser).On(qryObjICApprovalGroupUser.UserID = qryObjICUser.UserID)
            qryObjICApprovalGroupUser.OrderBy(qryObjICUser.UserType.Descending)
            qryObjICApprovalGroupUser.Where(qryObjICApprovalGroupUser.ApprovalGroupID = ApprovalGroupID)
            dt = qryObjICApprovalGroupUser.LoadDataTable()

            If Not pagenumber = 0 Then
                qryObjICApprovalGroupUser.es.PageNumber = pagenumber

                qryObjICApprovalGroupUser.es.PageSize = pagesize


                rg.DataSource = dt


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else
                qryObjICApprovalGroupUser.es.PageNumber = 1
                qryObjICApprovalGroupUser.es.PageSize = pagesize


                rg.DataSource = dt

                If DoDataBind Then


                    rg.DataBind()

                End If
            End If
        End Sub

        Public Shared Function GetUntaggedUserByUserTypeAndCompanyCodeAndApprovalGroupCode(ByVal UserType As String, ByVal ApprovalGroupID As String, ByVal OfficeCode As String, ByVal GroupCode As String) As DataTable

            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryObjICApprovalGroupUser As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUser")
            Dim qryObjICApprovalGroupUser2 As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUser")
            Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            'Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim UserID As New ArrayList
            Dim dt As New DataTable

            qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"), qryObjICUser.DisplayName)


            If UserType = "Client User" Then
                qryObjICApprovalGroupUser.Select(qryObjICApprovalGroupUser.UserID)
                qryObjICApprovalGroupUser.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUser.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
                qryObjICApprovalGroupUser.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
                qryObjICApprovalGroupUser.Where(qryObjICCompany.GroupCode = GroupCode)
                dt = qryObjICApprovalGroupUser.LoadDataTable
            Else
                qryObjICApprovalGroupUser.Select(qryObjICApprovalGroupUser.UserID)
                qryObjICApprovalGroupUser.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUser.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
                qryObjICApprovalGroupUser.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
                qryObjICApprovalGroupUser.Where(qryObjICCompany.GroupCode = GroupCode And qryObjICApprovalGroup.ApprovalGroupID = ApprovalGroupID)
                dt = qryObjICApprovalGroupUser.LoadDataTable
            End If

            qryObjICUser.Where(qryObjICUser.UserType = UserType And qryObjICUser.OfficeCode = OfficeCode And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    UserID.Add(dr("UserID"))
                Next
            Else

                UserID.Add("0")

            End If
            qryObjICUser.Where(qryObjICUser.UserID.NotIn(UserID))

            Return qryObjICUser.LoadDataTable
        End Function
        ''Before Farhan modified 25052015
        'Public Shared Function GetUntaggedUserByUserTypeAndCompanyCodeAndApprovalGroupCode(ByVal UserType As String, ByVal ApprovalGroupID As String, ByVal OfficeCode As String, ByVal GroupCode As String) As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryObjICUser")
        '    Dim qryObjICApprovalGroupUser As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUser")
        '    Dim qryObjICApprovalGroupUser2 As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUser")
        '    Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    'Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim UserID As New ArrayList
        '    Dim dt As New DataTable
        '    qryObjICUser.Select(qryObjICUser.UserID, (qryObjICUser.UserID.Cast(EntitySpaces.DynamicQuery.esCastType.String) + "-" + qryObjICUser.UserName).As("UserName"), qryObjICUser.DisplayName)


        '    If UserType = "Client User" Then
        '        qryObjICApprovalGroupUser.Select(qryObjICApprovalGroupUser.UserID)
        '        qryObjICApprovalGroupUser.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUser.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
        '        qryObjICApprovalGroupUser.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
        '        qryObjICApprovalGroupUser.Where(qryObjICCompany.GroupCode = GroupCode)
        '        dt = qryObjICApprovalGroupUser.LoadDataTable
        '    Else
        '        qryObjICApprovalGroupUser.Select(qryObjICApprovalGroupUser.UserID)
        '        qryObjICApprovalGroupUser.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUser.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
        '        qryObjICApprovalGroupUser.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
        '        qryObjICApprovalGroupUser.Where(qryObjICCompany.GroupCode = GroupCode And qryObjICApprovalGroup.ApprovalGroupID = ApprovalGroupID)
        '        dt = qryObjICApprovalGroupUser.LoadDataTable
        '    End If

        '    qryObjICUser.Where(qryObjICUser.UserType = UserType And qryObjICUser.OfficeCode = OfficeCode And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)
        '    If dt.Rows.Count > 0 Then
        '        For Each dr As DataRow In dt.Rows
        '            UserID.Add(dr("UserID"))
        '        Next
        '    Else

        '        UserID.Add("0")

        '    End If
        '    qryObjICUser.Where(qryObjICUser.UserID.NotIn(UserID))

        '    Return qryObjICUser.LoadDataTable
        'End Function
        Public Shared Sub DeleteTaggedApprovalGroupUser(ByVal TaggesUserID As String, ByVal UserID As String, ByVal UserName As String)

            Dim objICTaggedApprovalGroupUser As New ICApprovalGroupUsers
            Dim StrAction As String = ""


            objICTaggedApprovalGroupUser.LoadByPrimaryKey(TaggesUserID)

            StrAction += "Tagged User with ID [ " & objICTaggedApprovalGroupUser.UserID & " ] [ " & objICTaggedApprovalGroupUser.UpToICUserByUserID.UserName & " ]"
            StrAction += " for approval group [ " & objICTaggedApprovalGroupUser.ApprovalGroupID & " ] [ " & objICTaggedApprovalGroupUser.UpToICApprovalGroupManagementByApprovalGroupID.GroupName & " ]"
            StrAction += " deleted."
            objICTaggedApprovalGroupUser.MarkAsDeleted()
            objICTaggedApprovalGroupUser.Save()

            ICUtilities.AddAuditTrail(StrAction, "Tag User Approval Group", TaggesUserID, UserID.ToString(), UserName.ToString(), "DELETE")



        End Sub
        Public Shared Function IsUserTaggedWithApprovalRuleCondition(ByVal ApprovalGroupID As String, ByVal UserID As String) As Boolean
            Dim qryObjAppRuleCondUser As New ICApprovalRuleConditionUsersQuery("qryObjAppRuleCondUser")
            Dim qryObjAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjAppRuleCond")

            qryObjAppRuleCondUser.InnerJoin(qryObjAppRuleCond).On(qryObjAppRuleCondUser.ApprovaRuleCondID = qryObjAppRuleCond.ApprovalRuleConditionID)
            qryObjAppRuleCondUser.Where(qryObjAppRuleCondUser.UserID = UserID And qryObjAppRuleCond.ApprovalGroupID = ApprovalGroupID)
            If qryObjAppRuleCondUser.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsApprovalGroupTaggedWithApprovalRuleCondition(ByVal ApprovalGroupID As String) As Boolean
            Dim qryObjAppRuleCondUser As New ICApprovalRuleConditionUsersQuery("qryObjAppRuleCondUser")
            Dim qryObjAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjAppRuleCond")

            qryObjAppRuleCondUser.InnerJoin(qryObjAppRuleCond).On(qryObjAppRuleCondUser.ApprovaRuleCondID = qryObjAppRuleCond.ApprovalRuleConditionID)
            qryObjAppRuleCondUser.Where(qryObjAppRuleCond.ApprovalGroupID = ApprovalGroupID)
            If qryObjAppRuleCondUser.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace
