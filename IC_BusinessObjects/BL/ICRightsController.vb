﻿Namespace IC
    Public Class ICRightsController
        Public Shared Function GetAllICRights() As DataTable
            Dim objICRightsColl As New ICRightsCollection
            objICRightsColl.es.Connection.CommandTimeout = 3600
            objICRightsColl.Query.OrderBy(objICRightsColl.Query.RightName.Ascending)
            objICRightsColl.Query.Load()
            Return objICRightsColl.Query.LoadDataTable

        End Function
        Public Shared Function GetRightDetailNameByRightName(ByVal RightName As String) As DataTable
            Dim objICRightsColl As New ICRightsCollection
            objICRightsColl.es.Connection.CommandTimeout = 3600
            objICRightsColl.Query.Select(objICRightsColl.Query.RightDetailName)
            objICRightsColl.Query.Where(objICRightsColl.Query.RightName = RightName)
            objICRightsColl.Query.OrderBy(objICRightsColl.Query.RightName.Ascending)
            objICRightsColl.Query.Load()
            Return objICRightsColl.Query.LoadDataTable

        End Function
        Public Shared Function GetRightsAssignedToRoleByRoleID(ByVal RoleID As String) As Boolean
            Dim Result As Boolean = False
            Dim objICRoleRightsColl As New ICRoleRightsCollection
            objICRoleRightsColl.es.Connection.CommandTimeout = 3600
            objICRoleRightsColl.Query.Where(objICRoleRightsColl.Query.RoleID = RoleID)
            objICRoleRightsColl.Query.Load()
            If objICRoleRightsColl.Count > 0 Then
                Result = True
            End If
            Return Result

        End Function

    End Class
End Namespace
