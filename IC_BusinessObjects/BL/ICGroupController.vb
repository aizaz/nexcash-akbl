﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICGroupController
        Public Shared Sub AddGroup(ByVal cGroup As ICGroup, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICGroup As New ICGroup
            Dim prevICGroup As New ICGroup
            Dim IsActiveText As String
            Dim CurrentAt As String
            Dim PrevAt As String

            ICGroup.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICGroup.CreatedBy = cGroup.CreatedBy
                ICGroup.CreatedDate = cGroup.CreatedDate
                ICGroup.Creater = cGroup.Creater
                ICGroup.CreationDate = cGroup.CreationDate
            Else
                ICGroup.LoadByPrimaryKey(cGroup.GroupCode)
                prevICGroup.LoadByPrimaryKey(cGroup.GroupCode)
                ICGroup.CreatedBy = cGroup.CreatedBy
                ICGroup.CreatedDate = cGroup.CreatedDate
            End If
            ICGroup.GroupName = cGroup.GroupName

            ICGroup.IsActive = cGroup.IsActive

            If ICGroup.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            ICGroup.Save()

            If (isUpdate = False) Then
                CurrentAt = "Group : [Code:  " & ICGroup.GroupCode.ToString() & " ; Name:  " & ICGroup.GroupName.ToString() & " ; IsActive:  " & ICGroup.IsActive.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Group", ICGroup.GroupCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "Group : Current Values [Code:  " & ICGroup.GroupCode.ToString() & " ; Name:  " & ICGroup.GroupName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                PrevAt = "<br />Group : Previous Values [Code:  " & prevICGroup.GroupCode.ToString() & " ; Name:  " & prevICGroup.GroupName.ToString() & " ; IsActive:  " & prevICGroup.IsActive.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Group", ICGroup.GroupCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub
        Public Shared Function GetAllGroups() As ICGroupCollection
            Dim ICGroupColl As New ICGroupCollection
            ICGroupColl.es.Connection.CommandTimeout = 3600

            ICGroupColl.Query.OrderBy(ICGroupColl.Query.GroupName.Ascending)

            ICGroupColl.Query.Load()

            Return ICGroupColl

        End Function

        Public Shared Sub DeleteGroup(ByVal GroupCode As String, ByVal UserID As String, ByVal UserName As String)
            Dim ICGroup As New ICGroup
            Dim prevICGroup As New ICGroup
            Dim PrevAt As String = ""
            ICGroup.es.Connection.CommandTimeout = 3600
            prevICGroup.es.Connection.CommandTimeout = 3600

            ICGroup.LoadByPrimaryKey(GroupCode)
            PrevAt = "Group [Code:  " & ICGroup.GroupCode.ToString() & " ; Name:  " & ICGroup.GroupName.ToString() & " ; IsActive:  " & ICGroup.IsActive.ToString() & "] "

            ICGroup.MarkAsDeleted()
            ICGroup.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Group", prevICGroup.GroupCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Function GetAllActiveAndApproveGroups() As ICGroupCollection
            Dim ICGroupColl As New ICGroupCollection
            ICGroupColl.es.Connection.CommandTimeout = 3600
            ICGroupColl.Query.Where(ICGroupColl.Query.IsActive = True)
            ICGroupColl.Query.OrderBy(ICGroupColl.Query.GroupName.Ascending)
            ICGroupColl.Query.Load()
            Return ICGroupColl
        End Function
        Public Shared Function GetAllActiveAndApproveGroupsByGroupCode(ByVal GroupCode As String) As ICGroupCollection
            Dim ICGroupColl As New ICGroupCollection
            ICGroupColl.es.Connection.CommandTimeout = 3600
            ICGroupColl.Query.Where(ICGroupColl.Query.IsActive = True And ICGroupColl.Query.GroupCode = GroupCode)
            ICGroupColl.Query.OrderBy(ICGroupColl.Query.GroupName.Ascending)
            ICGroupColl.Query.Load()
            Return ICGroupColl
        End Function

        Public Shared Sub GetGroupgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collGroup As New ICGroupCollection

            If Not pagenumber = 0 Then
                collGroup.Query.Select(collGroup.Query.GroupCode, collGroup.Query.GroupName, collGroup.Query.IsActive)
                collGroup.Query.OrderBy(collGroup.Query.GroupCode.Descending)
                collGroup.Query.Load()
                rg.DataSource = collGroup

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collGroup.Query.es.PageNumber = 1
                collGroup.Query.es.PageSize = pagesize
                collGroup.Query.OrderBy(collGroup.Query.GroupCode.Descending)
                collGroup.Query.Load()
                rg.DataSource = collGroup


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub


        Public Shared Function GetAllActiveGroups() As ICGroupCollection
            Dim ICGroupColl As New ICGroupCollection
            ICGroupColl.es.Connection.CommandTimeout = 3600
            ICGroupColl.Query.Where(ICGroupColl.Query.IsActive = True)
            ICGroupColl.Query.OrderBy(ICGroupColl.Query.GroupName.Ascending)
            ICGroupColl.Query.Load()
            Return ICGroupColl
        End Function


#Region "Collection"
        Public Shared Function GetAllActiveGroupsIsCollectionAllowonCompany() As DataTable
            Dim qryGroup As New ICGroupQuery("qryGrp")
            Dim qryCompany As New ICCompanyQuery("qryComp")
            Dim dt As New DataTable
            qryGroup.es.Distinct = True
            qryGroup.Select(qryGroup.GroupCode.Distinct, qryGroup.GroupName.Distinct)
            qryGroup.InnerJoin(qryCompany).On(qryGroup.GroupCode = qryCompany.GroupCode)
            qryGroup.Where(qryGroup.IsActive = True And qryCompany.IsCollectionAllowed = True)
            qryGroup.Where(qryCompany.IsActive = True And qryCompany.IsCollectionApproved = True)

            qryGroup.OrderBy(qryGroup.GroupName.Ascending)
            dt = qryGroup.LoadDataTable()
            Return dt
        End Function
#End Region

    End Class
End Namespace
