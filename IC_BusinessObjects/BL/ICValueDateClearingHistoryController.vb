﻿Namespace IC
    Public Class ICValueDateClearingHistoryController
        Public Shared Function IsClearingProcessExecutedToday() As Boolean
            Dim objICCollClearingHistory As New ICCollectionValueDateClearingHistoryCollection
            objICCollClearingHistory.Query.Where(objICCollClearingHistory.Query.ExecutionDate.Date = Date.Now.Date)
            objICCollClearingHistory.Query.OrderBy(objICCollClearingHistory.Query.HistoryID.Ascending)
            If objICCollClearingHistory.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsClearingProcessExecutedByDate(ByVal Dates As Date) As Boolean
            Dim objICCollClearingHistory As New ICCollectionValueDateClearingHistoryCollection
            objICCollClearingHistory.Query.Where(objICCollClearingHistory.Query.ExecutionDate.Date = Dates)
            objICCollClearingHistory.Query.OrderBy(objICCollClearingHistory.Query.HistoryID.Ascending)
            If objICCollClearingHistory.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
        Private Sub AutoProcessValueDateClearing()
            Dim dt As New DataTable
            If ICUtilities.GetCollectionSettingValue("ValueDateClearingType") = "Manual" Then
                Exit Sub
            End If
            If IsClearingProcessExecutedByDate(Date.Now.Date) = True Then
                Exit Sub
            End If
            If Date.Compare(Date.Now.ToString("HH:mm:ss"), CDate(ICBO.ICUtilities.GetSettingValue("ValueDateClearingExecutionTime").ToString())) <> 0 Then
                Exit Sub
            End If
            ProcessValueDateClearing(Nothing, Nothing, "Auto", dt)
        End Sub
        Public Shared Function AddValueDateClearingHistory(ByVal objICClearingHistry As ICCollectionValueDateClearingHistory, ByVal IsUpdate As Boolean, ByVal ActionString As String, ByVal UserID As String, ByVal UserName As String) As Integer
            Dim objICCollClearingHistoryForSave As New ICCollectionValueDateClearingHistory
            If IsUpdate = True Then
                objICCollClearingHistoryForSave.LoadByPrimaryKey(objICClearingHistry.HistoryID)
            End If
            objICCollClearingHistoryForSave.ExecutionDate = objICClearingHistry.ExecutionDate
            objICCollClearingHistoryForSave.ExecutedBy = objICClearingHistry.ExecutedBy
            objICCollClearingHistoryForSave.PassCount = objICClearingHistry.PassCount
            objICCollClearingHistoryForSave.FailCount = objICClearingHistry.FailCount
            objICClearingHistry.Save()
            ICUtilities.AddAuditTrail(ActionString, "Value Date Clearing History", objICClearingHistry.HistoryID.ToString, UserID, UserName, "ADD")
            Return objICClearingHistry.HistoryID
        End Function
        Public Shared Sub AddValueDateClearingHistoryInstrument(ByVal objICClearingHistry As ICCollectionValueDateClearingInstruments, ByVal ActionString As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICCollClearingHistoryForSave As New ICCollectionValueDateClearingInstruments
            objICCollClearingHistoryForSave.HistoryID = objICClearingHistry.HistoryID
            objICCollClearingHistoryForSave.InstrumentNo = objICClearingHistry.InstrumentNo
            objICCollClearingHistoryForSave.CollectionID = objICClearingHistry.CollectionID
            objICCollClearingHistoryForSave.IsProcessed = objICClearingHistry.IsProcessed
            objICClearingHistry.Save()
            ICUtilities.AddAuditTrail(ActionString, "Value Date Clearing History Instrument", objICClearingHistry.ValueDateClearingInstrumentID.ToString, UserID, UserName, "ADD")
        End Sub
        Public Shared Sub ProcessValueDateClearing(ByVal UserID As String, ByVal UserName As String, ByVal ProcessType As String, ByRef dt As DataTable)
            Dim collObjICCollections As New ICCollectionsCollection
            Dim objICCollectionForUpdate As ICCollections
            Dim objICCollValueDateHistory As ICCollectionValueDateClearingHistory
            Dim objICCollValueDateHistoryInstrument As New ICCollectionValueDateClearingInstrumentsCollection
            Dim objICValueDateHistoryInstrument As New ICCollectionValueDateClearingInstruments
            Dim objICAuditTrail As New ICAuditTrail
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim TranStat As CBUtilities.TransactionStatus
            Dim FromAccountNo, FromAccountBranchCode, FromAccountCurrency, FromAccountType As String
            Dim objICOffice As ICOffice
            Dim ActionString As String = Nothing
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0
            Dim HistoryID As String
            Dim dt2 As New DataTable
            Dim ReqSqlString As String = Nothing
            Dim dr As DataRow
            dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
            dt.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
            '' Check Pending Clearing Instruments
            collObjICCollections.Query.Where(collObjICCollections.Query.ClearingValueDate <= Date.Now And collObjICCollections.Query.Status = 8)
            collObjICCollections.Query.OrderBy(collObjICCollections.Query.CollectionID.Ascending)
            If collObjICCollections.Query.Load Then
                ''Get history ID
                objICCollValueDateHistory = New ICCollectionValueDateClearingHistory
                HistoryID = AddValueDateClearingHistory(objICCollValueDateHistory, False, "Value Date Clearing Process Start", UserID, UserName)
                For Each objICCollection As ICCollections In collObjICCollections
                    ''Cheque collection / disbursementmode
                    objICOffice = New ICOffice
                    objICOffice.LoadByPrimaryKey(objICCollection.CollectionLocationCode)
                    If objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.DisbursementMode = "Cheque" Then
                        ''cheque Clearing Type
                        If objICOffice.IsApprove = False Then
                            objICAuditTrail = New ICAuditTrail
                            objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                            objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to Collecting Branch is not approved. "
                            objICAuditTrail.AuditDate = Date.Now
                            objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                            objICAuditTrail.ActionType = "SKIP"
                            objICAuditTrail.UserID = UserID
                            objICAuditTrail.Username = UserName
                            objICAuditTrailColl.Add(objICAuditTrail)
                            ''
                            FailCount = FailCount + 1
                            objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                            objICValueDateHistoryInstrument.HistoryID = HistoryID
                            objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                            objICValueDateHistoryInstrument.IsProcessed = False
                            objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                            dr = dt.NewRow
                            dr("Message") = "Fail to process Instrument(s)"
                            dr("InstrumentNo") = objICCollection.InstrumentNo
                            dr("Status") = "0"
                            dt.Rows.Add(dr)
                            Continue For
                        End If
                        If objICCollection.CollectionClearingType = "Inter City" Then
                            If objICOffice.InterCityOutwardClearingAccountNumber Is Nothing Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to Inter city clearing outward account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)

                                FailCount = FailCount + 1
                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, objICOffice.InterCityOutwardClearingAccountNumber, objICOffice.OfficeCode, objICOffice.InterCityOutwardClearingAccountCurrency, Nothing, Nothing, Nothing, objICOffice.InterCityOutwardClearingAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = objICOffice.InterCityOutwardClearingAccountNumber
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = objICOffice.InterCityOutwardClearingAccountBranchCode
                                        objICCollectionForUpdate.ClearingCBAccountType = objICOffice.InterCityOutwardClearingAccountType
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = "Inter City"
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1

                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If




                                        ''Add log
                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by Inter city clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments

                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from Inter city clearing outward account [ " & objICOffice.InterCityOutwardClearingAccountNumber & " ] due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If



                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)

                                    FailCount = FailCount + 1

                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        ElseIf objICCollection.CollectionClearingType = "Normal" Then
                            If objICOffice.NormalOutwardClearingAccountNumber Is Nothing Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to normal clearing outward account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                FailCount = FailCount + 1
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, objICOffice.NormalOutwardClearingAccountNumber, objICOffice.OfficeCode, objICOffice.NormalOutwardClearingAccountCurrency, Nothing, Nothing, Nothing, objICOffice.NormalOutwardClearingAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = objICOffice.NormalOutwardClearingAccountNumber
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = objICOffice.NormalOutwardClearingAccountCurrency
                                        objICCollectionForUpdate.ClearingCBAccountType = objICOffice.NormalOutwardClearingAccountType
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = "Normal"
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1



                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If

                                        ''Add log
                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by normal clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments



                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If



                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from Inter city clearing outward account [ " & objICOffice.InterCityOutwardClearingAccountNumber & " ] due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If



                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    FailCount = FailCount + 1


                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        ElseIf objICCollection.CollectionClearingType = "Same Day" Then
                            If objICOffice.SameDayOutwardClearingAccountNumber Is Nothing Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to same day clearing outward account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                FailCount = FailCount + 1
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, objICOffice.SameDayOutwardClearingAccountNumber, objICOffice.OfficeCode, objICOffice.SameDayOutwardClearingAccountCurrency, Nothing, Nothing, Nothing, objICOffice.SameDayOutwardClearingAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = objICOffice.SameDayOutwardClearingAccountNumber
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = objICOffice.SameDayOutwardClearingAccountCurrency
                                        objICCollectionForUpdate.ClearingCBAccountType = objICOffice.SameDayOutwardClearingAccountType
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = "Same Day"
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1
                                        ''Add log



                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If



                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by same day clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from same day clearing outward account [ " & objICOffice.SameDayOutwardClearingAccountNumber & " ] due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If


                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    FailCount = FailCount + 1


                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        ElseIf objICCollection.CollectionClearingType = "OBC" Then
                            If objICOffice.OBCOutwardClearingAccountNumber Is Nothing Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to OBC outward account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                FailCount = FailCount + 1
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, objICOffice.OBCOutwardClearingAccountNumber, objICOffice.OfficeCode, objICOffice.OBCOutwardClearingAccountCurrency, Nothing, Nothing, Nothing, objICOffice.OBCOutwardClearingAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = objICOffice.OBCOutwardClearingAccountNumber
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = objICOffice.OBCOutwardClearingAccountCurrency
                                        objICCollectionForUpdate.ClearingCBAccountType = objICOffice.OBCOutwardClearingAccountCurrency
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = "OBC"
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1


                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        ''Add log
                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by same OBC clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from OBC outward clearing account [ " & objICOffice.SameDayOutwardClearingAccountNumber & " ] due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments

                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If


                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    FailCount = FailCount + 1


                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        End If
                    ElseIf objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.DisbursementMode = "Pay Order" Then
                        If ICUtilities.GetCollectionSettingValue("POPayableAccount") = "Central" Then
                            If ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") = "" Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due Central PO Payable account is not defiend"
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                FailCount = FailCount + 1
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    FromAccountNo = ICUtilities.GetCollectionSettingValue("POPayableAccountNumber")
                                    FromAccountBranchCode = ICUtilities.GetCollectionSettingValue("POPayableAccountBranchCode")
                                    FromAccountCurrency = ICUtilities.GetCollectionSettingValue("POPayableAccountCurrency").ToUpper
                                    FromAccountType = ICUtilities.GetCollectionSettingValue("POPayableAccountType").ToUpper


                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, FromAccountNo, objICOffice.OfficeCode, FromAccountCurrency, Nothing, Nothing, Nothing, FromAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = FromAccountNo
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = FromAccountCurrency
                                        objICCollectionForUpdate.ClearingCBAccountType = FromAccountType
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1



                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If



                                        ''Add log
                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments



                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type  [ " & objICOffice.SameDayOutwardClearingAccountNumber & " ] account due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments

                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If


                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process of clearing type " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    FailCount = FailCount + 1
                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        Else
                            If objICOffice.POPayableAccountNumber Is Nothing Then
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due PO Payable account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ]"
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                FailCount = FailCount + 1
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            Else
                                Try
                                    FromAccountNo = objICOffice.POPayableAccountNumber
                                    FromAccountBranchCode = objICOffice.POPayableAccountBranchCode
                                    FromAccountCurrency = objICOffice.POPayableAccountCurrency
                                    FromAccountType = objICOffice.POPayableAccountType
                                    TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, FromAccountNo, objICOffice.OfficeCode, FromAccountCurrency, Nothing, Nothing, Nothing, FromAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                    If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                        ''Set Clearing Account
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        objICCollectionForUpdate.ClearingAccountNo = FromAccountNo
                                        objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                        objICCollectionForUpdate.ClearingAccountCurrency = FromAccountCurrency
                                        objICCollectionForUpdate.ClearingCBAccountType = FromAccountType
                                        objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                        objICCollectionForUpdate.ClearingProcess = objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType
                                        objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                        objICCollectionForUpdate.Status = 9
                                        objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                        ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                        PassCount = PassCount + 1


                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        ''Add log
                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "Process"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = True
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                        dr = dt.NewRow
                                        dr("Message") = "Process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "1"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    Else
                                        objICAuditTrail = New ICAuditTrail
                                        objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments

                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                        objICCollectionForUpdate = New ICCollections
                                        objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                        ''Update invoice status
                                        dt2 = New DataTable
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                        dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                        If dt2.Rows.Count > 0 Then
                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                        End If


                                        objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type  [ " & objICOffice.SameDayOutwardClearingAccountNumber & " ] account due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                        objICAuditTrail.AuditDate = Date.Now
                                        objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                        objICAuditTrail.ActionType = "SKIP"
                                        objICAuditTrail.UserID = UserID
                                        objICAuditTrail.Username = UserName
                                        objICAuditTrailColl.Add(objICAuditTrail)
                                        ''
                                        FailCount = FailCount + 1
                                        objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                        objICValueDateHistoryInstrument.HistoryID = HistoryID
                                        objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                        objICValueDateHistoryInstrument.IsProcessed = False
                                        objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                        dr = dt.NewRow
                                        dr("Message") = "Fail to process Instrument(s)"
                                        dr("InstrumentNo") = objICCollection.InstrumentNo
                                        dr("Status") = "0"
                                        dt.Rows.Add(dr)
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If


                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process of clearing type " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " due to fail in funds transfer due to " & ex.Message.ToString
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    FailCount = FailCount + 1
                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("Status") = "0"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dt.Rows.Add(dr)
                                    Continue For
                                End Try
                            End If
                        End If
                    ElseIf objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.DisbursementMode = "Demand Draft" Then

                        If objICOffice.POPayableAccountNumber Is Nothing Then
                            objICAuditTrail = New ICAuditTrail
                            objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                            objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due PO Payable account is not tagged with branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ]"
                            objICAuditTrail.AuditDate = Date.Now
                            objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                            objICAuditTrail.ActionType = "SKIP"
                            objICAuditTrail.UserID = UserID
                            objICAuditTrail.Username = UserName
                            objICAuditTrailColl.Add(objICAuditTrail)
                            ''
                            FailCount = FailCount + 1
                            objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                            objICValueDateHistoryInstrument.HistoryID = HistoryID
                            objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                            objICValueDateHistoryInstrument.IsProcessed = False
                            objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)


                            dr = dt.NewRow
                            dr("Message") = "Fail to process Instrument(s)"
                            dr("InstrumentNo") = objICCollection.InstrumentNo
                            dr("Status") = "0"
                            dt.Rows.Add(dr)
                            Continue For
                        Else
                            Try
                                FromAccountNo = objICOffice.DDPayableAccountNumber
                                FromAccountBranchCode = objICOffice.DDPayableAccountBranchCode
                                FromAccountCurrency = objICOffice.DDPayableAccountCurrency
                                FromAccountType = objICOffice.DDPayableAccountType
                                TranStat = CBUtilities.MoveCollectionFunds(objICCollection.CollectionID, FromAccountNo, objICOffice.OfficeCode, FromAccountCurrency, Nothing, Nothing, Nothing, FromAccountType, objICCollection.CollectionAccountNo, objICCollection.CollectionAccountBranchCode, objICCollection.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollection.CollectionAccountType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.OriginatingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.RespondingTransactionType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.VoucherType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.InstrumentType, objICCollection.UpToICCollectionProductTypeByCollectionProductTypeCode.ReversalTransactionType, UserID, UserName, objICCollection.CollectionAmount, "Collection")
                                If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments
                                    ''Set Clearing Account
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    objICCollectionForUpdate.ClearingAccountNo = FromAccountNo
                                    objICCollectionForUpdate.ClearingAccountBranchCode = objICOffice.OfficeCode
                                    objICCollectionForUpdate.ClearingAccountCurrency = FromAccountCurrency
                                    objICCollectionForUpdate.ClearingCBAccountType = FromAccountType
                                    objICCollectionForUpdate.CollectionTransferedDate = Date.Now
                                    objICCollectionForUpdate.ClearingProcess = objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType
                                    objICCollectionForUpdate.ClearingBranchCode = objICOffice.OfficeID
                                    objICCollectionForUpdate.Status = 9
                                    objICCollectionForUpdate.LastStatus = objICCollection.LastStatus
                                    ICCollectionsController.UpdateICCollections(objICCollectionForUpdate, UserID, UserName)
                                    PassCount = PassCount + 1


                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If


                                    ''Add log
                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] Processed in Value Date Clearing Process by " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "Process"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = True
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                    dr = dt.NewRow
                                    dr("Message") = "Process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "1"
                                    dt.Rows.Add(dr)
                                    Continue For
                                Else
                                    objICAuditTrail = New ICAuditTrail
                                    objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments


                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                    objICCollectionForUpdate = New ICCollections
                                    objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                    ''Update invoice status
                                    dt2 = New DataTable
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                    dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                    If dt2.Rows.Count > 0 Then
                                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                    End If



                                    objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process due to fail in funds transfer from " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " clearing type  [ " & objICOffice.SameDayOutwardClearingAccountNumber & " ] account due to [ " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat) & " ] from branch [ " & objICOffice.OfficeCode & "-" & objICOffice.OfficeName & " ] "
                                    objICAuditTrail.AuditDate = Date.Now
                                    objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                    objICAuditTrail.ActionType = "SKIP"
                                    objICAuditTrail.UserID = UserID
                                    objICAuditTrail.Username = UserName
                                    objICAuditTrailColl.Add(objICAuditTrail)
                                    ''
                                    FailCount = FailCount + 1
                                    objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                    objICValueDateHistoryInstrument.HistoryID = HistoryID
                                    objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                    objICValueDateHistoryInstrument.IsProcessed = False
                                    objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)



                                    dr = dt.NewRow
                                    dr("Message") = "Fail to process Instrument(s)"
                                    dr("InstrumentNo") = objICCollection.InstrumentNo
                                    dr("Status") = "0"
                                    dt.Rows.Add(dr)
                                    Continue For
                                End If
                            Catch ex As Exception
                                objICAuditTrail = New ICAuditTrail
                                objICValueDateHistoryInstrument = New ICCollectionValueDateClearingInstruments

                                objICCollectionForUpdate = New ICCollections
                                objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                ICCollectionsController.UpdateICCollectionsStatus(objICCollectionForUpdate.CollectionID, objICCollectionForUpdate.Status.ToString, "10", UserID, UserName)
                                objICCollectionForUpdate = New ICCollections
                                objICCollectionForUpdate.LoadByPrimaryKey(objICCollection.CollectionID)
                                ''Update invoice status
                                dt2 = New DataTable
                                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, UserID, objICCollectionForUpdate.FileID, "Select", "Master", objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, Nothing, Nothing)
                                dt2 = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlString)
                                If dt2.Rows.Count > 0 Then
                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionForUpdate.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollectionForUpdate.ReferenceField1, objICCollectionForUpdate.ReferenceField2, objICCollectionForUpdate.ReferenceField3, objICCollectionForUpdate.ReferenceField4, objICCollectionForUpdate.ReferenceField5, dt2.Rows(0)("Status").ToString, objICCollectionForUpdate.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, UserID, UserName)
                                End If


                                objICAuditTrail.AuditAction = "Instrument no [ " & objICCollection.InstrumentNo & " ] skipped in Value Date Clearing Process of clearing type " & objICCollectionForUpdate.UpToICCollectionProductTypeByCollectionProductTypeCode.ClearingType & " due to fail in funds transfer due to " & ex.Message.ToString
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.AuditType = "Value Date Clearing History Instrument"
                                objICAuditTrail.ActionType = "SKIP"
                                objICAuditTrail.UserID = UserID
                                objICAuditTrail.Username = UserName
                                objICAuditTrailColl.Add(objICAuditTrail)
                                ''
                                objICValueDateHistoryInstrument.InstrumentNo = objICCollection.InstrumentNo
                                objICValueDateHistoryInstrument.HistoryID = HistoryID
                                objICValueDateHistoryInstrument.CollectionID = objICCollection.CollectionID
                                objICValueDateHistoryInstrument.IsProcessed = False
                                objICCollValueDateHistoryInstrument.Add(objICValueDateHistoryInstrument)
                                FailCount = FailCount + 1
                                dr = dt.NewRow
                                dr("Message") = "Fail to process Instrument(s)"
                                dr("InstrumentNo") = objICCollection.InstrumentNo
                                dr("Status") = "0"
                                dt.Rows.Add(dr)
                                Continue For
                            End Try
                        End If
                    End If

                Next
                ''update table and logs
                objICCollValueDateHistory = New ICCollectionValueDateClearingHistory
                objICCollValueDateHistory.LoadByPrimaryKey(HistoryID)
                objICCollValueDateHistory.HistoryID = HistoryID
                objICCollValueDateHistory.PassCount = PassCount
                objICCollValueDateHistory.FailCount = FailCount
                objICCollValueDateHistory.ExecutionDate = Date.Now
                AddValueDateClearingHistory(objICCollValueDateHistory, True, "Update Instrument Count", UserID, UserName)
                If objICCollValueDateHistoryInstrument.Count > 0 Then
                    Dim i As Integer = 0
                    For Each obj As ICCollectionValueDateClearingInstruments In objICCollValueDateHistoryInstrument
                        obj.Save()
                        obj = New ICCollectionValueDateClearingInstruments
                        obj.LoadByPrimaryKey(objICCollValueDateHistoryInstrument(i).ValueDateClearingInstrumentID)
                        objICAuditTrailColl(i).RelatedID = obj.ValueDateClearingInstrumentID
                        objICAuditTrailColl(i).Save()
                        i = i + 1
                    Next
                End If

            Else
                ''if No instrument found
                If ProcessType = "Manual" Then
                    objICCollValueDateHistory = New ICCollectionValueDateClearingHistory
                    ''log and display message for manual process
                    dr = dt.NewRow
                    dr("Message") = "No Instrument Found"
                    dr("Status") = "0"
                    dr("InstrumentNo") = "-"
                    dt.Rows.Add(dr)
                    ActionString = "Value Date Clearing Process Auto executed at [ " & Date.Now & " ]. [ " & FailCount & " ] instrument(s) process"
                    objICCollValueDateHistory.PassCount = PassCount
                    objICCollValueDateHistory.FailCount = FailCount
                    objICCollValueDateHistory.ExecutionDate = Date.Now
                    AddValueDateClearingHistory(objICCollValueDateHistory, False, ActionString, UserID, UserName)
                Else
                    ''log for auto process
                    objICCollValueDateHistory = New ICCollectionValueDateClearingHistory
                    ActionString = "Value Date Clearing Process Auto executed at [ " & Date.Now & " ]. [ " & FailCount & " ] instrument(s) process"
                    objICCollValueDateHistory.PassCount = PassCount
                    objICCollValueDateHistory.FailCount = FailCount
                    objICCollValueDateHistory.ExecutionDate = Date.Now
                    AddValueDateClearingHistory(objICCollValueDateHistory, False, ActionString, UserID, UserName)
                End If
                Exit Sub
            End If
        End Sub
    End Class
End Namespace
