﻿Imports Telerik.Web.UI
Imports FtpLib
Imports System
Imports System.IO
Namespace IC
    Public Class ICCompanyController
        'Public Shared Sub AddCompany(ByVal cCompany As IC.ICCompany, ByVal isUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal StrAction As String)

        '    Dim objICCompany As New IC.ICCompany
        '    Dim i As Integer = 0
        '    objICCompany.es.Connection.CommandTimeout = 3600

        '    If (isUpdate = False) Then

        '        objICCompany.CreatedBy = cCompany.CreatedBy
        '        objICCompany.CreatedDate = cCompany.CreatedDate

        '    Else
        '        objICCompany.LoadByPrimaryKey(cCompany.CompanyCode)
        '    End If

        '    objICCompany.CompanyName = cCompany.CompanyName
        '    objICCompany.GroupCode = cCompany.GroupCode
        '    objICCompany.PhoneNumber1 = cCompany.PhoneNumber1
        '    objICCompany.PhoneNumber2 = cCompany.PhoneNumber2
        '    objICCompany.Address = cCompany.Address
        '    objICCompany.EmailAddress = cCompany.EmailAddress
        '    objICCompany.City = cCompany.City
        '    objICCompany.SkipPaymentQueue = cCompany.SkipPaymentQueue
        '    objICCompany.CuttOffTimeStart = cCompany.CuttOffTimeStart
        '    objICCompany.CuttOffTimeEnd = cCompany.CuttOffTimeEnd
        '    objICCompany.IsApprove = False

        '    objICCompany.IsActive = cCompany.IsActive

        '    objICCompany.Save()
        '    i = objICCompany.CompanyCode

        '    If isUpdate = False Then
        '        ICUtilities.AddAuditTrail("Company " & objICCompany.CompanyName.ToString() & " Added", "Company", i.ToString, UsersID.ToString, UsersName.ToString())
        '    ElseIf isUpdate = True Then
        '        ICUtilities.AddAuditTrail(StrAction.ToString, "Company", objICCompany.CompanyCode.ToString(), UsersID.ToString(), UsersName.ToString())
        '    End If


        'End Sub


        Public Shared Sub AddCompany(ByVal cCompany As IC.ICCompany, ByVal isUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal StrAction As String)

            Dim objICCompany As New IC.ICCompany
            Dim i As Integer = 0
            objICCompany.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then

                objICCompany.CreatedBy = cCompany.CreatedBy
                objICCompany.CreatedDate = cCompany.CreatedDate
                objICCompany.Creater = cCompany.Creater
                objICCompany.CreationDate = cCompany.CreationDate
            Else
                objICCompany.LoadByPrimaryKey(cCompany.CompanyCode)
                objICCompany.CreatedBy = cCompany.CreatedBy
                objICCompany.CreatedDate = cCompany.CreatedDate
            End If

            objICCompany.CompanyName = cCompany.CompanyName
            objICCompany.GroupCode = cCompany.GroupCode
            objICCompany.PhoneNumber1 = cCompany.PhoneNumber1
            objICCompany.PhoneNumber2 = cCompany.PhoneNumber2
            objICCompany.Address = cCompany.Address
            objICCompany.EmailAddress = cCompany.EmailAddress
            objICCompany.City = cCompany.City
            objICCompany.SkipPaymentQueue = cCompany.SkipPaymentQueue
            objICCompany.CuttOffTimeStart = cCompany.CuttOffTimeStart
            objICCompany.CuttOffTimeEnd = cCompany.CuttOffTimeEnd
            objICCompany.IsApprove = False
            objICCompany.IsCollectionApproved = False

            objICCompany.IsCollectionAllowed = cCompany.IsCollectionAllowed
            objICCompany.IsPaymentsAllowed = cCompany.IsPaymentsAllowed
            If cCompany.IsCollectionAllowed = True Then
                objICCompany.IsTellerScreenAllowed = cCompany.IsTellerScreenAllowed
                objICCompany.IsAutoFetchAllowed = cCompany.IsAutoFetchAllowed
                objICCompany.IsScrollUploadAllowed = cCompany.IsScrollUploadAllowed
            Else
                objICCompany.IsTellerScreenAllowed = False
                objICCompany.IsAutoFetchAllowed = False
                objICCompany.IsScrollUploadAllowed = False
            End If

            objICCompany.IsActive = cCompany.IsActive
            objICCompany.IsSingleSignatory = False


            '' MT 940 Work
            objICCompany.IsMT940StatementAllowed = cCompany.IsMT940StatementAllowed
            objICCompany.MT940StatementLocation = cCompany.MT940StatementLocation
            objICCompany.MT940FetchingFrequency = cCompany.MT940FetchingFrequency
            
            objICCompany.HostName = cCompany.HostName
            objICCompany.UserName = cCompany.UserName
            objICCompany.LocationPassword = cCompany.LocationPassword
            objICCompany.FolderName = cCompany.FolderName

            objICCompany.PackageInvoiceBillingDate = cCompany.PackageInvoiceBillingDate
            objICCompany.IsAllowOpenPayment = cCompany.IsAllowOpenPayment
            objICCompany.IsBeneRequired = cCompany.IsBeneRequired
            objICCompany.IsPaymentsAllowed = True
            objICCompany.Save()
            i = objICCompany.CompanyCode

            If isUpdate = False Then
                ICUtilities.AddAuditTrail(StrAction.ToString, "Company", i.ToString, UsersID.ToString, UsersName.ToString(), "ADD")
            ElseIf isUpdate = True Then
                ICUtilities.AddAuditTrail(StrAction.ToString, "Company", objICCompany.CompanyCode.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")
            End If


        End Sub



        'Public Shared Function GetCompanyActiveAndApproveByGroupCode(ByVal GroupCode As String) As ICCompanyCollection
        '    Dim ICComapnyColl As New ICCompanyCollection

        '    ICComapnyColl.es.Connection.CommandTimeout = 3600


        '    ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True And ICComapnyColl.Query.IsApprove = True)

        '    ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
        '    ICComapnyColl.Query.Load()
        '    Return ICComapnyColl
        'End Function


        Public Shared Function GetCompanyActiveAndApprove() As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection

            ICComapnyColl.es.Connection.CommandTimeout = 3600

            ICComapnyColl.Query.Select(ICComapnyColl.Query.CompanyCode, ICComapnyColl.Query.CompanyName, ICComapnyColl.Query.IsBeneRequired, ICComapnyColl.Query.IsAllowOpenPayment)
            ICComapnyColl.Query.Where(ICComapnyColl.Query.IsActive = True And ICComapnyColl.Query.IsApprove = True)
            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function
        'Public Shared Sub GetAllCompanyForRadGrid(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim dt As New DataTable

        '    qryObjICCompany.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName, qryObjICGroup.GroupName, qryObjICCompany.IsActive, qryObjICCompany.IsApprove)
        '    qryObjICCompany.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    dt = qryObjICCompany.LoadDataTable
        '    If Not pagenumber = 0 Then
        '        qryObjICCompany.es.PageNumber = pagenumber

        '        qryObjICCompany.es.PageSize = pagesize


        '        rg.DataSource = dt


        '        If DoDataBind Then


        '            rg.DataBind()

        '        End If
        '    Else
        '        qryObjICCompany.es.PageNumber = 1
        '        qryObjICCompany.es.PageSize = pagesize


        '        rg.DataSource = dt

        '        If DoDataBind Then


        '            rg.DataBind()

        '        End If
        '    End If
        'End Sub


        Public Shared Sub GetAllCompanyForRadGrid(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim dt As New DataTable

            qryObjICCompany.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName, qryObjICCompany.IsPaymentsAllowed, qryObjICCompany.IsCollectionAllowed, qryObjICCompany.IsCollectionApproved, qryObjICGroup.GroupName, qryObjICCompany.IsActive, qryObjICCompany.IsApprove)
            qryObjICCompany.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICCompany.OrderBy(qryObjICCompany.CompanyCode.Descending, qryObjICCompany.CompanyName.Descending, qryObjICGroup.GroupName.Descending)
            dt = qryObjICCompany.LoadDataTable
            If Not pagenumber = 0 Then
                qryObjICCompany.es.PageNumber = pagenumber

                qryObjICCompany.es.PageSize = pagesize


                rg.DataSource = dt


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else
                qryObjICCompany.es.PageNumber = 1
                qryObjICCompany.es.PageSize = pagesize


                rg.DataSource = dt

                If DoDataBind Then


                    rg.DataBind()

                End If
            End If
        End Sub



        Public Shared Function GetAllActiveCompaniesByGroupCode(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection

            ICComapnyColl.es.Connection.CommandTimeout = 3600


            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True)
            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function

        Public Shared Function GetCompanyActiveAndApproveByGroupCode(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection

            ICComapnyColl.es.Connection.CommandTimeout = 3600


            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True)

            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function
        Public Shared Function GetCompanyActiveAndApproveByGroupCodeAfterInitialTagging(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection

            ICComapnyColl.es.Connection.CommandTimeout = 3600


            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True And ICComapnyColl.Query.IsApprove = True)

            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function
        'Public Shared Sub GetAllCompany(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        '    Dim objFEISCity As New FEISCityCollection
        '    objFEISCity.es.Connection.CommandTimeout = 3600
        '    objFEISCity.LoadAll()
        '    objFEISCity.Query.OrderBy(objFEISCity.Query.CityCode, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    If Not pagenumber = 0 Then
        '        objFEISCity.Query.es.PageNumber = pagenumber
        '        objFEISCity.Query.es.PageSize = pagesize
        '        rg.DataSource = objFEISCity
        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        objFEISCity.Query.es.PageNumber = 1
        '        objFEISCity.Query.es.PageSize = pagesize
        '        rg.DataSource = objFEISCity
        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If
        '    End If


        'End Sub
        Public Shared Sub DeleteCompanyByCode(ByVal CompanyCode As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICCompany As New IC.ICCompany
            Dim StrCompanyName, StrCompanyCode As String
            objICCompany.es.Connection.CommandTimeout = 3600

            If objICCompany.LoadByPrimaryKey(CompanyCode) Then
                StrCompanyCode = objICCompany.CompanyCode.ToString
                StrCompanyName = objICCompany.CompanyName.ToString
                objICCompany.MarkAsDeleted()
                objICCompany.Save()
                ICUtilities.AddAuditTrail("Company : " & StrCompanyName.ToString & " and Code: " & StrCompanyCode.ToString & " deleted.", "Company", StrCompanyCode.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub
        Public Shared Sub ApproveCompany(ByVal CompanyCode As String, ByVal IsApproved As Boolean, ByVal ApprovedBy As String, ByVal UsersName As String)

            Dim objICCompany As New ICCompany

            objICCompany.es.Connection.CommandTimeout = 3600

            objICCompany.LoadByPrimaryKey(CompanyCode)
            objICCompany.IsApprove = IsApproved
            objICCompany.ApprovedBy = ApprovedBy
            objICCompany.ApprovedDate = Date.Now
            objICCompany.Save()
            If IsApproved = True Then
                ICUtilities.AddAuditTrail("Company :" & objICCompany.CompanyName & " approved.", "Company", objICCompany.CompanyCode.ToString, ApprovedBy.ToString, UsersName.ToString, "APPROVE")
            ElseIf IsApproved = False Then
                ICUtilities.AddAuditTrail("Company :" & objICCompany.CompanyName & " not approved.", "Company", objICCompany.CompanyCode.ToString, ApprovedBy.ToString, UsersName.ToString, "NOT APPROVE")
            End If
        End Sub
        'Public Shared Function GetAllActiveCompaniesByGroupCode(ByVal GroupCode As String) As ICCompanyCollection
        '    Dim ICComapnyColl As New ICCompanyCollection

        '    ICComapnyColl.es.Connection.CommandTimeout = 3600


        '    ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True)
        '    ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
        '    ICComapnyColl.Query.Load()
        '    Return ICComapnyColl
        'End Function
        Public Shared Function IsAllDataExistForCompanyToApprove(ByVal CompanyCode As String) As Boolean
            Dim Result As Boolean = False
            Dim qryObjAPNPtype As New ICAccountsPaymentNatureProductTypeQuery("qryObjAPNPtype")
            Dim qryObjAPNFileUploadTemplate As New ICAccountPayNatureFileUploadTemplateQuery("qryObjAPNFileUploadTemplate")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjAPNPtype.Select(qryObjAPNFileUploadTemplate.TemplateID)
            qryObjAPNPtype.InnerJoin(qryObjICAccounts).On(qryObjAPNPtype.AccountNumber = qryObjICAccounts.AccountNumber And qryObjAPNPtype.BranchCode = qryObjICAccounts.BranchCode And qryObjAPNPtype.Currency = qryObjICAccounts.Currency)
            qryObjAPNPtype.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjAPNPtype.InnerJoin(qryObjAPNFileUploadTemplate).On(qryObjAPNPtype.AccountNumber = qryObjAPNFileUploadTemplate.AccountNumber And qryObjAPNPtype.BranchCode = qryObjAPNFileUploadTemplate.BranchCode And qryObjAPNPtype.Currency = qryObjAPNFileUploadTemplate.Currency And qryObjAPNPtype.PaymentNatureCode = qryObjAPNFileUploadTemplate.PaymentNatureCode)
            qryObjAPNPtype.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            dt = qryObjAPNPtype.LoadDataTable
            If dt.Rows.Count > 0 Then
                Result = True
            End If
            Return Result
        End Function

     

#Region "Collection"


        Public Shared Function GetAllActiveandApproveCompaniesByGroupCode(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection
            ICComapnyColl.es.Connection.CommandTimeout = 3600

            ICComapnyColl.Query.Select(ICComapnyColl.Query.CompanyCode, ICComapnyColl.Query.CompanyName)
            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsCollectionAllowed = True)
            ICComapnyColl.Query.Where(ICComapnyColl.Query.IsActive = True And ICComapnyColl.Query.IsCollectionApproved = True)
            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function


        Public Shared Sub ApproveCompanyCollection(ByVal CompanyCode As String, ByVal IsCollectionApproved As Boolean, ByVal CollectionApprovedBy As String, ByVal UsersName As String)

            Dim objICCompany As New ICCompany

            objICCompany.es.Connection.CommandTimeout = 3600

            objICCompany.LoadByPrimaryKey(CompanyCode)
            objICCompany.IsCollectionApproved = IsCollectionApproved
            objICCompany.CollectionApprovedBy = CollectionApprovedBy
            objICCompany.CollectionApprovedDate = Date.Now
            objICCompany.Save()
            If IsCollectionApproved = True Then
                ICUtilities.AddAuditTrail("Company :" & objICCompany.CompanyName & " approved.", "Company", objICCompany.CompanyCode.ToString, CollectionApprovedBy.ToString, UsersName.ToString, "APPROVE")
            ElseIf IsCollectionApproved = False Then
                ICUtilities.AddAuditTrail("Company :" & objICCompany.CompanyName & " not approved.", "Company", objICCompany.CompanyCode.ToString, CollectionApprovedBy.ToString, UsersName.ToString, "NOT APPROVE")
            End If
        End Sub



#End Region

        'Public Shared Function DownLoadMT940FilesFromFTPLocationSaveInDB(ByVal FTPHost As String, ByVal FTPUserName As String, ByVal FTPPassword As String, ByVal FTPFolder As String, ByVal ComPanyCode As String) As Boolean
        '    Return True
        'End Function
        'MT 940
        Public Shared Sub DownloadAllMT940StatementsFromLocation()
            Try
                Dim dtMT940AllowedClients As New DataTable
                Dim objICCompany As New ICCompany
                dtMT940AllowedClients = GetAllActiveApproveCompaniesForMT940()
                For Each dr As DataRow In dtMT940AllowedClients.Rows

                    If dr("MT940StatementLocation") = "FTP" Then
                        If dr("MT940FetchingFrequency") = "Daily" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-1) Then
                                    If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        ElseIf dr("MT940FetchingFrequency") = "Weekly" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-8) Then
                                    If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        ElseIf dr("MT940FetchingFrequency") = "Monthly" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-30) Then
                                    If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromFTPLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        End If
                    ElseIf dr("MT940StatementLocation") = "Network" Then
                        If dr("MT940FetchingFrequency") = "Daily" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-1) Then
                                    If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        ElseIf dr("MT940FetchingFrequency") = "Weekly" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-8) Then
                                    If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        ElseIf dr("MT940FetchingFrequency") = "Monthly" Then
                            If dr("MT940FetchedDate").ToString <> "" Then
                                If CDate(dr("MT940FetchedDate")) = Date.Now.Date.AddDays(-30) Then
                                    If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                        objICCompany = New ICCompany
                                        objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                        objICCompany.MT940FetchedDate = Date.Now
                                        objICCompany.Save()
                                    End If
                                End If
                            Else
                                If DownLoadMT940FilesFromNetWorkLocationSaveInDB(dr("HostName").ToString, dr("UserName").ToString, dr("LocationPassword").ToString, dr("FolderName").ToString, dr("CompanyCode").ToString) = True Then
                                    objICCompany = New ICCompany
                                    objICCompany.LoadByPrimaryKey(dr("CompanyCode").ToString)
                                    objICCompany.MT940FetchedDate = Date.Now
                                    objICCompany.Save()
                                End If
                            End If
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function DownLoadMT940FilesFromFTPLocationSaveInDB(ByVal FTPHost As String, ByVal FTPUserName As String, ByVal FTPPassword As String, ByVal FTPFolder As String, ByVal ComPanyCode As String) As Boolean
            Dim ftp As FtpConnection
            Try

                Dim objICFile As New ICFiles
                Dim objICCompany As New ICCompany
                Dim aFileInfo As System.IO.FileInfo
                Dim fileInfo As FtpFileInfo

                Dim FileColl() As FtpFileInfo
                Dim NoOfFiles As Integer = 0
                Dim FileLocation As String = ""
                Dim MIMEType As String = ""
                Dim FileName As String = ""
                Dim AuditType As String = Nothing
                Dim Action As String = Nothing
                Dim AccountNo, BranchCode, Currency As String
                FileLocation = ICUtilities.GetSettingValue("PhysicalApplicationPath").ToString() & "UploadedFiles\"
                ftp = New FtpConnection(FTPHost, FTPUserName, FTPPassword)

                ftp.Open()
                ftp.Login()

                If Not (FTPFolder = "") Then
                    ftp.SetCurrentDirectory(FTPFolder.ToString())
                End If
                FileColl = ftp.GetFiles()
                NoOfFiles = 0
                objICCompany.LoadByPrimaryKey(ComPanyCode)
                For Each fileInfo In FileColl
                    Dim FileInfo2 As System.IO.FileInfo
                    FileName = fileInfo.Name.ToString
                    FileInfo2 = New IO.FileInfo(FileLocation & FileName.ToString())
                    If FileInfo2.Exists Then
                        FileInfo2.Delete()
                    End If
                    AccountNo = Nothing
                    BranchCode = Nothing
                    Currency = Nothing
                    If fileInfo.Extension = ".txt" Then
                        Action = Nothing
                        ftp.GetFile(fileInfo.Name.ToString(), FileLocation & FileName, True)
                        Action = "Client File: File [Name: " & fileInfo.Name.ToString() & "] of Company [Code: " & ComPanyCode.ToString() & " ; Name: " & objICCompany.CompanyName.ToString() & "] User [ID: " & FTPUserName & " ; Name: " & "FTPLocation: " & FTPHost & " \ " & FTPFolder & "]."
                        AuditType = "Client MT940 FTP File"
                        objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "MT940 File", "ICFileID", fileInfo.Name.ToString, Nothing, Nothing, Action.ToString(), "ADD")
                        Dim cICFiles As New ICFiles
                        cICFiles.LoadByPrimaryKey(objICFile.FileID)
                        cICFiles.Status = "Uploaded"
                        cICFiles.AcqMode = "FTP MT 940 File Upload"
                        cICFiles.FileBatchNo = Nothing
                        cICFiles.FileName = FileName
                        cICFiles.OriginalFileName = fileInfo.Name.ToString()
                        cICFiles.FileLocation = FileLocation & FileName.ToString()
                        cICFiles.CompanyCode = ComPanyCode.ToString()
                        cICFiles.AccountNumber = AccountNo
                        cICFiles.BranchCode = BranchCode
                        cICFiles.Currency = Currency
                        NoOfFiles = NoOfFiles + 1
                        ftp.RemoveFile(fileInfo.Name)
                        Action = "MT 940 File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ;"
                        Action += " AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ;"
                        Action += " FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & ""
                        AuditType = "Client MT940 FTP File"
                        ICFilesController.UpdateClientFile(cICFiles, Nothing, Nothing, Action.ToString(), AuditType)
                    Else
                        Dim objFileInfo As IO.FileInfo
                        objFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                        'Delete file from HDD.
                        If objFileInfo.Exists Then
                            objFileInfo.Delete()
                        End If
                        ftp.RemoveFile(fileInfo.Name)
                        ICUtilities.AddAuditTrail("Unable to Download MT 940 FTPLocation: " & FTPHost & " \ " & FTPFolder & " ; due to [Invalid file format.]", "Client MT940 FTP File", Nothing, Nothing, Nothing, "ERROR")
                        Continue For
                    End If
                Next
                ftp.Close()
                ftp.Dispose()
                Return True
            Catch ex As Exception
                ICUtilities.AddAuditTrail("Unable to Download MT 940 FTPLocation: " & FTPHost & " \ " & FTPFolder & " ; due to " & ex.Message.ToString & "", "Client MT940 FTP File", Nothing, Nothing, Nothing, "ERROR")

                Return True
                Exit Function
            End Try
        End Function
        Public Shared Function DownLoadMT940FilesFromNetWorkLocationSaveInDB(ByVal FTPHost As String, ByVal FTPUserName As String, ByVal FTPPassword As String, ByVal FTPFolder As String, ByVal ComPanyCode As String) As Boolean

            Try

                Dim objICFile As New ICFiles
                Dim objICCompany As New ICCompany
                Dim aFileInfo As FileInfo
                Dim Dir As DirectoryInfo
                Dim fileInfo As FileInfo
                Dim objFileInfo As IO.FileInfo
                Dim St As System.IO.Stream
                Dim FileColl() As FileInfo
                Dim NoOfFiles As Integer = 0
                Dim FileLocation As String = ""
                Dim MIMEType As String = ""
                Dim FileName As String = ""
                Dim AuditType As String = Nothing
                Dim Action As String = Nothing
                Dim AccountNo, BranchCode, Currency As String
                FileLocation = ICUtilities.GetSettingValue("PhysicalApplicationPath").ToString() & "UploadedFiles\"
             
                Dir = New DirectoryInfo("\\" & FTPHost & "\" & FTPFolder)
                If Dir.Exists Then
                    FileColl = Dir.GetFiles("*.txt")
                Else

                    FileColl = Nothing
                End If
                NoOfFiles = 0
                objICCompany.LoadByPrimaryKey(ComPanyCode)
                For Each fileInfo In FileColl
                    Dim FileInfo2 As System.IO.FileInfo
                    FileName = fileInfo.Name.ToString
                    FileInfo2 = New IO.FileInfo(FileLocation & FileName.ToString())
                    If FileInfo2.Exists Then
                        FileInfo2.Delete()
                    End If

                    fileInfo.CopyTo(FileLocation & fileInfo.Name)


                    AccountNo = Nothing
                    BranchCode = Nothing
                    Currency = Nothing
                    If fileInfo.Extension = ".txt" Then
                        Action = Nothing
                        'ICMT940FilesController.CreatDownloadedMT940Files(fileInfo.FullName.ToString)
                        Action = "Client MT940 NetWork File: File [Name: " & fileInfo.Name.ToString() & "] of Company [Code: " & ComPanyCode.ToString() & " ; Name: " & objICCompany.CompanyName.ToString() & "] User Name: " & FTPUserName & " ;" & "NetWork Location: " & FTPHost & " \ " & FTPFolder & "]."
                        AuditType = "Client MT940 NetWork File"
                        objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(fileInfo.FullName, "MT940 File", "ICFileID", fileInfo.Name.ToString, Nothing, Nothing, Action.ToString(), "ADD")
                        Dim cICFiles As New ICFiles
                        cICFiles.LoadByPrimaryKey(objICFile.FileID)
                        cICFiles.Status = "Uploaded"
                        cICFiles.AcqMode = "NetWork MT 940 File Upload"
                        cICFiles.FileBatchNo = Nothing
                        cICFiles.FileName = FileName
                        cICFiles.OriginalFileName = fileInfo.Name.ToString()
                        cICFiles.FileLocation = FileLocation & FileName.ToString()
                        cICFiles.CompanyCode = ComPanyCode.ToString()
                        cICFiles.AccountNumber = AccountNo
                        cICFiles.BranchCode = BranchCode
                        cICFiles.Currency = Currency
                        NoOfFiles = NoOfFiles + 1
                        objFileInfo = New IO.FileInfo(fileInfo.FullName)
                        If objFileInfo.Exists Then
                            objFileInfo.Delete()
                        End If
                        Action = "Client MT940 NetWork File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ;"
                        Action += " AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ;"
                        Action += " FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & ""
                        AuditType = "Client MT940 NetWork File"
                        ICFilesController.UpdateClientFile(cICFiles, Nothing, Nothing, Action.ToString(), AuditType)
                    Else
                        objFileInfo = New IO.FileInfo(fileInfo.FullName)
                        If objFileInfo.Exists Then
                            objFileInfo.Delete()
                        End If
                        ICUtilities.AddAuditTrail("Unable to Download MT 940 NetWork: " & FTPHost & " \ " & FTPFolder & " ; due to [Invalid file format.]", "Client MT940 NetWork File", Nothing, Nothing, Nothing, "ERROR")
                        Continue For
                    End If
                Next
                Return True
            Catch ex As Exception
                ICUtilities.AddAuditTrail("Unable to Download MT 940 NetWork: " & FTPHost & " \ " & FTPFolder & " ; due to " & ex.Message.ToString & "", "Client MT940 NetWork File", Nothing, Nothing, Nothing, "ERROR")

                Return True
                Exit Function
            End Try
        End Function
        
        Public Shared Function GetAllActiveApproveCompaniesForMT940() As DataTable
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            qryObjICCompany.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName, qryObjICCompany.HostName, qryObjICCompany.MT940StatementLocation)
            qryObjICCompany.Select(qryObjICCompany.UserName, qryObjICCompany.LocationPassword, qryObjICCompany.FolderName)
            qryObjICCompany.Select(qryObjICCompany.MT940FetchedDate.Date, qryObjICCompany.MT940FetchingFrequency)
            'qryObjICCompany.Select(qryObjICCompany.MT940FetchingFrequency.Case.When("Daily").Then(qryObjICCompany.MT940FetchedDate.Date = Date.Now.Date.AddDays(-1)).When("Monthly").Then(qryObjICCompany.MT940FetchedDate.Date = Date.Now.Date.AddMonths(-1).AddDays(-1)).When("Weekly").Then(qryObjICCompany.MT940FetchedDate.Case.When(qryObjICCompany.MT940FetchedDate.IsNotNull).Then(qryObjICCompany.MT940FetchedDate.Date = Date.Now.AddDays(-8))).End.As("MT940FetchedDate"))
            qryObjICCompany.Where(qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICCompany.IsMT940StatementAllowed = True)
            qryObjICCompany.GroupBy(qryObjICCompany.MT940FetchingFrequency, qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName, qryObjICCompany.HostName)
            qryObjICCompany.GroupBy(qryObjICCompany.MT940StatementLocation, qryObjICCompany.UserName, qryObjICCompany.LocationPassword, qryObjICCompany.FolderName)
            qryObjICCompany.GroupBy(qryObjICCompany.MT940FetchedDate.Date)
            Return qryObjICCompany.LoadDataTable
        End Function
        
        Public Shared Function IsValidAccountNoByCompanyCode(ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal CompanyCode As String) As Boolean
            Dim Result As Boolean = False
            Dim objICAccounts As New ICAccounts


            objICAccounts.Query.Where(objICAccounts.Query.AccountNumber = AccountNo And objICAccounts.Query.BranchCode = BranchCode And objICAccounts.Query.Currency = Currency And objICAccounts.Query.CompanyCode = CompanyCode)
            If objICAccounts.Query.Load() Then
                Result = True
            End If



            Return Result
        End Function
    End Class
End Namespace
