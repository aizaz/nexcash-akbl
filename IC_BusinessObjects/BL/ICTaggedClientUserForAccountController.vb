﻿Imports Telerik.Web.UI

Namespace IC

    Public Class ICTaggedClientUserForAccountController
        'CREATE THIS CLASS FOR IC_TaggedClientUserForAccount TABLE uses in Account Management Module 
        'for adding, updating and deleting tagged client user for accounts By JAWWAD 11-03-2014



        'created method for adding and updating value in db field of IC_TaggedClientUserForAccount TABLE
        Public Shared Sub AddTaggedClientUserForAccount(ByVal ICTaggedClientUserAccount As IC.ICTaggedClientUserForAccount, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTaggedClientUserForAccount As New ICTaggedClientUserForAccount
            Dim prevobjICTaggedClientUserForAccount As New IC.ICTaggedClientUserForAccount
            objICTaggedClientUserForAccount.es.Connection.CommandTimeout = 3600

            If IsUpDate = False Then

                objICTaggedClientUserForAccount.CreateBy = ICTaggedClientUserAccount.CreateBy
                objICTaggedClientUserForAccount.CreateDate = ICTaggedClientUserAccount.CreateDate
                objICTaggedClientUserForAccount.Creater = ICTaggedClientUserAccount.Creater
                objICTaggedClientUserForAccount.CreationDate = ICTaggedClientUserAccount.CreationDate
            Else
                objICTaggedClientUserForAccount.LoadByPrimaryKey(ICTaggedClientUserAccount.TaggesUserID)
                prevobjICTaggedClientUserForAccount.LoadByPrimaryKey(ICTaggedClientUserAccount.TaggesUserID)
                objICTaggedClientUserForAccount.CreateBy = ICTaggedClientUserAccount.CreateBy
                objICTaggedClientUserForAccount.CreateDate = ICTaggedClientUserAccount.CreateDate


            End If
            objICTaggedClientUserForAccount.AccountNumber = ICTaggedClientUserAccount.AccountNumber
            objICTaggedClientUserForAccount.BranchCode = ICTaggedClientUserAccount.BranchCode
            objICTaggedClientUserForAccount.Currency = ICTaggedClientUserAccount.Currency
            objICTaggedClientUserForAccount.UserID = ICTaggedClientUserAccount.UserID
            objICTaggedClientUserForAccount.IsActive = ICTaggedClientUserAccount.IsActive
            objICTaggedClientUserForAccount.Save()

            If IsUpDate = False Then

                CurrentAt = "Client User With ID : [ " & objICTaggedClientUserForAccount.UserID & "] Tagged with Client Account No : [" & objICTaggedClientUserForAccount.AccountNumber & "] Branch Code : [ " & objICTaggedClientUserForAccount.BranchCode & "] Currency : [ " & objICTaggedClientUserForAccount.Currency & "] with ID : [" & objICTaggedClientUserForAccount.TaggesUserID & " ]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added", "Account Tagged User", objICTaggedClientUserForAccount.TaggesUserID.ToString, UsersID, UsersName, "ADD")


                ' ElseIf IsUpDate = True Then

                'CurrentAt = "Client User With ID: [ " & objICTaggedClientUserForAccount.UserID.ToString() & "] Tagged with Client Account No :[ " & objICTaggedClientUserForAccount.AccountNumber.ToString() & "] Branch Code :[ " & objICTaggedClientUserForAccount.BranchCode.ToString() & "] Currency :[ " & objICTaggedClientUserForAccount.Currency.ToString() & " ] with ID: [" & objICTaggedClientUserForAccount.TaggesUserID.ToString() & "]"
                'PrevAt = "<br />TaggesUserID : Previous Values [ID:  " & prevobjICTaggedClientUserForAccount.TaggesUserID & " ]"

                ' CurrentAt = "TaggesUserID : Current Values [Client User With ID:  " & objICTaggedClientUserForAccount.UserID.ToString() & "; Tagged with Client Account No: " & objICTaggedClientUserForAccount.AccountNumber.ToString() & "; Brancg Code: " & objICTaggedClientUserForAccount.BranchCode.ToString() & "; Currency : " & objICTaggedClientUserForAccount.Currency.ToString() & "; with ID : " & objICTaggedClientUserForAccount.TaggesUserID.ToString() & "   ]"
                ' PrevAt = "<br />TaggesUserID : Previous Values [Client User With ID:  " & prevobjICTaggedClientUserForAccount.UserID.ToString() & "; Tagged with Client Account No : " & prevobjICTaggedClientUserForAccount.AccountNumber.ToString() & "; Branch Code : " & prevobjICTaggedClientUserForAccount.BranchCode.ToString() & "; Currency : " & prevobjICTaggedClientUserForAccount.Currency.ToString() & "; with ID : " & objICTaggedClientUserForAccount.TaggesUserID.ToString() & " ]"

                ' ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Account Tagged User", objICTaggedClientUserForAccount.TaggesUserID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
                'ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Account Tagged User", objICTaggedClientUserForAccount.AccountNumber.ToString + "-" + objICTaggedClientUserForAccount.BranchCode + "-" + objICTaggedClientUserForAccount.Currency, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


        End Sub

        'created method for loading gv in add page of Accounts Management Module
        Public Shared Sub GetTaggedAccountUser(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal CompanyCode As String)
            Dim qryObjICUser As New ICUserQuery("qryObjICUserr")
            Dim qryObjICTaggedUser As New ICTaggedClientUserForAccountQuery("qryObjICTaggedUser")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As New DataTable

            qryObjICTaggedUser.Select(qryObjICTaggedUser.TaggesUserID, qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
            qryObjICTaggedUser.InnerJoin(qryObjICUser).On(qryObjICTaggedUser.UserID = qryObjICUser.UserID)
            qryObjICTaggedUser.InnerJoin(qryObjICAccounts).On(qryObjICTaggedUser.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICTaggedUser.BranchCode = qryObjICAccounts.BranchCode And qryObjICTaggedUser.Currency = qryObjICAccounts.Currency)
            qryObjICTaggedUser.Where(qryObjICTaggedUser.AccountNumber = AccountNo And qryObjICTaggedUser.BranchCode = BranchCode And qryObjICTaggedUser.Currency = Currency)
            qryObjICTaggedUser.Where(qryObjICAccounts.CompanyCode = CompanyCode)
            dt = qryObjICTaggedUser.LoadDataTable()

            If Not pagenumber = 0 Then
                qryObjICTaggedUser.es.PageNumber = pagenumber

                qryObjICTaggedUser.es.PageSize = pagesize


                rg.DataSource = dt


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else
                qryObjICTaggedUser.es.PageNumber = 1
                qryObjICTaggedUser.es.PageSize = pagesize


                rg.DataSource = dt

                If DoDataBind Then


                    rg.DataBind()

                End If
            End If
        End Sub


        'Public Shared Function GetTaggedAccountUser(ByVal AccountNumber As String) As DataTable

        '    Dim qryObjICUser As New ICUserQuery("qryObjICUserr")
        '    'Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    'Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    'Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim qryObjICTaggedUser As New ICTaggedClientUserForAccountQuery("qryObjICTaggedUser")
        '    Dim dt As New DataTable

        '    qryObjICTaggedUser.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName)
        '    qryObjICTaggedUser.InnerJoin(qryObjICUser).On(qryObjICTaggedUser.AccountNumber = qryObjICUser.UserID)
        '    'qryObjICUser.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
        '    'qryObjICUser.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    'qryObjICUser.Where(qryObjICUser.UserType = "Client User" And qryObjICUser.IsActive = True And qryObjICUser.IsApproved = True)

        '    qryObjICTaggedUser.Where(qryObjICTaggedUser.AccountNumber = AccountNumber.ToString)
        '    'If IsUpdate = True Then
        '    '    qryObjICUser.Where(qryObjICUser.UserID.NotIn(qryObjICTaggedUser.[Select](qryObjICTaggedUser.UserID).Where(qryObjICTaggedUser.AccountNumber = AccountNo And qryObjICTaggedUser.BranchCode = BranchCode And qryObjICTaggedUser.Currency = Currency)))
        '    'End If
        '    dt = qryObjICUser.LoadDataTable()
        '    Return dt

        'End Function

        'created method for deleting user in add page gv in add page of Accounts Management Module
        Public Shared Sub DeletegvTaggedClientUser(ByVal TaggesUserID As String, ByVal UserID As String, ByVal UserName As String)

            Dim icTaggesClientUser As New ICTaggedClientUserForAccount
            Dim CountryName As String = ""
            Dim CurrentAt As String

            icTaggesClientUser.es.Connection.CommandTimeout = 3600

            icTaggesClientUser.LoadByPrimaryKey(TaggesUserID)
            CountryName = icTaggesClientUser.TaggesUserID.ToString()
            CurrentAt = "Client User with ID :[ " & icTaggesClientUser.UserID.ToString() & " ] Tagged with Client Account No: [ " & icTaggesClientUser.AccountNumber.ToString() & " ]"
            CurrentAt += " Branch Code: [ " & icTaggesClientUser.BranchCode.ToString() & " ] Currency: [ " & icTaggesClientUser.Currency.ToString() & " ] with ID: [ " & icTaggesClientUser.TaggesUserID.ToString() & " ]"
            'CurrentAt = "Client User : [ID:  " & icTaggesClientUser.TaggesUserID.ToString() & "; Account Number : " & icTaggesClientUser.AccountNumber.ToString() & "; Branch Code : " & icTaggesClientUser.BranchCode.ToString() & "; Currency : " & icTaggesClientUser.Currency.ToString() & " ]"

            icTaggesClientUser.MarkAsDeleted()
            icTaggesClientUser.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted  ", "Account Tagged User", icTaggesClientUser.TaggesUserID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")



        End Sub

        Public Shared Sub DeleteAlreadyTaggedClientUserinGVonddselectindexchange(ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal Companycode As String, ByVal UserID As String, ByVal UserName As String)

            Dim collAccounts As New IC.ICAccountsQuery("collAccounts")
            Dim collTaggesClientUser As New IC.ICTaggedClientUserForAccountQuery("collTaggesClientUser")
            Dim TaggesClientUser As ICTaggedClientUserForAccount
            Dim ActionString As String
            Dim dt As New DataTable



            collTaggesClientUser.Select(collTaggesClientUser.TaggesUserID, collTaggesClientUser.AccountNumber, collTaggesClientUser.BranchCode, collTaggesClientUser.Currency)
            collTaggesClientUser.InnerJoin(collAccounts).On(collTaggesClientUser.AccountNumber = collAccounts.AccountNumber And collTaggesClientUser.BranchCode = collAccounts.BranchCode And collTaggesClientUser.Currency = collAccounts.Currency)
            collTaggesClientUser.Where(collAccounts.AccountNumber = AccountNo And collAccounts.BranchCode = BranchCode And collAccounts.Currency = Currency)
            collTaggesClientUser.Where(collAccounts.CompanyCode = Companycode)
            dt = collTaggesClientUser.LoadDataTable
            If dt.Rows.Count > 0 Then

                For Each dr As DataRow In dt.Rows
                    ActionString = Nothing
                    TaggesClientUser = New ICTaggedClientUserForAccount
                    TaggesClientUser.LoadByPrimaryKey(dr("TaggesUserID").ToString)
                    ActionString = "Client User with ID :[ " & TaggesClientUser.UserID.ToString() & " ] Tagged with Client Account No: [ " & TaggesClientUser.AccountNumber.ToString() & " ]"
                    ActionString += " Branch Code: [ " & TaggesClientUser.BranchCode.ToString() & " ] Currency: [ " & TaggesClientUser.Currency.ToString() & " ] with ID: [ " & TaggesClientUser.TaggesUserID.ToString() & " ]"
                    ICUtilities.AddAuditTrail(ActionString & " Deleted  ", "Account Tagged User", TaggesClientUser.TaggesUserID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

                    TaggesClientUser.MarkAsDeleted()
                    TaggesClientUser.Save()
                Next

            End If



        End Sub


        ' created this function for checking time of the day to send notification to client users those are tagged with account
        'Public Shared Function SendDailyAccountBalanceToTaggedClientUser()

        '    'Dim objSendDailyAcntBalToTaggedClient As New IC.ICTaggedClientUserForAccountCollection

        '    'objSendDailyAcntBalToTaggedClient.es.Connection.CommandTimeout = 3600

        '    '    'objSendDailyAcntBalToTaggedClient.Query.Where(objSendDailyAcntBalToTaggedClient.Query.AccountNumber = Date.Now.Date)
        '    '    'If objSendDailyAcntBalToTaggedClient.Query.Load() Then
        '    '    '    Return objSendDailyAcntBalToTaggedClient
        '    '    'Else
        '    '    '    Return Nothing
        '    '    'End If




        '    '    'Dim date1 As DateTime = #12:00:00 PM#
        '    '    ''Dim date2 As DateTime = #12:00:00 PM#
        '    '    'Dim result As Date = System.DateTime.Now
        '    '    If (System.DateTime.Now = #2:00:00 PM#) Then

        '    '        Return System.DateTime.Now

        '    '    End If




        '    If TimeOfDay >= #3:30:00 PM# Then

        '        Return TimeOfDay

        '    End If



        '    'Dim compareTime As String = "1:42:21 PM"

        '    'If compareTime = DateTime.Now.ToString("1:42:21 PM") Then

        '    '    Return compareTime

        '    'End If



        '    'Dim myCompareTime As DateTime = DateTime.Parse("1:42:21 PM")

        '    'If myCompareTime.TimeOfDay = DateTime.Now.TimeOfDay Then

        '    '    Return myCompareTime.TimeOfDay()

        '    'End If


        'End Function

        'Public Shared Function TaggedClientUserForAccountNotification()


        '    'Dim date1 As DateTime = #12:00:00 PM#
        '    'If TimeOfDay >= #3:30:00 PM# Then

        '    '    Return TimeOfDay

        '    'End If

        '    Dim date1 As DateTime = Convert.ToDateTime("12:00:00 PM").Hour & ":" & Convert.ToDateTime("12:00:00 PM").Minute
        '    If TimeOfDay >= date1 Then

        '        Return date1

        '    End If

        'End Function

        Public Shared Sub SendDailyAccountBalanceToTaggedClientUser()

            Try
                Dim objICAccountsColl As New ICAccountsCollection
                Dim AccountBalance As String
                Dim ActionString As String
                objICAccountsColl = ICAccountsController.GetAllAccountNosWithDailyAccountBalanceAllow

                If objICAccountsColl.Count > 0 Then
                    For Each objICAccounts As ICAccounts In objICAccountsColl
                        AccountBalance = Nothing
                        Try
                            AccountBalance = CBUtilities.BalanceInquiry(objICAccounts.AccountNumber.ToString, CBUtilities.AccountType.RB, "Client Account Balance", objICAccounts.AccountNumber, objICAccounts.BranchCode)
                            If AccountBalance <> -1 Then
                                EmailUtilities.SendDailyAccountBalanceVIAEmailToTaggedUsers(objICAccounts.AccountNumber, AccountBalance, objICAccounts.BranchCode, objICAccounts.Currency)
                                SMSUtilities.SendDailyAccountBalanceVIASmsToTaggedUsers(objICAccounts.AccountNumber, AccountBalance, objICAccounts.BranchCode, objICAccounts.Currency)
                                objICAccounts.NotificationSendDate = Date.Now
                                objICAccounts.Save()
                            Else
                                ActionString = Nothing
                                ActionString = "Balance Inquiry fails for account no " & objICAccounts.AccountNumber & " to send daily account balance"
                                ICUtilities.AddAuditTrail(ActionString, "Accounts", objICAccounts.AccountNumber, Nothing, Nothing, "Balance Inquiry")
                                objICAccounts.NotificationSendDate = Date.Now
                                objICAccounts.Save()
                            End If
                        Catch ex As Exception
                            ActionString = Nothing
                            ActionString = "Balance Inquiry fails for account no " & objICAccounts.AccountNumber & " to send daily account balance"
                            ICUtilities.AddAuditTrail(ActionString, "Accounts", objICAccounts.AccountNumber, Nothing, Nothing, "Balance Inquiry")
                            objICAccounts.NotificationSendDate = Date.Now
                            objICAccounts.Save()
                        End Try
                    Next
                End If
            Catch ex As Exception
                'ActionString = Nothing
                'ActionString = "Fail to send Balance due to " & ex.Message
                'ICUtilities.AddAuditTrail(ActionString, "Accounts", objICAccounts.AccountNumber, Nothing, Nothing, "Balance Inquiry")
                Exit Sub
            End Try
        End Sub

    End Class
End Namespace