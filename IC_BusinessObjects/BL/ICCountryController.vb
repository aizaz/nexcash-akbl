﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class ICCountryController
        Public Shared Function GetCountryCodeByCountryName(ByRef CountryName As String) As String
            Dim collName As New ICCountryCollection
            collName.Query.Where(collName.Query.CountryName.ToLower = CountryName.ToLower.ToString())
            If collName.Query.Load() Then
                Return collName(0).CountryCode
            Else
                Return ""
            End If
        End Function
        Public Shared Sub AddCountry(ByVal cCountry As IC.ICCountry, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICCountry As New IC.ICCountry
            Dim prevICCountry As New IC.ICCountry
            Dim IsActiveText As String
            Dim IsApprovedText As String
            Dim CurrentAt As String
            Dim PrevAt As String

            ICCountry.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICCountry.CreateBy = cCountry.CreateBy
                ICCountry.CreateDate = cCountry.CreateDate
                ICCountry.Creater = cCountry.Creater
                ICCountry.CreationDate = cCountry.CreationDate
            Else
                ICCountry.LoadByPrimaryKey(cCountry.CountryCode)
                prevICCountry.LoadByPrimaryKey(cCountry.CountryCode)
                ICCountry.CreateBy = cCountry.CreateBy
                ICCountry.CreateDate = cCountry.CreateDate

            End If

            ICCountry.DisplayCode = cCountry.DisplayCode
            ICCountry.CountryName = cCountry.CountryName
            ICCountry.IsActive = cCountry.IsActive
            ICCountry.IsApproved = False

            If ICCountry.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If ICCountry.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            ICCountry.Save()

            If (isUpdate = False) Then
                CurrentAt = "Country : [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & ICCountry.IsActive.ToString() & " ; IsApproved:  " & ICCountry.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")


            Else

                CurrentAt = "Country : Current Values [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                PrevAt = "<br />Country : Previous Values [Code:  " & prevICCountry.CountryCode.ToString() & " ; Name:  " & prevICCountry.CountryName.ToString() & " ; IsActive:  " & prevICCountry.IsActive.ToString() & " ; IsApproved:  " & prevICCountry.IsApproved.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If


        End Sub
        'Public Shared Sub AddCountry(ByVal cCountry As IC.ICCountry, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

        '    Dim ICCountry As New IC.ICCountry
        '    Dim prevICCountry As New IC.ICCountry
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim CurrentAt As String
        '    Dim PrevAt As String

        '    ICCountry.es.Connection.CommandTimeout = 3600

        '    If (isUpdate = False) Then
        '       ICCountry.CreateBy = cCountry.CreateBy
        '        ICCountry.CreateDate = cCountry.CreateDate
        '        ICCountry.Creater = cCountry.Creater
        '        ICCountry.CreationDate = cCountry.CreationDate
        '    Else
        '        ICCountry.LoadByPrimaryKey(cCountry.CountryCode)
        '        prevICCountry.LoadByPrimaryKey(cCountry.CountryCode)
        '        ICCountry.CreateBy = cCountry.CreateBy
        '        ICCountry.CreateDate = cCountry.CreateDate
        '    End If

        '    ICCountry.CountryName = cCountry.CountryName
        '    ICCountry.IsActive = cCountry.IsActive
        '    ICCountry.IsApproved = False

        '    If ICCountry.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If ICCountry.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    ICCountry.Save()

        '    If (isUpdate = False) Then
        '       CurrentAt = "Country : [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & ICCountry.IsActive.ToString() & " ; IsApproved:  " & ICCountry.IsApproved.ToString() & "]"
        '        ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")


        '    Else

        '        CurrentAt = "Country : Current Values [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '        PrevAt = "<br />Country : Previous Values [Code:  " & prevICCountry.CountryCode.ToString() & " ; Name:  " & prevICCountry.CountryName.ToString() & " ; IsActive:  " & prevICCountry.IsActive.ToString() & " ; IsApproved:  " & prevICCountry.IsApproved.ToString() & "] "
        '        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '    End If


        'End Sub
        Public Shared Sub DeleteCountry(ByVal CountryCode As String, ByVal UserID As String, ByVal UserName As String)

            Dim ICCountry As New ICCountry
            Dim CountryName As String = ""
            Dim CurrentAt As String

            ICCountry.es.Connection.CommandTimeout = 3600

            ICCountry.LoadByPrimaryKey(CountryCode)
            CountryName = ICCountry.CountryName.ToString()

            CurrentAt = "Country : [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & ICCountry.IsActive.ToString() & " ; IsApproved:  " & ICCountry.IsApproved.ToString() & "]"

            ICCountry.MarkAsDeleted()
            ICCountry.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")


        End Sub
        Public Shared Sub ApproveCountry(ByVal CountryCode As String, ByVal isapproved As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICCountry As New IC.ICCountry
            Dim CurrentAt As String
            ICCountry.es.Connection.CommandTimeout = 3600

            ICCountry.LoadByPrimaryKey(CountryCode)
            ICCountry.IsApproved = isapproved
            ICCountry.ApprovedBy = UserID
            ICCountry.ApprovedOn = Date.Now

            ICCountry.Save()
            CurrentAt = "Country : [Code:  " & ICCountry.CountryCode.ToString() & " ; Name:  " & ICCountry.CountryName.ToString() & " ; IsActive:  " & ICCountry.IsActive.ToString() & " ; IsApproved:  " & ICCountry.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Country", ICCountry.CountryCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")


        End Sub
        Public Shared Function GetCountryddl() As ICCountryCollection

            Dim ICCountry As New IC.ICCountryCollection

            ICCountry.es.Connection.CommandTimeout = 3600

            ICCountry.Query.OrderBy(ICCountry.Query.CountryName.Ascending)
            ICCountry.LoadAll()
            Return ICCountry

        End Function
        Public Shared Sub GetCountrygv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


            Dim collCountry As New ICCountryCollection

            If Not pagenumber = 0 Then
                collCountry.Query.Select(collCountry.Query.CountryCode, collCountry.Query.CountryName, collCountry.Query.IsActive, collCountry.Query.IsApproved)
                collCountry.Query.Select(collCountry.Query.DisplayCode)
                collCountry.Query.OrderBy(collCountry.Query.CountryName.Ascending)
                collCountry.Query.Load()
                rg.DataSource = collCountry

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collCountry.Query.es.PageNumber = 1
                collCountry.Query.es.PageSize = pagesize
                collCountry.Query.OrderBy(collCountry.Query.CountryName.Ascending)
                collCountry.Query.Load()
                rg.DataSource = collCountry


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub

        'Public Shared Sub GetCountrygv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


        '    Dim collCountry As New ICCountryCollection

        '    If Not pagenumber = 0 Then
        '        collCountry.Query.Select(collCountry.Query.CountryCode, collCountry.Query.CountryName, collCountry.Query.IsActive, collCountry.Query.IsApproved)
        '        collCountry.Query.OrderBy(collCountry.Query.CountryName.Ascending)
        '        collCountry.Query.Load()
        '        rg.DataSource = collCountry

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        collCountry.Query.es.PageNumber = 1
        '        collCountry.Query.es.PageSize = pagesize
        '        collCountry.Query.OrderBy(collCountry.Query.CountryName.Ascending)
        '        collCountry.Query.Load()
        '        rg.DataSource = collCountry


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    End If
        'End Sub

        Public Shared Function GetCountryName(ByVal CountryCode As String) As String

            Dim FRCCountry As New IC.ICCountry

            FRCCountry.es.Connection.CommandTimeout = 3600

            FRCCountry.LoadByPrimaryKey(CountryCode)
            If FRCCountry.LoadByPrimaryKey(CountryCode) = True Then
                Return FRCCountry.CountryName.ToString()
            Else
                Return ""
            End If



        End Function

        Public Shared Function GetCountryActiveAndApproved() As IC.ICCountryCollection

            Dim ICCountry As New IC.ICCountryCollection

            ICCountry.es.Connection.CommandTimeout = 3600

            ICCountry.Query.Where(ICCountry.Query.IsActive = True And ICCountry.Query.IsApproved = True)
            ICCountry.Query.OrderBy(ICCountry.Query.CountryName.Ascending)
            ICCountry.Query.Load()
            Return ICCountry


        End Function
        Public Shared Function GetPakistan() As IC.ICCountryCollection

            Dim FRCCountry As New IC.ICCountryCollection

            FRCCountry.es.Connection.CommandTimeout = 3600

            FRCCountry.Query.Where(FRCCountry.Query.IsActive = True And FRCCountry.Query.IsApproved = True And FRCCountry.Query.CountryName.ToLower = "pakistan")
            FRCCountry.Query.Load()
            Return FRCCountry


        End Function
        ' Aizaz Ahmed [Dated: 12-Feb-2013]
        Public Shared Function IsCountryExistByCountryNameOrCode(ByVal Country As String) As Boolean
            Dim objICCountry As New ICCountry
            objICCountry.es.Connection.CommandTimeout = 3600
            Dim result As Integer
            If Integer.TryParse(Country, result) = True Then
                objICCountry.Query.Where(objICCountry.Query.CountryCode = Country And objICCountry.Query.IsActive = True And objICCountry.Query.IsApproved = True)
                If objICCountry.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            Else
                objICCountry = New ICCountry
                objICCountry.Query.Where(objICCountry.Query.CountryName.ToLower = Country.ToLower.ToString() And objICCountry.Query.IsActive = True And objICCountry.Query.IsApproved = True)
                If objICCountry.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            End If

        End Function
        Public Shared Function GetCountryByCountryNameOrCode(ByVal Country As String) As ICCountry
            Dim objICCountry As New ICCountry
            objICCountry.es.Connection.CommandTimeout = 3600
            Dim result As Integer
            If Integer.TryParse(Country, result) = True Then
                objICCountry.Query.Where(objICCountry.Query.CountryCode = Country And objICCountry.Query.IsActive = True And objICCountry.Query.IsApproved = True)
                If objICCountry.Query.Load() Then
                    Return objICCountry
                Else
                    Return Nothing
                End If
            Else
                objICCountry = New ICCountry
                objICCountry.Query.Where(objICCountry.Query.CountryName.ToLower = Country.ToLower.ToString() And objICCountry.Query.IsActive = True And objICCountry.Query.IsApproved = True)
                If objICCountry.Query.Load() Then
                    Return objICCountry
                Else
                    Return Nothing
                End If
            End If

        End Function
        Public Shared Function IsCountryExistByCountryNameForBulkUpload() As DataTable
            Dim qryObjICCountry As New ICCountryQuery("qryObjICCountry")

            qryObjICCountry.Select(qryObjICCountry.CountryName.ToLower, qryObjICCountry.CountryCode)
            qryObjICCountry.Where(qryObjICCountry.IsActive = True And qryObjICCountry.IsApproved = True)
            qryObjICCountry.OrderBy(qryObjICCountry.CountryCode.Ascending)
            Return qryObjICCountry.LoadDataTable
        End Function
    End Class
End Namespace
