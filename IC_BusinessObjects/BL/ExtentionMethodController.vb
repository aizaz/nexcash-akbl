﻿
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports System.Web

Public Module ExtentionMethodController

    Dim EncryptionKey As String

    <System.Runtime.CompilerServices.Extension>
    Public Function EncryptString(ByVal PlainText As String, Optional isQueryString As Boolean = True) As String

        Try
            EncryptionKey = ICUtilities.GetSettingValue("EncryptionKey")
            If Not String.IsNullOrEmpty(PlainText) Then
                'PlainText = HttpUtility.HtmlEncode(PlainText)
                Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(PlainText)
                Using encryptor As Aes = Aes.Create()
                    Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = pdb.GetBytes(32)
                    encryptor.IV = pdb.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                            cs.Write(clearBytes, 0, clearBytes.Length)
                            cs.Close()
                        End Using
                        PlainText = Convert.ToBase64String(ms.ToArray())
                        If isQueryString Then
                            PlainText = HttpUtility.UrlEncode(PlainText)
                        End If
                    End Using
                End Using

            End If

            Return PlainText

        Catch ex As Exception
            Return PlainText
        End Try

    End Function


    <System.Runtime.CompilerServices.Extension>
    Public Function EncryptString2(ByVal PlainText As String, Optional isQueryString As Boolean = True) As String

        Try
            EncryptionKey = ""
            If Not String.IsNullOrEmpty(PlainText) Then
                'PlainText = HttpUtility.HtmlEncode(PlainText)
                Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(PlainText)
                Using encryptor As Aes = Aes.Create()
                    Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = pdb.GetBytes(32)
                    encryptor.IV = pdb.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                            cs.Write(clearBytes, 0, clearBytes.Length)
                            cs.Close()
                        End Using
                        PlainText = Convert.ToBase64String(ms.ToArray())
                        If isQueryString Then
                            PlainText = HttpUtility.UrlEncode(PlainText)
                        End If
                    End Using
                End Using

            End If

            Return PlainText

        Catch ex As Exception
            Return PlainText
        End Try

    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function DecryptString(ByVal EncryptText As String) As String
        Try
            'EncryptText = HttpUtility.UrlDecode(EncryptText)
            EncryptionKey = ICUtilities.GetSettingValue("EncryptionKey")
            If Not String.IsNullOrEmpty(EncryptText) Then
                'EncryptText = HttpUtility.HtmlDecode(EncryptText)
                Dim cipherBytes As Byte() = Convert.FromBase64String(EncryptText)
                Using encryptor As Aes = Aes.Create()
                    Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = pdb.GetBytes(32)
                    encryptor.IV = pdb.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                            cs.Write(cipherBytes, 0, cipherBytes.Length)
                            cs.Close()
                        End Using
                        EncryptText = Encoding.Unicode.GetString(ms.ToArray())
                    End Using
                End Using

            End If

            Return EncryptText

        Catch ex As Exception
            Return EncryptText
        End Try
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function DecryptString2(ByVal EncryptText As String) As String
        Try
            'EncryptText = HttpUtility.UrlDecode(EncryptText)
            EncryptionKey = ""
            If Not String.IsNullOrEmpty(EncryptText) Then
                'EncryptText = HttpUtility.HtmlDecode(EncryptText)
                Dim cipherBytes As Byte() = Convert.FromBase64String(EncryptText)
                Using encryptor As Aes = Aes.Create()
                    Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                    encryptor.Key = pdb.GetBytes(32)
                    encryptor.IV = pdb.GetBytes(16)
                    Using ms As New MemoryStream()
                        Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                            cs.Write(cipherBytes, 0, cipherBytes.Length)
                            cs.Close()
                        End Using
                        EncryptText = Encoding.Unicode.GetString(ms.ToArray())
                    End Using
                End Using

            End If

            Return EncryptText

        Catch ex As Exception
            Return EncryptText
        End Try
    End Function

    <System.Runtime.CompilerServices.Extension>
    Public Function IsValidBase64String(ByVal EncryptText As String) As Boolean
        Try
            EncryptionKey = ICUtilities.GetSettingValue("EncryptionKey")

            If Not String.IsNullOrEmpty(EncryptText) Then
                Dim cipherBytes As Byte() = Convert.FromBase64String(EncryptText)
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function


End Module

Public Class ExtentionLayer

    Public Function DecryptString(ByVal EncryptedText As String) As String
        Return EncryptedText.DecryptString()
    End Function

    Public Function EncryptString(ByVal PlainText As String) As String
        Return PlainText.EncryptString()
    End Function

End Class
