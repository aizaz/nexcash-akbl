﻿Namespace IC
    Public Class ICUserRolesAccountPaymentNatureAndLocationsController
        Public Shared Sub DeleteUserRolesAPNaturesLocations(ByVal objICUserRolesAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
            Dim objICUserRolesaPNatureLocationForDelete As New ICUserRolesAccountPaymentNatureAndLocations
            objICUserRolesaPNatureLocationForDelete.es.Connection.CommandTimeout = 3600
            If objICUserRolesaPNatureLocationForDelete.LoadByPrimaryKey(objICUserRolesAPNatureLocation.UserID, objICUserRolesAPNatureLocation.AccountNumber, objICUserRolesAPNatureLocation.BranchCode, objICUserRolesAPNatureLocation.Currency, objICUserRolesAPNatureLocation.PaymentNatureCode, objICUserRolesAPNatureLocation.OfficeID, objICUserRolesAPNatureLocation.RoleID) Then
                objICUserRolesaPNatureLocationForDelete.MarkAsDeleted()
                objICUserRolesaPNatureLocationForDelete.Save()
                'If TaggingType = "Client" Then
                '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "DELETE")
                'Else
                '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "DELETE")
                'End If
            End If
        End Sub
        Public Shared Sub ApproveUserRolesAPNaturesLocations(ByVal objICUserRolesAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
            Dim objICUserRolesaPNatureLocationForApprove As New ICUserRolesAccountPaymentNatureAndLocations
            objICUserRolesaPNatureLocationForApprove.es.Connection.CommandTimeout = 3600
            If objICUserRolesaPNatureLocationForApprove.LoadByPrimaryKey(objICUserRolesAPNatureLocation.UserID, objICUserRolesAPNatureLocation.AccountNumber, objICUserRolesAPNatureLocation.BranchCode, objICUserRolesAPNatureLocation.Currency, objICUserRolesAPNatureLocation.PaymentNatureCode, objICUserRolesAPNatureLocation.OfficeID, objICUserRolesAPNatureLocation.RoleID) Then
                objICUserRolesaPNatureLocationForApprove.IsApprove = IsApproved
                objICUserRolesaPNatureLocationForApprove.ApprovedBy = UsersID.ToString
                objICUserRolesaPNatureLocationForApprove.ApprovedDate = Date.Now
                objICUserRolesaPNatureLocationForApprove.Save()
                'If TaggingType = "Client" Then
                '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "APPROVE")
                'Else
                '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "APPROVE")
                'End If
            End If
        End Sub
        Public Shared Sub AddUserRolesPaymentNatureAndLocations(ByVal objICUsrRolesAPNAndLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
            Dim objICURAPNAndLocation As New ICUserRolesAccountPaymentNatureAndLocations
            objICURAPNAndLocation.es.Connection.CommandTimeout = 3600
            objICURAPNAndLocation.AccountNumber = objICUsrRolesAPNAndLocation.AccountNumber
            objICURAPNAndLocation.BranchCode = objICUsrRolesAPNAndLocation.BranchCode
            objICURAPNAndLocation.Currency = objICUsrRolesAPNAndLocation.Currency
            objICURAPNAndLocation.PaymentNatureCode = objICUsrRolesAPNAndLocation.PaymentNatureCode
            objICURAPNAndLocation.OfficeID = objICUsrRolesAPNAndLocation.OfficeID
            objICURAPNAndLocation.UserID = objICUsrRolesAPNAndLocation.UserID
            objICURAPNAndLocation.CreatedBy = objICUsrRolesAPNAndLocation.CreatedBy
            objICURAPNAndLocation.CreatedDate = objICUsrRolesAPNAndLocation.CreatedDate

            objICURAPNAndLocation.Creater = objICUsrRolesAPNAndLocation.Creater
            objICURAPNAndLocation.CreationDate = objICUsrRolesAPNAndLocation.CreationDate

            objICURAPNAndLocation.RoleID = objICUsrRolesAPNAndLocation.RoleID
            objICURAPNAndLocation.IsApprove = False
            objICURAPNAndLocation.Save()
            'If TaggingType = "Client" Then
            '    ICUtilities.AddAuditTrail("Location " & objICURAPNAndLocation.OfficeID & "for  payment nature " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICURAPNAndLocation.AccountNumber.ToString + "" + objICURAPNAndLocation.BranchCode.ToString + "" + objICURAPNAndLocation.Currency.ToString + "" + objICURAPNAndLocation.PaymentNatureCode.ToString + "" + objICURAPNAndLocation.OfficeID.ToString, UsersID, UsersName, "ADD")
            'Else
            '    ICUtilities.AddAuditTrail("Location " & objICURAPNAndLocation.OfficeID & "for  payment nature " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICURAPNAndLocation.AccountNumber.ToString + "" + objICURAPNAndLocation.BranchCode.ToString + "" + objICURAPNAndLocation.Currency.ToString + "" + objICURAPNAndLocation.PaymentNatureCode.ToString + "" + objICURAPNAndLocation.OfficeID.ToString, UsersID, UsersName, "ADD")
            'End If

        End Sub
        'Public Shared Sub AddUserRolesPaymentNatureAndLocations(ByVal objICUsrRolesAPNAndLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
        '    Dim objICURAPNAndLocation As New ICUserRolesAccountPaymentNatureAndLocations
        '    objICURAPNAndLocation.es.Connection.CommandTimeout = 3600
        '    objICURAPNAndLocation.AccountNumber = objICUsrRolesAPNAndLocation.AccountNumber
        '    objICURAPNAndLocation.BranchCode = objICUsrRolesAPNAndLocation.BranchCode
        '    objICURAPNAndLocation.Currency = objICUsrRolesAPNAndLocation.Currency
        '    objICURAPNAndLocation.PaymentNatureCode = objICUsrRolesAPNAndLocation.PaymentNatureCode
        '    objICURAPNAndLocation.OfficeID = objICUsrRolesAPNAndLocation.OfficeID
        '    objICURAPNAndLocation.UserID = objICUsrRolesAPNAndLocation.UserID
        '    objICURAPNAndLocation.CreatedBy = objICUsrRolesAPNAndLocation.CreatedBy
        '    objICURAPNAndLocation.CreatedDate = objICUsrRolesAPNAndLocation.CreatedDate

        '    objICURAPNAndLocation.Creater = objICUsrRolesAPNAndLocation.Creater
        '    objICURAPNAndLocation.CreationDate = objICUsrRolesAPNAndLocation.CreationDate

        '    objICURAPNAndLocation.RoleID = objICUsrRolesAPNAndLocation.RoleID
        '    objICURAPNAndLocation.IsApprove = False
        '    objICURAPNAndLocation.Save()
        '    'If TaggingType = "Client" Then
        '    '    ICUtilities.AddAuditTrail("Location " & objICURAPNAndLocation.OfficeID & "for  payment nature " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICURAPNAndLocation.AccountNumber.ToString + "" + objICURAPNAndLocation.BranchCode.ToString + "" + objICURAPNAndLocation.Currency.ToString + "" + objICURAPNAndLocation.PaymentNatureCode.ToString + "" + objICURAPNAndLocation.OfficeID.ToString, UsersID, UsersName, "ADD")
        '    'Else
        '    '    ICUtilities.AddAuditTrail("Location " & objICURAPNAndLocation.OfficeID & "for  payment nature " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICURAPNAndLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICURAPNAndLocation.AccountNumber.ToString + "" + objICURAPNAndLocation.BranchCode.ToString + "" + objICURAPNAndLocation.Currency.ToString + "" + objICURAPNAndLocation.PaymentNatureCode.ToString + "" + objICURAPNAndLocation.OfficeID.ToString, UsersID, UsersName, "ADD")
        '    'End If

        'End Sub
        'Public Shared Sub DeleteUserRolesAPNaturesLocations(ByVal objICUserRolesAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
        '    Dim objICUserRolesaPNatureLocationForDelete As New ICUserRolesAccountPaymentNatureAndLocations
        '    objICUserRolesaPNatureLocationForDelete.es.Connection.CommandTimeout = 3600
        '    If objICUserRolesaPNatureLocationForDelete.LoadByPrimaryKey(objICUserRolesAPNatureLocation.UserID, objICUserRolesAPNatureLocation.AccountNumber, objICUserRolesAPNatureLocation.BranchCode, objICUserRolesAPNatureLocation.Currency, objICUserRolesAPNatureLocation.PaymentNatureCode, objICUserRolesAPNatureLocation.OfficeID, objICUserRolesAPNatureLocation.RoleID) Then
        '        objICUserRolesaPNatureLocationForDelete.MarkAsDeleted()
        '        objICUserRolesaPNatureLocationForDelete.Save()
        '        'If TaggingType = "Client" Then
        '        '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "DELETE")
        '        'Else
        '        '    ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "DELETE")
        '        'End If
        '    End If
        'End Sub
        'Public Shared Sub ApproveUserRolesAPNaturesLocations(ByVal objICUserRolesAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
        '    Dim objICUserRolesaPNatureLocationForApprove As New ICUserRolesAccountPaymentNatureAndLocations
        '    objICUserRolesaPNatureLocationForApprove.es.Connection.CommandTimeout = 3600
        '    If objICUserRolesaPNatureLocationForApprove.LoadByPrimaryKey(objICUserRolesAPNatureLocation.UserID, objICUserRolesAPNatureLocation.AccountNumber, objICUserRolesAPNatureLocation.BranchCode, objICUserRolesAPNatureLocation.Currency, objICUserRolesAPNatureLocation.PaymentNatureCode, objICUserRolesAPNatureLocation.OfficeID, objICUserRolesAPNatureLocation.RoleID) Then
        '        objICUserRolesaPNatureLocationForApprove.IsApprove = IsApproved
        '        objICUserRolesaPNatureLocationForApprove.ApprovedBy = UsersID.ToString
        '        objICUserRolesaPNatureLocationForApprove.ApprovedDate = Date.Now
        '        objICUserRolesaPNatureLocationForApprove.Save()
        '        If TaggingType = "Client" Then
        '            ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "APPROVE")
        '        Else
        '            ICUtilities.AddAuditTrail("Location " & objICUserRolesAPNatureLocation.OfficeID & "for  payment nature " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " for role " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureLocation.UpToICUserRolesAccountAndPaymentNatureByRoleID.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureLocation.AccountNumber.ToString + "" + objICUserRolesAPNatureLocation.BranchCode.ToString + "" + objICUserRolesAPNatureLocation.Currency.ToString + "" + objICUserRolesAPNatureLocation.PaymentNatureCode.ToString + "" + objICUserRolesAPNatureLocation.OfficeID.ToString, UsersID, UsersName, "APPROVE")
        '        End If
        '    End If
        'End Sub
        Public Shared Function GetAllUserRolesAccountPaymentNatureLocations(ByVal PaymentNatureCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.PaymentNatureCode = PaymentNatureCode.ToString And objICUserRolesAPNatureLocationsColl.Query.AccountNumber = AccountNumber.ToString)
            objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.BranchCode = BranchCode.ToString And objICUserRolesAPNatureLocationsColl.Query.Currency = Currency.ToString)
            If RoleID <> "0" Then
                objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.RoleID = RoleID.ToString)
            End If
            If UsersId <> "0" Then
                objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.UserID = UsersId.ToString)
            End If
            objICUserRolesAPNatureLocationsColl.Query.Load()
            Return objICUserRolesAPNatureLocationsColl

        End Function
        Public Shared Function GetAllUserRolesAccountPaymentNatureLocationsByRoleIDAndUserID(ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureLocationsColl.LoadAll()
            objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.RoleID = RoleID.ToString And objICUserRolesAPNatureLocationsColl.Query.UserID = UsersId.ToString)
            objICUserRolesAPNatureLocationsColl.Query.Load()
            Return objICUserRolesAPNatureLocationsColl
        End Function
        Public Shared Function GetAllUserRolesAPNAndPLocationsByAPNUserIDAndRoleID(ByVal RoleID As String, ByVal UsersID As String, ByVal PaymentNatureCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
            If UsersID <> "0" Then
                objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.UserID = UsersID.ToString)
            End If
            If RoleID <> "0" Then
                objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.RoleID = RoleID.ToString)
            End If
            objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.PaymentNatureCode = PaymentNatureCode And objICUserRolesAPNatureLocationsColl.Query.AccountNumber = AccountNumber)
            objICUserRolesAPNatureLocationsColl.Query.Where(objICUserRolesAPNatureLocationsColl.Query.BranchCode = BranchCode And objICUserRolesAPNatureLocationsColl.Query.Currency = Currency)
            objICUserRolesAPNatureLocationsColl.Query.Load()
            Return objICUserRolesAPNatureLocationsColl
        End Function
        ' Aizaz Work [Dated: 01-Nov-2013]
        Public Shared Function GetAllUserRolesAPNLocationsWithAccPayNatByAPNUserIDAndRoleID(ByVal RoleID As String, ByVal UsersID As String) As DataTable
            Dim qryICUserRolesAPNatureLocations As New ICUserRolesAccountPaymentNatureAndLocationsQuery
            qryICUserRolesAPNatureLocations.es2.Connection.CommandTimeout = 3600

            qryICUserRolesAPNatureLocations.Select((qryICUserRolesAPNatureLocations.AccountNumber + "-" + qryICUserRolesAPNatureLocations.BranchCode + "-" + qryICUserRolesAPNatureLocations.Currency + "-" + qryICUserRolesAPNatureLocations.PaymentNatureCode).As("AccountPaymentNature"), qryICUserRolesAPNatureLocations.OfficeID)

            If UsersID <> "0" Then
                qryICUserRolesAPNatureLocations.Where(qryICUserRolesAPNatureLocations.UserID = UsersID.ToString)
            End If
            If RoleID <> "0" Then
                qryICUserRolesAPNatureLocations.Where(qryICUserRolesAPNatureLocations.RoleID = RoleID.ToString)
            End If
            Return qryICUserRolesAPNatureLocations.LoadDataTable()
        End Function
        ' Aizaz Work [Dated: 26-Oct-2014]
        Public Shared Function GetAllDistinctAccountPaymentNatureLocationsByRoleID(ByVal RoleID As String) As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim collICUserRolesAPNatureLocations As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            collICUserRolesAPNatureLocations.Query.es.Distinct = True
            collICUserRolesAPNatureLocations.Query.Select(collICUserRolesAPNatureLocations.Query.RoleID, collICUserRolesAPNatureLocations.Query.AccountNumber, collICUserRolesAPNatureLocations.Query.BranchCode, collICUserRolesAPNatureLocations.Query.Currency, collICUserRolesAPNatureLocations.Query.PaymentNatureCode, collICUserRolesAPNatureLocations.Query.OfficeID)
            collICUserRolesAPNatureLocations.Query.Where(collICUserRolesAPNatureLocations.Query.RoleID = RoleID)
            collICUserRolesAPNatureLocations.Query.OrderBy(collICUserRolesAPNatureLocations.Query.AccountNumber.Ascending, collICUserRolesAPNatureLocations.Query.BranchCode.Ascending, collICUserRolesAPNatureLocations.Query.Currency.Ascending, collICUserRolesAPNatureLocations.Query.PaymentNatureCode.Ascending, collICUserRolesAPNatureLocations.Query.OfficeID.Ascending)
            collICUserRolesAPNatureLocations.Query.Load()
            Return collICUserRolesAPNatureLocations
        End Function
    End Class

End Namespace


