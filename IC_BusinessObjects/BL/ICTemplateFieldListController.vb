﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICTemplateFieldListController

        Public Shared Sub AddTemplateField(ByVal objICTemplateFieldsList As ICTemplateFields, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTemplateFieldListFoSave As New IC.ICTemplateFields
            objICTemplateFieldListFoSave.es.Connection.CommandTimeout = 3600
            If (IsUpdate = True) Then
                objICTemplateFieldListFoSave.LoadByPrimaryKey(objICTemplateFieldsList.FieldID, objICTemplateFieldsList.TemplateID)
            End If
            objICTemplateFieldListFoSave.FieldName = objICTemplateFieldsList.FieldName
            objICTemplateFieldListFoSave.FieldType = objICTemplateFieldsList.FieldType
            objICTemplateFieldListFoSave.FieldOrder = objICTemplateFieldsList.FieldOrder
            objICTemplateFieldListFoSave.FixLength = objICTemplateFieldsList.FixLength
            objICTemplateFieldListFoSave.TemplateID = objICTemplateFieldsList.TemplateID
            objICTemplateFieldListFoSave.FieldID = objICTemplateFieldsList.FieldID
            objICTemplateFieldListFoSave.IsRequired = objICTemplateFieldsList.IsRequired
            objICTemplateFieldListFoSave.IsMustRequired = objICTemplateFieldsList.IsMustRequired

            objICTemplateFieldListFoSave.IsRequiredCheque = objICTemplateFieldsList.IsRequiredCheque
            objICTemplateFieldListFoSave.IsMustRequiredCheque = objICTemplateFieldsList.IsMustRequiredCheque

            objICTemplateFieldListFoSave.IsRequiredDD = objICTemplateFieldsList.IsRequiredDD
            objICTemplateFieldListFoSave.IsMustRequiredDD = objICTemplateFieldsList.IsMustRequiredDD

            objICTemplateFieldListFoSave.IsRequiredPO = objICTemplateFieldsList.IsRequiredPO
            objICTemplateFieldListFoSave.IsMustRequiredPO = objICTemplateFieldsList.IsMustRequiredPO


            objICTemplateFieldListFoSave.IsRequiredFT = objICTemplateFieldsList.IsRequiredFT
            objICTemplateFieldListFoSave.IsMustRequiredFT = objICTemplateFieldsList.IsMustRequiredFT

            objICTemplateFieldListFoSave.IsRequiredIBFT = objICTemplateFieldsList.IsRequiredIBFT
            objICTemplateFieldListFoSave.IsMustRequiredIBFT = objICTemplateFieldsList.IsMustRequiredIBFT

            objICTemplateFieldListFoSave.IsRequiredCOTC = objICTemplateFieldsList.IsRequiredCOTC
            objICTemplateFieldListFoSave.IsMustRequiredCOTC = objICTemplateFieldsList.IsMustRequiredCOTC
            objICTemplateFieldListFoSave.IsRequiredBillPayment = objICTemplateFieldsList.IsRequiredBillPayment
            objICTemplateFieldListFoSave.IsMustRequiredBillPayment = objICTemplateFieldsList.IsMustRequiredBillPayment
            objICTemplateFieldListFoSave.CreateBy = objICTemplateFieldsList.CreateBy
            objICTemplateFieldListFoSave.CreateDate = objICTemplateFieldsList.CreateDate


            objICTemplateFieldListFoSave.Save()


            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Template field :" & objICTemplateFieldListFoSave.FieldName & " type " & objICTemplateFieldListFoSave.FieldType & " assigned to template : " & objICTemplateFieldListFoSave.UpToICTemplateByTemplateID.TemplateName & " .", "File Upload Template Field", objICTemplateFieldListFoSave.TemplateID.ToString + "" + objICTemplateFieldListFoSave.FieldID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")
            ElseIf IsUpdate = True Then
                ICUtilities.AddAuditTrail("Template field :" & objICTemplateFieldListFoSave.FieldName & " type " & objICTemplateFieldListFoSave.FieldType & " assigned to template : " & objICTemplateFieldListFoSave.UpToICTemplateByTemplateID.TemplateName & " after update .", "File Upload Template Field", objICTemplateFieldListFoSave.TemplateID.ToString + "" + objICTemplateFieldListFoSave.FieldID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")
            End If
        End Sub
        Public Shared Sub DeleteTemplateFieldForUpdate(ByVal TemplateFieldCode As String, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICTemplateFieldListColl As New ICTemplateFieldsCollection
            Dim objICTemplateFieldList As ICTemplateFields
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = TemplateFieldCode.ToString)
            objICTemplateFieldListColl.Query.Load()
            If objICTemplateFieldListColl.Query.Load Then
                For Each objICTempFieldForDelete As ICTemplateFields In objICTemplateFieldListColl
                    objICTemplateFieldList = New ICTemplateFields
                    objICTemplateFieldList.es.Connection.CommandTimeout = 3600
                    objICTemplateFieldList.LoadByPrimaryKey(objICTempFieldForDelete.FieldID, objICTempFieldForDelete.TemplateID)
                    objICTemplateFieldList.MarkAsDeleted()
                    objICTemplateFieldList.Save()
                    ICUtilities.AddAuditTrail("File upload template field : " & objICTempFieldForDelete.FieldName & " for template: " & objICTempFieldForDelete.UpToICTemplateByTemplateID.TemplateName & " deleted.", "File Upload Template Field", objICTempFieldForDelete.FieldID.ToString + "" + objICTempFieldForDelete.TemplateID, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub
        'Public Shared Sub ApproveTemplateField(ByVal TemplateFieldCode As String, ByVal TemplateID As String, ByVal isapproved As Boolean, ByVal ApprovedBy As String)

        '    Dim FRCTemplateFieldsList As New FRC.FRCTemplateFieldsList
        '    FRCTemplateFieldsList.es.Connection.CommandTimeout = 3600
        '    FRCTemplateFieldsList.LoadByPrimaryKey(TemplateFieldCode, TemplateID)
        '    '   FRCTemplateFieldsList.IsApproved = isapproved

        '    FRCTemplateFieldsList.Save()

        'End Sub
        Public Shared Sub GetFieldListForRadGridByFieldType(ByVal rg As RadGrid, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal FieldType As String, ByVal FileUpload As String)
            Dim objICFieldsListColl As New ICFieldsListCollection
            Dim dt As New DataTable
            objICFieldsListColl.es.Connection.CommandTimeout = 3600



            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.FieldName, objICFieldsListColl.Query.FieldDBLength, objICFieldsListColl.Query.IsRequiredCheque)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD, objICFieldsListColl.Query.IsRequiredFT, objICFieldsListColl.Query.IsRequiredFT)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT, objICFieldsListColl.Query.IsRequiredPO, objICFieldsListColl.Query.FieldID, objICFieldsListColl.Query.IsRequiredCOTC)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired, objICFieldsListColl.Query.IsRequiredCheque, objICFieldsListColl.Query.IsRequiredDD)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT, objICFieldsListColl.Query.IsRequiredIBFT, objICFieldsListColl.Query.IsRequiredPO, objICFieldsListColl.Query.IsRequiredBillPayment)
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired.Case.When(objICFieldsListColl.Query.MustRequired = True).Then("False").Else("True").End().As("IsEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("False").Else("True").End().As("IBFTEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("False").Else("True").End().As("ChequeEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("False").Else("True").End().As("DDEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("False").Else("True").End().As("FTEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("False").Else("True").End().As("POEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("False").Else("True").End().As("COTCEnabled"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredBillPayment.Case.When(objICFieldsListColl.Query.IsRequiredBillPayment = True).Then("False").Else("True").End().As("BillPaymentEnabled"))
            If FieldType = "NonFlexi" Then

                objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldType = "NonFlexi" Or objICFieldsListColl.Query.FieldType = "DetailField")
            Else
                objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldType = FieldType)

            End If

            If FileUpload = "FileUpload" Then
                objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredFileUploadTemplate = True)
            ElseIf FileUpload = "OnLineForm" Then
                objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredOnlineForm = True)
            End If
            objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldType.Descending, objICFieldsListColl.Query.FieldName.Ascending, objICFieldsListColl.Query.FieldOrder.Ascending)
            dt = objICFieldsListColl.Query.LoadDataTable

            If Not PageNumber = 0 Then
                objICFieldsListColl.Query.es.PageNumber = PageNumber
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICFieldsListColl.Query.es.PageNumber = 1
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If




        End Sub

        ''06-04-2015
        'Public Shared Sub GetFieldListForRadGridByFieldType(ByVal rg As RadGrid, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal FieldType As String, ByVal FileUpload As String)
        '    Dim objICFieldsListColl As New ICFieldsListCollection
        '    Dim dt As New DataTable
        '    objICFieldsListColl.es.Connection.CommandTimeout = 3600



        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.FieldName, objICFieldsListColl.Query.FieldDBLength, objICFieldsListColl.Query.IsRequiredCheque)
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD, objICFieldsListColl.Query.IsRequiredFT, objICFieldsListColl.Query.IsRequiredFT)
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT, objICFieldsListColl.Query.IsRequiredPO, objICFieldsListColl.Query.FieldID, objICFieldsListColl.Query.IsRequiredCOTC)
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired, objICFieldsListColl.Query.IsRequiredCheque, objICFieldsListColl.Query.IsRequiredDD)
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT, objICFieldsListColl.Query.IsRequiredIBFT, objICFieldsListColl.Query.IsRequiredPO)
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired.Case.When(objICFieldsListColl.Query.MustRequired = True).Then("False").Else("True").End().As("IsEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("False").Else("True").End().As("IBFTEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("False").Else("True").End().As("ChequeEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("False").Else("True").End().As("DDEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("False").Else("True").End().As("FTEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("False").Else("True").End().As("POEnabled"))
        '    objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("False").Else("True").End().As("COTCEnabled"))
        '    If FieldType = "NonFlexi" Then

        '        objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldType = "NonFlexi" Or objICFieldsListColl.Query.FieldType = "DetailField")
        '    Else
        '        objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldType = FieldType)

        '    End If

        '    If FileUpload = "FileUpload" Then
        '        objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredFileUploadTemplate = True)
        '    ElseIf FileUpload = "OnLineForm" Then
        '        objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredOnlineForm = True)
        '    End If
        '    objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldType.Descending, objICFieldsListColl.Query.FieldName.Ascending, objICFieldsListColl.Query.FieldOrder.Ascending)
        '    dt = objICFieldsListColl.Query.LoadDataTable

        '    If Not PageNumber = 0 Then
        '        objICFieldsListColl.Query.es.PageNumber = PageNumber
        '        objICFieldsListColl.Query.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        objICFieldsListColl.Query.es.PageNumber = 1
        '        objICFieldsListColl.Query.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If




        'End Sub
        'Public Shared Function GetAllTemplateFieldByUserID(ByVal UserID As String) As System.Data.DataTable
        '    Dim dta As New DataTable
        '    Dim objICUser As New ICUser
        '    objICUser.es.Connection.CommandTimeout = 3600
        '    '   Dim FRCAcqAgent As New FRCAcqAgent
        '    If objICUser.LoadByPrimaryKey(UserID) Then
        '        ' If FRCAcqAgent.LoadByPrimaryKey(frcUser.AcqAgentCode) Then
        '        Dim objICTemplateFieldsList As New ICTemplateFieldListCollection
        '        objICTemplateFieldsList.es.Connection.CommandTimeout = 3600
        '        objICTemplateFieldsList.Query.Where(objICTemplateFieldsList.Query.TemplateID.Equal(frcUser.AcqAgentCode))
        '        FRCTemplateFieldsList.Query.OrderBy(FRCTemplateFieldsList.Query.FieldOrder.Ascending)
        '        dta = FRCTemplateFieldsList.Query.LoadDataTable()


        '        'End If

        '    End If
        '    Return dta

        'End Function

        Public Shared Sub GetAllTemplateFieldByTemplateIDForRadGrid(ByVal TemplateID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal FieldType As String)


            Dim qryObjICTemplateFieldList As New ICTemplateFieldsQuery("qryObjICTemplateFieldList")
            Dim qryObjICTemplateFieldListMax As New ICTemplateFieldsQuery("qryObjICTemplateFieldListMax")
            Dim qryObjICTemplateFieldListMin As New ICTemplateFieldsQuery("qryObjICTemplateFieldListMin")
            Dim dt As New DataTable
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldID, qryObjICTemplateFieldList.TemplateID)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldName, qryObjICTemplateFieldList.TemplateFieldsID)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldName, qryObjICTemplateFieldList.TemplateFieldsID)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FixLength, qryObjICTemplateFieldList.IsRequiredDD)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredFT, qryObjICTemplateFieldList.IsRequiredIBFT)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredPO, qryObjICTemplateFieldList.IsRequiredCheque, qryObjICTemplateFieldList.IsRequiredBillPayment)
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequired, qryObjICTemplateFieldList.FieldOrder, qryObjICTemplateFieldList.IsRequiredCOTC)
            qryObjICTemplateFieldList.Select((qryObjICTemplateFieldListMax.[Select](qryObjICTemplateFieldListMax.FieldOrder.Max.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICTemplateFieldListMax.TemplateID = TemplateID)).As("MaxOrder"))
            qryObjICTemplateFieldList.Select((qryObjICTemplateFieldListMin.[Select](qryObjICTemplateFieldListMin.FieldOrder.Min.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICTemplateFieldListMin.TemplateID = TemplateID)).As("MinOrder"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequired.Case.When(qryObjICTemplateFieldList.IsRequired = True).Then("False").Else("True").End.As("IsRequired"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsMustRequired.Case.When(qryObjICTemplateFieldList.IsMustRequired = True).Then("False").Else("True").End.As("IsEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredIBFT.Case.When(qryObjICTemplateFieldList.IsRequiredIBFT = True).Then("False").Else("True").End().As("IBFTEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredCheque.Case.When(qryObjICTemplateFieldList.IsRequiredCheque = True).Then("False").Else("True").End().As("ChequeEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredDD.Case.When(qryObjICTemplateFieldList.IsRequiredDD = True).Then("False").Else("True").End().As("DDEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredFT.Case.When(qryObjICTemplateFieldList.IsRequiredFT = True).Then("False").Else("True").End().As("FTEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredPO.Case.When(qryObjICTemplateFieldList.IsRequiredPO = True).Then("False").Else("True").End().As("POEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredCOTC.Case.When(qryObjICTemplateFieldList.IsRequiredCOTC = True).Then("False").Else("True").End().As("COTCEnabled"))
            qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredBillPayment.Case.When(qryObjICTemplateFieldList.IsRequiredBillPayment = True).Then("False").Else("True").End().As("BillPaymentEnabled"))
            qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.TemplateID = TemplateID)

            If FieldType = "NonFlexi" Then
                qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.FieldType = "NonFlexi" Or qryObjICTemplateFieldList.FieldType = "DetailField")
            Else
                qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.FieldType = FieldType)
            End If

            qryObjICTemplateFieldList.OrderBy(qryObjICTemplateFieldList.FieldOrder.Ascending)
            dt = qryObjICTemplateFieldList.LoadDataTable



            If Not PageNumber = 0 Then
                qryObjICTemplateFieldList.es.PageNumber = PageNumber
                qryObjICTemplateFieldList.es.PageSize = PageSize


                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                qryObjICTemplateFieldList.es.PageNumber = 1
                qryObjICTemplateFieldList.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        ''06-04-2015
        'Public Shared Sub GetAllTemplateFieldByTemplateIDForRadGrid(ByVal TemplateID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal FieldType As String)


        '    Dim qryObjICTemplateFieldList As New ICTemplateFieldsQuery("qryObjICTemplateFieldList")
        '    Dim qryObjICTemplateFieldListMax As New ICTemplateFieldsQuery("qryObjICTemplateFieldListMax")
        '    Dim qryObjICTemplateFieldListMin As New ICTemplateFieldsQuery("qryObjICTemplateFieldListMin")
        '    Dim dt As New DataTable
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldID, qryObjICTemplateFieldList.TemplateID)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldName, qryObjICTemplateFieldList.TemplateFieldsID)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FieldName, qryObjICTemplateFieldList.TemplateFieldsID)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.FixLength, qryObjICTemplateFieldList.IsRequiredDD)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredFT, qryObjICTemplateFieldList.IsRequiredIBFT)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredPO, qryObjICTemplateFieldList.IsRequiredCheque)
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequired, qryObjICTemplateFieldList.FieldOrder, qryObjICTemplateFieldList.IsRequiredCOTC)
        '    qryObjICTemplateFieldList.Select((qryObjICTemplateFieldListMax.[Select](qryObjICTemplateFieldListMax.FieldOrder.Max.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICTemplateFieldListMax.TemplateID = TemplateID)).As("MaxOrder"))
        '    qryObjICTemplateFieldList.Select((qryObjICTemplateFieldListMin.[Select](qryObjICTemplateFieldListMin.FieldOrder.Min.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICTemplateFieldListMin.TemplateID = TemplateID)).As("MinOrder"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequired.Case.When(qryObjICTemplateFieldList.IsRequired = True).Then("False").Else("True").End.As("IsRequired"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsMustRequired.Case.When(qryObjICTemplateFieldList.IsMustRequired = True).Then("False").Else("True").End.As("IsEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredIBFT.Case.When(qryObjICTemplateFieldList.IsRequiredIBFT = True).Then("False").Else("True").End().As("IBFTEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredCheque.Case.When(qryObjICTemplateFieldList.IsRequiredCheque = True).Then("False").Else("True").End().As("ChequeEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredDD.Case.When(qryObjICTemplateFieldList.IsRequiredDD = True).Then("False").Else("True").End().As("DDEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredFT.Case.When(qryObjICTemplateFieldList.IsRequiredFT = True).Then("False").Else("True").End().As("FTEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredPO.Case.When(qryObjICTemplateFieldList.IsRequiredPO = True).Then("False").Else("True").End().As("POEnabled"))
        '    qryObjICTemplateFieldList.Select(qryObjICTemplateFieldList.IsRequiredCOTC.Case.When(qryObjICTemplateFieldList.IsRequiredCOTC = True).Then("False").Else("True").End().As("COTCEnabled"))
        '    qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.TemplateID = TemplateID)

        '    If FieldType = "NonFlexi" Then
        '        qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.FieldType = "NonFlexi" Or qryObjICTemplateFieldList.FieldType = "DetailField")
        '    Else
        '        qryObjICTemplateFieldList.Where(qryObjICTemplateFieldList.FieldType = FieldType)
        '    End If

        '    qryObjICTemplateFieldList.OrderBy(qryObjICTemplateFieldList.FieldOrder.Ascending)
        '    dt = qryObjICTemplateFieldList.LoadDataTable



        '    If Not PageNumber = 0 Then
        '        qryObjICTemplateFieldList.es.PageNumber = PageNumber
        '        qryObjICTemplateFieldList.es.PageSize = PageSize


        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If

        '    Else
        '        qryObjICTemplateFieldList.es.PageNumber = 1
        '        qryObjICTemplateFieldList.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If
        'End Sub
        Public Shared Function GetTemplateFieldByTemplateID(ByVal TemplateID As String) As System.Data.DataTable
            Dim dta As New DataTable

            Dim objICTemplateFieldListColl As New ICTemplateFields
            objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600


            objICTemplateFieldListColl.Query.Select(objICTemplateFieldListColl.Query.FieldName, objICTemplateFieldListColl.Query.FieldOrder.Cast(EntitySpaces.DynamicQuery.esCastType.Int32), objICTemplateFieldListColl.Query.FieldType, objICTemplateFieldListColl.Query.FixLength)
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = TemplateID)
            objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.FieldOrder.Cast(EntitySpaces.DynamicQuery.esCastType.Int32).Ascending)
            dta = objICTemplateFieldListColl.Query.LoadDataTable()



            Return dta

        End Function


        Public Shared Function CheckFieldExist(ByVal FieldName As String) As Boolean
            Dim objICTemplateFieldListColl As New ICTemplateFieldsCollection
            objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.FieldName.ToLower.Trim.ToString() = FieldName.ToLower.Trim.ToString())
            If objICTemplateFieldListColl.Query.Load() Then
                Return False
            Else
                Return True
            End If
        End Function

        'Aizaz Ahmed Work 25Mar2012
        Public Shared Function CheckFieldDBLength(ByVal FieldID As String, ByVal FieldContent As String) As Boolean
            Dim objICTemplateFieldList As New ICFieldsList
            Dim FieldContentLength As Integer = 0
            objICTemplateFieldList.es.Connection.CommandTimeout = 3600
            FieldContentLength = FieldContent.Length
            If objICTemplateFieldList.LoadByPrimaryKey(FieldID) Then
                If objICTemplateFieldList.FieldName = "AmountPKR" Then
                    Dim Amount As String = ""
                    Amount = FieldContent
                    If Amount.Contains(".") Then
                        Dim str As String()
                        str = Amount.Split(".")
                        Amount = ""
                        Amount = str(0) & str(1)
                    End If
                    If Amount.Length > objICTemplateFieldList.FieldDBLength Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    If FieldContentLength > objICTemplateFieldList.FieldDBLength Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            Else
                Return False
            End If
        End Function

        'Public Shared Function CheckFileStatusAgainstTemplate(ByVal TemplateCode As String) As Boolean
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObICStatus As New ICInstructionStatusQuery("qryObICStatus")
        '    Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")

        '    Dim Result As Boolean = False
        '    Dim dt As New DataTable
        '    qryObjICTemplate.Select(qryObICStatus.StatusName)
        '    qryObjICTemplate.InnerJoin(qryObjICFiles).On(qryObjICTemplate.TemplateID = qryObjICFiles.FileUploadTemplateID)
        '    qryObjICTemplate.InnerJoin(qryObjICInstruction).On(qryObjICFiles.FileID = qryObjICInstruction.FileID)
        '    qryObjICTemplate.InnerJoin(qryObICStatus).On(qryObjICInstruction.Status = qryObICStatus.StatusID)
        '    qryObjICTemplate.Where(qryObjICTemplate.TemplateID = TemplateCode.ToString And qryObICStatus.StatusName = "Pending Read")
        '    qryObICStatus.LoadDataTable()
        '    For Each dr As DataRow In dt.Rows
        '        If dr("StatusName").ToString = "Pending Read" Then
        '            Result = True
        '            Exit For
        '        End If
        '    Next

        '    Return Result
        'End Function
        Public Shared Sub GetAllTemplateFieldsForSingleInstructionFormSettingsRadGrid(ByVal DisbursementMode As String, ByVal IsOnlineForm As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim objICFieldsListColl As New ICFieldsListCollection

            Dim dt As New DataTable
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.FieldID, objICFieldsListColl.Query.FieldName)

            If DisbursementMode = "Direct Credit" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("False").Else("True").End.As("IsMustRequired"))

                'objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired.Case.When(objICFieldsListColl.Query.MustRequired = True).Then("True").Else("False").End.As("ChkRequired"))

            ElseIf DisbursementMode = "PO" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("False").Else("True").End.As("IsMustRequired"))

            ElseIf DisbursementMode = "Cheque" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("False").Else("True").End.As("IsMustRequired"))

            ElseIf DisbursementMode = "DD" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("False").Else("True").End.As("IsMustRequired"))

            ElseIf DisbursementMode = "Other Credit" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("False").Else("True").End.As("IsMustRequired"))

            ElseIf DisbursementMode = "COTC" Then
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("True").Else("False").End.As("IsRequired"))
                objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("False").Else("True").End.As("IsMustRequired"))

            End If


            objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredOnlineForm = True)

            If IsOnlineForm = False Then
                If DisbursementMode <> "Cheque" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldID <> 62)
                End If
            End If

            objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = objICFieldsListColl.Query.LoadDataTable
            If Not PageNumber = 0 Then
                objICFieldsListColl.Query.es.PageNumber = PageNumber
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICFieldsListColl.Query.es.PageNumber = 1
                objICFieldsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If



        End Sub
        ''Before Farhan modified  25052015
        'Public Shared Sub GetAllTemplateFieldsForSingleInstructionFormSettingsRadGrid(ByVal DisbursementMode As String, ByVal IsOnlineForm As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        '    Dim objICFieldsListColl As New ICFieldsListCollection
        '    Dim dt As New DataTable
        '    objICFieldsListColl.es.Connection.CommandTimeout = 3600







        '    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredOnlineForm = True)





        '    If IsOnlineForm = False Then
        '        If DisbursementMode <> "Cheque" Then
        '            objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldID <> 62)
        '        End If
        '    End If

        '    objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    dt = objICFieldsListColl.Query.LoadDataTable
        '    If Not PageNumber = 0 Then
        '        objICFieldsListColl.Query.es.PageNumber = PageNumber
        '        objICFieldsListColl.Query.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        objICFieldsListColl.Query.es.PageNumber = 1
        '        objICFieldsListColl.Query.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If



        'End Sub
        Public Shared Function CheckFileStatusAgainstTemplate(ByVal TemplateCode As String) As Boolean
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")

            Dim Result As Boolean = False
            Dim dt As New DataTable
            qryObjICFiles.Select(qryObjICFiles.FileUploadTemplateID, qryObjICFiles.Status, qryObjICFiles.FileID)
            qryObjICFiles.Where(qryObjICFiles.FileUploadTemplateID = TemplateCode.ToString And qryObjICFiles.Status = "Pending Read")
            qryObjICFiles.OrderBy(qryObjICFiles.FileID.Ascending)
            qryObjICFiles.Load()
            dt = qryObjICFiles.LoadDataTable()
            If dt.Rows.Count > 0 Then
                Result = True

            End If

            Return Result
        End Function
        Public Shared Function GetFieldsListByDisbursementMode(ByVal DisbMode As String) As ICFieldsListCollection
            Dim objICFieldsListColl As New ICFieldsListCollection
            objICFieldsListColl.es.Connection.CommandTimeout = 3600
            If Not DisbMode = "" Then
                If DisbMode = "Cheque" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredCheque = True And objICFieldsListColl.Query.MustRequired = True And objICFieldsListColl.Query.IsRequiredOnlineForm = True)
                ElseIf DisbMode = "DD" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredDD = True And objICFieldsListColl.Query.MustRequired = True And objICFieldsListColl.Query.IsRequiredOnlineForm = True)
                ElseIf DisbMode = "PO" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredPO = True And objICFieldsListColl.Query.MustRequired = True And objICFieldsListColl.Query.IsRequiredOnlineForm = True)
                ElseIf DisbMode = "Other Credit" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredIBFT = True And objICFieldsListColl.Query.MustRequired = True And objICFieldsListColl.Query.IsRequiredOnlineForm = True)
                ElseIf DisbMode = "Direct Credit" Then
                    objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredFT = True And objICFieldsListColl.Query.MustRequired = True And objICFieldsListColl.Query.IsRequiredOnlineForm = True)
                End If
            End If
            objICFieldsListColl.Query.Load()
            Return objICFieldsListColl
        End Function
    End Class
End Namespace
