﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICProductTypeController
        Public Shared Function AddProductType(ByVal ProductType As ICProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
            Dim PrdctType As New IC.ICProductType
            Dim prevPrdctType As New IC.ICProductType
            Dim DDPayableAccountID As New ICDDPayableAccountsCollection
            Dim CurrentAt As String
            Dim PrevAt As String
            Dim IsActiveText As String
            Dim IsApprovedText As String
            Dim IsSundaryAllowText As String

            PrdctType.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then
                PrdctType.CreateBy = ProductType.CreateBy
                PrdctType.CreateDate = ProductType.CreateDate
                PrdctType.Creater = ProductType.Creater
                PrdctType.CreationDate = ProductType.CreationDate
            Else
                PrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
                prevPrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
                If PrdctType.DisbursementMode = "DD" Then
                    If PrdctType.DisbursementMode <> ProductType.DisbursementMode Then
                        ' Delete all banks and accounts on ProductTypeCode
                        ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(PrdctType.ProductTypeCode, ProductType.DisbursementMode.ToString(), "", UserID.ToString(), UserName.ToString())
                    End If
                End If
                PrdctType.CreateBy = ProductType.CreateBy
                PrdctType.CreateDate = ProductType.CreateDate
            End If


            PrdctType.ProductTypeCode = ProductType.ProductTypeCode
            PrdctType.ProductTypeName = ProductType.ProductTypeName
            PrdctType.DisbursementMode = ProductType.DisbursementMode
            PrdctType.InstrumentType = ProductType.InstrumentType
            PrdctType.OriginatingTransactionType = ProductType.OriginatingTransactionType
            PrdctType.RespondingTranType = ProductType.RespondingTranType
            PrdctType.VoucherType = ProductType.VoucherType
            PrdctType.ReversalTransactionType = ProductType.ReversalTransactionType
            PrdctType.IsSundaryAllow = ProductType.IsSundaryAllow
            PrdctType.DebitTranCode = ProductType.DebitTranCode
            PrdctType.DebitReversalTranCode = ProductType.DebitReversalTranCode
            PrdctType.CreditTranCode = ProductType.CreditTranCode
            PrdctType.CreditReversalTranCode = ProductType.CreditReversalTranCode
            PrdctType.IsFunded = ProductType.IsFunded
            PrdctType.ReversalRespondingTranType = ProductType.ReversalRespondingTranType


            PrdctType.DebitGLCode = ProductType.DebitGLCode
            PrdctType.DebitBranchCode = ProductType.DebitBranchCode


            PrdctType.DebitCurrency = ProductType.DebitCurrency
            PrdctType.DebitClientNo = ProductType.DebitClientNo
            PrdctType.DebitSeqNo = ProductType.DebitSeqNo
            PrdctType.DebitProfitCentre = ProductType.DebitProfitCentre

            PrdctType.DebitReversalGLCode = ProductType.DebitReversalGLCode
            PrdctType.DebitReversalBranchCode = ProductType.DebitReversalBranchCode
            PrdctType.DebitReversalCurrency = ProductType.DebitReversalCurrency
            PrdctType.DebitReversalClientNo = ProductType.DebitReversalClientNo
            PrdctType.DebitReversalSeqNo = ProductType.DebitReversalSeqNo
            PrdctType.DebitReversalProfitCentre = ProductType.DebitReversalProfitCentre

            PrdctType.CreditGLCode = ProductType.CreditGLCode
            PrdctType.CreditBranchCode = ProductType.CreditBranchCode
            PrdctType.CreditCurrency = ProductType.CreditCurrency
            PrdctType.CreditClientNo = ProductType.CreditClientNo
            PrdctType.CreditSeqNo = ProductType.CreditSeqNo
            PrdctType.CreditProfitCentre = ProductType.CreditProfitCentre

            PrdctType.CreditReversalGLCode = ProductType.CreditReversalGLCode
            PrdctType.CreditReversalBranchCode = ProductType.CreditReversalBranchCode
            PrdctType.CreditReversalCurrency = ProductType.CreditReversalCurrency
            PrdctType.CreditReversalClientNo = ProductType.CreditReversalClientNo
            PrdctType.CreditReversalSeqNo = ProductType.CreditReversalSeqNo
            PrdctType.CreditReversalProfitCentre = ProductType.CreditReversalProfitCentre

            PrdctType.ClearingFTOrgTranType = ProductType.ClearingFTOrgTranType
            PrdctType.ClearingFTRespTranType = ProductType.ClearingFTRespTranType

            PrdctType.ClearingSameDayOrgTranType = ProductType.ClearingSameDayOrgTranType
            PrdctType.ClearingSameDayResTranType = ProductType.ClearingSameDayResTranType

            PrdctType.ClearingNormalOrgTranType = ProductType.ClearingNormalOrgTranType
            PrdctType.ClearingNormalResTranType = ProductType.ClearingNormalResTranType

            PrdctType.ClearingInterCityOrgTranType = ProductType.ClearingInterCityOrgTranType
            PrdctType.ClearingInterCityResTranType = ProductType.ClearingInterCityResTranType

            PrdctType.POCancellationTranType = ProductType.POCancellationTranType





            PrdctType.IsActive = ProductType.IsActive
            PrdctType.IsApproved = False

            If PrdctType.DisbursementMode = "Cheque" Then
                PrdctType.PayableAccountNumber = Nothing
                PrdctType.PayableAccountBranchCode = Nothing
                PrdctType.PayableAccountCurrency = Nothing

                PrdctType.FundedAccountNumber = Nothing
                PrdctType.FundedAccountBranchCode = Nothing
                PrdctType.FundedAccountCurrency = Nothing

                PrdctType.IsFunded = False
                PrdctType.NonPrincipalBankCode = ProductType.NonPrincipalBankCode

            ElseIf PrdctType.DisbursementMode = "PO" Then
                PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
                PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
                PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
                PrdctType.PayableAccountType = ProductType.PayableAccountType
                PrdctType.PayableAccountClientNo = ProductType.PayableAccountClientNo
                PrdctType.PayableAccountProfitCentre = ProductType.PayableAccountProfitCentre
                PrdctType.PayableAccountSeqNo = ProductType.PayableAccountSeqNo
                PrdctType.IsFunded = True

                PrdctType.FundedAccountNumber = Nothing
                PrdctType.FundedAccountBranchCode = Nothing
                PrdctType.FundedAccountCurrency = Nothing

            ElseIf PrdctType.DisbursementMode = "DD" Then
                PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
                PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
                PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
                PrdctType.PayableAccountType = ProductType.PayableAccountType
                PrdctType.PayableAccountClientNo = ProductType.PayableAccountClientNo
                PrdctType.PayableAccountProfitCentre = ProductType.PayableAccountProfitCentre
                PrdctType.PayableAccountSeqNo = ProductType.PayableAccountSeqNo
                PrdctType.IsFunded = True

                PrdctType.FundedAccountNumber = Nothing
                PrdctType.FundedAccountBranchCode = Nothing
                PrdctType.FundedAccountCurrency = Nothing

            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Or PrdctType.DisbursementMode = "Bill Payment" Then
                If ProductType.IsSundaryAllow = True Then
                    PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
                    PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
                    PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
                    PrdctType.FundedAccountType = ProductType.FundedAccountType
                    PrdctType.FundedAccountSeqNo = ProductType.FundedAccountSeqNo
                    PrdctType.FundedAccountClientNo = ProductType.FundedAccountClientNo
                    PrdctType.FundedAccountProfitCentre = ProductType.FundedAccountProfitCentre
                    PrdctType.PayableAccountNumber = Nothing
                    PrdctType.PayableAccountBranchCode = Nothing
                    PrdctType.PayableAccountCurrency = Nothing
                    PrdctType.IsFunded = ProductType.IsSundaryAllow

                Else
                    PrdctType.FundedAccountNumber = Nothing
                    PrdctType.FundedAccountBranchCode = Nothing
                    PrdctType.FundedAccountCurrency = Nothing
                    PrdctType.IsFunded = ProductType.IsSundaryAllow
                End If
            ElseIf PrdctType.DisbursementMode = "COTC" Then
                If ProductType.IsSundaryAllow = True Then
                    PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
                    PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
                    PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
                    PrdctType.FundedAccountType = ProductType.FundedAccountType
                    PrdctType.FundedAccountSeqNo = ProductType.FundedAccountSeqNo
                    PrdctType.FundedAccountClientNo = ProductType.FundedAccountClientNo
                    PrdctType.FundedAccountProfitCentre = ProductType.FundedAccountProfitCentre
                    PrdctType.PayableAccountNumber = Nothing
                    PrdctType.PayableAccountBranchCode = Nothing
                    PrdctType.PayableAccountCurrency = Nothing
                    PrdctType.IsFunded = ProductType.IsSundaryAllow

                Else
                    PrdctType.FundedAccountNumber = Nothing
                    PrdctType.FundedAccountBranchCode = Nothing
                    PrdctType.FundedAccountCurrency = Nothing
                    PrdctType.IsFunded = ProductType.IsSundaryAllow
                End If
            End If

            PrdctType.Save()

            If PrdctType.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If PrdctType.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            If PrdctType.IsSundaryAllow = True Then
                IsSundaryAllowText = True
            Else
                IsSundaryAllowText = False
            End If

            If (isUpdate = False) Then

                If PrdctType.DisbursementMode = "Cheque" Then
                    CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & ""
                    CurrentAt += " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                    CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                    CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                    CurrentAt += "IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ;"
                    CurrentAt += " IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                ElseIf PrdctType.DisbursementMode = "DD" Then
                    CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                    CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; "
                    CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                    CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                    CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                    CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                ElseIf PrdctType.DisbursementMode = "COTC" Then
                    CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                    CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ;"
                    CurrentAt += "Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                    CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                    CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                    CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                    CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                    CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

                ElseIf PrdctType.DisbursementMode = "PO" Then
                    CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                    CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ;  Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
                    CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
                    CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                    CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                    CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                    CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

                ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
                    CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                    CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                    CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                    CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                    CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                    CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                    CurrentAt += " IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                    CurrentAt += " IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                End If

            Else

                If PrdctType.DisbursementMode = "Cheque" Then

                    If PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Cheque" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "DD" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "



                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ;"
                        PrevAt += "; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "PO" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"




                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
                        PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"



                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & "; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " SundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                    End If

                ElseIf PrdctType.DisbursementMode = "DD" Then

                    If PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "DD" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Cheque" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; "
                        CurrentAt += " Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"

                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "PO" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                    End If
                ElseIf PrdctType.DisbursementMode = "COTC" Then

                    If PrdctType.DisbursementMode = "COTC" And prevPrdctType.DisbursementMode = "COTC" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ;"
                        CurrentAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;"
                        PrevAt += "Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "COTC" And prevPrdctType.DisbursementMode = "Cheque" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; "
                        CurrentAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"

                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "COTC" And prevPrdctType.DisbursementMode = "DD" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & ";"
                        PrevAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
                        PrevAt += " Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "COTC" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
                        CurrentAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                    End If
                ElseIf PrdctType.DisbursementMode = "PO" Then

                    If PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "PO" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
                        CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
                        PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Cheque" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
                        CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "DD" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & ";Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
                        CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"





                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
                        CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
                    End If


                ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then

                    If PrdctType.DisbursementMode = "Direct Credit" And prevPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Other Credit" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"



                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
                        PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Cheque" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ;"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"


                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "DD" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Contra GL Account : " & ProductType.DebitGLCode & ";"
                        CurrentAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ;"
                        CurrentAt += " Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; "
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "PO" Then
                        CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
                        CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; "
                        CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
                        CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
                        CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
                        CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
                        CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                        PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
                        PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
                        PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
                        PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
                        PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
                        PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
                        PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
                        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                    End If
                End If
            End If

            Return PrdctType.ProductTypeCode
        End Function
        'Public Shared Function AddProductType(ByVal ProductType As ICProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
        '    Dim PrdctType As New IC.ICProductType
        '    Dim prevPrdctType As New IC.ICProductType
        '    Dim DDPayableAccountID As New ICDDPayableAccountsCollection
        '    Dim CurrentAt As String
        '    Dim PrevAt As String
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim IsSundaryAllowText As String

        '    PrdctType.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then
        '        PrdctType.CreateBy = ProductType.CreateBy
        '        PrdctType.CreateDate = ProductType.CreateDate
        '        PrdctType.Creater = ProductType.Creater
        '        PrdctType.CreationDate = ProductType.CreationDate
        '    Else
        '        PrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        prevPrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        If PrdctType.DisbursementMode = "DD" Then
        '            If PrdctType.DisbursementMode <> ProductType.DisbursementMode Then
        '                ' Delete all banks and accounts on ProductTypeCode
        '                ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(PrdctType.ProductTypeCode, ProductType.DisbursementMode.ToString(), "", UserID.ToString(), UserName.ToString())
        '            End If
        '        End If
        '        PrdctType.CreateBy = ProductType.CreateBy
        '        PrdctType.CreateDate = ProductType.CreateDate
        '    End If


        '    PrdctType.ProductTypeCode = ProductType.ProductTypeCode
        '    PrdctType.ProductTypeName = ProductType.ProductTypeName
        '    PrdctType.DisbursementMode = ProductType.DisbursementMode
        '    PrdctType.InstrumentType = ProductType.InstrumentType
        '    PrdctType.OriginatingTransactionType = ProductType.OriginatingTransactionType
        '    PrdctType.RespondingTranType = ProductType.RespondingTranType
        '    PrdctType.VoucherType = ProductType.VoucherType
        '    PrdctType.ReversalTransactionType = ProductType.ReversalTransactionType
        '    PrdctType.IsSundaryAllow = ProductType.IsSundaryAllow
        '    PrdctType.DebitTranCode = ProductType.DebitTranCode
        '    PrdctType.DebitReversalTranCode = ProductType.DebitReversalTranCode
        '    PrdctType.CreditTranCode = ProductType.CreditTranCode
        '    PrdctType.CreditReversalTranCode = ProductType.CreditReversalTranCode
        '    PrdctType.IsFunded = ProductType.IsFunded



        '    PrdctType.DebitGLCode = ProductType.DebitGLCode
        '    PrdctType.DebitBranchCode = ProductType.DebitBranchCode


        '    PrdctType.DebitCurrency = ProductType.DebitCurrency
        '    PrdctType.DebitClientNo = ProductType.DebitClientNo
        '    PrdctType.DebitSeqNo = ProductType.DebitSeqNo
        '    PrdctType.DebitProfitCentre = ProductType.DebitProfitCentre

        '    PrdctType.DebitReversalGLCode = ProductType.DebitReversalGLCode
        '    PrdctType.DebitReversalBranchCode = ProductType.DebitReversalBranchCode
        '    PrdctType.DebitReversalCurrency = ProductType.DebitReversalCurrency
        '    PrdctType.DebitReversalClientNo = ProductType.DebitReversalClientNo
        '    PrdctType.DebitReversalSeqNo = ProductType.DebitReversalSeqNo
        '    PrdctType.DebitReversalProfitCentre = ProductType.DebitReversalProfitCentre

        '    PrdctType.CreditGLCode = ProductType.CreditGLCode
        '    PrdctType.CreditBranchCode = ProductType.CreditBranchCode
        '    PrdctType.CreditCurrency = ProductType.CreditCurrency
        '    PrdctType.CreditClientNo = ProductType.CreditClientNo
        '    PrdctType.CreditSeqNo = ProductType.CreditSeqNo
        '    PrdctType.CreditProfitCentre = ProductType.CreditProfitCentre

        '    PrdctType.CreditReversalGLCode = ProductType.CreditReversalGLCode
        '    PrdctType.CreditReversalBranchCode = ProductType.CreditReversalBranchCode
        '    PrdctType.CreditReversalCurrency = ProductType.CreditReversalCurrency
        '    PrdctType.CreditReversalClientNo = ProductType.CreditReversalClientNo
        '    PrdctType.CreditReversalSeqNo = ProductType.CreditReversalSeqNo
        '    PrdctType.CreditReversalProfitCentre = ProductType.CreditReversalProfitCentre



        '    PrdctType.IsActive = ProductType.IsActive
        '    PrdctType.IsApproved = False

        '    If PrdctType.DisbursementMode = "Cheque" Then
        '        PrdctType.PayableAccountNumber = Nothing
        '        PrdctType.PayableAccountBranchCode = Nothing
        '        PrdctType.PayableAccountCurrency = Nothing

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '        PrdctType.IsFunded = False
        '        PrdctType.NonPrincipalBankCode = ProductType.NonPrincipalBankCode

        '    ElseIf PrdctType.DisbursementMode = "PO" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.PayableAccountType = ProductType.PayableAccountType
        '        PrdctType.PayableAccountClientNo = ProductType.PayableAccountClientNo
        '        PrdctType.PayableAccountProfitCentre = ProductType.PayableAccountProfitCentre
        '        PrdctType.PayableAccountSeqNo = ProductType.PayableAccountSeqNo
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "DD" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.PayableAccountType = ProductType.PayableAccountType
        '        PrdctType.PayableAccountClientNo = ProductType.PayableAccountClientNo
        '        PrdctType.PayableAccountProfitCentre = ProductType.PayableAccountProfitCentre
        '        PrdctType.PayableAccountSeqNo = ProductType.PayableAccountSeqNo
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '        If ProductType.IsSundaryAllow = True Then
        '            PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
        '            PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
        '            PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
        '            PrdctType.FundedAccountType = ProductType.FundedAccountType
        '            PrdctType.FundedAccountSeqNo = ProductType.FundedAccountSeqNo
        '            PrdctType.FundedAccountClientNo = ProductType.FundedAccountClientNo
        '            PrdctType.FundedAccountProfitCentre = ProductType.FundedAccountProfitCentre
        '            PrdctType.PayableAccountNumber = Nothing
        '            PrdctType.PayableAccountBranchCode = Nothing
        '            PrdctType.PayableAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow

        '        Else
        '            PrdctType.FundedAccountNumber = Nothing
        '            PrdctType.FundedAccountBranchCode = Nothing
        '            PrdctType.FundedAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow
        '        End If

        '    End If

        '    PrdctType.Save()

        '    If PrdctType.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If PrdctType.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    If PrdctType.IsSundaryAllow = True Then
        '        IsSundaryAllowText = True
        '    Else
        '        IsSundaryAllowText = False
        '    End If

        '    If (isUpdate = False) Then

        '        If PrdctType.DisbursementMode = "Cheque" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & ""
        '            CurrentAt += " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '            CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '            CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '            CurrentAt += "IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ;"
        '            CurrentAt += " IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
        '        ElseIf PrdctType.DisbursementMode = "DD" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '            CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; "
        '            CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '            CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '            CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '            CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

        '        ElseIf PrdctType.DisbursementMode = "PO" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '            CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ;  Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
        '            CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
        '            CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '            CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '            CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '            CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '            CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
        '            CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
        '            CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '            CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '            CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '            CurrentAt += " IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
        '            CurrentAt += " IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
        '        End If

        '    Else

        '        If PrdctType.DisbursementMode = "Cheque" Then

        '            If PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "



        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ;"
        '                PrevAt += "; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"




        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
        '                PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"



        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & "; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
        '                PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " SundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "DD" Then

        '            If PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"


        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; "
        '                CurrentAt += " Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"

        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; "
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; IsFunded:  " & PrdctType.IsFunded.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"


        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
        '                PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; "
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "PO" Then

        '            If PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
        '                CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ;Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
        '                PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & "; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
        '                CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"



        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & ";Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
        '                CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"





        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ;"
        '                CurrentAt += " Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"


        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
        '                PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
        '            End If


        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then

        '            If PrdctType.DisbursementMode = "Direct Credit" And prevPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Other Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
        '                CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"



        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ;"
        '                PrevAt += " Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ;"
        '                CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ;"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"


        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; "
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; "
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Contra GL Account : " & ProductType.DebitGLCode & ";"
        '                CurrentAt += " Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ;"
        '                CurrentAt += " Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; "
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ;"
        '                CurrentAt += " Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; "
        '                CurrentAt += " Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; "
        '                CurrentAt += " ; Originating Transaction Type: " & PrdctType.OriginatingTransactionType & "; "
        '                CurrentAt += " Responding Transaction Type: " & PrdctType.RespondingTranType & "; Instrument Type: " & PrdctType.InstrumentType & ";"
        '                CurrentAt += " Voucher Type: " & PrdctType.VoucherType & "; Reversal Transaction Type: " & PrdctType.ReversalTransactionType & "; "
        '                CurrentAt += " IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ;"
        '                PrevAt += " Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ;"
        '                PrevAt += " Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ;"
        '                PrevAt += " Originating Transaction Type: " & prevPrdctType.OriginatingTransactionType & "; "
        '                PrevAt += " Responding Transaction Type: " & prevPrdctType.RespondingTranType & "; Reversal Transaction Type " & prevPrdctType.ReversalTransactionType & "; "
        '                PrevAt += " Instrument Type: " & prevPrdctType.InstrumentType & "; Voucher Type : " & prevPrdctType.VoucherType & "; "
        '                PrevAt += " IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '            End If
        '        End If
        '    End If

        '    Return PrdctType.ProductTypeCode
        'End Function
        Public Shared Function GetAllProductTypes() As ICProductTypeCollection
            Dim collProductType As New ICProductTypeCollection
            collProductType.es.Connection.CommandTimeout = 3600
            collProductType.Query.OrderBy(collProductType.Query.ProductTypeName.Ascending)
            collProductType.Query.Load()
            Return collProductType
        End Function
        Public Shared Function GetAllProductTypesActiveAndApprove() As ICProductTypeCollection
            Dim collProductType As New ICProductTypeCollection
            collProductType.es.Connection.CommandTimeout = 3600
            collProductType.Query.Where(collProductType.Query.IsActive = True And collProductType.Query.IsApproved = True)
            collProductType.Query.OrderBy(collProductType.Query.ProductTypeName.Ascending)
            collProductType.Query.Load()
            Return collProductType
        End Function

        Public Shared Sub GetProductTypesgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collProductType As New ICProductTypeCollection

            If Not pagenumber = 0 Then
                collProductType.Query.Select(collProductType.Query.ProductTypeCode, collProductType.Query.ProductTypeName, collProductType.Query.IsActive, collProductType.Query.IsApproved)
                collProductType.Query.Select(collProductType.Query.DisbursementMode.Case.When(collProductType.Query.DisbursementMode = "Direct Credit").Then("Fund Transfer").When(collProductType.Query.DisbursementMode = "Other Credit").Then("Inter Bank Fund Transfer").Else(collProductType.Query.DisbursementMode).End().As("DisbursementMode"))
                collProductType.Query.OrderBy(collProductType.Query.ProductTypeCode.Descending)
                collProductType.Query.Load()
                rg.DataSource = collProductType

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collProductType.Query.es.PageNumber = 1
                collProductType.Query.es.PageSize = pagesize
                collProductType.Query.OrderBy(collProductType.Query.ProductTypeCode.Descending)
                collProductType.Query.Load()
                rg.DataSource = collProductType


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub

        Public Shared Function GetAllActiveApprovedBankAccount() As ICBankAccountsCollection

            Dim objICBankAccount As New ICBankAccountsCollection
            Dim qryICProductType As New ICProductTypeQuery("ProdTyp")
            objICBankAccount.es.Connection.CommandTimeout = 3600

            objICBankAccount.Query.Where(objICBankAccount.Query.IsActive = True And objICBankAccount.Query.IsApproved = True)
            objICBankAccount.Query.OrderBy(objICBankAccount.Query.AccountNumber.Ascending)

            objICBankAccount.Query.Load()

            Return objICBankAccount

        End Function
        'Public Shared Function GetAllActiveApprovedBankAccountNew() As ICBankAccountsCollection

        '    Dim objICBankAccount As New ICBankAccountsCollection
        '    objICBankAccount.es.Connection.CommandTimeout = 3600
        '    objICBankAccount.Query.Select((objICBankAccount.Query.CBAccountType.Coalesce("'-'") + "-" + objICBankAccount.Query.AccountNumber.Coalesce("'-'") + "-" + objICBankAccount.Query.BranchCode.Coalesce("'-'") + "-" + objICBankAccount.Query.Currency.Coalesce("'-'") + "-" + objICBankAccount.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + objICBankAccount.Query.ClientNo.Coalesce("'-'") + "-" + objICBankAccount.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
        '    objICBankAccount.Query.Select((objICBankAccount.Query.AccountNumber.Coalesce("'-'") + "-" + objICBankAccount.Query.AccountTitle.Coalesce("'-'")).As("AccountTitle"))
        '    objICBankAccount.Query.Where(objICBankAccount.Query.IsActive = True And objICBankAccount.Query.IsApproved = True)
        '    objICBankAccount.Query.OrderBy(objICBankAccount.Query.AccountNumber.Ascending)

        '    objICBankAccount.Query.Load()

        '    Return objICBankAccount

        'End Function
        Public Shared Function GetAllActiveApprovedBankAccountNew() As ICBankAccountsCollection

            Dim objICBankAccount As New ICBankAccountsCollection
            objICBankAccount.es.Connection.CommandTimeout = 3600
            objICBankAccount.Query.Select((objICBankAccount.Query.CBAccountType.Coalesce("'-'") + "-" + objICBankAccount.Query.AccountNumber.Coalesce("'-'") + "-" + objICBankAccount.Query.BranchCode.Coalesce("'-'") + "-" + objICBankAccount.Query.Currency.Coalesce("'-'") + "-" + objICBankAccount.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + objICBankAccount.Query.ClientNo.Coalesce("'-'") + "-" + objICBankAccount.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            objICBankAccount.Query.Select((objICBankAccount.Query.AccountNumber.Coalesce("'-'") + "-" + objICBankAccount.Query.AccountTitle.Coalesce("'-'")).As("AccountTitle"))
            objICBankAccount.Query.Where(objICBankAccount.Query.IsActive = True And objICBankAccount.Query.IsApproved = True And objICBankAccount.Query.AccountType <> "Contra GL Account")
            objICBankAccount.Query.OrderBy(objICBankAccount.Query.AccountNumber.Ascending)

            objICBankAccount.Query.Load()

            Return objICBankAccount

        End Function

        Public Shared Function GetAllActiveApprovedNonPrincipalDDEnabledBank() As ICBankCollection

            Dim collCBank As New ICBankCollection

            collCBank.es.Connection.CommandTimeout = 3600

            collCBank.Query.Select(collCBank.Query.BankCode, collCBank.Query.BankName)
            collCBank.Query.Where(collCBank.Query.IsActive = True And collCBank.Query.IsApproved = True And collCBank.Query.IsPrincipal = False And collCBank.Query.IsDDEnabled = True)
            collCBank.Query.OrderBy(collCBank.Query.BankName.Ascending)


            collCBank.Query.Load()

            Return collCBank

        End Function
        ' Aizaz Ahmed [Dated: 12-Feb-2013]
        Public Shared Function IsProductTypeExistByProductTypeCode(ByVal ProductTypeCode As String, ByVal TemplateCode As String, ByVal APNFUTFields As ICAccountPaymentNatureFileUploadTemplatefields) As Boolean
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICAPNPTypeTemplates As New ICAccountsPaymentNatureProductTypeTemplatesQuery("qryObjICAPNPTypeTemplates")
            Dim dt As New DataTable
            qryObjICProductType.Select(qryObjICProductType.ProductTypeCode)
            qryObjICProductType.InnerJoin(qryObjICAPNPTypeTemplates).On(qryObjICProductType.ProductTypeCode = qryObjICAPNPTypeTemplates.ProductCode)
            qryObjICProductType.InnerJoin(qryObjICAPNPType).On(qryObjICAPNPTypeTemplates.AccountNumber = qryObjICAPNPType.AccountNumber And qryObjICAPNPTypeTemplates.BranchCode = qryObjICAPNPType.BranchCode And qryObjICAPNPTypeTemplates.Currency = qryObjICAPNPType.Currency And qryObjICAPNPTypeTemplates.PaymentNatureCode = qryObjICAPNPType.PaymentNatureCode And qryObjICAPNPTypeTemplates.ProductCode = qryObjICAPNPType.ProductTypeCode)
            qryObjICProductType.Where(qryObjICProductType.ProductTypeCode = ProductTypeCode.ToString And qryObjICProductType.IsActive = True And qryObjICProductType.IsApproved = True)
            qryObjICProductType.Where(qryObjICAPNPTypeTemplates.AccountNumber = APNFUTFields.AccountNumber And qryObjICAPNPTypeTemplates.BranchCode = APNFUTFields.BranchCode And qryObjICAPNPTypeTemplates.Currency = APNFUTFields.Currency And qryObjICAPNPTypeTemplates.PaymentNatureCode = APNFUTFields.PaymentNatureCode And qryObjICAPNPTypeTemplates.ProductCode = ProductTypeCode.ToString And qryObjICAPNPTypeTemplates.TemplateID = TemplateCode.ToString)
            qryObjICProductType.Where(qryObjICAPNPType.IsApproved = True)
            dt = qryObjICProductType.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        'Public Shared Function AddProductType(ByVal ProductType As ICProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
        '    Dim PrdctType As New IC.ICProductType
        '    Dim prevPrdctType As New IC.ICProductType
        '    Dim DDPayableAccountID As New ICDDPayableAccountsCollection
        '    Dim CurrentAt As String
        '    Dim PrevAt As String
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim IsSundaryAllowText As String

        '    PrdctType.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then
        '        PrdctType.CreateBy = ProductType.CreateBy
        '        PrdctType.CreateDate = ProductType.CreateDate

        '    Else
        '        PrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        prevPrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        If PrdctType.DisbursementMode = "DD" Then
        '            If PrdctType.DisbursementMode <> ProductType.DisbursementMode Then
        '                ' Delete all banks and accounts on ProductTypeCode
        '                ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(PrdctType.ProductTypeCode, ProductType.DisbursementMode.ToString(), "", UserID.ToString(), UserName.ToString())
        '            End If
        '        End If

        '    End If


        '    PrdctType.ProductTypeCode = ProductType.ProductTypeCode
        '    PrdctType.ProductTypeName = ProductType.ProductTypeName
        '    PrdctType.DisbursementMode = ProductType.DisbursementMode
        '    PrdctType.IsSundaryAllow = ProductType.IsSundaryAllow
        '    PrdctType.DebitTranCode = ProductType.DebitTranCode
        '    PrdctType.DebitReversalTranCode = ProductType.DebitReversalTranCode
        '    PrdctType.CreditTranCode = ProductType.CreditTranCode
        '    PrdctType.CreditReversalTranCode = ProductType.CreditReversalTranCode
        '    PrdctType.IsFunded = ProductType.IsFunded


        '    PrdctType.IsActive = ProductType.IsActive
        '    PrdctType.IsApproved = False

        '    If PrdctType.DisbursementMode = "Cheque" Then
        '        PrdctType.PayableAccountNumber = Nothing
        '        PrdctType.PayableAccountBranchCode = Nothing
        '        PrdctType.PayableAccountCurrency = Nothing

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '        PrdctType.IsFunded = False
        '        PrdctType.NonPrincipalBankCode = ProductType.NonPrincipalBankCode

        '    ElseIf PrdctType.DisbursementMode = "PO" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "DD" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '        If ProductType.IsSundaryAllow = True Then
        '            PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
        '            PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
        '            PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
        '            PrdctType.PayableAccountNumber = Nothing
        '            PrdctType.PayableAccountBranchCode = Nothing
        '            PrdctType.PayableAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow

        '        Else
        '            PrdctType.FundedAccountNumber = Nothing
        '            PrdctType.FundedAccountBranchCode = Nothing
        '            PrdctType.FundedAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow
        '        End If

        '    End If

        '    PrdctType.Save()

        '    If PrdctType.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If PrdctType.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    If PrdctType.IsSundaryAllow = True Then
        '        IsSundaryAllowText = True
        '    Else
        '        IsSundaryAllowText = False
        '    End If

        '    If (isUpdate = False) Then

        '        If PrdctType.DisbursementMode = "Cheque" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "DD" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "PO" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '        End If

        '    Else

        '        If PrdctType.DisbursementMode = "Cheque" Then

        '            If PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; SundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "DD" Then

        '            If PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "PO" Then

        '            If PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If


        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then

        '            If PrdctType.DisbursementMode = "Direct Credit" And prevPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Other Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            End If
        '        End If
        '    End If

        '    Return PrdctType.ProductTypeCode
        'End Function


        'Public Shared Function AddProductType(ByVal ProductType As ICProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
        '    Dim PrdctType As New IC.ICProductType
        '    Dim prevPrdctType As New IC.ICProductType
        '    Dim DDPayableAccountID As New ICDDPayableAccountsCollection
        '    Dim CurrentAt As String
        '    Dim PrevAt As String
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim IsSundaryAllowText As String

        '    PrdctType.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then
        '        PrdctType.CreateBy = ProductType.CreateBy
        '        PrdctType.CreateDate = ProductType.CreateDate

        '    Else
        '        PrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        prevPrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        If PrdctType.DisbursementMode = "DD" Then
        '            If PrdctType.DisbursementMode <> ProductType.DisbursementMode Then
        '                ' Delete all banks and accounts on ProductTypeCode
        '                ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(PrdctType.ProductTypeCode, ProductType.DisbursementMode.ToString(), "", UserID.ToString(), UserName.ToString())
        '            End If
        '        End If

        '    End If


        '    PrdctType.ProductTypeCode = ProductType.ProductTypeCode
        '    PrdctType.ProductTypeName = ProductType.ProductTypeName
        '    PrdctType.DisbursementMode = ProductType.DisbursementMode
        '    PrdctType.IsSundaryAllow = ProductType.IsSundaryAllow
        '    PrdctType.DebitTranCode = ProductType.DebitTranCode
        '    PrdctType.DebitReversalTranCode = ProductType.DebitReversalTranCode
        '    PrdctType.CreditTranCode = ProductType.CreditTranCode
        '    PrdctType.CreditReversalTranCode = ProductType.CreditReversalTranCode
        '    PrdctType.IsFunded = ProductType.IsFunded


        '    PrdctType.IsActive = ProductType.IsActive
        '    PrdctType.IsApproved = False

        '    If PrdctType.DisbursementMode = "Cheque" Then
        '        PrdctType.PayableAccountNumber = Nothing
        '        PrdctType.PayableAccountBranchCode = Nothing
        '        PrdctType.PayableAccountCurrency = Nothing

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '        PrdctType.IsFunded = False
        '        PrdctType.NonPrincipalBankCode = ProductType.NonPrincipalBankCode

        '    ElseIf PrdctType.DisbursementMode = "PO" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "DD" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '        If ProductType.IsSundaryAllow = True Then
        '            PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
        '            PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
        '            PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
        '            PrdctType.PayableAccountNumber = Nothing
        '            PrdctType.PayableAccountBranchCode = Nothing
        '            PrdctType.PayableAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow

        '        Else
        '            PrdctType.FundedAccountNumber = Nothing
        '            PrdctType.FundedAccountBranchCode = Nothing
        '            PrdctType.FundedAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow
        '        End If

        '    End If

        '    PrdctType.Save()

        '    If PrdctType.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If PrdctType.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    If PrdctType.IsSundaryAllow = True Then
        '        IsSundaryAllowText = True
        '    Else
        '        IsSundaryAllowText = False
        '    End If

        '    If (isUpdate = False) Then

        '        If PrdctType.DisbursementMode = "Cheque" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "DD" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "PO" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '        End If

        '    Else

        '        If PrdctType.DisbursementMode = "Cheque" Then

        '            If PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; SundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "DD" Then

        '            If PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "PO" Then

        '            If PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If


        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then

        '            If PrdctType.DisbursementMode = "Direct Credit" And prevPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Other Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            End If
        '        End If
        '    End If

        '    Return PrdctType.ProductTypeCode
        'End Function

        'Public Shared Function AddProductType(ByVal ProductType As ICProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
        '    Dim PrdctType As New IC.ICProductType
        '    Dim prevPrdctType As New IC.ICProductType
        '    Dim DDPayableAccountID As New ICDDPayableAccountsCollection
        '    Dim CurrentAt As String
        '    Dim PrevAt As String
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim IsSundaryAllowText As String

        '    PrdctType.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then
        '        PrdctType.CreateBy = ProductType.CreateBy
        '        PrdctType.CreateDate = ProductType.CreateDate

        '    Else
        '        PrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        prevPrdctType.LoadByPrimaryKey(ProductType.ProductTypeCode)
        '        If PrdctType.DisbursementMode = "DD" Then
        '            If PrdctType.DisbursementMode <> ProductType.DisbursementMode Then
        '                ' Delete all banks and accounts on ProductTypeCode
        '                ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(PrdctType.ProductTypeCode, ProductType.DisbursementMode.ToString(), "", UserID.ToString(), UserName.ToString())
        '            End If
        '        End If

        '    End If


        '    PrdctType.ProductTypeCode = ProductType.ProductTypeCode
        '    PrdctType.ProductTypeName = ProductType.ProductTypeName
        '    PrdctType.DisbursementMode = ProductType.DisbursementMode
        '    PrdctType.IsSundaryAllow = ProductType.IsSundaryAllow
        '    PrdctType.DebitTranCode = ProductType.DebitTranCode
        '    PrdctType.DebitReversalTranCode = ProductType.DebitReversalTranCode
        '    PrdctType.CreditTranCode = ProductType.CreditTranCode
        '    PrdctType.CreditReversalTranCode = ProductType.CreditReversalTranCode
        '    PrdctType.IsFunded = ProductType.IsFunded


        '    PrdctType.IsActive = ProductType.IsActive
        '    PrdctType.IsApproved = False

        '    If PrdctType.DisbursementMode = "Cheque" Then
        '        PrdctType.PayableAccountNumber = Nothing
        '        PrdctType.PayableAccountBranchCode = Nothing
        '        PrdctType.PayableAccountCurrency = Nothing

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '        PrdctType.IsFunded = False
        '        PrdctType.NonPrincipalBankCode = ProductType.NonPrincipalBankCode

        '    ElseIf PrdctType.DisbursementMode = "PO" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "DD" Then
        '        PrdctType.PayableAccountNumber = ProductType.PayableAccountNumber
        '        PrdctType.PayableAccountBranchCode = ProductType.PayableAccountBranchCode
        '        PrdctType.PayableAccountCurrency = ProductType.PayableAccountCurrency
        '        PrdctType.IsFunded = True

        '        PrdctType.FundedAccountNumber = Nothing
        '        PrdctType.FundedAccountBranchCode = Nothing
        '        PrdctType.FundedAccountCurrency = Nothing

        '    ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '        If ProductType.IsSundaryAllow = True Then
        '            PrdctType.FundedAccountNumber = ProductType.FundedAccountNumber
        '            PrdctType.FundedAccountBranchCode = ProductType.FundedAccountBranchCode
        '            PrdctType.FundedAccountCurrency = ProductType.FundedAccountCurrency
        '            PrdctType.PayableAccountNumber = Nothing
        '            PrdctType.PayableAccountBranchCode = Nothing
        '            PrdctType.PayableAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow

        '        Else
        '            PrdctType.FundedAccountNumber = Nothing
        '            PrdctType.FundedAccountBranchCode = Nothing
        '            PrdctType.FundedAccountCurrency = Nothing
        '            PrdctType.IsFunded = ProductType.IsSundaryAllow
        '        End If

        '    End If

        '    PrdctType.Save()

        '    If PrdctType.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If PrdctType.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    If PrdctType.IsSundaryAllow = True Then
        '        IsSundaryAllowText = True
        '    Else
        '        IsSundaryAllowText = False
        '    End If

        '    If (isUpdate = False) Then

        '        If PrdctType.DisbursementMode = "Cheque" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "DD" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "PO" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
        '            CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '        End If

        '    Else

        '        If PrdctType.DisbursementMode = "Cheque" Then

        '            If PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Cheque" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; SundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "DD" Then

        '            If PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "DD" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If

        '        ElseIf PrdctType.DisbursementMode = "PO" Then

        '            If PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "PO" And prevPrdctType.DisbursementMode = "Other Credit" Or prevPrdctType.DisbursementMode = "Direct Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())
        '            End If


        '        ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then

        '            If PrdctType.DisbursementMode = "Direct Credit" And prevPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Other Credit" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & prevPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & prevPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & prevPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & prevPrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "Cheque" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "DD" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br />Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Bank Code: " & prevPrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" And prevPrdctType.DisbursementMode = "PO" Then
        '                CurrentAt = "Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & IsSundaryAllowText.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '                PrevAt = "<br/>Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & prevPrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & prevPrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & prevPrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & prevPrdctType.IsFunded.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "]"
        '                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString())

        '            End If
        '        End If
        '    End If

        '    Return PrdctType.ProductTypeCode
        'End Function


        Public Shared Sub ApproveProductType(ByVal ProductTypeCode As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim PrdctType As New ICProductType
            Dim CurrentAt As String

            PrdctType.es.Connection.CommandTimeout = 3600
            PrdctType.LoadByPrimaryKey(ProductTypeCode)
            PrdctType.IsApproved = IsApproved
            PrdctType.ApprovedBy = UserID
            PrdctType.ApprovedOn = Date.Now
            PrdctType.Save()

            If PrdctType.DisbursementMode = "Cheque" Then
                CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

            ElseIf PrdctType.DisbursementMode = "DD" Then
                CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Bank Code: " & PrdctType.NonPrincipalBankCode.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

            ElseIf PrdctType.DisbursementMode = "PO" Then
                CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Payable Account Number: " & PrdctType.PayableAccountNumber.ToString() & " ; Payable Account Branch Code: " & PrdctType.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & PrdctType.PayableAccountCurrency.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

            ElseIf PrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
                CurrentAt = "Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Disbursement Mode: " & PrdctType.DisbursementMode.ToString() & " ; Funded Account Number: " & PrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & PrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & PrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & PrdctType.IsSundaryAllow.ToString() & " ; IsFunded:  " & PrdctType.IsFunded.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")
            End If

        End Sub





        Public Shared Sub DeleteProductType(ByVal ProductTypeCode As String, ByVal UserID As String, ByVal UserName As String)

            Dim PrdctType As New ICProductType
            Dim DelPrdctType As New ICProductType
            Dim ProdctTypeName As String = ""
            Dim CurrentAt As String

            PrdctType.es.Connection.CommandTimeout = 3600

            PrdctType.LoadByPrimaryKey(ProductTypeCode)
            DelPrdctType.LoadByPrimaryKey(ProductTypeCode)
            ProdctTypeName = PrdctType.ProductTypeName.ToString()

            If DelPrdctType.DisbursementMode = "Cheque" Then
                CurrentAt = "Product Type : [Code:  " & DelPrdctType.ProductTypeCode & " ; Name:  " & DelPrdctType.ProductTypeName & " ; Disbursement Mode: " & DelPrdctType.DisbursementMode & " ; IsFunded:  " & DelPrdctType.IsFunded & " ; IsActive:  " & DelPrdctType.IsActive & " ; IsApproved:  " & DelPrdctType.IsApproved & "]"

            ElseIf DelPrdctType.DisbursementMode = "DD" Then
                CurrentAt = "Product Type : [Code:  " & DelPrdctType.ProductTypeCode & " ; Name:  " & DelPrdctType.ProductTypeName & " ; Disbursement Mode: " & DelPrdctType.DisbursementMode & " ; Bank Code: " & DelPrdctType.NonPrincipalBankCode & " ; IsFunded:  " & DelPrdctType.IsFunded & " ; IsActive:  " & DelPrdctType.IsActive & " ; IsApproved:  " & DelPrdctType.IsApproved & "]"

            ElseIf DelPrdctType.DisbursementMode = "PO" Then
                CurrentAt = "Product Type : [Code:  " & DelPrdctType.ProductTypeCode & " ; Name:  " & DelPrdctType.ProductTypeName & " ; Disbursement Mode: " & DelPrdctType.DisbursementMode & " ; Payable Account Number: " & DelPrdctType.PayableAccountNumber & " ; Payable Account Branch Code: " & DelPrdctType.PayableAccountBranchCode & " ; Payable Account Currency: " & DelPrdctType.PayableAccountCurrency & " ; IsFunded:  " & DelPrdctType.IsFunded & " ; IsActive:  " & DelPrdctType.IsActive & " ; IsApproved:  " & DelPrdctType.IsApproved & "]"

            ElseIf DelPrdctType.DisbursementMode = "Direct Credit" Or DelPrdctType.DisbursementMode = "Other Credit" Then
                CurrentAt = "Product Type : [Code:  " & DelPrdctType.ProductTypeCode & " ; Name:  " & DelPrdctType.ProductTypeName & " ; Disbursement Mode: " & DelPrdctType.DisbursementMode & " ; Funded Account Number: " & DelPrdctType.FundedAccountNumber & " ; Funded Account Branch Code: " & DelPrdctType.FundedAccountBranchCode & " ; Funded Account Currency: " & DelPrdctType.FundedAccountCurrency & " ; IsSundryAllow:  " & DelPrdctType.IsSundaryAllow & " ; IsFunded:  " & DelPrdctType.IsFunded & " ; IsActive:  " & DelPrdctType.IsActive & " ; IsApproved:  " & DelPrdctType.IsApproved & "]"

            End If

            PrdctType.MarkAsDeleted()
            PrdctType.Save()

            If DelPrdctType.DisbursementMode = "Cheque" Then
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Product Type", DelPrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

            ElseIf DelPrdctType.DisbursementMode = "DD" Then
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Product Type", DelPrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

            ElseIf DelPrdctType.DisbursementMode = "PO" Then
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Product Type", DelPrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

            ElseIf DelPrdctType.DisbursementMode = "Direct Credit" Or PrdctType.DisbursementMode = "Other Credit" Then
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Product Type", DelPrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If



        End Sub






        Public Shared Function GetAllActiveApprovedNonPrincipalDDEnabledAndDDPreferredBank(ByVal ProductTypeCode As String) As DataTable

            Dim qryBank As New ICBankQuery("qryBank")
            Dim qryDDPayableAcc As New ICDDPayableAccountsQuery("qryDDPayableAcc")
            Dim dt As DataTable

            qryBank.Select(qryBank.BankCode, qryBank.BankName)
            qryBank.Where(qryBank.IsActive = True And qryBank.IsApproved = True And qryBank.IsPrincipal = False And qryBank.IsDDEnabled = True)
            qryBank.Where(qryBank.BankCode.NotIn(qryDDPayableAcc.[Select](qryDDPayableAcc.BankCode).Where(qryDDPayableAcc.ProductTypeCode = ProductTypeCode.ToString())))
            qryBank.OrderBy(qryBank.BankName.Ascending)

            dt = qryBank.LoadDataTable()

            Return dt

        End Function

        Public Shared Function IsDDPreferedBankIsAddedForDDProductTypeToApprove(ByVal ProductTypeCode As String) As Boolean

            Dim qryBank As New ICBankQuery("qryBank")
            Dim qryDDPayableAcc As New ICDDPayableAccountsQuery("qryDDPayableAcc")
            Dim dt As DataTable

            qryDDPayableAcc.Select(qryDDPayableAcc.BankCode)
            qryDDPayableAcc.InnerJoin(qryBank).On(qryDDPayableAcc.BankCode = qryBank.BankCode)
            qryDDPayableAcc.Where(qryBank.IsActive = True And qryBank.IsApproved = True And qryBank.IsPrincipal = False And qryBank.IsDDEnabled = True And qryBank.IsDDPreferred = True)
            qryDDPayableAcc.Where(qryDDPayableAcc.ProductTypeCode = ProductTypeCode.ToString)
            dt = qryDDPayableAcc.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If


        End Function
        Public Shared Function IsProductTypeExistByProductTypeCode2(ByVal TemplateCode As String, ByVal APNFUTFields As ICAccountPaymentNatureFileUploadTemplatefields) As DataTable
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICAPNPTypeTemplates As New ICAccountsPaymentNatureProductTypeTemplatesQuery("qryObjICAPNPTypeTemplates")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim dt As New DataTable
            qryObjICProductType.Select(qryObjICProductType.ProductTypeCode)
            qryObjICProductType.InnerJoin(qryObjICAPNPTypeTemplates).On(qryObjICProductType.ProductTypeCode = qryObjICAPNPTypeTemplates.ProductCode)
            qryObjICProductType.InnerJoin(qryObjICAPNPType).On(qryObjICAPNPTypeTemplates.AccountNumber = qryObjICAPNPType.AccountNumber And qryObjICAPNPTypeTemplates.BranchCode = qryObjICAPNPType.BranchCode And qryObjICAPNPTypeTemplates.Currency = qryObjICAPNPType.Currency And qryObjICAPNPTypeTemplates.PaymentNatureCode = qryObjICAPNPType.PaymentNatureCode And qryObjICAPNPTypeTemplates.ProductCode = qryObjICAPNPType.ProductTypeCode)
            qryObjICProductType.Where(qryObjICProductType.IsActive = True And qryObjICProductType.IsApproved = True And qryObjICAPNPType.IsApproved = True)
            qryObjICProductType.Where(qryObjICAPNPTypeTemplates.AccountNumber = APNFUTFields.AccountNumber And qryObjICAPNPTypeTemplates.BranchCode = APNFUTFields.BranchCode And qryObjICAPNPTypeTemplates.Currency = APNFUTFields.Currency And qryObjICAPNPTypeTemplates.PaymentNatureCode = APNFUTFields.PaymentNatureCode And qryObjICAPNPTypeTemplates.TemplateID = TemplateCode.ToString)
            dt = qryObjICProductType.LoadDataTable

            Return dt

        End Function
        Public Shared Function GetTranTypeByClearingAndTransferType(ByVal ClearingType As String, ByVal TransferType As String,
                                                          ByVal RequiredTranType As String, ByVal ProductType As ICProductType) As String

            Dim TranType As String = Nothing
            TranType = ""
            'If ClearingType = "Clearing" Then
            '    If ProductType.DisbursementMode = "PO" Then
            '        If TransferType = "InterCity" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingInterCityOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingInterCityResTranType
            '            ElseIf RequiredTranType = "Reversal" Then
            '                TranType = ProductType.ReversalTransactionType
            '            End If
            '        ElseIf TransferType = "SameDay" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingSameDayOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingSameDayResTranType
            '            End If
            '        ElseIf TransferType = "Normal" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingNormalOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingNormalResTranType
            '            End If
            '        End If
            '    ElseIf ProductType.DisbursementMode = "Cheque" Then
            '        If TransferType = "InterCity" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingInterCityOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingInterCityOrgTranType
            '            End If
            '        ElseIf TransferType = "SameDay" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingSameDayOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingSameDayOrgTranType
            '            End If
            '        ElseIf TransferType = "Normal" Then
            '            If RequiredTranType = "Originating" Then
            '                TranType = ProductType.ClearingNormalOrgTranType
            '            ElseIf RequiredTranType = "Responding" Then
            '                TranType = ProductType.ClearingNormalOrgTranType
            '            End If
            '        End If
            '    End If
            'ElseIf ClearingType = "Issuance" Then
            '    If ProductType.DisbursementMode = "PO" Or ProductType.DisbursementMode = "COTC" Then
            '        If RequiredTranType = "Originating" Then
            '            TranType = ProductType.OriginatingTransactionType
            '        ElseIf RequiredTranType = "Responding" Then
            '            TranType = ProductType.RespondingTranType
            '        End If
            '    ElseIf ProductType.DisbursementMode = "DD" Then
            '        If RequiredTranType = "Originating" Then
            '            TranType = ProductType.OriginatingTransactionType
            '        ElseIf RequiredTranType = "Responding" Then
            '            TranType = ProductType.RespondingTranType
            '        End If
            '    End If
            'ElseIf ClearingType = "Funds Transfer" Then
            '    If ProductType.DisbursementMode = "PO" Or ProductType.DisbursementMode = "COTC" Then
            '        If RequiredTranType = "Originating" Then
            '            TranType = ProductType.ClearingFTOrgTranType
            '        ElseIf RequiredTranType = "Responding" Then
            '            TranType = ProductType.ClearingFTRespTranType
            '        End If
            '    ElseIf ProductType.DisbursementMode = "Cheque" Then
            '        If RequiredTranType = "Originating" Then
            '            TranType = ProductType.ClearingFTOrgTranType
            '        ElseIf RequiredTranType = "Responding" Then
            '            TranType = ProductType.ClearingFTRespTranType
            '        End If
            '    End If
            'ElseIf ClearingType = "Reversal" Then
            '    TranType = ProductType.ReversalTransactionType
            'End If

            Return TranType
        End Function
        'Public Shared Function GetTranTypeByClearingAndTransferType(ByVal ClearingType As String, ByVal TransferType As String,
        '                                                  ByVal RequiredTranType As String, ByVal ProductType As ICProductType) As String

        '    Dim TranType As String = Nothing
        '    If ClearingType = "Clearing" Then
        '        If ProductType.DisbursementMode = "PO" Then
        '            If TransferType = "InterCity" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingInterCityOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingInterCityResTranType

        '                End If
        '            ElseIf TransferType = "SameDay" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingSameDayOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingSameDayResTranType
        '                End If
        '            ElseIf TransferType = "Normal" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingNormalOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingNormalResTranType
        '                End If
        '            End If
        '        ElseIf ProductType.DisbursementMode = "Cheque" Then
        '            If TransferType = "InterCity" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingInterCityOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingInterCityOrgTranType
        '                End If
        '            ElseIf TransferType = "SameDay" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingSameDayOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingSameDayOrgTranType
        '                End If
        '            ElseIf TransferType = "Normal" Then
        '                If RequiredTranType = "Originating" Then
        '                    TranType = ProductType.ClearingNormalOrgTranType
        '                ElseIf RequiredTranType = "Responding" Then
        '                    TranType = ProductType.ClearingNormalOrgTranType
        '                End If
        '            End If
        '        End If
        '    ElseIf ClearingType = "Issuance" Then
        '        If ProductType.DisbursementMode = "PO" Or ProductType.DisbursementMode = "COTC" Then
        '            If RequiredTranType = "Originating" Then
        '                TranType = ProductType.OriginatingTransactionType
        '            ElseIf RequiredTranType = "Responding" Then
        '                TranType = ProductType.RespondingTranType
        '            End If
        '        ElseIf ProductType.DisbursementMode = "DD" Then
        '            If RequiredTranType = "Originating" Then
        '                TranType = ProductType.OriginatingTransactionType
        '            ElseIf RequiredTranType = "Responding" Then
        '                TranType = ProductType.RespondingTranType
        '            End If
        '        End If
        '    ElseIf ClearingType = "Funds Transfer" Then
        '        If ProductType.DisbursementMode = "PO" Or ProductType.DisbursementMode = "COTC" Then
        '            If RequiredTranType = "Originating" Then
        '                TranType = ProductType.ClearingFTOrgTranType
        '            ElseIf RequiredTranType = "Responding" Then
        '                TranType = ProductType.ClearingFTRespTranType
        '            End If
        '        ElseIf ProductType.DisbursementMode = "Cheque" Then
        '            If RequiredTranType = "Originating" Then
        '                TranType = ProductType.ClearingFTOrgTranType
        '            ElseIf RequiredTranType = "Responding" Then
        '                TranType = ProductType.ClearingFTRespTranType
        '            End If
        '        End If
        '    End If

        '    Return TranType
        'End Function
        
    End Class
End Namespace

