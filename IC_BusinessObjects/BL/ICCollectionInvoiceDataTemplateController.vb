﻿Imports Telerik.Web.UI
Imports System
Imports System.Data.SqlClient
Namespace IC

    Public Class ICCollectionInvoiceDataTemplateController
        Public Shared Function AddInvoiceDataTemplate(ByVal objInvoiceDataTemplate As ICCollectionInvoiceDataTemplate, ByVal isUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String) As Integer

            Dim objICInvoiceDataTempForSave As New ICCollectionInvoiceDataTemplate
            Dim objICInvoiceDataTempFrom As New ICCollectionInvoiceDataTemplate
            Dim InvoiceTemplateID As Integer = 0
            Dim ActionStr As String = ""


            If (isUpdate = False) Then
                objICInvoiceDataTempForSave.CreateBy = objInvoiceDataTemplate.CreateBy
                objICInvoiceDataTempForSave.CreateDate = objInvoiceDataTemplate.CreateDate
            Else
                objICInvoiceDataTempForSave.LoadByPrimaryKey(objInvoiceDataTemplate.CollectionInvoiceTemplateID)
                objICInvoiceDataTempFrom.LoadByPrimaryKey(objInvoiceDataTemplate.CollectionInvoiceTemplateID)
            End If
            objICInvoiceDataTempForSave.TemplateName = objInvoiceDataTemplate.TemplateName
            objICInvoiceDataTempForSave.FileUploadTemplateCode = objInvoiceDataTemplate.FileUploadTemplateCode
            objICInvoiceDataTempForSave.FieldDelimiter = objInvoiceDataTemplate.FieldDelimiter
            objICInvoiceDataTempForSave.IsFixLength = objInvoiceDataTemplate.IsFixLength
            objICInvoiceDataTempForSave.TemplateFormat = objInvoiceDataTemplate.TemplateFormat
            objICInvoiceDataTempForSave.SheetName = objInvoiceDataTemplate.SheetName
            objICInvoiceDataTempForSave.TemplateType = objInvoiceDataTemplate.TemplateType
            objICInvoiceDataTempForSave.DetailIdentifier = objInvoiceDataTemplate.DetailIdentifier
            objICInvoiceDataTempForSave.HeaderIdentifier = objInvoiceDataTemplate.HeaderIdentifier
            objICInvoiceDataTempForSave.IsActive = objInvoiceDataTemplate.IsActive
            objICInvoiceDataTempForSave.IsPublished = objInvoiceDataTemplate.IsPublished
            objICInvoiceDataTempForSave.Save()
            InvoiceTemplateID = objICInvoiceDataTempForSave.CollectionInvoiceTemplateID
            If isUpdate = False Then
                ICUtilities.AddAuditTrail("Invoice Data template : " & objICInvoiceDataTempForSave.TemplateName & " :ID " & objICInvoiceDataTempForSave.CollectionInvoiceTemplateID & ", format [ " & objICInvoiceDataTempForSave.TemplateFormat & " ], Is published [ " & objICInvoiceDataTempForSave.IsPublished & " ]  added.", "Invoice Data Template", objICInvoiceDataTempForSave.CollectionInvoiceTemplateID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            Else
                ActionStr = "Invoice Data Template " & objICInvoiceDataTempForSave.TemplateName & "  Template name from [ " & objICInvoiceDataTempFrom.TemplateName & " ] to [ " & objICInvoiceDataTempForSave.TemplateName & " ] ; "
                '    ActionStr += "code from [ " & objICInvoiceDataTempFrom.FileUploadTemplateCode & " ] to [ " & objICInvoiceDataTempForSave.FileUploadTemplateCode & " ] ; "
                ActionStr += "Template format from [ " & objICInvoiceDataTempFrom.TemplateFormat & " ] to [ " & objICInvoiceDataTempForSave.TemplateFormat & " ]; "
                ActionStr += "Template type from [ " & objICInvoiceDataTempFrom.TemplateType & " ] to [ " & objICInvoiceDataTempForSave.TemplateType & " ]; "
                ActionStr += "Header identifier from [ " & objICInvoiceDataTempFrom.HeaderIdentifier & " ] to [ " & objICInvoiceDataTempForSave.HeaderIdentifier & " ]; "
                ActionStr += "Detail identifier from [ " & objICInvoiceDataTempFrom.DetailIdentifier & " ] to [ " & objICInvoiceDataTempForSave.DetailIdentifier & " ]; "
                ActionStr += "Sheet name from [ " & objICInvoiceDataTempFrom.SheetName & " ] to [ " & objICInvoiceDataTempForSave.SheetName & " ]; "
                ActionStr += "Is Published from [ " & objICInvoiceDataTempFrom.IsPublished & " ] to [ " & objICInvoiceDataTempForSave.IsPublished & " ] updated."

                ICUtilities.AddAuditTrail(ActionStr.ToString, "Invoice Data Template", objICInvoiceDataTempForSave.CollectionInvoiceTemplateID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If

            Return InvoiceTemplateID
        End Function

        Public Shared Sub DeleteTemplate(ByVal InvoiceTemplateID As String, ByVal UsersID As String, ByVal UsersName As String)


            Dim objInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
            Dim objInvoiceDataTemplateDeleted As New ICCollectionInvoiceDataTemplate



            objInvoiceDataTemplateDeleted.LoadByPrimaryKey(InvoiceTemplateID)
            objInvoiceDataTemplate.LoadByPrimaryKey(InvoiceTemplateID)
            objInvoiceDataTemplate.MarkAsDeleted()
            objInvoiceDataTemplate.Save()
            ICUtilities.AddAuditTrail("Invoice Data template : " & objInvoiceDataTemplateDeleted.TemplateName & ", type [ " & objInvoiceDataTemplateDeleted.TemplateType & " ] deleted.", "Invoice Data Template", objInvoiceDataTemplateDeleted.CollectionInvoiceTemplateID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
        End Sub

        Public Shared Sub GetInvoiceDataTemplateForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim objInvoiceDataTemplate As New ICCollectionInvoiceDataTemplateCollection
            Dim dt As New DataTable
            objInvoiceDataTemplate.es.Connection.CommandTimeout = 3600

            If Not PageNumber = 0 Then
                objInvoiceDataTemplate.Query.Select(objInvoiceDataTemplate.Query.CollectionInvoiceTemplateID, objInvoiceDataTemplate.Query.TemplateName, objInvoiceDataTemplate.Query.TemplateFormat, objInvoiceDataTemplate.Query.SheetName, objInvoiceDataTemplate.Query.FieldDelimiter, objInvoiceDataTemplate.Query.IsFixLength)
                objInvoiceDataTemplate.Query.Select(objInvoiceDataTemplate.Query.TemplateType, objInvoiceDataTemplate.Query.IsActive)
                objInvoiceDataTemplate.Query.Select(objInvoiceDataTemplate.Query.IsPublished.Case.When(objInvoiceDataTemplate.Query.IsPublished.IsNull).Then("False").Else(objInvoiceDataTemplate.Query.IsPublished).End.As("IsPublished"))
                objInvoiceDataTemplate.Query.OrderBy(objInvoiceDataTemplate.Query.CollectionInvoiceTemplateID.Descending)
                objInvoiceDataTemplate.Query.Load()
                dt = objInvoiceDataTemplate.Query.LoadDataTable

                'If Not PageNumber = 0 Then
                '    objInvoiceDataTemplate.Query.es.PageNumber = PageNumber
                '    objInvoiceDataTemplate.Query.es.PageSize = PageSize
                rg.DataSource = dt

                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objInvoiceDataTemplate.Query.es.PageNumber = 1
                objInvoiceDataTemplate.Query.es.PageSize = PageSize
                objInvoiceDataTemplate.Query.OrderBy(objInvoiceDataTemplate.Query.CollectionInvoiceTemplateID.Descending)
                objInvoiceDataTemplate.Query.Load()
                dt = objInvoiceDataTemplate.Query.LoadDataTable
                rg.DataSource = dt

                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

        'Public Shared Function GetAllInvoiceDataTemplates() As DataTable
        '    Dim collInvoiceDataTemplate As New ICCollectionInvoiceDataTemplateCollection
        '    collInvoiceDataTemplate.Query.Select(collInvoiceDataTemplate.Query.CollectionInvoiceTemplateID, collInvoiceDataTemplate.Query.TemplateName, collInvoiceDataTemplate.Query.FileUploadTemplateCode)
        '    collInvoiceDataTemplate.Query.OrderBy(collInvoiceDataTemplate.Query.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    Return collInvoiceDataTemplate.Query.LoadDataTable
        'End Function

        Public Shared Function GetAllInvoiceDataTemplates() As DataTable
            Dim collInvoiceDataTemplate As New ICCollectionInvoiceDataTemplateCollection
            collInvoiceDataTemplate.Query.Select(collInvoiceDataTemplate.Query.CollectionInvoiceTemplateID, collInvoiceDataTemplate.Query.TemplateName, collInvoiceDataTemplate.Query.FileUploadTemplateCode)
            collInvoiceDataTemplate.Query.Where(collInvoiceDataTemplate.Query.IsActive = True And collInvoiceDataTemplate.Query.IsPublished = True)
            collInvoiceDataTemplate.Query.OrderBy(collInvoiceDataTemplate.Query.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            Return collInvoiceDataTemplate.Query.LoadDataTable
        End Function

        Public Shared Function CheckInvoiceTemplateIDincollNature() As Boolean
            Dim collNature As New ICCollectionNatureCollection
            collNature.es.Connection.CommandTimeout = 3600

            'collNature.Query.Select(collNature.Query.te)

            If collNature.Query.Load() Then
                Return True
            Else
                Return False
            End If

        End Function

        'Public Shared Function ExecuteSqlStatementForCreateTableVIATemplate(ByVal SQLStringToExecute As String) As Boolean
        '    Dim Result As Boolean = False
        '    Dim SQLConn As New SqlConnection
        '    Dim SQLCmd As SqlCommand
        '    Dim ConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("SiteSqlServer").ToString
        '    If SQLConn.State = ConnectionState.Open Then
        '        SQLConn.Close()
        '        SQLConn.Dispose()
        '    End If
        '    SQLConn.ConnectionString = ConnectionString
        '    SQLConn.Open()
        '    SQLCmd = New SqlCommand(SQLStringToExecute, SQLConn)
        '    Try
        '        SQLCmd.ExecuteNonQuery()
        '        SQLConn.Close()
        '        SQLConn.Dispose()
        '        SQLConn = New SqlConnection
        '        SQLConn.ConnectionString = ConnectionString
        '        SQLConn.Open()
        '        SQLConn.Close()
        '        SQLConn.Dispose()
        '        Result = True
        '    Catch ex As SqlException
        '        Result = False
        '        Throw ex
        '    End Try
        '    Return Result
        'End Function
        'Public Shared Function GetSQLStringWithFieldsByTemplateID(ByVal CollectionInvoiceDataTemplateID As String, ByRef StrForAuditTrail As String, ByRef SqlStringDetail As String) As String

        '    Dim objICCollectionInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
        '    Dim objICCollectionInvoiceDataTemplateFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
        '    objICCollectionInvoiceDataTemplate.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
        '    objICCollectionInvoiceDataTemplateFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsTemplateID(CollectionInvoiceDataTemplateID)
        '    Dim SqlStringNonFlexi As String = Nothing
        '    SqlStringDetail = Nothing
        '    Dim i As Integer = 0
        '    Dim SqlPK As String = " PRIMARY KEY ("
        '    Dim SqlFK As String = Nothing



        '    SqlStringNonFlexi = "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "("
        '    SqlStringDetail += "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "_Detail("
        '    StrForAuditTrail = "Database [ IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " ] for template [ " & objICCollectionInvoiceDataTemplate.TemplateName & " ] created with fields : </br>"
        '    StrForAuditTrail += "Follwoing fields are set as Primary Key: </br>"

        '    For Each objICPrimaryKeyField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
        '        If objICPrimaryKeyField.IsReference = True Then
        '            StrForAuditTrail += objICPrimaryKeyField.FieldName & " " & objICPrimaryKeyField.FieldType
        '        End If
        '    Next



        '    For Each objICCollectionInvoiceDataTemplateField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
        '        If objICCollectionInvoiceDataTemplateField.FieldType = "NonFlexi" Then
        '            SqlStringNonFlexi += objICCollectionInvoiceDataTemplateField.FieldName
        '            If objICCollectionInvoiceDataTemplateField.IsReference = False Then
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
        '            End If
        '            If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Numeric(18,2) NOT NULL ,"
        '                    SqlStringNonFlexi += " Numeric(18,2) NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Numeric(18,2) NULL ,"
        '                End If

        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Integer NOT NULL ,"
        '                    SqlStringNonFlexi += " Integer NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Integer NULL ,"
        '                End If
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
        '                    SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
        '                End If
        '            End If
        '        Else
        '            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '            If objICCollectionInvoiceDataTemplateField.IsReference = False Then
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
        '            End If
        '            If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
        '                SqlStringDetail += " Numeric(18,2) NULL ,"
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
        '                SqlStringDetail += " Integer NULL ,"
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
        '                SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
        '            End If
        '        End If
        '    Next
        '    SqlStringNonFlexi += " CreatedDate DateTime NULL, CreatedBy int NULL,Status varchar(20),LastStatus varchar(20),FileID Varchar(20) NULL,TemplateID Varchar(20) NULL,"
        '    SqlPK = SqlPK.Remove(SqlPK.Length - 1, 1) & ")"
        '    SqlFK = SqlFK.Remove(SqlFK.Length - 1, 1)
        '    'SqlString = SqlString.Remove(SqlString.Length - 1, 1)
        '    SqlStringNonFlexi += SqlPK
        '    SqlStringNonFlexi += ")"
        '    SqlStringDetail += "FileID Varchar(20) NULL,TemplateID Varchar(20) NULL, " & SqlPK & ", CONSTRAINT fk_IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " FOREIGN KEY("
        '    SqlStringDetail += SqlFK & ") REFERENCES IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "(" & SqlFK & ")"
        '    SqlStringDetail += ")"
        '    StrForAuditTrail = StrForAuditTrail.Remove(StrForAuditTrail.Length - 1, 1)
        '    Return SqlStringNonFlexi

        'End Function
        Public Shared Function GetSQLInsertINTOStringByTemplateID(ByVal CollectionInvoiceDataTemplateID As String, ByRef StrForAuditTrail As String) As String

            Dim objICCollectionInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
            Dim objICCollectionInvoiceDataTemplateFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            objICCollectionInvoiceDataTemplate.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
            objICCollectionInvoiceDataTemplateFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsTemplateID(CollectionInvoiceDataTemplateID)
            Dim SqlString As String = Nothing

            SqlString = "CREATE TABLE Client_DB" & objICCollectionInvoiceDataTemplate.TemplateName
            SqlString += " (Client_DB_" & objICCollectionInvoiceDataTemplate.TemplateName & "ID Int Not Null IDENTITY(1,1) Primary Key,"


            StrForAuditTrail = "Database [ Client_DB" & objICCollectionInvoiceDataTemplate.TemplateName & " ] for template [ " & objICCollectionInvoiceDataTemplate.TemplateName & " ] created with fields : </br>"
            StrForAuditTrail += "Client_DB_" & objICCollectionInvoiceDataTemplate.TemplateName & "ID set as Primary Key. Other Fields are </br>"



            For Each objICCollectionInvoiceDataTemplateField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
                SqlString += objICCollectionInvoiceDataTemplateField.FieldName
                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"

                If objICCollectionInvoiceDataTemplateField.FieldDBType = "Numeric" Then
                    SqlString += " Numeric(18,0) NULL ,"
                ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Text" Then
                    SqlString += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
                ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Char" Then
                    SqlString += " Char(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
                ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Date" Then
                    SqlString += " Date NULL ,"
                ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "DateTime" Then
                    SqlString += " DateTime NULL ,"
                End If
            Next
            SqlString = SqlString.Remove(SqlString.Length - 1, 1)
            SqlString += ")"
            StrForAuditTrail = StrForAuditTrail.Remove(StrForAuditTrail.Length - 1, 1)
            Return SqlString

        End Function
        Public Shared Function DoesTableExist(ByVal TableName As String) As Boolean
            Try
                Dim Result As Boolean = False
                Dim SQLConn As New SqlConnection
                Dim ConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("SiteSqlServer").ToString
                If SQLConn.State = ConnectionState.Open Then
                    SQLConn.Close()
                    SQLConn.Dispose()
                End If
                SQLConn.ConnectionString = ConnectionString
                SQLConn.Open()
                Dim restrictions(3) As String
                restrictions(2) = TableName
                Dim dbTbl As DataTable = SQLConn.GetSchema("Tables", restrictions)


                If dbTbl.Rows.Count = 0 Then
                    'Table does not exist
                    Result = False
                Else
                    'Table exists
                    Result = True
                End If
                dbTbl.Dispose()
                SQLConn.Close()
                SQLConn.Dispose()
                Return Result
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DeleteInvoiceDataBase(ByVal CollectionInvoiceDataTemplateID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal TableType As String) As Boolean
            Dim Result As Boolean = False
            Dim ActionString As String = Nothing
            Dim SQLCmdToDropDataBase As String = Nothing
            Dim objICInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
            Dim InvoiceDataBaseToDelete As String = Nothing
            objICInvoiceDataTemplate.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
            If TableType = "Detail" Then
                InvoiceDataBaseToDelete = "IC_InvoiceDB_" & objICInvoiceDataTemplate.TemplateName & "_Detail"

                SQLCmdToDropDataBase = "Drop Table IC_InvoiceDB_" & objICInvoiceDataTemplate.TemplateName & "_Detail"
            Else
                InvoiceDataBaseToDelete = "IC_InvoiceDB_" & objICInvoiceDataTemplate.TemplateName

                SQLCmdToDropDataBase = "Drop Table IC_InvoiceDB_" & objICInvoiceDataTemplate.TemplateName
            End If

            Try
                If DoesTableExist(InvoiceDataBaseToDelete) = True Then
                    If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(SQLCmdToDropDataBase) = True Then
                        ActionString = "Invoice database [ IC_InvoiceDB_" & objICInvoiceDataTemplate.TemplateName & " ] for template [ " & objICInvoiceDataTemplate.TemplateName & " ] deleted."
                        ActionString += " Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
                        ICUtilities.AddAuditTrail(ActionString, "Invoice Data Template", objICInvoiceDataTemplate.CollectionInvoiceTemplateID, UsersID, UsersName, "DELETE")
                        Result = True
                    End If
                Else
                    Throw New Exception(InvoiceDataBaseToDelete & " do not exists.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
            Return Result
        End Function
        Public Shared Function GetTemplatesByCollectionNature(ByVal CollectionNatureCode As String) As DataTable
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplateQuery("qryObjICCollectionInvoiceDBTemplate")
            Dim dt As New DataTable
            qryObjICCollectionInvoiceDBTemplate.Select(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID, qryObjICCollectionInvoiceDBTemplate.TemplateName)
            qryObjICCollectionInvoiceDBTemplate.InnerJoin(qryObjICCollectionNature).On(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID = qryObjICCollectionNature.InvoiceTemplateID)
            qryObjICCollectionInvoiceDBTemplate.Where(qryObjICCollectionNature.CollectionNatureCode = CollectionNatureCode And qryObjICCollectionNature.IsActive = True)

            qryObjICCollectionInvoiceDBTemplate.OrderBy(qryObjICCollectionInvoiceDBTemplate.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICCollectionInvoiceDBTemplate.LoadDataTable()
            Return dt
        End Function
        Public Shared Function GetTemplatesByCollectionNatureCode(ByVal CollectionNatureCode As String) As DataTable
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplateQuery("qryObjICCollectionInvoiceDBTemplate")
            Dim dt As New DataTable
            qryObjICCollectionInvoiceDBTemplate.Select(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID, qryObjICCollectionInvoiceDBTemplate.TemplateName)
            qryObjICCollectionInvoiceDBTemplate.InnerJoin(qryObjICCollectionNature).On(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID = qryObjICCollectionNature.InvoiceTemplateID)
            qryObjICCollectionInvoiceDBTemplate.Where(qryObjICCollectionNature.CollectionNatureCode = CollectionNatureCode And qryObjICCollectionNature.IsActive = True)

            qryObjICCollectionInvoiceDBTemplate.OrderBy(qryObjICCollectionInvoiceDBTemplate.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICCollectionInvoiceDBTemplate.LoadDataTable()
            Return dt
        End Function


        Public Shared Function GetTemplatesByCollectionNatureAllowedFileUpload(ByVal CollectionNatureCode As String) As DataTable
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            Dim qryObjICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplateQuery("qryObjICCollectionInvoiceDBTemplate")
            Dim dt As New DataTable
            qryObjICCollectionInvoiceDBTemplate.Select(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID, qryObjICCollectionInvoiceDBTemplate.TemplateName)
            qryObjICCollectionInvoiceDBTemplate.InnerJoin(qryObjICCollectionNature).On(qryObjICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID = qryObjICCollectionNature.InvoiceTemplateID)
            qryObjICCollectionInvoiceDBTemplate.Where(qryObjICCollectionNature.CollectionNatureCode = CollectionNatureCode And qryObjICCollectionNature.IsActive = True)
            qryObjICCollectionInvoiceDBTemplate.Where(qryObjICCollectionNature.InvoiceDBUploadVIAFileUpload = True)
            qryObjICCollectionInvoiceDBTemplate.OrderBy(qryObjICCollectionInvoiceDBTemplate.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICCollectionInvoiceDBTemplate.LoadDataTable()
            Return dt
        End Function

        Public Shared Function GetAllTaggedandUntaggedInvoiceDataTemplates(ByVal Isupdate As Boolean, ByVal TemplateID As String) As DataTable
            Dim qryInvoiceDataTemplate As New ICCollectionInvoiceDataTemplateQuery("qryInvoiceDataTemplate")
            Dim qryCollNature As New ICCollectionNatureQuery("qryCollNature")

            qryInvoiceDataTemplate.Select(qryInvoiceDataTemplate.CollectionInvoiceTemplateID, qryInvoiceDataTemplate.TemplateName)
            If Isupdate = True Then
                qryInvoiceDataTemplate.Where(qryInvoiceDataTemplate.CollectionInvoiceTemplateID.NotIn(qryCollNature.[Select](qryCollNature.InvoiceTemplateID)) Or qryInvoiceDataTemplate.CollectionInvoiceTemplateID = TemplateID)
            Else
                qryInvoiceDataTemplate.Where(qryInvoiceDataTemplate.CollectionInvoiceTemplateID.NotIn(qryCollNature.[Select](qryCollNature.InvoiceTemplateID)))
            End If

            qryInvoiceDataTemplate.Where(qryInvoiceDataTemplate.IsActive = True And qryInvoiceDataTemplate.IsPublished = True)
            qryInvoiceDataTemplate.OrderBy(qryInvoiceDataTemplate.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            Return qryInvoiceDataTemplate.LoadDataTable
        End Function
    End Class
End Namespace
