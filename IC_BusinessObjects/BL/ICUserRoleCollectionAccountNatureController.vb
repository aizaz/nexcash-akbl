﻿Imports Telerik.Web.UI
Imports System.Web

Namespace IC

    Public Class ICUserRoleCollectionAccountNatureController
        Public Shared Function GetAllUserRolesCollectionAccountNatureByRoleIDAndUserID(ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesCollectionAccountsAndCollectionNatureCollection
            Dim objICUserRolesCollectionAccNatureColl As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            objICUserRolesCollectionAccNatureColl.LoadAll()
            If RoleID <> "0" Then
                objICUserRolesCollectionAccNatureColl.Query.Where(objICUserRolesCollectionAccNatureColl.Query.RoleID = RoleID.ToString)
            End If
            If UsersId <> "0" Then
                objICUserRolesCollectionAccNatureColl.Query.Where(objICUserRolesCollectionAccNatureColl.Query.UserID = UsersId.ToString)
            End If

            objICUserRolesCollectionAccNatureColl.Query.Load()
            Return objICUserRolesCollectionAccNatureColl
        End Function

        Public Shared Function GetAllUserRolesbyCollectionAccountCollectionNature(ByVal CollectionNatureCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesCollectionAccountsAndCollectionNatureCollection
            Dim collICUserRolesCollectionAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            collICUserRolesCollectionAccNature.es.Connection.CommandTimeout = 3600
            collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.CollectionNatureCode = CollectionNatureCode.ToString And collICUserRolesCollectionAccNature.Query.AccountNumber = AccountNumber.ToString)
            collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.BranchCode = BranchCode.ToString And collICUserRolesCollectionAccNature.Query.Currency = Currency.ToString)
            If UsersId <> "0" Then
                collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.UserID = UsersId.ToString)
            End If
            If RoleID <> "0" Then
                collICUserRolesCollectionAccNature.Query.Where(collICUserRolesCollectionAccNature.Query.RoleID = RoleID.ToString)
            End If
            collICUserRolesCollectionAccNature.Query.Load()
            Return collICUserRolesCollectionAccNature

        End Function

    End Class

End Namespace