﻿Namespace IC
    Public Class ICRestraintMatrixController
        Public Shared Function GetRestraintMEssagesVIARestraintCodes(ByVal RestraintCodesList As ArrayList, ByVal RestraintType As String) As ICRestraintMatrixCollection
            Dim objICRestraintMatrixColl As New ICRestraintMatrixCollection


            objICRestraintMatrixColl.Query.Where(objICRestraintMatrixColl.Query.RestraintCode.In(RestraintCodesList))
            objICRestraintMatrixColl.Query.Where(objICRestraintMatrixColl.Query.RestraintType = RestraintType)
            objICRestraintMatrixColl.Query.OrderBy(objICRestraintMatrixColl.Query.RestraintCode.Ascending)
            objICRestraintMatrixColl.Query.Load()
            Return objICRestraintMatrixColl
        End Function
    End Class
End Namespace
