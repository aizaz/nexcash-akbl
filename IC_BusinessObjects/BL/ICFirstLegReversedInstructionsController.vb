﻿Namespace IC
    Public Class ICFirstLegReversedInstructionsController
        Public Shared Function AddICFirstLegRevInstruction(ByVal objICFLRev As ICFirstLegReversedInstructions, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String) As Boolean
            Dim objICFLRevForSave As New ICFirstLegReversedInstructions
            Dim StrAuditTrail As String = ""
            If IsUpdate = True Then
                objICFLRevForSave.LoadByPrimaryKey(objICFLRev.FirstLegReversedID)
            End If
            objICFLRevForSave.RelatedID = objICFLRev.RelatedID
            objICFLRevForSave.EntityType = objICFLRev.EntityType
            objICFLRevForSave.Amount = objICFLRev.Amount
            objICFLRevForSave.FromAccountNo = objICFLRev.FromAccountNo
            objICFLRevForSave.FromAccountBranchCode = objICFLRev.FromAccountBranchCode
            objICFLRevForSave.FromAccountCurrency = objICFLRev.FromAccountCurrency
            objICFLRevForSave.FromAccountProductCode = objICFLRev.FromAccountProductCode
            objICFLRevForSave.FromAccountSchemeCode = objICFLRev.FromAccountSchemeCode
            objICFLRevForSave.FromAccountType = objICFLRev.FromAccountType
            objICFLRevForSave.ToAccountNo = objICFLRev.ToAccountNo
            objICFLRevForSave.ToAccountBranchCode = objICFLRev.ToAccountBranchCode
            objICFLRevForSave.ToAccountCurrency = objICFLRev.ToAccountCurrency
            objICFLRevForSave.ToAccountProductCode = objICFLRev.ToAccountProductCode
            objICFLRevForSave.ToAccountSchemeCode = objICFLRev.ToAccountSchemeCode
            objICFLRevForSave.ToAccountType = objICFLRev.ToAccountType
            objICFLRevForSave.RBIndicator = objICFLRev.RBIndicator
            objICFLRevForSave.TxnType = objICFLRev.TxnType
            objICFLRevForSave.VoucherType = objICFLRev.VoucherType
            objICFLRevForSave.InstrumentType = objICFLRev.InstrumentType
            objICFLRevForSave.DebitGLCode = objICFLRev.DebitGLCode
            objICFLRevForSave.CreditGLCode = objICFLRev.CreditGLCode
            objICFLRevForSave.InstrumentNo = objICFLRev.InstrumentNo
            objICFLRevForSave.IsReversal = objICFLRev.IsReversal
            objICFLRevForSave.TransactionBranchCode = objICFLRev.TransactionBranchCode
            objICFLRevForSave.Creator = objICFLRev.Creator
            objICFLRevForSave.CreationDate = objICFLRev.CreationDate
            objICFLRevForSave.ErrorMessage = objICFLRev.ErrorMessage
            objICFLRevForSave.LastStatus = objICFLRev.LastStatus
            objICFLRevForSave.Status = objICFLRev.Status
            objICFLRevForSave.IsFirstLEgReversed = objICFLRev.IsFirstLEgReversed
            objICFLRevForSave.ProductTypeCode = objICFLRev.ProductTypeCode
            objICFLRevForSave.ReversalType = objICFLRev.ReversalType
            objICFLRevForSave.Save()
            If IsUpdate = False Then
                StrAuditTrail = "First Leg Reversal Instruction with ID [ " & objICFLRevForSave.FirstLegReversedID & " ] of type [ " & objICFLRevForSave.EntityType & " ] original ID"
                StrAuditTrail += " [ " & objICFLRevForSave.RelatedID & " ] added. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
                ICUtilities.AddAuditTrail(StrAuditTrail, objICFLRevForSave.EntityType, objICFLRevForSave.RelatedID, UsersID, UsersName, "ADD")
            End If
            Return True
        End Function
        Public Shared Function DeleteFirstLegReversedInstruction(ByVal FirstLEgRevID As String, ByVal UserID As String, ByVal UsersName As String) As Boolean
            Dim objFLRev As New ICFirstLegReversedInstructions
            Dim StrAuditTrail As String = ""
            Dim Result As Boolean = False
            If objFLRev.LoadByPrimaryKey(FirstLEgRevID) Then
                StrAuditTrail = "First Leg Reversal Instruction with ID [ " & objFLRev.FirstLegReversedID & " ] of type [ " & objFLRev.EntityType & " ] original ID"
                StrAuditTrail += " [ " & objFLRev.RelatedID & " ] deleted. Action was taken by user [ " & UserID & " ] [ " & UsersName & " ]"
                ICUtilities.AddAuditTrail(StrAuditTrail, objFLRev.EntityType, objFLRev.RelatedID, UserID, UsersName, "DELETE")
                objFLRev.MarkAsDeleted()
                objFLRev.Save()
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function UpDateFirstLegReversedInstructionsStatus(ByVal FirstLEgRevID As String, ByVal FromStatus As String, ByVal TotStatus As String, ByVal UserID As String, ByVal UsersName As String) As Boolean
            Dim objFLRev As New ICFirstLegReversedInstructions
            Dim objICInstructionFromStatus As New ICInstructionStatus
            Dim objICInstructionToStatus As New ICInstructionStatus

            objICInstructionFromStatus.LoadByPrimaryKey(FromStatus)
            objICInstructionToStatus.LoadByPrimaryKey(TotStatus)
            Dim StrAuditTrail As String = ""
            Dim Result As Boolean = False
            If objFLRev.LoadByPrimaryKey(FirstLEgRevID) Then
                StrAuditTrail = "First Leg Reversal Instruction with ID [ " & objFLRev.FirstLegReversedID & " ] of type [ " & objFLRev.EntityType & " ] original ID"
                StrAuditTrail += " [ " & objFLRev.RelatedID & " ] status update from [ " & objICInstructionFromStatus.StatusID & " ] [ " & objICInstructionFromStatus.StatusName & " ] to "
                StrAuditTrail += " status [ " & objICInstructionToStatus.StatusID & " ] [ " & objICInstructionToStatus.StatusName & " ]. Action was taken by user [ " & UserID & " ] [ " & UsersName & " ]"
                ICUtilities.AddAuditTrail(StrAuditTrail, objFLRev.EntityType, objFLRev.RelatedID, UserID, UsersName, "UPDATE")
                objFLRev.Status = TotStatus
                objFLRev.LastStatus = FromStatus
                objFLRev.Save()
                Result = True
            End If
            Return Result
        End Function
    End Class
End Namespace
