﻿
Namespace IC
    Public Class ICInstrumentController
        Public Shared Sub AddInstrumentSeriesFromMasterSeries(ByVal cInstrument As ICInstruments, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICInstruments As New ICInstruments
            Dim InstrumentID As Integer = 0
            objICInstruments.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                objICInstruments.CreatedOn = cInstrument.CreatedOn
                objICInstruments.CreatedBy = cInstrument.CreatedBy
                objICInstruments.CreationDate = cInstrument.CreationDate
                objICInstruments.Creater = cInstrument.Creater
                objICInstruments.OfficeID = cInstrument.OfficeID
                objICInstruments.SubSetID = cInstrument.SubSetID
                objICInstruments.IsUSed = cInstrument.IsUSed
            Else

                objICInstruments.LoadByPrimaryKey(cInstrument.InstrumentsID)
                objICInstruments.CreatedOn = cInstrument.CreatedOn
                objICInstruments.CreatedBy = cInstrument.CreatedBy
            End If
            objICInstruments.MasterSeriesID = cInstrument.MasterSeriesID
            objICInstruments.InstrumentNumber = cInstrument.InstrumentNumber

            objICInstruments.Save()

            InstrumentID = objICInstruments.InstrumentsID
            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Instrument number [ " & objICInstruments.InstrumentNumber & " ] for account number [ " & objICInstruments.UpToICMasterSeriesByMasterSeriesID.AccountNumber & " ] for master series [ " & objICInstruments.MasterSeriesID & " ] added.", "Instrument", InstrumentID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
            End If

        End Sub
        Public Shared Sub DeleteAddedInstrumentSeriesByMasterSeriesID(ByVal MasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objICInstrument As New ICInstruments
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = MasterSeriesID)
            objICInstrumentColl.Query.Load()
            If objICInstrumentColl.Query.Load Then
                For Each objICInstrumentInColl As ICInstruments In objICInstrumentColl
                    objICInstrument.LoadByPrimaryKey(objICInstrumentInColl.InstrumentsID)
                    objICInstrument.MarkAsDeleted()
                    objICInstrument.Save()
                    ICUtilities.AddAuditTrail("Intrument No: [ " & objICInstrumentInColl.InstrumentsID.ToString & " ] for master series [ " & objICInstrumentInColl.MasterSeriesID & " ] for account number [ " & objICInstrumentInColl.UpToICMasterSeriesByMasterSeriesID.UpToICAccountsByAccountNumber.AccountNumber & " ] deleted", "Instrument", objICInstrumentInColl.InstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub
        Public Shared Function GetTopAssignedInstrumentNumbersToPrintLocationsByMSID(ByVal MasterSeriesID As String, ByVal TopOrder As Integer) As Boolean
            Dim qryObjICInstruments As New ICInstrumentsQuery("qryObjICInstruments")
            Dim Result As Boolean = False
            Dim dt As New DataTable
            qryObjICInstruments.Select(qryObjICInstruments.InstrumentNumber)
            qryObjICInstruments.Where(qryObjICInstruments.OfficeID.IsNull() And qryObjICInstruments.MasterSeriesID = MasterSeriesID)
            qryObjICInstruments.OrderBy(qryObjICInstruments.InstrumentNumber.Descending)
            qryObjICInstruments.es.Top = TopOrder
            dt = qryObjICInstruments.LoadDataTable()
            If dt.Rows.Count >= TopOrder Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Sub DeleteUpdatedInstrumentSeriesByMasterSeriesID(ByVal MasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal TopOrder As Integer)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objICInstrument As New ICInstruments
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = MasterSeriesID And objICInstrumentColl.Query.OfficeID.IsNull)
            objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber.Descending)
            objICInstrumentColl.Query.es.Top = TopOrder
            objICInstrumentColl.Query.Load()
            If objICInstrumentColl.Query.Load Then
                For Each objICInstrumentInColl As ICInstruments In objICInstrumentColl
                    objICInstrument.LoadByPrimaryKey(objICInstrumentInColl.InstrumentsID)
                    objICInstrument.MarkAsDeleted()
                    objICInstrument.Save()
                    ICUtilities.AddAuditTrail("Intrument No. [ " & objICInstrumentInColl.InstrumentNumber & " ] of Master Series with ID [ " & objICInstrumentInColl.MasterSeriesID.ToString & " ] of account number [ " & objICInstrumentInColl.UpToICMasterSeriesByMasterSeriesID.AccountNumber & " ] deleted on update.", "Instrument", objICInstrumentInColl.InstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub
        Public Shared Function GetLastInstrumentOFMasterSeries(ByVal MasterSeriesID As String) As Integer
            Dim qryObjInstrument As New ICInstrumentsQuery("qryObjInstrument")
            Dim dt As New DataTable

            qryObjInstrument.Select(qryObjInstrument.InstrumentNumber)
            qryObjInstrument.Where(qryObjInstrument.MasterSeriesID = MasterSeriesID)
            qryObjInstrument.OrderBy(qryObjInstrument.InstrumentNumber.Descending)
            qryObjInstrument.es.Top = 1
            dt = qryObjInstrument.LoadDataTable
            Return CInt(dt.Rows(0)(0))
        End Function

        Public Shared Function IsInstrumentSeriesIsAssignedOrPrintedForSubSet(ByVal SubSetFrom As Long, ByVal SubSetTo As Long, ByVal MasterSeriesID As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrumentColl As New ICInstrumentsCollection
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = MasterSeriesID And objICInstrumentColl.Query.InstrumentNumber.Between(SubSetFrom, SubSetTo))
            If objICInstrumentColl.Query.Load() Then

                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    If Not objICInstrument.OfficeID Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If

            Return Result
        End Function
        Public Shared Function AssignedSubSetToPrintLocationFromSubSet(ByVal objICSubSet As ICSubSet, ByVal UsersID As String, ByVal UsersName As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objICInstrumentForUpDate As ICInstruments
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = objICSubSet.MasterSeriesID And objICInstrumentColl.Query.InstrumentNumber.Between(objICSubSet.SubSetFrom, objICSubSet.SubSetTo))
            If objICInstrumentColl.Query.Load() Then

                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    objICInstrumentForUpDate = New ICInstruments
                    objICInstrumentForUpDate.es.Connection.CommandTimeout = 3600
                    If objICInstrumentForUpDate.LoadByPrimaryKey(objICInstrument.InstrumentsID) Then
                        objICInstrumentForUpDate.OfficeID = objICSubSet.OfficeID
                        objICInstrumentForUpDate.SubSetID = objICSubSet.SubSetID
                        objICInstrumentForUpDate.Save()
                        ICUtilities.AddAuditTrail("Instrument no. " & objICInstrumentForUpDate.InstrumentNumber & " of master series [ " & objICInstrumentForUpDate.MasterSeriesID & " ] of account no. [ " & objICInstrumentForUpDate.UpToICMasterSeriesByMasterSeriesID.AccountNumber & " ] assigned a print location " & objICSubSet.UpToICOfficeByOfficeID.OfficeName & " with sub set ID" & objICSubSet.SubSetID & " .", "Instrument", objICInstrumentForUpDate.InstrumentsID, UsersID.ToString, UsersName.ToString, "ASSIGN")
                    End If
                Next
            End If

            Return Result
        End Function
        Public Shared Function IsInstrumentsExistForUpdateOfSubSet(ByVal TopOrder As Integer, ByVal objICSubSet As ICSubSet, ByVal SubSetTo As String, ByVal IsExtended As Boolean) As Boolean
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objICInstrument As ICInstruments
            Dim dt As New DataTable
            Dim Result As Boolean = False
            Dim RequiredLeavesCount As Long
            objICInstrumentColl.es.Connection.CommandTimeout = 3600


            If IsExtended = False Then

                'objICInstrumentColl.Query.Select(objICInstrumentColl.Query.InstrumentNumber.Count)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubSetID = objICSubSet.SubSetID)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.OfficeID = objICSubSet.OfficeID)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = objICSubSet.MasterSeriesID)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                objICInstrumentColl.Query.Load()
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    For i = 0 To TopOrder - 1
                        objICInstrument = New ICInstruments
                        objICInstrument = objICInstrumentColl(i)
                        If objICInstrument.IsUSed Is Nothing And objICInstrument.IsAssigned Is Nothing Then
                            RequiredLeavesCount = RequiredLeavesCount + 1
                        End If
                    Next
                    If RequiredLeavesCount >= TopOrder Then
                        Result = True
                    End If
                    'If CLng(dt.Rows(0)(0)) >= TopOrder Then
                    '    Result = True
                    'End If
                End If
                
            ElseIf IsExtended = True Then

                objICInstrumentColl.Query.Select(objICInstrumentColl.Query.InstrumentNumber.Count)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.IsUSed.IsNull And objICInstrumentColl.Query.IsAssigned.IsNull And objICInstrumentColl.Query.OfficeID.IsNull)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = objICSubSet.MasterSeriesID And objICInstrumentColl.Query.SubSetID.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    If CLng(dt.Rows(0)(0) >= TopOrder) Then
                        Result = True
                    End If
                End If
                
            End If
            Return Result

        End Function
        Public Shared Sub UpDateInstrumentsForUpdateOfSubSet(ByVal TopOrder As Integer, ByVal objICSubSet As ICSubSet, ByVal SubSetTo As String, ByVal UsersID As String, ByVal UsersName As String, ByVal IsExtended As Boolean)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objInstrumentForUpDate As New ICInstruments
            Dim dt As New DataTable
            Dim Result As Boolean = False
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objInstrumentForUpDate.es.Connection.CommandTimeout = 3600
            If IsExtended = False Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubSetID = objICSubSet.SubSetID And objICInstrumentColl.Query.InstrumentNumber.Between(objICSubSet.SubSetTo, SubSetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.OfficeID = objICSubSet.OfficeID And objICInstrumentColl.Query.IsUSed.IsNull And objICInstrumentColl.Query.IsAssigned.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then

                    For Each objInstrument As ICInstruments In objICInstrumentColl
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.InstrumentsID)
                        objInstrumentForUpDate.SubSetID = Nothing
                        objInstrumentForUpDate.OfficeID = Nothing
                        objInstrumentForUpDate.Save()
                        ICUtilities.AddAuditTrail("Instrument number [ " & objInstrument.InstrumentNumber & " ] of master series with ID [ " & objInstrument.MasterSeriesID & " ] of account number [ " & objInstrument.UpToICMasterSeriesByMasterSeriesID.AccountNumber & " ] unassigned from [ " & objInstrument.UpToICOfficeByOfficeID.OfficeName & " ] [ " & objInstrument.UpToICOfficeByOfficeID.OfficeCode & " ].", "Instrument", objInstrument.InstrumentsID, UsersID.ToString, UsersName.ToString, "UNASSIGN")
                    Next
                End If
            ElseIf IsExtended = True Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber.Between(SubSetTo, objICSubSet.SubSetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.IsUSed.IsNull And objICInstrumentColl.Query.MasterSeriesID = objICSubSet.MasterSeriesID)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then

                    For Each objInstrument As ICInstruments In objICInstrumentColl
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.InstrumentsID)
                        objInstrumentForUpDate.SubSetID = objICSubSet.SubSetID
                        objInstrumentForUpDate.OfficeID = objICSubSet.OfficeID
                        objInstrumentForUpDate.Save()
                        ICUtilities.AddAuditTrail("Instrument number [ " & objInstrumentForUpDate.InstrumentNumber & " ] of master series with ID [ " & objInstrument.MasterSeriesID & " ] of account number [ " & objInstrument.UpToICMasterSeriesByMasterSeriesID.AccountNumber & " ] assigned to [ " & objInstrumentForUpDate.UpToICOfficeByOfficeID.OfficeName & " ].", "Instrument", objInstrumentForUpDate.InstrumentsID, UsersID.ToString, UsersName.ToString, "ASSIGN")
                    Next
                End If
            End If
           

        End Sub
        Public Shared Function IsInstrumentIsUsedAgainstSubSetIDAndOfficeID(ByVal SubSetID As String, ByVal OfficeID As String) As Boolean
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim Result As Boolean = False

            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubSetID = SubSetID And objICInstrumentColl.Query.OfficeID = OfficeID)

            If objICInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    If Not objICInstrument.IsUSed Is Nothing Or Not objICInstrument.IsAssigned Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If
            Return Result
        End Function
        Public Shared Sub DeleteInstrumentsAgainstSubSetIDAndOfficeID(ByVal SubSetID As String, ByVal OfficeID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim objICInstrumentForDelet As New ICInstruments
            Dim Result As Boolean = False

            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrumentForDelet.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubSetID = SubSetID And objICInstrumentColl.Query.OfficeID = OfficeID)
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.IsUSed.IsNull)
            If objICInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    objICInstrumentForDelet.LoadByPrimaryKey(objICInstrument.InstrumentsID)
                    objICInstrumentForDelet.OfficeID = Nothing
                    objICInstrumentForDelet.SubSetID = Nothing
                    objICInstrumentForDelet.Save()
                    ICUtilities.AddAuditTrail("Instrument No" & objICInstrument.InstrumentNumber & " unassigned from office " & objICInstrument.UpToICOfficeByOfficeID.OfficeName & " " & objICInstrument.UpToICOfficeByOfficeID.OfficeName & " .", "Instrument", objICInstrument.InstrumentsID, UsersID.ToString, UsersName.ToString, "UNASSIGN")
                Next
            End If

        End Sub
        Public Shared Function IsInstrumentNoIsUsedByInstrumentNo(ByVal InstrumentNo As String, ByVal InstructionID As String, ByVal Printing As Boolean) As Boolean
            Dim Result As Boolean = False
            Dim dt As New DataTable
            Dim qryObjICInstrument As New ICInstrumentsQuery("qryObjICInstrument")
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")

            qryObjICInstrument.Select(qryObjICInstrument.InstrumentNumber, qryObjICInstruction.InstructionID, qryObjICInstruction.Status)
            qryObjICInstrument.InnerJoin(qryObjICInstruction).On(qryObjICInstrument.InstrumentNumber.Cast(EntitySpaces.DynamicQuery.esCastType.String) = qryObjICInstruction.InstrumentNo)
            qryObjICInstrument.Where(qryObjICInstrument.InstrumentNumber = InstrumentNo)
            If Printing = True Then
                qryObjICInstrument.Where(qryObjICInstrument.IsUSed = True)
            Else
                qryObjICInstrument.Where(qryObjICInstrument.IsAssigned = True Or qryObjICInstrument.IsUSed = True)
            End If

            qryObjICInstrument.Where(qryObjICInstruction.InstructionID <> InstructionID)
            dt = qryObjICInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Result = True
            Else
                Result = False
            End If

            Return Result
        End Function
        Public Shared Function IsInstrumentNoIsAssignedToSpecificOfficeByInstrumentNoAndOfficeCode(ByVal InstrumentNo As String, ByVal OfficeCode As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrument As New ICInstruments
            Dim objICInstrumentColl As New ICInstrumentsCollection

            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber = InstrumentNo And objICInstrumentColl.Query.OfficeID = OfficeCode)
            If objICInstrumentColl.Query.Load Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Sub MarkInstrumentNumberIsUsedByInstructionIDAndInstrumentNumberAndOfficeCode(ByVal InstrumentNumber As String, ByVal OfficeCode As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber = InstrumentNumber And objICInstrumentColl.Query.OfficeID = OfficeCode)
            objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICInstrumentColl.Query.Load Then
                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICInstrument.InstrumentNumber & " ] is assigned to instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Action was taken by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICInstrument.IsUSed = True
                    objICInstrument.UsedOn = Date.Now
                    objICInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "Instrument", objICInstrument.InstrumentNumber.ToString, UsersID, UsersName, "ASSIGN")
                Next
            End If
        End Sub
        Public Shared Function GetFirstInstruemntNoByOfficeCodeAndAccountNoOnPrintingCheck(ByVal OfficeCode As String, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As String
            Dim InstrumentNo As String = Nothing
            Dim dt As New DataTable
            Dim qryObjICInstrument As New ICInstrumentsQuery("qryObjICInstrument")
            Dim qryObjICMasterSeries As New ICMasterSeriesQuery("qryObjICMasterSeries")

            qryObjICInstrument.Select(qryObjICInstrument.InstrumentNumber)
            qryObjICInstrument.InnerJoin(qryObjICMasterSeries).On(qryObjICInstrument.MasterSeriesID = qryObjICMasterSeries.MasterSeriesID)
            qryObjICInstrument.Where(qryObjICInstrument.OfficeID = OfficeCode)
            qryObjICInstrument.Where(qryObjICMasterSeries.AccountNumber = AccountNo And qryObjICMasterSeries.BranchCode = BranchCode And qryObjICMasterSeries.Currency = Currency)
            qryObjICInstrument.Where(qryObjICInstrument.IsAssigned.IsNull And qryObjICInstrument.IsUSed.IsNull)
            qryObjICInstrument.OrderBy(qryObjICInstrument.InstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            qryObjICInstrument.es.Top = 1
            dt = qryObjICInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                InstrumentNo = CStr(dt.Rows(0)(0))
            End If
            Return InstrumentNo
        End Function
        Public Shared Sub MarkInstrumentNumberIsAssignedByInstructionIDAndInstrumentNumberAndOfficeCode(ByVal InstrumentNumber As String, ByVal OfficeCode As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber = InstrumentNumber And objICInstrumentColl.Query.OfficeID = OfficeCode)
            objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICInstrumentColl.Query.Load Then
                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICInstrument.InstrumentNumber & " ] assigned to print location [ " & OfficeCode & " ] for instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is used by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICInstrument.IsAssigned = True
                    objICInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "Instrument", objICInstrument.InstrumentNumber.ToString, UsersID, UsersName, "ASSIGN")
                Next
            End If
        End Sub
        Public Shared Sub MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(ByVal InstrumentNumber As String, ByVal OfficeCode As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber = InstrumentNumber And objICInstrumentColl.Query.OfficeID = OfficeCode)
            objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.InstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICInstrumentColl.Query.Load Then
                For Each objICInstrument As ICInstruments In objICInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICInstrument.InstrumentNumber & " ] unassigned from print location [ " & OfficeCode & " ] for instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Actionn was taken by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICInstrument.IsAssigned = Nothing
                    objICInstrument.IsUSed = Nothing
                    objICInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "Instrument", objICInstrument.InstrumentNumber.ToString, UsersID, UsersName, "UNASSIGN")
                Next
            End If
        End Sub
        Public Shared Function IsInstrumentNoExistsByInstrumentNo(ByVal InstrumentNo As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrument As New ICInstruments
            Dim objICInstrumentColl As New ICInstrumentsCollection

            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber = InstrumentNo)
            If objICInstrumentColl.Query.Load Then
                    Result = True
            End If
            Return Result
        End Function
        Public Shared Function GetTotalUnUSedInstrumentCountByPrintLocationCode(ByVal PrintLocationCode As String) As Integer
            Dim qryObjICInstrument As New ICInstrumentsQuery("qryObjICInstrument")
            Dim dt As New DataTable

            qryObjICInstrument.Select(qryObjICInstrument.InstrumentNumber.Count.As("Count"))
            qryObjICInstrument.Where(qryObjICInstrument.IsUSed.IsNull And qryObjICInstrument.OfficeID = PrintLocationCode.ToString And qryObjICInstrument.IsAssigned.IsNull)
            dt = qryObjICInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return CInt(dt.Rows(0)(0))
            Else
                Return 0
            End If
        End Function
        Public Shared Function GetRequiredUnAssignedAndUnUsedInstrumentNoByAccNoAndPrintLocation(ByVal AccountNo As String, ByVal PrintLocationCode As String, ByVal ReqInstruments As Integer) As Boolean
            Dim qryObjICInstrument As New ICInstrumentsQuery("qryObjICInstrument")
            Dim qryObjICSubSet As New ICSubSetQuery("qryObjICSubSet")
            Dim qryObjICMasterSeries As New ICMasterSeriesQuery("qryObjICMasterSeries")
            Dim dt As New DataTable

            qryObjICInstrument.Select(qryObjICInstrument.InstrumentNumber.Count)
            qryObjICInstrument.InnerJoin(qryObjICSubSet).On(qryObjICInstrument.SubSetID = qryObjICSubSet.SubSetID)
            qryObjICInstrument.InnerJoin(qryObjICMasterSeries).On(qryObjICSubSet.MasterSeriesID = qryObjICMasterSeries.MasterSeriesID)
            qryObjICInstrument.Where(qryObjICInstrument.IsUSed.IsNull And qryObjICInstrument.OfficeID = PrintLocationCode.ToString And qryObjICInstrument.IsAssigned.IsNull And qryObjICMasterSeries.AccountNumber = AccountNo)
            dt = qryObjICInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                If CInt(dt.Rows(0)(0)) >= ReqInstruments Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function
      
    End Class
End Namespace
