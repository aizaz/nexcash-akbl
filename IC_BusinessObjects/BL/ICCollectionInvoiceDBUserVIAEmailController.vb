﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionInvoiceDBUserVIAEmailController
        Public Shared Sub AddCollectionNatureEmailUsers(ByVal objCollNatNotifiUsers As ICCollectionInvoiceDBUserVIAEmail)
            Dim objICCollectionInvoiceDBUserVIAEmail As New ICCollectionInvoiceDBUserVIAEmail

            objICCollectionInvoiceDBUserVIAEmail.CollectionNatureCode = objCollNatNotifiUsers.CollectionNatureCode
            objICCollectionInvoiceDBUserVIAEmail.UserID = objCollNatNotifiUsers.UserID
            objICCollectionInvoiceDBUserVIAEmail.Save()

        End Sub

         Public Shared Sub DeleteOldCollectionNatureEmailUsers(ByVal CollectionNatureCode As String)
            Dim collUserVIAEmail As New ICCollectionInvoiceDBUserVIAEmailCollection

            collUserVIAEmail.Query.Where(collUserVIAEmail.Query.CollectionNatureCode = CollectionNatureCode)
            If collUserVIAEmail.Query.Load() Then
                collUserVIAEmail.MarkAllAsDeleted()
                collUserVIAEmail.Save()
            End If
        End Sub

        Public Shared Sub DeleteCollNatureEmailUser(ByVal CollectionNatureCode As String, ByVal UserID As String)
            Dim objCollNatureUserNotifi As New ICCollectionInvoiceDBUserVIAEmail
            If objCollNatureUserNotifi.LoadByPrimaryKey(CollectionNatureCode, UserID) Then
                objCollNatureUserNotifi.MarkAsDeleted()
                objCollNatureUserNotifi.Save()
            End If
        End Sub

    End Class
End Namespace