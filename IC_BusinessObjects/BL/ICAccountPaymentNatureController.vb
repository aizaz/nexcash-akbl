﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class ICAccountPaymentNatureController
        Public Shared Function AddAccountPaymentNature(ByVal ICAccountPaymentNature As ICAccountsPaymentNature, ByVal ActionText As String, ByVal UserID As String, ByVal UserName As String) As String
            Dim objAccountPaymentNature As New ICAccountsPaymentNature
            objAccountPaymentNature.es.Connection.CommandTimeout = 3600
            objAccountPaymentNature.CreateBy = ICAccountPaymentNature.CreateBy
            objAccountPaymentNature.CreateDate = ICAccountPaymentNature.CreateDate


            objAccountPaymentNature.Creater = ICAccountPaymentNature.Creater
            objAccountPaymentNature.CreationDate = ICAccountPaymentNature.CreationDate

            objAccountPaymentNature.AccountNumber = ICAccountPaymentNature.AccountNumber
            objAccountPaymentNature.BranchCode = ICAccountPaymentNature.BranchCode
            objAccountPaymentNature.Currency = ICAccountPaymentNature.Currency
            objAccountPaymentNature.PaymentNatureCode = ICAccountPaymentNature.PaymentNatureCode
            objAccountPaymentNature.IsActive = True
            objAccountPaymentNature.IsApproved = True
            objAccountPaymentNature.Save()

            ICUtilities.AddAuditTrail(ActionText.ToString(), "Account Payment Nature", objAccountPaymentNature.AccountNumber & "-" & objAccountPaymentNature.BranchCode & "-" & objAccountPaymentNature.Currency & "-" & objAccountPaymentNature.PaymentNatureCode, UserID.ToString(), UserName.ToString(), "ADD")

            Return objAccountPaymentNature.AccountNumber & ";" & objAccountPaymentNature.BranchCode & ";" & objAccountPaymentNature.Currency & ";" & objAccountPaymentNature.PaymentNatureCode
        End Function
        'Public Shared Function GetFileUploadTemplateByAccountPaymentNature(ByVal AccPayNature As String) As DataTable
        '    Dim qryObjICAccPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
        '    Dim qryObjICAccPNatureFileTemp As New ICAccountPaymentNatureTemplateQuery("qryObjICAccPNatureTemp")
        '    Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")

        '    Dim dt As New DataTable

        '    qryObjICAccPNature.Select(qryObjICAccPNature.AccountNumber, qryObjICAccPNature.BranchCode, qryObjICAccPNature.Currency, qryObjICAccPNature.PaymentNatureCode, qryObjICAccPNatureFileTemp.TemplateID, qryObjICTemplate.TemplateName)
        '    qryObjICAccPNature.InnerJoin(qryObjICAccPNatureFileTemp).On(qryObjICAccPNature.AccountNumber = qryObjICAccPNatureFileTemp.AccountNumber And qryObjICAccPNature.BranchCode = qryObjICAccPNatureFileTemp.BranchCode And qryObjICAccPNature.Currency = qryObjICAccPNatureFileTemp.Currency And qryObjICAccPNature.PaymentNatureCode = qryObjICAccPNatureFileTemp.PaymentNatureCode)
        '    qryObjICAccPNature.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileTemp.TemplateID = qryObjICTemplate.TemplateID)

        '    If AccPayNature.ToString <> "0" Then
        '        qryObjICAccPNature.Where(qryObjICAccPNature.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNature.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNature.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
        '        dt = qryObjICAccPNature.LoadDataTable
        '        '  Return dt

        '    End If

        '    'dt = qryObjICAccPNature.LoadDataTable
        '    Return dt

        'End Function
        Public Shared Function GetFileUploadTemplateByAccountPaymentNatureForViewEmailSettings(ByVal AccPayNature As String) As DataTable
            Dim qryObjICAccPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
            Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")

            Dim dt As New DataTable

            qryObjICAccPNature.Select(qryObjICAccPNature.AccountNumber, qryObjICAccPNature.BranchCode, qryObjICAccPNature.Currency, qryObjICAccPNature.PaymentNatureCode, qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
            qryObjICAccPNature.InnerJoin(qryObjICAccPNatureFileUploadTemp).On(qryObjICAccPNature.AccountNumber = qryObjICAccPNatureFileUploadTemp.AccountNumber And qryObjICAccPNature.BranchCode = qryObjICAccPNatureFileUploadTemp.BranchCode And qryObjICAccPNature.Currency = qryObjICAccPNatureFileUploadTemp.Currency And qryObjICAccPNature.PaymentNatureCode = qryObjICAccPNatureFileUploadTemp.PaymentNatureCode)
            qryObjICAccPNature.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

            If AccPayNature.ToString <> "0" Then
                qryObjICAccPNature.Where(qryObjICAccPNature.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNature.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNature.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
                dt = qryObjICAccPNature.LoadDataTable
            End If
            Return dt
        End Function


        'Public Shared Function GetFileUploadTemplateByAccountPaymentNatureForSaveEmailSettings(ByVal AccPayNature As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String) As DataTable

        '    Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
        '    Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")
        '    Dim qryEmail As New ICEmailSettingsQuery("qryEmail")

        '    Dim dt As New DataTable

        '    qryObjICAccPNatureFileUploadTemp.Select(qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
        '    qryObjICAccPNatureFileUploadTemp.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

        '    If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
        '        If AccPayNature.ToString() <> "0" Then
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNatureFileUploadTemp.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNatureFileUploadTemp.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNatureFileUploadTemp.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.TemplateID.NotIn(qryEmail.[Select](qryEmail.FileUploadTemplateCode.Distinct).Where(qryEmail.AccountNumber = AccPayNature.Split("-")(0).ToString And qryEmail.BranchCode = AccPayNature.Split("-")(1).ToString And qryEmail.Currency = AccPayNature.Split("-")(2).ToString And qryEmail.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)))
        '        End If
        '    End If
        '    dt = qryObjICAccPNatureFileUploadTemp.LoadDataTable

        '    Return dt
        'End Function
        'Public Shared Function GetFileUploadTemplateByAccountPaymentNatureForSaveEmailSettings(ByVal AccPayNature As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String, ByVal UserType As String) As DataTable

        '    Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
        '    Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")
        '    Dim qryEmail As New ICEmailSettingsQuery("qryEmail")
        '    Dim qryUser As New ICUserQuery("qryUser")

        '    Dim dt As New DataTable

        '    qryObjICAccPNatureFileUploadTemp.Select(qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
        '    qryObjICAccPNatureFileUploadTemp.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

        '    If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
        '        If AccPayNature.ToString() <> "0" Then
        '            'qryUser.InnerJoin(qryEmail).On(qryEmail.UserID = qryUser.UserID)
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNatureFileUploadTemp.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNatureFileUploadTemp.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNatureFileUploadTemp.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.TemplateID.NotIn(qryEmail.[Select](qryEmail.FileUploadTemplateCode).InnerJoin(qryUser).On(qryEmail.UserID = qryUser.UserID).Where(qryEmail.AccountNumber = AccPayNature.Split("-")(0).ToString And qryEmail.BranchCode = AccPayNature.Split("-")(1).ToString And qryEmail.Currency = AccPayNature.Split("-")(2).ToString And qryEmail.PaymentNatureCode = AccPayNature.Split("-")(3).ToString And qryUser.UserType = UserType.ToString())))
        '        End If
        '    End If
        '    dt = qryObjICAccPNatureFileUploadTemp.LoadDataTable

        '    Return dt
        'End Function
        'Public Shared Function GetFileUploadTemplateByAccountPaymentNatureForSaveEmailSettings(ByVal AccPayNature As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String, ByVal UserType As String) As DataTable

        '    Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
        '    Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")
        '    Dim qryEmail As New ICEmailSettingsQuery("qryEmail")
        '    Dim qryUser As New ICUserQuery("qryUser")

        '    Dim dt As New DataTable

        '    qryObjICAccPNatureFileUploadTemp.Select(qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
        '    qryObjICAccPNatureFileUploadTemp.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

        '    If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
        '        If AccPayNature.ToString() <> "0" Then
        '            qryUser.InnerJoin(qryEmail).On(qryEmail.UserID = qryUser.UserID)
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNatureFileUploadTemp.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNatureFileUploadTemp.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNatureFileUploadTemp.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
        '            qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.TemplateID.NotIn(qryEmail.[Select](qryEmail.FileUploadTemplateCode.Distinct).InnerJoin(qryUser).On(qryEmail.UserID = qryUser.UserID).Where(qryEmail.AccountNumber = AccPayNature.Split("-")(0).ToString And qryEmail.BranchCode = AccPayNature.Split("-")(1).ToString And qryEmail.Currency = AccPayNature.Split("-")(2).ToString And qryEmail.PaymentNatureCode = AccPayNature.Split("-")(3).ToString And qryUser.UserType = UserType.ToString())))
        '        End If
        '    End If
        '    dt = qryObjICAccPNatureFileUploadTemp.LoadDataTable

        '    Return dt
        'End Function
        Public Shared Function GetFileUploadTemplateByAccountPaymentNatureForSaveEmailSettings(ByVal AccPayNature As String, ByVal AccntNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNatureCode As String, ByVal TemplateID As String, ByVal UserType As String) As DataTable

            Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
            Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")
            Dim qryEmail As New ICEmailSettingsQuery("qryEmail")
            Dim qryUser As New ICUserQuery("qryUser")

            Dim dt As New DataTable

            qryObjICAccPNatureFileUploadTemp.Select(qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
            qryObjICAccPNatureFileUploadTemp.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

            If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
                If AccPayNature.ToString() <> "0" Then
                    qryUser.InnerJoin(qryEmail).On(qryEmail.UserID = qryUser.UserID)
                    qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNatureFileUploadTemp.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNatureFileUploadTemp.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNatureFileUploadTemp.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)
                    qryObjICAccPNatureFileUploadTemp.Where(qryObjICAccPNatureFileUploadTemp.TemplateID.NotIn(qryEmail.[Select](qryEmail.FileUploadTemplateCode.Distinct).InnerJoin(qryUser).On(qryEmail.UserID = qryUser.UserID).Where(qryEmail.AccountNumber = AccPayNature.Split("-")(0).ToString And qryEmail.BranchCode = AccPayNature.Split("-")(1).ToString And qryEmail.Currency = AccPayNature.Split("-")(2).ToString And qryEmail.PaymentNatureCode = AccPayNature.Split("-")(3).ToString And qryUser.UserType = UserType.ToString())))
                End If
            End If
            dt = qryObjICAccPNatureFileUploadTemp.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetFileUploadTemplateByAccountPaymentNature(ByVal AccPayNature As String) As DataTable
            Dim qryObjICAccPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICAccPNatureFileUploadTemp As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAccPNatureTemp")
            Dim qryObjICTemplate As New ICTemplateQuery("qryObjICTemplate")

            Dim dt As New DataTable

            qryObjICAccPNature.Select(qryObjICAccPNature.AccountNumber, qryObjICAccPNature.BranchCode, qryObjICAccPNature.Currency, qryObjICAccPNature.PaymentNatureCode, qryObjICAccPNatureFileUploadTemp.TemplateID, qryObjICTemplate.TemplateName)
            qryObjICAccPNature.InnerJoin(qryObjICAccPNatureFileUploadTemp).On(qryObjICAccPNature.AccountNumber = qryObjICAccPNatureFileUploadTemp.AccountNumber And qryObjICAccPNature.BranchCode = qryObjICAccPNatureFileUploadTemp.BranchCode And qryObjICAccPNature.Currency = qryObjICAccPNatureFileUploadTemp.Currency And qryObjICAccPNature.PaymentNatureCode = qryObjICAccPNatureFileUploadTemp.PaymentNatureCode)
            qryObjICAccPNature.InnerJoin(qryObjICTemplate).On(qryObjICAccPNatureFileUploadTemp.TemplateID = qryObjICTemplate.TemplateID)

            If AccPayNature.ToString <> "0" Then
                qryObjICAccPNature.Where(qryObjICAccPNature.AccountNumber = AccPayNature.Split("-")(0).ToString And qryObjICAccPNature.BranchCode = AccPayNature.Split("-")(1).ToString And qryObjICAccPNature.Currency = AccPayNature.Split("-")(2).ToString And qryObjICAccPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString)

                dt = qryObjICAccPNature.LoadDataTable

            End If

            Return dt

        End Function
        Public Shared Sub SetgvAccountPaymentNatures(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")
            Dim qryPaymentNature As New ICPaymentNatureQuery("pn")
            Dim qryAccount As New ICAccountsQuery("acc")
            Dim qryAccountPaymentNatures As New ICAccountsPaymentNatureQuery("acccpn")

            If Not pagenumber = 0 Then

            Else
                qryAccountPaymentNatures.es.PageNumber = 1
                qryAccountPaymentNatures.es.PageSize = pagesize
            End If


            qryAccountPaymentNatures.Select(qryAccountPaymentNatures.AccountNumber, qryAccountPaymentNatures.BranchCode, qryAccountPaymentNatures.Currency, qryAccountPaymentNatures.PaymentNatureCode, qryPaymentNature.PaymentNatureName, qryAccount.AccountTitle, qryGroup.GroupName, qryCompany.CompanyName)
            qryAccountPaymentNatures.InnerJoin(qryAccount).On(qryAccountPaymentNatures.AccountNumber = qryAccount.AccountNumber And qryAccountPaymentNatures.BranchCode = qryAccount.BranchCode And qryAccountPaymentNatures.Currency = qryAccount.Currency)
            qryAccountPaymentNatures.InnerJoin(qryPaymentNature).On(qryAccountPaymentNatures.PaymentNatureCode = qryPaymentNature.PaymentNatureCode)
            qryAccountPaymentNatures.InnerJoin(qryCompany).On(qryAccount.CompanyCode = qryCompany.CompanyCode)
            qryAccountPaymentNatures.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)

            If Not GroupCode.ToString() = "0" Then
                qryAccountPaymentNatures.Where(qryGroup.GroupCode = GroupCode)
            End If
            If Not CompanyCode.ToString() = "0" Then
                qryAccountPaymentNatures.Where(qryCompany.CompanyCode = CompanyCode)
            End If
            qryAccountPaymentNatures.Where(qryAccount.IsApproved = True And qryAccount.IsActive = True)
            qryAccountPaymentNatures.OrderBy(qryGroup.GroupName.Descending, qryCompany.CompanyName.Descending, qryAccountPaymentNatures.AccountNumber.Descending, qryPaymentNature.PaymentNatureName.Descending)
            rg.DataSource = qryAccountPaymentNatures.LoadDataTable()
            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub
        Public Shared Sub DeleteAccountPaymentNature(ByVal ICAccountPaymentNature As ICAccountsPaymentNature, ByVal ActionText As String, ByVal UserID As String, ByVal UserName As String)
            Dim objAccountPaymentNature As New ICAccountsPaymentNature
            objAccountPaymentNature.es.Connection.CommandTimeout = 3600

            objAccountPaymentNature.LoadByPrimaryKey(ICAccountPaymentNature.AccountNumber, ICAccountPaymentNature.BranchCode, ICAccountPaymentNature.Currency, ICAccountPaymentNature.PaymentNatureCode)
            objAccountPaymentNature.MarkAsDeleted()
            objAccountPaymentNature.Save()

            ICUtilities.AddAuditTrail(ActionText.ToString(), "Account Payment Nature", ICAccountPaymentNature.AccountNumber & ";" & ICAccountPaymentNature.BranchCode & ";" & ICAccountPaymentNature.Currency & ";" & ICAccountPaymentNature.PaymentNatureCode, UserID.ToString(), UserName.ToString(), "DELETE")
        End Sub
        Public Shared Function GetAccountPaymentNatureByCompanyCode(ByVal CompanyCode As String) As DataTable
            Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICAccountPNature.BranchCode + "-" + qryObjICAccountPNature.Currency + "-" + qryObjICAccountPNature.PaymentNatureCode).As("AccountPaymentNature"))
            qryObjICAccountPNature.InnerJoin(qryObjICAccounts).On(qryObjICAccountPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPNature.Currency = qryObjICAccounts.Currency)
            qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAccountPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            qryObjICAccountPNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)

            dt = qryObjICAccountPNature.LoadDataTable
            Return dt

        End Function
        Public Shared Function GetPaymentNatureByCompanyCode(ByVal CompanyCode As String) As DataTable
            Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICAccountPNature.Select((qryObjICAccountPNature.PaymentNatureCode + "-" + qryObjICPaymentNature.PaymentNatureName).As("PaymentNature"))
            qryObjICAccountPNature.Select(qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAccountPNature.InnerJoin(qryObjICAccounts).On(qryObjICAccountPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPNature.Currency = qryObjICAccounts.Currency)
            qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAccountPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            qryObjICAccountPNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICAccountPNature.es.Distinct = True
            dt = qryObjICAccountPNature.LoadDataTable
            Return dt

        End Function
        'Public Shared Function GetPaymentNatureByCompanyCode(ByVal CompanyCode As String) As DataTable
        '    Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
        '    Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim dt As New DataTable
        '    qryObjICAccountPNature.Select((qryObjICPaymentNature.PaymentNatureCode + "-" + qryObjICPaymentNature.PaymentNatureName).As("PaymentNature"))
        '    qryObjICAccountPNature.Select(qryObjICPaymentNature.PaymentNatureCode)
        '    qryObjICAccountPNature.InnerJoin(qryObjICAccounts).On(qryObjICAccountPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPNature.Currency = qryObjICAccounts.Currency)
        '    qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
        '    qryObjICAccountPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)


        '    qryObjICAccountPNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
        '    qryObjICAccountPNature.GroupBy(qryObjICAccountPNature.PaymentNatureCode)
        '    dt = qryObjICAccountPNature.LoadDataTable
        '    Return dt

        'End Function
        ' Aizaz Work [Dated: 01-Nov-2013]
        Public Shared Function GetAllAccountPaymentNatureWithCompanyCode() As DataTable
            Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            '  Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICAccountPNature.BranchCode + "-" + qryObjICAccountPNature.Currency + "-" + qryObjICAccountPNature.PaymentNatureCode).As("AccountPaymentNature"), qryObjICAccounts.CompanyCode)
            qryObjICAccountPNature.InnerJoin(qryObjICAccounts).On(qryObjICAccountPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPNature.Currency = qryObjICAccounts.Currency)
            qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            '  qryObjICAccountPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)


            ' qryObjICAccountPNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            qryObjICAccountPNature.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            dt = qryObjICAccountPNature.LoadDataTable
            Return dt

        End Function
        Public Shared Function GetAllAccountPaymentNature() As DataTable
            Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")

            Dim dt As New DataTable
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICAccountPNature.Select((qryObjICAccountPNature.AccountNumber + "-" + qryObjICAccountPNature.BranchCode + "-" + qryObjICAccountPNature.Currency + "-" + qryObjICAccountPNature.PaymentNatureCode).As("AccountPaymentNature"))

            qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)

            dt = qryObjICAccountPNature.LoadDataTable
            Return dt

        End Function
    End Class
End Namespace