﻿Imports Telerik.Web.UI
Imports System
Imports System.Text.RegularExpressions

Namespace IC
    Public Class ICAccountPaymentNatureFileUploadTemplateFieldsController
        Public Shared Sub AddAPNFUploadTemplateFields(ByVal objICAPNFUTFields As ICAccountPaymentNatureFileUploadTemplatefields, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)


            Dim objICAPNFUTFieldsForSave As New ICAccountPaymentNatureFileUploadTemplatefields
            objICAPNFUTFieldsForSave.es.Connection.CommandTimeout = 360


            objICAPNFUTFieldsForSave.AccountNumber = objICAPNFUTFields.AccountNumber
            objICAPNFUTFieldsForSave.BranchCode = objICAPNFUTFields.BranchCode
            objICAPNFUTFieldsForSave.Currency = objICAPNFUTFields.Currency
            objICAPNFUTFieldsForSave.PaymentNatureCode = objICAPNFUTFields.PaymentNatureCode
            objICAPNFUTFieldsForSave.TemplateID = objICAPNFUTFields.TemplateID
            objICAPNFUTFieldsForSave.FieldID = objICAPNFUTFields.FieldID
            objICAPNFUTFieldsForSave.IsRequired = objICAPNFUTFields.IsRequired
            objICAPNFUTFieldsForSave.MustRequired = objICAPNFUTFields.MustRequired
            objICAPNFUTFieldsForSave.FieldOrder = objICAPNFUTFields.FieldOrder
            objICAPNFUTFieldsForSave.FieldType = objICAPNFUTFields.FieldType
            objICAPNFUTFieldsForSave.FixLength = objICAPNFUTFields.FixLength
            objICAPNFUTFieldsForSave.FlexiFieldID = objICAPNFUTFields.FlexiFieldID



            objICAPNFUTFieldsForSave.IsRequiredIBFT = objICAPNFUTFields.IsRequiredIBFT
            objICAPNFUTFieldsForSave.IsRequiredFT = objICAPNFUTFields.IsRequiredFT
            objICAPNFUTFieldsForSave.IsRequiredPO = objICAPNFUTFields.IsRequiredPO
            objICAPNFUTFieldsForSave.IsRequiredCQ = objICAPNFUTFields.IsRequiredCQ
            objICAPNFUTFieldsForSave.IsRequiredDD = objICAPNFUTFields.IsRequiredDD
            objICAPNFUTFieldsForSave.IsRequiredCOTC = objICAPNFUTFields.IsRequiredCOTC
            objICAPNFUTFieldsForSave.IsMustRequiredCOTC = objICAPNFUTFields.IsMustRequiredCOTC
            objICAPNFUTFieldsForSave.IsMustRequiredIBFT = objICAPNFUTFields.IsMustRequiredIBFT
            objICAPNFUTFieldsForSave.IsMustRequiredFT = objICAPNFUTFields.IsMustRequiredFT
            objICAPNFUTFieldsForSave.IsMustRequiredPO = objICAPNFUTFields.IsMustRequiredPO
            objICAPNFUTFieldsForSave.IsMustRequiredCQ = objICAPNFUTFields.IsMustRequiredCQ
            objICAPNFUTFieldsForSave.IsMustRequiredDD = objICAPNFUTFields.IsMustRequiredDD
            objICAPNFUTFieldsForSave.CreatedBy = objICAPNFUTFields.CreatedBy
            objICAPNFUTFieldsForSave.CreatedDate = objICAPNFUTFields.CreatedDate
            objICAPNFUTFieldsForSave.FieldName = objICAPNFUTFields.FieldName
            objICAPNFUTFieldsForSave.IsMustRequiredBillPayment = objICAPNFUTFields.IsMustRequiredBillPayment
            objICAPNFUTFieldsForSave.IsRequiredBillPayment = objICAPNFUTFields.IsRequiredBillPayment
            objICAPNFUTFieldsForSave.Save()

            'If IsUpdate = False Then
            '    ICUtilities.AddAuditTrail("Account payment nature file upload template field [ " & objICAPNFUTFieldsForSave.FieldName & " ] of type [ " & objICAPNFUTFieldsForSave.FieldType & " ] added for account [ " & objICAPNFUTFieldsForSave.AccountNumber & " ] , payment nature [ " & objICAPNFUTFieldsForSave.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] , file upload template [ " & objICAPNFUTFieldsForSave.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICTemplateByTemplateID.TemplateName & " ] added.", "Account Payment Nature File Upload Template Field", objICAPNFUTFieldsForSave.APNFUTemplateID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            'Else
            '    ICUtilities.AddAuditTrail("Account payment nature file upload template field [ " & objICAPNFUTFieldsForSave.FieldName & " ] of type [ " & objICAPNFUTFieldsForSave.FieldType & " ] added for account [ " & objICAPNFUTFieldsForSave.AccountNumber & " ] , payment nature [ " & objICAPNFUTFieldsForSave.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] , file upload template [ " & objICAPNFUTFieldsForSave.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICTemplateByTemplateID.TemplateName & " ] updated.", "Account Payment Nature File Upload Template Field", objICAPNFUTFieldsForSave.APNFUTemplateID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            'End If
        End Sub
        Public Shared Sub DeleteAPNFileUploadTemplateFields(ByVal objICAPNFTemplate As ICAccountPayNatureFileUploadTemplate, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNFUTemplateFieldsColl As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            Dim objICAPNFUTemplateField As ICAccountPaymentNatureFileUploadTemplatefields
            Dim ActionString As String = Nothing
            Dim FieldName As String = Nothing
            Dim FieldName2 As String = Nothing
            ActionString += "Account payment nature file upload template with account number [ " & objICAPNFTemplate.AccountNumber & " ], payment nature [ " & objICAPNFTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] "
            ActionString += ", file upload template [ " & objICAPNFTemplate.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFTemplate.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] fields [ "

            objICAPNFUTemplateFieldsColl.es.Connection.CommandTimeout = 3600


            objICAPNFUTemplateFieldsColl.Query.Where(objICAPNFUTemplateFieldsColl.Query.AccountNumber = objICAPNFTemplate.AccountNumber And objICAPNFUTemplateFieldsColl.Query.BranchCode = objICAPNFTemplate.BranchCode And objICAPNFUTemplateFieldsColl.Query.Currency = objICAPNFTemplate.Currency And objICAPNFUTemplateFieldsColl.Query.TemplateID = objICAPNFTemplate.TemplateID And objICAPNFUTemplateFieldsColl.Query.PaymentNatureCode = objICAPNFTemplate.PaymentNatureCode)

            If objICAPNFUTemplateFieldsColl.Query.Load() Then
                For Each objICAPNFUTemplateForDelete As ICAccountPaymentNatureFileUploadTemplatefields In objICAPNFUTemplateFieldsColl
                    objICAPNFUTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateField.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateField.LoadByPrimaryKey(objICAPNFUTemplateForDelete.APNFUTemplateID)
                    FieldName += objICAPNFUTemplateField.FieldName & " ; "
                    objICAPNFUTemplateField.MarkAsDeleted()
                    objICAPNFUTemplateField.Save()
                Next
                FieldName2 = FieldName.Remove(FieldName.Length - 2)
                ActionString += FieldName2
                ICUtilities.AddAuditTrail(ActionString & " ] deleted.", "Account Payment Nature File Upload Template Field", objICAPNFTemplate.TemplateID.ToString + "" + objICAPNFTemplate.AccountNumber.ToString + "" + objICAPNFTemplate.BranchCode.ToString + "" + objICAPNFTemplate.Currency.ToString + "" + objICAPNFTemplate.PaymentNatureCode.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub
        'Public Shared Sub DeleteAPNFileUploadTemplateFields(ByVal objICAPNFTemplate As ICAccountPayNatureFileUploadTemplate, ByVal UsersID As String, ByVal UsersName As String)
        '    Dim objICAPNFUTemplateFieldsColl As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
        '    Dim objICAPNFUTemplateField As ICAccountPaymentNatureFileUploadTemplatefields

        '    objICAPNFUTemplateFieldsColl.es.Connection.CommandTimeout = 3600


        '    objICAPNFUTemplateFieldsColl.Query.Where(objICAPNFUTemplateFieldsColl.Query.AccountNumber = objICAPNFTemplate.AccountNumber And objICAPNFUTemplateFieldsColl.Query.BranchCode = objICAPNFTemplate.BranchCode And objICAPNFUTemplateFieldsColl.Query.Currency = objICAPNFTemplate.Currency And objICAPNFUTemplateFieldsColl.Query.TemplateID = objICAPNFTemplate.TemplateID And objICAPNFUTemplateFieldsColl.Query.PaymentNatureCode = objICAPNFTemplate.PaymentNatureCode)

        '    If objICAPNFUTemplateFieldsColl.Query.Load() Then
        '        For Each objICAPNFUTemplateForDelete As ICAccountPaymentNatureFileUploadTemplatefields In objICAPNFUTemplateFieldsColl
        '            objICAPNFUTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
        '            objICAPNFUTemplateField.es.Connection.CommandTimeout = 3600
        '            objICAPNFUTemplateField.LoadByPrimaryKey(objICAPNFUTemplateForDelete.APNFUTemplateID)
        '            objICAPNFUTemplateField.MarkAsDeleted()
        '            objICAPNFUTemplateField.Save()
        '            ICUtilities.AddAuditTrail("Account payment nature file upload template field [ " & objICAPNFUTemplateForDelete.FieldName & " ] of type [ " & objICAPNFUTemplateForDelete.FieldType & " ] deleted for account number [ " & objICAPNFUTemplateForDelete.AccountNumber & " ], payment nature [ " & objICAPNFUTemplateForDelete.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] file upload template : [ " & objICAPNFUTemplateForDelete.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplateForDelete.UpToICAccountPayNatureFileUploadTemplateByTemplateID.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] deleted.", "Account Payment Nature File Upload Template Field", objICAPNFUTemplateForDelete.APNFUTemplateID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
        '        Next
        '    End If
        'End Sub
        Public Shared Sub GetAllAssignedFieldsToAPNFTemplatForRadGrid(ByVal PageNumber As String, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate, ByVal FieldType As String)
            Dim qryObjICAPNFUTemplate As New ICAccountPaymentNatureFileUploadTemplatefieldsQuery("qryObjICAPNFUTemplate")
            Dim qryObjICAPNFUTemplateMax As New ICAccountPaymentNatureFileUploadTemplatefieldsQuery("qryObjICAPNFUTemplateMax")
            Dim qryObjICAPNFUTemplateMin As New ICAccountPaymentNatureFileUploadTemplatefieldsQuery("qryObjICAPNFUTemplateMin")
            Dim dt As New DataTable



            qryObjICAPNFUTemplate.Select(qryObjICAPNFUTemplate.FieldName, qryObjICAPNFUTemplate.FieldType, qryObjICAPNFUTemplate.FieldOrder, qryObjICAPNFUTemplate.APNFUTemplateID)
            qryObjICAPNFUTemplate.Select(qryObjICAPNFUTemplate.IsRequiredCQ, qryObjICAPNFUTemplate.IsRequiredPO, qryObjICAPNFUTemplate.IsRequiredDD, qryObjICAPNFUTemplate.FixLength)
            qryObjICAPNFUTemplate.Select(qryObjICAPNFUTemplate.IsRequiredIBFT, qryObjICAPNFUTemplate.IsRequiredFT, qryObjICAPNFUTemplate.TemplateID)
            qryObjICAPNFUTemplate.Select((qryObjICAPNFUTemplateMax.[Select](qryObjICAPNFUTemplateMax.FieldOrder.Max.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICAPNFUTemplateMax.TemplateID = qryObjICAPNFUTemplate.TemplateID)).As("MaxOrder"))
            qryObjICAPNFUTemplate.Select((qryObjICAPNFUTemplateMin.[Select](qryObjICAPNFUTemplateMin.FieldOrder.Min.Cast(EntitySpaces.DynamicQuery.esCastType.Int32)).Where(qryObjICAPNFUTemplateMin.TemplateID = qryObjICAPNFUTemplate.TemplateID)).As("MinOrder"))
            qryObjICAPNFUTemplate.GroupBy(qryObjICAPNFUTemplate.FieldName, qryObjICAPNFUTemplate.FieldType, qryObjICAPNFUTemplate.FieldOrder, qryObjICAPNFUTemplate.APNFUTemplateID)
            qryObjICAPNFUTemplate.GroupBy(qryObjICAPNFUTemplate.IsRequiredCQ, qryObjICAPNFUTemplate.IsRequiredPO, qryObjICAPNFUTemplate.IsRequiredDD, qryObjICAPNFUTemplate.FixLength)
            qryObjICAPNFUTemplate.GroupBy(qryObjICAPNFUTemplate.IsRequiredIBFT, qryObjICAPNFUTemplate.IsRequiredFT, qryObjICAPNFUTemplate.TemplateID)
            qryObjICAPNFUTemplate.Where(qryObjICAPNFUTemplate.AccountNumber = objICAPNFUTemplate.AccountNumber And qryObjICAPNFUTemplate.BranchCode = objICAPNFUTemplate.BranchCode And qryObjICAPNFUTemplate.Currency = objICAPNFUTemplate.Currency And qryObjICAPNFUTemplate.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode And qryObjICAPNFUTemplate.TemplateID = objICAPNFUTemplate.TemplateID)
            If FieldType = "NonFlexi" Then
                qryObjICAPNFUTemplate.Where(qryObjICAPNFUTemplate.FieldType = FieldType Or qryObjICAPNFUTemplate.FieldType = "FlexiField")
            Else
                qryObjICAPNFUTemplate.Where(qryObjICAPNFUTemplate.FieldType = FieldType)
            End If
            qryObjICAPNFUTemplate.OrderBy(qryObjICAPNFUTemplate.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)

            dt = qryObjICAPNFUTemplate.LoadDataTable
            If Not PageNumber = 0 Then
                qryObjICAPNFUTemplate.es.PageNumber = PageNumber
                qryObjICAPNFUTemplate.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAPNFUTemplate.es.PageNumber = 1
                qryObjICAPNFUTemplate.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub UpdateFieldOrderByFieldID(ByVal FieldOrderFrom As String, ByVal FieldOrderTo As String, ByVal APNFUTemplateID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNFUTemplate As New ICAccountPaymentNatureFileUploadTemplatefields
            objICAPNFUTemplate.es.Connection.CommandTimeout = 3600
            If objICAPNFUTemplate.LoadByPrimaryKey(APNFUTemplateID) Then
                objICAPNFUTemplate.FieldOrder = FieldOrderTo
                objICAPNFUTemplate.Save()
                ICUtilities.AddAuditTrail("Account payment nature file upload template's field [ " & objICAPNFUTemplate.FieldName & " ]  order has been changed from [ " & FieldOrderFrom & " ] to [ " & FieldOrderTo & " ] .", "Account Payment Nature File Upload Template Field", objICAPNFUTemplate.APNFUTemplateID.ToString, UsersID, UsersName, "UPDATE")
            End If
        End Sub
        ' Aizaz Ahmed Code [Dated: 12-Feb-2013]
        Public Shared Function GetTemplateFieldsByAccountPaymentNatureTemplate(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal TemplateID As String) As ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            Dim collICAPNFUTemplate As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            collICAPNFUTemplate.es.Connection.CommandTimeout = 3600

            collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.AccountNumber = AccountNumber.ToString() And collICAPNFUTemplate.Query.BranchCode = BranchCode.ToString() And collICAPNFUTemplate.Query.Currency = Currency.ToString() And collICAPNFUTemplate.Query.PaymentNatureCode = PaymentNatureCode.ToString() And collICAPNFUTemplate.Query.TemplateID = TemplateID.ToString())
            collICAPNFUTemplate.Query.OrderBy(collICAPNFUTemplate.Query.FieldOrder.Ascending)
            collICAPNFUTemplate.Query.Load()

            Return collICAPNFUTemplate
        End Function
        Public Shared Function CheckFieldDBLength(ByVal FieldID As String, ByVal FieldContent As String, ByVal FieldName As String, ByVal FieldLength As Integer) As Boolean
            Dim objICFieldsList As New ICFieldsList
            Dim FieldContentLength As Integer = 0
            objICFieldsList.es.Connection.CommandTimeout = 3600
            FieldContentLength = FieldContent.Length
            If FieldName = "Amount" Then
                Dim Amount As String = ""
                Amount = FieldContent
                If Amount.Contains(".") Then
                    Dim str As String()
                    str = Amount.Split(".")
                    Amount = ""
                    Amount = str(0) & str(1)
                    If Amount.Length > FieldLength - 1 Then
                        Return False
                    Else
                        Return True
                    End If
                    If str(0).Length > 11 Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    If FieldContentLength > FieldLength - 3 Then
                        Return False
                    Else
                        Return True
                    End If
                End If

            Else
                If FieldContentLength > FieldLength Then
                    Return False
                Else
                    Return True
                End If
            End If

        End Function
        'Public Shared Function CheckFieldDBLength(ByVal FieldID As String, ByVal FieldContent As String) As Boolean
        '    Dim objICFieldsList As New ICFieldsList
        '    Dim FieldContentLength As Integer = 0
        '    objICFieldsList.es.Connection.CommandTimeout = 3600
        '    FieldContentLength = FieldContent.Length
        '    If objICFieldsList.LoadByPrimaryKey(FieldID) Then
        '        If objICFieldsList.FieldName = "Amount" Then
        '            Dim Amount As String = ""
        '            Amount = FieldContent
        '            If Amount.Contains(".") Then
        '                Dim str As String()
        '                str = Amount.Split(".")
        '                Amount = ""
        '                Amount = str(0) & str(1)
        '            End If
        '            If Amount.Length > objICFieldsList.FieldDBLength Then
        '                Return False
        '            Else
        '                Return True
        '            End If
        '        Else
        '            If FieldContentLength > objICFieldsList.FieldDBLength Then
        '                Return False
        '            Else
        '                Return True
        '            End If
        '        End If
        '    Else
        '        Return False
        '    End If
        'End Function
        Public Shared Function CheckFieldDBType(ByVal FieldID As String, ByVal FieldContent As String, ByVal FieldName As String, ByVal FieldType As String, ByVal FieldLength As Integer) As String
            Try
                Dim objICFieldsList As New ICFieldsList

                Dim Result As Integer
                Dim ResultDouble As Double
                Dim CheckResult As String = "OK"
                objICFieldsList.es.Connection.CommandTimeout = 3600


                If FieldName <> "Amount" Then
                    If FieldContent <> "" Then
                        If FieldType = "Integer" Then
                            If FieldContent.Length > FieldLength Then
                                CheckResult = Nothing
                                CheckResult = "Invalid field length"
                                Return CheckResult
                                Exit Function
                            End If
                            If IsNumeric(FieldContent) = False Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function

                            End If
                            If Integer.TryParse(FieldContent, Result) = False Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function
                            End If
                            If CDbl(FieldContent) <= 0 Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function
                            End If

                        ElseIf objICFieldsList.FieldDBType = "Numeric" Then
                            If FieldContent.Length > FieldLength Then
                                CheckResult = Nothing
                                CheckResult = " Invalid field length"
                                Return CheckResult
                                Exit Function
                            End If
                            If IsNumeric(FieldContent) = False Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function

                            End If
                            If Double.TryParse(FieldContent, ResultDouble) = False Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function
                            End If
                            If CDbl(FieldContent) <= 0 Then
                                CheckResult = Nothing
                                CheckResult = " is not in correct format."
                                Return CheckResult
                                Exit Function
                            End If

                        Else

                            Dim reg As Regex = New Regex("<[^>]+>")
                            Dim check As Boolean = reg.IsMatch(FieldContent)

                            If check Then
                                CheckResult = Nothing
                                CheckResult = " is containing potentially dangerous value"
                                'Throw New System.Web.HttpRequestValidationException("potentially dangerous value detected in string")
                                Return CheckResult
                                Exit Function
                            End If

                        End If
                    End If
                Else
                    Return CheckResult
                End If
                Return CheckResult
            Catch ex As Exception
                Return " is not in correct format."
            End Try
        End Function

        'Public Shared Function CheckFieldDBType(ByVal FieldID As String, ByVal FieldContent As String) As String
        '    Try
        '        Dim objICFieldsList As New ICFieldsList

        '        Dim Result As Integer
        '        Dim CheckResult As String = "OK"
        '        objICFieldsList.es.Connection.CommandTimeout = 3600

        '        If objICFieldsList.LoadByPrimaryKey(FieldID) Then
        '            If Not FieldContent Is Nothing Then
        '                If FieldContent <> "" Then
        '                    If Not objICFieldsList.FieldDBType Is Nothing Then
        '                        If objICFieldsList.FieldDBType <> "" Then
        '                            If objICFieldsList.FieldDBType = "Integer" Then
        '                                If FieldContent.Length > objICFieldsList.FieldDBLength Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = "Invalid field length"
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If
        '                                If IsNumeric(FieldContent) = False Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function

        '                                End If
        '                                If Integer.TryParse(FieldContent, Result) = False Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If
        '                                If CDbl(FieldContent) <= 0 Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If

        '                            ElseIf objICFieldsList.FieldDBType = "Numeric" Then
        '                                If FieldContent.Length > objICFieldsList.FieldDBLength Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " Invalid field length"
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If
        '                                If IsNumeric(FieldContent) = False Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function

        '                                End If
        '                                If Double.TryParse(FieldContent, Result) = False Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If
        '                                If CDbl(FieldContent) <= 0 Then
        '                                    CheckResult = Nothing
        '                                    CheckResult = " is not in correct format."
        '                                    Return CheckResult
        '                                    Exit Function
        '                                End If

        '                            End If
        '                        End If
        '                    End If

        '                End If

        '            End If
        '        Else
        '            Return CheckResult
        '        End If
        '        Return CheckResult
        '    Catch ex As Exception
        '        Return " is not in correct format."
        '    End Try
        'End Function
        ' Dual File Work By Aizaz Ahmed [Dated: 07-Mar-2013]
        Public Shared Function GetTemplateFieldsByAccountPaymentNatureTemplateOfDualFile(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal TemplateID As String, ByVal FieldType As String) As ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            Dim collICAPNFUTemplate As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            collICAPNFUTemplate.es.Connection.CommandTimeout = 3600

            collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.AccountNumber = AccountNumber.ToString() And collICAPNFUTemplate.Query.BranchCode = BranchCode.ToString() And collICAPNFUTemplate.Query.Currency = Currency.ToString() And collICAPNFUTemplate.Query.PaymentNatureCode = PaymentNatureCode.ToString() And collICAPNFUTemplate.Query.TemplateID = TemplateID.ToString())
            If FieldType.ToString() = "Header" Then
                collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType = "NonFlexi" Or collICAPNFUTemplate.Query.FieldType = "DetailField")
            ElseIf FieldType.ToString() = "Detail" Then
                collICAPNFUTemplate.Query.Where(collICAPNFUTemplate.Query.FieldType <> "NonFlexi" And collICAPNFUTemplate.Query.FieldType <> "DetailField")
            End If
            collICAPNFUTemplate.Query.OrderBy(collICAPNFUTemplate.Query.FieldOrder.Ascending)
            collICAPNFUTemplate.Query.Load()

            Return collICAPNFUTemplate
        End Function
    End Class
End Namespace
