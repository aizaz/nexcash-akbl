﻿Namespace IC
    Public Class ICFlexiFieldDataController
        Public Shared Sub AddFlexiFieldData(ByVal cFlexiFieldData As ICFlexiFieldData)
            Dim objICFlexiFieldData As New ICFlexiFieldData
            objICFlexiFieldData.es.Connection.CommandTimeout = 3600
            objICFlexiFieldData.FieldID = cFlexiFieldData.FieldID
            objICFlexiFieldData.InstructionID = cFlexiFieldData.InstructionID
            objICFlexiFieldData.FieldValue = cFlexiFieldData.FieldValue
            objICFlexiFieldData.Save()
        End Sub
        Public Shared Function GetFlexiFieldDataByInstructionID(ByVal InstructionID As String) As DataTable
            Dim qryFlexiField As New ICFlexiFieldsQuery("qryff")
            Dim qryFlexiFieldData As New ICFlexiFieldDataQuery("qryffdata")

            qryFlexiFieldData.Select(qryFlexiField.FieldTitle, qryFlexiFieldData.FieldValue)
            qryFlexiFieldData.InnerJoin(qryFlexiField).On(qryFlexiFieldData.FieldID = qryFlexiField.FieldID)
            qryFlexiFieldData.Where(qryFlexiFieldData.InstructionID = InstructionID)
            qryFlexiFieldData.OrderBy(qryFlexiField.FieldTitle.Ascending)
            Return qryFlexiFieldData.LoadDataTable()

        End Function
    End Class

End Namespace
