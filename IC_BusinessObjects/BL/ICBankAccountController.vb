﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICBankAccountController
        Public Shared Sub AddBankAccount(ByVal BankAcc As ICBankAccounts, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim objICBankAccounts As New ICBankAccounts
            Dim prevobjICBankAccounts As New ICBankAccounts
            Dim objBank As New ICBank
            Dim IsActiveText As String
            Dim IsApprovedText As String
            Dim IsStaleText As String

            Dim CurrentAt As String
            Dim PrevAt As String

            objICBankAccounts.es.Connection.CommandTimeout = 3600
            objBank.es.Connection.CommandTimeout = 3600


            If (isUpdate = False) Then
                objICBankAccounts.CreatedBy = BankAcc.CreatedBy
                objICBankAccounts.CreatedDate = BankAcc.CreatedDate
                objICBankAccounts.Creater = BankAcc.Creater
                objICBankAccounts.CreationDate = BankAcc.CreationDate
            Else
                objICBankAccounts.LoadByPrimaryKey(BankAcc.PrincipalBankAccountID)
                prevobjICBankAccounts.LoadByPrimaryKey(BankAcc.PrincipalBankAccountID)
                objICBankAccounts.CreatedBy = BankAcc.CreatedBy
                objICBankAccounts.CreatedDate = BankAcc.CreatedDate

            End If
            objICBankAccounts.AccountName = BankAcc.AccountName
            objICBankAccounts.AccountType = BankAcc.AccountType
            objICBankAccounts.AccountTitle = BankAcc.AccountTitle
            objICBankAccounts.AccountNumber = BankAcc.AccountNumber
            objICBankAccounts.BranchCode = BankAcc.BranchCode
            objICBankAccounts.Currency = BankAcc.Currency
            objICBankAccounts.PrincipalBankCode = BankAcc.PrincipalBankCode
            objICBankAccounts.CBAccountType = BankAcc.CBAccountType
            objICBankAccounts.IsStaleAccount = BankAcc.IsStaleAccount
            objICBankAccounts.IsActive = BankAcc.IsActive

            objICBankAccounts.ClientNo = BankAcc.ClientNo
            objICBankAccounts.SeqNo = BankAcc.SeqNo
            objICBankAccounts.ProfitCentre = BankAcc.ProfitCentre

            objICBankAccounts.IsApproved = False

            If objICBankAccounts.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If objICBankAccounts.IsStaleAccount = True Then
                IsStaleText = True
            Else
                IsStaleText = False
            End If

            If objICBankAccounts.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            objICBankAccounts.Save()

            If (isUpdate = False) Then

                If objICBankAccounts.CBAccountType = "GL" Then
                    CurrentAt = "Bank Account : [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & objICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & objICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & objICBankAccounts.IsApproved.ToString() & " ; Client No: " & objICBankAccounts.ClientNo & " ; Seq No: " & objICBankAccounts.SeqNo & " ; Profit Centre: " & objICBankAccounts.ProfitCentre & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "BankAccount", objICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                Else
                    CurrentAt = "Bank Account : [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & objICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & objICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & objICBankAccounts.IsApproved.ToString() & "]"
                    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "BankAccount", objICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
                End If

            Else
                If objICBankAccounts.CBAccountType = "GL" Then
                    CurrentAt = "Bank Account: Current Values [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & IsStaleText.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & " ; Client No: " & objICBankAccounts.ClientNo & " ; Seq No: " & objICBankAccounts.SeqNo & " ; Profit Centre: " & objICBankAccounts.ProfitCentre & " ]"
                    PrevAt = "<br />Bank Account : Previous Values [AccountNumber:  " & prevobjICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & prevobjICBankAccounts.BranchCode.ToString() & " ; Currency: " & prevobjICBankAccounts.Currency.ToString() & " ; AccountType:  " & prevobjICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & prevobjICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & prevobjICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & prevobjICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & prevobjICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & prevobjICBankAccounts.IsApproved.ToString() & " ; Client No: " & objICBankAccounts.ClientNo & " ; Seq No: " & objICBankAccounts.SeqNo & " ; Profit Centre: " & objICBankAccounts.ProfitCentre & " ]"
                    ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "BankAccount", objICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "UPDATED")


                Else
                    CurrentAt = "Bank Account: Current Values [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & IsStaleText.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & " ]"
                    PrevAt = "<br />Bank Account : Previous Values [AccountNumber:  " & prevobjICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & prevobjICBankAccounts.BranchCode.ToString() & " ; Currency: " & prevobjICBankAccounts.Currency.ToString() & " ; AccountType:  " & prevobjICBankAccounts.AccountType.ToString() & " ; CoreBankingAccountType:  " & objICBankAccounts.CBAccountType & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & prevobjICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & prevobjICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & prevobjICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & prevobjICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & prevobjICBankAccounts.IsApproved.ToString() & " ]"
                    ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "BankAccount", objICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "UPDATED")


                End If

            End If

        End Sub
        Public Shared Function ValidateRBAccountNoByAccountType(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal CBAccountType As String, ByVal AccountType As String) As Boolean
            Dim result As Boolean = False
            Dim collBankAcc As New ICBankAccountsCollection

            collBankAcc.es.Connection.CommandTimeout = 3600

            If AccNo <> "" And BranchCode <> "" And Currency <> "" Then
                collBankAcc.Query.Where(collBankAcc.Query.CBAccountType = CBAccountType)
                collBankAcc.Query.Where(collBankAcc.Query.AccountType = AccountType)
                collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.IsActive = True And collBankAcc.Query.IsApproved = True)
                If collBankAcc.Query.Load() Then
                    result = True
                End If
            Else
                result = False
            End If

            Return result
        End Function
        ''Jawwad 22042014
        Public Shared Function ValidateGLAccountNoByAccountType(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal CBAccountType As String, ByVal AccountType As String) As Boolean
            Dim result As Boolean = False
            Dim collBankAcc As New ICBankAccountsCollection

            collBankAcc.es.Connection.CommandTimeout = 3600

            'If AccNo <> "" And BranchCode <> "" And Currency <> "" And ClientNo <> "" And SeqNo <> "" And ProfotCentr <> "" Then
            If AccNo <> "" And BranchCode <> "" And Currency <> "" Then
                collBankAcc.Query.Where(collBankAcc.Query.CBAccountType = CBAccountType)
                collBankAcc.Query.Where(collBankAcc.Query.AccountType = AccountType)
                collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
                'collBankAcc.Query.Where(collBankAcc.Query.ProfitCentre = ProfotCentr.ToString())
                'collBankAcc.Query.Where(collBankAcc.Query.ClientNo = ClientNo.ToString())
                'collBankAcc.Query.Where(collBankAcc.Query.SeqNo = SeqNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.IsActive = True And collBankAcc.Query.IsApproved = True)
                If collBankAcc.Query.Load() Then
                    result = True
                End If
            Else
                result = False
            End If

            Return result
        End Function

        'Public Shared Function ValidateGLAccountNoByAccountType(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal ClientNo As String, ByVal SeqNo As String, ByVal ProfotCentr As String, ByVal CBAccountType As String, ByVal AccountType As String) As Boolean
        '    Dim result As Boolean = False
        '    Dim collBankAcc As New ICBankAccountsCollection

        '    collBankAcc.es.Connection.CommandTimeout = 3600

        '    If AccNo <> "" And BranchCode <> "" And Currency <> "" And ClientNo <> "" And SeqNo <> "" And ProfotCentr <> "" Then
        '        collBankAcc.Query.Where(collBankAcc.Query.CBAccountType = CBAccountType)
        '        collBankAcc.Query.Where(collBankAcc.Query.AccountType = AccountType)
        '        collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.ProfitCentre = ProfotCentr.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.ClientNo = ClientNo.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.SeqNo = SeqNo.ToString())
        '        collBankAcc.Query.Where(collBankAcc.Query.IsActive = True And collBankAcc.Query.IsApproved = True)
        '        If collBankAcc.Query.Load() Then
        '            result = True
        '        End If
        '    Else
        '        result = False
        '    End If

        '    Return result
        'End Function


        Public Shared Sub ApproveBankAccounts(ByVal PrincipalBankAccountID As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objICBankAccounts As New ICBankAccounts
            Dim CurrentAt As String

            objICBankAccounts.es.Connection.CommandTimeout = 3600
            objICBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID)
            objICBankAccounts.IsApproved = IsApproved
            objICBankAccounts.ApprovedBy = UserID
            objICBankAccounts.ApprovedOn = Date.Now
            objICBankAccounts.Save()

            CurrentAt = "Bank Account : [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & objICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & objICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & objICBankAccounts.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "BankAccount", objICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

        End Sub

        Public Shared Sub DeleteBankAccounts(ByVal PrincipalBankAccountID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICBankAccounts As New ICBankAccounts
            Dim prevobjICBankAccounts As New ICBankAccounts
            Dim PrevAt As String

            objICBankAccounts.es.Connection.CommandTimeout = 3600
            prevobjICBankAccounts.es.Connection.CommandTimeout = 3600
            objICBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID)
            prevobjICBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID)
            PrevAt = "Bank Account : Previous Values [AccountNumber:  " & objICBankAccounts.AccountNumber.ToString() & " ; BranchCode:  " & objICBankAccounts.BranchCode.ToString() & " ; Currency: " & objICBankAccounts.Currency.ToString() & " ; AccountType:  " & objICBankAccounts.AccountType.ToString() & " ; AccountName:  " & objICBankAccounts.AccountName.ToString() & " ; AccountTitle:  " & objICBankAccounts.AccountTitle.ToString() & " ; PrincipalBankCode: " & objICBankAccounts.PrincipalBankCode.ToString() & " ; IsStale:  " & objICBankAccounts.IsStaleAccount.ToString() & " ; IsActive:  " & objICBankAccounts.IsActive.ToString() & " ; IsApproved:  " & objICBankAccounts.IsApproved.ToString() & "]"
            objICBankAccounts.MarkAsDeleted()
            objICBankAccounts.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "BankAccount", prevobjICBankAccounts.AccountNumber.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub GetBankAccountsgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collBankAccount As New ICBankAccountsCollection

            If Not pagenumber = 0 Then
                collBankAccount.Query.Select(collBankAccount.Query.AccountName, collBankAccount.Query.AccountNumber, collBankAccount.Query.AccountTitle, collBankAccount.Query.IsActive, collBankAccount.Query.IsApproved, collBankAccount.Query.BranchCode, collBankAccount.Query.Currency, collBankAccount.Query.AccountType, collBankAccount.Query.PrincipalBankAccountID)
                collBankAccount.Query.OrderBy(collBankAccount.Query.AccountNumber.Descending, collBankAccount.Query.BranchCode.Descending, collBankAccount.Query.Currency.Descending)
                collBankAccount.Query.Load()
                rg.DataSource = collBankAccount

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collBankAccount.Query.es.PageNumber = 1
                collBankAccount.Query.es.PageSize = pagesize
                collBankAccount.Query.OrderBy(collBankAccount.Query.AccountName.Ascending)
                collBankAccount.Query.Load()
                rg.DataSource = collBankAccount

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub

        Public Shared Function GetPrincipleBankCodeandIMD() As ICBank
            Dim ICBank As New ICBank

            ICBank.es.Connection.CommandTimeout = 3600
            ICBank.Query.Select(ICBank.Query.BankCode, ICBank.Query.BankIMD)
            ICBank.Query.Where(ICBank.Query.IsPrincipal = True And ICBank.Query.IsActive = True, ICBank.Query.IsApproved = True)
            ICBank.Query.Load()
            Return ICBank

          
        End Function


        Public Shared Function IsStaleAccountPresent() As Boolean
            Dim collicBankAccounts As New ICBankAccountsCollection

            collicBankAccounts.Query.Where(collicBankAccounts.Query.IsStaleAccount = True)
            If collicBankAccounts.Query.Load() = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function GetStaleAccount() As String
            Dim collicBankAccounts As New ICBankAccountsCollection
            Dim StaleAccount As String = Nothing
            Dim dt As New DataTable
            collicBankAccounts.Query.Select(collicBankAccounts.Query.AccountNumber)
            collicBankAccounts.Query.Where(collicBankAccounts.Query.IsStaleAccount = True And collicBankAccounts.Query.IsApproved = True And collicBankAccounts.Query.IsActive = True)
            collicBankAccounts.Query.Load()
            dt = collicBankAccounts.Query.LoadDataTable
            If dt.Rows.Count > 0 Then
                StaleAccount = CStr(dt.Rows(0)(0))
            End If
            Return StaleAccount
        End Function
        Public Shared Function GetStaleAccountSingleObject() As ICBankAccounts
            Dim collicBankAccounts As New ICBankAccountsCollection


            collicBankAccounts.Query.Where(collicBankAccounts.Query.IsStaleAccount = True And collicBankAccounts.Query.IsApproved = True And collicBankAccounts.Query.IsActive = True)
            collicBankAccounts.Query.Load()

        
            Return collicBankAccounts(0)
        End Function
        Public Shared Sub SetAllStaleAccountsAsFalse()

            Dim collicBankAccounts As New ICBankAccountsCollection
            If collicBankAccounts.LoadAll() Then
                For Each objBankAccounts As ICBankAccounts In collicBankAccounts
                    objBankAccounts.IsStaleAccount = False
                    objBankAccounts.Save()
                Next
            End If
        End Sub
        'Public Shared Function ValidateNormalClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
        '    Dim result As Boolean = False
        '    Dim collBankAcc As New ICBankAccountsCollection

        '    collBankAcc.es.Connection.CommandTimeout = 3600

        '    collBankAcc.Query.Where(collBankAcc.Query.AccountType = "Normal Clearing Account")
        '    collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
        '    If collBankAcc.Query.Load() Then
        '        result = True
        '    Else
        '        result = False
        '    End If

        '    Return result
        'End Function

        'Public Shared Function ValidateIntercityClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
        '    Dim result As Boolean = False
        '    Dim collBankAcc As New ICBankAccountsCollection

        '    collBankAcc.es.Connection.CommandTimeout = 3600

        '    collBankAcc.Query.Where(collBankAcc.Query.AccountType = "Intercity Clearing Account")
        '    collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
        '    If collBankAcc.Query.Load() Then
        '        result = True
        '    Else
        '        result = False
        '    End If

        '    Return result
        'End Function


        'Public Shared Function ValidateSameDayClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
        '    Dim result As Boolean = False
        '    Dim collBankAcc As New ICBankAccountsCollection

        '    collBankAcc.es.Connection.CommandTimeout = 3600

        '    collBankAcc.Query.Where(collBankAcc.Query.AccountType = "SameDay Clearing Account")
        '    collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
        '    collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
        '    If collBankAcc.Query.Load() Then
        '        result = True
        '    Else
        '        result = False
        '    End If

        '    Return result
        'End Function
        Public Shared Function ValidateNormalClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
            Dim result As Boolean = False
            Dim collBankAcc As New ICBankAccountsCollection

            collBankAcc.es.Connection.CommandTimeout = 3600

            If AccNo <> "" And BranchCode <> "" And Currency <> "" Then
                collBankAcc.Query.Where(collBankAcc.Query.AccountType = "Normal Clearing Account")
                collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
                If collBankAcc.Query.Load() Then
                    result = True
                End If
            Else
                result = False
            End If

            Return result
        End Function


        Public Shared Function ValidateIntercityClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
            Dim result As Boolean = False
            Dim collBankAcc As New ICBankAccountsCollection

            collBankAcc.es.Connection.CommandTimeout = 3600
            If AccNo <> "" And BranchCode <> "" And Currency <> "" Then
                collBankAcc.Query.Where(collBankAcc.Query.AccountType = "Intercity Clearing Account")
                collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
                If collBankAcc.Query.Load() Then
                    result = True
                End If
            Else
                result = False
            End If

            Return result
        End Function


        Public Shared Function ValidateSameDayClearingAccNoBranchCodeCurrency(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
            Dim result As Boolean = False
            Dim collBankAcc As New ICBankAccountsCollection

            collBankAcc.es.Connection.CommandTimeout = 3600
            If AccNo <> "" And BranchCode <> "" And Currency <> "" Then
                collBankAcc.Query.Where(collBankAcc.Query.AccountType = "Same Day Clearing Account")
                collBankAcc.Query.Where(collBankAcc.Query.AccountNumber = AccNo.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.BranchCode = BranchCode.ToString())
                collBankAcc.Query.Where(collBankAcc.Query.Currency = Currency.ToString())
                If collBankAcc.Query.Load() Then
                    result = True
                End If
            Else
                result = False
            End If

            Return result
        End Function
        ''Javed 15-04-2013
        Public Shared Function GetBankAccountObjectForAccountType(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As ICBankAccounts
            Dim objICBankAccounts As New ICBankAccounts
            Dim Result As Boolean = False

            objICBankAccounts.es.Connection.CommandTimeout = 3600
            objICBankAccounts.Query.Where(objICBankAccounts.Query.AccountNumber = AccountNumber And objICBankAccounts.Query.BranchCode = BranchCode And objICBankAccounts.Query.Currency = Currency)
            objICBankAccounts.Query.Load()

            Return objICBankAccounts
        End Function
        Public Shared Function GetAllBankAccountsByAccountTypes(ByVal AccountType As String) As ICBankAccountsCollection
            Dim collSameDayAcc As New ICBankAccountsCollection
            collSameDayAcc.es.Connection.CommandTimeout = 3600

            collSameDayAcc.Query.Select((collSameDayAcc.Query.CBAccountType.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.BranchCode.Coalesce("'-'") + "-" + collSameDayAcc.Query.Currency.Coalesce("'-'") + "-" + collSameDayAcc.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collSameDayAcc.Query.ClientNo.Coalesce("'-'") + "-" + collSameDayAcc.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collSameDayAcc.Query.Select((collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountTitle.Coalesce("'-'")).As("AccountTitle"))
            collSameDayAcc.Query.Where(collSameDayAcc.Query.AccountType = AccountType)
            collSameDayAcc.Query.OrderBy(collSameDayAcc.Query.AccountNumber.Ascending)
            collSameDayAcc.Query.Load()

            Return collSameDayAcc




        End Function
        Public Shared Function GetAllBankAccountsByAccountTypesActiveAndApprove(ByVal AccountType As String) As ICBankAccountsCollection
            Dim collBankAccounts As New ICBankAccountsCollection
            collBankAccounts.es.Connection.CommandTimeout = 3600

            collBankAccounts.Query.Select((collBankAccounts.Query.CBAccountType.Coalesce("'-'") + "-" + collBankAccounts.Query.AccountNumber.Coalesce("'-'") + "-" + collBankAccounts.Query.BranchCode.Coalesce("'-'") + "-" + collBankAccounts.Query.Currency.Coalesce("'-'") + "-" + collBankAccounts.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collBankAccounts.Query.ClientNo.Coalesce("'-'") + "-" + collBankAccounts.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collBankAccounts.Query.Select((collBankAccounts.Query.AccountNumber.Coalesce("'-'") + "-" + collBankAccounts.Query.AccountTitle.Coalesce("'-'")).As("AccountTitle"))
            collBankAccounts.Query.Where(collBankAccounts.Query.AccountType = AccountType And collBankAccounts.Query.IsActive = True And collBankAccounts.Query.IsApproved = True)
            collBankAccounts.Query.OrderBy(collBankAccounts.Query.AccountNumber.Ascending)
            collBankAccounts.Query.Load()

            Return collBankAccounts




        End Function

        Public Shared Function GetAllBankAccountsByAccountTypesActiveAndApproveIncludingAccountId(ByVal AccountType As String) As ICBankAccountsCollection
            Dim collBankAccounts As New ICBankAccountsCollection
            collBankAccounts.es.Connection.CommandTimeout = 3600

            collBankAccounts.Query.Select((collBankAccounts.Query.CBAccountType.Coalesce("'-'") + "-" + collBankAccounts.Query.AccountNumber.Coalesce("'-'") + "-" + collBankAccounts.Query.BranchCode.Coalesce("'-'") + "-" + collBankAccounts.Query.Currency.Coalesce("'-'") + "-" + collBankAccounts.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collBankAccounts.Query.ClientNo.Coalesce("'-'") + "-" + collBankAccounts.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collBankAccounts.Query.Select((collBankAccounts.Query.AccountNumber.Coalesce("'-'") + "-" + collBankAccounts.Query.AccountTitle.Coalesce("'-'")).As("AccountTitle"), collBankAccounts.Query.PrincipalBankAccountID)
            collBankAccounts.Query.Where(collBankAccounts.Query.AccountType = AccountType And collBankAccounts.Query.IsActive = True And collBankAccounts.Query.IsApproved = True)
            collBankAccounts.Query.OrderBy(collBankAccounts.Query.AccountNumber.Ascending)
            collBankAccounts.Query.Load()

            Return collBankAccounts




        End Function

    End Class
End Namespace
