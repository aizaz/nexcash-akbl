﻿Namespace IC
    Public Class ICIBFTRTGSRuleController
        Public Shared Sub AddIBFTRTGSRules(ByRef cIBFTRTGSRules As IC.ICIBFTRTGSRules, ByVal isUpdate As Boolean)

            Dim objICICIBFTRule As New ICIBFTRTGSRules
            objICICIBFTRule.es.Connection.CommandTimeout = 3600
            If (isUpdate = False) Then

                objICICIBFTRule.CreateBy = cIBFTRTGSRules.CreateBy
                objICICIBFTRule.CreateDate = cIBFTRTGSRules.CreateDate
                objICICIBFTRule.IsActive = True
            Else
                objICICIBFTRule.LoadByPrimaryKey(cIBFTRTGSRules.IBFTRTGSRuleID)
            End If
            objICICIBFTRule.CompanyCode = cIBFTRTGSRules.CompanyCode
            'FRCDisbursementRules.IBFTRTGSRuleName = cIBFTRTGSRules.IBFTRTGSRuleName
            objICICIBFTRule.FieldCondition = cIBFTRTGSRules.FieldCondition
            objICICIBFTRule.FieldName = cIBFTRTGSRules.FieldName
            objICICIBFTRule.DisbType = cIBFTRTGSRules.DisbType
            objICICIBFTRule.FieldType = cIBFTRTGSRules.FieldType
            objICICIBFTRule.FieldValue = cIBFTRTGSRules.FieldValue
            objICICIBFTRule.Save()

            cIBFTRTGSRules.IBFTRTGSRuleID = objICICIBFTRule.IBFTRTGSRuleID

        End Sub
        Public Shared Function CheckDuplicate(ByVal objICDisbRules As ICIBFTRTGSRules) As Boolean
            Dim objICICIBFTRule As New ICIBFTRTGSRules
            objICICIBFTRule.es.Connection.CommandTimeout = 3600

            objICICIBFTRule.Query.Where(objICICIBFTRule.Query.FieldName = objICDisbRules.FieldName And objICICIBFTRule.Query.FieldValue = objICDisbRules.FieldValue And objICICIBFTRule.Query.FieldCondition = objICDisbRules.FieldCondition And objICICIBFTRule.Query.DisbType = objICDisbRules.DisbType And objICICIBFTRule.Query.CompanyCode = objICDisbRules.CompanyCode)
            If objICICIBFTRule.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function



        Public Shared Sub DeleteIBFTRTGSRules(ByVal IBFTRulesCode As String)

            Dim objICICIBFTRule As New ICIBFTRTGSRules
            objICICIBFTRule.es.Connection.CommandTimeout = 3600
            objICICIBFTRule.LoadByPrimaryKey(IBFTRulesCode)
            objICICIBFTRule.MarkAsDeleted()
            objICICIBFTRule.Save()


        End Sub
        Public Shared Sub ApproveIBFTRTGSRules(ByVal DisbursementRulesCode As String, ByVal isapproved As Boolean, ByVal ApprovedBy As String)

            Dim objICICIBFTRule As New ICIBFTRTGSRules
            objICICIBFTRule.es.Connection.CommandTimeout = 3600
            objICICIBFTRule.LoadByPrimaryKey(DisbursementRulesCode)
            objICICIBFTRule.IsApproved = isapproved
            objICICIBFTRule.ApprovedBy = ApprovedBy
            objICICIBFTRule.ApprovedOn = Date.Now
            objICICIBFTRule.Save()



        End Sub
        Public Shared Function GetIBFTRTGSRules() As ICIBFTRTGSRulesCollection

            Dim objICICIBFTRuleColl As New ICIBFTRTGSRulesCollection
            objICICIBFTRuleColl.es.Connection.CommandTimeout = 3600
            objICICIBFTRuleColl.LoadAll()
            Return objICICIBFTRuleColl



        End Function

        Public Shared Function GetIBFTRTGSByCompanyCodeAndTransferType(ByVal CompanyCode As String, ByVal Transfertype As String) As ICIBFTRTGSRulesCollection

            Dim objICICIBFTRuleColl As New ICIBFTRTGSRulesCollection
            objICICIBFTRuleColl.es.Connection.CommandTimeout = 3600
            objICICIBFTRuleColl.Query.Where(objICICIBFTRuleColl.Query.CompanyCode = CompanyCode And objICICIBFTRuleColl.Query.DisbType = Transfertype)
            objICICIBFTRuleColl.Query.Load()
            Return objICICIBFTRuleColl



        End Function
    End Class
End Namespace

