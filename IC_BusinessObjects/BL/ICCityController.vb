﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICCityController
        Public Shared Sub AddCity(ByVal cCity As ICCity, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)


            Dim ICCity As New IC.ICCity
            Dim prevICCity As New IC.ICCity
            Dim IsActiveText As String
            Dim IsApprovedText As String
            Dim CurrentAt As String
            Dim PrevAt As String
            Dim currentProvince As New ICProvince
            Dim previousProvince As New ICProvince

            ICCity.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                ICCity.CreateBy = cCity.CreateBy
                ICCity.CreateDate = cCity.CreateDate
                currentProvince.LoadByPrimaryKey(cCity.ProvinceCode.ToString())
                ICCity.Creater = cCity.Creater
                ICCity.CreationDate = cCity.CreationDate
            Else
                ICCity.LoadByPrimaryKey(cCity.CityCode)
                prevICCity.LoadByPrimaryKey(cCity.CityCode)
                currentProvince.LoadByPrimaryKey(ICCity.ProvinceCode.ToString())
                previousProvince.LoadByPrimaryKey(prevICCity.ProvinceCode.ToString())
                ICCity.CreateBy = cCity.CreateBy
                ICCity.CreateDate = cCity.CreateDate
            End If


            ICCity.DisplayCode = cCity.DisplayCode
            ICCity.ProvinceCode = cCity.ProvinceCode
            ICCity.CityName = cCity.CityName
            ICCity.CityCode = cCity.CityCode


            ICCity.IsActive = cCity.IsActive
            ICCity.IsApproved = False

            If ICCity.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If ICCity.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            ICCity.Save()

            If (isUpdate = False) Then
                CurrentAt = "City : [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & ICCity.IsActive.ToString() & " ; IsApproved:  " & ICCity.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")


            Else

                CurrentAt = "City : Current Values [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                PrevAt = "<br />City : Previous Values [Code:  " & prevICCity.CityCode.ToString() & " ; Name:  " & prevICCity.CityName.ToString() & " ; Province:  " & previousProvince.ProvinceName.ToString() & " ; IsActive:  " & prevICCity.IsActive.ToString() & " ; IsApproved:  " & prevICCity.IsApproved.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If


        End Sub
        'Public Shared Sub AddCity(ByVal cCity As ICCity, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)


        '    Dim ICCity As New IC.ICCity
        '    Dim prevICCity As New IC.ICCity
        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim CurrentAt As String
        '    Dim PrevAt As String
        '    Dim currentProvince As New ICProvince
        '    Dim previousProvince As New ICProvince

        '    ICCity.es.Connection.CommandTimeout = 3600

        '    If (isUpdate = False) Then
        '       ICCity.CreateBy = cCity.CreateBy
        '        ICCity.CreateDate = cCity.CreateDate
        '        currentProvince.LoadByPrimaryKey(cCity.ProvinceCode.ToString())
        '        ICCity.Creater = cCity.Creater
        '        ICCity.CreationDate = cCity.CreationDate
        '    Else
        '        ICCity.LoadByPrimaryKey(cCity.CityCode)
        '        prevICCity.LoadByPrimaryKey(cCity.CityCode)
        '        currentProvince.LoadByPrimaryKey(ICCity.ProvinceCode.ToString())
        '        previousProvince.LoadByPrimaryKey(prevICCity.ProvinceCode.ToString())
        '        ICCity.CreateBy = cCity.CreateBy
        '        ICCity.CreateDate = cCity.CreateDate
        '    End If



        '    ICCity.ProvinceCode = cCity.ProvinceCode
        '    ICCity.CityName = cCity.CityName
        '    ICCity.CityCode = cCity.CityCode


        '    ICCity.IsActive = cCity.IsActive
        '    ICCity.IsApproved = False

        '    If ICCity.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If ICCity.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    ICCity.Save()

        '    If (isUpdate = False) Then
        '       CurrentAt = "City : [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & ICCity.IsActive.ToString() & " ; IsApproved:  " & ICCity.IsApproved.ToString() & "]"
        '        ICUtilities.AddAuditTrail(CurrentAt & " Added ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")


        '    Else

        '        CurrentAt = "City : Current Values [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '        PrevAt = "<br />City : Previous Values [Code:  " & prevICCity.CityCode.ToString() & " ; Name:  " & prevICCity.CityName.ToString() & " ; Province:  " & previousProvince.ProvinceName.ToString() & " ; IsActive:  " & prevICCity.IsActive.ToString() & " ; IsApproved:  " & prevICCity.IsApproved.ToString() & "] "
        '        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '    End If


        'End Sub

        Public Shared Sub DeleteCity(ByVal CityCode As String, ByVal UserID As String, ByVal UserName As String)


            Dim ICCity As New IC.ICCity
            Dim CityName As String = ""
            Dim PrevAt As String
            Dim currentProvince As New ICProvince
            ICCity.es.Connection.CommandTimeout = 3600

            ICCity.LoadByPrimaryKey(CityCode)
            currentProvince.LoadByPrimaryKey(ICCity.ProvinceCode.ToString())
            PrevAt = "City : [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & ICCity.IsActive.ToString() & " ; IsApproved:  " & ICCity.IsApproved.ToString() & "]"

            ICCity.MarkAsDeleted()
            ICCity.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")


        End Sub
        Public Shared Sub ApproveCity(ByVal CityCode As String, ByVal isapproved As Boolean, ByVal UserID As String, ByVal UserName As String)


            Dim ICCity As New IC.ICCity
            Dim CurrentAt As String
            Dim currentProvince As New ICProvince

            ICCity.es.Connection.CommandTimeout = 3600

            ICCity.LoadByPrimaryKey(CityCode)
            currentProvince.LoadByPrimaryKey(ICCity.ProvinceCode.ToString())
            ICCity.IsApproved = isapproved
            ICCity.ApprovedBy = UserID
            ICCity.ApprovedOn = Date.Now
            ICCity.Save()

            CurrentAt = "City : [Code:  " & ICCity.CityCode.ToString() & " ; Name:  " & ICCity.CityName.ToString() & " ; Province:  " & currentProvince.ProvinceName.ToString() & " ; IsActive:  " & ICCity.IsActive.ToString() & " ; IsApproved:  " & ICCity.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "City", ICCity.CityCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")
        End Sub
        Public Shared Function GetCityddl(ByVal ProvinceCode As String) As ICCityCollection


            Dim ICCity As New IC.ICCityCollection

            ICCity.es.Connection.CommandTimeout = 3600
            ICCity.LoadAll()
            ICCity.Query.Where(ICCity.Query.ProvinceCode = ProvinceCode.ToString())
            ICCity.Query.Load()

            Return ICCity

        End Function
        Public Shared Sub GetCitygv(ByVal ProvinceCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


            Dim collCity As New ICCityCollection
            collCity.Query.Where(collCity.Query.ProvinceCode = ProvinceCode.ToString())
            If Not pagenumber = 0 Then
                collCity.Query.Select(collCity.Query.CityCode, collCity.Query.CityName, collCity.Query.IsActive, collCity.Query.IsApproved)
                collCity.Query.Select(collCity.Query.DisplayCode)
                collCity.Query.OrderBy(collCity.Query.CityName.Ascending)
                collCity.Query.Load()
                rg.DataSource = collCity

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collCity.Query.es.PageNumber = 1
                collCity.Query.es.PageSize = pagesize
                collCity.Query.OrderBy(collCity.Query.CityName.Ascending)
                collCity.Query.Load()
                rg.DataSource = collCity


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub

        'Public Shared Sub GetCitygv(ByVal ProvinceCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


        '    Dim collCity As New ICCityCollection
        '    collCity.Query.Where(collCity.Query.ProvinceCode = ProvinceCode.ToString())
        '    If Not pagenumber = 0 Then
        '        collCity.Query.Select(collCity.Query.CityCode, collCity.Query.CityName, collCity.Query.IsActive, collCity.Query.IsApproved)
        '        collCity.Query.OrderBy(collCity.Query.CityName.Ascending)
        '        collCity.Query.Load()
        '        rg.DataSource = collCity

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        collCity.Query.es.PageNumber = 1
        '        collCity.Query.es.PageSize = pagesize
        '        collCity.Query.OrderBy(collCity.Query.CityName.Ascending)
        '        collCity.Query.Load()
        '        rg.DataSource = collCity


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub


        Public Shared Function GetCityByCountryCode(ByVal CountryCode As String) As ICCityCollection


            Dim ICCityColl As New IC.ICCityCollection
            Dim qryCountry As New ICCountryQuery("co")
            Dim qryProvince As New ICProvinceQuery("pr")
            Dim qryCity As New ICCityQuery("ci")


            ICCityColl.es.Connection.CommandTimeout = 3600



            qryCity.Select(qryCity.CityCode, qryCity.CityName)
            qryCity.InnerJoin(qryProvince).On(qryCity.ProvinceCode = qryProvince.ProvinceCode)
            qryCity.InnerJoin(qryCountry).On(qryProvince.CountryCode = qryCountry.CountryCode)
            qryCity.Where(qryCountry.CountryCode = CountryCode.ToString())
            qryCity.OrderBy(qryCity.CityName.Ascending)
            ICCityColl.Load(qryCity)
            Return ICCityColl

        End Function

        'Public Shared Function GetCityName(ByVal CityCode As String) As String
        '    Dim ICCity As New IC.ICCity
        '    ICCity.es.Connection.CommandTimeout = 3600
        '    If ICCity.LoadByPrimaryKey(CityCode) = True Then
        '        Return ICCity.CityName.ToString()
        '    Else
        '        Return ""
        '    End If
        'End Function
        Public Shared Function GetCityName(ByVal CityCode As String) As String
            Dim collICCity As New ICCityCollection
            collICCity.es.Connection.CommandTimeout = 3600

            collICCity.Query.Where(collICCity.Query.IsActive = True And collICCity.Query.IsApproved = True)
            collICCity.Query.Where(collICCity.Query.CityCode = CityCode)
            If collICCity.Query.Load() Then
                Return collICCity(0).CityName.ToString()
            Else
                Return ""
            End If
        End Function

        Public Shared Function GetCityActiveAndApprovedByProvinceCode(ByVal ProvinceCode As String) As ICCityCollection


            Dim ICCityColl As New IC.ICCityCollection

            ICCityColl.es.Connection.CommandTimeout = 3600

            ICCityColl.Query.Where(ICCityColl.Query.ProvinceCode = ProvinceCode And ICCityColl.Query.IsActive = True And ICCityColl.Query.IsApproved = True)
            ICCityColl.Query.OrderBy(ICCityColl.Query.CityName.Ascending)
            ICCityColl.Query.Load()
            Return ICCityColl

        End Function
        Public Shared Function GetAllCityActiveAndApproved() As ICCityCollection


            Dim ICCityColl As New IC.ICCityCollection

            ICCityColl.es.Connection.CommandTimeout = 3600

            ICCityColl.Query.Where(ICCityColl.Query.IsActive = True And ICCityColl.Query.IsApproved = True)
            ICCityColl.Query.OrderBy(ICCityColl.Query.CityName.Ascending)
            ICCityColl.Query.Load()
            Return ICCityColl

        End Function
        Public Shared Function GetCityByCountryCodeActiveAndApproved(ByVal CountryCode As String) As ICCityCollection


            Dim ICCityColl As New IC.ICCityCollection
            Dim qryCountry As New ICCountryQuery("co")
            Dim qryProvince As New ICProvinceQuery("pr")
            Dim qryCity As New ICCityQuery("ci")


            ICCityColl.es.Connection.CommandTimeout = 3600


            qryCity.Select(qryCity.CityCode, qryCity.CityName)
            qryCity.InnerJoin(qryProvince).On(qryCity.ProvinceCode = qryProvince.ProvinceCode)
            qryCity.InnerJoin(qryCountry).On(qryProvince.CountryCode = qryCountry.CountryCode)
            qryCity.Where(qryCountry.CountryCode = CountryCode.ToString() And qryCity.IsActive = True And qryCity.IsApproved = True)
            qryCity.OrderBy(qryCity.CityName.Ascending)
            ICCityColl.Load(qryCity)
            Return ICCityColl

        End Function
        Public Shared Function GetCityActiveAndApprovedCityByCityCode(ByVal CityCode As String) As ICCityCollection


            Dim ICCityColl As New IC.ICCityCollection

            ICCityColl.es.Connection.CommandTimeout = 3600

            If CityCode <> "0" Then
                ICCityColl.Query.Where(ICCityColl.Query.CityCode = CityCode.ToString() And ICCityColl.Query.IsActive = True And ICCityColl.Query.IsApproved = True)
                ICCityColl.Query.Load()
            Else
                ICCityColl.Query.Where(ICCityColl.Query.IsActive = True And ICCityColl.Query.IsApproved = True)
                ICCityColl.Query.Load()

            End If
            ICCityColl.Query.OrderBy(ICCityColl.Query.CityName.Ascending)
            Return ICCityColl

        End Function
        ' Aizaz Ahmed [Dated: 12-Feb-2013]
        Public Shared Function IsCityExistByCityNameOrCode(ByVal City As String) As Boolean
            Dim objICCity As New ICCity
            objICCity.es.Connection.CommandTimeout = 3600
            Dim result As Integer
            If Integer.TryParse(City, result) = True Then
                objICCity.Query.Where(objICCity.Query.CityCode = City And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                If objICCity.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            Else
                objICCity = New ICCity
                objICCity.Query.Where(objICCity.Query.CityName.ToLower = City.ToLower.ToString() And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                If objICCity.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function GetCityByCityNameOrCode(ByVal City As String) As ICCity
            Dim objICCity As ICCity

            Dim result As Integer
            If Integer.TryParse(City, result) = True Then
                objICCity = New ICCity
                objICCity.Query.Where(objICCity.Query.CityCode = City And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                If objICCity.Query.Load() Then
                    Return objICCity
                Else
                    objICCity = New ICCity
                    objICCity.Query.Where(objICCity.Query.DisplayCode = City And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                    If objICCity.Query.Load() Then
                        Return objICCity
                    Else
                        Return Nothing
                    End If
                End If
            Else
                objICCity = New ICCity
                objICCity.Query.Where(objICCity.Query.CityName.ToLower = City.ToLower.ToString() And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                If objICCity.Query.Load() Then
                    Return objICCity
                Else
                    objICCity = New ICCity
                    objICCity.Query.Where(objICCity.Query.DisplayCode = City And objICCity.Query.IsActive = True And objICCity.Query.IsApproved = True)
                    If objICCity.Query.Load() Then
                        Return objICCity
                    Else
                        Return Nothing
                    End If
                End If
            End If
        End Function
        Public Shared Function IsCityExistByCityNameOrCodeForBulkUpload() As DataTable
            Dim qryObjICCity As New ICCityQuery("qryObjICCity")
            qryObjICCity.Select(qryObjICCity.CityCode, qryObjICCity.CityName.ToLower.Trim)
            qryObjICCity.Where(qryObjICCity.IsActive = True And qryObjICCity.IsApproved = True)
            qryObjICCity.OrderBy(qryObjICCity.CityCode.Ascending)
            Return qryObjICCity.LoadDataTable

        End Function
        Public Shared Function GetActiveandApproveCitybyProvinceandCityName(ByVal ProvinceName As String, ByVal CityName As String) As String
            Dim qryCity As New ICCityQuery("qryCity")
            Dim qryProvince As New ICProvinceQuery("qryProvince")
            Dim CityCode As String = ""
            Dim dt As New DataTable
            qryCity.Select(qryCity.CityCode)
            qryCity.InnerJoin(qryProvince).On(qryCity.ProvinceCode = qryProvince.ProvinceCode)
            qryCity.Where(qryProvince.ProvinceName.ToLower = ProvinceName.ToLower.ToString())
            qryCity.Where(qryCity.CityName.ToLower = CityName.ToLower.ToString())
            qryCity.Where(qryCity.IsActive = True And qryCity.IsApproved = True)
            dt = qryCity.LoadDataTable()
            If dt.Rows.Count > 0 Then
                CityCode = qryCity.LoadDataTable.Rows(0)(0)

                If Not CityCode.Trim = "" And Not CityCode Is Nothing Then
                    Return CityCode
                Else
                    Return CityCode = ""
                End If
            Else
                Return CityCode = ""
            End If
        End Function
    End Class
End Namespace
