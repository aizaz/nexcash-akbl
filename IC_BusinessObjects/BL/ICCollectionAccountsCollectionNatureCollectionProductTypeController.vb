﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionAccountsCollectionNatureCollectionProductTypeController

        Public Shared Sub AddCollectionAccountCollectionNatureCollectionProductType(ByVal ICCollAccountCollectionNatureCollectionProductType As ICCollectionAccountsCollectionNatureProductType, ByVal UserID As String, ByVal UserName As String, ByVal isUpdate As Boolean, ByVal OldValues As String)
            Dim objCollAccountNatureProductType As New ICCollectionAccountsCollectionNatureProductType
            Dim prevobjCollAccountNatureProductType As New ICCollectionAccountsCollectionNatureProductType
            Dim CurrentAt As String
            Dim PrevAt As String

            If (isUpdate = False) Then
                objCollAccountNatureProductType.CreateBy = ICCollAccountCollectionNatureCollectionProductType.CreateBy
                objCollAccountNatureProductType.CreateDate = ICCollAccountCollectionNatureCollectionProductType.CreateDate
            Else
                objCollAccountNatureProductType.LoadByPrimaryKey(ICCollAccountCollectionNatureCollectionProductType.AccountNumber, ICCollAccountCollectionNatureCollectionProductType.BranchCode, ICCollAccountCollectionNatureCollectionProductType.Currency, ICCollAccountCollectionNatureCollectionProductType.CollectionNatureCode, ICCollAccountCollectionNatureCollectionProductType.ProductTypeCode)
                prevobjCollAccountNatureProductType.LoadByPrimaryKey(ICCollAccountCollectionNatureCollectionProductType.AccountNumber, ICCollAccountCollectionNatureCollectionProductType.BranchCode, ICCollAccountCollectionNatureCollectionProductType.Currency, ICCollAccountCollectionNatureCollectionProductType.CollectionNatureCode, ICCollAccountCollectionNatureCollectionProductType.ProductTypeCode)

                objCollAccountNatureProductType.CreateBy = ICCollAccountCollectionNatureCollectionProductType.CreateBy
                objCollAccountNatureProductType.CreateDate = ICCollAccountCollectionNatureCollectionProductType.CreateDate
            End If

            objCollAccountNatureProductType.AccountNumber = ICCollAccountCollectionNatureCollectionProductType.AccountNumber
            objCollAccountNatureProductType.BranchCode = ICCollAccountCollectionNatureCollectionProductType.BranchCode
            objCollAccountNatureProductType.Currency = ICCollAccountCollectionNatureCollectionProductType.Currency
            objCollAccountNatureProductType.CollectionNatureCode = ICCollAccountCollectionNatureCollectionProductType.CollectionNatureCode
            objCollAccountNatureProductType.ProductTypeCode = ICCollAccountCollectionNatureCollectionProductType.ProductTypeCode
            objCollAccountNatureProductType.IsActive = ICCollAccountCollectionNatureCollectionProductType.IsActive
            objCollAccountNatureProductType.Save()

            If (isUpdate = False) Then
                CurrentAt = "Collection Account, Nature and Product Type Tagging : Current Values [Account Number : " & ICCollAccountCollectionNatureCollectionProductType.AccountNumber & " ; Branch Code : " & ICCollAccountCollectionNatureCollectionProductType.BranchCode & " ; Currency : " & ICCollAccountCollectionNatureCollectionProductType.Currency & " ; Collection Nature Code : " & ICCollAccountCollectionNatureCollectionProductType.CollectionNatureCode & " ; Product Type Code : " & ICCollAccountCollectionNatureCollectionProductType.ProductTypeCode & "] "
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Collection Account, Nature and Product Type Tagging", objCollAccountNatureProductType.AccountNumber & "-" & objCollAccountNatureProductType.BranchCode & "-" & objCollAccountNatureProductType.Currency & "-" & objCollAccountNatureProductType.CollectionNatureCode, UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "Collection Account, Nature and Product Type Tagging : Current Values [Account Number : " & objCollAccountNatureProductType.AccountNumber & " ; Branch Code : " & objCollAccountNatureProductType.BranchCode & " ; Currency : " & objCollAccountNatureProductType.Currency & " ; Collection Nature Code : " & objCollAccountNatureProductType.CollectionNatureCode & " ; Product Type Code : " & objCollAccountNatureProductType.ProductTypeCode & "] "
                PrevAt = OldValues
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Collection Account, Nature and Product Type Tagging", objCollAccountNatureProductType.AccountNumber & "-" & objCollAccountNatureProductType.BranchCode & "-" & objCollAccountNatureProductType.Currency & "-" & objCollAccountNatureProductType.CollectionNatureCode, UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub


        Public Shared Sub SetgvCollectionAccountCollectionNaturesProductType(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")
            Dim qryCollectionNature As New ICCollectionNatureQuery("pn")
            Dim qryCollectionAccount As New ICCollectionAccountsQuery("acc")
            Dim qryProductType As New ICCollectionProductTypeQuery("qryProductType")
            Dim qryAccountCollectionNatures As New ICCollectionAccountsCollectionNatureProductTypeQuery("acccpn")
            Dim dt As New DataTable
            Dim dtProductType As DataTable
            Dim dr As DataRow
            Dim drProductType As DataRow


            If Not pagenumber = 0 Then

                qryAccountCollectionNatures.Select(qryAccountCollectionNatures.AccountNumber, qryAccountCollectionNatures.BranchCode, qryAccountCollectionNatures.Currency, qryAccountCollectionNatures.CollectionNatureCode, qryCollectionNature.CollectionNatureName, qryCollectionAccount.AccountTitle, qryGroup.GroupName, qryCompany.CompanyName)
                qryAccountCollectionNatures.InnerJoin(qryCollectionAccount).On(qryAccountCollectionNatures.AccountNumber = qryCollectionAccount.AccountNumber And qryAccountCollectionNatures.BranchCode = qryCollectionAccount.BranchCode And qryAccountCollectionNatures.Currency = qryCollectionAccount.Currency)
                qryAccountCollectionNatures.InnerJoin(qryCollectionNature).On(qryAccountCollectionNatures.CollectionNatureCode = qryCollectionNature.CollectionNatureCode)
                qryAccountCollectionNatures.InnerJoin(qryCompany).On(qryCollectionAccount.CompanyCode = qryCompany.CompanyCode)
                qryAccountCollectionNatures.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)

                qryAccountCollectionNatures.GroupBy(qryAccountCollectionNatures.AccountNumber, qryAccountCollectionNatures.BranchCode, qryAccountCollectionNatures.Currency, qryAccountCollectionNatures.CollectionNatureCode, qryCollectionNature.CollectionNatureName, qryCollectionAccount.AccountTitle, qryGroup.GroupName, qryCompany.CompanyName)

                If Not GroupCode.ToString() = "0" Then
                    qryAccountCollectionNatures.Where(qryGroup.GroupCode = GroupCode)
                End If
                If Not CompanyCode.ToString() = "0" Then
                    qryAccountCollectionNatures.Where(qryCompany.CompanyCode = CompanyCode)
                End If
                qryAccountCollectionNatures.OrderBy(qryGroup.GroupName.Ascending, qryCompany.CompanyName.Ascending, qryAccountCollectionNatures.AccountNumber.Ascending, qryCollectionNature.CollectionNatureName.Ascending)

                dt = qryAccountCollectionNatures.LoadDataTable()
                dt.Columns.Add(New DataColumn("ProductTypeName", GetType(System.String)))

                For Each dr In dt.Rows
                    qryAccountCollectionNatures = New ICCollectionAccountsCollectionNatureProductTypeQuery("acccpn")

                    qryAccountCollectionNatures.Select(qryProductType.ProductTypeName)
                    qryAccountCollectionNatures.InnerJoin(qryProductType).On(qryAccountCollectionNatures.ProductTypeCode = qryProductType.ProductTypeCode)
                    qryAccountCollectionNatures.Where(qryAccountCollectionNatures.AccountNumber = dr("AccountNumber") And qryAccountCollectionNatures.BranchCode = dr("BranchCode") And qryAccountCollectionNatures.Currency = dr("Currency") And qryAccountCollectionNatures.CollectionNatureCode = dr("CollectionNatureCode"))
                    qryAccountCollectionNatures.GroupBy(qryProductType.ProductTypeName)

                    dtProductType = New DataTable
                    dtProductType = qryAccountCollectionNatures.LoadDataTable()

                    If dtProductType.Rows.Count > 0 Then
                        Dim ProductTypeName As String = ""

                        For Each drProductType In dtProductType.Rows
                            ProductTypeName = ProductTypeName & "" & drProductType("ProductTypeName") & ", "
                        Next

                        ProductTypeName = ProductTypeName.ToString().Remove(ProductTypeName.ToString().Length - 2).ToString()
                        dr("ProductTypeName") = ProductTypeName
                    End If
                Next

                rg.DataSource = dt
                If DoDataBind Then
                    rg.DataBind()
                End If

            Else
                qryAccountCollectionNatures.es.PageNumber = 1
                qryAccountCollectionNatures.es.PageSize = pagesize
                rg.DataSource = qryAccountCollectionNatures.LoadDataTable()
                If DoDataBind Then
                    rg.DataBind()
                End If
            End If



        End Sub


        'Public Shared Sub DeleteCollectionAccountCollectionNatureProductType(ByVal ICCollectionAccountCollectionNature As ICCollectionAccountsCollectionNatureProductType, ByVal ActionText As String, ByVal UserID As String, ByVal UserName As String)
        '    Dim objCollectionAccountCollectionNature As New ICCollectionAccountsCollectionNatureProductType
        '    objCollectionAccountCollectionNature.es.Connection.CommandTimeout = 3600

        '    objCollectionAccountCollectionNature.LoadByPrimaryKey(ICCollectionAccountCollectionNature.AccountNumber, ICCollectionAccountCollectionNature.BranchCode, ICCollectionAccountCollectionNature.Currency, ICCollectionAccountCollectionNature.CollectionNatureCode, ICCollectionAccountCollectionNature.ProductTypeCode)
        '    objCollectionAccountCollectionNature.MarkAsDeleted()
        '    objCollectionAccountCollectionNature.Save()

        '    ICUtilities.AddAuditTrail(ActionText.ToString(), "Collection Account Collection Nature Collection Product Type", ICCollectionAccountCollectionNature.AccountNumber & ";" & ICCollectionAccountCollectionNature.BranchCode & ";" & ICCollectionAccountCollectionNature.Currency & ";" & ICCollectionAccountCollectionNature.CollectionNatureCode, UserID.ToString(), UserName.ToString())
        'End Sub

        Public Shared Sub DeleteCollectionAccountCollectionNatureProductType(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal CollectionNatureCode As String, ByVal UserID As String, ByVal UserName As String)
            Dim collCollectionAccountCollectionNature As New ICCollectionAccountsCollectionNatureProductTypeCollection
            collCollectionAccountCollectionNature.es.Connection.CommandTimeout = 3600
            Dim objCollAccNPro As ICCollectionAccountsCollectionNatureProductType
            Dim CurrentAt As String = ""

            collCollectionAccountCollectionNature.Query.Where(collCollectionAccountCollectionNature.Query.AccountNumber = AccNo And collCollectionAccountCollectionNature.Query.BranchCode = BranchCode And collCollectionAccountCollectionNature.Query.Currency = Currency And collCollectionAccountCollectionNature.Query.CollectionNatureCode = CollectionNatureCode)
            If collCollectionAccountCollectionNature.Query.Load() Then
                For Each objCollAccNProInColl As ICCollectionAccountsCollectionNatureProductType In collCollectionAccountCollectionNature

                    objCollAccNPro = New ICCollectionAccountsCollectionNatureProductType
                    objCollAccNPro.es.Connection.CommandTimeout = 3600

                    objCollAccNPro.LoadByPrimaryKey(objCollAccNProInColl.AccountNumber.ToString, objCollAccNProInColl.BranchCode.ToString, objCollAccNProInColl.Currency.ToString, objCollAccNProInColl.CollectionNatureCode.ToString, objCollAccNProInColl.ProductTypeCode.ToString)
                    CurrentAt = "Collection Account, Nature and Product Type Tagging [Account Number : " & AccNo & " ; Branch Code : " & BranchCode & " ; Currency : " & Currency & " ; Collection Nature Code : " & CollectionNatureCode & " ; Product Type Code : " & ProductTypeCode & "] "
                    objCollAccNPro.MarkAsDeleted()
                    objCollAccNPro.Save()
                Next
            End If
            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Collection Account, Nature and Product Type Tagging", AccNo & ";" & BranchCode & ";" & Currency & ";" & CollectionNatureCode, UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Function GetTaggedProductType(ByVal ProdTypeCode As String, ByVal CollNatureCode As String, ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean

            Dim objCollAccNatureProdType As New ICCollectionAccountsCollectionNatureProductType
            Dim result As Boolean = False

            objCollAccNatureProdType.Query.Select(objCollAccNatureProdType.Query.ProductTypeCode)

            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.ProductTypeCode = ProdTypeCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.CollectionNatureCode = CollNatureCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.AccountNumber = AccNo)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.BranchCode = BranchCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.Currency = Currency)

            If objCollAccNatureProdType.Query.Load() Then
                result = True
            Else
                result = False
            End If

            Return result

        End Function


        Public Shared Function GetUnTaggedProductType(ByVal ProdTypeCode As String, ByVal CollNatureCode As String, ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean

            Dim objCollAccNatureProdType As New ICCollectionAccountsCollectionNatureProductType
            Dim result As Boolean = False

            objCollAccNatureProdType.Query.Select(objCollAccNatureProdType.Query.ProductTypeCode)

            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.ProductTypeCode = ProdTypeCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.CollectionNatureCode = CollNatureCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.AccountNumber = AccNo)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.BranchCode = BranchCode)
            objCollAccNatureProdType.Query.Where(objCollAccNatureProdType.Query.Currency = Currency)

            If objCollAccNatureProdType.Query.Load() Then
                result = True
            Else
                result = False
            End If

            Return result

        End Function



        'Public Shared Function GetProductTyeByCollAccCollNature(ByVal CollNatureCode As String, ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable
        '    Dim qryCollAccNatureProdType As New ICCollectionAccountsCollectionNatureProductTypeQuery("qryAccNatureProdType")
        '    Dim qryProductType As New ICCollectionProductTypeQuery("qryProductType")
        '    Dim dt As New DataTable

        '    qryCollAccNatureProdType.Select(qryProductType.DisbursementMode, qryProductType.ProductTypeCode, qryProductType.ProductTypeName)
        '    qryCollAccNatureProdType.Select((qryCollAccNatureProdType.ProductTypeCode + "-" + qryProductType.DisbursementMode).As("ProductTypeCodeAndDisbursementMode"))

        '    qryCollAccNatureProdType.InnerJoin(qryProductType).On(qryCollAccNatureProdType.ProductTypeCode = qryProductType.ProductTypeCode)
        '    qryCollAccNatureProdType.Where(qryCollAccNatureProdType.CollectionNatureCode = CollNatureCode)
        '    qryCollAccNatureProdType.Where(qryCollAccNatureProdType.AccountNumber = AccNo)
        '    qryCollAccNatureProdType.Where(qryCollAccNatureProdType.BranchCode = BranchCode)
        '    qryCollAccNatureProdType.Where(qryCollAccNatureProdType.Currency = Currency)

        '    dt = qryCollAccNatureProdType.LoadDataTable()

        '    Return dt


        'End Function


        Public Shared Function GetProductTyeByCollAccCollNature(ByVal CollNatureCode As String, ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable
            Dim qryCollAccNatureProdType As New ICCollectionAccountsCollectionNatureProductTypeQuery("qryAccNatureProdType")
            Dim qryProductType As New ICCollectionProductTypeQuery("qryProductType")
            Dim dt As New DataTable

            qryCollAccNatureProdType.Select(qryProductType.ProductTypeName)
            qryCollAccNatureProdType.Select((qryCollAccNatureProdType.ProductTypeCode + "-" + qryProductType.DisbursementMode).As("ProductTypeCodeAndDisbursementMode"))

            qryCollAccNatureProdType.InnerJoin(qryProductType).On(qryCollAccNatureProdType.ProductTypeCode = qryProductType.ProductTypeCode)
            qryCollAccNatureProdType.Where(qryCollAccNatureProdType.CollectionNatureCode = CollNatureCode)
            qryCollAccNatureProdType.Where(qryCollAccNatureProdType.AccountNumber = AccNo)
            qryCollAccNatureProdType.Where(qryCollAccNatureProdType.BranchCode = BranchCode)
            qryCollAccNatureProdType.Where(qryCollAccNatureProdType.Currency = Currency)

            dt = qryCollAccNatureProdType.LoadDataTable()

            Return dt


        End Function


    End Class
End Namespace
