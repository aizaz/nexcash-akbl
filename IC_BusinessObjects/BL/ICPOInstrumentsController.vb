﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICPOInstrumentsController
        Public Shared Sub AddPOInstrumentSeriesFromPOMasterSeries(ByVal POInstruments As ICPOInstruments, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objICPOInstruments As New ICPOInstruments
            Dim prevobjICPOInstruments As New ICPOInstruments
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""

            objICPOInstruments.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objICPOInstruments.CreatedBy = POInstruments.CreatedBy
                objICPOInstruments.CreatedDate = POInstruments.CreatedDate
                objICPOInstruments.IsUsed = POInstruments.IsUsed
                objICPOInstruments.Creater = POInstruments.Creater
                objICPOInstruments.CreationDate = POInstruments.CreationDate

            Else
                objICPOInstruments.LoadByPrimaryKey(POInstruments.POMasterSeriesID)
                objICPOInstruments.CreatedBy = POInstruments.CreatedBy
                objICPOInstruments.CreatedDate = POInstruments.CreatedDate
                prevobjICPOInstruments.LoadByPrimaryKey(POInstruments.POMasterSeriesID)
            End If

            objICPOInstruments.PreFix = POInstruments.PreFix
            objICPOInstruments.POMasterSeriesID = POInstruments.POMasterSeriesID
            objICPOInstruments.POInstrumentNumber = POInstruments.POInstrumentNumber


            objICPOInstruments.Save()

            If (isUpdate = False) Then
                CurrentAt = "POInstruments [Code:  " & objICPOInstruments.POInstrumentsID.ToString() & " ; POMasterSeriesID: " & objICPOInstruments.POMasterSeriesID.ToString() & " ; Prefix:  " & objICPOInstruments.PreFix.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "PO Instrument", objICPOInstruments.POInstrumentsID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "POInstruments : Current Values [Code:  " & objICPOInstruments.POInstrumentsID.ToString() & " ; POMasterSeriesID: " & objICPOInstruments.POMasterSeriesID.ToString() & " ; Prefix:  " & objICPOInstruments.PreFix.ToString() & "]"
                PrevAt = "<br />POMasterSeries : Previous Values [Code:  " & prevobjICPOInstruments.POInstrumentsID.ToString() & " ; POMasterSeriesID: " & prevobjICPOInstruments.POMasterSeriesID.ToString() & " ; Prefix:  " & prevobjICPOInstruments.PreFix.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "PO Instrument", objICPOInstruments.POInstrumentsID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Sub


        Public Shared Sub DeleteAddedPOInstrumentSeriesByPOMasterSeriesID(ByVal POMasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim objICPOInstrument As New ICPOInstruments

            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600
            objICPOInstrument.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID)
            objICPOInstrumentColl.Query.Load()

            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstrumentInColl As ICPOInstruments In objICPOInstrumentColl
                    objICPOInstrument.LoadByPrimaryKey(objICPOInstrumentInColl.POInstrumentsID)
                    objICPOInstrument.MarkAsDeleted()
                    objICPOInstrument.Save()
                    ICUtilities.AddAuditTrail("Instrument No: " & objICPOInstrumentInColl.POInstrumentsID.ToString & " ; PO Master Series No: " & objICPOInstrumentInColl.POMasterSeriesID.ToString & " ; Prefix: " & objICPOInstrumentInColl.PreFix.ToString & " deleted", "POInstrument", objICPOInstrumentInColl.POInstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub

        Public Shared Sub DeleteUpdatedPOInstrumentSeriesByPOMasterSeriesID(ByVal POMasterSeriesID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal TopOrder As Integer)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim objICPOInstrument As New ICPOInstruments

            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600
            objICPOInstrument.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID)
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber.Descending)
            objICPOInstrumentColl.Query.es.Top = TopOrder
            objICPOInstrumentColl.Query.Load()
            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstrumentInColl As ICPOInstruments In objICPOInstrumentColl
                    objICPOInstrument.LoadByPrimaryKey(objICPOInstrumentInColl.POInstrumentsID)
                    objICPOInstrument.MarkAsDeleted()
                    objICPOInstrument.Save()
                    ICUtilities.AddAuditTrail("Instrument No: " & objICPOInstrumentInColl.POInstrumentsID.ToString & " of Master Series with ID: " & objICPOInstrumentInColl.POMasterSeriesID.ToString & " deleted on update.", "POInstrument", objICPOInstrumentInColl.POInstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub

        Public Shared Function GetLastPOInstrumentOfPOMasterSeries(ByVal POMasterSeriesID As String) As Integer
            Dim qryPOInstrument As New ICPOInstrumentsQuery("qryPOInstrument")
            Dim dt As New DataTable

            qryPOInstrument.Select(qryPOInstrument.POInstrumentNumber)
            qryPOInstrument.Where(qryPOInstrument.POMasterSeriesID = POMasterSeriesID)
            qryPOInstrument.OrderBy(qryPOInstrument.POInstrumentNumber.Descending)
            qryPOInstrument.es.Top = 1
            dt = qryPOInstrument.LoadDataTable
            Return CInt(dt.Rows(0)(0))
        End Function
        'Public Shared Function GetFirstUnAssignedInstrumentOfPOInstrumentSeries() As String
        '    Dim qryObjICDDInstrument As New ICPOInstrumentsQuery("qryObjICDDInstrument")
        '    Dim dt As New DataTable
        '    Dim StrInstrumentNumber As String = Nothing

        '    qryObjICDDInstrument.Select((qryObjICDDInstrument.PreFix.Cast(EntitySpaces.DynamicQuery.esCastType.String) + "-" + qryObjICDDInstrument.POInstrumentNumber.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("POInstrumentNumber"))
        '    qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull)
        '    qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.POMasterSeriesID.Ascending, qryObjICDDInstrument.PreFix.Ascending, qryObjICDDInstrument.POInstrumentNumber.Ascending)
        '    qryObjICDDInstrument.es.Top = 1
        '    dt = qryObjICDDInstrument.LoadDataTable
        '    If dt.Rows.Count > 0 Then
        '        StrInstrumentNumber = CStr(dt.Rows(0)(0))
        '    End If
        '    Return StrInstrumentNumber
        'End Function
        Public Shared Function GetFirstUnAssignedInstrumentOfPOInstrumentSeriesByPrintLocation(ByVal PrintLocationCode As String) As String
            Dim qryObjICDDInstrument As New ICPOInstrumentsQuery("qryObjICDDInstrument")
            Dim dt As New DataTable
            Dim StrInstrumentNumber As String = Nothing

            qryObjICDDInstrument.Select((qryObjICDDInstrument.PreFix.Cast(EntitySpaces.DynamicQuery.esCastType.String) + "-" + qryObjICDDInstrument.POInstrumentNumber.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("POInstrumentNumber"))
            qryObjICDDInstrument.Where(qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull And qryObjICDDInstrument.OfficeCode = PrintLocationCode)
            qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.POMasterSeriesID.Ascending, qryObjICDDInstrument.PreFix.Ascending, qryObjICDDInstrument.POInstrumentNumber.Ascending)
            qryObjICDDInstrument.es.Top = 1
            dt = qryObjICDDInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                StrInstrumentNumber = CStr(dt.Rows(0)(0))
            End If
            Return StrInstrumentNumber
        End Function
        Public Shared Sub MarkInstrumentNumberAssignedByInstructionIDAndPOInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal PrintLocationCode As String)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix And objICPOInstrumentColl.Query.OfficeCode = PrintLocationCode)
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstrument As ICPOInstruments In objICPOInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICPOInstrument.PreFix & "-" & objICPOInstrument.POInstrumentNumber & " ] assigned to instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is assigned by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICPOInstrument.IsAssigned = True
                    objICPOInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "PO Instrument", objICPOInstrument.POInstrumentsID.ToString, UsersID, UsersName, "ASSIGN")
                Next
            End If
        End Sub
        'Public Shared Sub MarkInstrumentNumberAssignedByInstructionIDAndPOInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
        '    Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
        '    Dim StrActionAuditTrail As String = Nothing
        '    objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

        '    objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix)
        '    objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    If objICPOInstrumentColl.Query.Load Then
        '        For Each objICPOInstrument As ICPOInstruments In objICPOInstrumentColl
        '            StrActionAuditTrail = Nothing
        '            StrActionAuditTrail += "Instrument number [ " & objICPOInstrument.PreFix & "-" & objICPOInstrument.POInstrumentNumber & " ] assigned to instruction with ID [ " & InstructionID & " ]."
        '            StrActionAuditTrail += "Instrument number is assigned by [ " & UsersID & " ] [ " & UsersName & " ]."
        '            objICPOInstrument.IsAssigned = True
        '            objICPOInstrument.Save()
        '            ICUtilities.AddAuditTrail(StrActionAuditTrail, "PO Instrument", objICPOInstrument.POInstrumentsID.ToString, UsersID, UsersName, "ASSIGN")
        '        Next
        '    End If
        'End Sub
        Public Shared Sub MarkInstrumentNumberIsUsedByInstructionIDAndPOInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal PrintLocationCode As String)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix And objICPOInstrumentColl.Query.OfficeCode = PrintLocationCode)
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstrument As ICPOInstruments In objICPOInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICPOInstrument.PreFix & "-" & objICPOInstrument.POInstrumentNumber & " ] used by instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Action was taken by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICPOInstrument.IsUsed = True
                    objICPOInstrument.UsedOn = Date.Now
                    objICPOInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "PO Instrument", objICPOInstrument.POInstrumentsID.ToString, UsersID, UsersName, "USED")
                Next
            End If
        End Sub
        'Public Shared Sub MarkInstrumentNumberIsUsedByInstructionIDAndPOInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
        '    Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
        '    Dim StrActionAuditTrail As String = Nothing
        '    objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

        '    objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix)
        '    objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    If objICPOInstrumentColl.Query.Load Then
        '        For Each objICPOInstrument As ICPOInstruments In objICPOInstrumentColl
        '            StrActionAuditTrail = Nothing
        '            StrActionAuditTrail += "Instrument number [ " & objICPOInstrument.PreFix & "-" & objICPOInstrument.POInstrumentNumber & " ] used by instruction with ID [ " & InstructionID & " ]."
        '            StrActionAuditTrail += "Action was taken by [ " & UsersID & " ] [ " & UsersName & " ]."
        '            objICPOInstrument.IsUsed = True
        '            objICPOInstrument.UsedOn = Date.Now
        '            objICPOInstrument.Save()
        '            ICUtilities.AddAuditTrail(StrActionAuditTrail, "PO Instrument", objICPOInstrument.POInstrumentsID.ToString, UsersID, UsersName, "USED")
        '        Next
        '    End If
        'End Sub
        Public Shared Function IsPOInstrumentNumberExistsByPrefixAndInstrumentNo(ByVal InstrumentNumber As String, ByVal Prefix As String) As Boolean
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix)
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICPOInstrumentColl.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsPOInstrumentNumberAssignedOrPrintedByPrefixAndInstrumentNoAndInstructionID(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal PrintLocationCode As String) As Boolean
            Dim qryObjICPOInstrument As New ICPOInstrumentsQuery("qryObjICPOInstrument")
            Dim dt As New DataTable
            qryObjICPOInstrument.Select(qryObjICPOInstrument.PreFix, qryObjICPOInstrument.POInstrumentNumber)
            qryObjICPOInstrument.Where(qryObjICPOInstrument.PreFix = Prefix And qryObjICPOInstrument.POInstrumentNumber = InstrumentNumber And qryObjICPOInstrument.OfficeCode = PrintLocationCode)
            qryObjICPOInstrument.Where(qryObjICPOInstrument.IsUsed = True Or qryObjICPOInstrument.IsAssigned = True)
            dt = qryObjICPOInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Sub MarkInstrumentNumberIsCancelledByInstructionIDAndPOInstrumentNumber(ByVal InstrumentNumber As String, ByVal Prefix As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim StrActionAuditTrail As String = Nothing
            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600

            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber = InstrumentNumber And objICPOInstrumentColl.Query.PreFix = Prefix)
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstrument As ICPOInstruments In objICPOInstrumentColl
                    StrActionAuditTrail = Nothing
                    StrActionAuditTrail += "Instrument number [ " & objICPOInstrument.PreFix & "-" & objICPOInstrument.POInstrumentNumber & " ] unassigned by instruction with ID [ " & InstructionID & " ]."
                    StrActionAuditTrail += "Instrument number is unassigned by [ " & UsersID & " ] [ " & UsersName & " ]."
                    objICPOInstrument.IsAssigned = Nothing
                    objICPOInstrument.IsUsed = False
                    objICPOInstrument.Save()
                    ICUtilities.AddAuditTrail(StrActionAuditTrail, "PO Instrument", objICPOInstrument.POInstrumentsID.ToString, UsersID, UsersName, "UNASSIGN")
                Next
            End If
        End Sub
        ''21-10-2013
        Public Shared Sub GetAllAssignedSubSetsToPrintLocationsForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal OfficeCode As String, ByVal POMasterSereisID As String)

            Dim qryObjICPOInstrument As New ICPOInstrumentsQuery("qryObjICPOInstrument")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICPOInstrumentCount As New ICPOInstrumentsQuery("qryObjICPOInstrumentCount")
            Dim qryObjICPOInstrumentUsedCount As New ICPOInstrumentsQuery("qryObjICPOInstrumentUsedCount")
            Dim qryObjICPOInstrumentUnUsedCount As New ICPOInstrumentsQuery("qryObjICPOInstrumentUnUsedCount")
            Dim qryObjICPOMasterSeries As New ICPOMasterSeriesQuery("qryObjICPOMasterSeries")
            Dim dt As New DataTable



            qryObjICPOInstrument.Select(qryObjICPOInstrument.POMasterSeriesID, qryObjICPOInstrument.SubsetFrom, qryObjICPOInstrument.SubsetTo, qryObjICPOInstrument.OfficeCode)
            qryObjICPOInstrument.Select((qryObjICPOMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String) + " - " + qryObjICPOMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("POMasterSeries"))
            qryObjICPOInstrument.Select((qryObjICOffice.OfficeCode + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICPOInstrument.Select(qryObjICPOInstrumentCount.POInstrumentNumber.Count.As("Series"))
            qryObjICPOInstrument.Select((qryObjICPOInstrumentUsedCount.[Select](qryObjICPOInstrumentUsedCount.POInstrumentNumber.Count).Where((qryObjICPOInstrumentUsedCount.IsAssigned = True Or qryObjICPOInstrumentUsedCount.IsUsed = True) And qryObjICPOInstrumentUsedCount.POMasterSeriesID = qryObjICPOInstrument.POMasterSeriesID And qryObjICPOInstrumentUsedCount.OfficeCode = qryObjICPOInstrument.OfficeCode And qryObjICPOInstrumentUsedCount.SubsetFrom = qryObjICPOInstrument.SubsetFrom And qryObjICPOInstrumentUsedCount.SubsetTo = qryObjICPOInstrument.SubsetTo).As("Used")))
            qryObjICPOInstrument.Select((qryObjICPOInstrumentUnUsedCount.[Select](qryObjICPOInstrumentUnUsedCount.POInstrumentNumber.Count).Where(qryObjICPOInstrumentUnUsedCount.IsUsed = False And qryObjICPOInstrumentUnUsedCount.IsAssigned.IsNull And qryObjICPOInstrumentUnUsedCount.POMasterSeriesID = qryObjICPOInstrument.POMasterSeriesID And qryObjICPOInstrumentUnUsedCount.OfficeCode = qryObjICPOInstrument.OfficeCode And qryObjICPOInstrumentUnUsedCount.SubsetFrom = qryObjICPOInstrument.SubsetFrom And qryObjICPOInstrumentUnUsedCount.SubsetTo = qryObjICPOInstrument.SubsetTo)).As("UnUsed"))

            qryObjICPOInstrument.InnerJoin(qryObjICOffice).On(qryObjICPOInstrument.OfficeCode = qryObjICOffice.OfficeID)
            qryObjICPOInstrument.InnerJoin(qryObjICPOInstrumentCount).On(qryObjICPOInstrument.POInstrumentsID = qryObjICPOInstrumentCount.POInstrumentsID)

            qryObjICPOInstrument.InnerJoin(qryObjICPOMasterSeries).On(qryObjICPOInstrument.POMasterSeriesID = qryObjICPOMasterSeries.POMasterSeriesID)

            If Not OfficeCode = "" Then
                qryObjICPOInstrument.Where(qryObjICOffice.OfficeID = OfficeCode)
            End If
            If Not POMasterSereisID = "" Then
                qryObjICPOInstrument.Where(qryObjICPOInstrument.POMasterSeriesID = POMasterSereisID)
            End If


            qryObjICPOInstrument.GroupBy(qryObjICPOInstrument.POMasterSeriesID, qryObjICPOInstrument.SubsetFrom, qryObjICPOInstrument.SubsetTo, qryObjICPOInstrument.OfficeCode, qryObjICPOMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICPOMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICOffice.OfficeCode, qryObjICOffice.OfficeName)
            qryObjICPOInstrument.OrderBy(qryObjICPOInstrument.POMasterSeriesID.Descending)
            dt = qryObjICPOInstrument.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICPOInstrument.es.PageNumber = PageNumber
                qryObjICPOInstrument.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICPOInstrument.es.PageNumber = 1
                qryObjICPOInstrument.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetSinglePOInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(ByVal POMasterSeriesID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String, ByVal OfficeCode As String) As ICPOInstruments
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID, objICPOInstrumentColl.Query.POInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.OfficeCode = OfficeCode)
            objICPOInstrumentColl.Query.Load()
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POMasterSeriesID.Ascending)
            Return objICPOInstrumentColl(0)
        End Function
        Public Shared Function IsPOInstrumentSeriesIsAssignedOrPrintedForSubSet(ByVal SubSetFrom As Long, ByVal SubSetTo As Long, ByVal POMasterSeriesID As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrumentColl As New ICPOInstrumentsCollection
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID And objICInstrumentColl.Query.POInstrumentNumber.Between(SubSetFrom, SubSetTo))
            If objICInstrumentColl.Query.Load() Then
                For Each objICPOInstrument As ICPOInstruments In objICInstrumentColl
                    If Not objICPOInstrument.OfficeCode Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If

            Return Result
        End Function
        Public Shared Function GetPOInstrumentsForSubSetByPOMasterSeriesIDAndSubSetRange(ByVal POMasterSeriesID As String, ByVal SubSetFrom As Long, ByVal SubSetTo As Long) As ICPOInstrumentsCollection
            Dim objICPOInstrumentcoll As New ICPOInstrumentsCollection
            objICPOInstrumentcoll.Query.Where(objICPOInstrumentcoll.Query.POMasterSeriesID = POMasterSeriesID And objICPOInstrumentcoll.Query.POInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICPOInstrumentcoll.Query.Load()
            objICPOInstrumentcoll.Query.OrderBy(objICPOInstrumentcoll.Query.POInstrumentsID.Ascending)
            Return objICPOInstrumentcoll
        End Function
        Public Shared Function IsPOInstrumentsExistForUpdateOfPOSubSet(ByVal TopOrder As Integer, ByVal objICPOInstrument As ICPOInstruments, ByVal SubSetTo As String, ByVal IsExtended As Boolean) As Boolean
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim objICInstrument As ICPOInstruments
            Dim RequiredLeavesCount As Long
            Dim dt As New DataTable
            Dim Result As Boolean = False
            objICPOInstrumentColl.es.Connection.CommandTimeout = 3600
            If IsExtended = False Then
                objICPOInstrumentColl.Query.Select(objICPOInstrumentColl.Query.POInstrumentNumber.Count)
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber.Between(objICPOInstrument.SubsetTo + 1, SubSetTo))
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = objICPOInstrument.POMasterSeriesID)
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.OfficeCode = objICPOInstrument.OfficeCode And objICPOInstrumentColl.Query.IsAssigned.IsNull And objICPOInstrumentColl.Query.IsUsed = False)
                objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber.Descending)
                objICPOInstrumentColl.Query.es.Top = TopOrder
                objICPOInstrumentColl.Query.Load()
                dt = objICPOInstrumentColl.Query.LoadDataTable
                If objICPOInstrumentColl.Query.Load Then
                    If CLng(dt.Rows(0)(0) >= TopOrder) Then
                        Result = True
                    End If
                End If
            ElseIf IsExtended = True Then
                objICPOInstrumentColl.Query.Select(objICPOInstrumentColl.Query.POInstrumentNumber.Count)
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber.Between(SubSetTo + 1, objICPOInstrument.SubsetTo))
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.IsUsed = False And objICPOInstrumentColl.Query.IsAssigned.IsNull And objICPOInstrumentColl.Query.OfficeCode.IsNull)
                objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = objICPOInstrument.POMasterSeriesID)
                objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber.Descending)
                objICPOInstrumentColl.Query.es.Top = TopOrder
                dt = objICPOInstrumentColl.Query.LoadDataTable
                If objICPOInstrumentColl.Query.Load Then
                    If CLng(dt.Rows(0)(0) >= TopOrder) Then
                        Result = True
                    End If
                End If
            End If
            Return Result

        End Function
        Public Shared Sub UpDatePOInstrumentsForUpdateOfSubSet(ByVal TopOrder As Integer, ByVal objICPOInstrument As ICPOInstruments, ByVal SubSetTo As String, ByVal UsersID As String, ByVal UsersName As String, ByVal IsExtended As Boolean)
            Dim objICInstrumentColl As New ICPOInstrumentsCollection
            Dim objICInstrumentCollSave As New ICPOInstrumentsCollection
            Dim objInstrumentForUpDate As New ICPOInstruments
            Dim objICOffice As New ICOffice
            Dim dt As New DataTable
            Dim Result As Boolean = False
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objInstrumentForUpDate.es.Connection.CommandTimeout = 3600
            If IsExtended = False Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.POInstrumentNumber.Between(objICPOInstrument.SubsetTo + 1, SubSetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.OfficeCode = objICPOInstrument.OfficeCode And objICInstrumentColl.Query.IsUsed = False And objICInstrumentColl.Query.IsAssigned.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.POInstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    For Each objInstrument As ICPOInstruments In objICInstrumentColl
                        objInstrumentForUpDate = New ICPOInstruments
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.POInstrumentsID.ToString)
                        objInstrumentForUpDate.SubsetFrom = Nothing
                        objInstrumentForUpDate.SubsetTo = Nothing
                        objInstrumentForUpDate.OfficeCode = Nothing
                        objICInstrumentCollSave.Add(objInstrumentForUpDate)
                    Next
                    If objICInstrumentCollSave.Count > 0 Then
                        objICInstrumentCollSave.Save()
                        objICOffice.LoadByPrimaryKey(objICPOInstrument.OfficeCode)
                        ICUtilities.AddAuditTrail("PO Sub Set of master series with ID [ " & objICPOInstrument.POMasterSeriesID & " ] unassigned from [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & objICPOInstrument.SubsetFrom & " ] to [ " & objICPOInstrument.SubsetTo - TopOrder & " ]. ", "PO Sub Set", objICOffice.OfficeID.ToString, UsersID.ToString, UsersName.ToString, "UNASSIGN")

                    End If
                End If
            ElseIf IsExtended = True Then
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.POInstrumentNumber.Between(SubSetTo + 1, objICPOInstrument.SubsetTo))
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.IsUsed = False And objICInstrumentColl.Query.POMasterSeriesID = objICPOInstrument.POMasterSeriesID)
                objICInstrumentColl.Query.Where(objICInstrumentColl.Query.SubsetFrom.IsNull And objICInstrumentColl.Query.SubsetTo.IsNull)
                objICInstrumentColl.Query.OrderBy(objICInstrumentColl.Query.POInstrumentNumber.Descending)
                objICInstrumentColl.Query.es.Top = TopOrder
                dt = objICInstrumentColl.Query.LoadDataTable
                If objICInstrumentColl.Query.Load Then
                    For Each objInstrument As ICPOInstruments In objICInstrumentColl
                        objInstrumentForUpDate = New ICPOInstruments
                        objInstrumentForUpDate.LoadByPrimaryKey(objInstrument.POInstrumentsID.ToString)
                        objInstrumentForUpDate.SubsetFrom = objICPOInstrument.SubsetFrom
                        objInstrumentForUpDate.SubsetTo = objICPOInstrument.SubsetTo
                        objInstrumentForUpDate.OfficeCode = objICPOInstrument.OfficeCode
                        objICInstrumentCollSave.Add(objInstrumentForUpDate)
                    Next
                    If objICInstrumentCollSave.Count > 0 Then
                        objICInstrumentCollSave.Save()
                        objICOffice.LoadByPrimaryKey(objICPOInstrument.OfficeCode)
                        ICUtilities.AddAuditTrail("PO Sub Set of series with ID [ " & objICPOInstrument.POMasterSeriesID & " ] assigned to [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & objICPOInstrument.SubsetFrom & " ] to [ " & objICPOInstrument.SubsetTo & " ]. ", "PO Sub Set", objICOffice.OfficeID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")

                    End If
                End If
            End If


        End Sub
        Public Shared Sub UpDateSubSetRangeOnUpDate(ByVal POMasterSeriesID As String, ByVal SubSetFrom As Long, ByVal SubSetTo As Long, ByVal OfficeCode As String, ByVal objICPOInstrument As ICPOInstruments)
            Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
            Dim objICPOInstrumentsForUpDate As ICPOInstruments
            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID And objICPOInstrumentColl.Query.POInstrumentNumber.Between(SubSetFrom, SubSetTo))
            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.OfficeCode = OfficeCode)
            objICPOInstrumentColl.Query.Load()
            objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentsID.Ascending)

            If objICPOInstrumentColl.Query.Load Then
                For Each objICPOInstruments As ICPOInstruments In objICPOInstrumentColl
                    objICPOInstrumentsForUpDate = New ICPOInstruments
                    objICPOInstrumentsForUpDate.LoadByPrimaryKey(objICPOInstruments.POInstrumentsID.ToString)
                    objICPOInstrumentsForUpDate.SubsetFrom = SubSetFrom
                    objICPOInstrumentsForUpDate.SubsetTo = SubSetTo

                    objICPOInstrumentsForUpDate.Save()
                Next
            End If
        End Sub
        Public Shared Function IsPOInstrumentIsUsedOrAssignedAgainstSubSetIDAndOfficeID(ByVal POMasterSeriesID As String, ByVal OfficeID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String) As Boolean
            Dim objIPOCInstrumentColl As New ICPOInstrumentsCollection
            Dim Result As Boolean = False

            objIPOCInstrumentColl.es.Connection.CommandTimeout = 3600

            objIPOCInstrumentColl.Query.Where(objIPOCInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID And objIPOCInstrumentColl.Query.OfficeCode = OfficeID)
            objIPOCInstrumentColl.Query.Where(objIPOCInstrumentColl.Query.SubsetFrom = SubSetFrom And objIPOCInstrumentColl.Query.SubsetTo = SubSetTo)

            If objIPOCInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICPOInstruments In objIPOCInstrumentColl
                    If Not objICInstrument.IsUsed = False Or Not objICInstrument.IsAssigned Is Nothing Then
                        Result = True
                        Exit For
                    End If
                Next
            End If
            Return Result
        End Function
        Public Shared Sub DeletePOInstrumentsAgainstOfficeID(ByVal POMasterSeriesID As String, ByVal OfficeID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objIPOCInstrumentColl As New ICPOInstrumentsCollection
            Dim objIPOCInstrumentCollForDelete As New ICPOInstrumentsCollection
            Dim objICOffice As New ICOffice
            Dim Result As Boolean = False

            objIPOCInstrumentColl.es.Connection.CommandTimeout = 3600

            objIPOCInstrumentColl.Query.Where(objIPOCInstrumentColl.Query.POMasterSeriesID = POMasterSeriesID And objIPOCInstrumentColl.Query.OfficeCode = OfficeID)
            objIPOCInstrumentColl.Query.Where(objIPOCInstrumentColl.Query.SubsetFrom = SubSetFrom And objIPOCInstrumentColl.Query.SubsetTo = SubSetTo)

            If objIPOCInstrumentColl.Query.Load() Then
                For Each objICInstrument As ICPOInstruments In objIPOCInstrumentColl
                    objICInstrument.SubsetTo = Nothing
                    objICInstrument.SubsetFrom = Nothing
                    objICInstrument.OfficeCode = Nothing
                    objIPOCInstrumentCollForDelete.Add(objICInstrument)
                Next
                If objIPOCInstrumentColl.Count > 0 Then
                    objIPOCInstrumentCollForDelete.Save()
                    objICOffice.LoadByPrimaryKey(OfficeID)
                    ICUtilities.AddAuditTrail("PO Sub Set with ID [ " & POMasterSeriesID & " ] unassigned from office [ " & objICOffice.OfficeCode & " ] [ " & objICOffice.OfficeName & " ] from [ " & SubSetFrom & " ] to [ " & SubSetTo & " ] .", "PO Sub Set", objICOffice.OfficeID, UsersID.ToString, UsersName.ToString, "UNASSIGN")
                End If
            End If

        End Sub
        Public Shared Function IsPOInstrumentNoIsAssignedToSpecificPrintLocationByInstrumentNoAndOfficeCode(ByVal InstrumentNo As String, ByVal Prefix As String, ByVal OfficeCode As String) As Boolean
            Dim Result As Boolean = False
            Dim objICInstrument As New ICPOInstruments
            Dim objICInstrumentColl As New ICPOInstrumentsCollection

            objICInstrument.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.es.Connection.CommandTimeout = 3600

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.POInstrumentNumber = InstrumentNo And objICInstrumentColl.Query.OfficeCode = OfficeCode And objICInstrumentColl.Query.PreFix = Prefix)
            If objICInstrumentColl.Query.Load Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function GetTotalUnUSedPOInstrumentCountByPrintLocationCode(ByVal PrintLocationCode As String) As Integer
            Dim qryObjICPOInstrument As New ICPOInstrumentsQuery("qryObjICPOInstrument")
            Dim dt As New DataTable

            qryObjICPOInstrument.Select(qryObjICPOInstrument.POInstrumentNumber.Count.As("Count"))
            qryObjICPOInstrument.Where(qryObjICPOInstrument.IsUsed = False And qryObjICPOInstrument.OfficeCode = PrintLocationCode.ToString And qryObjICPOInstrument.IsAssigned.IsNull)
            dt = qryObjICPOInstrument.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return CInt(dt.Rows(0)(0))
            Else
                Return 0
            End If
        End Function
    End Class

End Namespace
