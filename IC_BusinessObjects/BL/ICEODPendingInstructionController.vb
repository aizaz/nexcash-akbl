﻿Imports Microsoft.VisualBasic
Imports ICBO
Namespace IC
    Public Class ICEODPendingInstructionController
        Public Shared Sub AddPendingEODInstruction(ByVal objICEODInstruction As ICPendingEODInstructions, ByVal IsUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objICEODInstructionForSave As New ICPendingEODInstructions
            If IsUpdate = True Then
                objICEODInstructionForSave.LoadByPrimaryKey(objICEODInstruction.EODInstructionID)
            End If
            objICEODInstructionForSave.InstructionID = objICEODInstruction.InstructionID
            objICEODInstructionForSave.FromAccountNo = objICEODInstruction.FromAccountNo
            objICEODInstructionForSave.FromAccountBranchCode = objICEODInstruction.FromAccountBranchCode
            objICEODInstructionForSave.FromAccountSchemeCode = objICEODInstruction.FromAccountSchemeCode
            objICEODInstructionForSave.FromAccountProductCode = objICEODInstruction.FromAccountProductCode
            objICEODInstructionForSave.FromAccountCurrency = objICEODInstruction.FromAccountCurrency
            objICEODInstructionForSave.FromAccountType = objICEODInstruction.FromAccountType
            objICEODInstructionForSave.ToAccountNo = objICEODInstruction.ToAccountNo
            objICEODInstructionForSave.ToAccountBranchCode = objICEODInstruction.ToAccountBranchCode
            objICEODInstructionForSave.ToAccountSchemeCode = objICEODInstruction.ToAccountSchemeCode
            objICEODInstructionForSave.ToAccountProductCode = objICEODInstruction.ToAccountProductCode
            objICEODInstructionForSave.ToAccountCurrency = objICEODInstruction.ToAccountCurrency
            objICEODInstructionForSave.ToAccountType = objICEODInstruction.ToAccountType
            objICEODInstructionForSave.Status = objICEODInstruction.Status
            objICEODInstructionForSave.LastStatus = objICEODInstruction.LastStatus
            objICEODInstructionForSave.FinalStatus = objICEODInstruction.FinalStatus
            objICEODInstructionForSave.Amount = objICEODInstruction.Amount
            objICEODInstructionForSave.LastProcessDate = objICEODInstruction.LastProcessDate
            objICEODInstructionForSave.DrGlCode = objICEODInstruction.DrGlCode
            objICEODInstructionForSave.CrGlCode = objICEODInstruction.CrGlCode
            objICEODInstructionForSave.ProductTypeCode = objICEODInstruction.ProductTypeCode
            objICEODInstructionForSave.IsProcessed = objICEODInstruction.IsProcessed
            objICEODInstructionForSave.UserID = objICEODInstruction.UserID
            objICEODInstructionForSave.FailureStatus = objICEODInstruction.FailureStatus
            objICEODInstructionForSave.PaidDate = objICEODInstruction.PaidDate
            objICEODInstructionForSave.IsReversal = objICEODInstruction.IsReversal
            objICEODInstructionForSave.OriginatingTranType = objICEODInstruction.OriginatingTranType
            objICEODInstructionForSave.RespondingTranType = objICEODInstruction.RespondingTranType
            objICEODInstructionForSave.Save()
        End Sub
        Public Shared Sub UpdatePendingEODInstructionStatus(ByVal InstructionID As String, ByVal FromStatus As String, ByVal ToStatus As String)
            Dim objEODPendingInstruction As New ICPendingEODInstructions
            objEODPendingInstruction.Query.Where(objEODPendingInstruction.Query.InstructionID = InstructionID)
            objEODPendingInstruction.Query.Load()
            If objEODPendingInstruction.Query.Load Then
                objEODPendingInstruction.Status = ToStatus
                objEODPendingInstruction.LastStatus = FromStatus
                objEODPendingInstruction.Save()
            End If
        End Sub
        Public Shared Sub DeletePendingEODInstruction(ByVal InstructionID As String)
            Dim objEODPendingInstruction As New ICPendingEODInstructions
            objEODPendingInstruction.Query.Where(objEODPendingInstruction.Query.InstructionID = InstructionID)
            objEODPendingInstruction.Query.Load()
            If objEODPendingInstruction.Query.Load Then
                objEODPendingInstruction.MarkAsDeleted()
                objEODPendingInstruction.Save()
            End If
        End Sub
        Public Shared Sub EODAutoProcess()
            Dim objICInstruction As ICInstruction
            Dim objICProductTypeCode As ICProductType
            Dim ArrayListPassedIDS As New ArrayList
            Dim objICPendingEODInstructionColl As New ICPendingEODInstructionsCollection
            Dim StanNo As String = Nothing
            Dim FromAccountNo, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAccountCurrency As String
            Dim ToAccountNo, ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountCurrency As String
            Dim FromAccountType, ToAccountType As CBUtilities.AccountType
            Dim FinalStatus, FailureStatus, UserID As String
            objICPendingEODInstructionColl = GetAllPendingEODInstructions()
            If objICPendingEODInstructionColl.Count > 0 Then
                Try
                    If CBUtilities.GetEODStatus() = "02" Then
                        For Each objICPEODInstruction As ICPendingEODInstructions In objICPendingEODInstructionColl
                            FromAccountNo = ""
                            FromAccountBrCode = ""
                            FromAccountProductCode = ""
                            FromAccountSchemeCode = ""
                            ToAccountNo = ""
                            ToAccountBrCode = ""
                            ToAccountProductCode = ""
                            ToAccountSchemeCode = ""
                            FromAccountType = Nothing
                            ToAccountType = Nothing
                            FinalStatus = ""
                            FailureStatus = ""
                            UserID = ""
                            FromAccountNo = objICPEODInstruction.FromAccountNo
                            FromAccountBrCode = objICPEODInstruction.FromAccountBranchCode
                            FromAccountProductCode = objICPEODInstruction.FromAccountProductCode
                            FromAccountSchemeCode = objICPEODInstruction.FromAccountSchemeCode
                            FromAccountCurrency = objICPEODInstruction.FromAccountCurrency
                            If objICPEODInstruction.FromAccountType = 2 Then
                                FromAccountType = CBUtilities.AccountType.RB
                            ElseIf objICPEODInstruction.FromAccountType = 3 Then
                                FromAccountType = CBUtilities.AccountType.GL
                            End If
                            ToAccountNo = objICPEODInstruction.ToAccountNo
                            ToAccountBrCode = objICPEODInstruction.ToAccountBranchCode
                            ToAccountProductCode = objICPEODInstruction.ToAccountProductCode
                            ToAccountSchemeCode = objICPEODInstruction.ToAccountSchemeCode
                            ToAccountCurrency = objICPEODInstruction.ToAccountCurrency
                            If objICPEODInstruction.ToAccountType = 2 Then
                                ToAccountType = CBUtilities.AccountType.RB
                            ElseIf objICPEODInstruction.ToAccountType = 3 Then
                                ToAccountType = CBUtilities.AccountType.GL
                            End If
                            FinalStatus = objICPEODInstruction.FinalStatus
                            FailureStatus = objICPEODInstruction.FailureStatus
                            UserID = objICPEODInstruction.UserID

                            objICProductTypeCode = New ICProductType
                            objICInstruction = New ICInstruction
                            StanNo = Nothing
                            objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                            If objICProductTypeCode.LoadByPrimaryKey(objICPEODInstruction.ProductTypeCode) Then
                                If objICProductTypeCode.DisbursementMode.ToString.ToLower = "po" Then
                                    If objICPEODInstruction.IsReversal = False Then
                                        If CBUtilities.FundsTransfer(StanNo, FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID, Nothing, Nothing, objICPEODInstruction.DrGlCode, objICPEODInstruction.CrGlCode) = True Then
                                            If objICPEODInstruction.FinalStatus.ToString = "16" Then
                                                If ToAccountType = CBUtilities.AccountType.GL Then
                                                    objICInstruction.PayableAccountType = "GL"
                                                    objICInstruction.PayableAccountNumber = ToAccountNo
                                                    objICInstruction.PayableAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.PayableAccountCurrency = ToAccountCurrency
                                                    objICInstruction.PayableClientNo = Nothing
                                                    objICInstruction.PayableSeqNo = Nothing
                                                    objICInstruction.PayableProfitCentre = Nothing
                                                ElseIf ToAccountType = CBUtilities.AccountType.RB Then
                                                    objICInstruction.PayableAccountType = "GL"
                                                    objICInstruction.PayableAccountNumber = ToAccountNo
                                                    objICInstruction.PayableAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.PayableAccountCurrency = ToAccountCurrency
                                                    objICInstruction.PayableClientNo = Nothing
                                                    objICInstruction.PayableSeqNo = Nothing
                                                    objICInstruction.PayableProfitCentre = Nothing
                                                End If
                                                ICInstructionController.UpDateInstruction(objICInstruction, UserID, Nothing, "EOD Process Update.", "Update")
                                                objICInstruction = New ICInstruction
                                                objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                                ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                            ElseIf objICPEODInstruction.FinalStatus.ToString = "30" Then
                                                If ToAccountType = CBUtilities.AccountType.GL Then
                                                    objICInstruction.ClearingAccountType = "GL"
                                                    objICInstruction.ClearingAccountNo = ToAccountNo
                                                    objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                    objICInstruction.ClearingClientNo = Nothing
                                                    objICInstruction.ClearingSeqNo = Nothing
                                                    objICInstruction.ClearingSeqNo = Nothing
                                                ElseIf ToAccountType = CBUtilities.AccountType.RB Then
                                                    objICInstruction.ClearingAccountType = "GL"
                                                    objICInstruction.ClearingAccountNo = ToAccountNo
                                                    objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                    objICInstruction.ClearingClientNo = Nothing
                                                    objICInstruction.ClearingSeqNo = Nothing
                                                    objICInstruction.ClearingSeqNo = Nothing
                                                End If
                                                ICInstructionController.UpDateInstruction(objICInstruction, UserID, Nothing, "EOD Process Update.", "Update")
                                                objICInstruction = New ICInstruction
                                                objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                                ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                            End If
                                        End If
                                    Else
                                        If CBUtilities.Reversal(FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID, objICPEODInstruction.DrGlCode, objICPEODInstruction.CrGlCode, objICPEODInstruction.OriginatingTranType, objICPEODInstruction.RespondingTranType) = True Then
                                            objICInstruction = New ICInstruction
                                            objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                            ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        End If
                                    End If
                                ElseIf objICProductTypeCode.DisbursementMode.ToString.ToLower = "dd" Then
                                    If objICPEODInstruction.IsReversal = False Then

                                        If objICPEODInstruction.FinalStatus.ToString = "16" Then
                                            If CBUtilities.FundsTransfer(StanNo, FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID, Nothing, Nothing, objICPEODInstruction.DrGlCode, objICPEODInstruction.CrGlCode) = True Then
                                                If ToAccountType = CBUtilities.AccountType.GL Then
                                                    objICInstruction.PayableAccountType = "GL"
                                                    objICInstruction.PayableAccountNumber = ToAccountNo
                                                    objICInstruction.PayableAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.PayableAccountCurrency = ToAccountCurrency
                                                    objICInstruction.PayableClientNo = Nothing
                                                    objICInstruction.PayableSeqNo = Nothing
                                                    objICInstruction.PayableProfitCentre = Nothing
                                                ElseIf ToAccountType = CBUtilities.AccountType.RB Then
                                                    objICInstruction.PayableAccountType = "GL"
                                                    objICInstruction.PayableAccountNumber = ToAccountNo
                                                    objICInstruction.PayableAccountBranchCode = ToAccountBrCode
                                                    objICInstruction.PayableAccountCurrency = ToAccountCurrency
                                                    objICInstruction.PayableClientNo = Nothing
                                                    objICInstruction.PayableSeqNo = Nothing
                                                    objICInstruction.PayableProfitCentre = Nothing
                                                End If
                                                ICInstructionController.UpDateInstruction(objICInstruction, UserID, Nothing, "EOD Process Update.", "Update")
                                                objICInstruction = New ICInstruction
                                                objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                                ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                            End If
                                        ElseIf objICPEODInstruction.FinalStatus.ToString = "30" Then
                                            If ToAccountType = CBUtilities.AccountType.GL Then
                                                objICInstruction.ClearingAccountType = "GL"
                                                objICInstruction.ClearingAccountNo = ToAccountNo
                                                objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                objICInstruction.ClearingClientNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                            ElseIf ToAccountType = CBUtilities.AccountType.RB Then
                                                objICInstruction.ClearingAccountType = "GL"
                                                objICInstruction.ClearingAccountNo = ToAccountNo
                                                objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                objICInstruction.ClearingClientNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                            End If
                                            ICInstructionController.UpDateInstruction(objICInstruction, UserID, Nothing, "EOD Process Update.", "Update")
                                            objICInstruction = New ICInstruction
                                            objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                            ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        End If



                                    Else




                                        If CBUtilities.Reversal(FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID, objICPEODInstruction.DrGlCode, objICPEODInstruction.CrGlCode, objICPEODInstruction.OriginatingTranType, objICPEODInstruction.RespondingTranType) = True Then
                                            objICInstruction = New ICInstruction
                                            objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                            ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        End If

                                        ''If CBUtilities.Reversal(FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID) = True Then
                                        'objICInstruction = New ICInstruction
                                        'objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                        'ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                        'ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        ''End If

                                End If
                                   
                                ElseIf objICProductTypeCode.DisbursementMode.ToString.ToLower = "direct credit" Then
                                    If CBUtilities.FundsTransfer(StanNo, FromAccountNo, FromAccountBrCode, FromAccountCurrency, "", "", "", FromAccountType, ToAccountNo, ToAccountBrCode, ToAccountCurrency, "", "", "", ToAccountType, objICPEODInstruction.Amount, "Instruction", objICPEODInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UserID, Nothing, Nothing, objICPEODInstruction.DrGlCode, objICPEODInstruction.CrGlCode) = True Then
                                        If objICPEODInstruction.FinalStatus.ToString = "15" Then
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                            ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        End If
                                    End If
                                ElseIf objICProductTypeCode.DisbursementMode.ToString.ToLower = "cheque" Then
                                    If objICPEODInstruction.IsReversal = False Then
                                        If objICPEODInstruction.FinalStatus.ToString = "30" Then
                                            If ToAccountType = CBUtilities.AccountType.GL Then
                                                objICInstruction.ClearingAccountType = "GL"
                                                objICInstruction.ClearingAccountNo = ToAccountNo
                                                objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                objICInstruction.ClearingClientNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                            ElseIf ToAccountType = CBUtilities.AccountType.RB Then
                                                objICInstruction.ClearingAccountType = "GL"
                                                objICInstruction.ClearingAccountNo = ToAccountNo
                                                objICInstruction.ClearingAccountBranchCode = ToAccountBrCode
                                                objICInstruction.ClearingAccountCurrency = ToAccountCurrency
                                                objICInstruction.ClearingClientNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                                objICInstruction.ClearingSeqNo = Nothing
                                            End If
                                            ICInstructionController.UpDateInstruction(objICInstruction, UserID, Nothing, "EOD Process Update.", "Update")
                                            objICInstruction = New ICInstruction
                                            objICInstruction.LoadByPrimaryKey(objICPEODInstruction.InstructionID.ToString)
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, FinalStatus, UserID, Nothing, "Remarks", "UPDATE", "EOD Auto Process")
                                            ArrayListPassedIDS.Add(objICPEODInstruction.InstructionID)
                                        End If

                                    End If
                                End If
                            End If
                        Next
                    End If
                    If ArrayListPassedIDS.Count > 0 Then
                        For Each InstructionID In ArrayListPassedIDS
                            DeletePendingEODInstruction(InstructionID)
                        Next
                    End If
                Catch ex As Exception
                   
                        ICUtilities.AddAuditTrail("EOD auto process fails due to " & ex.Message.ToString, "EOD", Nothing, Nothing, Nothing, "UPDATE")
                        Exit Sub

                End Try
            End If
        End Sub
        Public Shared Function GetPendingEODInstructionByInstructionID(ByVal InstructionID As String) As ICPendingEODInstructions
            Dim objICPendingEODInstruction As New ICPendingEODInstructions
            objICPendingEODInstruction.Query.Where(objICPendingEODInstruction.Query.InstructionID = InstructionID)
            objICPendingEODInstruction.Query.Load()
            Return objICPendingEODInstruction
        End Function
        Public Shared Function GetAllPendingEODInstructions() As ICPendingEODInstructionsCollection
            Dim objICPendingEODInstructionColl As New ICPendingEODInstructionsCollection

            objICPendingEODInstructionColl.Query.Where(objICPendingEODInstructionColl.Query.IsProcessed = False And objICPendingEODInstructionColl.Query.Status = "47")
            objICPendingEODInstructionColl.Query.Load()
            Return objICPendingEODInstructionColl
        End Function
    End Class
End Namespace
