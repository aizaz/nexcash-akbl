﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC

    Public Class ICEmailProcessingController

        Public Shared Sub AddEmailSettings(ByVal email As ICEmailSettings, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICEmail As New ICEmailSettings
            Dim prevobjICEmail As New ICEmailSettings

            Dim CurrentAt As String = ""
            Dim IsApprovedText As String

            objICEmail.es.Connection.CommandTimeout = 3600
            prevobjICEmail.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                objICEmail.CreatedBy = email.CreatedBy
                objICEmail.CreatedDate = email.CreatedDate
                objICEmail.Creater = email.Creater
                objICEmail.CreationDate = email.CreationDate
            Else
                objICEmail.LoadByPrimaryKey(email.EmailIDSettingsID)
                objICEmail.CreatedBy = email.CreatedBy
                objICEmail.CreatedDate = email.CreatedDate
            End If

            objICEmail.AccountNumber = email.AccountNumber
            objICEmail.BranchCode = email.BranchCode
            objICEmail.Currency = email.Currency
            objICEmail.PaymentNatureCode = email.PaymentNatureCode
            objICEmail.GroupCode = email.GroupCode
            objICEmail.UserID = email.UserID
            objICEmail.FileUploadTemplateCode = email.FileUploadTemplateCode
            objICEmail.IsApprove = False

            If objICEmail.IsApprove = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If


            objICEmail.Save()

            If (IsUpdate = False) Then
                CurrentAt = "EmailProcessing : Current Values [Code:  " & objICEmail.EmailIDSettingsID.ToString() & " ;  AccountNumber:  " & objICEmail.AccountNumber.ToString() & " ; BranchCode:  " & objICEmail.BranchCode.ToString() & " ; Currency: " & objICEmail.Currency.ToString() & " ; PaymentNatureCode: " & objICEmail.PaymentNatureCode.ToString() & " ; GroupCode: " & objICEmail.GroupCode.ToString() & " ; UserID: " & objICEmail.UserID.ToString() & " ; FileUploadTemplateCode: " & objICEmail.FileUploadTemplateCode.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "EmailProcessing", objICEmail.EmailIDSettingsID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")

            Else
                CurrentAt = "EmailProcessing : Current Values [Code:  " & objICEmail.EmailIDSettingsID.ToString() & " ;  AccountNumber:  " & objICEmail.AccountNumber.ToString() & " ; BranchCode:  " & objICEmail.BranchCode.ToString() & " ; Currency: " & objICEmail.Currency.ToString() & " ; PaymentNatureCode: " & objICEmail.PaymentNatureCode.ToString() & " ; GroupCode: " & objICEmail.GroupCode.ToString() & " ; UserID: " & objICEmail.UserID.ToString() & " ; FileUploadTemplateCode: " & objICEmail.FileUploadTemplateCode.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Updated ", "EmailProcessing", objICEmail.EmailIDSettingsID.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")

            End If

        End Sub

        'Public Shared Sub DeleteEmailSettings(ByVal objICEmailSetting As ICEmailSettings, ByVal UserID As String, ByVal UserName As String)
        '    Dim objEmailForDelete As New ICEmailSettings
        '    Dim PrevAt As String = ""
        '    objEmailForDelete.es.Connection.CommandTimeout = 3600
        '    If objEmailForDelete.LoadByPrimaryKey(objICEmailSetting.EmailIDSettingsID) Then
        '        PrevAt = "EmailProcessing [Code:  " & objICEmailSetting.EmailIDSettingsID.ToString() & " ;  AccountNumber:  " & objICEmailSetting.AccountNumber.ToString() & " ; BranchCode:  " & objICEmailSetting.BranchCode.ToString() & " ; Currency: " & objICEmailSetting.Currency.ToString() & " ; PaymentNatureCode: " & objICEmailSetting.PaymentNatureCode.ToString() & " ; GroupCode: " & objICEmailSetting.GroupCode.ToString() & " ; UserID: " & objICEmailSetting.UserID.ToString() & " ; FileUploadTemplateCode: " & objICEmailSetting.FileUploadTemplateCode.ToString() & " ; IsApproved:  " & objICEmailSetting.IsApprove.ToString() & "]"
        '        objEmailForDelete.MarkAsDeleted()
        '        objEmailForDelete.Save()
        '        ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "EmailProcessing", objICEmailSetting.EmailIDSettingsID.ToString(), UserID.ToString(), UserName.ToString())
        '    End If

        'End Sub

        Public Shared Function GetEmailSettingsColl(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNCode As String, ByVal TempID As String, ByVal GroupCode As String) As ICEmailSettingsCollection

            Dim collEmail As New ICEmailSettingsCollection
            collEmail.Query.Where(collEmail.Query.AccountNumber = AccNo And collEmail.Query.BranchCode = BranchCode.ToString)
            collEmail.Query.Where(collEmail.Query.Currency = Currency And collEmail.Query.PaymentNatureCode = PayNCode.ToString)
            collEmail.Query.Where(collEmail.Query.FileUploadTemplateCode = TempID.ToString And collEmail.Query.GroupCode = GroupCode.ToString)
            collEmail.Query.Load()
            Return collEmail
        End Function
        Public Shared Function GetEmailSettings(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNCode As String, ByVal TempID As String, ByVal UserType As String) As DataTable

            Dim qryEmail As New ICEmailSettingsQuery("qryEmail")
            Dim qryUser As New ICUserQuery("qryUser")
            Dim dt As DataTable

            qryEmail.Select(qryEmail.EmailIDSettingsID, qryEmail.UserID)
            qryEmail.InnerJoin(qryUser).On(qryEmail.UserID = qryUser.UserID)
            qryEmail.Where(qryEmail.AccountNumber = AccNo And qryEmail.BranchCode = BranchCode.ToString)
            qryEmail.Where(qryEmail.Currency = Currency And qryEmail.PaymentNatureCode = PayNCode.ToString)
            qryEmail.Where(qryEmail.FileUploadTemplateCode = TempID.ToString)
            qryEmail.Where(qryUser.UserType = UserType)
            dt = qryEmail.LoadDataTable()
            Return dt

        End Function
        'Public Shared Sub GetgvAssignedUsers(ByVal UserType As String, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        '    Dim qryUsers As New ICUserQuery("usr")
        '    Dim qryEmail As New ICEmailSettingsQuery("email")
        '    Dim qryoffice As New ICOfficeQuery("qryoff")
        '    Dim dt As DataTable

        '    qryEmail.Select(qryEmail.EmailIDSettingsID, qryUsers.UserID, qryUsers.UserName)
        '    qryEmail.InnerJoin(qryUsers).On(qryEmail.UserID = qryUsers.UserID)
        '    qryEmail.InnerJoin(qryoffice).On(qryoffice.OfficeID = qryUsers.OfficeCode)

        '    qryEmail.Where(qryUsers.UserType = UserType)
        '    qryEmail.OrderBy(qryUsers.UserName.Ascending)

        '    dt = qryEmail.LoadDataTable
        '    rg.DataSource = dt

        '    If DoDataBind Then
        '        rg.DataBind()
        '    End If

        'End Sub
        'Public Shared Sub GetgvAssignedUsers(ByVal UserType As String, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal objICAPNatur As ICAccountsPaymentNature, ByVal GroupCode As String, ByVal FileUploadTemplateCode As String)

        '    Dim qryUsers As New ICUserQuery("usr")
        '    Dim qryEmail As New ICEmailSettingsQuery("email")
        '    Dim qryoffice As New ICOfficeQuery("qryoff")
        '    Dim dt As DataTable

        '    qryEmail.Select(qryEmail.EmailIDSettingsID, qryUsers.UserID, qryUsers.UserName)
        '    qryEmail.InnerJoin(qryUsers).On(qryEmail.UserID = qryUsers.UserID)
        '    qryEmail.InnerJoin(qryoffice).On(qryoffice.OfficeID = qryUsers.OfficeCode)
        '    qryEmail.Where(qryEmail.AccountNumber = objICAPNatur.AccountNumber And qryEmail.BranchCode = objICAPNatur.BranchCode And qryEmail.Currency = objICAPNatur.Currency And qryEmail.PaymentNatureCode = objICAPNatur.PaymentNatureCode And qryEmail.FileUploadTemplateCode = FileUploadTemplateCode)
        '    qryEmail.Where(qryUsers.UserType = UserType)
        '    qryEmail.OrderBy(qryUsers.UserName.Ascending)

        '    dt = qryEmail.LoadDataTable
        '    rg.DataSource = dt

        '    If DoDataBind Then
        '        rg.DataBind()
        '    End If

        'End Sub
        'Public Shared Sub GetgvAssignedUsers(ByVal UserType As String, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal objICAPNatur As ICAccountsPaymentNature, ByVal GroupCode As String, ByVal FileUploadTemplateCode As String)

        '    Dim qryUsers As New ICUserQuery("usr")
        '    Dim qryEmail As New ICEmailSettingsQuery("email")
        '    Dim qryoffice As New ICOfficeQuery("qryoff")
        '    Dim qryObjICEmailSettingCount As New ICEmailSettingsQuery("objICEmailSettingCount")
        '    Dim dt As DataTable

        '    qryEmail.Select(qryEmail.EmailIDSettingsID, qryUsers.UserID, qryUsers.UserName)
        '    qryEmail.Select(qryObjICEmailSettingCount.[Select](qryObjICEmailSettingCount.EmailIDSettingsID.Count()).Where(qryObjICEmailSettingCount.AccountNumber = qryEmail.AccountNumber And qryObjICEmailSettingCount.BranchCode = qryEmail.BranchCode And qryObjICEmailSettingCount.Currency = qryEmail.Currency And qryObjICEmailSettingCount.PaymentNatureCode = qryEmail.PaymentNatureCode And qryObjICEmailSettingCount.FileUploadTemplateCode = qryEmail.FileUploadTemplateCode).As("Count"))
        '    qryEmail.InnerJoin(qryUsers).On(qryEmail.UserID = qryUsers.UserID)
        '    qryEmail.InnerJoin(qryoffice).On(qryoffice.OfficeID = qryUsers.OfficeCode)
        '    qryEmail.Where(qryEmail.AccountNumber = objICAPNatur.AccountNumber And qryEmail.BranchCode = objICAPNatur.BranchCode And qryEmail.Currency = objICAPNatur.Currency And qryEmail.PaymentNatureCode = objICAPNatur.PaymentNatureCode And qryEmail.FileUploadTemplateCode = FileUploadTemplateCode)
        '    qryEmail.Where(qryUsers.UserType = UserType)
        '    qryEmail.GroupBy(qryEmail.EmailIDSettingsID, qryUsers.UserID, qryUsers.UserName, qryEmail.AccountNumber, qryEmail.BranchCode, qryEmail.Currency, qryEmail.PaymentNatureCode, qryEmail.FileUploadTemplateCode)
        '    qryEmail.OrderBy(qryUsers.UserName.Ascending)

        '    dt = qryEmail.LoadDataTable
        '    rg.DataSource = dt

        '    If DoDataBind = True Then
        '        rg.DataBind()
        '    End If

        'End Sub
        Public Shared Sub GetgvAssignedUsers(ByVal UserType As String, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal objICAPNatur As ICAccountsPaymentNature, ByVal GroupCode As String, ByVal FileUploadTemplateCode As String)

            Dim qryUsers As New ICUserQuery("usr")
            Dim qryEmail As New ICEmailSettingsQuery("email")
            'Dim qryoffice As New ICOfficeQuery("qryoff")
            Dim qryObjICEmailSettingCount As New ICEmailSettingsQuery("objICEmailSettingCount")
            Dim dt As DataTable

            qryEmail.Select(qryUsers.UserID, qryUsers.UserName, qryEmail.EmailIDSettingsID)
            qryEmail.Select(qryObjICEmailSettingCount.[Select](qryObjICEmailSettingCount.EmailIDSettingsID.Count()).InnerJoin(qryUsers).On(qryObjICEmailSettingCount.UserID = qryUsers.UserID).Where(qryObjICEmailSettingCount.AccountNumber = qryEmail.AccountNumber And qryObjICEmailSettingCount.BranchCode = qryEmail.BranchCode And qryObjICEmailSettingCount.Currency = qryEmail.Currency And qryObjICEmailSettingCount.PaymentNatureCode = qryEmail.PaymentNatureCode And qryObjICEmailSettingCount.FileUploadTemplateCode = qryEmail.FileUploadTemplateCode And qryUsers.UserType = UserType).As("Count"))
            qryEmail.InnerJoin(qryUsers).On(qryEmail.UserID = qryUsers.UserID)
            '   qryEmail.InnerJoin(qryoffice).On(qryoffice.OfficeID = qryUsers.OfficeCode)
            qryEmail.Where(qryEmail.AccountNumber = objICAPNatur.AccountNumber And qryEmail.BranchCode = objICAPNatur.BranchCode And qryEmail.Currency = objICAPNatur.Currency And qryEmail.PaymentNatureCode = objICAPNatur.PaymentNatureCode And qryEmail.FileUploadTemplateCode = FileUploadTemplateCode)
            qryEmail.Where(qryUsers.UserType = UserType)
            qryEmail.GroupBy(qryEmail.EmailIDSettingsID, qryUsers.UserID, qryUsers.UserName, qryEmail.AccountNumber, qryEmail.BranchCode, qryEmail.Currency, qryEmail.PaymentNatureCode, qryEmail.FileUploadTemplateCode)
            qryEmail.OrderBy(qryUsers.UserName.Ascending)

            dt = qryEmail.LoadDataTable
            rg.DataSource = dt

            If DoDataBind Then
                rg.DataBind()
            End If

        End Sub

        'Public Shared Sub GetEmailSettingTaggedUsersgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal Group As String, ByVal Company As String, ByVal AccPayNature As String, ByVal Template As String)

        '    Dim qryEmailSettings As New ICEmailSettingsQuery("qryEmailSettings")
        '    Dim qryCompany As New ICCompanyQuery("qryCompany")
        '    Dim qryGroup As New ICGroupQuery("qryGroup")
        '    Dim qryTemplate As New ICTemplateQuery("qryTemplate")
        '    Dim qryAccount As New ICAccountsQuery("qryAccPNature")
        '    Dim qryPayNature As New ICPaymentNatureQuery("qryPayNature")
        '    Dim dt As DataTable

        '    If Not pagenumber = 0 Then
        '        qryEmailSettings.Select(qryEmailSettings.AccountNumber.Distinct, qryEmailSettings.BranchCode.Distinct, qryEmailSettings.Currency.Distinct)
        '        qryEmailSettings.Select(qryEmailSettings.PaymentNatureCode.Distinct, qryEmailSettings.FileUploadTemplateCode.Distinct, qryTemplate.TemplateName.Distinct)
        '        qryEmailSettings.Select(qryPayNature.PaymentNatureName.Distinct, qryCompany.CompanyName.Distinct, qryGroup.GroupName.Distinct, qryAccount.CompanyCode.Distinct, qryEmailSettings.GroupCode.Distinct)

        '        qryEmailSettings.InnerJoin(qryPayNature).On(qryEmailSettings.PaymentNatureCode = qryPayNature.PaymentNatureCode)
        '        qryEmailSettings.InnerJoin(qryTemplate).On(qryEmailSettings.FileUploadTemplateCode = qryTemplate.TemplateID)
        '        qryEmailSettings.InnerJoin(qryAccount).On(qryEmailSettings.AccountNumber = qryAccount.AccountNumber And qryEmailSettings.BranchCode = qryAccount.BranchCode And qryEmailSettings.Currency = qryAccount.Currency)
        '        qryEmailSettings.InnerJoin(qryGroup).On(qryEmailSettings.GroupCode = qryGroup.GroupCode)
        '        qryEmailSettings.InnerJoin(qryCompany).On(qryGroup.GroupCode = qryCompany.GroupCode)

        '        qryEmailSettings.GroupBy(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency, qryEmailSettings.PaymentNatureCode, qryEmailSettings.FileUploadTemplateCode, qryTemplate.TemplateName, qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

        '        If Group.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryGroup.GroupCode = Group.ToString())
        '        End If

        '        If Company.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryCompany.CompanyCode = Company.ToString())
        '        End If


        '        If AccPayNature.ToString() <> "0" Then
        '           qryEmailSettings.Where(qryAccount.AccountNumber = AccPayNature.Split("-")(0).ToString() And qryAccount.BranchCode = AccPayNature.Split("-")(1).ToString() And qryAccount.Currency = AccPayNature.Split("-")(2).ToString())
        '        End If

        '        If Template.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryTemplate.TemplateID = Template.ToString())
        '        End If

        '        qryEmailSettings.OrderBy(qryGroup.GroupName.Ascending, qryCompany.CompanyName.Ascending)
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        qryEmailSettings.es.PageNumber = 1
        '        qryEmailSettings.es.PageSize = pagesize
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If


        '    End If
        'End Sub

        'Public Shared Sub GetEmailSettingTaggedUsersgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal Group As String, ByVal Company As String, ByVal AccPayNature As String, ByVal Template As String)

        '    Dim qryEmailSettings As New ICEmailSettingsQuery("qryEmailSettings")
        '    Dim qryCompany As New ICCompanyQuery("qryCompany")
        '    Dim qryGroup As New ICGroupQuery("qryGroup")
        '    Dim qryTemplate As New ICTemplateQuery("qryTemplate")
        '    Dim qryAccount As New ICAccountsQuery("qryAcc")
        '    Dim qryAccountPNature As New ICAccountsPaymentNatureQuery("qryAccPNature")
        '    Dim qryPayNature As New ICPaymentNatureQuery("qryPayNature")
        '    Dim qryObjICUser As New ICUserQuery("qryObjICUser")
        '    Dim dt As DataTable

        '    If Not pagenumber = 0 Then
        '        qryEmailSettings.Select(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency)
        '        qryEmailSettings.Select(qryEmailSettings.PaymentNatureCode, qryObjICUser.UserType, qryEmailSettings.FileUploadTemplateCode, qryTemplate.TemplateName)
        '        qryEmailSettings.Select(qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

        '        qryEmailSettings.InnerJoin(qryAccountPNature).On(qryEmailSettings.AccountNumber = qryAccountPNature.AccountNumber And qryEmailSettings.BranchCode = qryAccountPNature.BranchCode And qryEmailSettings.Currency = qryAccountPNature.Currency And qryEmailSettings.PaymentNatureCode = qryAccountPNature.PaymentNatureCode)
        '        qryEmailSettings.InnerJoin(qryPayNature).On(qryAccountPNature.PaymentNatureCode = qryPayNature.PaymentNatureCode)
        '        qryEmailSettings.InnerJoin(qryTemplate).On(qryEmailSettings.FileUploadTemplateCode = qryTemplate.TemplateID)
        '        qryEmailSettings.InnerJoin(qryAccount).On(qryAccountPNature.AccountNumber = qryAccount.AccountNumber And qryAccountPNature.BranchCode = qryAccount.BranchCode And qryAccountPNature.Currency = qryAccount.Currency)
        '        qryEmailSettings.InnerJoin(qryCompany).On(qryAccount.CompanyCode = qryCompany.CompanyCode)
        '        qryEmailSettings.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)
        '        qryEmailSettings.InnerJoin(qryObjICUser).On(qryEmailSettings.UserID = qryObjICUser.UserID)


        '        qryEmailSettings.GroupBy(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency, qryEmailSettings.PaymentNatureCode, qryEmailSettings.FileUploadTemplateCode, qryObjICUser.UserType, qryTemplate.TemplateName, qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

        '        If Group.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryGroup.GroupCode = Group.ToString())
        '        End If

        '        If Company.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryCompany.CompanyCode = Company.ToString())
        '        End If


        '        If AccPayNature.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryAccountPNature.AccountNumber = AccPayNature.Split("-")(0).ToString() And qryAccountPNature.BranchCode = AccPayNature.Split("-")(1).ToString() And qryAccountPNature.Currency = AccPayNature.Split("-")(2).ToString() And qryAccountPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString())
        '        End If

        '        If Template.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryTemplate.TemplateID = Template.ToString())
        '        End If

        '        qryEmailSettings.OrderBy(qryGroup.GroupName.Ascending)
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        qryEmailSettings.es.PageNumber = 1
        '        qryEmailSettings.es.PageSize = pagesize
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If


        '    End If
        'End Sub
        Public Shared Function GetICEmailSettingSingleObjectForUpdate(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal TemplateID As String) As ICEmailSettings
            Dim objICEmailSettingColl As New ICEmailSettingsCollection
            objICEmailSettingColl.es.Connection.CommandTimeout = 3600

            objICEmailSettingColl.Query.Where(objICEmailSettingColl.Query.AccountNumber = AccountNumber.ToString)
            objICEmailSettingColl.Query.Where(objICEmailSettingColl.Query.BranchCode = BranchCode.ToString)
            objICEmailSettingColl.Query.Where(objICEmailSettingColl.Query.Currency = Currency.ToString)
            objICEmailSettingColl.Query.Where(objICEmailSettingColl.Query.PaymentNatureCode = PaymentNatureCode.ToString)
            objICEmailSettingColl.Query.Where(objICEmailSettingColl.Query.FileUploadTemplateCode = TemplateID.ToString)

            objICEmailSettingColl.Query.Load()

            Return objICEmailSettingColl(0)

        End Function
        Public Shared Function GetICEmailSettingSingleObjectForUpdate(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal TemplateID As String, ByVal UserType As String) As Integer
            Dim qryobjICEmailSetting As New ICEmailSettingsQuery("qryobjICEmailSetting")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim EmailSettingID As Integer = 0
            Dim dt As New DataTable
            qryobjICEmailSetting.Select(qryobjICEmailSetting.EmailIDSettingsID)
            qryobjICEmailSetting.InnerJoin(qryObjICUser).On(qryobjICEmailSetting.UserID = qryObjICUser.UserID)
            qryobjICEmailSetting.Where(qryobjICEmailSetting.BranchCode = BranchCode.ToString)
            qryobjICEmailSetting.Where(qryobjICEmailSetting.Currency = Currency.ToString)
            qryobjICEmailSetting.Where(qryobjICEmailSetting.AccountNumber = AccountNumber.ToString)
            qryobjICEmailSetting.Where(qryObjICUser.UserType = UserType.ToString)
            qryobjICEmailSetting.Where(qryobjICEmailSetting.PaymentNatureCode = PaymentNatureCode)
            qryobjICEmailSetting.Where(qryobjICEmailSetting.FileUploadTemplateCode = TemplateID)
            qryobjICEmailSetting.OrderBy(qryobjICEmailSetting.EmailIDSettingsID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryobjICEmailSetting.LoadDataTable()
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    EmailSettingID = CInt(dr("EmailIDSettingsID"))
                    If Not EmailSettingID = 0 Then
                        Exit For
                    End If
                Next
            End If
            Return EmailSettingID
        End Function
        Public Shared Function GetEmailSettingsColl(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNCode As String, ByVal TempID As String) As ICEmailSettingsCollection

            Dim collEmail As New ICEmailSettingsCollection

            collEmail.Query.Where(collEmail.Query.AccountNumber = AccNo And collEmail.Query.BranchCode = BranchCode.ToString)
            collEmail.Query.Where(collEmail.Query.Currency = Currency And collEmail.Query.PaymentNatureCode = PayNCode.ToString)
            collEmail.Query.Where(collEmail.Query.FileUploadTemplateCode = TempID.ToString)

            collEmail.Query.Load()
            Return collEmail
        End Function


        Public Shared Sub GetEmailSettingTaggedUsersgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal Group As String, ByVal Company As String, ByVal AccPayNature As String, ByVal Template As String)

            Dim qryEmailSettings As New ICEmailSettingsQuery("qryEmailSettings")
            Dim qryCompany As New ICCompanyQuery("qryCompany")
            Dim qryGroup As New ICGroupQuery("qryGroup")
            Dim qryTemplate As New ICTemplateQuery("qryTemplate")
            Dim qryAccount As New ICAccountsQuery("qryAcc")
            Dim qryAccountPNature As New ICAccountsPaymentNatureQuery("qryAccPNature")
            Dim qryPayNature As New ICPaymentNatureQuery("qryPayNature")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim dt As DataTable

            If Not pagenumber = 0 Then
                qryEmailSettings.Select(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency)
                qryEmailSettings.Select(qryEmailSettings.PaymentNatureCode, qryObjICUser.UserType, qryEmailSettings.FileUploadTemplateCode, qryTemplate.TemplateName)
                qryEmailSettings.Select(qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

                qryEmailSettings.InnerJoin(qryAccountPNature).On(qryEmailSettings.AccountNumber = qryAccountPNature.AccountNumber And qryEmailSettings.BranchCode = qryAccountPNature.BranchCode And qryEmailSettings.Currency = qryAccountPNature.Currency And qryEmailSettings.PaymentNatureCode = qryAccountPNature.PaymentNatureCode)
                qryEmailSettings.InnerJoin(qryPayNature).On(qryAccountPNature.PaymentNatureCode = qryPayNature.PaymentNatureCode)
                qryEmailSettings.InnerJoin(qryTemplate).On(qryEmailSettings.FileUploadTemplateCode = qryTemplate.TemplateID)
                qryEmailSettings.InnerJoin(qryAccount).On(qryAccountPNature.AccountNumber = qryAccount.AccountNumber And qryAccountPNature.BranchCode = qryAccount.BranchCode And qryAccountPNature.Currency = qryAccount.Currency)
                qryEmailSettings.InnerJoin(qryCompany).On(qryAccount.CompanyCode = qryCompany.CompanyCode)
                qryEmailSettings.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)
                qryEmailSettings.InnerJoin(qryObjICUser).On(qryEmailSettings.UserID = qryObjICUser.UserID)

                qryEmailSettings.GroupBy(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency, qryEmailSettings.PaymentNatureCode, qryEmailSettings.FileUploadTemplateCode, qryObjICUser.UserType, qryTemplate.TemplateName, qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

                If Group.ToString() <> "0" Then
                    qryEmailSettings.Where(qryGroup.GroupCode = Group.ToString())
                End If

                If Company.ToString() <> "0" Then
                    qryEmailSettings.Where(qryCompany.CompanyCode = Company.ToString())
                End If


                If AccPayNature.ToString() <> "0" Then
                    qryEmailSettings.Where(qryAccountPNature.AccountNumber = AccPayNature.Split("-")(0).ToString() And qryAccountPNature.BranchCode = AccPayNature.Split("-")(1).ToString() And qryAccountPNature.Currency = AccPayNature.Split("-")(2).ToString() And qryAccountPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString())
                End If

                If Template.ToString() <> "0" Then
                    qryEmailSettings.Where(qryTemplate.TemplateID = Template.ToString())
                End If
                qryEmailSettings.Where(qryAccount.IsApproved = True And qryAccount.IsActive = True)
                qryEmailSettings.OrderBy(qryGroup.GroupName.Ascending)
                dt = qryEmailSettings.LoadDataTable()
                rg.DataSource = dt

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                qryEmailSettings.es.PageNumber = 1
                qryEmailSettings.es.PageSize = pagesize
                dt = qryEmailSettings.LoadDataTable()
                rg.DataSource = dt


                If DoDataBind Then
                    rg.DataBind()
                End If


            End If
        End Sub



        'Public Shared Sub GetEmailSettingTaggedUsersgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal Group As String, ByVal Company As String, ByVal AccPayNature As String, ByVal Template As String)

        '    Dim qryEmailSettings As New ICEmailSettingsQuery("qryEmailSettings")
        '    Dim qryCompany As New ICCompanyQuery("qryCompany")
        '    Dim qryGroup As New ICGroupQuery("qryGroup")
        '    Dim qryTemplate As New ICTemplateQuery("qryTemplate")
        '    Dim qryAccount As New ICAccountsQuery("qryAcc")
        '    Dim qryAccountPNature As New ICAccountsPaymentNatureQuery("qryAccPNature")
        '    Dim qryPayNature As New ICPaymentNatureQuery("qryPayNature")
        '    Dim dt As DataTable

        '    If Not pagenumber = 0 Then
        '        qryEmailSettings.Select(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency)
        '        qryEmailSettings.Select(qryEmailSettings.PaymentNatureCode, qryEmailSettings.FileUploadTemplateCode, qryTemplate.TemplateName)
        '        qryEmailSettings.Select(qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

        '        qryEmailSettings.InnerJoin(qryAccountPNature).On(qryEmailSettings.AccountNumber = qryAccountPNature.AccountNumber And qryEmailSettings.BranchCode = qryAccountPNature.BranchCode And qryEmailSettings.Currency = qryAccountPNature.Currency And qryEmailSettings.PaymentNatureCode = qryAccountPNature.PaymentNatureCode)
        '        qryEmailSettings.InnerJoin(qryPayNature).On(qryAccountPNature.PaymentNatureCode = qryPayNature.PaymentNatureCode)
        '        qryEmailSettings.InnerJoin(qryTemplate).On(qryEmailSettings.FileUploadTemplateCode = qryTemplate.TemplateID)
        '        qryEmailSettings.InnerJoin(qryAccount).On(qryAccountPNature.AccountNumber = qryAccount.AccountNumber And qryAccountPNature.BranchCode = qryAccount.BranchCode And qryAccountPNature.Currency = qryAccount.Currency)
        '        qryEmailSettings.InnerJoin(qryCompany).On(qryAccount.CompanyCode = qryCompany.CompanyCode)
        '        qryEmailSettings.InnerJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)


        '        qryEmailSettings.GroupBy(qryEmailSettings.AccountNumber, qryEmailSettings.BranchCode, qryEmailSettings.Currency, qryEmailSettings.PaymentNatureCode, qryEmailSettings.FileUploadTemplateCode, qryTemplate.TemplateName, qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryEmailSettings.GroupCode)

        '        If Group.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryGroup.GroupCode = Group.ToString())
        '        End If

        '        If Company.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryCompany.CompanyCode = Company.ToString())
        '        End If


        '        If AccPayNature.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryAccountPNature.AccountNumber = AccPayNature.Split("-")(0).ToString() And qryAccountPNature.BranchCode = AccPayNature.Split("-")(1).ToString() And qryAccountPNature.Currency = AccPayNature.Split("-")(2).ToString() And qryAccountPNature.PaymentNatureCode = AccPayNature.Split("-")(3).ToString())
        '        End If

        '        If Template.ToString() <> "0" Then
        '            qryEmailSettings.Where(qryTemplate.TemplateID = Template.ToString())
        '        End If

        '        qryEmailSettings.OrderBy(qryGroup.GroupName.Ascending)
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        qryEmailSettings.es.PageNumber = 1
        '        qryEmailSettings.es.PageSize = pagesize
        '        dt = qryEmailSettings.LoadDataTable()
        '        rg.DataSource = dt


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If


        '    End If
        'End Sub
        Public Shared Sub DeleteEmailSettings(ByVal objICEmailSetting As ICEmailSettings, ByVal UserID As String, ByVal UserName As String)
            Dim objEmailForDelete As New ICEmailSettings
            Dim PrevAt As String = ""
            objEmailForDelete.es.Connection.CommandTimeout = 3600
            If objEmailForDelete.LoadByPrimaryKey(objICEmailSetting.EmailIDSettingsID) Then
                PrevAt = "EmailProcessing [Code:  " & objICEmailSetting.EmailIDSettingsID.ToString() & " ;  AccountNumber:  " & objEmailForDelete.AccountNumber.ToString() & " ; BranchCode:  " & objEmailForDelete.BranchCode.ToString() & " ; Currency: " & objEmailForDelete.Currency.ToString() & " ; PaymentNatureCode: " & objEmailForDelete.PaymentNatureCode.ToString() & " ; GroupCode: " & objEmailForDelete.GroupCode.ToString() & " ; UserID: " & objEmailForDelete.UserID.ToString() & " ; FileUploadTemplateCode: " & objEmailForDelete.FileUploadTemplateCode.ToString() & " ; IsApproved:  " & objEmailForDelete.IsApprove.ToString() & "]"
                objEmailForDelete.MarkAsDeleted()
                objEmailForDelete.Save()
                ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "EmailProcessing", objICEmailSetting.EmailIDSettingsID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If

        End Sub
        Public Shared Sub DeleteEmailSettings(ByVal EmailSettingID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objEmailForDelete As New ICEmailSettings
            '  Dim prevobjEmailForDelete As New ICEmailSettings
            Dim Action As String = ""
            objEmailForDelete.es.Connection.CommandTimeout = 3600
            If objEmailForDelete.LoadByPrimaryKey(EmailSettingID) Then
                Action = "EmailProcessing [Code:  " & EmailSettingID.ToString() & " ;  AccountNumber:  " & objEmailForDelete.AccountNumber.ToString() & " ; BranchCode:  " & objEmailForDelete.BranchCode.ToString() & " ; Currency: " & objEmailForDelete.Currency.ToString() & " ; PaymentNatureCode: " & objEmailForDelete.PaymentNatureCode.ToString() & " ; GroupCode: " & objEmailForDelete.GroupCode.ToString() & " ; UserID: " & objEmailForDelete.UserID.ToString() & " ; FileUploadTemplateCode: " & objEmailForDelete.FileUploadTemplateCode.ToString() & " ; IsApproved:  " & objEmailForDelete.IsApprove.ToString() & "]"
                objEmailForDelete.MarkAsDeleted()
                objEmailForDelete.Save()
                ICUtilities.AddAuditTrail(Action & " Deleted ", "EmailProcessing", EmailSettingID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If

        End Sub
        Public Shared Function IsTemplateCodeTaggedWithUser(ByVal FileUploadTemplateCode As String, ByVal UserID As String) As Boolean
            Dim qryObjICEmailSetting As New ICEmailSettingsQuery("qryObjICEmailSetting")
            Dim qryObjICFUTemplate As New ICTemplateQuery("qryObjICFUTemplate")
            Dim Result As Boolean = False
            qryObjICEmailSetting.InnerJoin(qryObjICFUTemplate).On(qryObjICEmailSetting.FileUploadTemplateCode = qryObjICFUTemplate.TemplateID)
            qryObjICEmailSetting.Where(qryObjICFUTemplate.FileUploadTemplateCode = FileUploadTemplateCode And qryObjICEmailSetting.UserID = UserID)
            If qryObjICEmailSetting.LoadDataTable.Rows.Count > 0 Then
                Result = True
            End If


            Return Result



        End Function
       
#Region "Email and FTP Processing Work By Aizaz Ahemd [Dated: 12-Mar-2013]"
        Public Shared Function GetEmailSettingsDataTable() As DataTable
            Dim qryEmailSet As New ICEmailSettingsQuery("emailset")
            Dim qryUser As New ICUserQuery("usr")
            Dim qryTemplate As New ICTemplateQuery("tmp")
            Dim qryOffice As New ICOfficeQuery("ofc")
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")

            qryEmailSet.Select(qryEmailSet.UserID, qryUser.UserName, qryUser.Email, qryEmailSet.FileUploadTemplateCode.As("TemplateID"), qryTemplate.FileUploadTemplateCode.As("TemplateCode"), qryEmailSet.AccountNumber, qryEmailSet.BranchCode, qryEmailSet.Currency, qryEmailSet.PaymentNatureCode, qryCompany.CompanyCode, qryCompany.CompanyName, qryGroup.GroupCode, qryGroup.GroupName)
            qryEmailSet.LeftJoin(qryUser).On(qryEmailSet.UserID = qryUser.UserID)
            qryEmailSet.LeftJoin(qryTemplate).On(qryEmailSet.FileUploadTemplateCode = qryTemplate.TemplateID)
            qryEmailSet.LeftJoin(qryOffice).On(qryUser.OfficeCode = qryOffice.OfficeID)
            qryEmailSet.LeftJoin(qryCompany).On(qryOffice.CompanyCode = qryCompany.CompanyCode)
            qryEmailSet.LeftJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)







            qryEmailSet.OrderBy(qryEmailSet.UserID.Ascending)

            Return qryEmailSet.LoadDataTable()
        End Function

#End Region
    End Class
End Namespace
