﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICProvinceController
        Public Shared Function GetActiveandApproveProvincebyProvinceandCountryName(ByVal ProvinceName As String, ByVal CountryName As String) As String
            Dim qryProvince As New ICProvinceQuery("qryProvince")
            Dim qryCountry As New ICCountryQuery("qryCountry")
            Dim ProvinceCode As String = ""
            Dim dt As New DataTable
            qryProvince.Select(qryProvince.ProvinceCode)
            qryProvince.InnerJoin(qryCountry).On(qryProvince.CountryCode = qryCountry.CountryCode)
            qryProvince.Where(qryProvince.ProvinceName.ToLower = ProvinceName.ToLower.ToString())
            qryProvince.Where(qryCountry.CountryName.ToLower = CountryName.ToLower.ToString())
            qryProvince.Where(qryProvince.IsActive = True And qryProvince.IsApproved = True)
            dt = qryProvince.LoadDataTable()
            If dt.Rows.Count > 0 Then
                ProvinceCode = qryProvince.LoadDataTable.Rows(0)(0)
                If Not ProvinceCode.Trim = "" And Not ProvinceCode Is Nothing Then
                    ProvinceCode = ProvinceCode
                Else
                    ProvinceCode = ""
                End If
            Else
                ProvinceCode = ""
            End If
            Return ProvinceCode
        End Function


        Public Shared Sub AddProvince(ByVal cProvince As IC.ICProvince, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim ICProvince As New IC.ICProvince
            Dim prevICProvince As New IC.ICProvince
            Dim currentCountry As New ICCountry
            Dim previousCountry As New ICCountry

            Dim IsActiveText As String
            Dim IsApprovedText As String
            Dim CurrentAt As String
            Dim PrevAt As String

            ICProvince.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then
                ICProvince.CreateBy = cProvince.CreateBy
                ICProvince.CreateDate = cProvince.CreateDate
                ICProvince.Creater = cProvince.Creater
                ICProvince.CreationDate = cProvince.CreationDate
                currentCountry.LoadByPrimaryKey(cProvince.CountryCode.ToString())
            Else
                ICProvince.LoadByPrimaryKey(cProvince.ProvinceCode)
                prevICProvince.LoadByPrimaryKey(cProvince.ProvinceCode)
                currentCountry.LoadByPrimaryKey(ICProvince.CountryCode.ToString())
                previousCountry.LoadByPrimaryKey(prevICProvince.CountryCode.ToString())
                ICProvince.CreateBy = cProvince.CreateBy
                ICProvince.CreateDate = cProvince.CreateDate
            End If


            ICProvince.DisplayCode = cProvince.DisplayCode
            ICProvince.ProvinceCode = cProvince.ProvinceCode
            ICProvince.ProvinceName = cProvince.ProvinceName
            ICProvince.CountryCode = cProvince.CountryCode

            ICProvince.IsActive = cProvince.IsActive
            ICProvince.IsApproved = False

            If ICProvince.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If ICProvince.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            ICProvince.Save()


            If (isUpdate = False) Then
                CurrentAt = "Province :  [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & ICProvince.IsActive.ToString() & " ; IsApproved:  " & ICProvince.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else

                CurrentAt = "Province : Current Values [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                PrevAt = "<br />Province : Previous Values [Code:  " & prevICProvince.ProvinceCode.ToString() & " ; Name:  " & prevICProvince.ProvinceName.ToString() & " ; Country:  " & previousCountry.CountryName.ToString() & " ; IsActive:  " & prevICProvince.IsActive.ToString() & " ; IsApproved:  " & prevICProvince.IsApproved.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If


        End Sub
        'Public Shared Sub AddProvince(ByVal cProvince As IC.ICProvince, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

        '    Dim ICProvince As New IC.ICProvince
        '    Dim prevICProvince As New IC.ICProvince
        '    Dim currentCountry As New ICCountry
        '    Dim previousCountry As New ICCountry

        '    Dim IsActiveText As String
        '    Dim IsApprovedText As String
        '    Dim CurrentAt As String
        '    Dim PrevAt As String

        '    ICProvince.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then
        '        ICProvince.CreateBy = cProvince.CreateBy
        '        ICProvince.CreateDate = cProvince.CreateDate
        '        ICProvince.Creater = cProvince.Creater
        '        ICProvince.CreationDate = cProvince.CreationDate
        '        currentCountry.LoadByPrimaryKey(cProvince.CountryCode.ToString())
        '    Else
        '        ICProvince.LoadByPrimaryKey(cProvince.ProvinceCode)
        '        prevICProvince.LoadByPrimaryKey(cProvince.ProvinceCode)
        '        currentCountry.LoadByPrimaryKey(ICProvince.CountryCode.ToString())
        '        previousCountry.LoadByPrimaryKey(prevICProvince.CountryCode.ToString())
        '        ICProvince.CreateBy = cProvince.CreateBy
        '        ICProvince.CreateDate = cProvince.CreateDate
        '    End If



        '    ICProvince.ProvinceCode = cProvince.ProvinceCode
        '    ICProvince.ProvinceName = cProvince.ProvinceName
        '    ICProvince.CountryCode = cProvince.CountryCode

        '    ICProvince.IsActive = cProvince.IsActive
        '    ICProvince.IsApproved = False

        '    If ICProvince.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If ICProvince.IsApproved = True Then
        '        IsApprovedText = True
        '    Else
        '        IsApprovedText = False
        '    End If

        '    ICProvince.Save()


        '    If (isUpdate = False) Then
        '        CurrentAt = "Province :  [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & ICProvince.IsActive.ToString() & " ; IsApproved:  " & ICProvince.IsApproved.ToString() & "]"
        '        ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

        '    Else

        '        CurrentAt = "Province : Current Values [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
        '        PrevAt = "<br />Province : Previous Values [Code:  " & prevICProvince.ProvinceCode.ToString() & " ; Name:  " & prevICProvince.ProvinceName.ToString() & " ; Country:  " & previousCountry.CountryName.ToString() & " ; IsActive:  " & prevICProvince.IsActive.ToString() & " ; IsApproved:  " & prevICProvince.IsApproved.ToString() & "] "
        '        ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '    End If


        'End Sub
        Public Shared Function GetAllActiveAndApproveProvinces() As ICProvinceCollection


            Dim objICProvinceColl As New IC.ICProvinceCollection

            objICProvinceColl.es.Connection.CommandTimeout = 3600

            objICProvinceColl.Query.Where(objICProvinceColl.Query.IsActive = True And objICProvinceColl.Query.IsApproved = True)
            objICProvinceColl.Query.OrderBy(objICProvinceColl.Query.ProvinceName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICProvinceColl.Query.Load()
            Return objICProvinceColl

        End Function
        Public Shared Sub DeleteProvince(ByVal cProvinceCode As String, ByVal UserID As String, ByVal UserName As String)

            Dim ICProvince As New ICProvince
            Dim PrevAt As String
            Dim ProvinceName As String = ""
            Dim currentCountry As New ICCountry


            ICProvince.es.Connection.CommandTimeout = 3600
            ICProvince.LoadByPrimaryKey(cProvinceCode)
            currentCountry.LoadByPrimaryKey(ICProvince.CountryCode.ToString())

            PrevAt = "Province :  [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & ICProvince.IsActive.ToString() & " ; IsApproved:  " & ICProvince.IsApproved.ToString() & "]"

            ICProvince.MarkAsDeleted()
            ICProvince.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub
        Public Shared Function GetProvinceddl(ByVal CountryCode As String) As ICProvinceCollection

            Dim objICProvince As New ICProvinceCollection
            objICProvince.es.Connection.CommandTimeout = 3600
            objICProvince.LoadAll()
            objICProvince.Query.Where(objICProvince.Query.CountryCode = CountryCode)
            objICProvince.Query.OrderBy(objICProvince.Query.ProvinceName.Ascending)
            objICProvince.Query.Load()
            Return objICProvince
        End Function
        Public Shared Sub GetProvincegv(ByVal CountryCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


            Dim collProvince As New ICProvinceCollection
            Dim dt As New DataTable


            collProvince.Query.Select(collProvince.Query.CountryCode, collProvince.Query.ProvinceCode, collProvince.Query.ProvinceName, collProvince.Query.IsActive, collProvince.Query.IsApproved)
            collProvince.Query.Select(collProvince.Query.DisplayCode)
            collProvince.Query.Where(collProvince.Query.CountryCode = CountryCode.ToString())
            collProvince.Query.OrderBy(collProvince.Query.ProvinceName.Ascending)
            dt = collProvince.Query.LoadDataTable



            If Not pagenumber = 0 Then

                collProvince.Query.es.PageNumber = pagenumber
                collProvince.Query.es.PageSize = pagesize
                rg.DataSource = dt

                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else

                collProvince.Query.es.PageNumber = 1
                collProvince.Query.es.PageSize = pagesize

                rg.DataSource = dt


                If DoDataBind = True Then
                    rg.DataBind()
                End If

            End If

        End Sub
        'Public Shared Sub GetProvincegv(ByVal CountryCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


        '    Dim collProvince As New ICProvinceCollection
        '    Dim dt As New DataTable


        '    collProvince.Query.Select(collProvince.Query.CountryCode, collProvince.Query.ProvinceCode, collProvince.Query.ProvinceName, collProvince.Query.IsActive, collProvince.Query.IsApproved)
        '    collProvince.Query.Where(collProvince.Query.CountryCode = CountryCode.ToString())
        '    collProvince.Query.OrderBy(collProvince.Query.ProvinceName.Ascending)
        '    dt = collProvince.Query.LoadDataTable



        '    If Not pagenumber = 0 Then

        '        collProvince.Query.es.PageNumber = pagenumber
        '        collProvince.Query.es.PageSize = pagesize
        '        rg.DataSource = dt

        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If

        '    Else

        '        collProvince.Query.es.PageNumber = 1
        '        collProvince.Query.es.PageSize = pagesize

        '        rg.DataSource = dt


        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub
        Public Shared Sub ApproveProvince(ByVal ProvinceCode As String, ByVal isapproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim ICProvince As New IC.ICProvince
            Dim currentCountry As New ICCountry
            Dim CurrentAt As String
            ICProvince.es.Connection.CommandTimeout = 3600
            ICProvince.LoadByPrimaryKey(ProvinceCode)
            currentCountry.LoadByPrimaryKey(ICProvince.CountryCode.ToString())
            ICProvince.IsApproved = isapproved
            ICProvince.ApprovedBy = UserID
            ICProvince.ApprovedOn = Date.Now
            ICProvince.Save()

            CurrentAt = "Province :  [Code:  " & ICProvince.ProvinceCode.ToString() & " ; Name:  " & ICProvince.ProvinceName.ToString() & " ; Country:  " & currentCountry.CountryName.ToString() & " ; IsActive:  " & ICProvince.IsActive.ToString() & " ; IsApproved:  " & ICProvince.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Province", ICProvince.ProvinceCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")


        End Sub

        Public Shared Function GetActiveAndApproveProvinceByCountryCode(ByVal CountryCode As String) As IC.ICProvinceCollection

            Dim ICProvince As New IC.ICProvinceCollection
            ICProvince.es.Connection.CommandTimeout = 3600
            ICProvince.Query.Where(ICProvince.Query.CountryCode = CountryCode.ToString() And ICProvince.Query.IsActive = True And ICProvince.Query.IsApproved = True)
            ICProvince.Query.OrderBy(ICProvince.Query.ProvinceName.Ascending)
            ICProvince.LoadAll()
            Return ICProvince



        End Function

        Public Shared Function GetProvinceActiveAndApprove(ByVal CountryCode As String) As IC.ICProvinceCollection

            Dim ICProvince As New IC.ICProvinceCollection
            ICProvince.es.Connection.CommandTimeout = 3600
            ICProvince.Query.Where(ICProvince.Query.CountryCode = CountryCode.ToString() And ICProvince.Query.IsActive = True And ICProvince.Query.IsApproved = True)
            ICProvince.Query.OrderBy(ICProvince.Query.ProvinceName.Ascending)
            ICProvince.LoadAll()
            Return ICProvince

        End Function



        Public Shared Function GetPakistanProvinceActiveAndApprove() As DataTable
            Dim qryProvince As New ICProvinceQuery("prv")
            Dim qryCountry As New ICCountryQuery("ctr")

            qryProvince.Select(qryProvince.ProvinceCode, qryProvince.ProvinceName)
            qryProvince.InnerJoin(qryCountry).On(qryProvince.CountryCode = qryCountry.CountryCode)
            qryProvince.Where(qryProvince.IsApproved = True And qryProvince.IsActive = True And qryCountry.CountryName.LTrim.RTrim.Trim.ToLower = "pakistan")
            qryProvince.OrderBy(qryProvince.ProvinceName.Ascending)

            Return qryProvince.LoadDataTable()
        End Function

        Public Shared Function GetProvinceName(ByVal ProvinceCode As String) As String

            Dim ICProvince As New IC.ICProvince

            ICProvince.es.Connection.CommandTimeout = 3600

            If ProvinceCode Is Nothing Then
                Return Nothing
            Else
                ICProvince.LoadByPrimaryKey(ProvinceCode)

                If ICProvince.LoadByPrimaryKey(ProvinceCode) = True Then
                    Return ICProvince.ProvinceName.ToString()
                Else
                    If ProvinceCode.ToString() = "0" Then
                        Return ""
                    Else
                        Return ProvinceCode.ToString()
                    End If
                End If
            End If
        End Function
        ' Aizaz Ahmed [Dated: 12-Feb-2013]
        Public Shared Function IsProvinceExistByCountryNameOrCode(ByVal Province As String) As Boolean
            Dim objICProvince As New ICProvince
            objICProvince.es.Connection.CommandTimeout = 3600
            Dim result As Integer
            If Integer.TryParse(Province, result) = True Then
                objICProvince.Query.Where(objICProvince.Query.CountryCode = Province And objICProvince.Query.IsActive = True And objICProvince.Query.IsApproved = True)
                If objICProvince.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            Else
                objICProvince = New ICProvince
                objICProvince.Query.Where(objICProvince.Query.ProvinceName.ToLower = Province.ToLower.ToString() And objICProvince.Query.IsActive = True And objICProvince.Query.IsApproved = True)
                If objICProvince.Query.Load() Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function GetProvinceByCountryNameOrCode(ByVal Province As String) As ICProvince
            Dim objICProvince As New ICProvince
            objICProvince.es.Connection.CommandTimeout = 3600
            Dim result As Integer
            If Integer.TryParse(Province, result) = True Then
                objICProvince.Query.Where(objICProvince.Query.CountryCode = Province And objICProvince.Query.IsActive = True And objICProvince.Query.IsApproved = True)
                If objICProvince.Query.Load() Then
                    Return objICProvince
                Else
                    Return Nothing
                End If
            Else
                objICProvince = New ICProvince
                objICProvince.Query.Where(objICProvince.Query.ProvinceName.ToLower = Province.ToLower.ToString() And objICProvince.Query.IsActive = True And objICProvince.Query.IsApproved = True)
                If objICProvince.Query.Load() Then
                    Return objICProvince
                Else
                    Return Nothing
                End If
            End If
        End Function
        Public Shared Function IsProvinceExistByCountryNameOrCodeForBulkUpload() As DataTable
            Dim qryObjICProvince As New ICProvinceQuery("qryObjICProvince")
            qryObjICProvince.Select(qryObjICProvince.ProvinceCode, qryObjICProvince.ProvinceName.ToLower.Trim)
            qryObjICProvince.Where(qryObjICProvince.IsActive = True And qryObjICProvince.IsApproved = True)
            qryObjICProvince.OrderBy(qryObjICProvince.ProvinceCode.Ascending)
            Return qryObjICProvince.LoadDataTable

        End Function
    End Class



End Namespace
