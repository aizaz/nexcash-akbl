﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICDDPayableAccountsController
        Public Shared Sub AddDDPayableAccounts(ByVal DDPayableAccounts As ICDDPayableAccounts, ByVal UserID As String, ByVal UserName As String)

            Dim objDDPayableAccounts As New ICDDPayableAccounts
            Dim prevobjDDPayableAccounts As New ICDDPayableAccounts
            Dim CurrentAt As String


            objDDPayableAccounts.es.Connection.CommandTimeout = 3600

            objDDPayableAccounts.ProductTypeCode = DDPayableAccounts.ProductTypeCode
            objDDPayableAccounts.BankCode = DDPayableAccounts.BankCode
            objDDPayableAccounts.PayableAccountNumber = DDPayableAccounts.PayableAccountNumber
            objDDPayableAccounts.PayableAccountBranchCode = DDPayableAccounts.PayableAccountBranchCode
            objDDPayableAccounts.PayableAccountCurrency = DDPayableAccounts.PayableAccountCurrency
            objDDPayableAccounts.PayableAccountType = DDPayableAccounts.PayableAccountType
            objDDPayableAccounts.PayableAccountSeqNo = DDPayableAccounts.PayableAccountSeqNo
            objDDPayableAccounts.PayableAccountClientNo = DDPayableAccounts.PayableAccountClientNo
            objDDPayableAccounts.PayableAccountProfitCentre = DDPayableAccounts.PayableAccountProfitCentre
            objDDPayableAccounts.PreferenceOrder = DDPayableAccounts.PreferenceOrder

            objDDPayableAccounts.Save()


            CurrentAt = "DDPayable Accounts : [Code: " & objDDPayableAccounts.DDPayableAccountID.ToString() & " ; Product Type Code:  " & objDDPayableAccounts.ProductTypeCode.ToString() & " ; Bank Code:  " & objDDPayableAccounts.BankCode.ToString() & " ; Payable Account Number: " & objDDPayableAccounts.PayableAccountNumber.ToString() & " ; Payable Account Branch: " & objDDPayableAccounts.PayableAccountBranchCode.ToString() & " ; Payable Account Currency: " & objDDPayableAccounts.PayableAccountCurrency.ToString() & " ; Preference Order: " & objDDPayableAccounts.PreferenceOrder.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Added ", "DDPayableAccounts", DDPayableAccounts.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")


        End Sub

     
        Public Shared Function GetDDPayableAccountPreferenceOrder(ByVal ProductTypeCode As String) As Integer
            Dim qryDDPayableAccount As New ICDDPayableAccountsQuery("DDPayableAccount")
            Dim dt As New DataTable

            qryDDPayableAccount.Select(qryDDPayableAccount.PreferenceOrder.Max.Coalesce("'0'"))
            qryDDPayableAccount.Where(qryDDPayableAccount.ProductTypeCode = ProductTypeCode.ToString())

            dt = qryDDPayableAccount.LoadDataTable()
            If dt.Rows.Count > 0 Then
                Return CInt(dt.Rows(0)(0).ToString()) + 1
            Else
                Return 1
            End If
        End Function
       
        Public Shared Function GetAccountsgv(ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal ProductTypeCode As String)

            Dim qryDDPayableAccounts As New ICDDPayableAccountsQuery("ddacc")
            Dim qryDDPayableAccountsTopPOrder As New ICDDPayableAccountsQuery("qryDDPayableAccountsTopPOrder")
            Dim qryBank As New ICBankQuery("bnk")
            Dim dt As New DataTable
            qryDDPayableAccounts.Select(qryDDPayableAccounts.DDPayableAccountID, qryDDPayableAccounts.ProductTypeCode, qryDDPayableAccounts.PayableAccountNumber)
            qryDDPayableAccounts.Select(qryBank.BankName, qryDDPayableAccounts.PreferenceOrder)
            qryDDPayableAccounts.Select((qryDDPayableAccountsTopPOrder.[Select](qryDDPayableAccountsTopPOrder.PreferenceOrder.Count).Where(qryDDPayableAccountsTopPOrder.ProductTypeCode = qryDDPayableAccounts.ProductTypeCode).As("count")))
            qryDDPayableAccounts.InnerJoin(qryBank).On(qryDDPayableAccounts.BankCode = qryBank.BankCode)
            qryDDPayableAccounts.Where(qryDDPayableAccounts.ProductTypeCode = ProductTypeCode.ToString())
            qryDDPayableAccounts.OrderBy(qryDDPayableAccounts.PreferenceOrder.Ascending)

            rg.DataSource = qryDDPayableAccounts.LoadDataTable()

            If DoDataBind Then
                rg.DataBind()
            End If
        End Function

        Public Shared Sub DeleteDDPayableAccounts(ByVal DDPayableAccountID As String, ByVal UserID As String, ByVal UserName As String)
            Dim DDPayableAccounts As New ICDDPayableAccounts
            Dim PrevAt As String
            Dim PreferenceOrder As Integer = 0
            Dim ProductTypeCode As String = ""

            DDPayableAccounts.es.Connection.CommandTimeout = 3600
            DDPayableAccounts.LoadByPrimaryKey(DDPayableAccountID)
            PrevAt = "DDPayableAccounts : [Code:  " & DDPayableAccounts.DDPayableAccountID.ToString() & " ; BankCode:  " & DDPayableAccounts.BankCode.ToString() & " ; ProductTypeCode:  " & DDPayableAccounts.ProductTypeCode.ToString() & " ; PreferenceOrder :  " & DDPayableAccounts.PreferenceOrder.ToString() & "]"

            PreferenceOrder = DDPayableAccounts.PreferenceOrder
            ProductTypeCode = DDPayableAccounts.ProductTypeCode

            DDPayableAccounts.MarkAsDeleted()
            DDPayableAccounts.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "DDPayableAccounts", DDPayableAccounts.BankCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")


            Dim collDDPayableAccounts As New ICDDPayableAccountsCollection
            Dim objDDPayableAccounts As New ICDDPayableAccounts
            collDDPayableAccounts.Query.Where(collDDPayableAccounts.Query.ProductTypeCode = ProductTypeCode, collDDPayableAccounts.Query.PreferenceOrder > PreferenceOrder)
            If collDDPayableAccounts.Query.Load() Then
                For Each objDDPayableAccounts In collDDPayableAccounts
                    objDDPayableAccounts.PreferenceOrder = objDDPayableAccounts.PreferenceOrder - 1
                Next
                collDDPayableAccounts.Save()
            End If
        End Sub

      
        Public Shared Function GetAllDDPayAbleAccountsByProductTypeCode(ByVal ProductTypeCode As String) As ICDDPayableAccountsCollection
            Dim objICDDPayAbleAccountsColl As New ICDDPayableAccountsCollection
            objICDDPayAbleAccountsColl.es.Connection.CommandTimeout = 3600
            objICDDPayAbleAccountsColl.Query.Where(objICDDPayAbleAccountsColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objICDDPayAbleAccountsColl.Query.OrderBy(objICDDPayAbleAccountsColl.Query.DDPayableAccountID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICDDPayAbleAccountsColl.Query.Load()
            Return objICDDPayAbleAccountsColl
        End Function
        Public Shared Function GetDDPayableAccountsByProductTypeAndOfficeID(ByVal ProductTypeCode As String, ByVal OfficeID As String) As DataTable
            Dim dtAccountNumber As New DataTable
            Dim qryObjICDDPayableAccounts As New ICDDPayableAccountsQuery("qryObjICDDPayableAccounts")
            'Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")

            qryObjICDDPayableAccounts.Select(qryObjICDDPayableAccounts.PayableAccountNumber, qryObjICDDPayableAccounts.PayableAccountBranchCode, qryObjICDDPayableAccounts.PayableAccountCurrency)
            qryObjICDDPayableAccounts.Select(qryObjICDDPayableAccounts.PayableAccountType, qryObjICDDPayableAccounts.PayableAccountSeqNo, qryObjICDDPayableAccounts.PayableAccountProfitCentre, qryObjICDDPayableAccounts.PayableAccountClientNo)
            'qryObjICDDPayableAccounts.InnerJoin(qryObjICBank).On(qryObjICDDPayableAccounts.BankCode = qryObjICBank.BankCode)
            qryObjICDDPayableAccounts.InnerJoin(qryObjICOffice).On(qryObjICDDPayableAccounts.BankCode = qryObjICOffice.BankCode)
            qryObjICDDPayableAccounts.Where(qryObjICOffice.OfficeID = OfficeID And qryObjICOffice.OffceType = "Branch Office" And qryObjICDDPayableAccounts.ProductTypeCode = ProductTypeCode)
            dtAccountNumber = qryObjICDDPayableAccounts.LoadDataTable()
            Return dtAccountNumber
        End Function
        Public Shared Sub DeleteAllDDPayableAccountsByProductTypeCode(ByVal ProductTypeCode As String, ByVal DisbMode As String, ByVal UserID As String, ByVal UserName As String)
            Dim collDDPayableAccounts As New ICDDPayableAccountsCollection
            Dim DDPayableAccounts As New ICDDPayableAccounts

            Dim At As String = ""
            collDDPayableAccounts.Query.Where(collDDPayableAccounts.Query.ProductTypeCode = ProductTypeCode)
            If collDDPayableAccounts.Query.Load() Then

                'For Each DDPayableAccounts In collDDPayableAccounts
                '    DDPayableAccounts.MarkAsDeleted()
                '    At = "DDPayableAccounts : [Code:  " & DDPayableAccounts.DDPayableAccountID.ToString() & " ; BankCode:  " & DDPayableAccounts.BankCode.ToString() & " ; ProductTypeCode:  " & DDPayableAccounts.ProductTypeCode.ToString() & " ; PreferenceOrder :  " & DDPayableAccounts.PreferenceOrder.ToString() & "] These accounts delete due to the change of disbursement mode."
                '    ICUtilities.AddAuditTrail(At & " Deleted ", "DDPayableAccounts", DDPayableAccounts.DDPayableAccountID.ToString(), UserID.ToString(), UserName.ToString())
                'Next
                collDDPayableAccounts.MarkAllAsDeleted()
                collDDPayableAccounts.Save()

                At = "<br/>Product Type : [ All the accounts that belongs to this Product Type " & ProductTypeCode.ToString() & " has deleted due to the change of disbursement mode from Demand Draft to " & DisbMode.ToString() & " ]"
                ICUtilities.AddAuditTrail(At & "Deleted ", "Product Type", ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

            End If
        End Sub
        Public Shared Sub DeleteAllDDPayableAccountsByProductTypeCode(ByVal ProductTypeCode As String, ByVal DisbMode As String, ByVal EventOccured As String, ByVal UserID As String, ByVal UserName As String)
            Dim collDDPayableAccounts As New ICDDPayableAccountsCollection
            Dim DDPayableAccounts As New ICDDPayableAccounts

            Dim At As String = ""
            collDDPayableAccounts.Query.Where(collDDPayableAccounts.Query.ProductTypeCode = ProductTypeCode)
            If collDDPayableAccounts.Query.Load() Then
                collDDPayableAccounts.MarkAllAsDeleted()
                collDDPayableAccounts.Save()

                If EventOccured.ToString() = "Cancel" Then
                    At = "<br/>Product Type : [ All the accounts that belongs to this Product Type " & ProductTypeCode.ToString() & " has deleted due to the Cancel of Product Type ]"
                    ICUtilities.AddAuditTrail(At & "Deleted ", "Product Type", ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
                Else
                    At = "<br/>Product Type : [ All the accounts that belongs to this Product Type " & ProductTypeCode.ToString() & " has deleted due to the change of disbursement mode from Demand Draft to " & DisbMode.ToString() & " ]"
                    ICUtilities.AddAuditTrail(At & "Deleted ", "Product Type", ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
                End If
            End If
        End Sub
        Public Shared Function CompareDDPayableAccountsForCancellation(ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As Boolean
            Dim objICDDPayableAccountsColl As New ICDDPayableAccountsCollection
            objICDDPayableAccountsColl.Query.Where(objICDDPayableAccountsColl.Query.PayableAccountNumber = AccountNo And objICDDPayableAccountsColl.Query.PayableAccountBranchCode = BranchCode And objICDDPayableAccountsColl.Query.PayableAccountCurrency = Currency)
            If objICDDPayableAccountsColl.Query.Load Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace
