﻿Namespace IC
    Public Class ICInstructionApprovalRuleController
        Public Shared Function IsInstructionVerifyApprovalRules(ByVal objICInstruction As ICInstruction, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim Result As Boolean = False
            Dim ActionString As String = ""
            Dim objICAppruleCollection As New ICApprovalRuleCollection



            ''Get All Active Approval Rules againts Payment Nature

            objICAppruleCollection = GetAllApprovalRulesByPaymentNature(objICInstruction.PaymentNatureCode.ToString, objICInstruction.Amount, objICInstruction.CompanyCode)
            If objICAppruleCollection.Count = 0 Then
                ActionString = "Instruction Skip:Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped client approval"
                ActionString += "due to no approval rule found. Action was taken by user [ " & UserID & " ] [ " & UserName & " ] at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(ActionString, "Instruction", objICInstruction.InstructionID.ToString, UserID, UserName, "UPDATE")
                Return Result
                Exit Function
            End If

            ''Note / maintain approval log

            LogInstructionApprovalActivity(objICInstruction.InstructionID.ToString, UserID, UserName, False)

            

            ''Loop over approval rules
            For Each objICApprovalRule As ICApprovalRule In objICAppruleCollection
                If IsApprovalRuleVerified(objICApprovalRule.ApprovalRuleID, UserID, UserName, objICInstruction.InstructionID) = True Then
                    ActionString = "Instruction passed approval rule [ " & objICApprovalRule.ApprovalRuleID & " ] [ " & objICApprovalRule.ApprovalRuleName & " ]"
                    ActionString += ". Action was taken by user [ " & UserID & " ] [ " & UserName & " ] at [ " & Date.Now & " ]."
                    ICUtilities.AddAuditTrail(ActionString, "Instruction", objICInstruction.InstructionID.ToString, UserID, UserName, "UPDATE")
                    Result = True
                    Return Result
                    Exit For
                    Exit Function
                End If
            Next
            Return Result
        End Function

        Public Shared Function IsApprovalRuleVerified(ByVal ApprovalRuleID As String, ByVal UserID As String, ByVal UserName As String, ByVal InstructionID As String) As Boolean
            Dim ResultAnd As Boolean = False
            Dim ResultOr As Boolean = True
            Dim CollobjICApprovalRuleCond As ICApprovalRuleConditionsCollection
            CollobjICApprovalRuleCond = New ICApprovalRuleConditionsCollection
            ''Verify all And conditions
            CollobjICApprovalRuleCond = GetApprovalRuleConditionByApprovalRuleIDandType(ApprovalRuleID, "And")

            ''Loop Over and Conditions

            For Each objICApprovalRuleCond As ICApprovalRuleConditions In CollobjICApprovalRuleCond
                If IsConditionIsVerified(objICApprovalRuleCond.ApprovalRuleConditionID, UserID, UserName, InstructionID) = False Then
                    ResultAnd = False
                    Return ResultAnd
                    Exit For
                    Exit Function
                Else
                    ResultAnd = True
                End If
            Next

            If CollobjICApprovalRuleCond.Count = 0 Then
                ResultAnd = True
            End If

            ''Verify all or conditions
            CollobjICApprovalRuleCond = New ICApprovalRuleConditionsCollection
            CollobjICApprovalRuleCond = GetApprovalRuleConditionByApprovalRuleIDandType(ApprovalRuleID, "Or")
            If CollobjICApprovalRuleCond.Count > 0 Then
                ResultOr = False
            End If
            For Each objICApprovalRuleCond As ICApprovalRuleConditions In CollobjICApprovalRuleCond
                If IsConditionIsVerified(objICApprovalRuleCond.ApprovalRuleConditionID, UserID, UserName, InstructionID) = True Then
                    ResultOr = True
                    Exit For
                End If
            Next
            If ResultAnd = True And ResultOr = True Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsConditionIsVerified(ByVal ApprovalRuleConditionID As String, ByVal UserID As String, ByVal UserName As String, ByVal InstructionID As String) As Boolean
            Dim Result As Boolean = True
            Dim objICApprovalRuleCond As New ICApprovalRuleConditions
            Dim ApprovalCount As Integer = 0
            Dim dt As New DataTable
            Dim UserCount As Integer = 0

            ''Load condition object
            objICApprovalRuleCond.LoadByPrimaryKey(ApprovalRuleConditionID)

            ''Get condition users
            dt = ICApprovalRuleConditionUserController.GetApprovalRuleCondUSersByApprovalRuleCondIDAndAppGroupID(ApprovalRuleConditionID, objICApprovalRuleCond.ConditionType, objICApprovalRuleCond.SelectionCriteria, objICApprovalRuleCond.ApprovalGroupID)

            If objICApprovalRuleCond.SelectionCriteria = "All" Then
                For Each dr As DataRow In dt.Rows
                    If IsApprovalIsGivenByCurrentUser(InstructionID, dr("UserID")) = False Then
                        Result = False
                        Exit For
                        Return Result
                        Exit Function
                    End If
                Next
            ElseIf objICApprovalRuleCond.SelectionCriteria = "Specific" Then
                For Each dr As DataRow In dt.Rows
                    If IsApprovalIsGivenByCurrentUser(InstructionID, dr("UserID")) = False Then
                        Result = False
                        Exit For
                        Return Result
                        Exit Function
                    End If
                Next
            ElseIf objICApprovalRuleCond.SelectionCriteria = "Any" Then
                UserCount = objICApprovalRuleCond.ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID(0).UserCount
                For Each dr As DataRow In dt.Rows
                    If IsApprovalIsGivenByCurrentUser(InstructionID, dr("UserID")) = True Then
                        ApprovalCount = ApprovalCount + 1
                        If ApprovalCount = UserCount Then
                            Result = True
                            Exit For
                            Return Result
                            Exit Function

                        End If
                    End If
                Next
                If ApprovalCount = UserCount Then
                    Result = True
                Else
                    Result = False
                End If
            End If
            Return Result
        End Function

        Public Shared Sub LogInstructionApprovalActivity(ByVal InstructionID As String, ByVal UserID As String, ByVal UserName As String, ByVal IsAppVoid As Boolean)
            Dim objICApprovalLog As New ICInstructionApprovalLog
            Dim ActionString As String = ""


            If IsAppVoid = True Then
                ActionString = "Approval is void by user [ " & UserID & " ] [ " & UserName & " ] for instruction with ID [ " & InstructionID & " ] at [ " & Date.Now & " ]"
            Else
                ActionString = "Approval is given by user [ " & UserID & " ] [ " & UserName & " ] for instruction with ID [ " & InstructionID & " ] at [ " & Date.Now & " ]"
            End If
            objICApprovalLog.InstructionID = InstructionID
            objICApprovalLog.IsAprrovalVoid = IsAppVoid
            objICApprovalLog.ApprovalDateTime = Date.Now
            objICApprovalLog.UserID = UserID
            objICApprovalLog.Action = ActionString
            objICApprovalLog.Save()
            'ICUtilities.AddAuditTrail(ActionString, "Instruction Approval Log", objICApprovalLog.UserApprovalLogID, UserID, UserName, "ADD")
            ICUtilities.AddAuditTrail(ActionString, "Instruction", InstructionID, UserID, UserName, "ADD")
        End Sub
        Public Shared Sub MarkAllApprovalsVoidByInstructionID(ByVal InstructionID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICApprovalLog As New ICInstructionApprovalLogCollection
            Dim ActionString As String = ""


            objICApprovalLog.Query.Where(objICApprovalLog.Query.InstructionID = InstructionID)
            objICApprovalLog.Query.OrderBy(objICApprovalLog.Query.UserApprovalLogID.Ascending)
            objICApprovalLog.Query.Load()

            For Each objICApprova As ICInstructionApprovalLog In objICApprovalLog
                ActionString = ""
                ActionString = "Approval is void by user [ " & UserID & " ] [ " & UserName & " ] for instruction with ID [ " & InstructionID & " ] at [ " & Date.Now & " ]"
                ActionString += " Assigned approval number was [ " & objICApprova.UserApprovalLogID & " ]"
                objICApprova.IsAprrovalVoid = True
                objICApprova.Save()
                ICUtilities.AddAuditTrail(ActionString, "Instruction Approval Log", objICApprova.UserApprovalLogID, UserID, UserName, "UPDATE")
            Next







        End Sub

#Region "Approval Queue"

        Public Shared Function GetTaggedPaymentNatureWithUser(ByVal ArrayListRoleID As ArrayList, ByVal UserID As String) As DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICUserRolesAPNatureAndLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNatureAndLocation")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As DataTable
            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICUserRolesAPNatureAndLocation).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICUserRolesAPNatureAndLocation.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICUserRolesAPNatureAndLocation.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICUserRolesAPNatureAndLocation.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICUserRolesAPNatureAndLocation.PaymentNatureCode And qryObjICUserRolesAPNature.UserID = qryObjICUserRolesAPNatureAndLocation.UserID And qryObjICUserRolesAPNature.RolesID = qryObjICUserRolesAPNatureAndLocation.RoleID)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True And qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayListRoleID))
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.GroupBy(qryObjICUserRolesAPNature.AccountNumber, qryObjICUserRolesAPNature.BranchCode, qryObjICUserRolesAPNature.Currency, qryObjICUserRolesAPNature.PaymentNatureCode, qryObjICUserRolesAPNatureAndLocation.OfficeID)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetApprovalRulesForUserVIAPaymentNature(ByVal UserID As String, ByVal PaymentNatureCode As DataTable) As ArrayList
            Dim qryObjICApprovalRule As New ICApprovalRuleQuery("qryObjICApprovalRule")
            Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
            Dim qryObjICApprovalRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICApprovalRuleCondUsers")
            Dim qryObjICApprovalGroupUsers As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUsers")
            Dim ApprovalRuleIDList As New ArrayList
            Dim PaymentNatureCodeList As New ArrayList
            PaymentNatureCodeList.Add("-1")

            For Each dr As DataRow In PaymentNatureCode.Rows
                PaymentNatureCodeList.Add(dr("PaymentNatureCode"))
            Next

            qryObjICApprovalRule.Select(qryObjICApprovalRule.ApprovalRuleID)
            qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICApprovalRule.ApprovalRuleID = qryObjICApprovalRuleCond.ApprovalRuleID)
            qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCondUsers).On(qryObjICApprovalRuleCond.ApprovalRuleConditionID = qryObjICApprovalRuleCondUsers.ApprovaRuleCondID)
            qryObjICApprovalRule.InnerJoin(qryObjICApprovalGroupUsers).On(qryObjICApprovalRuleCond.ApprovalGroupID = qryObjICApprovalGroupUsers.ApprovalGroupID)
            qryObjICApprovalRule.Where(qryObjICApprovalRule.PaymentNatureCode.In(PaymentNatureCodeList) And qryObjICApprovalRule.IsActive = True)
            qryObjICApprovalRule.Where(qryObjICApprovalRuleCondUsers.UserID = UserID)
            qryObjICApprovalRule.es.Distinct = True
            For Each dr2 As DataRow In qryObjICApprovalRule.LoadDataTable.Rows
                ApprovalRuleIDList.Add(dr2("ApprovalRuleID"))
            Next


            Return ApprovalRuleIDList

        End Function

        Public Shared Function GetApprovalRulesMinOrMaxAmount(ByVal ApprovalID As ArrayList, ByVal MinOrMax As String) As String
            Dim qryObjICApprovalRule As New ICApprovalRuleQuery("qryObjICApprovalRule")

            If MinOrMax = "Min" Then
                qryObjICApprovalRule.Select(qryObjICApprovalRule.FromAmount.Min)
            Else
                qryObjICApprovalRule.Select(qryObjICApprovalRule.ToAmount.Max)
            End If

            qryObjICApprovalRule.Where(qryObjICApprovalRule.ApprovalRuleID.In(ApprovalID) And qryObjICApprovalRule.IsActive = True)


            Return qryObjICApprovalRule.LoadDataTable.Rows(0)(0).ToString
        End Function
        'Public Shared Function GetAllApprovalRulesByPaymentNature(ByVal PaymentNatureCode As String) As ICApprovalRuleCollection
        '    Dim collObjICAppRule As New ICApprovalRuleCollection
        '    collObjICAppRule.Query.Where(collObjICAppRule.Query.PaymentNatureCode = PaymentNatureCode And collObjICAppRule.Query.IsActive = True)
        '    collObjICAppRule.Query.OrderBy(collObjICAppRule.Query.ApprovalRuleID.Ascending)
        '    collObjICAppRule.Query.Load()
        '    Return collObjICAppRule
        'End Function
        'Public Shared Function GetAllApprovalRulesByPaymentNature(ByVal PaymentNatureCode As String, ByVal Amount As Double, ByVal CompanyCode As String) As ICApprovalRuleCollection
        '    Dim collObjICAppRule As New ICApprovalRuleCollection
        '    collObjICAppRule.Query.Where(collObjICAppRule.Query.PaymentNatureCode = PaymentNatureCode And collObjICAppRule.Query.IsActive = True)
        '    collObjICAppRule.Query.Where(collObjICAppRule.Query.FromAmount >= Amount And Amount <= collObjICAppRule.Query.ToAmount And collObjICAppRule.Query.CompanyCode = CompanyCode)
        '    collObjICAppRule.Query.OrderBy(collObjICAppRule.Query.ApprovalRuleID.Ascending)
        '    collObjICAppRule.Query.Load()
        '    Return collObjICAppRule
        'End Function
        Public Shared Function GetAllApprovalRulesByPaymentNature(ByVal PaymentNatureCode As String, ByVal Amount As Double, ByVal CompanyCode As String) As ICApprovalRuleCollection
            Dim collObjICAppRule As New ICApprovalRuleCollection
            collObjICAppRule.Query.Where(collObjICAppRule.Query.PaymentNatureCode = PaymentNatureCode And collObjICAppRule.Query.IsActive = True)
            collObjICAppRule.Query.Where(collObjICAppRule.Query.FromAmount <= Amount And collObjICAppRule.Query.ToAmount >= Amount And collObjICAppRule.Query.CompanyCode = CompanyCode)
            collObjICAppRule.Query.OrderBy(collObjICAppRule.Query.ApprovalRuleID.Ascending)
            collObjICAppRule.Query.Load()
            Return collObjICAppRule
        End Function
        Public Shared Function GetApprovalRuleConditionByApprovalRuleIDandType(ByVal ApprovalRuleID As String, ByVal ConditionType As String) As ICApprovalRuleConditionsCollection
            Dim CollObjICApprovalRuleCond As New ICApprovalRuleConditionsCollection



            CollObjICApprovalRuleCond.Query.Where(CollObjICApprovalRuleCond.Query.ApprovalRuleID = ApprovalRuleID And CollObjICApprovalRuleCond.Query.ConditionType = ConditionType)
            CollObjICApprovalRuleCond.Query.OrderBy(CollObjICApprovalRuleCond.Query.ApprovalRuleConditionID.Ascending)
            CollObjICApprovalRuleCond.Query.Load()

            Return CollObjICApprovalRuleCond
        End Function





        Private Function GetApprovalRuleConditionUsersByCondTypeAndSelectionAndApprovalGroup(ByVal ApprovalRuleID As String, ByVal ConditionType As String, ByVal SelectionCriteria As String, ByVal ApprovalGroupID As String) As DataTable
            Dim qryObjICAppRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppRuleCondUsers")
            Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")

            qryObjICAppRuleCondUsers.Select(qryObjICAppRuleCondUsers.UserID, qryObjICAppRuleCondUsers.UserCount)

            qryObjICAppRuleCondUsers.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICAppRuleCondUsers.ApprovaRuleCondID = qryObjICApprovalRuleCond.ApprovalRuleConditionID)
            qryObjICAppRuleCondUsers.Where(qryObjICApprovalRuleCond.ApprovalRuleID = ApprovalRuleID And qryObjICApprovalRuleCond.ConditionType = ConditionType)
            qryObjICAppRuleCondUsers.Where(qryObjICApprovalRuleCond.SelectionCriteria = SelectionCriteria And qryObjICApprovalRuleCond.ApprovalGroupID = ApprovalGroupID)
            qryObjICAppRuleCondUsers.OrderBy(qryObjICAppRuleCondUsers.ApprovalRuleConditionUserID.Ascending)

            Return qryObjICAppRuleCondUsers.LoadDataTable



        End Function

        Private Function GetInstructionApprovalUsersCountByApprovalGroupAndInstructionID(ByVal ApprovalGroupID As String, ByVal InstructionID As String, ByVal ApprovalRuleCondID As String, ByVal ConditionType As String, ByVal SelectionCriteria As String) As Integer
            Dim qryObjICInstrutionApproval As New ICInstructionApprovalLogQuery("qryObjICInstrutionApproval")
            Dim qryObjICAppruleConditionUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppruleConditionUsers")
            Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")


            qryObjICInstrutionApproval.Select(qryObjICInstrutionApproval.UserID.Count)
            qryObjICInstrutionApproval.InnerJoin(qryObjICAppruleConditionUsers).On(qryObjICInstrutionApproval.UserID = qryObjICAppruleConditionUsers.UserID)
            qryObjICInstrutionApproval.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICAppruleConditionUsers.ApprovaRuleCondID = qryObjICApprovalRuleCond.ApprovalRuleConditionID)
            qryObjICInstrutionApproval.Where(qryObjICApprovalRuleCond.ApprovalGroupID = ApprovalGroupID And qryObjICApprovalRuleCond.ApprovalRuleConditionID = ApprovalRuleCondID)
            qryObjICInstrutionApproval.Where(qryObjICApprovalRuleCond.ConditionType = ConditionType And qryObjICApprovalRuleCond.SelectionCriteria = SelectionCriteria)
            qryObjICInstrutionApproval.Where(qryObjICInstrutionApproval.InstructionID = InstructionID And qryObjICInstrutionApproval.IsAprrovalVoid = False)
            qryObjICInstrutionApproval.GroupBy(qryObjICInstrutionApproval.InstructionID)
            Return qryObjICInstrutionApproval.LoadDataTable.Rows(0)(0)


        End Function
        Private Function GetInstructionApprovalUsersByApprovalGroupAndInstructionID(ByVal ApprovalGroupID As String, ByVal InstructionID As String, ByVal ApprovalRuleCondID As String, ByVal ConditionType As String, ByVal SelectionCriteria As String) As DataTable
            Dim qryObjICInstrutionApproval As New ICInstructionApprovalLogQuery("qryObjICInstrutionApproval")
            Dim qryObjICAppruleConditionUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppruleConditionUsers")
            Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")


            qryObjICInstrutionApproval.Select(qryObjICInstrutionApproval.UserID)
            qryObjICInstrutionApproval.InnerJoin(qryObjICAppruleConditionUsers).On(qryObjICInstrutionApproval.UserID = qryObjICAppruleConditionUsers.UserID)
            qryObjICInstrutionApproval.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICAppruleConditionUsers.ApprovaRuleCondID = qryObjICApprovalRuleCond.ApprovalRuleConditionID)
            qryObjICInstrutionApproval.Where(qryObjICApprovalRuleCond.ApprovalGroupID = ApprovalGroupID And qryObjICApprovalRuleCond.ApprovalRuleConditionID = ApprovalRuleCondID)
            qryObjICInstrutionApproval.Where(qryObjICApprovalRuleCond.ConditionType = ConditionType And qryObjICApprovalRuleCond.SelectionCriteria = SelectionCriteria)
            qryObjICInstrutionApproval.Where(qryObjICInstrutionApproval.InstructionID = InstructionID And qryObjICInstrutionApproval.IsAprrovalVoid = False)
            qryObjICInstrutionApproval.GroupBy(qryObjICInstrutionApproval.InstructionID)
            Return qryObjICInstrutionApproval.LoadDataTable
        End Function
        Public Shared Function IsApprovalIsGivenByCurrentUser(ByVal InstructionID As String, ByVal UserID As String) As Boolean
            Dim qryObjICInstrutionApproval As New ICInstructionApprovalLogQuery("qryObjICInstrutionApproval")
            qryObjICInstrutionApproval.Where(qryObjICInstrutionApproval.InstructionID = InstructionID And qryObjICInstrutionApproval.UserID = UserID And qryObjICInstrutionApproval.IsAprrovalVoid = False)

            If qryObjICInstrutionApproval.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

#End Region
    End Class
End Namespace
