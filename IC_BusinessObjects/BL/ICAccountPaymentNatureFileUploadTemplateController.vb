﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICAccountPaymentNatureFileUploadTemplateController
        Public Shared Sub GetAllAccountPaymentNatureFileUploadTemplateForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As radgrid)
            Dim qryObjICAPNFTemplate As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAPNFTemplate")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccountPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryFileUploadTemplate As New ICTemplateQuery("qryFileUploadTemplate")
            Dim dt As New DataTable
            qryObjICAPNFTemplate.Select(qryObjICAPNFTemplate.AccountNumber, qryObjICPaymentNature.PaymentNatureName, qryObjICCompany.CompanyName, qryFileUploadTemplate.TemplateName)
            qryObjICAPNFTemplate.Select(qryObjICAPNFTemplate.TemplateID, qryObjICAPNFTemplate.BranchCode, qryObjICAPNFTemplate.Currency, qryObjICAPNFTemplate.PaymentNatureCode)
            qryObjICAPNFTemplate.InnerJoin(qryFileUploadTemplate).On(qryObjICAPNFTemplate.TemplateID = qryFileUploadTemplate.TemplateID)
            qryObjICAPNFTemplate.InnerJoin(qryObjICAccountPaymentNature).On(qryObjICAPNFTemplate.PaymentNatureCode = qryObjICAccountPaymentNature.PaymentNatureCode And qryObjICAPNFTemplate.AccountNumber = qryObjICAccountPaymentNature.AccountNumber And qryObjICAPNFTemplate.BranchCode = qryObjICAccountPaymentNature.BranchCode And qryObjICAPNFTemplate.Currency = qryObjICAccountPaymentNature.Currency)
            qryObjICAPNFTemplate.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNFTemplate.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAPNFTemplate.InnerJoin(qryObjICAccounts).On(qryObjICAccountPaymentNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPaymentNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPaymentNature.Currency = qryObjICAccounts.Currency)
            qryObjICAPNFTemplate.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAPNFTemplate.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)

            If Not CompanyCode.ToString = "" Then
                qryObjICAPNFTemplate.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If
            If Not GroupCode.ToString = "" Then
                qryObjICAPNFTemplate.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            qryObjICAPNFTemplate.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICAPNFTemplate.OrderBy(qryObjICCompany.CompanyName.Descending, qryObjICAPNFTemplate.AccountNumber.Descending, qryObjICPaymentNature.PaymentNatureName.Descending, qryFileUploadTemplate.TemplateName.Descending)
            dt = qryObjICAPNFTemplate.LoadDataTable()
            If Not PageNumber = 0 Then
                qryObjICAPNFTemplate.es.PageNumber = PageNumber
                qryObjICAPNFTemplate.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAPNFTemplate.es.PageNumber = 1
                qryObjICAPNFTemplate.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub AddAccountPaymentNatureFileUpload(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNFUTemplateForSave As New ICAccountPayNatureFileUploadTemplate
            Dim objICAPNFUTemplateFrom As New ICAccountPayNatureFileUploadTemplate
            Dim StrAction As String = ""
            objICAPNFUTemplateForSave.es.Connection.CommandTimeout = 3600
            objICAPNFUTemplateFrom.es.Connection.CommandTimeout = 3600
            If IsUpDate = False Then
                objICAPNFUTemplateForSave.CreatedBy = objICAPNFUTemplate.CreatedBy
                objICAPNFUTemplateForSave.CreatedDate = objICAPNFUTemplate.CreatedDate
                objICAPNFUTemplateForSave.Creater = objICAPNFUTemplate.Creater
                objICAPNFUTemplateForSave.CreationDate = objICAPNFUTemplate.CreationDate
            Else
                objICAPNFUTemplateForSave.LoadByPrimaryKey(objICAPNFUTemplate.TemplateID, objICAPNFUTemplate.AccountNumber, objICAPNFUTemplate.BranchCode, objICAPNFUTemplate.Currency, objICAPNFUTemplate.PaymentNatureCode)
                objICAPNFUTemplateFrom.LoadByPrimaryKey(objICAPNFUTemplate.TemplateID, objICAPNFUTemplate.AccountNumber, objICAPNFUTemplate.BranchCode, objICAPNFUTemplate.Currency, objICAPNFUTemplate.PaymentNatureCode)
                objICAPNFUTemplateForSave.CreatedBy = objICAPNFUTemplate.CreatedBy
                objICAPNFUTemplateForSave.CreatedDate = objICAPNFUTemplate.CreatedDate
            End If
            objICAPNFUTemplateForSave.AccountNumber = objICAPNFUTemplate.AccountNumber
            objICAPNFUTemplateForSave.BranchCode = objICAPNFUTemplate.BranchCode
            objICAPNFUTemplateForSave.Currency = objICAPNFUTemplate.Currency
            objICAPNFUTemplateForSave.TemplateID = objICAPNFUTemplate.TemplateID
            objICAPNFUTemplateForSave.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
            objICAPNFUTemplateForSave.Save()
            If IsUpDate = False Then
                ICUtilities.AddAuditTrail("Account payment nature file upload template with Account number [ " & objICAPNFUTemplateForSave.AccountNumber & " ] payment nature [ " & objICAPNFUTemplateForSave.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] file upload template [ " & objICAPNFUTemplateForSave.TemplateID & " ] tagged.", "Account Payment Nature File Upload Template", objICAPNFUTemplateForSave.TemplateID.ToString & "-" & objICAPNFUTemplateForSave.AccountNumber.ToString & "-" & objICAPNFUTemplateForSave.BranchCode.ToString & "-" & objICAPNFUTemplateForSave.Currency.ToString & "-" & objICAPNFUTemplateForSave.PaymentNatureCode.ToString, UsersID, UsersName, "ADD")
            Else
                StrAction = "Account payment nature file upload template "
                StrAction += "Account number from [ " & objICAPNFUTemplateFrom.AccountNumber & " ] to [" & objICAPNFUTemplateForSave.AccountNumber & " ] ;"
                'StrAction += "Branch code from [ " & objICAPNFUTemplateFrom.BranchCode & " ] to [ " & objICAPNFUTemplateForSave.BranchCode & " ] ;"
                'StrAction += "Currency from [ " & objICAPNFUTemplateFrom.Currency & " ] to  [ " & objICAPNFUTemplateForSave.Currency & " ] ;"
                StrAction += "Template from [ " & objICAPNFUTemplateFrom.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplateFrom.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] to [ " & objICAPNFUTemplateForSave.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplateForSave.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] ;"
                StrAction += "Payment nature from [ " & objICAPNFUTemplateFrom.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] to [ " & objICAPNFUTemplateForSave.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] updated successfully."
                ICUtilities.AddAuditTrail(StrAction, "Account Payment Nature File Upload Template", objICAPNFUTemplateForSave.TemplateID.ToString & "-" & objICAPNFUTemplateForSave.AccountNumber.ToString & "-" & objICAPNFUTemplateForSave.BranchCode.ToString & "-" & objICAPNFUTemplateForSave.Currency.ToString, UsersID, UsersName, "UPDATE")
            End If
        End Sub
        Public Shared Sub DleteAPNFileUploadTemplate(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICAPNFUTemplateForDelete As New ICAccountPayNatureFileUploadTemplate
            objICAPNFUTemplateForDelete.es.Connection.CommandTimeout = 3600

            objICAPNFUTemplateForDelete.LoadByPrimaryKey(objICAPNFUTemplate.TemplateID, objICAPNFUTemplate.AccountNumber, objICAPNFUTemplate.BranchCode, objICAPNFUTemplate.Currency, objICAPNFUTemplate.PaymentNatureCode)
            objICAPNFUTemplateForDelete.MarkAsDeleted()
            objICAPNFUTemplateForDelete.Save()
            ICUtilities.AddAuditTrail("File upload template : [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] for account number [ " & objICAPNFUTemplate.AccountNumber.ToString & "]  , payment nature [ " & objICAPNFUTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] deleted.", "Account Payment Nature File Upload Template", objICAPNFUTemplate.TemplateID.ToString & "-" & objICAPNFUTemplate.AccountNumber.ToString & "-" & objICAPNFUTemplate.BranchCode.ToString & "-" & objICAPNFUTemplate.Currency.ToString & "-" & objICAPNFUTemplate.PaymentNatureCode.ToString, UsersID, UsersName, "DELETE")
        End Sub
        Public Shared Function CheckIsInstructionExistInPendingReadStatusAgainstPaymentNatureAndAccount(ByVal PaymentNatureCode As String) As Boolean
            Dim qryObjICInstruction As New ICInstructionQuery("objICInstruction")
            Dim qryObjICStatus As New ICInstructionStatusQuery("qryObjICStatus")
            Dim Result As Boolean = False
            qryObjICStatus.Select(qryObjICStatus.StatusName)
            qryObjICStatus.InnerJoin(qryObjICInstruction).On(qryObjICStatus.StatusID = qryObjICInstruction.Status)
            qryObjICStatus.Where(qryObjICStatus.StatusName = "Pending Read" And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode)
            If qryObjICStatus.Load Then
                Result = True

            End If
            Return Result
        End Function
        Public Shared Function GetTemplatesByAccountPaymentNature(ByVal objICAPNFNature As ICAccountsPaymentNature) As DataTable
            Dim qryObjICFileUploadTemplate As New ICTemplateQuery("qryObjICFileUploadTemplate")
            Dim qryObjICAPNFUTemplate As New ICAccountPayNatureFileUploadTemplateQuery("qryObjICAPNFUTemplate")
            Dim dt As New DataTable
            qryObjICAPNFUTemplate.Select(qryObjICFileUploadTemplate.TemplateID, qryObjICFileUploadTemplate.TemplateName)
            qryObjICAPNFUTemplate.InnerJoin(qryObjICFileUploadTemplate).On(qryObjICAPNFUTemplate.TemplateID = qryObjICFileUploadTemplate.TemplateID)
            qryObjICAPNFUTemplate.Where(qryObjICAPNFUTemplate.AccountNumber = objICAPNFNature.AccountNumber And qryObjICAPNFUTemplate.BranchCode = objICAPNFNature.BranchCode And qryObjICAPNFUTemplate.Currency = objICAPNFNature.Currency And qryObjICAPNFUTemplate.PaymentNatureCode = objICAPNFNature.PaymentNatureCode)

            qryObjICAPNFUTemplate.OrderBy(qryObjICFileUploadTemplate.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICAPNFUTemplate.LoadDataTable()
            Return dt
        End Function
        Public Shared Function IsTemplateIsTaggedWithAPNProductType(ByVal APNFileUploadTemplate As ICAccountPayNatureFileUploadTemplate) As Boolean
            Dim Result As Boolean = False
            Dim objICAPNPTypeTemplate As New ICAccountsPaymentNatureProductTypeTemplatesCollection

            objICAPNPTypeTemplate.Query.Where(objICAPNPTypeTemplate.Query.AccountNumber = APNFileUploadTemplate.AccountNumber And objICAPNPTypeTemplate.Query.BranchCode = APNFileUploadTemplate.BranchCode And objICAPNPTypeTemplate.Query.Currency = APNFileUploadTemplate.Currency And objICAPNPTypeTemplate.Query.PaymentNatureCode = APNFileUploadTemplate.PaymentNatureCode And objICAPNPTypeTemplate.Query.TemplateID = APNFileUploadTemplate.TemplateID)
            If objICAPNPTypeTemplate.Query.Load Then
                Result = True
            End If
            Return Result
        End Function
    End Class
End Namespace
