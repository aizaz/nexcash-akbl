﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICAccountsController

        ''Farhan 13 April 2015

        Public Shared Sub AddAccounts(ByVal ICAccount As ICAccounts, ByVal objICAccountsFrom As ICAccounts, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String)
            Dim objICAccounts As New ICAccounts
            objICAccounts.es.Connection.CommandTimeout = 3600

            If IsUpDate = False Then

                objICAccounts.CreateBy = ICAccount.CreateBy
                objICAccounts.CreateDate = ICAccount.CreateDate
                objICAccounts.Creater = ICAccount.Creater
                objICAccounts.CreationDate = ICAccount.CreationDate
            Else
                objICAccounts.LoadByPrimaryKey(objICAccountsFrom.AccountNumber, objICAccountsFrom.BranchCode, objICAccountsFrom.Currency)
                objICAccounts.CreateBy = ICAccount.CreateBy
                objICAccounts.CreateDate = ICAccount.CreateDate
            End If
            objICAccounts.AccountNumber = ICAccount.AccountNumber
            objICAccounts.BranchCode = ICAccount.BranchCode
            objICAccounts.Currency = ICAccount.Currency
            objICAccounts.AccountTitle = ICAccount.AccountTitle
            objICAccounts.CompanyCode = ICAccount.CompanyCode
            objICAccounts.IsActive = ICAccount.IsActive
            objICAccounts.DailyBalanceAllow = ICAccount.DailyBalanceAllow
            objICAccounts.CustomerSwiftCode = ICAccount.CustomerSwiftCode
            objICAccounts.IsApproved = False
            objICAccounts.Save()

            If IsUpDate = False Then
                ICUtilities.AddAuditTrail("Account [ " & objICAccounts.AccountNumber.ToString() & " ], Branch Code [ " & objICAccounts.BranchCode & " ], Currency [ " & objICAccounts.Currency & " ] , title [" & objICAccounts.AccountTitle & " ], Customer Swift Code [ " & objICAccounts.CustomerSwiftCode & " ] , is active  [ " & objICAccounts.IsActive & " ] for client " & objICAccounts.UpToICCompanyByCompanyCode.CompanyName & " ISApproved:  [" & objICAccounts.IsApproved.ToString() & "]  Added", "Client Account", objICAccounts.AccountNumber.ToString + "-" + objICAccounts.BranchCode + "-" + objICAccounts.Currency, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpDate = True Then
                ICUtilities.AddAuditTrail(ActionString.ToString, "Client Account", objICAccounts.AccountNumber.ToString + "-" + objICAccounts.BranchCode + "-" + objICAccounts.Currency, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


        End Sub

        'Public Shared Sub AddAccounts(ByVal ICAccount As ICAccounts, ByVal objICAccountsFrom As ICAccounts, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String)
        '    Dim objICAccounts As New ICAccounts
        '    objICAccounts.es.Connection.CommandTimeout = 3600

        '    If IsUpDate = False Then

        '        objICAccounts.CreateBy = ICAccount.CreateBy
        '        objICAccounts.CreateDate = ICAccount.CreateDate
        '        objICAccounts.Creater = ICAccount.Creater
        '        objICAccounts.CreationDate = ICAccount.CreationDate
        '    Else
        '        objICAccounts.LoadByPrimaryKey(objICAccountsFrom.AccountNumber, objICAccountsFrom.BranchCode, objICAccountsFrom.Currency)
        '        objICAccounts.CreateBy = ICAccount.CreateBy
        '        objICAccounts.CreateDate = ICAccount.CreateDate
        '    End If
        '    objICAccounts.AccountNumber = ICAccount.AccountNumber
        '    objICAccounts.BranchCode = ICAccount.BranchCode
        '    objICAccounts.Currency = ICAccount.Currency
        '    objICAccounts.AccountTitle = ICAccount.AccountTitle
        '    objICAccounts.CompanyCode = ICAccount.CompanyCode
        '    objICAccounts.IsActive = ICAccount.IsActive
        '    objICAccounts.DailyBalanceAllow = ICAccount.DailyBalanceAllow
        '    objICAccounts.CustomerSwiftCode = ICAccount.CustomerSwiftCode
        '    objICAccounts.Save()

        '    If IsUpDate = False Then
        '        ICUtilities.AddAuditTrail("Account [ " & objICAccounts.AccountNumber.ToString() & " ], Branch Code [ " & objICAccounts.BranchCode & " ], Currency [ " & objICAccounts.Currency & " ] , title [" & objICAccounts.AccountTitle & " ], Customer Swift Code [ " & objICAccounts.CustomerSwiftCode & " ] , is active  [ " & objICAccounts.IsActive & " ] for client " & objICAccounts.UpToICCompanyByCompanyCode.CompanyName & " Added", "Client Account", objICAccounts.AccountNumber.ToString + "-" + objICAccounts.BranchCode + "-" + objICAccounts.Currency, UsersID.ToString, UsersName.ToString, "ADD")
        '    ElseIf IsUpDate = True Then
        '        ICUtilities.AddAuditTrail(ActionString.ToString, "Client Account", objICAccounts.AccountNumber.ToString + "-" + objICAccounts.BranchCode + "-" + objICAccounts.Currency, UsersID.ToString, UsersName.ToString, "UPDATE")
        '    End If


        'End Sub

        ''Farhan 13 April 2015
        Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String)


            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjMasterSeries As New ICMasterSeriesQuery("qryObjMasterSeries")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable
            qryObjICAccounts.Select(qryObjICAccounts.AccountNumber, qryObjICAccounts.BranchCode, qryObjICAccounts.Currency, qryObjICAccounts.IsActive, qryObjICAccounts.CreationDate, qryObjICAccounts.IsApproved)
            qryObjICAccounts.Select(qryObjICAccounts.AccountTitle, qryObjMasterSeries.MasterSeriesID.Count.Coalesce("'-'").As("MasterSeries"))
            qryObjICAccounts.LeftJoin(qryObjMasterSeries).On(qryObjICAccounts.AccountNumber = qryObjMasterSeries.AccountNumber And qryObjICAccounts.BranchCode = qryObjMasterSeries.BranchCode And qryObjICAccounts.Currency = qryObjMasterSeries.Currency)
            qryObjICAccounts.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAccounts.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)

            qryObjICAccounts.GroupBy(qryObjICAccounts.AccountNumber, qryObjICAccounts.BranchCode, qryObjICAccounts.Currency, qryObjICAccounts.AccountTitle, qryObjICAccounts.IsActive, qryObjICAccounts.IsApproved, qryObjICAccounts.CreationDate)
            'qryObjICAccounts.OrderBy(qryObjICAccounts.AccountNumber.Descending, qryObjICAccounts.BranchCode.Descending, qryObjICAccounts.Currency.Descending, qryObjICAccounts.AccountTitle.Descending)
            qryObjICAccounts.OrderBy(qryObjICAccounts.CreationDate.Descending)
            If Not GroupCode.ToString = "" Then
                qryObjICAccounts.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode.ToString = "" Then
                qryObjICAccounts.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If
            dt = qryObjICAccounts.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICAccounts.es.PageNumber = PageNumber
                qryObjICAccounts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAccounts.es.PageNumber = 1
                qryObjICAccounts.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub



        'Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As radgrid, ByVal GroupCode As String, ByVal CompanyCode As String)


        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjMasterSeries As New ICMasterSeriesQuery("qryObjMasterSeries")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim dt As New DataTable
        '    qryObjICAccounts.Select(qryObjICAccounts.AccountNumber, qryObjICAccounts.BranchCode, qryObjICAccounts.Currency, qryObjICAccounts.IsActive, qryObjICAccounts.CreationDate)
        '    qryObjICAccounts.Select(qryObjICAccounts.AccountTitle, qryObjMasterSeries.MasterSeriesID.Count.Coalesce("'-'").As("MasterSeries"))
        '    qryObjICAccounts.LeftJoin(qryObjMasterSeries).On(qryObjICAccounts.AccountNumber = qryObjMasterSeries.AccountNumber And qryObjICAccounts.BranchCode = qryObjMasterSeries.BranchCode And qryObjICAccounts.Currency = qryObjMasterSeries.Currency)
        '    qryObjICAccounts.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICAccounts.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)

        '    qryObjICAccounts.GroupBy(qryObjICAccounts.AccountNumber, qryObjICAccounts.BranchCode, qryObjICAccounts.Currency, qryObjICAccounts.AccountTitle, qryObjICAccounts.IsActive, qryObjICAccounts.CreationDate)
        '    'qryObjICAccounts.OrderBy(qryObjICAccounts.AccountNumber.Descending, qryObjICAccounts.BranchCode.Descending, qryObjICAccounts.Currency.Descending, qryObjICAccounts.AccountTitle.Descending)
        '    qryObjICAccounts.OrderBy(qryObjICAccounts.CreationDate.Descending)
        '    If Not GroupCode.ToString = "" Then
        '        qryObjICAccounts.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
        '    End If
        '    If Not CompanyCode.ToString = "" Then
        '        qryObjICAccounts.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    End If
        '    dt = qryObjICAccounts.LoadDataTable


        '    If Not PageNumber = 0 Then
        '        qryObjICAccounts.es.PageNumber = PageNumber
        '        qryObjICAccounts.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICAccounts.es.PageNumber = 1
        '        qryObjICAccounts.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If
        'End Sub
        Public Shared Function GetAllAccounts() As ICAccountsCollection
            Dim collICAccounts As New ICAccountsCollection
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            collICAccounts.Query.Load()
            Return collICAccounts
        End Function


        Public Shared Sub DeleteAccount(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim ICAccount As New IC.ICAccounts
            ICAccount.LoadByPrimaryKey(AccountNumber, BranchCode, Currency)
            ICAccount.MarkAsDeleted()
            ICUtilities.AddAuditTrail("Account : [ " & AccountNumber.ToString & " ], Branch Code [ " & BranchCode & " ], Currency [ " & Currency & " ] deleted.", "Client Account", AccountNumber.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            ICAccount.Save()
        End Sub
        ''Farhan 13 April 2015
        Public Shared Sub ApproveAccount(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal ApprovedBy As Integer, ByVal UserName As String, ByVal IsApproved As Boolean)
            Dim ICAccount As New IC.ICAccounts
            Dim CurrentAt As String
            ICAccount.LoadByPrimaryKey(AccountNumber, BranchCode, Currency)
            ICAccount.ApprovedBy = ApprovedBy
            ICAccount.IsApproved = IsApproved

            ICAccount.ApprovedOn = Now
            ICAccount.Save()

            CurrentAt = "Account :[Account Number:  " & ICAccount.AccountNumber.ToString() & " ], Branch Code [ " & ICAccount.BranchCode & " ], Currency [ " & ICAccount.Currency & " ] , title [" & ICAccount.AccountTitle & " ], Customer Swift Code [ " & ICAccount.CustomerSwiftCode & " ] , is active  [ " & ICAccount.IsActive & " ] for client " & ICAccount.UpToICCompanyByCompanyCode.CompanyName & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Client Account", ICAccount.AccountNumber.ToString + "-" + ICAccount.BranchCode + "-" + ICAccount.Currency, ApprovedBy.ToString(), UserName.ToString(), "APPROVE")



        End Sub

        Public Shared Function GetAllActiveAndApproveAccountByGroupCode(ByVal GroupCode As String) As DataTable
            Dim objICAccnts As New ICAccountsQuery("qryICAccnts")
            Dim objICCompany As New IC.ICCompanyQuery("qryICCompany")

            objICAccnts.Select(objICCompany.CompanyName, objICAccnts.AccountNumber, objICAccnts.BranchCode, objICAccnts.Currency)
            objICAccnts.InnerJoin(objICCompany).On(objICAccnts.CompanyCode = objICCompany.CompanyCode)
            objICAccnts.Where(objICCompany.GroupCode = GroupCode And objICAccnts.IsActive = True And objICAccnts.IsApproved = True)

            Return objICAccnts.LoadDataTable()

        End Function

        '' Work By Aizaz Ahmed - Get All Accounts Company wise for Drop Down in AccountPaymentNatureProductType Module


        Public Shared Function GetAllAccountsByCompanyCode(ByVal CompanyCode As String) As ICAccountsCollection
            Dim collICAccounts As New ICAccountsCollection
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.Where(collICAccounts.Query.CompanyCode = CompanyCode And collICAccounts.Query.IsActive = True And collICAccounts.Query.IsApproved = True)
            collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            collICAccounts.Query.Load()
            Return collICAccounts
        End Function
        Public Shared Function GetAllAccountsByCompanyCodeForDropDown(ByVal CompanyCode As String) As ICAccountsCollection
            Dim collICAccounts As New ICAccountsCollection
            Dim dt As New DataTable
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.Select((collICAccounts.Query.AccountNumber + "," + collICAccounts.Query.BranchCode + "," + collICAccounts.Query.Currency).As("AccountNumber"), (collICAccounts.Query.AccountNumber + "-" + collICAccounts.Query.AccountTitle).As("AccountTitle"))
            collICAccounts.Query.Where(collICAccounts.Query.CompanyCode = CompanyCode And collICAccounts.Query.IsActive = True And collICAccounts.Query.IsApproved = True)
            'collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            dt = collICAccounts.Query.LoadDataTable

            collICAccounts.Query.Load()
            Return collICAccounts
        End Function
        Public Shared Function GetAllAccountsByCompanyCodeForSubSeriesDropDown(ByVal CompanyCode As String) As ICAccountsCollection
            Dim collICAccounts As New ICAccountsCollection
            Dim dt As New DataTable
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.Select((collICAccounts.Query.AccountNumber + "," + collICAccounts.Query.BranchCode + "," + collICAccounts.Query.Currency).As("AccountNumber"), collICAccounts.Query.AccountNumber.As("AccountTitle"))
            collICAccounts.Query.Where(collICAccounts.Query.CompanyCode = CompanyCode And collICAccounts.Query.IsActive = True)
            collICAccounts.Query.Where(collICAccounts.Query.IsApproved = True)
            'collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            dt = collICAccounts.Query.LoadDataTable

            collICAccounts.Query.Load()
            Return collICAccounts
        End Function
        Public Shared Function GetAllAccountNumberByPrimaryKeyForDropDown(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As ICAccountsCollection
            Dim collICAccounts As New ICAccountsCollection
            Dim dt As New DataTable
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.Select((collICAccounts.Query.AccountNumber + "," + collICAccounts.Query.BranchCode + "," + collICAccounts.Query.Currency).As("AccountNumber"), collICAccounts.Query.AccountTitle)
            collICAccounts.Query.Where(collICAccounts.Query.AccountNumber = AccountNumber And collICAccounts.Query.BranchCode = BranchCode And collICAccounts.Query.Currency = Currency)
            'collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            dt = collICAccounts.Query.LoadDataTable

            collICAccounts.Query.Load()
            Return collICAccounts
        End Function
        Public Shared Function GetAccountByAccountNumber(ByVal AccountNumber As String) As ICAccounts
            Dim collICAccounts As New ICAccountsCollection
            collICAccounts.es.Connection.CommandTimeout = 3600
            collICAccounts.Query.Where(collICAccounts.Query.AccountNumber = AccountNumber And collICAccounts.Query.IsActive = True)
            collICAccounts.Query.OrderBy(collICAccounts.Query.AccountNumber.Ascending)
            collICAccounts.Query.Load()
            Return collICAccounts(0)
        End Function

        '' Javed 22-05-2013



        Public Shared Function GetAllAccountsTaggedWithUserByUserIDAndCompanyCode(ByVal UsersID As String, ByVal CompanyCode As String) As DataTable




            Dim dt As New DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")


            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.AccountNumber.As("MainAccountNumber"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICAccounts.AccountTitle).As("AccountNumber"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.CompanyCode = CompanyCode And qryObjICUserRolesAPNature.UserID = UsersID)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt

        End Function

        Public Shared Function GetAllAccountNosWithDailyAccountBalanceAllow() As ICAccountsCollection
            Dim objICAccountsColl As New ICAccountsCollection



            objICAccountsColl.Query.Where(objICAccountsColl.Query.DailyBalanceAllow = True And objICAccountsColl.Query.IsActive = True)
            objICAccountsColl.Query.Where(objICAccountsColl.Query.NotificationSendDate.IsNull Or objICAccountsColl.Query.NotificationSendDate.Date = Now.Date.AddDays(-1))

            objICAccountsColl.Query.Load()


            Return objICAccountsColl



        End Function
    End Class
End Namespace

