﻿Imports Telerik.Web.UI
Imports System.Web
Imports DotNetNuke.Web.UI.WebControls
Imports EntitySpaces.Core
Namespace IC
    Public Class ICInstructionController
        Public Shared Function AddInstruction(ByVal ICInstruction As ICInstruction, ByVal objICBeneficiary As ICBeneficiary, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String) As String
            Dim objInstruction As New ICInstruction
            Dim objICInstructionActivity As New ICInstructionActivity
            Dim StrAction As String = ""
            Dim StrBeneId As Integer
            objInstruction.es.Connection.CommandTimeout = 3600
            objICInstructionActivity.es.Connection.CommandTimeout = 3600




            StrBeneId = ICBeneficiaryController.AddBeneficiaryForInstruction(objICBeneficiary, UsersID.ToString, UsersName.ToString)



            objInstruction.Description = ICInstruction.Description
            objInstruction.Remarks = ICInstruction.Remarks
            objInstruction.CompanyCode = ICInstruction.CompanyCode
            objInstruction.CreateBy = ICInstruction.CreateBy
            objInstruction.CreateDate = ICInstruction.CreateDate
            objInstruction.Status = ICInstruction.Status
            objInstruction.FileID = ICInstruction.FileID
            objInstruction.AcquisitionMode = ICInstruction.AcquisitionMode
            objInstruction.LastStatus = ICInstruction.LastStatus
            objInstruction.ErrorMessage = ICInstruction.ErrorMessage
            objInstruction.Amount = ICInstruction.Amount
            objInstruction.AmountInWords = ICInstruction.AmountInWords
            objInstruction.BeneficiaryAccountNo = ICInstruction.BeneficiaryAccountNo
            If ICInstruction.BeneficiaryAccountTitle <> "" And Not ICInstruction.BeneficiaryAccountTitle Is Nothing Then
                objInstruction.BeneficiaryAccountTitle = ICInstruction.BeneficiaryAccountTitle
            Else
                objInstruction.BeneficiaryAccountTitle = ""
            End If
            objInstruction.BeneficiaryAddress = ICInstruction.BeneficiaryAddress
            objInstruction.BeneficiaryBankCode = ICInstruction.BeneficiaryBankCode
            objInstruction.BeneficiaryBankName = ICInstruction.BeneficiaryBankName
            objInstruction.BeneficiaryBankAddress = ICInstruction.BeneficiaryBankAddress
            objInstruction.BeneficiaryBranchCode = ICInstruction.BeneficiaryBranchCode
            objInstruction.BeneficiaryBranchName = ICInstruction.BeneficiaryBranchName
            objInstruction.BeneBranchAddress = ICInstruction.BeneBranchAddress
            objInstruction.BeneficiaryCity = ICInstruction.BeneficiaryCity
            objInstruction.BeneficiaryCNIC = ICInstruction.BeneficiaryCNIC
            objInstruction.BeneficiaryCode = StrBeneId.ToString
            objInstruction.BeneficiaryCountry = ICInstruction.BeneficiaryCountry
            objInstruction.BeneficiaryEmail = ICInstruction.BeneficiaryEmail
            objInstruction.BeneficiaryMobile = ICInstruction.BeneficiaryMobile
            objInstruction.BeneficiaryName = ICInstruction.BeneficiaryName
            objInstruction.BeneficiaryPhone = ICInstruction.BeneficiaryPhone
            objInstruction.BeneficiaryProvince = ICInstruction.BeneficiaryProvince
            objInstruction.BeneficiaryType = ICInstruction.BeneficiaryType
            objInstruction.ClientAccountNo = ICInstruction.ClientAccountNo
            objInstruction.ClientAddress = ICInstruction.ClientAddress
            objInstruction.ClientBankCode = ICInstruction.ClientBankCode
            objInstruction.ClientBankName = ICInstruction.ClientBankName
            objInstruction.ClientBranchAddress = ICInstruction.ClientBranchAddress
            objInstruction.ClientBranchCode = ICInstruction.ClientBranchCode
            objInstruction.ClientBranchName = ICInstruction.ClientBranchName
            objInstruction.ClientCity = ICInstruction.ClientCity
            objInstruction.ClientCountry = ICInstruction.ClientCountry
            objInstruction.ClientEmail = ICInstruction.ClientEmail
            objInstruction.ClientMobile = ICInstruction.ClientMobile
            objInstruction.ClientName = ICInstruction.ClientName
            objInstruction.ClientProvince = ICInstruction.ClientProvince
            objInstruction.DetailAmount = ICInstruction.DetailAmount
            objInstruction.DDPayableLocation = ICInstruction.DDPayableLocation
            objInstruction.InstrumentNo = ICInstruction.InstrumentNo
            objInstruction.InvoiceNo = ICInstruction.InvoiceNo
            objInstruction.PaymentMode = ICInstruction.PaymentMode
            objInstruction.PaymentNatureCode = ICInstruction.PaymentNatureCode
            objInstruction.PrintLocationName = ICInstruction.PrintLocationName
            objInstruction.ProductTypeCode = ICInstruction.ProductTypeCode
            objInstruction.TransactionDate = ICInstruction.TransactionDate
            objInstruction.TXNCode = ICInstruction.TXNCode
            objInstruction.ValueDate = ICInstruction.ValueDate
            objInstruction.IsPrinted = ICInstruction.IsPrinted
            objInstruction.IsRePrinted = ICInstruction.IsRePrinted
            objInstruction.RePrintCounter = ICInstruction.RePrintCounter
            objInstruction.IsReIssued = ICInstruction.IsReIssued
            objInstruction.IsReValidated = ICInstruction.IsReValidated
            objInstruction.ReferenceField1 = ICInstruction.ReferenceField1
            objInstruction.ReferenceField2 = ICInstruction.ReferenceField2
            objInstruction.ReferenceField3 = ICInstruction.ReferenceField3
            objInstruction.ClientAccountBranchCode = ICInstruction.ClientAccountBranchCode
            objInstruction.ClientAccountCurrency = ICInstruction.ClientAccountCurrency
            If ICInstruction.BeneAccountBranchCode <> "" And Not ICInstruction.BeneAccountBranchCode Is Nothing Then
                objInstruction.BeneAccountBranchCode = ICInstruction.BeneAccountBranchCode
            Else
                objInstruction.BeneAccountBranchCode = ""
            End If
            If ICInstruction.BeneAccountCurrency <> "" And Not ICInstruction.BeneAccountCurrency Is Nothing Then
                objInstruction.BeneAccountCurrency = ICInstruction.BeneAccountCurrency
            Else
                objInstruction.BeneAccountCurrency = ""
            End If

            objInstruction.GroupCode = ICInstruction.GroupCode
            objInstruction.PayableAccountNumber = ICInstruction.PayableAccountNumber
            objInstruction.PayableAccountBranchCode = ICInstruction.PayableAccountBranchCode
            objInstruction.PayableAccountCurrency = ICInstruction.PayableAccountCurrency
            objInstruction.ClearingAccountNo = ICInstruction.ClearingAccountNo
            objInstruction.ClearingAccountBranchCode = ICInstruction.ClearingAccountBranchCode
            objInstruction.ClearingAccountCurrency = ICInstruction.ClearingAccountCurrency
            objInstruction.DDDrawnCityCode = ICInstruction.DDDrawnCityCode
            objInstruction.PrintLocationCode = ICInstruction.PrintLocationCode
            objInstruction.CreatedOfficeCode = ICInstruction.CreatedOfficeCode
            objInstruction.FileBatchNo = ICInstruction.FileBatchNo
            objInstruction.PrintDate = ICInstruction.PrintDate


            objInstruction.ReturnReason = ICInstruction.ReturnReason
            objInstruction.ScheduleTransactionID = ICInstruction.ScheduleTransactionID
            objInstruction.IsCheduleTransactionProcessed = ICInstruction.IsCheduleTransactionProcessed
            objInstruction.ScheduleTransactionTime = ICInstruction.ScheduleTransactionTime
            objInstruction.CancelledBy = ICInstruction.CancelledBy
            objInstruction.CancelledApproveBy = ICInstruction.CancelledApproveBy
            objInstruction.CancelledApprovalDate = ICInstruction.CancelledApprovalDate
            objInstruction.BeneficaryAccountType = ICInstruction.BeneficaryAccountType
            ''COTC Work


            objInstruction.BeneficiaryIDType = ICInstruction.BeneficiaryIDType
            objInstruction.BeneficiaryIDNo = ICInstruction.BeneficiaryIDNo
            If ICInstruction.BeneID <> "0" Then
                objInstruction.BeneID = ICInstruction.BeneID
            End If


            ''Bill Payment Work
            objInstruction.UBPCompanyID = ICInstruction.UBPCompanyID
            objInstruction.UBPReferenceField = ICInstruction.UBPReferenceField
            objInstruction.Save()

            ICUtilities.AddAuditTrail(ActionString.ToString(), "Instruction", objInstruction.InstructionID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
            StrAction += "Instruction via [ " & objInstruction.AcquisitionMode & " ] for beneficiary [ " & objInstruction.BeneficiaryName & " ]"
            StrAction += ", Status [ " & objInstruction.UpToICInstructionStatusByStatus.StatusName & " ] added."
            objICInstructionActivity.Action = StrAction
            objICInstructionActivity.ActionBy = UsersID.ToString
            objICInstructionActivity.FromStatus = objInstruction.LastStatus
            objICInstructionActivity.ToStatus = objInstruction.Status
            objICInstructionActivity.InstructionID = objInstruction.InstructionID
            objICInstructionActivity.ActionDate = Date.Now
            objICInstructionActivity.CompanyCode = objInstruction.CompanyCode
            If Not HttpContext.Current Is Nothing Then
                objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
            End If
            objICInstructionActivity.BatchNo = objInstruction.FileBatchNo

            AddInstructionActivity(objICInstructionActivity, UsersID.ToString, UsersName.ToString, StrAction.ToString, "ADD")
            StrAction = ""
            Return objInstruction.InstructionID.ToString()

        End Function
        Public Shared Sub UpDateInstruction(ByVal ICInstruction As ICInstruction, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String, ByVal ActionType As String)
            Dim objInstruction As New ICInstruction
            Dim objICInstructionActivity As New ICInstructionActivity
            objInstruction.es.Connection.CommandTimeout = 3600
            objICInstructionActivity.es.Connection.CommandTimeout = 3600
            objInstruction.LoadByPrimaryKey(ICInstruction.InstructionID)
            objInstruction.Description = ICInstruction.Description
            objInstruction.Remarks = ICInstruction.Remarks
            objInstruction.CompanyCode = ICInstruction.CompanyCode
            objInstruction.CreateBy = ICInstruction.CreateBy
            objInstruction.CreateDate = ICInstruction.CreateDate
            objInstruction.Status = ICInstruction.Status
            objInstruction.FileID = ICInstruction.FileID
            objInstruction.AcquisitionMode = ICInstruction.AcquisitionMode
            objInstruction.LastStatus = ICInstruction.LastStatus
            objInstruction.ErrorMessage = ICInstruction.ErrorMessage
            objInstruction.Amount = ICInstruction.Amount
            objInstruction.AmountInWords = ICInstruction.AmountInWords
            objInstruction.BeneficiaryAccountNo = ICInstruction.BeneficiaryAccountNo
            objInstruction.BeneficiaryAccountTitle = ICInstruction.BeneficiaryAccountTitle
            objInstruction.BeneficiaryAddress = ICInstruction.BeneficiaryAddress
            objInstruction.BeneficiaryBankCode = ICInstruction.BeneficiaryBankCode
            objInstruction.BeneficiaryBankName = ICInstruction.BeneficiaryBankName
            objInstruction.BeneficiaryBankAddress = ICInstruction.BeneficiaryBankAddress
            objInstruction.BeneficiaryBranchCode = ICInstruction.BeneficiaryBranchCode
            objInstruction.BeneficiaryBranchName = ICInstruction.BeneficiaryBranchName
            objInstruction.BeneBranchAddress = ICInstruction.BeneBranchAddress
            objInstruction.BeneficiaryCity = ICInstruction.BeneficiaryCity
            objInstruction.BeneficiaryCNIC = ICInstruction.BeneficiaryCNIC
            objInstruction.BeneficiaryCountry = ICInstruction.BeneficiaryCountry
            objInstruction.BeneficiaryEmail = ICInstruction.BeneficiaryEmail
            objInstruction.BeneficiaryMobile = ICInstruction.BeneficiaryMobile
            objInstruction.BeneficiaryName = ICInstruction.BeneficiaryName
            objInstruction.BeneficiaryPhone = ICInstruction.BeneficiaryPhone
            objInstruction.BeneficiaryProvince = ICInstruction.BeneficiaryProvince
            objInstruction.BeneficiaryType = ICInstruction.BeneficiaryType
            objInstruction.ClientAccountNo = ICInstruction.ClientAccountNo
            objInstruction.ClientAddress = ICInstruction.ClientAddress
            objInstruction.ClientBankCode = ICInstruction.ClientBankCode
            objInstruction.ClientBankName = ICInstruction.ClientBankName
            objInstruction.ClientBranchAddress = ICInstruction.ClientBranchAddress
            objInstruction.ClientBranchCode = ICInstruction.ClientBranchCode
            objInstruction.ClientBranchName = ICInstruction.ClientBranchName
            objInstruction.ClientCity = ICInstruction.ClientCity
            objInstruction.ClientCountry = ICInstruction.ClientCountry
            objInstruction.ClientEmail = ICInstruction.ClientEmail
            objInstruction.ClientMobile = ICInstruction.ClientMobile
            objInstruction.ClientName = ICInstruction.ClientName
            objInstruction.ClientProvince = ICInstruction.ClientProvince
            objInstruction.DetailAmount = ICInstruction.DetailAmount
            objInstruction.DDPayableLocation = ICInstruction.DDPayableLocation
            objInstruction.InstrumentNo = ICInstruction.InstrumentNo
            objInstruction.InvoiceNo = ICInstruction.InvoiceNo
            objInstruction.PaymentMode = ICInstruction.PaymentMode
            objInstruction.PaymentNatureCode = ICInstruction.PaymentNatureCode
            objInstruction.PrintLocationName = ICInstruction.PrintLocationName
            objInstruction.ProductTypeCode = ICInstruction.ProductTypeCode
            objInstruction.TransactionDate = ICInstruction.TransactionDate
            objInstruction.TXNCode = ICInstruction.TXNCode
            objInstruction.ValueDate = ICInstruction.ValueDate
            objInstruction.IsPrinted = ICInstruction.IsPrinted
            objInstruction.IsRePrinted = ICInstruction.IsRePrinted
            objInstruction.RePrintCounter = ICInstruction.RePrintCounter
            objInstruction.IsReIssued = ICInstruction.IsReIssued
            objInstruction.IsReValidated = ICInstruction.IsReValidated
            objInstruction.ReferenceField1 = ICInstruction.ReferenceField1
            objInstruction.ReferenceField2 = ICInstruction.ReferenceField2
            objInstruction.ReferenceField3 = ICInstruction.ReferenceField3
            objInstruction.ClientAccountBranchCode = ICInstruction.ClientAccountBranchCode
            objInstruction.ClientAccountCurrency = ICInstruction.ClientAccountCurrency
            objInstruction.BeneAccountBranchCode = ICInstruction.BeneAccountBranchCode
            objInstruction.BeneAccountCurrency = ICInstruction.BeneAccountCurrency
            objInstruction.GroupCode = ICInstruction.GroupCode
            objInstruction.PayableAccountNumber = ICInstruction.PayableAccountNumber
            objInstruction.PayableAccountBranchCode = ICInstruction.PayableAccountBranchCode
            objInstruction.PayableAccountCurrency = ICInstruction.PayableAccountCurrency
            objInstruction.ClearingAccountNo = ICInstruction.ClearingAccountNo
            objInstruction.ClearingAccountBranchCode = ICInstruction.ClearingAccountBranchCode
            objInstruction.ClearingAccountCurrency = ICInstruction.ClearingAccountCurrency
            objInstruction.DDDrawnCityCode = ICInstruction.DDDrawnCityCode
            objInstruction.PrintLocationCode = ICInstruction.PrintLocationCode
            objInstruction.CreatedOfficeCode = ICInstruction.CreatedOfficeCode
            objInstruction.FileBatchNo = ICInstruction.FileBatchNo
            objInstruction.DrawnOnBranchCode = ICInstruction.DrawnOnBranchCode
            objInstruction.CancellationDate = ICInstruction.CancellationDate
            objInstruction.VerificationDate = ICInstruction.VerificationDate
            objInstruction.LastPrintDate = ICInstruction.LastPrintDate
            objInstruction.StaleDate = ICInstruction.StaleDate
            objInstruction.RevalidationDate = ICInstruction.RevalidationDate
            objInstruction.ClientPrimaryApprovedBy = ICInstruction.ClientPrimaryApprovedBy
            objInstruction.ClientSecondaryApprovedBy = ICInstruction.ClientSecondaryApprovedBy
            objInstruction.ClientPrimaryApprovedDate = ICInstruction.ClientPrimaryApprovedDate
            objInstruction.ClientSecondaryApprovalDateTime = ICInstruction.ClientSecondaryApprovalDateTime
            objInstruction.BankPrimaryApprovedBy = ICInstruction.BankPrimaryApprovedBy
            objInstruction.BankPrimaryApprovedDate = ICInstruction.BankPrimaryApprovedDate
            objInstruction.BankSecondaryApprovedBy = ICInstruction.BankSecondaryApprovedBy
            objInstruction.BankSecondaryApprovedDate = ICInstruction.BankSecondaryApprovedDate
            objInstruction.PreviousPrintLocationCode = ICInstruction.PreviousPrintLocationCode
            objInstruction.PreviousPrintLocationName = ICInstruction.PreviousPrintLocationName
            objInstruction.PrintLocationAmendedBy = ICInstruction.PrintLocationAmendedBy
            objInstruction.PrintLocationAmendmentApprovedBy = ICInstruction.PrintLocationAmendmentApprovedBy
            objInstruction.PrintLocationApprovedAmendmentDate = ICInstruction.PrintLocationApprovedAmendmentDate
            objInstruction.IsPrintLocationAmendmentApproved = ICInstruction.IsPrintLocationAmendmentApproved
            objInstruction.PrintLocationAmendmentCancelledBy = ICInstruction.PrintLocationAmendmentCancelledBy
            objInstruction.PrintLocationAmendmentCancellDate = ICInstruction.PrintLocationAmendmentCancellDate
            objInstruction.IsPrintLocationAmendmentCancelled = ICInstruction.IsPrintLocationAmendmentCancelled
            objInstruction.PrintLocationAmendmentDate = ICInstruction.PrintLocationAmendmentDate
            objInstruction.IsPrintLocationAmended = ICInstruction.IsPrintLocationAmended

            objInstruction.IsReprintingAllowed = ICInstruction.IsReprintingAllowed
            objInstruction.ReprintingAllowedDate = ICInstruction.ReprintingAllowedDate
            objInstruction.ReprintingAllowedBy = ICInstruction.ReprintingAllowedBy

            objInstruction.ReprintingAllowedCancelledBy = ICInstruction.ReprintingAllowedCancelledBy
            objInstruction.ReprintingAllowCancelledDate = ICInstruction.ReprintingAllowCancelledDate
            objInstruction.IsAllowReprintingCancelled = ICInstruction.IsAllowReprintingCancelled

            objInstruction.IsAllowReprintingApproved = ICInstruction.IsAllowReprintingApproved
            objInstruction.AllowReprintingApprovedBy = ICInstruction.AllowReprintingApprovedBy
            objInstruction.AllowReprintingApprovedDate = ICInstruction.AllowReprintingApprovedDate


            objInstruction.IsReIssuanceAllowed = ICInstruction.IsReIssuanceAllowed
            objInstruction.ReIssuanceAllowedDate = ICInstruction.ReIssuanceAllowedDate
            objInstruction.ReissuanceAlloweBy = ICInstruction.ReissuanceAlloweBy

            objInstruction.IsAllowedReIssuanceCancelled = ICInstruction.IsAllowedReIssuanceCancelled
            objInstruction.AllowedReIssuanceCancelledDate = ICInstruction.AllowedReIssuanceCancelledDate
            objInstruction.AllowedReIssuanceCancelledBy = ICInstruction.AllowedReIssuanceCancelledBy
            objInstruction.IsAllowedReIssuanceApproved = ICInstruction.IsAllowedReIssuanceApproved
            objInstruction.AllowedReIssuanceApprovedBy = ICInstruction.AllowedReIssuanceApprovedBy
            objInstruction.AllowReIssuanceApprovedDate = ICInstruction.AllowReIssuanceApprovedDate
            objInstruction.ReIssuanceID = ICInstruction.ReIssuanceID
            objInstruction.NewInstructionID = ICInstruction.NewInstructionID
            objInstruction.PhysicalInstrumentNumber = ICInstruction.PhysicalInstrumentNumber


            objInstruction.IsReValiDationAllowed = ICInstruction.IsReValiDationAllowed
            objInstruction.ReValiDationAllowedBy = ICInstruction.ReValiDationAllowedBy
            objInstruction.ReValiDatedDate = ICInstruction.ReValiDatedDate
            objInstruction.IsReValiDationApproved = ICInstruction.IsReValiDationApproved
            objInstruction.ReValidationApprovedBy = ICInstruction.ReValidationApprovedBy
            objInstruction.ReValidationApprovalDate = ICInstruction.ReValidationApprovalDate
            objInstruction.IsReValiDationCancelled = ICInstruction.IsReValiDationCancelled
            objInstruction.ReValiDationCancelledBy = ICInstruction.ReValiDationCancelledBy
            objInstruction.IsReValiDationCancelled = ICInstruction.IsReValiDationCancelled
            objInstruction.ClearingBankCode = ICInstruction.ClearingBankCode
            objInstruction.ClearedBy = ICInstruction.ClearedBy
            objInstruction.ClearingDate = ICInstruction.ClearingDate
            objInstruction.ClearingOfficeCode = ICInstruction.ClearingOfficeCode
            objInstruction.ClearingApprovedBy = ICInstruction.ClearingApprovedBy
            objInstruction.ClearingApprovedDate = ICInstruction.ClearingApprovedDate
            objInstruction.ClearingApprovedOfficeCode = ICInstruction.ClearingApprovedOfficeCode
            objInstruction.AmendedPrintLocationCode = ICInstruction.AmendedPrintLocationCode
            objInstruction.AmendedPrintLocationName = ICInstruction.AmendedPrintLocationName

            objInstruction.PayableClientNo = ICInstruction.PayableClientNo
            objInstruction.PayableSeqNo = ICInstruction.PayableSeqNo
            objInstruction.PayableClientNo = ICInstruction.PayableClientNo
            objInstruction.PayableAccountType = ICInstruction.PayableAccountType
            objInstruction.PayableProfitCentre = ICInstruction.PayableProfitCentre
            objInstruction.ClearingClientNo = ICInstruction.ClearingClientNo
            objInstruction.ClearingSeqNo = ICInstruction.ClearingSeqNo
            objInstruction.ClearingProfitCentre = ICInstruction.ClearingProfitCentre
            objInstruction.ClearingAccountType = ICInstruction.ClearingAccountType
            objInstruction.PrintDate = ICInstruction.PrintDate
            objInstruction.IsAmendmentComplete = ICInstruction.IsAmendmentComplete
            objInstruction.ClubID = ICInstruction.ClubID
            objInstruction.StatusSettledBy = ICInstruction.StatusSettledBy
            objInstruction.StatusSettledDate = ICInstruction.StatusSettledDate
            objInstruction.StatusReversedBy = ICInstruction.StatusReversedBy
            objInstruction.StatusReversedDate = ICInstruction.StatusReversedDate
            objInstruction.StatusCancelledBy = ICInstruction.StatusCancelledBy
            objInstruction.StatusCancelledDate = ICInstruction.StatusCancelledDate
            objInstruction.StatusChangeRemarks = ICInstruction.StatusChangeRemarks
            objInstruction.StatusApproveRemarks = ICInstruction.StatusApproveRemarks
            objInstruction.StatusCancelledApprovedBy = ICInstruction.StatusCancelledApprovedBy
            objInstruction.StatusSettledApprovedBy = ICInstruction.StatusSettledApprovedBy
            objInstruction.StatusReversedApprovedBy = ICInstruction.StatusReversedApprovedBy
            objInstruction.StatusCancelledApprovedDate = ICInstruction.StatusCancelledApprovedDate
            objInstruction.StatusReversedApprovedDate = ICInstruction.StatusReversedDate
            objInstruction.StatusSettledApprovedDate = ICInstruction.StatusReversedApprovedDate

            objInstruction.StatusChangeApprovalCancelledby = ICInstruction.StatusChangeApprovalCancelledby
            objInstruction.StatusChangeApprovalCancelledDate = ICInstruction.StatusChangeApprovalCancelledDate


            objInstruction.ScheduleTransactionID = ICInstruction.ScheduleTransactionID
            objInstruction.IsCheduleTransactionProcessed = ICInstruction.IsCheduleTransactionProcessed
            objInstruction.ScheduleTransactionTime = ICInstruction.ScheduleTransactionTime


            objInstruction.CancelledBy = ICInstruction.CancelledBy
            objInstruction.CancelledApproveBy = ICInstruction.CancelledApproveBy
            objInstruction.CancelledApprovalDate = ICInstruction.CancelledApprovalDate

            ''Cotc
            objInstruction.BeneficiaryIDType = ICInstruction.BeneficiaryIDType
            objInstruction.BeneficiaryIDNo = ICInstruction.BeneficiaryIDNo
            objInstruction.Rin = ICInstruction.Rin
            objInstruction.DisbursedBy = ICInstruction.DisbursedBy
            objInstruction.DisbursedDate = ICInstruction.DisbursedDate
            objInstruction.DisbursedBranchCode = ICInstruction.DisbursedBranchCode

            objInstruction.BeneficaryAccountType = ICInstruction.BeneficaryAccountType
            objInstruction.ReturnReason = ICInstruction.ReturnReason



            ''Bill Payment Changes

            objInstruction.AmountAfterDueDate = ICInstruction.AmountAfterDueDate
            objInstruction.AmountBeforeDueDate = ICInstruction.AmountBeforeDueDate
            objInstruction.DueDate = ICInstruction.DueDate


            ''IBFT STAN

            objInstruction.IBFTStan = ICInstruction.IBFTStan
            objInstruction.FinalStatus = ICInstruction.FinalStatus



            objInstruction.StatusHoldBy = ICInstruction.StatusHoldBy
            objInstruction.StatusHoldDate = ICInstruction.StatusHoldDate
            objInstruction.StatusChangeRemarks = ICInstruction.StatusChangeRemarks
            objInstruction.StatusApproveRemarks = ICInstruction.StatusApproveRemarks
            objInstruction.StatusHoldApprovedBy = ICInstruction.StatusHoldApprovedBy
            objInstruction.StatusHoldApprovedDate = ICInstruction.StatusHoldApprovedDate

            objInstruction.StatusRevertBy = ICInstruction.StatusRevertBy
            objInstruction.StatusRevertDate = ICInstruction.StatusRevertDate
            objInstruction.StatusRevertApprovedBy = ICInstruction.StatusRevertApprovedBy
            objInstruction.StatusRevertApprovedDate = ICInstruction.StatusRevertApprovedDate



            objInstruction.StatusUnHoldBy = ICInstruction.StatusUnHoldBy
            objInstruction.StatusUnHoldDate = ICInstruction.StatusUnHoldDate
            objInstruction.StatusUnHoldApprovedBy = ICInstruction.StatusUnHoldApprovedBy
            objInstruction.StatusUnHoldApprovedDate = ICInstruction.StatusUnHoldApprovedDate

            objInstruction.StatusChangeApprovalCancelledby = ICInstruction.StatusChangeApprovalCancelledby
            objInstruction.StatusChangeApprovalCancelledDate = ICInstruction.StatusChangeApprovalCancelledDate
            



            objInstruction.Save()




           



            ICUtilities.AddAuditTrail(ActionString.ToString(), "Instruction", objInstruction.InstructionID.ToString(), UsersID, UsersName, ActionType)
            objICInstructionActivity.Action = ActionString
            objICInstructionActivity.ActionBy = UsersID
            objICInstructionActivity.FromStatus = objInstruction.LastStatus
            objICInstructionActivity.ToStatus = objInstruction.Status
            objICInstructionActivity.InstructionID = objInstruction.InstructionID
            objICInstructionActivity.ActionDate = Date.Now
            objICInstructionActivity.CompanyCode = objInstruction.CompanyCode
            objICInstructionActivity.BatchNo = objInstruction.FileBatchNo
            If Not HttpContext.Current Is Nothing Then
                objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
            End If

            AddInstructionActivity(objICInstructionActivity, UsersID, UsersName, ActionString.ToString, "UPDATE")
        End Sub
        
        Public Shared Sub CopyInstructionOnApprovalOfReIssuance(ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal Remarks As String, ByVal ActionType As String)
            Dim objICOldInstruction As New ICInstruction
            Dim objICCopyInstruction As New ICInstruction
            Dim objICInstructionActivity As New ICInstructionActivity
            Dim StrNewInstructionID As String = Nothing
            Dim InstrumentNo As String = Nothing
            Dim ActionString As String = Nothing
            objICCopyInstruction.es.Connection.CommandTimeout = 3600
            objICOldInstruction.es.Connection.CommandTimeout = 3600
            objICInstructionActivity.es.Connection.CommandTimeout = 3600

            If objICOldInstruction.LoadByPrimaryKey(InstructionID) Then
                objICCopyInstruction.Description = objICOldInstruction.Description
                objICCopyInstruction.Remarks = objICOldInstruction.Remarks
                objICCopyInstruction.CompanyCode = objICOldInstruction.CompanyCode
                objICCopyInstruction.CreateBy = objICOldInstruction.CreateBy
                objICCopyInstruction.CreateDate = objICOldInstruction.CreateDate
                objICCopyInstruction.FileID = objICOldInstruction.FileID
                objICCopyInstruction.AcquisitionMode = objICOldInstruction.AcquisitionMode
                objICCopyInstruction.LastStatus = objICOldInstruction.Status
                objICCopyInstruction.ErrorMessage = objICOldInstruction.ErrorMessage
                objICCopyInstruction.Amount = objICOldInstruction.Amount
                objICCopyInstruction.AmountInWords = objICOldInstruction.AmountInWords
                objICCopyInstruction.BeneficiaryAccountNo = objICOldInstruction.BeneficiaryAccountNo
                objICCopyInstruction.BeneficiaryAccountTitle = objICOldInstruction.BeneficiaryAccountTitle
                objICCopyInstruction.BeneficiaryAddress = objICOldInstruction.BeneficiaryAddress
                objICCopyInstruction.BeneficiaryBankCode = objICOldInstruction.BeneficiaryBankCode
                objICCopyInstruction.BeneficiaryBankName = objICOldInstruction.BeneficiaryBankName
                objICCopyInstruction.BeneficiaryBankAddress = objICOldInstruction.BeneficiaryBankAddress
                objICCopyInstruction.BeneficiaryBranchCode = objICOldInstruction.BeneficiaryBranchCode
                objICCopyInstruction.BeneficiaryBranchName = objICOldInstruction.BeneficiaryBranchName
                objICCopyInstruction.BeneBranchAddress = objICOldInstruction.BeneBranchAddress
                objICCopyInstruction.BeneficiaryCity = objICOldInstruction.BeneficiaryCity
                objICCopyInstruction.BeneficiaryCNIC = objICOldInstruction.BeneficiaryCNIC
                objICCopyInstruction.BeneficiaryCountry = objICOldInstruction.BeneficiaryCountry
                objICCopyInstruction.BeneficiaryEmail = objICOldInstruction.BeneficiaryEmail
                objICCopyInstruction.BeneficiaryMobile = objICOldInstruction.BeneficiaryMobile
                objICCopyInstruction.BeneficiaryName = objICOldInstruction.BeneficiaryName
                objICCopyInstruction.BeneficiaryPhone = objICOldInstruction.BeneficiaryPhone
                objICCopyInstruction.BeneficiaryProvince = objICOldInstruction.BeneficiaryProvince
                objICCopyInstruction.BeneficiaryType = objICOldInstruction.BeneficiaryType
                objICCopyInstruction.ClientAccountNo = objICOldInstruction.ClientAccountNo
                objICCopyInstruction.ClientAddress = objICOldInstruction.ClientAddress
                objICCopyInstruction.ClientBankCode = objICOldInstruction.ClientBankCode
                objICCopyInstruction.ClientBankName = objICOldInstruction.ClientBankName
                objICCopyInstruction.ClientBranchAddress = objICOldInstruction.ClientBranchAddress
                objICCopyInstruction.ClientBranchCode = objICOldInstruction.ClientBranchCode
                objICCopyInstruction.ClientBranchName = objICOldInstruction.ClientBranchName
                objICCopyInstruction.ClientCity = objICOldInstruction.ClientCity
                objICCopyInstruction.ClientCountry = objICOldInstruction.ClientCountry
                objICCopyInstruction.ClientEmail = objICOldInstruction.ClientEmail
                objICCopyInstruction.ClientMobile = objICOldInstruction.ClientMobile
                objICCopyInstruction.ClientName = objICOldInstruction.ClientName
                objICCopyInstruction.ClientProvince = objICOldInstruction.ClientProvince
                objICCopyInstruction.DetailAmount = objICOldInstruction.DetailAmount
                objICCopyInstruction.DDPayableLocation = objICOldInstruction.DDPayableLocation
                objICCopyInstruction.InvoiceNo = objICOldInstruction.InvoiceNo
                objICCopyInstruction.PaymentMode = objICOldInstruction.PaymentMode
                objICCopyInstruction.PaymentNatureCode = objICOldInstruction.PaymentNatureCode
                objICCopyInstruction.PrintLocationName = objICOldInstruction.PrintLocationName
                objICCopyInstruction.ProductTypeCode = objICOldInstruction.ProductTypeCode
                objICCopyInstruction.TransactionDate = objICOldInstruction.TransactionDate
                objICCopyInstruction.TXNCode = objICOldInstruction.TXNCode
                objICCopyInstruction.ValueDate = objICOldInstruction.ValueDate
                objICCopyInstruction.IsPrinted = objICOldInstruction.IsPrinted
                objICCopyInstruction.IsRePrinted = objICOldInstruction.IsRePrinted
                objICCopyInstruction.RePrintCounter = objICOldInstruction.RePrintCounter
                objICCopyInstruction.IsReIssued = objICOldInstruction.IsReIssued
                objICCopyInstruction.IsReValidated = objICOldInstruction.IsReValidated
                objICCopyInstruction.ReferenceField1 = objICOldInstruction.ReferenceField1
                objICCopyInstruction.ReferenceField2 = objICOldInstruction.ReferenceField2
                objICCopyInstruction.ReferenceField3 = objICOldInstruction.ReferenceField3
                objICCopyInstruction.ClientAccountBranchCode = objICOldInstruction.ClientAccountBranchCode
                objICCopyInstruction.ClientAccountCurrency = objICOldInstruction.ClientAccountCurrency
                objICCopyInstruction.BeneAccountBranchCode = objICOldInstruction.BeneAccountBranchCode
                objICCopyInstruction.BeneAccountCurrency = objICOldInstruction.BeneAccountCurrency
                objICCopyInstruction.GroupCode = objICOldInstruction.GroupCode


                objICCopyInstruction.DDDrawnCityCode = objICOldInstruction.DDDrawnCityCode
                objICCopyInstruction.PrintLocationCode = objICOldInstruction.PrintLocationCode
                objICCopyInstruction.CreatedOfficeCode = objICOldInstruction.CreatedOfficeCode
                objICCopyInstruction.FileBatchNo = objICOldInstruction.FileBatchNo
                objICCopyInstruction.DrawnOnBranchCode = objICOldInstruction.DrawnOnBranchCode
                objICCopyInstruction.CancellationDate = objICOldInstruction.CancellationDate
                objICCopyInstruction.VerificationDate = objICOldInstruction.VerificationDate
                objICCopyInstruction.LastPrintDate = objICOldInstruction.LastPrintDate
                objICCopyInstruction.StaleDate = objICOldInstruction.StaleDate
                objICCopyInstruction.RevalidationDate = objICOldInstruction.RevalidationDate
                objICCopyInstruction.ClientPrimaryApprovedBy = objICOldInstruction.ClientPrimaryApprovedBy
                objICCopyInstruction.ClientSecondaryApprovedBy = objICOldInstruction.ClientSecondaryApprovedBy
                objICCopyInstruction.ClientPrimaryApprovedDate = objICOldInstruction.ClientPrimaryApprovedDate
                objICCopyInstruction.ClientSecondaryApprovalDateTime = objICOldInstruction.ClientSecondaryApprovalDateTime
                objICCopyInstruction.BankPrimaryApprovedBy = objICOldInstruction.BankPrimaryApprovedBy
                objICCopyInstruction.BankPrimaryApprovedDate = objICOldInstruction.BankPrimaryApprovedDate
                objICCopyInstruction.BankSecondaryApprovedBy = objICOldInstruction.BankSecondaryApprovedBy
                objICCopyInstruction.BankSecondaryApprovedDate = objICOldInstruction.BankSecondaryApprovedDate
                objICCopyInstruction.PreviousPrintLocationCode = objICOldInstruction.PreviousPrintLocationCode
                objICCopyInstruction.PreviousPrintLocationName = objICOldInstruction.PreviousPrintLocationName
                objICCopyInstruction.PrintLocationAmendedBy = objICOldInstruction.PrintLocationAmendedBy
                objICCopyInstruction.PrintLocationAmendmentApprovedBy = objICOldInstruction.PrintLocationAmendmentApprovedBy
                objICCopyInstruction.PrintLocationApprovedAmendmentDate = objICOldInstruction.PrintLocationApprovedAmendmentDate
                objICCopyInstruction.IsPrintLocationAmendmentApproved = objICOldInstruction.IsPrintLocationAmendmentApproved
                objICCopyInstruction.PrintLocationAmendmentCancelledBy = objICOldInstruction.PrintLocationAmendmentCancelledBy
                objICCopyInstruction.PrintLocationAmendmentCancellDate = objICOldInstruction.PrintLocationAmendmentCancellDate
                objICCopyInstruction.IsPrintLocationAmendmentCancelled = objICOldInstruction.IsPrintLocationAmendmentCancelled
                objICCopyInstruction.PrintLocationAmendmentDate = objICOldInstruction.PrintLocationAmendmentDate
                objICCopyInstruction.IsPrintLocationAmended = objICOldInstruction.IsPrintLocationAmended
                objICCopyInstruction.IsReprintingAllowed = objICOldInstruction.IsReprintingAllowed
                objICCopyInstruction.ReprintingAllowedDate = objICOldInstruction.ReprintingAllowedDate
                objICCopyInstruction.ReprintingAllowedBy = objICOldInstruction.ReprintingAllowedBy
                objICCopyInstruction.ReprintingAllowedCancelledBy = objICOldInstruction.ReprintingAllowedCancelledBy
                objICCopyInstruction.ReprintingAllowCancelledDate = objICOldInstruction.ReprintingAllowCancelledDate
                objICCopyInstruction.IsAllowReprintingCancelled = objICOldInstruction.IsAllowReprintingCancelled
                objICCopyInstruction.IsAllowReprintingApproved = objICOldInstruction.IsAllowReprintingApproved
                objICCopyInstruction.AllowReprintingApprovedBy = objICOldInstruction.AllowReprintingApprovedBy
                objICCopyInstruction.AllowReprintingApprovedDate = objICOldInstruction.AllowReprintingApprovedDate
                objICCopyInstruction.IsReIssuanceAllowed = objICOldInstruction.IsReIssuanceAllowed
                objICCopyInstruction.ReIssuanceAllowedDate = objICOldInstruction.ReIssuanceAllowedDate
                objICCopyInstruction.ReissuanceAlloweBy = objICOldInstruction.ReissuanceAlloweBy
                objICCopyInstruction.IsAllowedReIssuanceCancelled = objICOldInstruction.IsAllowedReIssuanceCancelled
                objICCopyInstruction.AllowedReIssuanceCancelledDate = objICOldInstruction.AllowedReIssuanceCancelledDate
                objICCopyInstruction.AllowedReIssuanceCancelledBy = objICOldInstruction.AllowedReIssuanceCancelledBy
                objICCopyInstruction.IsAllowedReIssuanceApproved = True
                objICCopyInstruction.AllowedReIssuanceApprovedBy = UsersID
                objICCopyInstruction.AllowReIssuanceApprovedDate = Date.Now
                objICCopyInstruction.ReIssuanceID = objICOldInstruction.InstructionID
                objICCopyInstruction.IsReValiDationAllowed = objICOldInstruction.IsReValiDationAllowed
                objICCopyInstruction.ReValiDationAllowedBy = objICOldInstruction.ReValiDationAllowedBy
                objICCopyInstruction.ReValiDatedDate = objICOldInstruction.ReValiDatedDate
                objICCopyInstruction.IsReValiDationApproved = objICOldInstruction.IsReValiDationApproved
                objICCopyInstruction.ReValidationApprovedBy = objICOldInstruction.ReValidationApprovedBy
                objICCopyInstruction.ReValidationApprovalDate = objICOldInstruction.ReValidationApprovalDate
                objICCopyInstruction.IsReValiDationCancelled = objICOldInstruction.IsReValiDationCancelled
                objICCopyInstruction.ReValiDationCancelledBy = objICOldInstruction.ReValiDationCancelledBy
                objICCopyInstruction.IsReValiDationCancelled = objICOldInstruction.IsReValiDationCancelled
                objICCopyInstruction.ClearingBankCode = objICOldInstruction.ClearingBankCode
                objICCopyInstruction.IsReIssuanceAllowed = True
                objICCopyInstruction.ReissuanceAlloweBy = UsersID
                objICCopyInstruction.ReIssuanceAllowedDate = Date.Now




                objICCopyInstruction.PayableAccountNumber = objICOldInstruction.PayableAccountNumber
                objICCopyInstruction.PayableAccountBranchCode = objICOldInstruction.PayableAccountBranchCode
                objICCopyInstruction.PayableAccountCurrency = objICOldInstruction.PayableAccountCurrency
                objICCopyInstruction.PayableClientNo = objICOldInstruction.PayableClientNo
                objICCopyInstruction.PayableSeqNo = objICOldInstruction.PayableSeqNo
                objICCopyInstruction.PayableProfitCentre = objICOldInstruction.PayableProfitCentre
                objICCopyInstruction.PayableAccountType = objICOldInstruction.PayableAccountType
                objICCopyInstruction.PrintDate = objICOldInstruction.PrintDate
                objICCopyInstruction.IsAmendmentComplete = objICOldInstruction.IsAmendmentComplete
                objICCopyInstruction.ClubID = objICOldInstruction.ClubID
                objICCopyInstruction.StatusSettledBy = objICOldInstruction.StatusSettledBy
                objICCopyInstruction.StatusSettledDate = objICOldInstruction.StatusSettledDate
                objICCopyInstruction.StatusReversedBy = objICOldInstruction.StatusReversedBy
                objICCopyInstruction.StatusReversedDate = objICOldInstruction.StatusReversedDate
                objICCopyInstruction.StatusCancelledBy = objICOldInstruction.StatusCancelledBy
                objICCopyInstruction.StatusCancelledDate = objICOldInstruction.StatusCancelledDate
                objICCopyInstruction.StatusChangeRemarks = objICOldInstruction.StatusChangeRemarks
                objICCopyInstruction.StatusApproveRemarks = objICOldInstruction.StatusApproveRemarks
                objICCopyInstruction.StatusCancelledApprovedBy = objICOldInstruction.StatusCancelledApprovedBy
                objICCopyInstruction.StatusSettledApprovedBy = objICOldInstruction.StatusSettledApprovedBy
                objICCopyInstruction.StatusReversedApprovedBy = objICOldInstruction.StatusReversedApprovedBy
                objICCopyInstruction.StatusCancelledApprovedDate = objICOldInstruction.StatusCancelledApprovedDate
                objICCopyInstruction.StatusReversedApprovedDate = objICOldInstruction.StatusReversedDate
                objICCopyInstruction.StatusSettledApprovedDate = objICOldInstruction.StatusReversedApprovedDate

                objICCopyInstruction.StatusChangeApprovalCancelledby = objICOldInstruction.StatusChangeApprovalCancelledby
                objICCopyInstruction.StatusChangeApprovalCancelledDate = objICOldInstruction.StatusChangeApprovalCancelledDate
                objICCopyInstruction.InstrumentNo = Nothing
                If objICOldInstruction.PaymentMode.ToLower.ToString = "dd" Or objICOldInstruction.PaymentMode.ToLower.ToString = "po" Then
                    objICCopyInstruction.Status = 16
                ElseIf objICOldInstruction.PaymentMode.ToLower.ToString = "cheque" Then

                    If objICOldInstruction.UpToICCompanyByCompanyCode.IsSingleSignatory = True Then
                        objICCopyInstruction.Status = 10
                    Else
                        objICCopyInstruction.Status = 8
                    End If
                End If
                '' Add New from copy Instruction



                ''COTC
                objICCopyInstruction.BeneficiaryIDType = objICOldInstruction.BeneficiaryIDType
                objICCopyInstruction.BeneficiaryIDNo = objICOldInstruction.BeneficiaryIDNo
                objICCopyInstruction.ReturnReason = Nothing
                objICCopyInstruction.Save()

                ActionString = Nothing
                ActionString += "Copy Instruction of client [ " & objICCopyInstruction.UpToICCompanyByCompanyCode.CompanyName & " ], Re-Issuance ID [ " & objICOldInstruction.InstructionID & " ] "
                ActionString += "of amount [ " & objICCopyInstruction.Amount & "] [ " & objICCopyInstruction.AmountInWords & " ] added. Client account number was [ " & objICCopyInstruction.ClientAccountNo & " ],"
                ActionString += " branch code [" & objICCopyInstruction.ClientAccountBranchCode & " ], currency [" & objICCopyInstruction.ClientAccountCurrency & " ]."
                ActionString += " Product type [ " & objICCopyInstruction.UpToICProductTypeByProductTypeCode.ProductTypeName & " ], Disbursement mode [ " & objICCopyInstruction.PaymentMode & " ], Payment nature [ " & objICCopyInstruction.PaymentNatureCode & " ]."


                '' Audit trail entry
                ICUtilities.AddAuditTrail(ActionString, "Instruction", objICCopyInstruction.InstructionID.ToString, UsersID, UsersName, ActionType)


                '' Update is assign instrument no's
                'If objICOldInstruction.PaymentMode.ToLower.ToString = "dd" Then
                '    ICDDInstrumentsController.MarkInstrumentNumberAssignedByInstructionIDAndDDInstrumentNumber(InstrumentNo.Split("-")(1).ToString, InstrumentNo.Split("-")(0).ToString, objICCopyInstruction.InstructionID.ToString, UsersID, UsersName)
                'ElseIf objICOldInstruction.PaymentMode.ToLower.ToString = "po" Then
                '    ICPOInstrumentsController.MarkInstrumentNumberAssignedByInstructionIDAndPOInstrumentNumber(InstrumentNo.Split("-")(1).ToString, InstrumentNo.Split("-")(0).ToString, objICCopyInstruction.InstructionID.ToString, UsersID, UsersName)
                'End If

                '' Add activity of adding instruction from copy Instruction

                objICInstructionActivity.Action = ActionString
                objICInstructionActivity.ActionBy = UsersID.ToString
                objICInstructionActivity.FromStatus = objICCopyInstruction.LastStatus
                objICInstructionActivity.ToStatus = objICCopyInstruction.Status
                objICInstructionActivity.InstructionID = objICCopyInstruction.InstructionID
                objICInstructionActivity.ActionDate = Date.Now
                objICInstructionActivity.CompanyCode = objICCopyInstruction.CompanyCode
                objICInstructionActivity.BatchNo = objICCopyInstruction.FileBatchNo
                If Not HttpContext.Current Is Nothing Then
                    objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If
                AddInstructionActivity(objICInstructionActivity, UsersID.ToString, UsersName.ToString, ActionString.ToString, "ADD")

                '' Update copy Instruction
                objICOldInstruction.NewInstructionID = objICCopyInstruction.InstructionID
                objICOldInstruction.LastStatus = objICOldInstruction.Status
                objICOldInstruction.Status = 5
                ActionString = Nothing
                ActionString += "Instruction id [ " & objICOldInstruction.InstructionID & " ] after re-issuance updated. New Instruction ID [ " & objICOldInstruction.NewInstructionID & " ] ."
                ActionString += "Action was taken by user [ " & UsersID & " ][ " & UsersName & " ] ."
                ''update old instruction
                UpDateInstruction(objICOldInstruction, UsersID, UsersName, ActionString, "UPDATE")
                ''update status
                UpdateInstructionStatus(objICOldInstruction.InstructionID.ToString, objICOldInstruction.LastStatus, "5", UsersID, UsersName, "Remarks", Remarks)
                'ICUtilities.AddAuditTrail(ActionString.ToString(), "Instruction", objInstruction.InstructionID.ToString(), UsersID.ToString(), UsersName.ToString())

                '' Add activity of updating instruction from copy Instruction

                objICInstructionActivity.Action = ActionString
                objICInstructionActivity.ActionBy = UsersID.ToString
                objICInstructionActivity.FromStatus = objICCopyInstruction.LastStatus
                objICInstructionActivity.ToStatus = objICCopyInstruction.Status
                objICInstructionActivity.InstructionID = objICCopyInstruction.InstructionID
                objICInstructionActivity.ActionDate = Date.Now
                objICInstructionActivity.CompanyCode = objICCopyInstruction.CompanyCode
                objICInstructionActivity.BatchNo = objICCopyInstruction.FileBatchNo
                If Not HttpContext.Current Is Nothing Then
                    objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If
                AddInstructionActivity(objICInstructionActivity, UsersID.ToString, UsersName.ToString, ActionString.ToString, "UPDATE")


                'Update instruction's detail if exists
                UpdateInstructionDetailForReIssuanceInstruction(InstructionID, objICCopyInstruction.InstructionID.ToString, UsersID.ToString, UsersName.ToString)



            End If
        End Sub
        
        Public Shared Sub UpdateInstructionDetailForReIssuanceInstruction(ByVal OldInstructionID As String, ByVal NewInstructionID As String, ByVal UsersID As String, ByVal UsersName As String)

            Dim objICInstructionDetailColl As New ICInstructionDetailCollection
            Dim StrAction As String = Nothing
            objICInstructionDetailColl.es.Connection.CommandTimeout = 3600

            objICInstructionDetailColl.Query.Where(objICInstructionDetailColl.Query.InstructionID = OldInstructionID.ToString)
            objICInstructionDetailColl.Query.Load()
            If objICInstructionDetailColl.Query.Load() = True Then
                For Each objICInstructionDetail As ICInstructionDetail In objICInstructionDetailColl
                    objICInstructionDetail.InstructionID = NewInstructionID.ToString
                    objICInstructionDetail.Save()
                    StrAction = Nothing
                    StrAction = "Instruction detail with ID: [ " & objICInstructionDetail.InstructionDetailID & " ] assigned new instruction with ID: [ " & NewInstructionID & " ] after reissuance. Old assigned instruction ID was [ " & OldInstructionID & " ]"
                    ICUtilities.AddAuditTrail(StrAction, "Instruction", NewInstructionID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
                Next
            End If

        End Sub
        Public Shared Sub GetAllInstructionForErrorAndInstructionTracking(ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Amount As Double, ByVal AmountOperator As String)
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")
            Dim dt As New DataTable

            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
            qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status, qryObjICInstruction.GroupCode)
            qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName, qryObjICInstructionStatus.StatusName)
            qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
            End If
            If AccountNumber.ToString <> "" And BranchCode.ToString <> "" And Currency.ToString <> "" And PaymentNatureCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = AccountNumber.ToString And qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString And qryObjICInstruction.ClientAccountCurrency = Currency.ToString And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode.ToString)
            End If
            If ProductTypeCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
            End If
            If InstructionNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If CompanyCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
            End If
            If GroupCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.GroupCode = GroupCode.ToString)
            End If
            If InstructionStatus.ToString <> "0" Then
                qryObjICInstruction.Where(qryObjICInstruction.Status = InstructionStatus)
            End If
            If Not Amount = -1 Then
                If AmountOperator.ToString = "=" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount = Amount)
                ElseIf AmountOperator.ToString = "<" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount < Amount)
                ElseIf AmountOperator.ToString = ">" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount > Amount)
                ElseIf AmountOperator.ToString = "<=" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount <= Amount)
                ElseIf AmountOperator.ToString = ">=" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount >= Amount)
                ElseIf AmountOperator.ToString = "<>" Then
                    qryObjICInstruction.Where(qryObjICInstruction.Amount <> Amount)
                End If
            End If
            qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
            dt = qryObjICInstruction.LoadDataTable
            If Not CurrentPage = 0 Then
                qryObjICInstruction.es.PageNumber = CurrentPage
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICInstruction.es.PageNumber = 1
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        'Public Shared Sub GetAllInstructionForErrorAndInstructionTracking(ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Amount As Double, ByVal AmountOperator As String)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")
        '    Dim dt As New DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status, qryObjICInstruction.GroupCode)
        '    qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName, qryObjICInstructionStatus.StatusName)
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
        '    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
        '    End If
        '    If AccountNumber.ToString <> "" And BranchCode.ToString <> "" And Currency.ToString <> "" And PaymentNatureCode.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = AccountNumber.ToString And qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString And qryObjICInstruction.ClientAccountCurrency = Currency.ToString And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode.ToString)
        '    End If
        '    If ProductTypeCode.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
        '    End If
        '    If InstructionNo.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
        '    End If
        '    If InstrumentNo.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '    End If
        '    If InstrumentNo.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '    End If
        '    If CompanyCode.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If GroupCode.ToString <> "" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.GroupCode = GroupCode.ToString)
        '    End If
        '    If InstructionStatus.ToString <> "0" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.Status = InstructionStatus)
        '    End If
        '    If Not Amount = 0 Then
        '        If AmountOperator.ToString = "=" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount = Amount)
        '        ElseIf AmountOperator.ToString = "<" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount < Amount)
        '        ElseIf AmountOperator.ToString = ">" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount > Amount)
        '        ElseIf AmountOperator.ToString = "<=" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount <= Amount)
        '        ElseIf AmountOperator.ToString = ">=" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount >= Amount)
        '        ElseIf AmountOperator.ToString = "<>" Then
        '            qryObjICInstruction.Where(qryObjICInstruction.Amount <> Amount)
        '        End If
        '    End If
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub

        Public Shared Sub GetAllInstructionForClearingApproval(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal OfficeCode As String)
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim dt As New DataTable

            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
            qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status, qryObjICInstruction.GroupCode)
            qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName, qryObjICUser.UserName)
            qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICUser).On(qryObjICInstruction.CreateBy = qryObjICUser.UserID)
            If DateType <> "" Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
                    End If
                End If
            End If
            If AccountNumber.ToString <> "" And BranchCode.ToString <> "" And Currency.ToString <> "" And PaymentNatureCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = AccountNumber.ToString And qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString And qryObjICInstruction.ClientAccountCurrency = Currency.ToString And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode.ToString)
            End If
            If ProductTypeCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
            End If
            If InstructionNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If CompanyCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
            End If
            If GroupCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.GroupCode = GroupCode.ToString)
            End If
            If InstructionStatus.ToString <> "0" Then
                qryObjICInstruction.Where(qryObjICInstruction.Status = InstructionStatus)
            End If
            qryObjICInstruction.Where(qryObjICInstruction.ClearingOfficeCode = OfficeCode)
            qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
            dt = qryObjICInstruction.LoadDataTable
            If Not CurrentPage = 0 Then
                qryObjICInstruction.es.PageNumber = CurrentPage
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICInstruction.es.PageNumber = 1
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub GetAccountNumbersTaggedWithPrincipalBankUSer(ByVal UserID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)

            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAPnature As New ICAccountsPaymentNatureQuery("qryObjICAPnature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.AccountNumber, qryObjICUserRolesAPNature.BranchCode, qryObjICUserRolesAPNature.Currency, qryObjICCompany.CompanyName)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPnature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPnature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPnature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPnature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPnature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPnature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPnature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPnature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            If Not PageNumber = 0 Then
                qryObjICUserRolesAPNature.es.PageNumber = PageNumber
                qryObjICUserRolesAPNature.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICUserRolesAPNature.es.PageNumber = 1
                qryObjICUserRolesAPNature.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub

        Public Shared Sub AddInstructionActivity(ByVal ICInstructionActivity As ICInstructionActivity, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String, ByVal ActionType As String)
            Dim objInstructionActivity As New ICInstructionActivity
            objInstructionActivity.es.Connection.CommandTimeout = 3600

            objInstructionActivity.InstructionID = ICInstructionActivity.InstructionID
            objInstructionActivity.Action = ICInstructionActivity.Action
            objInstructionActivity.FromStatus = ICInstructionActivity.FromStatus
            objInstructionActivity.ToStatus = ICInstructionActivity.ToStatus
            objInstructionActivity.ActionBy = ICInstructionActivity.ActionBy
            objInstructionActivity.ActionDate = ICInstructionActivity.ActionDate
            objInstructionActivity.UserIP = ICInstructionActivity.UserIP
            objInstructionActivity.BatchNo = ICInstructionActivity.BatchNo
            objInstructionActivity.CompanyCode = ICInstructionActivity.CompanyCode
            objInstructionActivity.Save()

            ICUtilities.AddAuditTrail(ActionString.ToString(), "Instruction Activity", objInstructionActivity.InstructionActivityID.ToString(), UsersID, UsersName, ActionType)
        End Sub
        Public Shared Sub AddInstructionDetail(ByVal ICInstructionDetail As ICInstructionDetail, ByVal UsersID As String, ByVal UsersName As String)
            Dim objInstructionDetail As New ICInstructionDetail
            Dim ActionString As String
            objInstructionDetail.es.Connection.CommandTimeout = 3600

            objInstructionDetail.InstructionID = ICInstructionDetail.InstructionID
            objInstructionDetail.FFAmt1 = ICInstructionDetail.FFAmt1
            objInstructionDetail.FFAmt2 = ICInstructionDetail.FFAmt2
            objInstructionDetail.FFAmt3 = ICInstructionDetail.FFAmt3
            objInstructionDetail.FFAmt4 = ICInstructionDetail.FFAmt4
            objInstructionDetail.FFAmt5 = ICInstructionDetail.FFAmt5
            objInstructionDetail.FFAmt6 = ICInstructionDetail.FFAmt6
            objInstructionDetail.FFAmt7 = ICInstructionDetail.FFAmt7
            objInstructionDetail.FFAmt8 = ICInstructionDetail.FFAmt8
            objInstructionDetail.FFAmt9 = ICInstructionDetail.FFAmt9
            objInstructionDetail.FFAmt10 = ICInstructionDetail.FFAmt10
            objInstructionDetail.FFAmt11 = ICInstructionDetail.FFAmt11
            objInstructionDetail.FFAmt12 = ICInstructionDetail.FFAmt12
            objInstructionDetail.FFAmt13 = ICInstructionDetail.FFAmt13
            objInstructionDetail.FFAmt14 = ICInstructionDetail.FFAmt14
            objInstructionDetail.FFAmt15 = ICInstructionDetail.FFAmt15
            objInstructionDetail.FFAmt16 = ICInstructionDetail.FFAmt16
            objInstructionDetail.FFAmt17 = ICInstructionDetail.FFAmt17
            objInstructionDetail.FFAmt18 = ICInstructionDetail.FFAmt18
            objInstructionDetail.FFAmt19 = ICInstructionDetail.FFAmt19
            objInstructionDetail.FFAmt20 = ICInstructionDetail.FFAmt20
            objInstructionDetail.FFAmt21 = ICInstructionDetail.FFAmt21
            objInstructionDetail.FFAmt22 = ICInstructionDetail.FFAmt22
            objInstructionDetail.FFAmt23 = ICInstructionDetail.FFAmt23
            objInstructionDetail.FFAmt24 = ICInstructionDetail.FFAmt24
            objInstructionDetail.FFAmt25 = ICInstructionDetail.FFAmt25
            objInstructionDetail.FFAmt26 = ICInstructionDetail.FFAmt26
            objInstructionDetail.FFAmt27 = ICInstructionDetail.FFAmt27
            objInstructionDetail.FFAmt28 = ICInstructionDetail.FFAmt28
            objInstructionDetail.FFAmt29 = ICInstructionDetail.FFAmt29
            objInstructionDetail.FFAmt30 = ICInstructionDetail.FFAmt30
            objInstructionDetail.FFAmt31 = ICInstructionDetail.FFAmt31
            objInstructionDetail.FFAmt32 = ICInstructionDetail.FFAmt32
            objInstructionDetail.FFAmt33 = ICInstructionDetail.FFAmt33
            objInstructionDetail.FFAmt34 = ICInstructionDetail.FFAmt34
            objInstructionDetail.FFAmt35 = ICInstructionDetail.FFAmt35
            objInstructionDetail.FFAmt36 = ICInstructionDetail.FFAmt36
            objInstructionDetail.FFAmt37 = ICInstructionDetail.FFAmt37
            objInstructionDetail.FFAmt38 = ICInstructionDetail.FFAmt38
            objInstructionDetail.FFAmt39 = ICInstructionDetail.FFAmt39
            objInstructionDetail.FFAmt40 = ICInstructionDetail.FFAmt40
            objInstructionDetail.FFAmt41 = ICInstructionDetail.FFAmt41
            objInstructionDetail.FFAmt42 = ICInstructionDetail.FFAmt42
            objInstructionDetail.FFAmt43 = ICInstructionDetail.FFAmt43
            objInstructionDetail.FFAmt44 = ICInstructionDetail.FFAmt44
            objInstructionDetail.FFAmt45 = ICInstructionDetail.FFAmt45
            objInstructionDetail.FFAmt46 = ICInstructionDetail.FFAmt46
            objInstructionDetail.FFAmt47 = ICInstructionDetail.FFAmt47
            objInstructionDetail.FFAmt48 = ICInstructionDetail.FFAmt48
            objInstructionDetail.FFAmt49 = ICInstructionDetail.FFAmt49
            objInstructionDetail.FFAmt50 = ICInstructionDetail.FFAmt50
            objInstructionDetail.FFTxt1 = ICInstructionDetail.FFTxt1
            objInstructionDetail.FFTxt2 = ICInstructionDetail.FFTxt2
            objInstructionDetail.FFTxt3 = ICInstructionDetail.FFTxt3
            objInstructionDetail.FFTxt4 = ICInstructionDetail.FFTxt4
            objInstructionDetail.FFTxt5 = ICInstructionDetail.FFTxt5
            objInstructionDetail.FFTxt6 = ICInstructionDetail.FFTxt6
            objInstructionDetail.FFTxt7 = ICInstructionDetail.FFTxt7
            objInstructionDetail.FFTxt8 = ICInstructionDetail.FFTxt8
            objInstructionDetail.FFTxt9 = ICInstructionDetail.FFTxt9
            objInstructionDetail.FFTxt10 = ICInstructionDetail.FFTxt10
            objInstructionDetail.FFTxt11 = ICInstructionDetail.FFTxt11
            objInstructionDetail.FFTxt12 = ICInstructionDetail.FFTxt12
            objInstructionDetail.FFTxt13 = ICInstructionDetail.FFTxt13
            objInstructionDetail.FFTxt14 = ICInstructionDetail.FFTxt14
            objInstructionDetail.FFTxt15 = ICInstructionDetail.FFTxt15
            objInstructionDetail.FFTxt16 = ICInstructionDetail.FFTxt16
            objInstructionDetail.FFTxt17 = ICInstructionDetail.FFTxt17
            objInstructionDetail.FFTxt18 = ICInstructionDetail.FFTxt18
            objInstructionDetail.FFTxt19 = ICInstructionDetail.FFTxt19
            objInstructionDetail.FFTxt20 = ICInstructionDetail.FFTxt20
            objInstructionDetail.FFTxt21 = ICInstructionDetail.FFTxt21
            objInstructionDetail.FFTxt22 = ICInstructionDetail.FFTxt22
            objInstructionDetail.FFTxt23 = ICInstructionDetail.FFTxt23
            objInstructionDetail.FFTxt24 = ICInstructionDetail.FFTxt24
            objInstructionDetail.FFTxt25 = ICInstructionDetail.FFTxt25
            objInstructionDetail.FFTxt26 = ICInstructionDetail.FFTxt26
            objInstructionDetail.FFTxt27 = ICInstructionDetail.FFTxt27
            objInstructionDetail.FFTxt28 = ICInstructionDetail.FFTxt28
            objInstructionDetail.FFTxt29 = ICInstructionDetail.FFTxt29
            objInstructionDetail.FFTxt30 = ICInstructionDetail.FFTxt30
            objInstructionDetail.FFTxt31 = ICInstructionDetail.FFTxt31
            objInstructionDetail.FFTxt32 = ICInstructionDetail.FFTxt32
            objInstructionDetail.FFTxt33 = ICInstructionDetail.FFTxt33
            objInstructionDetail.FFTxt34 = ICInstructionDetail.FFTxt34
            objInstructionDetail.FFTxt35 = ICInstructionDetail.FFTxt35
            objInstructionDetail.FFTxt36 = ICInstructionDetail.FFTxt36
            objInstructionDetail.FFTxt37 = ICInstructionDetail.FFTxt37
            objInstructionDetail.FFTxt38 = ICInstructionDetail.FFTxt38
            objInstructionDetail.FFTxt39 = ICInstructionDetail.FFTxt39
            objInstructionDetail.FFTxt40 = ICInstructionDetail.FFTxt40
            objInstructionDetail.FFTxt41 = ICInstructionDetail.FFTxt41
            objInstructionDetail.FFTxt42 = ICInstructionDetail.FFTxt42
            objInstructionDetail.FFTxt43 = ICInstructionDetail.FFTxt43
            objInstructionDetail.FFTxt44 = ICInstructionDetail.FFTxt44
            objInstructionDetail.FFTxt45 = ICInstructionDetail.FFTxt45
            objInstructionDetail.FFTxt46 = ICInstructionDetail.FFTxt46
            objInstructionDetail.FFTxt47 = ICInstructionDetail.FFTxt47
            objInstructionDetail.FFTxt48 = ICInstructionDetail.FFTxt48
            objInstructionDetail.FFTxt49 = ICInstructionDetail.FFTxt49
            objInstructionDetail.FFTxt50 = ICInstructionDetail.FFTxt50
            objInstructionDetail.FFTxt51 = ICInstructionDetail.FFTxt51
            objInstructionDetail.FFTxt52 = ICInstructionDetail.FFTxt52
            objInstructionDetail.FFTxt53 = ICInstructionDetail.FFTxt53
            objInstructionDetail.FFTxt54 = ICInstructionDetail.FFTxt54
            objInstructionDetail.FFTxt55 = ICInstructionDetail.FFTxt55
            objInstructionDetail.FFTxt56 = ICInstructionDetail.FFTxt56
            objInstructionDetail.FFTxt57 = ICInstructionDetail.FFTxt57
            objInstructionDetail.FFTxt58 = ICInstructionDetail.FFTxt58
            objInstructionDetail.FFTxt59 = ICInstructionDetail.FFTxt59
            objInstructionDetail.FFTxt60 = ICInstructionDetail.FFTxt60
            objInstructionDetail.FFTxt61 = ICInstructionDetail.FFTxt61
            objInstructionDetail.FFTxt62 = ICInstructionDetail.FFTxt62
            objInstructionDetail.FFTxt63 = ICInstructionDetail.FFTxt63
            objInstructionDetail.FFTxt64 = ICInstructionDetail.FFTxt64
            objInstructionDetail.FFTxt65 = ICInstructionDetail.FFTxt65
            objInstructionDetail.FFTxt66 = ICInstructionDetail.FFTxt66
            objInstructionDetail.FFTxt67 = ICInstructionDetail.FFTxt67
            objInstructionDetail.FFTxt68 = ICInstructionDetail.FFTxt68
            objInstructionDetail.FFTxt69 = ICInstructionDetail.FFTxt69
            objInstructionDetail.FFTxt70 = ICInstructionDetail.FFTxt70
            objInstructionDetail.FFTxt71 = ICInstructionDetail.FFTxt71
            objInstructionDetail.FFTxt72 = ICInstructionDetail.FFTxt72
            objInstructionDetail.FFTxt73 = ICInstructionDetail.FFTxt73
            objInstructionDetail.FFTxt74 = ICInstructionDetail.FFTxt74
            objInstructionDetail.FFTxt75 = ICInstructionDetail.FFTxt75
            objInstructionDetail.FFTxt76 = ICInstructionDetail.FFTxt76
            objInstructionDetail.FFTxt77 = ICInstructionDetail.FFTxt77
            objInstructionDetail.FFTxt78 = ICInstructionDetail.FFTxt78
            objInstructionDetail.FFTxt79 = ICInstructionDetail.FFTxt79
            objInstructionDetail.FFTxt80 = ICInstructionDetail.FFTxt80
            objInstructionDetail.FFTxt81 = ICInstructionDetail.FFTxt81
            objInstructionDetail.FFTxt82 = ICInstructionDetail.FFTxt82
            objInstructionDetail.FFTxt83 = ICInstructionDetail.FFTxt83
            objInstructionDetail.FFTxt84 = ICInstructionDetail.FFTxt84
            objInstructionDetail.FFTxt85 = ICInstructionDetail.FFTxt85
            objInstructionDetail.FFTxt86 = ICInstructionDetail.FFTxt86
            objInstructionDetail.FFTxt87 = ICInstructionDetail.FFTxt87
            objInstructionDetail.FFTxt88 = ICInstructionDetail.FFTxt88
            objInstructionDetail.FFTxt89 = ICInstructionDetail.FFTxt89
            objInstructionDetail.FFTxt90 = ICInstructionDetail.FFTxt90
            objInstructionDetail.FFTxt91 = ICInstructionDetail.FFTxt91
            objInstructionDetail.FFTxt92 = ICInstructionDetail.FFTxt92
            objInstructionDetail.FFTxt93 = ICInstructionDetail.FFTxt93
            objInstructionDetail.FFTxt94 = ICInstructionDetail.FFTxt94
            objInstructionDetail.FFTxt95 = ICInstructionDetail.FFTxt95
            objInstructionDetail.FFTxt96 = ICInstructionDetail.FFTxt96
            objInstructionDetail.FFTxt97 = ICInstructionDetail.FFTxt97
            objInstructionDetail.FFTxt98 = ICInstructionDetail.FFTxt98
            objInstructionDetail.FFTxt99 = ICInstructionDetail.FFTxt99
            objInstructionDetail.FFTxt100 = ICInstructionDetail.FFTxt100
            objInstructionDetail.DetailAmount = ICInstructionDetail.DetailAmount
            objInstructionDetail.DetailBeneAccountNo = ICInstructionDetail.DetailBeneAccountNo
            objInstructionDetail.DetailBeneAddress = ICInstructionDetail.DetailBeneAddress
            objInstructionDetail.DetailBeneBankName = ICInstructionDetail.DetailBeneBankName
            objInstructionDetail.DetailBeneBranchAddress = ICInstructionDetail.DetailBeneBranchAddress
            objInstructionDetail.DetailBeneBranchCode = ICInstructionDetail.DetailBeneBranchCode
            objInstructionDetail.DetailBeneBranchName = ICInstructionDetail.DetailBeneBranchName
            objInstructionDetail.DetailBeneCNIC = ICInstructionDetail.DetailBeneCNIC
            objInstructionDetail.DetailBeneCountry = ICInstructionDetail.DetailBeneCountry
            objInstructionDetail.DetailBeneProvince = ICInstructionDetail.DetailBeneProvince
            objInstructionDetail.DetailBeneCity = ICInstructionDetail.DetailBeneCity
            objInstructionDetail.DetailBeneEmail = ICInstructionDetail.DetailBeneEmail
            objInstructionDetail.DetailBeneMobile = ICInstructionDetail.DetailBeneMobile
            objInstructionDetail.DetailBenePhone = ICInstructionDetail.DetailBenePhone
            objInstructionDetail.DetailInvoiceNo = ICInstructionDetail.DetailInvoiceNo
            objInstructionDetail.DetailISNO = ICInstructionDetail.DetailISNO
            objInstructionDetail.DetailTXNCODE = ICInstructionDetail.DetailTXNCODE
            objInstructionDetail.DetailFFText1 = ICInstructionDetail.DetailFFText1
            objInstructionDetail.DetailFFText2 = ICInstructionDetail.DetailFFText2
            objInstructionDetail.DetailFFText3 = ICInstructionDetail.DetailFFText3
            objInstructionDetail.DetailFFText4 = ICInstructionDetail.DetailFFText4
            objInstructionDetail.DetailFFText5 = ICInstructionDetail.DetailFFText5
            objInstructionDetail.DetailFFText6 = ICInstructionDetail.DetailFFText6
            objInstructionDetail.DetailFFText7 = ICInstructionDetail.DetailFFText7
            objInstructionDetail.DetailFFText8 = ICInstructionDetail.DetailFFText8
            objInstructionDetail.DetailFFText9 = ICInstructionDetail.DetailFFText9
            objInstructionDetail.DetailFFText10 = ICInstructionDetail.DetailFFText10
            objInstructionDetail.DetailFFText11 = ICInstructionDetail.DetailFFText11
            objInstructionDetail.DetailFFText12 = ICInstructionDetail.DetailFFText12
            objInstructionDetail.DetailFFText13 = ICInstructionDetail.DetailFFText13
            objInstructionDetail.DetailFFText14 = ICInstructionDetail.DetailFFText14
            objInstructionDetail.DetailFFText15 = ICInstructionDetail.DetailFFText15
            objInstructionDetail.DetailFFText16 = ICInstructionDetail.DetailFFText16
            objInstructionDetail.DetailFFText17 = ICInstructionDetail.DetailFFText17
            objInstructionDetail.DetailFFText18 = ICInstructionDetail.DetailFFText18
            objInstructionDetail.DetailFFText19 = ICInstructionDetail.DetailFFText19
            objInstructionDetail.DetailFFText20 = ICInstructionDetail.DetailFFText20
            objInstructionDetail.DetailFFText21 = ICInstructionDetail.DetailFFText21
            objInstructionDetail.DetailFFText22 = ICInstructionDetail.DetailFFText22
            objInstructionDetail.DetailFFText23 = ICInstructionDetail.DetailFFText23
            objInstructionDetail.DetailFFText24 = ICInstructionDetail.DetailFFText24
            objInstructionDetail.DetailFFText25 = ICInstructionDetail.DetailFFText25
            objInstructionDetail.DetailFFText26 = ICInstructionDetail.DetailFFText26
            objInstructionDetail.DetailFFText27 = ICInstructionDetail.DetailFFText27
            objInstructionDetail.DetailFFText28 = ICInstructionDetail.DetailFFText28
            objInstructionDetail.DetailFFText29 = ICInstructionDetail.DetailFFText29
            objInstructionDetail.DetailFFText30 = ICInstructionDetail.DetailFFText30
            objInstructionDetail.DetailFFText31 = ICInstructionDetail.DetailFFText31
            objInstructionDetail.DetailFFText32 = ICInstructionDetail.DetailFFText32
            objInstructionDetail.DetailFFText33 = ICInstructionDetail.DetailFFText33
            objInstructionDetail.DetailFFText34 = ICInstructionDetail.DetailFFText34
            objInstructionDetail.DetailFFText35 = ICInstructionDetail.DetailFFText35
            objInstructionDetail.DetailFFText36 = ICInstructionDetail.DetailFFText36
            objInstructionDetail.DetailFFText37 = ICInstructionDetail.DetailFFText37
            objInstructionDetail.DetailFFText38 = ICInstructionDetail.DetailFFText38
            objInstructionDetail.DetailFFText39 = ICInstructionDetail.DetailFFText39
            objInstructionDetail.DetailFFText40 = ICInstructionDetail.DetailFFText40
            objInstructionDetail.DetailFFText41 = ICInstructionDetail.DetailFFText41
            objInstructionDetail.DetailFFText42 = ICInstructionDetail.DetailFFText42
            objInstructionDetail.DetailFFText43 = ICInstructionDetail.DetailFFText43
            objInstructionDetail.DetailFFText44 = ICInstructionDetail.DetailFFText44
            objInstructionDetail.DetailFFText45 = ICInstructionDetail.DetailFFText45
            objInstructionDetail.DetailFFText46 = ICInstructionDetail.DetailFFText46
            objInstructionDetail.DetailFFText47 = ICInstructionDetail.DetailFFText47
            objInstructionDetail.DetailFFText48 = ICInstructionDetail.DetailFFText48
            objInstructionDetail.DetailFFText49 = ICInstructionDetail.DetailFFText49
            objInstructionDetail.DetailFFText50 = ICInstructionDetail.DetailFFText50
            objInstructionDetail.DetailFFText51 = ICInstructionDetail.DetailFFText51
            objInstructionDetail.DetailFFText52 = ICInstructionDetail.DetailFFText52
            objInstructionDetail.DetailFFText53 = ICInstructionDetail.DetailFFText53
            objInstructionDetail.DetailFFText54 = ICInstructionDetail.DetailFFText54
            objInstructionDetail.DetailFFText55 = ICInstructionDetail.DetailFFText55
            objInstructionDetail.DetailFFText56 = ICInstructionDetail.DetailFFText56
            objInstructionDetail.DetailFFText57 = ICInstructionDetail.DetailFFText57
            objInstructionDetail.DetailFFText58 = ICInstructionDetail.DetailFFText58
            objInstructionDetail.DetailFFText59 = ICInstructionDetail.DetailFFText59
            objInstructionDetail.DetailFFText60 = ICInstructionDetail.DetailFFText60
            objInstructionDetail.DetailFFText61 = ICInstructionDetail.DetailFFText61
            objInstructionDetail.DetailFFText62 = ICInstructionDetail.DetailFFText62
            objInstructionDetail.DetailFFText63 = ICInstructionDetail.DetailFFText63
            objInstructionDetail.DetailFFText64 = ICInstructionDetail.DetailFFText64
            objInstructionDetail.DetailFFText65 = ICInstructionDetail.DetailFFText65
            objInstructionDetail.DetailFFText66 = ICInstructionDetail.DetailFFText66
            objInstructionDetail.DetailFFText67 = ICInstructionDetail.DetailFFText67
            objInstructionDetail.DetailFFText68 = ICInstructionDetail.DetailFFText68
            objInstructionDetail.DetailFFText69 = ICInstructionDetail.DetailFFText69
            objInstructionDetail.DetailFFText70 = ICInstructionDetail.DetailFFText70
            objInstructionDetail.DetailFFText71 = ICInstructionDetail.DetailFFText71
            objInstructionDetail.DetailFFText72 = ICInstructionDetail.DetailFFText72
            objInstructionDetail.DetailFFText73 = ICInstructionDetail.DetailFFText73
            objInstructionDetail.DetailFFText74 = ICInstructionDetail.DetailFFText74
            objInstructionDetail.DetailFFText75 = ICInstructionDetail.DetailFFText75
            objInstructionDetail.DetailFFText76 = ICInstructionDetail.DetailFFText76
            objInstructionDetail.DetailFFText77 = ICInstructionDetail.DetailFFText77
            objInstructionDetail.DetailFFText78 = ICInstructionDetail.DetailFFText78
            objInstructionDetail.DetailFFText79 = ICInstructionDetail.DetailFFText79
            objInstructionDetail.DetailFFText80 = ICInstructionDetail.DetailFFText80
            objInstructionDetail.DetailFFText81 = ICInstructionDetail.DetailFFText81
            objInstructionDetail.DetailFFText82 = ICInstructionDetail.DetailFFText82
            objInstructionDetail.DetailFFText83 = ICInstructionDetail.DetailFFText83
            objInstructionDetail.DetailFFText84 = ICInstructionDetail.DetailFFText84
            objInstructionDetail.DetailFFText85 = ICInstructionDetail.DetailFFText85
            objInstructionDetail.DetailFFText86 = ICInstructionDetail.DetailFFText86
            objInstructionDetail.DetailFFText87 = ICInstructionDetail.DetailFFText87
            objInstructionDetail.DetailFFText88 = ICInstructionDetail.DetailFFText88
            objInstructionDetail.DetailFFText89 = ICInstructionDetail.DetailFFText89
            objInstructionDetail.DetailFFText90 = ICInstructionDetail.DetailFFText90
            objInstructionDetail.DetailFFText91 = ICInstructionDetail.DetailFFText91
            objInstructionDetail.DetailFFText92 = ICInstructionDetail.DetailFFText92
            objInstructionDetail.DetailFFText93 = ICInstructionDetail.DetailFFText93
            objInstructionDetail.DetailFFText94 = ICInstructionDetail.DetailFFText94
            objInstructionDetail.DetailFFText95 = ICInstructionDetail.DetailFFText95
            objInstructionDetail.DetailFFText96 = ICInstructionDetail.DetailFFText96
            objInstructionDetail.DetailFFText97 = ICInstructionDetail.DetailFFText97
            objInstructionDetail.DetailFFText98 = ICInstructionDetail.DetailFFText98
            objInstructionDetail.DetailFFText99 = ICInstructionDetail.DetailFFText99
            objInstructionDetail.DetailFFText100 = ICInstructionDetail.DetailFFText100
            objInstructionDetail.DetailFFAmt1 = ICInstructionDetail.DetailFFAmt1
            objInstructionDetail.DetailFFAmt2 = ICInstructionDetail.DetailFFAmt2
            objInstructionDetail.DetailFFAmt3 = ICInstructionDetail.DetailFFAmt3
            objInstructionDetail.DetailFFAmt4 = ICInstructionDetail.DetailFFAmt4
            objInstructionDetail.DetailFFAmt5 = ICInstructionDetail.DetailFFAmt5
            objInstructionDetail.DetailFFAmt6 = ICInstructionDetail.DetailFFAmt6
            objInstructionDetail.DetailFFAmt7 = ICInstructionDetail.DetailFFAmt7
            objInstructionDetail.DetailFFAmt8 = ICInstructionDetail.DetailFFAmt8
            objInstructionDetail.DetailFFAmt9 = ICInstructionDetail.DetailFFAmt9
            objInstructionDetail.DetailFFAmt10 = ICInstructionDetail.DetailFFAmt10
            objInstructionDetail.DetailFFAmt11 = ICInstructionDetail.DetailFFAmt11
            objInstructionDetail.DetailFFAmt12 = ICInstructionDetail.DetailFFAmt12
            objInstructionDetail.DetailFFAmt13 = ICInstructionDetail.DetailFFAmt13
            objInstructionDetail.DetailFFAmt14 = ICInstructionDetail.DetailFFAmt14
            objInstructionDetail.DetailFFAmt15 = ICInstructionDetail.DetailFFAmt15
            objInstructionDetail.DetailFFAmt16 = ICInstructionDetail.DetailFFAmt16
            objInstructionDetail.DetailFFAmt17 = ICInstructionDetail.DetailFFAmt17
            objInstructionDetail.DetailFFAmt18 = ICInstructionDetail.DetailFFAmt18
            objInstructionDetail.DetailFFAmt19 = ICInstructionDetail.DetailFFAmt19
            objInstructionDetail.DetailFFAmt20 = ICInstructionDetail.DetailFFAmt20
            objInstructionDetail.DetailFFAmt21 = ICInstructionDetail.DetailFFAmt21
            objInstructionDetail.DetailFFAmt22 = ICInstructionDetail.DetailFFAmt22
            objInstructionDetail.DetailFFAmt23 = ICInstructionDetail.DetailFFAmt23
            objInstructionDetail.DetailFFAmt24 = ICInstructionDetail.DetailFFAmt24
            objInstructionDetail.DetailFFAmt25 = ICInstructionDetail.DetailFFAmt25
            objInstructionDetail.DetailFFAmt26 = ICInstructionDetail.DetailFFAmt26
            objInstructionDetail.DetailFFAmt27 = ICInstructionDetail.DetailFFAmt27
            objInstructionDetail.DetailFFAmt28 = ICInstructionDetail.DetailFFAmt28
            objInstructionDetail.DetailFFAmt29 = ICInstructionDetail.DetailFFAmt29
            objInstructionDetail.DetailFFAmt30 = ICInstructionDetail.DetailFFAmt30
            objInstructionDetail.DetailFFAmt31 = ICInstructionDetail.DetailFFAmt31
            objInstructionDetail.DetailFFAmt32 = ICInstructionDetail.DetailFFAmt32
            objInstructionDetail.DetailFFAmt33 = ICInstructionDetail.DetailFFAmt33
            objInstructionDetail.DetailFFAmt34 = ICInstructionDetail.DetailFFAmt34
            objInstructionDetail.DetailFFAmt35 = ICInstructionDetail.DetailFFAmt35
            objInstructionDetail.DetailFFAmt36 = ICInstructionDetail.DetailFFAmt36
            objInstructionDetail.DetailFFAmt37 = ICInstructionDetail.DetailFFAmt37
            objInstructionDetail.DetailFFAmt38 = ICInstructionDetail.DetailFFAmt38
            objInstructionDetail.DetailFFAmt39 = ICInstructionDetail.DetailFFAmt39
            objInstructionDetail.DetailFFAmt40 = ICInstructionDetail.DetailFFAmt40
            objInstructionDetail.DetailFFAmt41 = ICInstructionDetail.DetailFFAmt41
            objInstructionDetail.DetailFFAmt42 = ICInstructionDetail.DetailFFAmt42
            objInstructionDetail.DetailFFAmt43 = ICInstructionDetail.DetailFFAmt43
            objInstructionDetail.DetailFFAmt44 = ICInstructionDetail.DetailFFAmt44
            objInstructionDetail.DetailFFAmt45 = ICInstructionDetail.DetailFFAmt45
            objInstructionDetail.DetailFFAmt46 = ICInstructionDetail.DetailFFAmt46
            objInstructionDetail.DetailFFAmt47 = ICInstructionDetail.DetailFFAmt47
            objInstructionDetail.DetailFFAmt48 = ICInstructionDetail.DetailFFAmt48
            objInstructionDetail.DetailFFAmt49 = ICInstructionDetail.DetailFFAmt49
            objInstructionDetail.DetailFFAmt50 = ICInstructionDetail.DetailFFAmt50

            objInstructionDetail.Save()
            ActionString = "Instruction Detail [InstructionDetailID : " & objInstructionDetail.InstructionDetailID & "] of Instruction [ID: " & objInstructionDetail.InstructionID & "] added by User [UserID : " & UsersID & " ; UserName : " & UsersName & "]"
            ICUtilities.AddAuditTrail(ActionString.ToString(), "Instruction Detail", objInstructionDetail.InstructionDetailID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")

        End Sub
        Public Shared Function GetAllOnlineFormSettingFieldsTaggedWithAPNautueAndProductType(ByVal objICAPNature As ICAccountsPaymentNature, ByVal CompanyCode As String, ByVal ProductTypeCode As String) As ICOnlineFormSettingsCollection
            Dim objOnlineFormSettingColl As New ICOnlineFormSettingsCollection
            Dim dt As New DataTable
            objOnlineFormSettingColl.Query.Select(objOnlineFormSettingColl.Query.FieldID, objOnlineFormSettingColl.Query.IsRequired, objOnlineFormSettingColl.Query.IsVisible)
            objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.AccountNumber = objICAPNature.AccountNumber And objOnlineFormSettingColl.Query.BranchCode = objICAPNature.BranchCode And objOnlineFormSettingColl.Query.Currency = objICAPNature.Currency And objOnlineFormSettingColl.Query.PaymentNatureCode = objICAPNature.PaymentNatureCode)
            objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.CompanyCode = CompanyCode.ToString)
            objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.IsApproved = True)
            objOnlineFormSettingColl.Query.Where(objOnlineFormSettingColl.Query.ProductTypeCode = ProductTypeCode.ToString)
            objOnlineFormSettingColl.Query.OrderBy(objOnlineFormSettingColl.Query.FieldID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objOnlineFormSettingColl.Query.Load()
            Return objOnlineFormSettingColl
        End Function
        Public Shared Function GetInstructionForClearing(ByVal InstructionID As String) As ICInstructionCollection
            Dim objICInstruction As New ICInstructionCollection

            objICInstruction.Query.Where(objICInstruction.Query.InstructionID = InstructionID.ToString())
            objICInstruction.Query.Load()

            Return objICInstruction
        End Function
        'Public Shared Function GetAccountPaymentNatureTaggedWithUser(ByVal UserID As String, ByVal CompanyCode As String) As DataTable
        '    Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
        '    Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim dt As DataTable
        '    qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
        '    qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICUserRolesAPNature.PaymentNatureCode).As("AccountPaymentNature"))
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    dt = qryObjICUserRolesAPNature.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUser(ByVal UserID As String, ByVal CompanyCode As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICUserRolesAPNature.PaymentNatureCode).As("AccountPaymentNature"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICCompany.CompanyCode = CompanyCode.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)

            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserForDropDownForApprovalRule(ByVal UserID As String, ByVal CompanyCode As String, ByVal ArrayList As ArrayList, ByVal PaymentNatureList As ArrayList) As DataTable
            Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICUserRolesAPNature.PaymentNatureCode).As("AccountPaymentNature"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.CompanyCode = CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.PaymentNatureCode.In(PaymentNatureList))
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserForDropDown(ByVal UserID As String, ByVal CompanyCode As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICUserRolesAPNature.PaymentNatureCode).As("AccountPaymentNature"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.CompanyCode = CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserSecond(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICUserRolesAPNatureAndLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNatureAndLocation")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As DataTable
            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"), qryObjICUserRolesAPNature.PaymentNatureCode.As("PaymentNature"), qryObjICUserRolesAPNatureAndLocation.OfficeID.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'0'").As("OfficeID"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICUserRolesAPNatureAndLocation).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICUserRolesAPNatureAndLocation.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICUserRolesAPNatureAndLocation.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICUserRolesAPNatureAndLocation.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICUserRolesAPNatureAndLocation.PaymentNatureCode And qryObjICUserRolesAPNature.UserID = qryObjICUserRolesAPNatureAndLocation.UserID And qryObjICUserRolesAPNature.RolesID = qryObjICUserRolesAPNatureAndLocation.RoleID)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True And qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.GroupBy(qryObjICUserRolesAPNature.AccountNumber, qryObjICUserRolesAPNature.BranchCode, qryObjICUserRolesAPNature.Currency, qryObjICUserRolesAPNature.PaymentNatureCode, qryObjICUserRolesAPNatureAndLocation.OfficeID)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserSecondForApprovalRule(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal PaymentNatureCodeList As ArrayList) As DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICUserRolesAPNatureAndLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNatureAndLocation")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As DataTable
            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"), qryObjICUserRolesAPNature.PaymentNatureCode.As("PaymentNature"), qryObjICUserRolesAPNatureAndLocation.OfficeID.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'0'").As("OfficeID"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICUserRolesAPNatureAndLocation).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICUserRolesAPNatureAndLocation.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICUserRolesAPNatureAndLocation.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICUserRolesAPNatureAndLocation.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICUserRolesAPNatureAndLocation.PaymentNatureCode And qryObjICUserRolesAPNature.UserID = qryObjICUserRolesAPNatureAndLocation.UserID And qryObjICUserRolesAPNature.RolesID = qryObjICUserRolesAPNatureAndLocation.RoleID)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True And qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList) And qryObjICUserRolesAPNature.PaymentNatureCode.In(PaymentNatureCodeList))
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.GroupBy(qryObjICUserRolesAPNature.AccountNumber, qryObjICUserRolesAPNature.BranchCode, qryObjICUserRolesAPNature.Currency, qryObjICUserRolesAPNature.PaymentNatureCode, qryObjICUserRolesAPNatureAndLocation.OfficeID)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserSecondForFileUpload(ByVal UserID As String, ByVal ArrayListRoleID As ArrayList) As DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICUserRolesAPNatureAndLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNatureAndLocation")
            Dim dt As DataTable
            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"), qryObjICUserRolesAPNature.PaymentNatureCode.As("PaymentNature"))

            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True And qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayListRoleID))
            qryObjICUserRolesAPNature.GroupBy(qryObjICUserRolesAPNature.AccountNumber, qryObjICUserRolesAPNature.BranchCode, qryObjICUserRolesAPNature.Currency, qryObjICUserRolesAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountNumbersTaggedWithUser(ByVal UserID As String, ByVal CompanyCode As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select((qryObjICAccounts.AccountNumber + "-" + qryObjICAccounts.BranchCode + "-" + qryObjICAccounts.Currency).As("AccountNumber"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICCompany.CompanyCode = CompanyCode.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetAccountNumbersTaggedWithUserSecond(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable

        '    Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
        '    Dim dt As DataTable
        '    qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"))


        '    qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
        '    qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
        '    qryObjICUserRolesAPNature.es.Distinct = True
        '    dt = qryObjICUserRolesAPNature.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAccountNumbersTaggedWithUserSecond(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal CompanyCode As String) As DataTable

            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAPnature As New ICAccountsPaymentNatureQuery("qryObjICAPnature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.AccountNumber.As("MainAccountNo"), (qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPnature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPnature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPnature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPnature.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPnature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPnature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPnature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.CompanyCode = CompanyCode And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAccountNumbersTaggedWithUserSecondForApprovalRule(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal CompanyCode As String, ByVal PaymentNatureList As ArrayList) As DataTable

            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAPnature As New ICAccountsPaymentNatureQuery("qryObjICAPnature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.AccountNumber.As("MainAccountNo"), (qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPnature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPnature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPnature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPnature.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPnature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPnature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPnature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.CompanyCode = CompanyCode And qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.PaymentNatureCode.In(PaymentNatureList))
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetAllTaggedPrintLocationByAPNatureAndProductType(ByVal objICAPNature As ICAccountsPaymentNature, ByVal ProductTypeCode As String, ByVal RoleID As ArrayList) As DataTable
        '    Dim qryObjICAPNPTPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPTPrintLocation")
        '    Dim qryObjICUserRolesPrintLocationSecond As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICAPNPrintLocationsSeond")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim dt As New DataTable
        '    qryObjICAPNPTPrintLocation.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
        '    qryObjICAPNPTPrintLocation.InnerJoin(qryObjICOffice).On(qryObjICAPNPTPrintLocation.OfficeID = qryObjICOffice.OfficeID)
        '    qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.AccountNumber = objICAPNature.AccountNumber And qryObjICAPNPTPrintLocation.BranchCode = objICAPNature.BranchCode)
        '    qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.Currency = objICAPNature.Currency And qryObjICAPNPTPrintLocation.PaymentNatureCode = objICAPNature.PaymentNatureCode)
        '    qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.ProductTypeCode = ProductTypeCode.ToString)
        '    qryObjICAPNPTPrintLocation.Where(qryObjICOffice.OfficeID.In(qryObjICUserRolesPrintLocationSecond.[Select](qryObjICUserRolesPrintLocationSecond.OfficeID).Where(qryObjICUserRolesPrintLocationSecond.AccountNumber = qryObjICAPNPTPrintLocation.AccountNumber And qryObjICUserRolesPrintLocationSecond.BranchCode = qryObjICAPNPTPrintLocation.BranchCode And qryObjICUserRolesPrintLocationSecond.Currency = qryObjICAPNPTPrintLocation.Currency And qryObjICUserRolesPrintLocationSecond.RoleID.In(RoleID) And qryObjICUserRolesPrintLocationSecond.IsApprove = True)))
        '    dt = qryObjICAPNPTPrintLocation.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAllTaggedPrintLocationByAPNatureAndProductType(ByVal objICAPNature As ICAccountsPaymentNature, ByVal ProductTypeCode As String, ByVal RoleID As ArrayList) As DataTable
            Dim qryObjICAPNPTPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPTPrintLocation")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICUserRolesPrintLocationSecond As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICAPNPrintLocationsSeond")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim dt As New DataTable
            qryObjICAPNPTPrintLocation.Select(qryObjICOffice.OfficeID, (qryObjICOffice.OfficeCode.Coalesce("'-'") + " - " + qryObjICOffice.OfficeName.Coalesce("'-'")).As("OfficeName"))
            qryObjICAPNPTPrintLocation.InnerJoin(qryObjICAPNPType).On(qryObjICAPNPTPrintLocation.AccountNumber = qryObjICAPNPType.AccountNumber And qryObjICAPNPTPrintLocation.BranchCode = qryObjICAPNPType.BranchCode And qryObjICAPNPTPrintLocation.Currency = qryObjICAPNPType.Currency And qryObjICAPNPTPrintLocation.PaymentNatureCode = qryObjICAPNPType.PaymentNatureCode And qryObjICAPNPTPrintLocation.ProductTypeCode = qryObjICAPNPType.ProductTypeCode)
            qryObjICAPNPTPrintLocation.InnerJoin(qryObjICOffice).On(qryObjICAPNPTPrintLocation.OfficeID = qryObjICOffice.OfficeID)
            qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.AccountNumber = objICAPNature.AccountNumber And qryObjICAPNPTPrintLocation.BranchCode = objICAPNature.BranchCode)
            qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.Currency = objICAPNature.Currency And qryObjICAPNPTPrintLocation.PaymentNatureCode = objICAPNature.PaymentNatureCode)
            qryObjICAPNPTPrintLocation.Where(qryObjICAPNPTPrintLocation.ProductTypeCode = ProductTypeCode.ToString)
            qryObjICAPNPTPrintLocation.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICAPNPType.IsApproved = True)
            'qryObjICAPNPTPrintLocation.Where(qryObjICOffice.OfficeID.In(qryObjICUserRolesPrintLocationSecond.[Select](qryObjICUserRolesPrintLocationSecond.OfficeID).Where(qryObjICUserRolesPrintLocationSecond.AccountNumber = qryObjICAPNPTPrintLocation.AccountNumber And qryObjICUserRolesPrintLocationSecond.BranchCode = qryObjICAPNPTPrintLocation.BranchCode And qryObjICUserRolesPrintLocationSecond.Currency = qryObjICAPNPTPrintLocation.Currency And qryObjICUserRolesPrintLocationSecond.RoleID.In(RoleID) And qryObjICUserRolesPrintLocationSecond.IsApprove = True)))
            dt = qryObjICAPNPTPrintLocation.LoadDataTable
            Return dt
        End Function
        'Public Shared Sub GetAllInstructionsForRadGridForPrintingWithUserLocation(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String, ByVal UserOfficeCode As String)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status)
        '    qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)

        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date <= CDate(ToDate))
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
        '        End If

        '        If BatchCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.FileBatchNo = BatchCode.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If

        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstructionID = InstructionNo.ToString)
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '        End If
        '        If ReferenceNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ReferenceNo = ReferenceNo.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If

        '        If Not Amount = 0 Then
        '            If AmountOperator.ToString = "=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount = Amount)
        '            ElseIf AmountOperator.ToString = "<" Then
        '                ArrAND.Add(qryObjICInstruction.Amount < Amount)
        '            ElseIf AmountOperator.ToString = ">" Then
        '                ArrAND.Add(qryObjICInstruction.Amount > Amount)
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <= Amount)
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount >= Amount)
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <> Amount)
        '            End If
        '        End If
        '    End If

        '    If AccountNumber.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In AccountNumber.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstruction.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstruction.PaymentNatureCode = PaymentNature.ToString())

        '            If UserOfficeCode.ToString <> "" Then
        '                ArrAND.Add(qryObjICInstruction.PrintLocationCode = UserOfficeCode.ToString)
        '            End If
        '            If InstructionStatus.Count > 0 Then
        '                ArrAND.Add(qryObjICInstruction.Status.In(InstructionStatus))
        '            End If
        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstruction.And(objAdd2))

        '        Next
        '    End If


        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
        '    End If


        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)

        '        Next

        '    End If

        '    qryObjICInstruction.Where(qryObjICInstruction.And(objAdd), qryObjICInstruction.Or(objOr))
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    Dim str As String = qryObjICInstruction.es.LastQuery
        '    dt = New DataTable
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub

        Public Shared Sub GetAllInstructionsForRadGrid(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal CompanyCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal OfficeID As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList)
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable

            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
            qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status)
            qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ApprovalDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.ApprovalDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CancellationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.CancellationDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.LastPrintDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.LastPrintDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.RevalidationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.RevalidationDate.Date <= CDate(ToDate))
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.VerificationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        qryObjICInstruction.Where(qryObjICInstruction.VerificationDate.Date <= CDate(ToDate))
                    End If
                End If

                If CompanyCode.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
                End If
                If AccountNumber.ToString <> "" And BranchCode.ToString <> "" And Currency.ToString <> "" And PaymentNatureCode.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = AccountNumber.ToString And qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString And qryObjICInstruction.ClientAccountCurrency = Currency.ToString And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode.ToString)
                End If
                If ProductTypeCode.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
                End If
                If BatchCode.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.FileBatchNo = BatchCode.ToString)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
                End If
                If InstrumentNo.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
                End If
                If ReferenceNo.ToString <> "" Then
                    qryObjICInstruction.Where(qryObjICInstruction.ReferenceNo = ReferenceNo.ToString)
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount = Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount < Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount > Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount <= Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount >= Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        qryObjICInstruction.Where(qryObjICInstruction.Amount <> Amount)
                    End If
                End If
            End If
            If InstructionStatus.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.Status.In(InstructionStatus))
            End If
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
            End If
            If OfficeID.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.CreatedOfficeCode.In(OfficeID) Or qryObjICInstruction.CreateBy = UsersID.ToString)
            End If

            qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
            dt = qryObjICInstruction.LoadDataTable
            If Not CurrentPage = 0 Then
                qryObjICInstruction.es.PageNumber = CurrentPage
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICInstruction.es.PageNumber = 1
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub GetAllInstructionsForRadGridSecond(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String)
            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,IC_UBPSCompany.CompanyName,IC_Instruction.UBPReferenceField,"
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_UBPSCompany on IC_Instruction.UBPCompanyID=IC_UBPSCompany.UBPSCompanyID "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
            WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If BatchCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@BatchCode"
                    params.Add("BatchCode", BatchCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If
                If ReferenceNo.ToString <> "" Then
                    WhereClasue += " AND ReferenceNo=@ReferenceNo"
                    params.Add("ReferenceNo", ReferenceNo)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If InstructionStatus.Count > 0 Then
                WhereClasue += " AND IC_Instruction.Status IN ("
                For i = 0 To InstructionStatus.Count - 1
                    WhereClasue += InstructionStatus(i) & ","
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            WhereClasue += " AND ( "
            Dim x As Integer = 0
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " ( "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    'If x = 100 Then
                    '    x = 0
                    '    WhereClasue += ")"
                    '    WhereClasue += " ( "
                    '    WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                    'End If
                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
                    x = x + 1
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
                WhereClasue += " )"
                If AccountNumber(0)(3) = False Then
                    WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                    params.Add("UsersID", UsersID)
                End If
            Else
                WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                params.Add("UsersID", UsersID)
            End If
            WhereClasue += " ) "

            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClasue += "AND PaymentMode IN ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClasue += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        'Public Shared Sub GetAllInstructionsForRadGridSecond(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String)
        '    Dim dt As DataTable
        '    Dim StrQuery As String = Nothing
        '    Dim WhereClasue As String = Nothing
        '    WhereClasue = " Where "
        '    StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
        '    StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        '    StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        '    StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        '    StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        '    StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        '    StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        '    StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
        '    StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
        '    StrQuery += "IC_ProductType.ProductTypeName,"
        '    StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        '    StrQuery += "when 'false' then "
        '    StrQuery += "CONVERT(varchar,'false') "
        '    StrQuery += "else "
        '    StrQuery += "IsAmendmentComplete "
        '    StrQuery += " end "
        '    StrQuery += "as IsAmendmentComplete, "
        '    StrQuery += "case acquisitionmode "
        '    StrQuery += "when 'Online Form' then "
        '    StrQuery += "ic_instruction.FileBatchno else "
        '    StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
        '    StrQuery += "end as FileBatchNo "
        '    StrQuery += "from IC_Instruction "
        '    StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        '    StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        '    StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        '    StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        '    StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        '    StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
        '    WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
        '        End If
        '        If BatchCode.ToString <> "" Then
        '            WhereClasue += " AND IC_Instruction.FileBatchNo='" & BatchCode & "'"
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If
        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            WhereClasue += " AND InstructionID=" & InstructionNo & ""
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
        '        End If
        '        If ReferenceNo.ToString <> "" Then
        '            WhereClasue += " AND ReferenceNo='" & ReferenceNo & "'"
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If
        '        If Not Amount = 0 Then
        '            If AmountOperator.ToString = "=" Then
        '                WhereClasue += " AND Amount = " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<" Then
        '                WhereClasue += " AND Amount < " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">" Then
        '                WhereClasue += " AND Amount > " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                WhereClasue += " AND Amount <= " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                WhereClasue += " AND Amount >= " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                WhereClasue += " AND Amount <> " & Amount & ""
        '            End If
        '        End If
        '    End If
        '    If InstructionStatus.Count > 0 Then
        '        WhereClasue += " AND IC_Instruction.Status IN ("
        '        For i = 0 To InstructionStatus.Count - 1
        '            WhereClasue += InstructionStatus(i) & ","
        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '    End If
        '    WhereClasue += " AND ( "
        '    Dim x As Integer = 0
        '    If AccountNumber.Rows.Count > 0 Then
        '        WhereClasue += " ( "
        '        WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
        '        For Each dr As DataRow In AccountNumber.Rows
        '            'If x = 100 Then
        '            '    x = 0
        '            '    WhereClasue += ")"
        '            '    WhereClasue += " ( "
        '            '    WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
        '            'End If
        '            WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
        '            x = x + 1
        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '        WhereClasue += " )"
        '        If AccountNumber(0)(3) = False Then
        '            WhereClasue += " OR (IC_Instruction.CreateBy=" & UsersID & " )"
        '        End If
        '    Else
        '        WhereClasue += " OR (IC_Instruction.CreateBy=" & UsersID & " )"
        '    End If
        '    WhereClasue += " ) "

        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        WhereClasue += "AND PaymentMode IN ("
        '        For i = 0 To AssignedPaymentModes.Count - 1
        '            WhereClasue += "'" & AssignedPaymentModes(i) & "',"
        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '    End If
        '    If WhereClasue.Contains(" Where  AND ") = True Then
        '        WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        '    End If
        '    StrQuery += WhereClasue
        '    StrQuery += " Order By InstructionID Desc"
        '    Dim utill As New esUtility()
        '    dt = New DataTable
        '    dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        '    If Not CurrentPage = 0 Then

        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub

        
        Public Shared Sub GetInstructionsForAmendmentForRadGrid(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal OfficeID As ArrayList, ByVal UsersID As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String)
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim dt As New DataTable

            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date, qryObjICInstruction.PaymentNatureCode)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), (qryObjICOffice.OfficeCode.Coalesce("''") + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            qryObjICInstruction.Select(qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status, qryObjICInstruction.GroupCode)
            qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
            qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)

            qryObjICInstruction.Select(qryObjICInstruction.AcquisitionMode.Case.When(qryObjICInstruction.AcquisitionMode = "Online Form").Then(qryObjICInstruction.FileBatchNo).Else(qryObjICInstruction.FileBatchNo + "-" + qryObjICFiles.OriginalFileName).End().As("FileBatchNo"))
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            If CompanyCode <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode)
            End If
            If AccountNumber.ToString <> "" And BranchCode.ToString <> "" And Currency.ToString <> "" And PaymentNatureCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = AccountNumber.ToString And qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString And qryObjICInstruction.ClientAccountCurrency = Currency.ToString And qryObjICInstruction.PaymentNatureCode = PaymentNatureCode.ToString)
            End If
            If ProductTypeCode.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
            End If
            If InstructionNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            If InstructionStatus.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.Status.In(InstructionStatus))
            End If
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
            End If
            If OfficeID.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.CreatedOfficeCode.In(OfficeID) Or qryObjICInstruction.CreateBy = UsersID.ToString)
            Else
                qryObjICInstruction.Where(qryObjICInstruction.CreateBy = UsersID.ToString)
            End If
            qryObjICInstruction.Where(qryObjICInstruction.Status.NotIn("30", "21"))
            qryObjICInstruction.Where(qryObjICInstruction.PrintLocationCode.IsNotNull And qryObjICInstruction.IsPrintLocationAmended = False)
            qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
            dt = qryObjICInstruction.LoadDataTable
            If Not CurrentPage = 0 Then
                qryObjICInstruction.es.PageNumber = CurrentPage
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICInstruction.es.PageNumber = 1
                qryObjICInstruction.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub GetInstructionsForApprovalAfterAmendmentForRadGrid(ByVal dtAccountNumber As DataTable, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal AssignedPaymentModes As ArrayList)
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(UsersID.ToString)
            Dim ArrAND As New ArrayList
            Dim ArrOR As New ArrayList
            Dim dt As DataTable
            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,VerificationDate,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,ISNULL(co.OfficeCode + ' - ' + co.OfficeName,'-') as ChangedLocation,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete , "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PreviousPrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_Office as CO on IC_Instruction.AmendedPrintLocationCode=co.OfficeID "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            WhereClasue += " And IC_Instruction.Status Not IN (21,30) And IsPrintLocationAmended=1 And PrintLocationCode IS NULL "
            If dtAccountNumber.Rows.Count > 0 Then
                WhereClasue += " And ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + " & "'-'" & "+ Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In dtAccountNumber.Rows
                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += " )"
            End If

            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClasue += "AND PaymentMode IN ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClasue += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            If WhereClasue.Contains(" Where  And ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  And ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
            If Not CurrentPage = 0 Then
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        'Public Shared Sub GetInstructionsForApprovalAfterAmendmentForRadGrid(ByVal dtAccountNumber As DataTable, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal AssignedPaymentModes As ArrayList)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICOfficeChanged As New ICOfficeQuery("qryObjICOfficeChanged")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As DataTable
        '    'Dim dt As New DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICCompany.CompanyName, qryObjICInstruction.PaymentMode, qryObjICInstruction.ProductTypeCode)
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICProductType.ProductTypeName, qryObjICInstruction.BeneficiaryAccountNo, qryObjICInstruction.BeneAccountBranchCode, qryObjICInstruction.BeneAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICOffice.OffceType.Case.When(qryObjICOffice.OffceType = "Company Office").Then(qryObjICOffice.OfficeCode.Coalesce("''") + " - " + qryObjICOffice.OfficeName).Else(qryObjICOffice.OfficeCode.Coalesce("''") + " - " + qryObjICOffice.OfficeName).End.As("OfficeName"))
        '    qryObjICInstruction.Select(qryObjICOfficeChanged.OffceType.Case.When(qryObjICOfficeChanged.OffceType = "Company Office").Then(qryObjICOfficeChanged.OfficeCode.Coalesce("''") + " - " + qryObjICOfficeChanged.OfficeName).Else(qryObjICOfficeChanged.OfficeCode.Coalesce("''") + " - " + qryObjICOfficeChanged.OfficeName).End.As("ChangedLocation"))
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())

        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PreviousPrintLocationCode = qryObjICOffice.OfficeID)

        '    qryObjICInstruction.LeftJoin(qryObjICOfficeChanged).On(qryObjICInstruction.AmendedPrintLocationCode = qryObjICOfficeChanged.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)




        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
        '    End If

        '    If dtAccountNumber.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In dtAccountNumber.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString

        '            OfficeIDStr = dr("OfficeID").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstruction.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstruction.PaymentNatureCode = PaymentNature.ToString())
        '            arrAND2.Add(qryObjICInstruction.CreatedOfficeCode = OfficeIDStr.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstruction.And(objAdd2))

        '        Next
        '    End If

        '    ArrAND.Add(qryObjICInstruction.Status.NotIn(21, 30))
        '    ArrAND.Add(qryObjICInstruction.IsPrintLocationAmended = True)
        '    ArrAND.Add(qryObjICInstruction.PrintLocationCode.IsNull)
        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If


        '    'qryObjICInstruction.Where(qryObjICInstruction.Status <> "21" And qryObjICInstruction.Status <> "30")
        '    'qryObjICInstruction.Where(qryObjICInstruction.IsPrintLocationAmended = True And qryObjICInstruction.IsPrintLocationAmendmentCancelled = False And qryObjICInstruction.IsPrintLocationAmendmentApproved = False)
        '    qryObjICInstruction.Where(qryObjICInstruction.And(objAdd), qryObjICInstruction.Or(objOr))
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub
        Public Shared Function GetAllTaggedBatchNumbersByCompanyCode(ByVal CompanyCode As String, ByVal AssignedPaymentModes As ArrayList, ByVal AssignedOfficeIDs As ArrayList) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo)
            qryObjICInstruction.Where(qryObjICInstruction.CompanyCode = CompanyCode)
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
            End If
            If AssignedOfficeIDs.Count > 0 And Not AssignedOfficeIDs Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.CreatedOfficeCode.In(AssignedOfficeIDs))
            End If
            qryObjICInstruction.es.Distinct = True
            dt = qryObjICInstruction.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
            Dim StrQuery As String = Nothing
            Dim WhereClause As String = Nothing
            WhereClause = " Where "
            Dim dt As New DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
            StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClause += " And ins2.PaymentMode in ( '"
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClause += AssignedPaymentModes(i) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If ArrayListStatus.Count > 0 Then
                WhereClause += " And ins2.Status in ( "
                For i = 0 To ArrayListStatus.Count - 1
                    WhereClause += ArrayListStatus(i) & ","
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClause += " And ins2.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
            If dtAccountNo.Rows.Count > 0 Then
                WhereClause += " And ( "
                WhereClause += " ( "
                WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode +" & "'-'" & "+ Convert(Varchar,ins2.CreatedOfficeCode) IN ("
                For Each dr As DataRow In dtAccountNo.Rows
                    WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
                WhereClause += ")"
                WhereClause += " )"
                If dtAccountNo.Rows(0)(3).ToString = False Then
                    WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
                    params.Add("UserIDs", UserIDs)
                End If

            Else
                WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
                params.Add("UserIDs", UserIDs)
                'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
            End If

            If WhereClause.Contains(" Where  And") Then
                WhereClause = WhereClause.Replace(" Where  And", " Where ")
            End If
            StrQuery += WhereClause
            StrQuery += " ))"

            Dim util As New esUtility
            dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            Return dt
        End Function
        'Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
        '    Dim StrQuery As String = Nothing
        '    Dim WhereClause As String = Nothing
        '    WhereClause = " Where "
        '    Dim dt As New DataTable
        '    StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        '    StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        '    StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        '    StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        '    StrQuery += "(Select IC_Instruction.InstructionID from IC_Instruction as ins2 "
        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        WhereClause += " And ins2.PaymentMode in ( '"
        '        For i = 0 To AssignedPaymentModes.Count - 1
        '            WhereClause += AssignedPaymentModes(i) & "',"
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1)
        '        WhereClause += ")"
        '    End If
        '    If ArrayListStatus.Count > 0 Then
        '        WhereClause += " And ins2.Status in ( "
        '        For i = 0 To ArrayListStatus.Count - 1
        '            WhereClause += ArrayListStatus(i) & ","
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1)
        '        WhereClause += ")"
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        WhereClause += " And ins2.CompanyCode=" & CompanyCode
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        WhereClause += " AND ( "
        '        WhereClause += " ( "
        '        WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode +" & "'-'" & "+ Convert(Varchar,ins2.CreatedOfficeCode) IN ("
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
        '        WhereClause += ")"
        '        WhereClause += " )"
        '        If dtAccountNo.Rows(0)(3).ToString = False Then
        '            WhereClause += " OR (IC_Instruction.CreateBy=" & UserIDs & " )"
        '        End If

        '    Else
        '        WhereClause += " OR (IC_Instruction.CreateBy=" & UserIDs & " )"
        '        'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        '    End If

        '    If WhereClause.Contains(" Where  And") Then
        '        WhereClause = WhereClause.Replace(" Where  And", " Where ")
        '    End If
        '    StrQuery += WhereClause
        '    StrQuery += " ))"

        '    Dim util As New esUtility
        '    dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        '    Return dt
        'End Function
        
        Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
            Dim objICuser As New ICUser
            objICuser.LoadByPrimaryKey(UserIDs)
            Dim StrQuery As String = Nothing
            Dim WhereClause As String = Nothing
            WhereClause = " Where "
            Dim dt As New DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters
            StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
            StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClause += " And ins2.PaymentMode in ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClause += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If ArrayListStatus.Count > 0 Then
                WhereClause += " And ins2.Status in ( "
                For i = 0 To ArrayListStatus.Count - 1
                    WhereClause += ArrayListStatus(i) & ","
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClause += " And ins2.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            Else
                WhereClause += " And ins2.CompanyCode=0"
            End If
            If dtAccountNo.Rows.Count > 0 Then
                WhereClause += " AND ("
                WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
                For Each dr As DataRow In dtAccountNo.Rows
                    WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
                WhereClause += ")"
                WhereClause += " And PrintLocationCode=@OfficeCode"
                params.Add("OfficeCode", objICuser.OfficeCode.ToString)
            End If
            WhereClause += ")"
            If WhereClause.Contains(" Where  And") Then
                WhereClause = WhereClause.Replace(" Where  And", " Where ")
            End If
            StrQuery += WhereClause
            StrQuery += " ) Order By FileBatchNoName Asc"

            Dim util As New esUtility
            dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            Return dt
        End Function
        'Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICInstructionSecond As New ICInstructionQuery("qryObjICInstructionSecond")
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As New DataTable
        '    Dim objICuser As New ICUser
        '    objICuser.LoadByPrimaryKey(UserIDs)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo.Case.When(qryObjICInstruction.FileBatchNo <> "SI").Then((qryObjICInstruction.FileBatchNo + " - " + qryObjICFiles.OriginalFileName)).Else(qryObjICInstruction.FileBatchNo).End().As("FileBatchNoName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo)

        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstructionSecond.PaymentMode.In(AssignedPaymentModes))
        '    End If

        '    If ArrayListStatus.Count > 0 Then
        '        ArrAND.Add(qryObjICInstructionSecond.Status.In(ArrayListStatus))
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        ArrAND.Add(qryObjICInstructionSecond.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString

        '            OfficeIDStr = dr("OfficeID").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.PaymentNatureCode = PaymentNature.ToString())
        '            If OfficeIDStr.ToString <> "" Then
        '                arrAND2.Add(qryObjICInstruction.PrintLocationCode = objICuser.OfficeCode.ToString)
        '            End If
        '            If ArrayListStatus.Count > 0 Then
        '                arrAND2.Add(qryObjICInstruction.Status.In(ArrayListStatus))
        '            End If

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstructionSecond.And(objAdd2))

        '        Next
        '        If dtAccountNo.Rows(0)(3).ToString = False Then
        '            ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        End If

        '    Else
        '        ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        '    End If





        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If


        '    qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(qryObjICInstructionSecond.[Select](qryObjICInstructionSecond.InstructionID).Where(qryObjICInstructionSecond.And(objAdd), qryObjICInstructionSecond.Or(objOr))))

        '    qryObjICInstruction.es.Distinct = True

        '    dt = qryObjICInstruction.LoadDataTable

        '    Return dt
        'End Function
        'Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String, ByVal UserType As String, ByVal BankBrCode As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICInstructionSecond As New ICInstructionQuery("qryObjICInstructionSecond")
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo.Case.When(qryObjICInstruction.FileBatchNo <> "SI").Then((qryObjICInstruction.FileBatchNo + " - " + qryObjICFiles.OriginalFileName)).Else(qryObjICInstruction.FileBatchNo).End().As("FileBatchNoName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo)

        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstructionSecond.PaymentMode.In(AssignedPaymentModes))
        '    End If

        '    If ArrayListStatus.Count > 0 Then
        '        ArrAND.Add(qryObjICInstructionSecond.Status.In(ArrayListStatus))
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        ArrAND.Add(qryObjICInstructionSecond.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString
        '            If UserType = "Bank User" Then
        '                OfficeIDStr = BankBrCode
        '            Else
        '                OfficeIDStr = dr("OfficeID").ToString
        '            End If

        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.PaymentNatureCode = PaymentNature.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.CreatedOfficeCode = OfficeIDStr.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstructionSecond.And(objAdd2))

        '        Next
        '        If dtAccountNo.Rows(0)(3).ToString = False Then
        '            ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        End If

        '    Else
        '        ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        '    End If





        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If


        '    qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(qryObjICInstructionSecond.[Select](qryObjICInstructionSecond.InstructionID).Where(qryObjICInstructionSecond.And(objAdd), qryObjICInstructionSecond.Or(objOr))))

        '    qryObjICInstruction.es.Distinct = True

        '    dt = qryObjICInstruction.LoadDataTable

        '    Return dt
        'End Function
        'Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String, ByVal UserType As String, ByVal BankBrCode As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICInstructionSecond As New ICInstructionQuery("qryObjICInstructionSecond")
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo.Case.When(qryObjICInstruction.FileBatchNo <> "SI").Then((qryObjICInstruction.FileBatchNo + " - " + qryObjICFiles.OriginalFileName)).Else(qryObjICInstruction.FileBatchNo).End().As("FileBatchNoName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo)

        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstructionSecond.PaymentMode.In(AssignedPaymentModes))
        '    End If

        '    If ArrayListStatus.Count > 0 Then
        '        ArrAND.Add(qryObjICInstructionSecond.Status.In(ArrayListStatus))
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        ArrAND.Add(qryObjICInstructionSecond.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString
        '            If UserType = "Bank User" Then
        '                OfficeIDStr = BankBrCode
        '            Else
        '                OfficeIDStr = dr("OfficeID").ToString
        '            End If

        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.PaymentNatureCode = PaymentNature.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.PrintLocationCode = OfficeIDStr.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstructionSecond.And(objAdd2))

        '        Next
        '        If dtAccountNo.Rows(0)(3).ToString = False Then
        '            ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        End If

        '    Else
        '        ArrOR.Add(qryObjICInstructionSecond.CreateBy = UserIDs)
        '        'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        '    End If





        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If


        '    qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(qryObjICInstructionSecond.[Select](qryObjICInstructionSecond.InstructionID).Where(qryObjICInstructionSecond.And(objAdd), qryObjICInstructionSecond.Or(objOr))))

        '    qryObjICInstruction.es.Distinct = True

        '    dt = qryObjICInstruction.LoadDataTable

        '    Return dt
        'End Function



        Public Shared Function GetMaxBatchNumber() As Integer
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable

            Dim BatchNo As Integer = 0

            qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo.Max.Cast(EntitySpaces.DynamicQuery.esCastType.Int32))
            dt = qryObjICInstruction.LoadDataTable

            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    If dr("FileBatchNo").ToString = "" Then
                        BatchNo = 0
                    Else
                        BatchNo = CInt(dr("FileBatchNo"))
                    End If
                Next
            Else
                BatchNo = 0
            End If
            Return BatchNo
        End Function
        Public Shared Function GetInstructionApproverDetailsByInstructionID(ByVal InstructionID As String) As DataTable
            Dim qryObjICInstructionlog As New ICInstructionApprovalLogQuery("qryObjICInstructionlog")
            Dim qryObjICUserQuery As New ICUserQuery("qryObjICUserQuery")

            qryObjICInstructionlog.Select(qryObjICUserQuery.UserName, qryObjICUserQuery.DisplayName, qryObjICInstructionlog.ApprovalDateTime)
            qryObjICInstructionlog.LeftJoin(qryObjICUserQuery).On(qryObjICInstructionlog.UserID = qryObjICUserQuery.UserID)
            qryObjICInstructionlog.Where(qryObjICInstructionlog.InstructionID = InstructionID And qryObjICInstructionlog.IsAprrovalVoid = False)
            qryObjICInstructionlog.OrderBy(qryObjICInstructionlog.UserApprovalLogID.Ascending)
            Return qryObjICInstructionlog.LoadDataTable


        End Function
        Public Shared Function GetInstructionDetailsByInstructionID(ByVal InstructionID As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICBeneCountry As New ICCountryQuery("qryObjICBeneCountry")
            Dim qryObjICBeneProvince As New ICProvinceQuery("qryObjICBeneProvince")
            Dim qryObjICBeneCity As New ICCityQuery("qryObjICBeneCity")
            Dim qryObjICClientCountry As New ICCountryQuery("qryObjICClientCountry")
            Dim qryObjICClientProvince As New ICProvinceQuery("qryObjICClientProvince")
            Dim qryObjICClientCity As New ICCityQuery("qryObjICClientCity")
            Dim qryObjICStatus As New ICInstructionStatusQuery("qryObjICStatus")
            Dim qryObjICLastStatus As New ICInstructionStatusQuery("qryObjICLastStatus")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryObjICUserForDisbursed As New ICUserQuery("qryObjICUserForDisbursed")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICOfficeForPrintLocationCode As New ICOfficeQuery("qryObjICOfficeForPrintLocationCode")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjDDDrawnOffice As New ICOfficeQuery("qryObjDDDrawnOffice")
            Dim qryObjUBPCompany As New ICUBPSCompanyQuery("qryObjUBPCompany")


            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.Amount, qryObjICInstruction.AmountInWords, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryEmail.Coalesce("'-'"))
            qryObjICInstruction.Select((qryObjICInstruction.BeneficiaryAccountNo).As("BeneficiaryAccountNo").Coalesce("'-'"))
            qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo).As("ClientAccountNo").Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryMobile.Coalesce("'-'"), qryObjICInstruction.BeneficiaryBankCode.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryAccountTitle.Coalesce("'-'"), qryObjICInstruction.Description.Coalesce("'-'"), qryObjICInstruction.ClientName)
            qryObjICInstruction.Select(qryObjICInstruction.Remarks.Coalesce("'-'"), qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
            qryObjICInstruction.Select(qryObjICInstruction.FileID.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"), qryObjICInstruction.TransactionDate.Date, qryObjICInstruction.AcquisitionMode.As("AcqMode"))
            qryObjICInstruction.Select(qryObjICInstruction.ErrorMessage.Coalesce("'-'"), qryObjICStatus.StatusName.Coalesce("'-'").As("Status"), qryObjICLastStatus.StatusName.Coalesce("'-'").As("LastStatus"))
            qryObjICInstruction.Select((qryObjICUser.UserName + " - " + qryObjICUser.DisplayName).Coalesce("'-'").As("CreatedBy"), qryObjICInstruction.BeneficiaryCNIC.Coalesce("'-'"), qryObjICBank.BankName.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICBeneCountry.CountryName.Coalesce("'-'").As("BeneficiaryCountry"), qryObjICBeneProvince.ProvinceName.Coalesce("'-'").As("BeneficiaryProvince"), qryObjICBeneCity.CityName.Coalesce("'-'").As("BeneficiaryCity"))
            qryObjICInstruction.Select(qryObjICClientCountry.CountryName.Coalesce("'-'").As("ClientCountry"), qryObjICClientProvince.ProvinceName.Coalesce("'-'").As("ClientProvince"), qryObjICClientCity.CityName.Coalesce("'-'").As("ClientCity"))
            qryObjICInstruction.Select((qryObjICOfficeForPrintLocationCode.OfficeCode.Coalesce("'-'") + " - " + qryObjICOfficeForPrintLocationCode.OfficeName.Coalesce("'-'")).As("PrintLocationName"), qryObjICOffice.OfficeName.Coalesce("'-'").As("CreatedOfficeName"), qryObjICInstruction.FileBatchNo.Coalesce("'-'").As("FileBatchNo"))
            qryObjICInstruction.Select(qryObjICProductType.ProductTypeName, qryObjICInstruction.PrintDate.Date, qryObjICInstruction.SkipReason.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode.Case.When(qryObjICInstruction.PaymentMode = "Other Credit").Then("Inter Bank Fund Transfer").When(qryObjICInstruction.PaymentMode = "Direct Credit").Then("Fund Transfer").Else(qryObjICInstruction.PaymentMode).End.As("PaymentMode"))
            qryObjICInstruction.Select((qryObjDDDrawnOffice.OfficeCode + " - " + qryObjDDDrawnOffice.OfficeName).Coalesce("'-'").As("DrawnOnBranchCode"))
            qryObjICInstruction.Select(qryObjICInstruction.BeneficaryAccountType.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjUBPCompany.CompanyName, qryObjICInstruction.UBPReferenceField.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryIDNo.Coalesce("'-'"), qryObjICInstruction.BeneficiaryIDType.Coalesce("'-'"))
            qryObjICInstruction.LeftJoin(qryObjICStatus).On(qryObjICInstruction.Status = qryObjICStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICLastStatus).On(qryObjICInstruction.LastStatus = qryObjICLastStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICClientCountry).On(qryObjICInstruction.ClientCountry = qryObjICClientCountry.CountryCode)
            qryObjICInstruction.LeftJoin(qryObjICClientProvince).On(qryObjICInstruction.ClientProvince = qryObjICClientProvince.ProvinceCode)
            qryObjICInstruction.LeftJoin(qryObjICClientCity).On(qryObjICInstruction.ClientCity = qryObjICClientCity.CityCode)
            qryObjICInstruction.LeftJoin(qryObjICBeneCountry).On(qryObjICInstruction.BeneficiaryCountry = qryObjICBeneCountry.CountryCode)
            qryObjICInstruction.LeftJoin(qryObjICBeneProvince).On(qryObjICInstruction.BeneficiaryProvince = qryObjICBeneProvince.ProvinceCode)
            qryObjICInstruction.LeftJoin(qryObjICBeneCity).On(qryObjICInstruction.BeneficiaryCity = qryObjICBeneCity.CityCode)
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.CreatedOfficeCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
            qryObjICInstruction.LeftJoin(qryObjICUser).On(qryObjICInstruction.CreateBy = qryObjICUser.UserID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICOfficeForPrintLocationCode).On(qryObjICInstruction.PrintLocationCode = qryObjICOfficeForPrintLocationCode.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjDDDrawnOffice).On(qryObjICInstruction.DrawnOnBranchCode = qryObjDDDrawnOffice.OfficeID)
            qryObjICInstruction.LeftJoin(qryObjUBPCompany).On(qryObjICInstruction.UBPCompanyID = qryObjUBPCompany.UBPSCompanyID)
            'qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileBatchNo = qryObjICFiles.FileBatchNo)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionID.ToString)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetInstructionDetailsByInstructionIDForChequeReturnd(ByVal InstructionID As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.Amount, qryObjICInstruction.ProductTypeCode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"))
            qryObjICInstruction.Select((qryObjICOffice.OfficeCode + " - " + qryObjICOffice.OfficeName).As("OfficeName").Coalesce("'-'"), qryObjICInstruction.BeneficiaryAccountNo.Coalesce("'-'"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionID.ToString)

            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function

        Public Shared Sub UpdateInstructionStatus(ByVal InstructionID As String, ByVal FromStatus As String, ByVal ToStatus As String, ByVal UsersID As String, ByVal UsersName As String, ByVal RemarksType As String, ByVal ActionType As String, Optional ByVal Remarks As String = "")
            Dim objICInstruction As New ICInstruction
            Dim objICInstructionActivity As New ICInstructionActivity
            Dim objICStatus As New ICInstructionStatus
            Dim objICLastStatus As New ICInstructionStatus
            Dim StrAuditTrail As String = Nothing
            Dim StrInstructionActivity As String = Nothing


            objICInstruction.es.Connection.CommandTimeout = 3600
            objICInstructionActivity.es.Connection.CommandTimeout = 3600
            objICStatus.es.Connection.CommandTimeout = 3600
            objICLastStatus.es.Connection.CommandTimeout = 3600

            If objICInstruction.LoadByPrimaryKey(InstructionID) Then
                objICStatus.LoadByPrimaryKey(ToStatus)
                objICLastStatus.LoadByPrimaryKey(FromStatus)
                objICInstruction.Status = ToStatus
                objICInstruction.LastStatus = FromStatus

                If Remarks.ToString <> "" Then
                    If RemarksType.ToString = "Error" Then
                        objICInstruction.ErrorMessage = Remarks
                    ElseIf RemarksType.ToString = "Remarks" Then
                        objICInstruction.Remarks = Remarks
                    End If
                End If

                ''Release Date for PO and DD to mark Stale
                If ToStatus.ToString = "16" Then
                    If objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                        objICInstruction.PrintDate = Date.Now
                    End If
                End If
                If (ToStatus = "5" Or ToStatus = "19" Or ToStatus = "20" Or ToStatus = "24" Or ToStatus = "28" Or ToStatus = "32" Or ToStatus = "43") And objICInstruction.CancellationDate Is Nothing Then
                    objICInstruction.CancellationDate = Date.Now
                End If
                objICInstruction.Save()

                StrAuditTrail += "Instruction with ID: [ " & objICInstruction.InstructionID & " ] , Beneficiary name [ " & objICInstruction.BeneficiaryName & " ], Client Account number [ " & objICInstruction.ClientAccountNo & " ], Client Account Branch Code [ " & objICInstruction.ClientAccountBranchCode & " ], Currency [ " & objICInstruction.ClientAccountCurrency & " ] Status updated from [ " & FromStatus & " ] [ " & objICInstruction.UpToICInstructionStatusByLastStatus.StatusName & " ] ;"
                StrAuditTrail += " to Status [ " & objICInstruction.Status & " ] [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]"
                StrAuditTrail += "Action was taken by [ " & UsersID & " ][ " & UsersName & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail.ToString, "Instruction", objICInstruction.InstructionID.ToString, UsersID, UsersName, ActionType)

                objICInstructionActivity.Action = StrAuditTrail
                objICInstructionActivity.ActionBy = UsersID
                objICInstructionActivity.ActionDate = Date.Now
                objICInstructionActivity.InstructionID = objICInstruction.InstructionID
                objICInstructionActivity.FromStatus = FromStatus
                objICInstructionActivity.ToStatus = ToStatus
                objICInstructionActivity.CompanyCode = objICInstruction.CompanyCode
                objICInstructionActivity.BatchNo = objICInstruction.FileBatchNo
                If Not HttpContext.Current Is Nothing Then
                    objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If
                ICInstructionController.AddInstructionActivity(objICInstructionActivity, UsersID, UsersName, StrAuditTrail, "UPDATE")


                'Apply Charges - 22-Jan-2014 (Umer)


                ICPackageManagementController.ApplyCharges(InstructionID, objICInstruction.Status)







            End If
        End Sub

        Public Shared Function GetInstructionByInstrumentNoOrInstructionId(ByVal InstrumentNo As String, ByVal InstructionNo As String, ByVal ArrayListAssignedPaymentModes As ArrayList, ByVal InstructionStatus As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICProductType As New ICProductTypeQuery("")


            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICCompany.CompanyName, qryObjICInstruction.ClientAccountNo)
            qryObjICInstruction.Select(qryObjICInstruction.Amount, qryObjICInstruction.InstrumentNo, qryObjICInstruction.PrintLocationName)
            qryObjICInstruction.Select(qryObjICPaymentNature.PaymentNatureName, qryObjICProductType.ProductTypeName)




            qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.InnerJoin(qryObjICPaymentNature).On(qryObjICInstruction.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICInstruction.InnerJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            If InstructionNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionNo.ToString)
            End If
            If InstrumentNo.ToString <> "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
            End If
            qryObjICInstruction.Where(qryObjICInstruction.PaymentMode.In(ArrayListAssignedPaymentModes))

        End Function
        Public Shared Function GetAllInstructionByArrayListInstructionIDAndStatusAsDataTable(ByVal ArrayListInstructionNumber As ArrayList, ByVal PaymentMode As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber))
            End If
            If PaymentMode = "PO" Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode <> "Cheque")
            ElseIf PaymentMode = "Cheque" Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetAllInstructionByArrayListInstructionIDAndStatusAsDataTable(ByVal ArrayListInstructionNumber As ArrayList) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    If ArrayListInstructionNumber.Count > 0 Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber))
        '    End If
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Load()
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        'Public Shared Function GetAllInstructionByArrayListInstructionIDAndStatusAsDataTable(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    If ArrayListInstructionNumber.Count > 0 Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
        '    End If
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Load()
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAllInstructionWithAccntNoBranchCodeCurrencyAndInstructionIDArrList(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal InstructionIDArrayList As ArrayList, ByVal PaymentMode As String) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection
            If PaymentMode = "Cheque" Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode = PaymentMode)
            ElseIf PaymentMode = "PO" Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Cheque")
            Else
                objICInstructionColl.Query.Where(objICInstructionColl.Query.ProductTypeCode = PaymentMode)
            End If
            objICInstructionColl.es.Connection.CommandTimeout = 3600
            objICInstructionColl.Query.Where(objICInstructionColl.Query.ClientAccountNo = AccountNumber And objICInstructionColl.Query.ClientAccountBranchCode = BranchCode)
            objICInstructionColl.Query.Where(objICInstructionColl.Query.ClientAccountCurrency = Currency And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList))
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function
        'Public Shared Function GetAllInstructionWithInstructionIDArrListAndStatus(ByVal StatusID As String, ByVal InstructionIDArrayList As ArrayList) As ICInstructionCollection
        '    Dim objICInstructionColl As New ICInstructionCollection
        '    objICInstructionColl.es.Connection.CommandTimeout = 3600
        '    objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = StatusID And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList))
        '    objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    objICInstructionColl.Query.Load()
        '    Return objICInstructionColl
        'End Function
        ' Aizaz Ahmed Work [Dated: 14-Feb-2013]
        Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusAsDataTable(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")

            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode)

            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusAsDataTableForEmail(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
            qryObjICInstruction.Select(qryObjICInstruction.GroupCode)
            qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICInstruction.GroupCode)
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNo(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, (qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode).As("APNLocation"), qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
            qryObjICInstruction.Select(qryObjICInstruction.InstrumentNo)
            qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNo(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
        '    qryObjICInstruction.Select(qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
        '    qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

        '    If ArrayListInstructionNumber.Count > 0 Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
        '    End If
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
        '    qryObjICInstruction.Load()
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNoForClearingEmail(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"), qryObjICInstruction.ClientAccountNo, (qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode).As("APNLocation"), qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
            qryObjICInstruction.Select(qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
            qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNoForClearingEmail(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
        '    qryObjICInstruction.Select(qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
        '    qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

        '    If ArrayListInstructionNumber.Count > 0 Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status = Status)
        '    End If
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo, qryObjICInstruction.GroupCode)
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending)
        '    qryObjICInstruction.Load()
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        'Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusAsDataTableForEmailWithCount(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As ArrayList) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count().As("Count"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
        '    qryObjICInstruction.InnerJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

        '    If ArrayListInstructionNumber.Count > 0 Then
        '        qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status.In(Status))
        '    End If
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName)
        '    qryObjICInstruction.Load()
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusAsDataTableForEmailWithCount(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As ArrayList) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count().As("Count"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode.As("ProductTypeName"), qryObjICCompany.CompanyName)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber) And qryObjICInstruction.Status.In(Status))
            End If
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICCompany.CompanyName, qryObjICFiles.OriginalFileName, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllInstructionWithInstructionIDArrListAndStatus(ByVal StatusID As String, ByVal InstructionIDArrayList As ArrayList) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection
            objICInstructionColl.es.Connection.CommandTimeout = 3600
            objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = StatusID And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList))
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function
        Public Shared Function GetAllInstructionWithInstructionIDArrListStatusAndProductTypeCode(ByVal StatusID As String, ByVal InstructionIDArrayList As ArrayList, ByVal ProductTypeCode As String) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection
            objICInstructionColl.es.Connection.CommandTimeout = 3600
            objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = StatusID And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList) And objICInstructionColl.Query.ProductTypeCode = ProductTypeCode.ToString())
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function
        Public Shared Function GetAllInstructionWithInstructionIDArrListStatusProductTypeCodeAndDDPayableLocation(ByVal StatusID As String, ByVal InstructionIDArrayList As ArrayList, ByVal ProductTypeCode As String, ByVal DDPayableLocation As String) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection
            objICInstructionColl.es.Connection.CommandTimeout = 3600
            objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = StatusID And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList) And objICInstructionColl.Query.ProductTypeCode = ProductTypeCode.ToString() And objICInstructionColl.Query.DrawnOnBranchCode = DDPayableLocation.ToString())
            'objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = StatusID And objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList) And objICInstructionColl.Query.ProductTypeCode = ProductTypeCode.ToString() And objICInstructionColl.Query.DDPayableLocation = DDPayableLocation.ToString())
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function

        Public Shared Function GetInstructionsAccountAmountProductTypeAndDDPayableLocationWiseByArrayListInstructionIDAndStatus(ByVal ArrayListInstructionNumber As ArrayList, ByVal Status As String, ByVal ProductTypeCode As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.Amount.Sum().As("Amount"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICInstruction.DrawnOnBranchCode.As("DDPayableLocation"))
            If ArrayListInstructionNumber.Count > 0 Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionNumber))
            End If
            qryObjICInstruction.Where(qryObjICInstruction.Status = Status.ToString() And qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString())
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ProductTypeCode, qryObjICInstruction.DrawnOnBranchCode)
            qryObjICInstruction.Load()
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        ' Aizaz Ahmed Work [Dated: 26-Feb-2013]
        Public Shared Function GetInstructionForClearing(ByVal ID As String, ByVal IDType As String, ByVal PaymentMode As String) As ICInstruction
            Dim objICInstruction As New ICInstruction
            If IDType.ToString() = "InstructionID" Then
                objICInstruction.LoadByPrimaryKey(ID.ToString())
            ElseIf IDType.ToString() = "InstrumentNo" Then
                objICInstruction.Query.Where(objICInstruction.InstrumentNo = ID.ToString() And objICInstruction.PaymentMode = PaymentMode.ToString())
                objICInstruction.Query.Load()
            End If
            Return objICInstruction
        End Function
        Public Shared Function GetInstructionDetails(ByVal InstructionID As String) As DataTable
            Dim qryInstructionDetail As New ICInstructionDetailQuery("insd")

            qryInstructionDetail.Where(qryInstructionDetail.InstructionID = InstructionID.ToString())
            Return qryInstructionDetail.LoadDataTable()

        End Function
        'Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueue(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList) As DataTable
        '    Dim dt As New DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

        '    qryObjICInstruction.Select(qryObjICProductType.ProductTypeName, qryObjICInstruction.InstructionID.Count.As("Count"))
        '    qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName.Case.When(qryObjICInstructionStatus.StatusName = "Unverified").Then("Not Verified").Else("Verified").End().As("StatusName"))
        '    qryObjICInstruction.Select(qryObjICFiles.FileName.Case.When(qryObjICFiles.FileName.IsNull).Then("Online Form").Else(qryObjICFiles.FileName).End().As("FileName"))
        '    qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
        '    qryObjICInstruction.GroupBy(qryObjICFiles.FileName, qryObjICProductType.ProductTypeName, qryObjICInstructionStatus.StatusName)
        '    qryObjICInstruction.OrderBy(qryObjICFiles.FileName.Ascending)
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueue(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.InstructionID.Count.As("Count"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICCompany.CompanyName)
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICCompany.CompanyName)
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending, qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        ''Done Group Header
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueForReIssuance(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.InstructionID.Count.As("Count"))
            'qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName.Case.When(qryObjICInstructionStatus.StatusName = "Unverified").Then("Not Verified").Else(qryObjICInstructionStatus.StatusName).End().As("StatusName"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICCompany.CompanyName)
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstruction.Where(qryObjICInstruction.ReIssuanceID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.GroupCode)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICCompany.CompanyName)
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending, qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function

        Public Shared Function GetInstructionsCurrentStatusFromProcessingQueues(ByVal ArrayListInstructionID As ArrayList, ByVal ArrayListInstructionStatus As ArrayList, ByVal RequiredStatusName As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID)
            qryObjICInstruction.Select(qryObjICInstruction.Remarks.Case.When(qryObjICInstructionStatus.StatusName = RequiredStatusName.ToString).Then(qryObjICInstruction.Remarks).Else(qryObjICInstruction.ErrorMessage).End().As("Status"))
            qryObjICInstruction.InnerJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionID))
            qryObjICInstruction.Where(qryObjICInstructionStatus.StatusID.In(ArrayListInstructionStatus))
            qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID.Descending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt

        End Function
        Public Shared Function GetInstructionsByArrayInstructionIDGroupByAccountNo(ByVal ArrayListInstructionID As ArrayList, ByVal PrintLocationCode As String, ByVal PaymentMode As String) As DataTable
            Dim dt As New DataTable

            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.PrintLocationCode)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionID) And qryObjICInstruction.PrintLocationCode = PrintLocationCode And qryObjICInstruction.PaymentMode = PaymentMode)
            qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo.IsNull Or qryObjICInstruction.InstrumentNo = "")
            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.PrintLocationCode)
            dt = qryObjICInstruction.LoadDataTable
            Return dt



        End Function
        'Public Shared Function GetInstructionsByArrayInstructionIDGroupByAccountNo(ByVal ArrayListInstructionID As ArrayList, ByVal PrintLocationCode As String, ByVal PaymentMode As String) As DataTable
        '    Dim dt As New DataTable

        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"), qryObjICInstruction.ClientAccountNo, qryObjICInstruction.PrintLocationCode)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(ArrayListInstructionID) And qryObjICInstruction.PrintLocationCode = PrintLocationCode And qryObjICInstruction.PaymentMode = PaymentMode)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstrumentNo.IsNull)
        '    qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.PrintLocationCode)
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt



        'End Function
        Public Shared Function GetFinalDTForSummary(ByVal dt As DataTable) As DataTable
            Dim CurrentStatus As String = Nothing
            Dim Message As String = Nothing
            Dim InstructionID As String = Nothing
            Dim dtFinalForSummary As New DataTable
            Dim dtAfterSort As New DataTable
            Dim StatusFound As Boolean = False
            Dim drForFinaldt As DataRow

            Dim drAfterSortRows As DataRow()



            dtAfterSort.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtAfterSort.Columns.Add(New DataColumn("Status", GetType(System.String)))
            dtAfterSort.Columns.Add(New DataColumn("Message", GetType(System.String)))

            drAfterSortRows = dt.Select("", "Status ASC")
            For Each drSorted As DataRow In drAfterSortRows
                dtAfterSort.ImportRow(drSorted)

            Next
            Dim i As Integer = 0
            dtFinalForSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtFinalForSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
            dt.DefaultView.Sort = "Status ASC"
            For Each dr As DataRow In dtAfterSort.Rows
                i = i + 1
                StatusFound = False
                If CurrentStatus = dr("Status").ToString Then
                    InstructionID += dr("InstructionID").ToString & " ; "

                    For j As Integer = i To dtAfterSort.Rows.Count - 1
                        If dtAfterSort(j)(1).ToString = CurrentStatus Then
                            StatusFound = True
                        End If
                    Next
                    If StatusFound = False Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                        drForFinaldt("Message") = Message.ToString
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                    End If

                Else
                    If dtAfterSort.Rows.Count = 1 Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = dr("InstructionID").ToString
                        drForFinaldt("Message") = dr("Message").ToString
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                    Else
                        If Not InstructionID Is Nothing And Not Message Is Nothing Then
                            drForFinaldt = dtFinalForSummary.NewRow
                            drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                            drForFinaldt("Message") = Message.ToString
                            dtFinalForSummary.Rows.Add(drForFinaldt)
                        End If
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                        CurrentStatus = dr("Status")
                        InstructionID = dr("InstructionID").ToString & " ; "
                        Message = dr("Message").ToString
                        For j As Integer = i To dtAfterSort.Rows.Count - 1
                            If dtAfterSort(j)(1).ToString = CurrentStatus Then
                                StatusFound = True
                            End If
                        Next
                        If StatusFound = False Then
                            drForFinaldt = dtFinalForSummary.NewRow
                            drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                            drForFinaldt("Message") = Message.ToString
                            dtFinalForSummary.Rows.Add(drForFinaldt)
                            InstructionID = Nothing
                            Message = Nothing
                        End If
                    End If
                    CurrentStatus = Nothing
                    CurrentStatus = dr("Status").ToString
                End If

            Next
            Return dtFinalForSummary
        End Function
        Public Shared Function GetFinalDTForSummaryForBulkClearing(ByVal dt As DataTable) As DataTable
            Dim CurrentStatus As String = Nothing
            Dim Message As String = Nothing
            Dim InstructionID As String = Nothing
            Dim ChequeReturned As Boolean = Nothing
            Dim dtFinalForSummary As New DataTable
            Dim dtAfterSort As New DataTable
            Dim StatusFound As Boolean = False
            Dim drForFinaldt As DataRow

            Dim drAfterSortRows As DataRow()



            dtAfterSort.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtAfterSort.Columns.Add(New DataColumn("Status", GetType(System.String)))
            dtAfterSort.Columns.Add(New DataColumn("Message", GetType(System.String)))
            dtAfterSort.Columns.Add(New DataColumn("IsChequeReturned", GetType(System.Boolean)))

            drAfterSortRows = dt.Select("", "Status ASC")
            For Each drSorted As DataRow In drAfterSortRows
                dtAfterSort.ImportRow(drSorted)

            Next
            Dim i As Integer = 0
            dtFinalForSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtFinalForSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
            dtFinalForSummary.Columns.Add(New DataColumn("IsChequeReturned", GetType(System.Boolean)))
            dt.DefaultView.Sort = "Status ASC"
            For Each dr As DataRow In dtAfterSort.Rows
                i = i + 1
                StatusFound = False
                If CurrentStatus = dr("Status").ToString Then
                    InstructionID += dr("InstructionID").ToString & " ; "

                    For j As Integer = i To dtAfterSort.Rows.Count - 1
                        If dtAfterSort(j)(1).ToString = CurrentStatus Then
                            StatusFound = True
                        End If
                    Next
                    If StatusFound = False Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                        drForFinaldt("Message") = Message.ToString
                        drForFinaldt("IsChequeReturned") = ChequeReturned
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                    End If

                Else
                    If dtAfterSort.Rows.Count = 1 Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = dr("InstructionID").ToString
                        drForFinaldt("Message") = dr("Message").ToString
                        drForFinaldt("IsChequeReturned") = dr("IsChequeReturned")
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                    Else
                        If Not InstructionID Is Nothing And Not Message Is Nothing Then
                            drForFinaldt = dtFinalForSummary.NewRow
                            drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                            drForFinaldt("Message") = Message.ToString
                            drForFinaldt("IsChequeReturned") = ChequeReturned
                            dtFinalForSummary.Rows.Add(drForFinaldt)
                        End If
                        CurrentStatus = Nothing
                        InstructionID = Nothing
                        Message = Nothing
                        ChequeReturned = Nothing
                        CurrentStatus = dr("Status")
                        InstructionID = dr("InstructionID").ToString & " ; "
                        Message = dr("Message").ToString
                        ChequeReturned = dr("IsChequeReturned")



                        For j As Integer = i To dtAfterSort.Rows.Count - 1
                            If dtAfterSort(j)(1).ToString = CurrentStatus Then
                                StatusFound = True
                            End If
                        Next
                        If StatusFound = False Then
                            drForFinaldt = dtFinalForSummary.NewRow
                            drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                            drForFinaldt("Message") = Message.ToString
                            drForFinaldt("IsChequeReturned") = ChequeReturned
                            dtFinalForSummary.Rows.Add(drForFinaldt)
                            InstructionID = Nothing
                            Message = Nothing
                            ChequeReturned = Nothing
                        End If
                    End If
                    CurrentStatus = Nothing
                    CurrentStatus = dr("Status").ToString
                End If

            Next
            Return dtFinalForSummary
        End Function
        Public Shared Function GetAllGroupCodesByArrayListInstructionID(ByVal InstructionIDArraList As ArrayList) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.GroupCode)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArraList))
            qryObjICInstruction.OrderBy(qryObjICInstruction.GroupCode.Ascending)
            qryObjICInstruction.es.Distinct = True

            dt = qryObjICInstruction.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndGroupCode(ByVal InstructionIDArraList As ArrayList, ByVal StatusArryList As ArrayList, ByVal GroupCode As String) As String
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"), qryObjICInstruction.GroupCode)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArraList) And qryObjICInstruction.Status.In(StatusArryList) And qryObjICInstruction.GroupCode = GroupCode)
            qryObjICInstruction.GroupBy(qryObjICInstruction.GroupCode)

            dt = qryObjICInstruction.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return dt(0)(0).ToString
            Else
                Return "0"
            End If
        End Function
        Public Shared Function GetMaximumClubIDForClubPosting() As Integer
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.ClubID.Case.When(qryObjICInstruction.ClubID.Max.IsNull).Then(1).Else((qryObjICInstruction.ClubID.Max) + 1).End.As("ClubID"))
            dt = qryObjICInstruction.LoadDataTable()
            Return CInt(dt(0)(0))
        End Function
        Public Shared Function GetAllInstructionsForMarkAsStale() As ICInstructionCollection
            Dim StaleDate As Date = Nothing
            Dim arryInstructionID As New ArrayList
            Dim ArryListPaymentMode As New ArrayList
            ArryListPaymentMode.Add("PO")
            ArryListPaymentMode.Add("DD")
            ArryListPaymentMode.Add("Cheque")
            StaleDate = CDate(Now.Date.AddMonths(-6))
            Dim objICInstructionColl As ICInstructionCollection
            objICInstructionColl = Nothing
            objICInstructionColl = New ICInstructionCollection

            objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = 21)
            objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode.In(ArryListPaymentMode))
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
            objICInstructionColl.Query.Load()
            If objICInstructionColl.Query.Load Then
                For Each objICInstruction In objICInstructionColl
                    If Not objICInstruction.PrintDate Is Nothing Then
                        If CDate(objICInstruction.PrintDate) <= StaleDate Then
                            arryInstructionID.Add(objICInstruction.InstructionID)
                        End If
                    ElseIf Not objICInstruction.LastPrintDate Is Nothing Then
                        If CDate(objICInstruction.LastPrintDate) <= StaleDate Then
                            arryInstructionID.Add(objICInstruction.InstructionID)
                        End If
                    End If
                Next
            End If
            objICInstructionColl = Nothing
            objICInstructionColl = New ICInstructionCollection

            If arryInstructionID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(arryInstructionID))
                objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        End Function
        Public Shared Sub DeleteInstructionByFileIDInError(ByVal FileID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ActionString As String = Nothing
            Dim InstructionID As Integer = Nothing
            objICInstructionColl.Query.Where(objICInstructionColl.Query.FileID = FileID)
            objICInstructionColl.Query.Load()
            If objICInstructionColl.Count > 0 Then
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    Dim delobjICInstruction As New ICInstruction
                    ActionString = Nothing
                    InstructionID = Nothing
                    ActionString = " Instruction with ID [ " & objICInstruction.InstructionID & " ] deleted due to unsuccessfull completion of file parsing."
                    InstructionID = objICInstruction.InstructionID
                    DeleteInstructionActivityByInstructionID(InstructionID, UserID, UserName)
                    DeleteInstructionDetailByInstructionID(InstructionID, UserID, UserName)
                    delobjICInstruction.LoadByPrimaryKey(objICInstruction.InstructionID)
                    delobjICInstruction.MarkAsDeleted()
                    delobjICInstruction.Save()
                Next
            End If
        End Sub
        Public Shared Sub DeleteInstructionDetailByInstructionID(ByVal InstructionIDs As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICInstructionDetailColl As New ICInstructionDetailCollection
            Dim ActionString As String = Nothing
            Dim InstructionDetailID As Integer = Nothing
            objICInstructionDetailColl.Query.Where(objICInstructionDetailColl.Query.InstructionID = InstructionIDs)
            objICInstructionDetailColl.Query.Load()
            If objICInstructionDetailColl.Count > 0 Then
                For Each objICInstructionDetail As ICInstructionDetail In objICInstructionDetailColl
                    Dim delobjICInstructionDetail As New ICInstructionDetail
                    ActionString = Nothing
                    InstructionDetailID = Nothing
                    InstructionDetailID = objICInstructionDetail.InstructionDetailID
                    delobjICInstructionDetail.LoadByPrimaryKey(objICInstructionDetail.InstructionDetailID)
                    ActionString = " Instruction Detail with ID [ " & objICInstructionDetail.InstructionID & " ] for Instruction with ID [ " & InstructionIDs & " ] deleted."

                    delobjICInstructionDetail.MarkAsDeleted()
                    delobjICInstructionDetail.Save()
                    ICUtilities.AddAuditTrail(ActionString, "Instruction Detail", InstructionDetailID, UserID, UserName, "DELETE")
                Next
            End If
        End Sub
        Public Shared Sub DeleteInstructionActivityByInstructionID(ByVal InstructionIDs As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICInstructionActivtyColl As New ICInstructionActivityCollection
            Dim ActionString As String = Nothing
            Dim InstructionDetailID As Integer = Nothing
            objICInstructionActivtyColl.Query.Where(objICInstructionActivtyColl.Query.InstructionID = InstructionIDs)
            objICInstructionActivtyColl.Query.Load()
            If objICInstructionActivtyColl.Count > 0 Then
                For Each objICInstructionActivity As ICInstructionActivity In objICInstructionActivtyColl
                    Dim delobjICInstructionActivity As New ICInstructionActivity
                    ActionString = Nothing
                    InstructionDetailID = Nothing
                    InstructionDetailID = objICInstructionActivity.InstructionActivityID
                    delobjICInstructionActivity.LoadByPrimaryKey(objICInstructionActivity.InstructionActivityID)
                    ActionString = " Instruction Activty with ID [ " & objICInstructionActivity.InstructionActivityID & " ] for Instruction with ID [ " & InstructionIDs & " ] deleted."

                    delobjICInstructionActivity.MarkAsDeleted()
                    delobjICInstructionActivity.Save()
                    ICUtilities.AddAuditTrail(ActionString, "Instruction Activity", InstructionDetailID, UserID, UserName, "DELETE")
                Next
            End If
        End Sub
        'Public Shared Sub DeleteInstructionByFileIDInError(ByVal FileID As String, ByVal UserID As String, ByVal UserName As String)
        '    Dim objICInstructionColl As New ICInstructionCollection
        '    Dim ActionString As String = Nothing
        '    Dim InstructionID As Integer = Nothing
        '    objICInstructionColl.Query.Where(objICInstructionColl.Query.FileID = FileID)
        '    objICInstructionColl.Query.Load()
        '    If objICInstructionColl.Count > 0 Then
        '        For Each objICInstruction As ICInstruction In objICInstructionColl
        '            ActionString = Nothing
        '            InstructionID = Nothing
        '            ActionString = " Instruction with ID [ " & objICInstruction.InstructionID & " ] deleted due to unsuccessfull completion of file parsing."
        '            InstructionID = objICInstruction.InstructionID
        '            DeleteInstructionActivityByInstructionID(InstructionID, UserID, UserName)
        '            DeleteInstructionDetailByInstructionID(InstructionID, UserID, UserName)
        '            objICInstruction.MarkAsDeleted()
        '            objICInstruction.Save()
        '        Next
        '    End If
        'End Sub
        'Public Shared Sub DeleteInstructionDetailByInstructionID(ByVal InstructionIDs As String, ByVal UserID As String, ByVal UserName As String)
        '    Dim objICInstructionDetailColl As New ICInstructionDetailCollection
        '    Dim ActionString As String = Nothing
        '    Dim InstructionDetailID As Integer = Nothing
        '    objICInstructionDetailColl.Query.Where(objICInstructionDetailColl.Query.InstructionID = InstructionIDs)
        '    objICInstructionDetailColl.Query.Load()
        '    If objICInstructionDetailColl.Count > 0 Then
        '        For Each objICInstructionDetail As ICInstructionDetail In objICInstructionDetailColl
        '            ActionString = Nothing
        '            InstructionDetailID = Nothing
        '            InstructionDetailID = objICInstructionDetail.InstructionDetailID
        '            ActionString = " Instruction Detail with ID [ " & objICInstructionDetail.InstructionID & " ] for Instruction with ID [ " & InstructionIDs & " ] deleted."

        '            objICInstructionDetail.MarkAsDeleted()
        '            objICInstructionDetail.Save()
        '            ICUtilities.AddAuditTrail(ActionString, "Instruction Detail", InstructionDetailID, UserID, UserName, "DELETE")
        '        Next
        '    End If
        'End Sub
        'Public Shared Sub DeleteInstructionActivityByInstructionID(ByVal InstructionIDs As String, ByVal UserID As String, ByVal UserName As String)
        '    Dim objICInstructionActivtyColl As New ICInstructionActivityCollection
        '    Dim ActionString As String = Nothing
        '    Dim InstructionDetailID As Integer = Nothing
        '    objICInstructionActivtyColl.Query.Where(objICInstructionActivtyColl.Query.InstructionID = InstructionIDs)
        '    objICInstructionActivtyColl.Query.Load()
        '    If objICInstructionActivtyColl.Count > 0 Then
        '        For Each objICInstructionActivity As ICInstructionActivity In objICInstructionActivtyColl
        '            ActionString = Nothing
        '            InstructionDetailID = Nothing
        '            InstructionDetailID = objICInstructionActivity.InstructionActivityID
        '            ActionString = " Instruction Activty with ID [ " & objICInstructionActivity.InstructionActivityID & " ] for Instruction with ID [ " & InstructionIDs & " ] deleted."


        '            ICUtilities.AddAuditTrail(ActionString, "Instruction Activity", InstructionDetailID, UserID, UserName, "DELETE")
        '        Next
        '        objICInstructionActivtyColl.MarkAllAsDeleted()
        '        objICInstructionActivtyColl.Save()
        '    End If
        'End Sub
        Public Shared Function GetInstructionDetailsForDualTemplate(ByVal InstructionID As String) As DataTable
            Dim qryInstructionDetail As New ICInstructionDetailQuery("insd")

            qryInstructionDetail.Select(qryInstructionDetail.DetailAmount, qryInstructionDetail.DetailBeneAccountNo, qryInstructionDetail.DetailBeneAddress, qryInstructionDetail.DetailBeneBankName)
            qryInstructionDetail.Select(qryInstructionDetail.DetailBeneBranchAddress, qryInstructionDetail.DetailBeneBranchCode, qryInstructionDetail.DetailBeneBranchName, qryInstructionDetail.DetailBeneCNIC)
            qryInstructionDetail.Select(qryInstructionDetail.DetailBeneCountry, qryInstructionDetail.DetailBeneProvince, qryInstructionDetail.DetailBeneCity, qryInstructionDetail.DetailBeneEmail, qryInstructionDetail.DetailDetailAmt)
            qryInstructionDetail.Select(qryInstructionDetail.DetailBeneEmail, qryInstructionDetail.DetailBenePhone, qryInstructionDetail.DetailInvoiceNo, qryInstructionDetail.DetailISNO, qryInstructionDetail.DetailTXNCODE)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText1, qryInstructionDetail.DetailFFText2, qryInstructionDetail.DetailFFText3, qryInstructionDetail.DetailFFText4, qryInstructionDetail.DetailFFText5)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText6, qryInstructionDetail.DetailFFText7, qryInstructionDetail.DetailFFText8, qryInstructionDetail.DetailFFText9, qryInstructionDetail.DetailFFText10)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt1, qryInstructionDetail.DetailFFAmt2, qryInstructionDetail.DetailFFAmt3, qryInstructionDetail.DetailFFAmt4, qryInstructionDetail.DetailFFAmt5)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt6, qryInstructionDetail.DetailFFAmt7, qryInstructionDetail.DetailFFAmt8, qryInstructionDetail.DetailFFAmt9, qryInstructionDetail.DetailFFAmt10)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt11, qryInstructionDetail.DetailFFAmt12, qryInstructionDetail.DetailFFAmt13, qryInstructionDetail.DetailFFAmt14, qryInstructionDetail.DetailFFAmt15)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt16, qryInstructionDetail.DetailFFAmt17, qryInstructionDetail.DetailFFAmt18, qryInstructionDetail.DetailFFAmt19, qryInstructionDetail.DetailFFAmt20)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt21, qryInstructionDetail.DetailFFAmt22, qryInstructionDetail.DetailFFAmt23, qryInstructionDetail.DetailFFAmt24, qryInstructionDetail.DetailFFAmt25)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt26, qryInstructionDetail.DetailFFAmt27, qryInstructionDetail.DetailFFAmt28, qryInstructionDetail.DetailFFAmt29, qryInstructionDetail.DetailFFAmt30)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt31, qryInstructionDetail.DetailFFAmt32, qryInstructionDetail.DetailFFAmt33, qryInstructionDetail.DetailFFAmt34, qryInstructionDetail.DetailFFAmt35)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt36, qryInstructionDetail.DetailFFAmt37, qryInstructionDetail.DetailFFAmt38, qryInstructionDetail.DetailFFAmt39, qryInstructionDetail.DetailFFAmt40)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt41, qryInstructionDetail.DetailFFAmt42, qryInstructionDetail.DetailFFAmt43, qryInstructionDetail.DetailFFAmt44, qryInstructionDetail.DetailFFAmt45)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFAmt46, qryInstructionDetail.DetailFFAmt47, qryInstructionDetail.DetailFFAmt48, qryInstructionDetail.DetailFFAmt49, qryInstructionDetail.DetailFFAmt50)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText11, qryInstructionDetail.DetailFFText12, qryInstructionDetail.DetailFFText13, qryInstructionDetail.DetailFFText14, qryInstructionDetail.DetailFFText15)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText16, qryInstructionDetail.DetailFFText17, qryInstructionDetail.DetailFFText18, qryInstructionDetail.DetailFFText19, qryInstructionDetail.DetailFFText20)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText21, qryInstructionDetail.DetailFFText22, qryInstructionDetail.DetailFFText23, qryInstructionDetail.DetailFFText24, qryInstructionDetail.DetailFFText25)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText26, qryInstructionDetail.DetailFFText27, qryInstructionDetail.DetailFFText28, qryInstructionDetail.DetailFFText29, qryInstructionDetail.DetailFFText30)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText31, qryInstructionDetail.DetailFFText32, qryInstructionDetail.DetailFFText33, qryInstructionDetail.DetailFFText34, qryInstructionDetail.DetailFFText35)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText36, qryInstructionDetail.DetailFFText37, qryInstructionDetail.DetailFFText38, qryInstructionDetail.DetailFFText39, qryInstructionDetail.DetailFFText40)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText41, qryInstructionDetail.DetailFFText42, qryInstructionDetail.DetailFFText43, qryInstructionDetail.DetailFFText44, qryInstructionDetail.DetailFFText45)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText46, qryInstructionDetail.DetailFFText47, qryInstructionDetail.DetailFFText48, qryInstructionDetail.DetailFFText49, qryInstructionDetail.DetailFFText50)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText51, qryInstructionDetail.DetailFFText52, qryInstructionDetail.DetailFFText53, qryInstructionDetail.DetailFFText54, qryInstructionDetail.DetailFFText55)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText56, qryInstructionDetail.DetailFFText57, qryInstructionDetail.DetailFFText58, qryInstructionDetail.DetailFFText59, qryInstructionDetail.DetailFFText60)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText61, qryInstructionDetail.DetailFFText62, qryInstructionDetail.DetailFFText63, qryInstructionDetail.DetailFFText64, qryInstructionDetail.DetailFFText65)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText66, qryInstructionDetail.DetailFFText67, qryInstructionDetail.DetailFFText68, qryInstructionDetail.DetailFFText69, qryInstructionDetail.DetailFFText70)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText71, qryInstructionDetail.DetailFFText72, qryInstructionDetail.DetailFFText73, qryInstructionDetail.DetailFFText74, qryInstructionDetail.DetailFFText75)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText76, qryInstructionDetail.DetailFFText77, qryInstructionDetail.DetailFFText78, qryInstructionDetail.DetailFFText79, qryInstructionDetail.DetailFFText80)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText81, qryInstructionDetail.DetailFFText82, qryInstructionDetail.DetailFFText83, qryInstructionDetail.DetailFFText84, qryInstructionDetail.DetailFFText85)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText86, qryInstructionDetail.DetailFFText87, qryInstructionDetail.DetailFFText88, qryInstructionDetail.DetailFFText89, qryInstructionDetail.DetailFFText90)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText91, qryInstructionDetail.DetailFFText92, qryInstructionDetail.DetailFFText93, qryInstructionDetail.DetailFFText94, qryInstructionDetail.DetailFFText95)
            qryInstructionDetail.Select(qryInstructionDetail.DetailFFText96, qryInstructionDetail.DetailFFText97, qryInstructionDetail.DetailFFText98, qryInstructionDetail.DetailFFText99, qryInstructionDetail.DetailFFText100)




            qryInstructionDetail.Where(qryInstructionDetail.InstructionID = InstructionID.ToString())
            Return qryInstructionDetail.LoadDataTable()

        End Function
        Public Shared Function GetInstructionDetailsForHeaderTemplate(ByVal InstructionID As String) As DataTable
            Dim qryInstructionDetail As New ICInstructionDetailQuery("insd")

            qryInstructionDetail.Select(qryInstructionDetail.FFAmt1, qryInstructionDetail.FFAmt2, qryInstructionDetail.FFAmt3, qryInstructionDetail.FFAmt4, qryInstructionDetail.FFAmt5)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt6, qryInstructionDetail.FFAmt7, qryInstructionDetail.FFAmt8, qryInstructionDetail.FFAmt9, qryInstructionDetail.FFAmt10)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt11, qryInstructionDetail.FFAmt12, qryInstructionDetail.FFAmt13, qryInstructionDetail.FFAmt14, qryInstructionDetail.FFAmt15)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt16, qryInstructionDetail.FFAmt17, qryInstructionDetail.FFAmt18, qryInstructionDetail.FFAmt19, qryInstructionDetail.FFAmt20)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt1, qryInstructionDetail.FFAmt22, qryInstructionDetail.FFAmt23, qryInstructionDetail.FFAmt24, qryInstructionDetail.FFAmt25)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt26, qryInstructionDetail.FFAmt27, qryInstructionDetail.FFAmt28, qryInstructionDetail.FFAmt29, qryInstructionDetail.FFAmt30)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt31, qryInstructionDetail.FFAmt32, qryInstructionDetail.FFAmt33, qryInstructionDetail.FFAmt34, qryInstructionDetail.FFAmt35)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt36, qryInstructionDetail.FFAmt37, qryInstructionDetail.FFAmt38, qryInstructionDetail.FFAmt39, qryInstructionDetail.FFAmt40)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt41, qryInstructionDetail.FFAmt42, qryInstructionDetail.FFAmt43, qryInstructionDetail.FFAmt44, qryInstructionDetail.FFAmt45)
            qryInstructionDetail.Select(qryInstructionDetail.FFAmt46, qryInstructionDetail.FFAmt47, qryInstructionDetail.FFAmt48, qryInstructionDetail.FFAmt49, qryInstructionDetail.FFAmt50)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt1, qryInstructionDetail.FFTxt2, qryInstructionDetail.FFTxt3, qryInstructionDetail.FFTxt4, qryInstructionDetail.FFTxt5)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt6, qryInstructionDetail.FFTxt7, qryInstructionDetail.FFTxt8, qryInstructionDetail.FFTxt9, qryInstructionDetail.FFTxt10)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt11, qryInstructionDetail.FFTxt12, qryInstructionDetail.FFTxt13, qryInstructionDetail.FFTxt14, qryInstructionDetail.FFTxt15)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt16, qryInstructionDetail.FFTxt17, qryInstructionDetail.FFTxt18, qryInstructionDetail.FFTxt19, qryInstructionDetail.FFTxt20)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt21, qryInstructionDetail.FFTxt22, qryInstructionDetail.FFTxt23, qryInstructionDetail.FFTxt24, qryInstructionDetail.FFTxt25)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt26, qryInstructionDetail.FFTxt27, qryInstructionDetail.FFTxt28, qryInstructionDetail.FFTxt29, qryInstructionDetail.FFTxt30)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt31, qryInstructionDetail.FFTxt32, qryInstructionDetail.FFTxt33, qryInstructionDetail.FFTxt34, qryInstructionDetail.FFTxt35)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt36, qryInstructionDetail.FFTxt37, qryInstructionDetail.FFTxt38, qryInstructionDetail.FFTxt39, qryInstructionDetail.FFTxt40)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt41, qryInstructionDetail.FFTxt42, qryInstructionDetail.FFTxt43, qryInstructionDetail.FFTxt44, qryInstructionDetail.FFTxt45)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt46, qryInstructionDetail.FFTxt47, qryInstructionDetail.FFTxt48, qryInstructionDetail.FFTxt49, qryInstructionDetail.FFTxt50)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt51, qryInstructionDetail.FFTxt52, qryInstructionDetail.FFTxt53, qryInstructionDetail.FFTxt54, qryInstructionDetail.FFTxt55)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt56, qryInstructionDetail.FFTxt57, qryInstructionDetail.FFTxt58, qryInstructionDetail.FFTxt59, qryInstructionDetail.FFTxt60)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt61, qryInstructionDetail.FFTxt62, qryInstructionDetail.FFTxt63, qryInstructionDetail.FFTxt64, qryInstructionDetail.FFTxt65)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt66, qryInstructionDetail.FFTxt67, qryInstructionDetail.FFTxt68, qryInstructionDetail.FFTxt69, qryInstructionDetail.FFTxt70)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt71, qryInstructionDetail.FFTxt72, qryInstructionDetail.FFTxt73, qryInstructionDetail.FFTxt74, qryInstructionDetail.FFTxt75)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt76, qryInstructionDetail.FFTxt77, qryInstructionDetail.FFTxt78, qryInstructionDetail.FFTxt79, qryInstructionDetail.FFTxt70)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt71, qryInstructionDetail.FFTxt72, qryInstructionDetail.FFTxt73, qryInstructionDetail.FFTxt74, qryInstructionDetail.FFTxt75)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt76, qryInstructionDetail.FFTxt77, qryInstructionDetail.FFTxt78, qryInstructionDetail.FFTxt79, qryInstructionDetail.FFTxt80)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt81, qryInstructionDetail.FFTxt82, qryInstructionDetail.FFTxt83, qryInstructionDetail.FFTxt84, qryInstructionDetail.FFTxt85)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt86, qryInstructionDetail.FFTxt87, qryInstructionDetail.FFTxt88, qryInstructionDetail.FFTxt89, qryInstructionDetail.FFTxt90)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt91, qryInstructionDetail.FFTxt92, qryInstructionDetail.FFTxt93, qryInstructionDetail.FFTxt94, qryInstructionDetail.FFTxt95)
            qryInstructionDetail.Select(qryInstructionDetail.FFTxt96, qryInstructionDetail.FFTxt97, qryInstructionDetail.FFTxt98, qryInstructionDetail.FFTxt99, qryInstructionDetail.FFTxt100)
            qryInstructionDetail.Where(qryInstructionDetail.InstructionID = InstructionID.ToString())
            Return qryInstructionDetail.LoadDataTable()

        End Function
        Public Shared Sub UpdateInstructionSkipReason(ByVal objICInstructionsForUpdate As ICInstruction, ByVal UsersID As String, ByVal UsersName As String, Optional ByVal CustomMessage As String = "")
            Dim objICInstruction As New ICInstruction
            objICInstruction.LoadByPrimaryKey(objICInstructionsForUpdate.InstructionID)
            If CustomMessage <> "" Then
                objICInstruction.SkipReason = CustomMessage
            Else
                objICInstruction.SkipReason = objICInstructionsForUpdate.SkipReason
            End If
            objICInstruction.Save()
            ICUtilities.AddAuditTrail(objICInstructionsForUpdate.SkipReason, "Instruction", objICInstruction.InstructionID.ToString, UsersID, UsersName, "SKIP")
        End Sub
        'Public Shared Sub GetAllInstructionsForStatusChangeQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal Status As String)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), (qryObjICOffice.OfficeCode.Coalesce("'-'") + " - " + qryObjICOffice.OfficeName.Coalesce("'-'")).As("OfficeName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date, qryObjICInstruction.BeneficiaryAccountNo.Coalesce("'-'"), qryObjICInstruction.BeneAccountBranchCode.Coalesce("'-'"), qryObjICInstruction.BeneAccountCurrency.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status)
        '    qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date <= CDate(ToDate))
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
        '        End If
        '        If BatchCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.FileBatchNo = BatchCode.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If
        '        If Status <> "" Then
        '            ArrAND.Add(qryObjICInstruction.Status = Status)
        '        End If
        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstructionID = InstructionNo.ToString)
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '        End If
        '        If ReferenceNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ReferenceNo = ReferenceNo.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If
        '        If Not Amount = 0 Then
        '            If AmountOperator.ToString = "=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount = Amount)
        '            ElseIf AmountOperator.ToString = "<" Then
        '                ArrAND.Add(qryObjICInstruction.Amount < Amount)
        '            ElseIf AmountOperator.ToString = ">" Then
        '                ArrAND.Add(qryObjICInstruction.Amount > Amount)
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <= Amount)
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount >= Amount)
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <> Amount)
        '            End If
        '        End If
        '    End If
        '    If InstructionStatus.Count > 0 Then
        '        ArrAND.Add(qryObjICInstruction.Status.NotIn(InstructionStatus))
        '    End If
        '    ArrAND.Add(qryObjICInstruction.IsPrintLocationAmended = False)
        '    If AccountNumber.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In AccountNumber.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString

        '            OfficeIDStr = dr("OfficeID").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstruction.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstruction.PaymentNatureCode = PaymentNature.ToString())
        '            arrAND2.Add(qryObjICInstruction.CreatedOfficeCode = OfficeIDStr.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstruction.And(objAdd2))

        '        Next
        '    End If


        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If
        '    qryObjICInstruction.Where(qryObjICInstruction.And(objAdd), qryObjICInstruction.Or(objOr))
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    Dim str As String = qryObjICInstruction.es.LastQuery
        '    dt = New DataTable
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub
        Public Shared Sub GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueueForRevertFirstLeg(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal FileBatchNo As String)
            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,IC_Instruction.Amount,StatusName, "
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(IC_Instruction.InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,ic_instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName,case isnull(IC_FirstLegReversedInstructions.IsFirstLEgReversed,0) when 0  then 0 else IC_FirstLegReversedInstructions.IsFirstLEgReversed end as IsFirstLEgReversed,FirstLegReversedID "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            StrQuery += "left join IC_FirstLegReversedInstructions on IC_Instruction.InstructionID=IC_FirstLegReversedInstructions.RelatedID "
            StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
            WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=@Status"
                    params.Add("Status", Status)
                End If
                If FileBatchNo <> "0" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@FileBatchNo"
                    params.Add("FileBatchNo", FileBatchNo)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=@Status"
                    params.Add("Status", Status)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount < 1 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If

            WhereClasue += " AND ( "

            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " ( "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + ic_instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows

                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
                WhereClasue += " )"
                If AccountNumber(0)(3) = False Then
                    WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                    params.Add("UsersID", UsersID)
                End If
            Else
                WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                params.Add("UsersID", UsersID)
            End If
            WhereClasue += " ) "

            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal FileBatchNo As String)
            Dim dt As DataTable
            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName, "
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,ic_instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
            WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=" & Status & " "
                End If
                If FileBatchNo <> "0" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo='" & FileBatchNo & "' "
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=" & InstructionNo & ""
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=" & Status & " "
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
                End If
                If Not Amount < 1 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = " & Amount & ""
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < " & Amount & ""
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > " & Amount & ""
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= " & Amount & ""
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= " & Amount & ""
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> " & Amount & ""
                    End If
                End If
            End If

            WhereClasue += " AND ( "

            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " ( "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + ic_instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows

                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
                WhereClasue += " )"
                If AccountNumber(0)(3) = False Then
                    WhereClasue += " OR (IC_Instruction.CreateBy=" & UsersID & " )"
                End If
            Else
                WhereClasue += " OR (IC_Instruction.CreateBy=" & UsersID & " )"
            End If
            WhereClasue += " ) "

            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForStatusChangeQueue(ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList) As DataTable

            Dim StrQuery As String = Nothing
            Dim WhereClause As String = Nothing
            WhereClause = " Where "
            Dim dt As New DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
            StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
            If ArrayListStatus.Count > 0 Then
                WhereClause += " And ins2.Status NOT IN ( "
                For i = 0 To ArrayListStatus.Count - 1
                    WhereClause += ArrayListStatus(i) & ","
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClause += " And ins2.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            Else
                WhereClause += " And ins2.CompanyCode=0"
            End If
            If dtAccountNo.Rows.Count > 0 Then
                WhereClause += " AND "
                WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode +" & "'-'" & "+ Convert(Varchar,ins2.CreatedOfficeCode) IN ("
                For Each dr As DataRow In dtAccountNo.Rows
                    WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeId").ToString & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
                WhereClause += ")"
            End If
            If WhereClause.Contains(" Where  And") Then
                WhereClause = WhereClause.Replace(" Where  And", " Where ")
            End If
            StrQuery += WhereClause
            StrQuery += " ) Order By FileBatchNoName Asc"

            Dim util As New esUtility
            dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            Return dt
        End Function
        'Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForStatusChangeQueue(ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList) As DataTable
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICInstructionSecond As New ICInstructionQuery("qryObjICInstructionSecond")
        '    Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo.Case.When(qryObjICInstruction.FileBatchNo <> "SI").Then((qryObjICInstruction.FileBatchNo + " - " + qryObjICFiles.OriginalFileName)).Else(qryObjICInstruction.FileBatchNo).End().As("FileBatchNoName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo)

        '    If ArrayListStatus.Count > 0 Then
        '        ArrAND.Add(qryObjICInstructionSecond.Status.NotIn(ArrayListStatus))
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        ArrAND.Add(qryObjICInstructionSecond.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString

        '            OfficeIDStr = dr("OfficeID").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.PaymentNatureCode = PaymentNature.ToString())
        '            arrAND2.Add(qryObjICInstructionSecond.CreatedOfficeCode = OfficeIDStr.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstructionSecond.And(objAdd2))

        '        Next
        '    End If
        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If
        '    qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(qryObjICInstructionSecond.[Select](qryObjICInstructionSecond.InstructionID).Where(qryObjICInstructionSecond.And(objAdd), qryObjICInstructionSecond.Or(objOr), qryObjICInstruction.IsPrintLocationAmended = False)))
        '    qryObjICInstruction.es.Distinct = True
        '    dt = qryObjICInstruction.LoadDataTable
        '    Return dt
        'End Function

        Public Shared Sub GetAllInstructionsForRadGridForPrintingWithUserLocation(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String)
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(UsersID.ToString)
            Dim ArrAND As New ArrayList
            Dim ArrOR As New ArrayList
            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,VerificationDate,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,ISNULL(CONVERT(varchar,IC_Instruction.ReIssuanceID),'-') as ReissuanceID,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            StrQuery += "LEFT JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
            WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If BatchCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@BatchCode"
                    params.Add("BatchCode", BatchCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If
                If ReferenceNo.ToString <> "" Then
                    WhereClasue += " AND ReferenceNo=@ReferenceNo"
                    params.Add("ReferenceNo", ReferenceNo)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If InstructionStatus.Count > 0 Then
                WhereClasue += " AND IC_Instruction.Status IN ("
                For i = 0 To InstructionStatus.Count - 1
                    WhereClasue += InstructionStatus(i) & ","
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            WhereClasue += " AND ( "
            Dim x As Integer = 0
            If AccountNumber.Rows.Count > 0 Then

                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += " )"
                WhereClasue += " And PrintLocationCode=@OfficeCode"
                params.Add("OfficeCode", objICUser.OfficeCode.ToString)
                WhereClasue += " )"
            End If

            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClasue += "AND PaymentMode IN ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClasue += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

        'Public Shared Sub GetAllInstructionsForRadGridForPrintingWithUserLocation(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature, OfficeIDStr As String
        '    Dim objICUser As New ICUser
        '    objICUser.LoadByPrimaryKey(UsersID.ToString)
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), qryObjICOffice.OfficeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate)
        '    qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status)
        '    qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)

        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date <= CDate(ToDate))
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
        '        End If

        '        If BatchCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.FileBatchNo = BatchCode.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If

        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstructionID = InstructionNo.ToString)
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '        End If
        '        If ReferenceNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ReferenceNo = ReferenceNo.ToString)
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If

        '        If Not Amount = 0 Then
        '            If AmountOperator.ToString = "=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount = Amount)
        '            ElseIf AmountOperator.ToString = "<" Then
        '                ArrAND.Add(qryObjICInstruction.Amount < Amount)
        '            ElseIf AmountOperator.ToString = ">" Then
        '                ArrAND.Add(qryObjICInstruction.Amount > Amount)
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <= Amount)
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount >= Amount)
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <> Amount)
        '            End If
        '        End If
        '    End If

        '    If AccountNumber.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In AccountNumber.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            OfficeIDStr = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountNumber").ToString.Split("-")(0)
        '            BranchCode = dr("AccountNumber").ToString.Split("-")(1)
        '            Currency = dr("AccountNumber").ToString.Split("-")(2)
        '            PaymentNature = dr("PaymentNature").ToString


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstruction.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstruction.PaymentNatureCode = PaymentNature.ToString())

        '            If objICUser.OfficeCode.ToString <> "" Then
        '                ArrAND.Add(qryObjICInstruction.PrintLocationCode = objICUser.OfficeCode.ToString)
        '            End If
        '            If InstructionStatus.Count > 0 Then
        '                ArrAND.Add(qryObjICInstruction.Status.In(InstructionStatus))
        '            End If
        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstruction.And(objAdd2))

        '        Next
        '    End If


        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        ArrAND.Add(qryObjICInstruction.PaymentMode.In(AssignedPaymentModes))
        '    End If


        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)

        '        Next

        '    End If

        '    qryObjICInstruction.Where(qryObjICInstruction.And(objAdd), qryObjICInstruction.Or(objOr))
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    Dim str As String = qryObjICInstruction.es.LastQuery
        '    dt = New DataTable
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub

        Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNatureForClearingApproval(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal UserID As String)

            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(UserID)
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName,isnull(ClearingType,'-') as ClearingType, "
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,(ClearingUser.UserName + '-' + ClearingUser.DisplayName) as UserName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
            StrQuery += "left join IC_User as ClearingUser on IC_Instruction.ClearedBy=ClearingUser.userid "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If

                WhereClasue += " AND IC_Instruction.Status=@Status"
                params.Add("Status", Status)

            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If
                WhereClasue += " AND IC_Instruction.Status=@Status"
                params.Add("Status", Status)
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = -1 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " AND "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode  IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            WhereClasue += " AND IC_Instruction.ClearingOfficeCode=@OfficeCode"
            params.Add("OfficeCode", objICUser.OfficeCode)

            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

        ''Before Farhan Modified
        'Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNatureForClearingApproval(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal UserID As String)
        '    Dim dt As DataTable
        '    Dim StrQuery As String = Nothing
        '    Dim WhereClasue As String = Nothing
        '    Dim objICUser As New ICUser
        '    objICUser.LoadByPrimaryKey(UserID)
        '    WhereClasue = " Where "
        '    StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName,isnull(ClearingType,'-') as ClearingType, "
        '    StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        '    StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        '    StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        '    StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        '    StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        '    StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        '    StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
        '    StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
        '    StrQuery += "IC_ProductType.ProductTypeName,ClearingUser.UserName,"
        '    StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        '    StrQuery += "when 'false' then "
        '    StrQuery += "CONVERT(varchar,'false') "
        '    StrQuery += "else "
        '    StrQuery += "IsAmendmentComplete "
        '    StrQuery += " end "
        '    StrQuery += "as IsAmendmentComplete, "
        '    StrQuery += "case acquisitionmode "
        '    StrQuery += "when 'Online Form' then "
        '    StrQuery += "ic_instruction.FileBatchno else "
        '    StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
        '    StrQuery += "end as FileBatchNo "
        '    StrQuery += "from IC_Instruction "
        '    StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        '    StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        '    StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        '    StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        '    StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
        '    StrQuery += "left join IC_User as ClearingUser on IC_Instruction.ClearedBy=ClearingUser.userid "
        '    StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If

        '        WhereClasue += " AND IC_Instruction.Status=" & Status & " "

        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            WhereClasue += " AND InstructionID=" & InstructionNo & ""
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
        '        End If
        '        WhereClasue += " AND IC_Instruction.Status=" & Status & " "
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If
        '        If Not Amount = -1 Then
        '            If AmountOperator.ToString = "=" Then
        '                WhereClasue += " AND Amount = " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<" Then
        '                WhereClasue += " AND Amount < " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">" Then
        '                WhereClasue += " AND Amount > " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                WhereClasue += " AND Amount <= " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                WhereClasue += " AND Amount >= " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                WhereClasue += " AND Amount <> " & Amount & ""
        '            End If
        '        End If
        '    End If
        '    If AccountNumber.Rows.Count > 0 Then
        '        WhereClasue += " AND "
        '        WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode  IN ("
        '        For Each dr As DataRow In AccountNumber.Rows
        '            WhereClasue += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '    End If
        '    WhereClasue += " AND IC_Instruction.ClearingOfficeCode=" & objICUser.OfficeCode & " "

        '    If WhereClasue.Contains(" Where  AND ") = True Then
        '        WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        '    End If
        '    StrQuery += WhereClasue
        '    StrQuery += " Order By InstructionID Desc"
        '    Dim utill As New esUtility()
        '    dt = New DataTable
        '    dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        '    If Not CurrentPage = 0 Then

        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub
        

        
        Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNature(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal FileBatchNo As String)
            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName, "
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=@Status"
                    params.Add("Status", Status)
                End If
                If FileBatchNo <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@FileBatchNo"
                    params.Add("FileBatchNo", FileBatchNo)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If

                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = -1 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " AND "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode  IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If


            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

        'Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNature(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String)
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim AccountNmbrStr, BranchCode, Currency, PaymentNature As String
        '    Dim ArrAND As New ArrayList
        '    Dim ArrOR As New ArrayList
        '    Dim dt As DataTable

        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.ValueDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryName, qryObjICInstruction.BeneficiaryAddress.Coalesce("'-'"), qryObjICInstruction.Amount)
        '    qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICInstruction.InstrumentNo.Coalesce("'-'"), (qryObjICOffice.OfficeCode.Coalesce("'-'") + " - " + qryObjICOffice.OfficeName.Coalesce("'-'")).As("OfficeName"))
        '    qryObjICInstruction.Select(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency)
        '    qryObjICInstruction.Select(qryObjICInstruction.FileBatchNo, qryObjICBank.BankName, qryObjICInstruction.ReferenceNo.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.PreviousPrintLocationCode.Coalesce("'-'"), qryObjICInstruction.PreviousPrintLocationName.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.VerificationDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.ApprovalDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.StaleDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.CancellationDate.Date, qryObjICInstruction.BeneficiaryAccountNo.Coalesce("'-'"), qryObjICInstruction.BeneAccountBranchCode.Coalesce("'-'"), qryObjICInstruction.BeneAccountCurrency.Coalesce("'-'"))
        '    qryObjICInstruction.Select(qryObjICInstruction.LastPrintDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.RevalidationDate.Date)
        '    qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode, qryObjICInstruction.CompanyCode, qryObjICInstruction.Status)
        '    qryObjICInstruction.Select(qryObjICCompany.CompanyName, qryObjICProductType.ProductTypeName)
        '    qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
        '    qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
        '    qryObjICInstruction.LeftJoin(qryObjICBank).On(qryObjICInstruction.BeneficiaryBankCode = qryObjICBank.BankCode)
        '    qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And RefeRenceNo = "" Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CreateDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ValueDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.ApprovalDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.StaleDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.CancellationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.LastPrintDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.RevalidationDate.Date <= CDate(ToDate))
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date.Between(CDate(FromDate), CDate(ToDate)))
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                ArrAND.Add(qryObjICInstruction.VerificationDate.Date <= CDate(ToDate))
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ProductTypeCode = ProductTypeCode.ToString)
        '        End If

        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If
        '        If GroupCode.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.GroupCode = GroupCode.ToString)
        '        End If
        '        If Status.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.Status = Status.ToString)
        '        End If
        '        If RefeRenceNo <> "" Then
        '            ArrAND.Add(qryObjICInstruction.ReferenceNo = RefeRenceNo)
        '        End If
        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstructionID = InstructionNo.ToString)
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            ArrAND.Add(qryObjICInstruction.InstrumentNo = InstrumentNo.ToString)
        '        End If

        '        If CompanyCode.ToString <> "0" Then
        '            ArrAND.Add(qryObjICInstruction.CompanyCode = CompanyCode.ToString)
        '        End If
        '        If Not Amount = -1 Then
        '            If AmountOperator.ToString = "=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount = Amount)
        '            ElseIf AmountOperator.ToString = "<" Then
        '                ArrAND.Add(qryObjICInstruction.Amount < Amount)
        '            ElseIf AmountOperator.ToString = ">" Then
        '                ArrAND.Add(qryObjICInstruction.Amount > Amount)
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <= Amount)
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                ArrAND.Add(qryObjICInstruction.Amount >= Amount)
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                ArrAND.Add(qryObjICInstruction.Amount <> Amount)
        '            End If
        '        End If
        '    End If

        '    If AccountNumber.Rows.Count > 0 Then
        '        Dim arrAND2 As New ArrayList
        '        For Each dr As DataRow In AccountNumber.Rows
        '            AccountNmbrStr = Nothing
        '            BranchCode = Nothing
        '            Currency = Nothing
        '            PaymentNature = Nothing
        '            AccountNmbrStr = dr("AccountPaymentNature").ToString.Split("-")(0)
        '            BranchCode = dr("AccountPaymentNature").ToString.Split("-")(1)
        '            Currency = dr("AccountPaymentNature").ToString.Split("-")(2)
        '            PaymentNature = dr("AccountPaymentNature").ToString.Split("-")(3)


        '            arrAND2 = New ArrayList

        '            arrAND2.Add(qryObjICInstruction.ClientAccountNo = AccountNmbrStr.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountBranchCode = BranchCode.ToString())
        '            arrAND2.Add(qryObjICInstruction.ClientAccountCurrency = Currency.ToString())
        '            arrAND2.Add(qryObjICInstruction.PaymentNatureCode = PaymentNature.ToString())

        '            ' Convert And ArrayList into Object Array
        '            Dim objAdd2() As Object
        '            If arrAND2.Count > 0 Then
        '                ReDim objAdd2(arrAND2.Count - 1)
        '                Dim count As Integer
        '                For count = 0 To arrAND2.Count - 1
        '                    objAdd2(count) = arrAND2(count)
        '                Next
        '            End If
        '            ArrOR.Add(qryObjICInstruction.And(objAdd2))

        '        Next

        '    End If


        '    ' Convert And ArrayList into Object Array
        '    Dim objAdd() As Object
        '    If ArrAND.Count > 0 Then
        '        ReDim objAdd(ArrAND.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrAND.Count - 1
        '            objAdd(count) = ArrAND(count)
        '        Next
        '    End If
        '    ' Convert Or ArrayList into Object Array
        '    Dim objOr() As Object
        '    If ArrOR.Count > 0 Then
        '        ReDim objOr(ArrOR.Count - 1)
        '        Dim count As Integer
        '        For count = 0 To ArrOR.Count - 1
        '            objOr(count) = ArrOR(count)
        '        Next
        '    End If
        '    qryObjICInstruction.Where(qryObjICInstruction.And(objAdd), qryObjICInstruction.Or(objOr))
        '    qryObjICInstruction.OrderBy(qryObjICInstruction.InstructionID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        '    Dim str As String = qryObjICInstruction.es.LastQuery
        '    dt = New DataTable
        '    dt = qryObjICInstruction.LoadDataTable
        '    If Not CurrentPage = 0 Then
        '        qryObjICInstruction.es.PageNumber = CurrentPage
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICInstruction.es.PageNumber = 1
        '        qryObjICInstruction.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub


        Public Shared Function GetAccountPaymentNatureTaggedWithUserForAPNature(ByVal UserID As String, ByVal ArrayList As ArrayList) As DataTable
            Dim qryObjICAPNature As New ICAccountsPaymentNatureQuery("qryObjICAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As DataTable
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICPaymentNature.PaymentNatureName).As("AccountAndPaymentNature"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency + "-" + qryObjICUserRolesAPNature.PaymentNatureCode).As("AccountPaymentNature"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAPNature).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAPNature.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAPNature.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAPNature.Currency And qryObjICUserRolesAPNature.PaymentNatureCode = qryObjICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.es.Distinct = True
            dt = qryObjICUserRolesAPNature.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllTaggedBatchNumbersByCompanyAccountPaymentNature(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable

            Dim StrQuery As String = Nothing
            Dim WhereClause As String = Nothing
            WhereClause = " Where "
            Dim dt As New DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
            StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClause += " And ins2.PaymentMode in ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClause += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If ArrayListStatus.Count > 0 Then
                WhereClause += " And ins2.Status in ( "
                For i = 0 To ArrayListStatus.Count - 1
                    WhereClause += ArrayListStatus(i) & ","
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClause += " And ins2.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            Else
                WhereClause += " And ins2.CompanyCode=0"
            End If
            If dtAccountNo.Rows.Count > 0 Then
                WhereClause += " AND ("
                WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
                For Each dr As DataRow In dtAccountNo.Rows
                    WhereClause += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
                WhereClause += ")"
            End If
            WhereClause += ")"
            If WhereClause.Contains(" Where  And") Then
                WhereClause = WhereClause.Replace(" Where  And", " Where ")
            End If
            StrQuery += WhereClause
            StrQuery += " ) Order By FileBatchNoName Asc"

            Dim util As New esUtility
            dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            Return dt

        End Function

        'Public Shared Function GetAllTaggedBatchNumbersByCompanyAccountPaymentNature(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable

        '    Dim StrQuery As String = Nothing
        '    Dim WhereClause As String = Nothing
        '    WhereClause = " Where "
        '    Dim dt As New DataTable
        '    StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        '    StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        '    StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        '    StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        '    StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
        '    If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
        '        WhereClause += " And ins2.PaymentMode in ("
        '        For i = 0 To AssignedPaymentModes.Count - 1
        '            WhereClause += "'" & AssignedPaymentModes(i) & "',"
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1)
        '        WhereClause += ")"
        '    End If
        '    If ArrayListStatus.Count > 0 Then
        '        WhereClause += " And ins2.Status in ( "
        '        For i = 0 To ArrayListStatus.Count - 1
        '            WhereClause += ArrayListStatus(i) & ","
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1)
        '        WhereClause += ")"
        '    End If
        '    If CompanyCode.ToString <> "0" Then
        '        WhereClause += " And ins2.CompanyCode=" & CompanyCode
        '    Else
        '        WhereClause += " And ins2.CompanyCode=0"
        '    End If
        '    If dtAccountNo.Rows.Count > 0 Then
        '        WhereClause += " AND ("
        '        WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
        '        For Each dr As DataRow In dtAccountNo.Rows
        '            WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "',"
        '        Next
        '        WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
        '        WhereClause += ")"
        '    End If
        '    WhereClause += ")"
        '    If WhereClause.Contains(" Where  And") Then
        '        WhereClause = WhereClause.Replace(" Where  And", " Where ")
        '    End If
        '    StrQuery += WhereClause
        '    StrQuery += " ) Order By FileBatchNoName Asc"

        '    Dim util As New esUtility
        '    dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        '    Return dt

        'End Function
        Public Shared Function GetInstructionByPaymentModeAndArrayListID(ByVal PaymentMode As String, ByVal ArrayListInstructionID As ArrayList, ByVal Status As String) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection

            objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArrayListInstructionID) And objICInstructionColl.Query.PaymentMode = PaymentMode And objICInstructionColl.Query.Status = Status)
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function

        '' Commented by Farah Anwar 07-Sep-2015

        'Public Shared Sub GetAllInstructionsForStatusChangeQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal Status As String)

        '    Dim dt As DataTable
        '    Dim StrQuery As String = Nothing
        '    Dim WhereClasue As String = Nothing
        '    WhereClasue = " Where "
        '    StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
        '    StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        '    StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        '    StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        '    StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        '    StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        '    StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        '    StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
        '    StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,ClearingOfficeCode,"
        '    StrQuery += "IC_ProductType.ProductTypeName,"
        '    StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        '    StrQuery += "when 'false' then "
        '    StrQuery += "CONVERT(varchar,'false') "
        '    StrQuery += "else "
        '    StrQuery += "IsAmendmentComplete "
        '    StrQuery += " end "
        '    StrQuery += "as IsAmendmentComplete, "
        '    StrQuery += "case acquisitionmode "
        '    StrQuery += "when 'Online Form' then "
        '    StrQuery += "ic_instruction.FileBatchno else "
        '    StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
        '    StrQuery += "end as FileBatchNo "
        '    StrQuery += "from IC_Instruction "
        '    StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        '    StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        '    StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        '    StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        '    StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        '    If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
        '        If DateType.ToString = "Creation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Value Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Approval Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Stale Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Cancellation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Last Print Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Revalidation Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        ElseIf DateType.ToString = "Verfication Date" Then
        '            If Not FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
        '            ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
        '                WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
        '            End If
        '        End If
        '        If ProductTypeCode.ToString <> "" Then
        '            WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
        '        End If
        '        If BatchCode.ToString <> "" Then
        '            WhereClasue += " AND IC_Instruction.FileBatchNo='" & BatchCode & "'"
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If
        '        If Status <> "" Then
        '            WhereClasue += " AND IC_Instruction.Status=" & Status & " "
        '        End If
        '    Else
        '        If InstructionNo.ToString <> "" Then
        '            WhereClasue += " AND InstructionID=" & InstructionNo & ""
        '        End If
        '        If InstrumentNo.ToString <> "" Then
        '            WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
        '        End If
        '        If ReferenceNo.ToString <> "" Then
        '            WhereClasue += " AND ReferenceNo='" & ReferenceNo & "'"
        '        End If
        '        If CompanyCode.ToString <> "0" Then
        '            WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
        '        End If
        '        If Not Amount = 0 Then
        '            If AmountOperator.ToString = "=" Then
        '                WhereClasue += " AND Amount = " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<" Then
        '                WhereClasue += " AND Amount < " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">" Then
        '                WhereClasue += " AND Amount > " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<=" Then
        '                WhereClasue += " AND Amount <= " & Amount & ""
        '            ElseIf AmountOperator.ToString = ">=" Then
        '                WhereClasue += " AND Amount >= " & Amount & ""
        '            ElseIf AmountOperator.ToString = "<>" Then
        '                WhereClasue += " AND Amount <> " & Amount & ""
        '            End If
        '        End If
        '    End If
        '    If InstructionStatus.Count > 0 Then
        '        WhereClasue += " AND IC_Instruction.Status NOT IN ("
        '        For i = 0 To InstructionStatus.Count - 1
        '            WhereClasue += InstructionStatus(i) & ","
        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '    End If
        '    If AccountNumber.Rows.Count > 0 Then
        '        WhereClasue += " And "
        '        WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
        '        For Each dr As DataRow In AccountNumber.Rows
        '            WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

        '        Next
        '        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        '        WhereClasue += ")"
        '    End If
        '    WhereClasue += " And IsPrintLocationAmended=0"
        '    If WhereClasue.Contains(" Where  AND ") = True Then
        '        WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        '    End If
        '    StrQuery += WhereClasue
        '    StrQuery += " Order By InstructionID Desc"
        '    Dim utill As New esUtility()
        '    dt = New DataTable
        '    dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        '    If Not CurrentPage = 0 Then

        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub

        Public Shared Sub UpdateInstructionCurrentStatus(ByVal InstructionID As String, ByVal ToStatus As String, ByVal UsersID As String, ByVal UsersName As String, ByVal RemarksType As String, ByVal ActionType As String, Optional ByVal Remarks As String = "")
            Dim objICInstruction As New ICInstruction
            Dim objICInstructionActivity As New ICInstructionActivity
            Dim objICStatus As New ICInstructionStatus
            Dim objICLastStatus As New ICInstructionStatus
            Dim StrAuditTrail As String = Nothing
            Dim StrInstructionActivity As String = Nothing

            If objICInstruction.LoadByPrimaryKey(InstructionID) Then
                objICStatus.LoadByPrimaryKey(ToStatus)
                objICInstruction.Status = ToStatus

                If Remarks.ToString <> "" Then
                    If RemarksType.ToString = "Error" Then
                        objICInstruction.ErrorMessage = Remarks
                    ElseIf RemarksType.ToString = "Remarks" Then
                        objICInstruction.Remarks = Remarks
                    End If
                End If

                objICInstruction.Save()

                StrAuditTrail += "Instruction with ID: [ " & objICInstruction.InstructionID & " ] , Beneficiary name [ " & objICInstruction.BeneficiaryName & " ], Client Account number [ " & objICInstruction.ClientAccountNo & " ], Client Account Branch Code [ " & objICInstruction.ClientAccountBranchCode & " ], Currency [ " & objICInstruction.ClientAccountCurrency & " ] Status updated from [ " & ToStatus & " ] [ " & objICInstruction.UpToICInstructionStatusByLastStatus.StatusName & " ] ;"
                StrAuditTrail += " to Status [ " & objICInstruction.Status & " ] [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]"
                StrAuditTrail += "Action was taken by [ " & UsersID & " ][ " & UsersName & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail.ToString, "Instruction", objICInstruction.InstructionID.ToString, UsersID, UsersName, ActionType)

                objICInstructionActivity.Action = StrAuditTrail
                objICInstructionActivity.ActionBy = UsersID
                objICInstructionActivity.ActionDate = Date.Now
                objICInstructionActivity.InstructionID = objICInstruction.InstructionID
                objICInstructionActivity.ToStatus = ToStatus
                objICInstructionActivity.CompanyCode = objICInstruction.CompanyCode
                objICInstructionActivity.BatchNo = objICInstruction.FileBatchNo
                If Not HttpContext.Current Is Nothing Then
                    objICInstructionActivity.UserIP = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If
                ICInstructionController.AddInstructionActivity(objICInstructionActivity, UsersID, UsersName, StrAuditTrail, "UPDATE")
            End If
        End Sub



        Public Shared Sub GetAllInstructionsForStatusChangeQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal Status As String)

            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,ClearingOfficeCode,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If BatchCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@BatchCode"
                    params.Add("BatchCode", BatchCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Status <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=@Status"
                    params.Add("Status", Status)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If
                If ReferenceNo.ToString <> "" Then
                    WhereClasue += " AND ReferenceNo=@ReferenceNo"
                    params.Add("ReferenceNo", ReferenceNo)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If InstructionStatus.Count > 0 Then
                'WhereClasue += " AND IC_Instruction.Status NOT IN ("
                WhereClasue += " AND IC_Instruction.Status IN ("
                For i = 0 To InstructionStatus.Count - 1
                    WhereClasue += InstructionStatus(i) & ","
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If

            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " And "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            WhereClasue += " And IsPrintLocationAmended=0"
            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub






        Public Shared Sub GetAllInstructionsForStatusChangeApprovalQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal Status As String)

            Dim dt As DataTable
            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,ClearingOfficeCode,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
                End If
                If BatchCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo='" & BatchCode & "'"
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
                End If
                If Status <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=" & Status & " "
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=" & InstructionNo & ""
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
                End If
                If ReferenceNo.ToString <> "" Then
                    WhereClasue += " AND ReferenceNo='" & ReferenceNo & "'"
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = " & Amount & ""
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < " & Amount & ""
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > " & Amount & ""
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= " & Amount & ""
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= " & Amount & ""
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> " & Amount & ""
                    End If
                End If
            End If
            If InstructionStatus.Count > 0 Then
                WhereClasue += " AND IC_Instruction.Status IN ("
                For i = 0 To InstructionStatus.Count - 1
                    WhereClasue += InstructionStatus(i) & ","
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " And "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetAllInstructionsByArrayListInstructionID(ByVal InstructionIDArrayList As ArrayList) As ICInstructionCollection
            Dim objICInstructionColl As New ICInstructionCollection
            objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(InstructionIDArrayList))
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
            objICInstructionColl.Query.Load()
            Return objICInstructionColl
        End Function
        ''11112013 Javed
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueByAPNature(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.ClientAccountNo.As("ClientAccountNo"), qryObjICInstruction.InstructionID.Count.As("Count"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo)
            qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode).As("APNLocation"))
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstruction.ClientAccountNo, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICCompany.CompanyName, qryObjICInstruction.InstrumentNo)
            qryObjICInstruction.OrderBy(qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.ClientAccountNo.As("ClientAccountNo"), qryObjICInstruction.InstructionID.Count.As("Count"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICCompany.CompanyName)
            qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode + "-" + qryObjICInstruction.PrintLocationCode.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("APNLocation"))
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstruction.ClientAccountNo, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICInstruction.PrintLocationCode, qryObjICCompany.CompanyName)
            qryObjICInstruction.OrderBy(qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.ClientAccountNo.As("ClientAccountNo"), qryObjICInstruction.InstructionID.Count.As("Count"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICCompany.CompanyName)
            qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode + "-" + qryObjICInstruction.CreatedOfficeCode.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("APNLocation"))
            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstruction.ClientAccountNo, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICInstruction.CreatedOfficeCode, qryObjICCompany.CompanyName)
            qryObjICInstruction.OrderBy(qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ByVal InstructionIDArraList As ArrayList, ByVal StatusArryList As ArrayList, ByVal APNLocation As String, ByVal TaggingType As String) As String
        '    Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        '    Dim dt As New DataTable
        '    qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"))
        '    qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArraList) And qryObjICInstruction.Status.In(StatusArryList))
        '    If TaggingType = "APNatureAndLocation" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3), qryObjICInstruction.CreatedOfficeCode = APNLocation.Split("-")(4))
        '    ElseIf TaggingType = "Printing" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3))
        '    ElseIf TaggingType = "APNature" Then
        '        qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3))

        '    End If

        '    'qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, ryObjICInstruction.PaymentNatureCode, qryObjICInstruction.CreatedOfficeCode)

        '    dt = qryObjICInstruction.LoadDataTable
        '    If dt.Rows.Count > 0 Then
        '        Return dt(0)(0).ToString
        '    Else
        '        Return "0"
        '    End If
        'End Function
        Public Shared Function GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ByVal InstructionIDArraList As ArrayList, ByVal StatusArryList As ArrayList, ByVal APNLocation As String, ByVal TaggingType As String, Optional ByVal InstructionType As String = "") As String
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim dt As New DataTable
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID.Count.As("Count"))
            If InstructionType = "" Then
                qryObjICInstruction.Where(qryObjICInstruction.InstructionID.In(InstructionIDArraList) And qryObjICInstruction.Status.In(StatusArryList))
            ElseIf InstructionType = "ReIssuance" Then
                qryObjICInstruction.Where(qryObjICInstruction.ReIssuanceID.In(InstructionIDArraList) And qryObjICInstruction.Status.In(StatusArryList))
            End If

            If TaggingType = "APNatureAndLocation" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3), qryObjICInstruction.CreatedOfficeCode = APNLocation.Split("-")(4))
            ElseIf TaggingType = "Printing" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3))
            ElseIf TaggingType = "APNature" Then
                qryObjICInstruction.Where(qryObjICInstruction.ClientAccountNo = APNLocation.Split("-")(0), qryObjICInstruction.ClientAccountBranchCode = APNLocation.Split("-")(1), qryObjICInstruction.ClientAccountCurrency = APNLocation.Split("-")(2), qryObjICInstruction.PaymentNatureCode = APNLocation.Split("-")(3))

            End If

            qryObjICInstruction.GroupBy(qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode, qryObjICInstruction.CreatedOfficeCode)

            dt = qryObjICInstruction.LoadDataTable
            If dt.Rows.Count > 0 Then
                Return dt(0)(0).ToString
            Else
                Return "0"
            End If
        End Function
        Public Shared Function GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForReIssuance(ByVal InstructionIDArrayList As ArrayList, ByVal Status As ArrayList, ByVal IsPoOrDD As Boolean, ByVal TaggingType As String, Optional ByVal PaymentMode As String = Nothing) As DataTable
            Dim dt As New DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            Dim qryObjICInstructionStatus As New ICInstructionStatusQuery("qryObjICInstructionStatus")

            qryObjICInstruction.Select(qryObjICProductType.ProductTypeCode.As("ProductTypeName"), qryObjICInstruction.ClientAccountNo.As("ClientAccountNo"), qryObjICInstruction.InstructionID.Count.As("Count"))
            qryObjICInstruction.Select(qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.Select(qryObjICFiles.OriginalFileName.Case.When(qryObjICFiles.OriginalFileName.IsNull).Then("Online Form").Else(qryObjICFiles.OriginalFileName).End().As("FileName"))
            qryObjICInstruction.Select(qryObjICInstruction.PaymentMode, qryObjICCompany.CompanyName)
            If TaggingType = "Printing" Then
                qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode + "-" + qryObjICInstruction.PrintLocationCode.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("APNLocation"))
            ElseIf TaggingType = "APNLocation" Then
                qryObjICInstruction.Select((qryObjICInstruction.ClientAccountNo + "-" + qryObjICInstruction.ClientAccountBranchCode + "-" + qryObjICInstruction.ClientAccountCurrency + "-" + qryObjICInstruction.PaymentNatureCode + "-" + qryObjICInstruction.CreatedOfficeCode.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("APNLocation"))
            End If

            qryObjICInstruction.LeftJoin(qryObjICFiles).On(qryObjICInstruction.FileID = qryObjICFiles.FileID)
            qryObjICInstruction.LeftJoin(qryObjICProductType).On(qryObjICInstruction.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICInstruction.LeftJoin(qryObjICInstructionStatus).On(qryObjICInstruction.Status = qryObjICInstructionStatus.StatusID)
            qryObjICInstruction.LeftJoin(qryObjICCompany).On(qryObjICInstruction.CompanyCode = qryObjICCompany.CompanyCode)

            qryObjICInstruction.Where(qryObjICInstruction.ReIssuanceID.In(InstructionIDArrayList) And qryObjICInstruction.Status.In(Status))
            If IsPoOrDD = True Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = "PO" Or qryObjICInstruction.PaymentMode = "DD")
            End If
            If Not PaymentMode Is Nothing Then
                qryObjICInstruction.Where(qryObjICInstruction.PaymentMode = PaymentMode)
            End If
            qryObjICInstruction.GroupBy(qryObjICFiles.OriginalFileName, qryObjICProductType.ProductTypeCode, qryObjICInstruction.ClientAccountNo, qryObjICInstructionStatus.StatusName, qryObjICInstructionStatus.GroupHeader, qryObjICInstruction.Status)
            qryObjICInstruction.GroupBy(qryObjICInstruction.PaymentMode, qryObjICInstruction.ClientAccountNo, qryObjICInstruction.ClientAccountBranchCode, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.ClientAccountCurrency, qryObjICInstruction.PaymentNatureCode)
            If TaggingType = "Printing" Then
                qryObjICInstruction.GroupBy(qryObjICInstruction.PrintLocationCode)
            ElseIf TaggingType = "APNLocation" Then
                qryObjICInstruction.GroupBy(qryObjICInstruction.CreatedOfficeCode)
            End If
            qryObjICInstruction.GroupBy(qryObjICCompany.CompanyName)
            qryObjICInstruction.OrderBy(qryObjICFiles.OriginalFileName.Ascending)
            dt = qryObjICInstruction.LoadDataTable
            Return dt
        End Function
        Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNatureForReValidationApproval(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal UserID As String, ByVal BatchNo As String)
            Dim dt As DataTable
            Dim params As New EntitySpaces.Interfaces.esParameters

            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(UserID)
            WhereClasue = " Where "
            StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName, "
            StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
            StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
            StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
            StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
            StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
            StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
            StrQuery += "IC_ProductType.ProductTypeName,"
            StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
            StrQuery += "when 'false' then "
            StrQuery += "CONVERT(varchar,'false') "
            StrQuery += "else "
            StrQuery += "IsAmendmentComplete "
            StrQuery += " end "
            StrQuery += "as IsAmendmentComplete, "
            StrQuery += "case acquisitionmode "
            StrQuery += "when 'Online Form' then "
            StrQuery += "ic_instruction.FileBatchno else "
            StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
            StrQuery += "end as FileBatchNo "
            StrQuery += "from IC_Instruction "
            StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
            StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
            StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
            StrQuery += "left join IC_User on IC_Instruction.ReValiDationAllowedBy=IC_User.UserID "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
            If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
                If DateType.ToString = "Creation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Value Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Approval Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Stale Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Cancellation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Last Print Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Revalidation Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                ElseIf DateType.ToString = "Verfication Date" Then
                    If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                        params.Add("FromDate", FromDate)
                        params.Add("ToDate", ToDate)
                    ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                        WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                        params.Add("ToDate", ToDate)
                    End If
                End If
                If ProductTypeCode.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                    params.Add("ProductTypeCode", ProductTypeCode)
                End If
                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Status.ToString <> "" Then
                    WhereClasue += " AND IC_Instruction.Status=@Status"
                    params.Add("Status", Status)
                End If
                If BatchNo <> "" Then
                    WhereClasue += " AND IC_Instruction.FileBatchNo=@BatchNo"
                    params.Add("BatchNo", BatchNo)
                End If
            Else
                If InstructionNo.ToString <> "" Then
                    WhereClasue += " AND InstructionID=@InstructionNo"
                    params.Add("InstructionNo", InstructionNo)
                End If
                If InstrumentNo.ToString <> "" Then
                    WhereClasue += " AND InstrumentNo=@InstrumentNo"
                    params.Add("InstrumentNo", InstrumentNo)
                End If

                If CompanyCode.ToString <> "0" Then
                    WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                    params.Add("CompanyCode", CompanyCode)
                End If
                If Not Amount = 0 Then
                    If AmountOperator.ToString = "=" Then
                        WhereClasue += " AND Amount = @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<" Then
                        WhereClasue += " AND Amount < @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">" Then
                        WhereClasue += " AND Amount > @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<=" Then
                        WhereClasue += " AND Amount <= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = ">=" Then
                        WhereClasue += " AND Amount >= @Amount"
                        params.Add("Amount", Amount)
                    ElseIf AmountOperator.ToString = "<>" Then
                        WhereClasue += " AND Amount <> @Amount"
                        params.Add("Amount", Amount)
                    End If
                End If
            End If
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " AND "
                WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode  IN ("
                For Each dr As DataRow In AccountNumber.Rows
                    WhereClasue += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
            End If
            WhereClasue += " AND IC_User.OfficeCode=@OfficeCode"
            params.Add("OfficeCode", objICUser.OfficeCode)

            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By InstructionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Function GetAllTaggedBatchNumbersByCompanyAccountPaymentNatureForApproveReValidation(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
            Dim objICUser As New ICUser
            Dim params As New EntitySpaces.Interfaces.esParameters
            Dim StrQuery As String = Nothing
            Dim WhereClause As String = Nothing
            objICUser.LoadByPrimaryKey(UserIDs)
            WhereClause = " Where "
            Dim dt As New DataTable
            StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
            StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
            StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
            StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
            StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
            StrQuery += "left join IC_User on ins2.ReValiDationAllowedBy=IC_User.UserID "
            WhereClause += " And IC_User.OfficeCode=" & objICUser.OfficeCode & " "
            If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
                WhereClause += " And ins2.PaymentMode in ("
                For i = 0 To AssignedPaymentModes.Count - 1
                    WhereClause += "'" & AssignedPaymentModes(i) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If ArrayListStatus.Count > 0 Then
                WhereClause += " And ins2.Status in ( "
                For i = 0 To ArrayListStatus.Count - 1
                    WhereClause += ArrayListStatus(i) & ","
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1)
                WhereClause += ")"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClause += " And ins2.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            Else
                WhereClause += " And ins2.CompanyCode=0"
            End If
            If dtAccountNo.Rows.Count > 0 Then
                WhereClause += " AND ("
                WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
                For Each dr As DataRow In dtAccountNo.Rows
                    WhereClause += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
                Next
                WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
                WhereClause += ")"
            End If
            WhereClause += ")"
            If WhereClause.Contains(" Where  And") Then
                WhereClause = WhereClause.Replace(" Where  And", " Where ")
            End If
            StrQuery += WhereClause
            StrQuery += " ) Order By FileBatchNoName Asc"

            Dim util As New esUtility
            dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
            Return dt

        End Function
        Public Shared Function GetInstructionDetailByInstructionID(ByVal InstructionID As String) As DataTable
            Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            qryObjICInstruction.Select(qryObjICInstruction.InstructionID, qryObjICInstruction.ValueDate, qryObjICInstruction.CreateDate.Date, qryObjICInstruction.BeneficiaryName)
            qryObjICInstruction.Select(qryObjICInstruction.BeneficiaryAccountNo.Coalesce("'-'"), qryObjICInstruction.BeneAccountBranchCode.Coalesce("'-'"), qryObjICInstruction.BeneAccountCurrency.Coalesce("'-'"), qryObjICInstruction.InstrumentNo.Coalesce("'-'"))
            qryObjICInstruction.Select((qryObjICOffice.OfficeCode + "-" + qryObjICOffice.OfficeName).As("OfficeName"), qryObjICInstruction.Amount, qryObjICInstruction.PaymentMode)
            qryObjICInstruction.Select(qryObjICInstruction.ProductTypeCode)
            qryObjICInstruction.Select(qryObjICInstruction.IsAmendmentComplete.Case.When(qryObjICInstruction.IsAmendmentComplete.IsNull).Then("False").Else(qryObjICInstruction.IsAmendmentComplete).End())
            qryObjICInstruction.LeftJoin(qryObjICOffice).On(qryObjICInstruction.PrintLocationCode = qryObjICOffice.OfficeID)
            qryObjICInstruction.Where(qryObjICInstruction.InstructionID = InstructionID)
            Return qryObjICInstruction.LoadDataTable
        End Function
        Public Shared Function GetAccountPaymentNatureTaggedWithUserForSweepAction(ByVal UserID As String, ByVal ArrayList As ArrayList, ByVal GroupCode As String,
                                                                                  ByVal CompanyCode As String) As DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICUserRolesAPNatureAndLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNatureAndLocation")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            'qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode).As("AccountAndBranchCode"))
            qryObjICUserRolesAPNature.Select((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency).As("AccountNumber"))
            qryObjICUserRolesAPNature.Select(qryObjICUserRolesAPNature.AccountNumber.As("Value"))
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            If GroupCode <> "0" Then
                qryObjICUserRolesAPNature.Where(qryObjICCompany.GroupCode = GroupCode)
            End If
            If CompanyCode <> "0" Then
                qryObjICUserRolesAPNature.Where(qryObjICCompany.CompanyCode = CompanyCode)
            End If
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True And qryObjICUserRolesAPNature.UserID = UserID.ToString)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(ArrayList))
            qryObjICUserRolesAPNature.GroupBy((qryObjICUserRolesAPNature.AccountNumber + "-" + qryObjICUserRolesAPNature.BranchCode + "-" + qryObjICUserRolesAPNature.Currency), qryObjICUserRolesAPNature.AccountNumber)
            qryObjICUserRolesAPNature.es.Distinct = True

            Return qryObjICUserRolesAPNature.LoadDataTable
        End Function
        Public Shared Function GenerateRIN() As String
            Dim RIN As String = ""
            Dim fiveRandom As New Random()
            Dim tsFive As New TimeSpan()

            tsFive = DateTime.Now.Subtract(Convert.ToDateTime("01/01/1900"))
            RIN = fiveRandom.Next(10000, 99999) & tsFive.Days.ToString() & DateTime.Now.Hour.ToString("00") & DateTime.Now.Minute.ToString("00") & DateTime.Now.Second.ToString("00")
            If CheckDuplicateRIN(RIN) = True Then
                GenerateRIN()
            Else
                Return RIN
            End If

        End Function
        Public Shared Function CheckDuplicateRIN(ByVal RIN As String) As Boolean

            Dim objICInstruction As New ICInstruction


            objICInstruction.Query.Where(objICInstruction.Query.Rin = RIN)
            If objICInstruction.Query.Load() Then
                Return True
            Else
                Return False
            End If



        End Function
        'Disbursement Coding (COTC Page)
        Public Shared Function GetInstructionToDisburse(ByVal RIN As String) As ICInstruction
            Dim objICInstruction As New ICInstruction
            objICInstruction.Query.Where(objICInstruction.Query.Rin.Equal(RIN) And objICInstruction.Query.Status.In("48", "49") And objICInstruction.Query.PaymentMode.Equal("COTC"))
            objICInstruction.Query.Load()
            Return objICInstruction

        End Function
        Public Shared Function GetFileBatchNoByInstructionID(ByVal InstructionID As String) As String
            Dim objICInstructionQuery As New ICInstructionQuery("objICInstructionQuery")
            Dim objICFilesQuery As New ICFilesQuery("objICFilesQuery")

            objICInstructionQuery.Select(objICInstructionQuery.FileBatchNo.Case.When(objICInstructionQuery.FileBatchNo = "SI").Then("SI").Else((objICInstructionQuery.FileBatchNo + " - " + objICFilesQuery.OriginalFileName)).End().As("FileBatchNo"))
            objICInstructionQuery.LeftJoin(objICFilesQuery).On(objICInstructionQuery.FileID = objICFilesQuery.FileID)
            objICInstructionQuery.Where(objICInstructionQuery.InstructionID = InstructionID)
            Return objICInstructionQuery.LoadDataTable.Rows(0)(0).ToString
        End Function
        ' Farah Work for Cheque Return Reason [18-Aug-2014]
        'Public Shared Function GetAllChequeReturnedReasons() As DataTable
        '    Dim collChequeReturnedReasons As New ICReturnReasonCollection
        '    Dim dt As New DataTable
        '    collChequeReturnedReasons.Query.Where(collChequeReturnedReasons.Query.IsActive = True)
        '    collChequeReturnedReasons.Query.OrderBy(collChequeReturnedReasons.Query.ReturnReason.Ascending)
        '    collChequeReturnedReasons.Query.Load()
        '    dt = collChequeReturnedReasons.Query.LoadDataTable()
        '    Return dt
        'End Function
        'Beneficiary Limit Work at Pay Queue by Aizaz Ahmed [Dated: 17-Nov-2014]
        Public Shared Function CheckBeneFiciaryGroupLimitByInstructionID(ByVal InstructionID As String, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim objInstruction As New ICInstruction
            Dim objCompany As New ICCompany
            Dim objBene As New ICBene
            Dim objBeneGroup As New ICBeneGroup
            Dim CurrentDayAmount As Double = 0
            Dim InstructionAmount As Double = 0
            Dim BeneGroupLimitAmount As Double = 0

            If objInstruction.LoadByPrimaryKey(InstructionID) Then
                'Check Company for BeneAllowed Only
                If objCompany.LoadByPrimaryKey(objInstruction.CompanyCode) Then
                    If objCompany.IsAllowOpenPayment = True Then
                        Return True
                    ElseIf objCompany.IsBeneRequired = True Then
                        If Not objInstruction.BeneID Is Nothing Then
                            'Check BeneGroup's Current Date Amount Release from Pay Queue
                            If objBene.LoadByPrimaryKey(objInstruction.BeneID) Then
                                If objBeneGroup.LoadByPrimaryKey(objBene.BeneGroupCode) Then
                                    BeneGroupLimitAmount = objBeneGroup.BeneTransactionLimit
                                    'Check BeneGroup's Current Date Amount Release from Pay Queue
                                    CurrentDayAmount = GetBeneDailyLimitAmountByDate(objBeneGroup.BeneGroupCode, Date.Now)
                                    InstructionAmount = objInstruction.Amount
                                    ''Add BeneGroups Current Date Amount with Instruction Amount and Compare with BeneGroup Limit
                                    If (CurrentDayAmount + InstructionAmount) <= BeneGroupLimitAmount Then
                                        ''Update BeneGroups Current Date Amount + Instruction Amount 
                                        UpdateBeneDailyLimitAmount(objInstruction.InstructionID, Date.Now, "ADD", UserID, UserName)
                                        Return True
                                    Else
                                        Return False
                                    End If
                                End If
                            Else
                                Return True
                            End If
                        Else
                            Return True
                        End If
                    End If
                End If
            End If
        End Function
        Public Shared Function GetBeneDailyLimitAmountByDate(ByVal BeneGroupCode As String, ByVal Dated As Date) As Double
            Dim objBeneGroupLimit As New ICBeneGroupLimit
            Dim collBeneGroupLimit As New ICBeneGroupLimitCollection

            collBeneGroupLimit.Query.Where(collBeneGroupLimit.Query.BeneGroupCode = BeneGroupCode And collBeneGroupLimit.Query.ReleaseDate.Date = Dated.ToString("dd-MMM-yyyy"))
            If collBeneGroupLimit.Query.Load() Then
                Return collBeneGroupLimit(0).Amount
            Else
                Return 0
            End If
        End Function
        Public Shared Sub UpdateBeneDailyLimitAmount(ByVal InstructionID As String, ByVal Dated As Date, ByVal Action As String, ByVal UserID As String, ByVal UserName As String)
            Dim objInstruction As New ICInstruction
            Dim objBene As New ICBene
            Dim objBeneGroup As New ICBeneGroup
            Dim AuditAction As String = ""
            Dim objBeneGroupLimit As New ICBeneGroupLimit
            Dim collBeneGroupLimit As New ICBeneGroupLimitCollection
            If objInstruction.LoadByPrimaryKey(InstructionID) Then
                If objInstruction.UpToICCompanyByCompanyCode.IsAllowOpenPayment = False And objInstruction.UpToICCompanyByCompanyCode.IsBeneRequired = True Then
                    If Not objInstruction.BeneID Is Nothing Then
                        If objBene.LoadByPrimaryKey(objInstruction.BeneID) Then
                            If objBeneGroup.LoadByPrimaryKey(objBene.BeneGroupCode) Then
                                collBeneGroupLimit.Query.Where(collBeneGroupLimit.Query.BeneGroupCode = objBeneGroup.BeneGroupCode And collBeneGroupLimit.Query.ReleaseDate.Date = Dated.ToString("dd-MMM-yyyy"))
                                If collBeneGroupLimit.Query.Load() Then
                                    objBeneGroupLimit = collBeneGroupLimit(0)
                                    If Action.ToString() = "ADD" Then
                                        objBeneGroupLimit.Amount = (objBeneGroupLimit.Amount + objInstruction.Amount)
                                    ElseIf Action.ToString() = "SUBSTRACT" Then
                                        objBeneGroupLimit.Amount = (objBeneGroupLimit.Amount - objInstruction.Amount)
                                    End If
                                    objBeneGroupLimit.Save()
                                    AuditAction = "Beneficiary Group Limit " & Action & ". Details are: Instruction [ID: " & InstructionID & " ; Amount: " & objInstruction.Amount & " ] ; BeneGroup [Code: " & objBeneGroup.BeneGroupCode & " ] ; Bene [Code: " & objBene.BeneficiaryCode & "]"
                                    ICUtilities.AddAuditTrail(AuditAction, "Beneficiary Group Limit", objBeneGroupLimit.BeneGroupLimitID, UserID, UserName, Action)

                                Else
                                    If Action.ToString() = "ADD" Then
                                        objBeneGroupLimit.BeneGroupCode = objBeneGroup.BeneGroupCode
                                        objBeneGroupLimit.Amount = objInstruction.Amount
                                        objBeneGroupLimit.ReleaseDate = Dated.ToString("dd-MMM-yyyy")
                                        objBeneGroupLimit.Save()
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End Sub

        Public Shared Function GetAllChequeReturnedReasons() As DataTable

            Dim qryObjICReason As New ICReturnReasonQuery("qryObjICReason")
            Dim dt As New DataTable
            qryObjICReason.Select(qryObjICReason.ReturnReason, qryObjICReason.ReturnReasonID)
            qryObjICReason.Where(qryObjICReason.IsActive = True)
            qryObjICReason.OrderBy(qryObjICReason.ReturnReason.Ascending)
            qryObjICReason.Load()
            dt = qryObjICReason.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllInstructionByStatus(ByVal Status As String, ByVal InstructionID As String) As Boolean
            Dim collObjICInstruction As New ICInstructionActivityCollection

            collObjICInstruction.Query.Where(collObjICInstruction.Query.InstructionID = InstructionID And collObjICInstruction.Query.ToStatus = Status)
            collObjICInstruction.Query.Load()
            If collObjICInstruction.Count > 0 Then
                Return True
            Else
                Return False
            End If

        End Function
    End Class
End Namespace

