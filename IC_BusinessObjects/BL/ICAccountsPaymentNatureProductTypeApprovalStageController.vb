﻿Namespace IC
    Public Class ICAccountsPaymentNatureProductTypeApprovalStageController
        Public Shared Function AddAccountsPaymentNatureProductTypeApprovalStage(ByVal ICAccountsPaymentNatureProductTypeApprovalStage As ICAccountsPaymentNatureProductTypeApprovalStage) As String
            Dim cICAccountsPaymentNatureProductTypeApprovalStage As New ICAccountsPaymentNatureProductTypeApprovalStage
            cICAccountsPaymentNatureProductTypeApprovalStage.es.Connection.CommandTimeout = 3600
            cICAccountsPaymentNatureProductTypeApprovalStage.AccountsPaymentNatureProductTypeCode = ICAccountsPaymentNatureProductTypeApprovalStage.AccountsPaymentNatureProductTypeCode
            cICAccountsPaymentNatureProductTypeApprovalStage.ApprovalStageID = ICAccountsPaymentNatureProductTypeApprovalStage.ApprovalStageID
            cICAccountsPaymentNatureProductTypeApprovalStage.Save()
            Return cICAccountsPaymentNatureProductTypeApprovalStage.AccountsPaymentNatureProductTypeApprovalStageID.ToString()
        End Function

        Public Shared Function GetAllApprovalStagesByAccountsPaymentNatureProductTypeCode(ByVal AccountsPaymentNatureProductTypeCode As String) As ICAccountsPaymentNatureProductTypeApprovalStageCollection
            Dim cICAccountsPaymentNatureProductTypeApprovalStage As New ICAccountsPaymentNatureProductTypeApprovalStageCollection
            cICAccountsPaymentNatureProductTypeApprovalStage.es.Connection.CommandTimeout = 3600

            cICAccountsPaymentNatureProductTypeApprovalStage.Query.Where(cICAccountsPaymentNatureProductTypeApprovalStage.Query.AccountsPaymentNatureProductTypeCode = AccountsPaymentNatureProductTypeCode)
            cICAccountsPaymentNatureProductTypeApprovalStage.Query.OrderBy(cICAccountsPaymentNatureProductTypeApprovalStage.Query.ApprovalStageID.Ascending)
            cICAccountsPaymentNatureProductTypeApprovalStage.LoadAll()

            Return cICAccountsPaymentNatureProductTypeApprovalStage
        End Function

        Public Shared Sub DeleteApprovalStagesByAccountsPaymentNatureProductTypeApprovalStageID(ByVal AccountsPaymentNatureProductTypeApprovalStageID As String)
            Dim cICAAPNPTApprovalStage As New ICAccountsPaymentNatureProductTypeApprovalStage

            If cICAAPNPTApprovalStage.LoadByPrimaryKey(AccountsPaymentNatureProductTypeApprovalStageID) Then
                cICAAPNPTApprovalStage.MarkAsDeleted()
                cICAAPNPTApprovalStage.Save()
            End If
        End Sub

    End Class
End Namespace