﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICFileUploadTemplateController
        Public Shared Function AddFileUploadTemplate(ByVal objICTemplate As ICTemplate, ByVal isUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String) As Integer

            Dim objICTemplateForSave As New ICTemplate
            Dim objICTemplateFrom As New ICTemplate
            Dim TemplateID As Integer = 0
            Dim ActionStr As String = ""
            objICTemplateForSave.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objICTemplateForSave.CreateBy = objICTemplate.CreateBy
                objICTemplateForSave.CreateDate = objICTemplate.CreateDate
                objICTemplateForSave.Creater = objICTemplate.Creater
                objICTemplateForSave.CreationDate = objICTemplate.CreationDate
            Else
                objICTemplateForSave.LoadByPrimaryKey(objICTemplate.TemplateID)
                objICTemplateFrom.LoadByPrimaryKey(objICTemplate.TemplateID)
                objICTemplateForSave.CreateBy = objICTemplate.CreateBy
                objICTemplateForSave.CreateDate = objICTemplate.CreateDate
            End If
            objICTemplateForSave.TemplateName = objICTemplate.TemplateName
            objICTemplateForSave.FileUploadTemplateCode = objICTemplate.FileUploadTemplateCode
            objICTemplateForSave.FieldDelimiter = objICTemplate.FieldDelimiter
            objICTemplateForSave.IsFixLength = objICTemplate.IsFixLength
            objICTemplateForSave.TemplateFormat = objICTemplate.TemplateFormat
            objICTemplateForSave.SheetName = objICTemplate.SheetName
            objICTemplateForSave.TemplateType = objICTemplate.TemplateType
            objICTemplateForSave.DetailIdentifier = objICTemplate.DetailIdentifier
            objICTemplateForSave.HeaderIdentifier = objICTemplate.HeaderIdentifier
            objICTemplateForSave.IsActive = objICTemplate.IsActive
            objICTemplateForSave.Save()
            TemplateID = objICTemplateForSave.TemplateID
            If isUpdate = False Then
                ActionStr = "File upload template [ " & objICTemplateForSave.TemplateName & " ;"
                ActionStr += "code [ " & objICTemplateForSave.FileUploadTemplateCode & " ] ; "
                ActionStr += "Template format [ " & objICTemplateForSave.TemplateFormat & " ]; "
                ActionStr += "Template type [ " & objICTemplateForSave.TemplateType & " ]; "
                ActionStr += "Delimit character [ " & objICTemplateForSave.FieldDelimiter & " ]; "
                ActionStr += "Header identifier [ " & objICTemplateForSave.HeaderIdentifier & " ]; "
                ActionStr += "Detail identifier  [ " & objICTemplateForSave.DetailIdentifier & " ]; "
                ActionStr += "Sheet name [ " & objICTemplateForSave.SheetName & " ] added."
                ICUtilities.AddAuditTrail(ActionStr, "File Upload Template", objICTemplateForSave.TemplateID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
            Else
                ActionStr = "File upload template " & objICTemplateForSave.TemplateName & " updated. Template name from [ " & objICTemplateFrom.TemplateName & " ] to [ " & objICTemplateForSave.TemplateName & " ] ; "
                ActionStr += "code from [ " & objICTemplateFrom.FileUploadTemplateCode & " ] to [ " & objICTemplateForSave.FileUploadTemplateCode & " ] ; "
                ActionStr += "Template format from [ " & objICTemplateFrom.TemplateFormat & " ] to [ " & objICTemplateForSave.TemplateFormat & " ]; "
                ActionStr += "Template type from [ " & objICTemplateFrom.TemplateType & " ] to [ " & objICTemplateForSave.TemplateType & " ]; "
                ActionStr += "Delimit character from [ " & objICTemplateFrom.FieldDelimiter & " ] to [ " & objICTemplateForSave.FieldDelimiter & " ]; "
                ActionStr += "Header identifier from [ " & objICTemplateFrom.HeaderIdentifier & " ] to [ " & objICTemplateForSave.HeaderIdentifier & " ]; "
                ActionStr += "Detail identifier from [ " & objICTemplateFrom.DetailIdentifier & " ] to [ " & objICTemplateForSave.DetailIdentifier & " ]; "
                ActionStr += "Sheet name from [ " & objICTemplateFrom.SheetName & " ] to [ " & objICTemplateForSave.SheetName & " ]."
                ICUtilities.AddAuditTrail(ActionStr.ToString, "File Upload Template", objICTemplateForSave.TemplateID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If

            Return TemplateID
        End Function


        Public Shared Function GetAllFileUploadTemplates() As DataTable


            Dim objICTemplateColl As New ICTemplateCollection
            objICTemplateColl.Query.Select(objICTemplateColl.Query.TemplateID, objICTemplateColl.Query.TemplateName, objICTemplateColl.Query.FileUploadTemplateCode)
            objICTemplateColl.Query.OrderBy(objICTemplateColl.Query.TemplateName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            Return objICTemplateColl.Query.LoadDataTable
        End Function
        Public Shared Sub DeleteTemplate(ByVal TemplateID As String, ByVal UsersID As String, ByVal UsersName As String)


            Dim objICTemplate As New IC.ICTemplate
            Dim objICTemplateDeleted As New ICTemplate
            objICTemplate.es.Connection.CommandTimeout = 3600
            objICTemplateDeleted.es.Connection.CommandTimeout = 3600


            objICTemplateDeleted.LoadByPrimaryKey(TemplateID)
            objICTemplate.LoadByPrimaryKey(TemplateID)
            objICTemplate.MarkAsDeleted()
            objICTemplate.Save()
            ICUtilities.AddAuditTrail("File upload template : " & objICTemplateDeleted.TemplateName & ", Code: [ " & objICTemplateDeleted.FileUploadTemplateCode & " ], type: [ " & objICTemplateDeleted.TemplateType & " ] , format: [ " & objICTemplateDeleted.TemplateFormat & " ] deleted.", "File Upload Template", objICTemplateDeleted.TemplateID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
        End Sub

        Public Shared Sub GetFileUploadTemplateForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim objICTemplateColl As New ICTemplateCollection
            Dim dt As New DataTable

            objICTemplateColl.es.Connection.CommandTimeout = 3600
            objICTemplateColl.Query.OrderBy(objICTemplateColl.Query.TemplateID.Descending)
            objICTemplateColl.LoadAll()
            dt = objICTemplateColl.Query.LoadDataTable

            If Not PageNumber = 0 Then
                objICTemplateColl.Query.es.PageNumber = PageNumber
                objICTemplateColl.Query.es.PageSize = PageSize

                rg.DataSource = dt




                If DoDataBind = True Then

                    rg.DataBind()
                End If
            Else
                objICTemplateColl.Query.es.PageNumber = 1
                objICTemplateColl.Query.es.PageSize = PageSize

                rg.DataSource = dt




                If DoDataBind = True Then

                    rg.DataBind()
                End If
            End If

        End Sub


    End Class
End Namespace

