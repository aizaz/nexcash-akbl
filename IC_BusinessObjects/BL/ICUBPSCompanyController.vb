﻿
Imports Telerik.Web.UI
Namespace IC
    Public Class ICUBPSCompanyController
        Public Shared Sub AddUBPSCompany(ByVal objICUBPSCompany As ICUBPSCompany, ByVal isUpdate As Boolean, ByVal StrUserActivity As String, ByVal UserID As String, ByVal UserName As String, ByVal StrAction As String)

            Dim objICUBPSCompanyForSave As New ICUBPSCompany


            If isUpdate = True Then
                objICUBPSCompanyForSave.LoadByPrimaryKey(objICUBPSCompany.UBPSCompanyID)
                objICUBPSCompanyForSave.CreatedDate = Date.Now
                objICUBPSCompanyForSave.CreatedBy = objICUBPSCompany.CreatedBy
            Else
                objICUBPSCompanyForSave.Creator = UserID
                objICUBPSCompanyForSave.CreationDate = Date.Now
                objICUBPSCompanyForSave.CreatedDate = Date.Now
                objICUBPSCompanyForSave.CreatedBy = objICUBPSCompany.CreatedBy
            End If
            objICUBPSCompanyForSave.CompanyCode = objICUBPSCompany.CompanyCode
            objICUBPSCompanyForSave.CompanyName = objICUBPSCompany.CompanyName
            objICUBPSCompanyForSave.ReferenceFieldCaption = objICUBPSCompany.ReferenceFieldCaption
            objICUBPSCompanyForSave.ReferenceFieldMinLength = objICUBPSCompany.ReferenceFieldMinLength
            objICUBPSCompanyForSave.ReferenceFieldMaxLength = objICUBPSCompany.ReferenceFieldMaxLength
            objICUBPSCompanyForSave.MinValue = objICUBPSCompany.MinValue
            objICUBPSCompanyForSave.MaxValue = objICUBPSCompany.MaxValue
            objICUBPSCompanyForSave.IsBillingInquiry = objICUBPSCompany.IsBillingInquiry
            objICUBPSCompanyForSave.RegularExpressionForReferenceField = objICUBPSCompany.RegularExpressionForReferenceField
            objICUBPSCompanyForSave.MultiplesOfAmount = objICUBPSCompany.MultiplesOfAmount
            objICUBPSCompanyForSave.XAmountValue = objICUBPSCompany.XAmountValue
            objICUBPSCompanyForSave.IsActive = objICUBPSCompany.IsActive
            objICUBPSCompanyForSave.CompanyType = objICUBPSCompany.CompanyType
            objICUBPSCompanyForSave.ReferenceFieldFormat = objICUBPSCompany.ReferenceFieldFormat
            objICUBPSCompanyForSave.IsApproved = False
            objICUBPSCompanyForSave.Save()

            If isUpdate = False Then
                ICUtilities.AddAuditTrail(StrAction, "UBP Company", objICUBPSCompanyForSave.UBPSCompanyID.ToString, UserID, UserName, "ADD")
            Else
                ICUtilities.AddAuditTrail(StrAction, "UBP Company", objICUBPSCompanyForSave.UBPSCompanyID.ToString, UserID, UserName, "UPDATE")
            End If


        End Sub
      

        Public Shared Sub GetAllUBPSCompanyGV(ByVal CompanyCode As String, ByVal CompanyName As String, ByVal CompanyType As String, ByVal rg As RadGrid, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
            Dim dt As New DataTable
            Dim objICUBPSCompanyColl As New ICUBPSCompanyCollection
            objICUBPSCompanyColl.Query.Select(objICUBPSCompanyColl.Query.CompanyCode, objICUBPSCompanyColl.Query.CompanyName, objICUBPSCompanyColl.Query.CompanyType, objICUBPSCompanyColl.Query.IsActive)
            objICUBPSCompanyColl.Query.Select(objICUBPSCompanyColl.Query.UBPSCompanyID, objICUBPSCompanyColl.Query.IsBillingInquiry, objICUBPSCompanyColl.Query.MultiplesOfAmount)
            objICUBPSCompanyColl.Query.Select(objICUBPSCompanyColl.Query.IsApproved)

            If CompanyCode <> "0" Then
                objICUBPSCompanyColl.Query.Where(objICUBPSCompanyColl.Query.CompanyCode = CompanyCode)
            End If

            If CompanyName <> "0" Then
                objICUBPSCompanyColl.Query.Where(objICUBPSCompanyColl.Query.CompanyName = CompanyName)
            End If

            If CompanyType <> "0" Then
                objICUBPSCompanyColl.Query.Where(objICUBPSCompanyColl.Query.CompanyType = CompanyType)
            End If

            objICUBPSCompanyColl.Query.OrderBy(objICUBPSCompanyColl.Query.UBPSCompanyID.Descending)


            dt = objICUBPSCompanyColl.Query.LoadDataTable()

            If Not PageNumber = 0 Then
                objICUBPSCompanyColl.Query.es.PageNumber = PageNumber
                objICUBPSCompanyColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                objICUBPSCompanyColl.Query.es.PageNumber = 1
                objICUBPSCompanyColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub

        Public Shared Sub DeleteUBPSCompanyByCode(ByVal UBPSCompanyID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICUBPSCompany As New ICUBPSCompany

            Dim StrCompanyName, StrCompanyCode As String
            Dim StrUserActivity As String = ""

            If objICUBPSCompany.LoadByPrimaryKey(UBPSCompanyID) Then
                StrCompanyCode = objICUBPSCompany.CompanyCode.ToString
                StrCompanyName = objICUBPSCompany.CompanyName.ToString

                StrUserActivity = "UBP Company with Name [" & StrCompanyName & "] , Code [ " & StrCompanyCode & " ] is deleted successfully. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"

                objICUBPSCompany.MarkAsDeleted()
                objICUBPSCompany.Save()

                ICUtilities.AddAuditTrail(StrUserActivity, "UBP Company", UBPSCompanyID, UsersID, UsersName, "DELETE")

            End If
        End Sub

        Public Shared Sub ApproveUBPSCompanyByCode(ByVal UBPSCompanyID As String, ByVal UsersID As String, ByVal UsersName As String, ByVal IsApproved As Boolean)
            Dim objICUBPSCompany As New ICUBPSCompany

            Dim StrCompanyName, StrCompanyCode As String
            Dim StrUserActivity As String = ""

            If objICUBPSCompany.LoadByPrimaryKey(UBPSCompanyID) Then
                StrCompanyCode = objICUBPSCompany.CompanyCode.ToString
                StrCompanyName = objICUBPSCompany.CompanyName.ToString

                StrUserActivity = "UBPSCompany Code [ " & objICUBPSCompany.CompanyCode & " ], Name [" & objICUBPSCompany.CompanyName & "] is "
                If IsApproved = True Then
                    StrUserActivity += "approved successfully. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
                Else
                    StrUserActivity += "un approved successfully. Action was taken by user [ " & UsersID & " ] [ " & UsersName & " ]"
                End If


                objICUBPSCompany.IsApproved = IsApproved
                objICUBPSCompany.ApprovedDate = Date.Now
                objICUBPSCompany.ApprovedBy = UsersID
                objICUBPSCompany.Save()

                ICUtilities.AddAuditTrail(StrUserActivity, "UBP Company", UBPSCompanyID, UsersID, UsersName, "APPROVE")

            End If
        End Sub

        

        Public Shared Function GetRegularExpressionByCompanyID(ByVal CompanyID As String) As ICUBPSCompany
            Dim objIcUBPSCompany As New ICUBPSCompany

            If objIcUBPSCompany.LoadByPrimaryKey(CompanyID) Then
                ' objIBCompany.Query.Select(objIBCompany.Query.ReferenceFieldMinLength, objIBCompany.Query.ReferenceFieldMaxLength, objIBCompany.Query.RegularExpressionForReferenceField)
                objIcUBPSCompany.Query.Select(objIcUBPSCompany.Query.ReferenceFieldMinLength, objIcUBPSCompany.Query.ReferenceFieldMaxLength, objIcUBPSCompany.Query.RegularExpressionForReferenceField, objIcUBPSCompany.Query.ReferenceFieldCaption, objIcUBPSCompany.Query.ReferenceFieldFormat)
            End If

            Return objIcUBPSCompany

        End Function

        Public Shared Function GetAllCompanyTypeByCompanyName(ByVal CompanyID As String) As DataTable
            Dim objICUBPSCompanyTypeColl As New ICUBPSCompanyCollection
            Dim dt As New DataTable

            objICUBPSCompanyTypeColl.Query.Select(objICUBPSCompanyTypeColl.Query.CompanyType)
            If CompanyID <> "0" Then
                objICUBPSCompanyTypeColl.Query.Where(objICUBPSCompanyTypeColl.Query.UBPSCompanyID = CompanyID)
            End If
            objICUBPSCompanyTypeColl.Query.es.Distinct = True
            dt = objICUBPSCompanyTypeColl.Query.LoadDataTable

            If dt.Rows.Count > 0 Then
                Return dt
            End If

        End Function

        Public Shared Function GetAllCompanyByCompanyCode(ByVal CompanyCode As String) As DataTable
            Dim objICUBPSCompanyTypeColl As New ICUBPSCompanyCollection
            Dim dt As New DataTable

            objICUBPSCompanyTypeColl.Query.Select(objICUBPSCompanyTypeColl.Query.UBPSCompanyID, objICUBPSCompanyTypeColl.Query.CompanyName)
            If CompanyCode <> "0" Then
                objICUBPSCompanyTypeColl.Query.Where(objICUBPSCompanyTypeColl.Query.CompanyCode = CompanyCode)
            End If
            dt = objICUBPSCompanyTypeColl.Query.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllCompanyCode() As DataTable
            Dim objICUBPSCompanyTypeColl As New ICUBPSCompanyCollection
            Dim dt As New DataTable

            objICUBPSCompanyTypeColl.Query.Select(objICUBPSCompanyTypeColl.Query.CompanyCode)
            objICUBPSCompanyTypeColl.Query.es.Distinct = True
            dt = objICUBPSCompanyTypeColl.Query.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllUBPActiveApproveCompany() As DataTable
            Dim objICUBPCompanyColl As New ICUBPSCompanyCollection
            objICUBPCompanyColl.Query.Where(objICUBPCompanyColl.Query.IsActive = True And objICUBPCompanyColl.Query.IsApproved = True)
            objICUBPCompanyColl.Query.OrderBy(objICUBPCompanyColl.Query.UBPSCompanyID.Ascending)
            Return objICUBPCompanyColl.Query.LoadDataTable
        End Function
        Public Shared Function GetAllUBPActiveApproveCompanyByCompanyType(ByVal ComapnyType As String) As DataTable
            Dim objICUBPCompanyColl As New ICUBPSCompanyCollection
            objICUBPCompanyColl.Query.Where(objICUBPCompanyColl.Query.IsActive = True And objICUBPCompanyColl.Query.IsApproved = True)
            objICUBPCompanyColl.Query.Where(objICUBPCompanyColl.Query.CompanyType = ComapnyType)
            objICUBPCompanyColl.Query.OrderBy(objICUBPCompanyColl.Query.UBPSCompanyID.Ascending)
            Return objICUBPCompanyColl.Query.LoadDataTable
        End Function
        Public Shared Function CheckReferenceFieldLength(ByVal ReferenceFieldTextLength As String, ByVal UBPCompanyCode As String) As Boolean
            Dim Result As Boolean = True
            Dim objICUBPCompany As New ICUBPSCompany
            If objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode) Then
                If (ReferenceFieldTextLength < objICUBPCompany.ReferenceFieldMinLength) Or (ReferenceFieldTextLength > objICUBPCompany.ReferenceFieldMaxLength) Then
                    Result = False

                End If
            End If
            Return Result
        End Function
        Public Shared Function CheckReferenceFieldDataWithRegularExpression(ByVal ReferenceFieldText As String, ByVal UBPCompanyCode As String) As Boolean
            Dim Result As Boolean = False
            Dim objICUBPCompany As New ICUBPSCompany
            If objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode) Then
                If objICUBPCompany.RegularExpressionForReferenceField = "Numeric" Then
                    If IsNumeric(ReferenceFieldText) = True Then
                        Result = True
                    Else
                        Result = False
                    End If
                ElseIf objICUBPCompany.RegularExpressionForReferenceField = "Alpha Numeric" Then
                    Result = True
                End If
            End If
            Return Result
        End Function
        Public Shared Function CheckUBPCompanyAmountAcceptance(ByVal EnteredAmount As Double, ByVal UBPCompanyCode As String) As String
            Dim Result As String = ""
            Dim objICUBPCompany As New ICUBPSCompany
            Dim Remainder As Integer = 0
            If objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode) Then
                If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" And EnteredAmount = 0 Then
                    Result = "OK"
                Else
                    If objICUBPCompany.MultiplesOfAmount = "Any Amount" And Not EnteredAmount = 0 Then
                        Result = "OK"
                    ElseIf objICUBPCompany.MultiplesOfAmount = "Any amount within Min & Max" Then
                        If Not EnteredAmount = 0 Then
                            If Not EnteredAmount < objICUBPCompany.MinValue And Not EnteredAmount > objICUBPCompany.MaxValue Then
                                Result = "OK"
                            Else
                                Result = "Amount must be in between minimum and maximum limit"
                            End If
                        Else
                            Result = "Amount must be greater than zero"
                        End If
                    ElseIf objICUBPCompany.MultiplesOfAmount = "Multiple of X within Min & Max" Then
                        If Not EnteredAmount = 0 Then
                            If Not EnteredAmount < objICUBPCompany.MinValue And Not EnteredAmount > objICUBPCompany.MaxValue Then
                                If (EnteredAmount Mod objICUBPCompany.XAmountValue) = 0 Then
                                    Result = "OK"
                                Else
                                    Result = "Amount must be in multiples of [ " & objICUBPCompany.XAmountValue & " ]"
                                End If
                            Else
                                Result = "Amount must be in between minimum and maximum limit"
                            End If
                        Else
                            Result = "Amount must be greater than zero"
                        End If
                    End If

                End If
            End If
            Return Result
        End Function
        Public Shared Function IsUBPCompanyExistByCompanyNameOrCodeorID(ByVal UBSCompanCode As String) As Boolean
            Dim objICUBPCompany As ICUBPSCompany
            objICUBPCompany = New ICUBPSCompany
            Dim result As Integer
            If Integer.TryParse(UBSCompanCode, result) = True Then
                objICUBPCompany.Query.Where(objICUBPCompany.Query.UBPSCompanyID = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return True
                Else
                    objICUBPCompany = New ICUBPSCompany
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return True
                        Exit Function
                    Else

                        Return False
                        Exit Function
                    End If
                End If
            Else
                objICUBPCompany = New ICUBPSCompany
                objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return True
                    Exit Function
                Else

                    objICUBPCompany = New ICUBPSCompany
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyName = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return True
                        Exit Function
                    Else

                        Return False
                        Exit Function
                    End If
                End If
            End If
        End Function
        Public Shared Function GetUBPCompanyIDByCompanyCodeor(ByVal UBSCompanCode As String) As String
            Dim objICUBPCompany As ICUBPSCompanyCollection
            objICUBPCompany = New ICUBPSCompanyCollection
            Dim CompanyID As String = ""

            objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
            If objICUBPCompany.Query.Load() Then
                CompanyID = objICUBPCompany(0).UBPSCompanyID.ToString

            Else
                CompanyID = "0"
            End If

            Return CompanyID
        End Function
        Public Shared Function GetUBPCompanyIDByCompanyNameOrCodeorID(ByVal UBSCompanCode As String) As String
            Dim objICUBPCompany As ICUBPSCompanyCollection
            objICUBPCompany = New ICUBPSCompanyCollection
            Dim result As Integer
            If Integer.TryParse(UBSCompanCode, result) = True Then
                objICUBPCompany.Query.Where(objICUBPCompany.Query.UBPSCompanyID = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return objICUBPCompany(0).UBPSCompanyID.ToString
                Else
                    objICUBPCompany = New ICUBPSCompanyCollection
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return objICUBPCompany(0).UBPSCompanyID.ToString

                    Else

                        Return ""
                        Exit Function
                    End If
                End If
            Else
                objICUBPCompany = New ICUBPSCompanyCollection
                objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return objICUBPCompany(0).UBPSCompanyID.ToString
                    Exit Function
                Else

                    objICUBPCompany = New ICUBPSCompanyCollection
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyName = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return objICUBPCompany(0).UBPSCompanyID.ToString
                        Exit Function
                    Else

                        Return ""
                        Exit Function
                    End If
                End If
            End If
        End Function
        Public Shared Function GetUBPCompanyCodeByCompanyNameOrCodeorID(ByVal UBSCompanCode As String) As String
            Dim objICUBPCompany As ICUBPSCompanyCollection
            objICUBPCompany = New ICUBPSCompanyCollection
            Dim result As Integer
            If Integer.TryParse(UBSCompanCode, result) = True Then
                objICUBPCompany.Query.Where(objICUBPCompany.Query.UBPSCompanyID = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return objICUBPCompany(0).CompanyCode.ToString
                Else
                    objICUBPCompany = New ICUBPSCompanyCollection
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return objICUBPCompany(0).CompanyCode.ToString

                    Else

                        Return ""
                        Exit Function
                    End If
                End If
            Else
                objICUBPCompany = New ICUBPSCompanyCollection
                objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                If objICUBPCompany.Query.Load() Then
                    Return objICUBPCompany(0).CompanyCode.ToString
                    Exit Function
                Else

                    objICUBPCompany = New ICUBPSCompanyCollection
                    objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyName = UBSCompanCode And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                    If objICUBPCompany.Query.Load() Then
                        Return objICUBPCompany(0).CompanyCode.ToString
                        Exit Function
                    Else

                        Return ""
                        Exit Function
                    End If
                End If
            End If
        End Function
    End Class

End Namespace