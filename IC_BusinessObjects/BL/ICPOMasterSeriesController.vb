﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICPOMasterSeriesController
        Public Shared Function AddPOMasterSeries(ByVal POMasterSeries As ICPOMasterSeries, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As Integer
            Dim objICPOMasterSeries As New ICPOMasterSeries
            Dim prevobjICPOMasterSeries As New ICPOMasterSeries
            Dim IsActiveText As String
            Dim i As Integer = 0
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""

            objICPOMasterSeries.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objICPOMasterSeries.CreatedBy = POMasterSeries.CreatedBy
                objICPOMasterSeries.CreatedDate = POMasterSeries.CreatedDate
                objICPOMasterSeries.Creater = POMasterSeries.Creater
                objICPOMasterSeries.CreationDate = POMasterSeries.CreationDate

            Else
                objICPOMasterSeries.LoadByPrimaryKey(POMasterSeries.POMasterSeriesID)
                objICPOMasterSeries.CreatedBy = POMasterSeries.CreatedBy
                objICPOMasterSeries.CreatedDate = POMasterSeries.CreatedDate
                prevobjICPOMasterSeries.LoadByPrimaryKey(POMasterSeries.POMasterSeriesID)
            End If

            objICPOMasterSeries.PreFix = POMasterSeries.PreFix

            objICPOMasterSeries.StartFrom = POMasterSeries.StartFrom
            objICPOMasterSeries.EndsAt = POMasterSeries.EndsAt

            objICPOMasterSeries.IsActive = True

            If objICPOMasterSeries.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            objICPOMasterSeries.Save()

            If (isUpdate = False) Then
                CurrentAt = "POMasterSeries [Code:  " & objICPOMasterSeries.POMasterSeriesID.ToString() & " ; Prefix:  " & objICPOMasterSeries.PreFix.ToString() & " ; Start From:  " & objICPOMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICPOMasterSeries.EndsAt.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "POMasterSeries", objICPOMasterSeries.POMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "POMasterSeries : Current Values [Code:  " & objICPOMasterSeries.POMasterSeriesID.ToString() & " ; Prefix:  " & objICPOMasterSeries.PreFix.ToString() & " ; Start From:  " & objICPOMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICPOMasterSeries.EndsAt.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                PrevAt = "<br />POMasterSeries : Previous Values [Code:  " & prevobjICPOMasterSeries.POMasterSeriesID.ToString() & " ; Prefix:  " & prevobjICPOMasterSeries.PreFix.ToString() & " ; Start From:  " & prevobjICPOMasterSeries.StartFrom.ToString() & " ; Ends At:  " & prevobjICPOMasterSeries.EndsAt.ToString() & " ; IsActive:  " & prevobjICPOMasterSeries.IsActive.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "POMasterSeries", objICPOMasterSeries.POMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

            If Not objICPOMasterSeries.POMasterSeriesID = 0 Then
                i = objICPOMasterSeries.POMasterSeriesID
            Else
                i = 0
            End If
            Return i

        End Function

        Public Shared Sub GetAllPOMasterSeriesForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryPOMasterSeries As New ICPOMasterSeriesQuery("qryPOMasterSeries")
            Dim qryPOInstruments As New ICPOInstrumentsQuery("qryPOInstruments")
            Dim qryPOInstrumentUsed As New ICPOInstrumentsQuery("qryPOInstrumentUsed")
            Dim qryPOInstrumentUnUsed As New ICPOInstrumentsQuery("qryPOInstrumentUnUsed")
            Dim dt As New DataTable

           
            qryPOMasterSeries.Select(qryPOMasterSeries.POMasterSeriesID, qryPOMasterSeries.StartFrom, qryPOMasterSeries.EndsAt, qryPOMasterSeries.PreFix)
            qryPOMasterSeries.Select(qryPOInstruments.POInstrumentNumber.Count.Coalesce("'-'").As("TotalCount"))


            qryPOMasterSeries.Select((qryPOInstrumentUsed.[Select](qryPOInstrumentUsed.POInstrumentNumber.Count).Where((qryPOInstrumentUsed.IsUsed = True Or qryPOInstrumentUsed.IsAssigned = True) And qryPOInstrumentUsed.POMasterSeriesID = qryPOMasterSeries.POMasterSeriesID).As("IsUsed")))
            qryPOMasterSeries.Select((qryPOInstrumentUnUsed.[Select](qryPOInstrumentUnUsed.POInstrumentNumber.Count).Where((qryPOInstrumentUnUsed.IsUsed = False And qryPOInstrumentUnUsed.IsAssigned.IsNull) And qryPOInstrumentUnUsed.POMasterSeriesID = qryPOMasterSeries.POMasterSeriesID).As("UsedOn")))

        
            qryPOMasterSeries.InnerJoin(qryPOInstruments).On(qryPOMasterSeries.POMasterSeriesID = qryPOInstruments.POMasterSeriesID)
            qryPOMasterSeries.GroupBy(qryPOMasterSeries.POMasterSeriesID, qryPOMasterSeries.StartFrom, qryPOMasterSeries.EndsAt, qryPOMasterSeries.PreFix)
            qryPOMasterSeries.OrderBy(qryPOMasterSeries.POMasterSeriesID.Descending)
            dt = qryPOMasterSeries.LoadDataTable
            If Not PageNumber = 0 Then
                qryPOMasterSeries.es.PageNumber = PageNumber
                qryPOMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                qryPOMasterSeries.es.PageNumber = 1
                qryPOMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            End If

        End Sub

        Public Shared Sub DeletePOMasterSeries(ByVal POMasterSereisID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICPOMasterSeries As New ICPOMasterSeries
            Dim prevobjICPOMasterSeries As New ICPOMasterSeries
            Dim CurrentAt As String = ""

            objICPOMasterSeries.es.Connection.CommandTimeout = 3600
            prevobjICPOMasterSeries.es.Connection.CommandTimeout = 3600

            objICPOMasterSeries.LoadByPrimaryKey(POMasterSereisID)
            prevobjICPOMasterSeries.LoadByPrimaryKey(POMasterSereisID)

            CurrentAt = "POMasterSeries [Code:  " & objICPOMasterSeries.POMasterSeriesID.ToString() & " ; Prefix:  " & objICPOMasterSeries.PreFix.ToString() & " ; Start From:  " & objICPOMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICPOMasterSeries.EndsAt.ToString() & " ; IsActive:  " & objICPOMasterSeries.IsActive.ToString() & "]"

            objICPOMasterSeries.MarkAsDeleted()
            objICPOMasterSeries.Save()

         
            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "POMasterSeries", prevobjICPOMasterSeries.POMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")


        End Sub
        ''21-10-2013
        Public Shared Function GetAllPOMasterSeries() As ICPOMasterSeriesCollection
            Dim objICPOMAsterSeriesColl As New ICPOMasterSeriesCollection
            objICPOMAsterSeriesColl.es.Connection.CommandTimeout = 3600
            objICPOMAsterSeriesColl.Query.OrderBy(objICPOMAsterSeriesColl.Query.POMasterSeriesID.Descending)
            objICPOMAsterSeriesColl.Query.Load()
            Return objICPOMAsterSeriesColl

        End Function
     
    End Class
End Namespace
