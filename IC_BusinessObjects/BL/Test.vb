﻿Namespace IC
    Public Class MT940Formats : Implements Raptorious.SharpMt940Lib.Mt940Format.IMt940Format


        Public ReadOnly Property Header As Raptorious.SharpMt940Lib.Mt940Format.Seperator Implements Raptorious.SharpMt940Lib.Mt940Format.IMt940Format.Header
            Get
                Header = New Raptorious.SharpMt940Lib.Mt940Format.Seperator("{1:F01SONEPKKACTO.SN..ISN..}{2:I940-}{4:")

            End Get
        End Property

        Public ReadOnly Property Trailer As Raptorious.SharpMt940Lib.Mt940Format.Seperator Implements Raptorious.SharpMt940Lib.Mt940Format.IMt940Format.Trailer
            Get
                Trailer = New Raptorious.SharpMt940Lib.Mt940Format.Seperator("-}")
            End Get
        End Property
    End Class
End Namespace
