﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICMISReportController

        Public Shared Function AddMISReport(ByVal Report As ICMISReports, ByVal CurrentAt As String, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
            Dim objReport As New ICMISReports
            Dim prevobjReport As New ICMISReports
            '  Dim CurrentAt As String
            Dim PrevAt As String

            objReport.es.Connection.CommandTimeout = 3600
            prevobjReport.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objReport.CreatedBy = Report.CreatedBy
                objReport.CreatedDate = Report.CreatedDate
                prevobjReport.CreatedDate = Report.CreatedDate
            Else
                objReport.LoadByPrimaryKey(Report.ReportID)
                prevobjReport.LoadByPrimaryKey(Report.ReportID.ToString())

            End If

            ' objReport.ReportID = Report.ReportID
            objReport.ReportName = Report.ReportName
            objReport.ReportFileData = Report.ReportFileData
            objReport.ReportFileSize = Report.ReportFileSize
            objReport.ReportFileType = Report.ReportFileType
            objReport.ReportType = Report.ReportType

            objReport.Save()

            If (isUpdate = False) Then
                CurrentAt = "MISReport : [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; ReportFileSize:  " & objReport.ReportFileSize.ToString() & " ; ReportFileType:  " & objReport.ReportFileType.ToString() & " ; ReportType:  " & objReport.ReportType.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "MISReport", objReport.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            Else
                '  CurrentAt = "MISReport : Current Values  [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; ReportFileSize:  " & objReport.ReportFileSize.ToString() & " ; ReportFileType:  " & objReport.ReportFileType.ToString() & " ; ReportType:  " & objReport.ReportType.ToString() & "]"
                GetAllUserIDbyReportID(Report.ReportID.ToString())
                PrevAt = "<br />MISReport : Previous Values  [Code:  " & prevobjReport.ReportID.ToString() & " ; Name:  " & prevobjReport.ReportName.ToString() & " ; ReportFileSize:  " & prevobjReport.ReportFileSize.ToString() & " ; ReportFileType:  " & prevobjReport.ReportFileType.ToString() & " ; ReportType:  " & prevobjReport.ReportType.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "MISReport", objReport.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            End If

            Return objReport.ReportID
        End Function


        Public Shared Sub GetAllUserIDbyReportID(ByVal ReportID As String)
            Dim collMISReportUser As New ICMISReportUsersCollection
            Dim objMISReportUser As New ICMISReportUsers

            objMISReportUser.es.Connection.CommandTimeout = 3600

            collMISReportUser.Query.Where(collMISReportUser.Query.ReportID = ReportID.ToString())
            collMISReportUser.LoadAll()
            If collMISReportUser.Count > 0 Then
                For Each objMISReportUser In collMISReportUser
                    objMISReportUser.IsApproved = False
                Next
                collMISReportUser.Save()
            End If

        End Sub

        'Public Shared Function GetReportNameforReportViewer() As ICMISReportsCollection
        '    Dim qryObjMISReport As New ICMISReportsCollection

        '    qryObjMISReport.Query.Select(qryObjMISReport.Query.ReportID, qryObjMISReport.Query.ReportName)
        '    qryObjMISReport.Query.Where(qryObjMISReport.Query.ReportType = "Report")
        '    qryObjMISReport.Query.OrderBy(qryObjMISReport.Query.ReportName.Ascending)
        '    qryObjMISReport.Query.Load()

        '    Return qryObjMISReport

        'End Function
        Public Shared Function GetReportNameforReportViewer(ByVal UsersID As String, ByVal ReportType As String) As DataTable
            Dim qryObjMISReport As New ICMISReportsQuery("qryObjMISReport")
            Dim qryObjMISReportUsers As New ICMISReportUsersQuery("qryObjAssignMISReport")
            Dim dt As New DataTable

            qryObjMISReport.Select(qryObjMISReport.ReportID, qryObjMISReport.ReportName)
            qryObjMISReport.InnerJoin(qryObjMISReportUsers).On(qryObjMISReport.ReportID = qryObjMISReportUsers.ReportID)
            qryObjMISReport.Where(qryObjMISReport.ReportType = ReportType)
            If Not UsersID.ToString = "1" Then
                qryObjMISReport.Where(qryObjMISReportUsers.UserID = UsersID And qryObjMISReportUsers.IsApproved = True)
            End If

            dt = qryObjMISReport.LoadDataTable

            Return dt

        End Function


        Public Shared Sub DeleteReport(ByVal Report As String, ByVal UserID As String, ByVal UserName As String)

            Dim objReport As New ICMISReports
            Dim PrevAt As String
            objReport.es.Connection.CommandTimeout = 3600

            objReport.LoadByPrimaryKey(Report)
            PrevAt = "Report : [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; ReportFileSize:  " & objReport.ReportFileSize.ToString() & " ; ReportFileType:  " & objReport.ReportFileType.ToString() & " ; ReportType:  " & objReport.ReportType.ToString() & "]"

            objReport.MarkAsDeleted()
            objReport.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "MISReport", objReport.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub GetgvMISReportList(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collICReport As New ICMISReportsCollection

            If Not pagenumber = 0 Then
                collICReport.Query.Select(collICReport.Query.ReportID, collICReport.Query.ReportName, collICReport.Query.ReportType)
                collICReport.Query.OrderBy(collICReport.Query.ReportID.Descending)
                collICReport.Query.Load()
                rg.DataSource = collICReport

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collICReport.Query.es.PageNumber = 1
                collICReport.Query.es.PageSize = pagesize
                collICReport.Query.OrderBy(collICReport.Query.ReportID.Descending)
                collICReport.Query.Load()
                rg.DataSource = collICReport

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub
    End Class
End Namespace
