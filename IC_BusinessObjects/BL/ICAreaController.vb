﻿Namespace IC
    Public Class ICAreaController
        Public Shared Sub AddArea(ByVal cArea As IC.ICArea, ByVal isUpdate As Boolean)

            Dim ICArea As New IC.ICArea

            ICArea.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then

                ICArea.CreateBy = cArea.CreateBy
                ICArea.CreateDate = cArea.CreateDate
            Else
                ICArea.LoadByPrimaryKey(cArea.AreaCode)
            End If
            ICArea.AreaCode = cArea.AreaCode
            ICArea.AreaName = cArea.AreaName
            ICArea.CityCode = cArea.CityCode
            ICArea.IsActive = cArea.IsActive
            ICArea.IsApproved = False
            ICArea.Save()


        End Sub
        Public Shared Sub DeleteArea(ByVal AreaCode As String)
            Dim ICArea As New IC.ICArea

            ICArea.es.Connection.CommandTimeout = 3600

            ICArea.LoadByPrimaryKey(AreaCode)
            ICArea.MarkAsDeleted()
            ICArea.Save()
        End Sub
        Public Shared Sub ApproveArea(ByVal AreaCode As String, ByVal IsApproved As Boolean, ByVal ApprovedBy As String)

            Dim ICArea As New ICArea

            ICArea.es.Connection.CommandTimeout = 3600

            ICArea.LoadByPrimaryKey(AreaCode)
            ICArea.IsApproved = IsApproved
            ICArea.ApprovedBy = ApprovedBy
            ICArea.ApprovedOn = Date.Now
            ICArea.Save()


        End Sub
        Public Shared Function GetAllApprovedAndActiveAreas() As DataTable
            Dim ICArea As New IC.ICAreaCollection


            ICArea.es.Connection.CommandTimeout = 3600

            ICArea.Query.Where(ICArea.Query.IsActive = True And ICArea.Query.IsApproved = True)
            ICArea.Query.OrderBy(ICArea.Query.AreaName.Ascending)

            Return ICArea.Query.LoadDataTable()
        End Function
        Public Shared Function GetAllAreas() As ICAreaCollection
            Dim ICArea As New IC.ICAreaCollection


            ICArea.es.Connection.CommandTimeout = 3600

            ICArea.LoadAll()
            ICArea.Query.OrderBy(ICArea.Query.AreaName.Ascending)

            Return ICArea
        End Function
        Public Shared Function GetAreasByCityCode(ByVal CityCode As String) As ICAreaCollection
            Dim ICArea As New IC.ICAreaCollection


            ICArea.es.Connection.CommandTimeout = 3600

            ICArea.Query.Where(ICArea.Query.CityCode = CityCode.ToString())
            ICArea.Query.OrderBy(ICArea.Query.AreaName.Ascending)
            ICArea.Query.Load()
            Return ICArea
        End Function

    End Class
End Namespace
