﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICCollectionMISReportController

        Public Shared Function AddCollectionMISReport(ByVal Report As ICCollectionMISReport, ByVal CurrentAt As String, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
            Dim objReport As New ICCollectionMISReport
            Dim prevobjReport As New ICCollectionMISReport
            Dim PrevAt As String

            If (isUpdate = False) Then
                objReport.CreatedBy = Report.CreatedBy
                objReport.CreatedDate = Report.CreatedDate
                prevobjReport.CreatedDate = Report.CreatedDate
                objReport.Creater = Report.Creater
                objReport.CreationDate = Report.CreationDate
            Else
                objReport.LoadByPrimaryKey(Report.ReportID)
                prevobjReport.LoadByPrimaryKey(Report.ReportID.ToString())

            End If

            objReport.ReportName = Report.ReportName
            objReport.ReportFileData = Report.ReportFileData
            objReport.ReportFileSize = Report.ReportFileSize
            objReport.ReportFileType = Report.ReportFileType
           
            objReport.Save()

            If (isUpdate = False) Then
                CurrentAt = "Collection MIS Report : [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; Report File Size:  " & objReport.ReportFileSize.ToString() & " ; Report File Type:  " & objReport.ReportFileType.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Collection MIS Report", objReport.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            Else
                '  CurrentAt = "MISReport : Current Values  [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; ReportFileSize:  " & objReport.ReportFileSize.ToString() & " ; ReportFileType:  " & objReport.ReportFileType.ToString() & " ; ReportType:  " & objReport.ReportType.ToString() & "]"
                GetAllUserIDbyReportID(Report.ReportID.ToString())
                PrevAt = "<br />Collection MIS Report : Previous Values  [Code:  " & prevobjReport.ReportID.ToString() & " ; Name:  " & prevobjReport.ReportName.ToString() & " ; Report File Size:  " & prevobjReport.ReportFileSize.ToString() & " ; Report File Type:  " & prevobjReport.ReportFileType.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Collection MIS Report", objReport.ReportID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            End If

            Return objReport.ReportID
        End Function

        Public Shared Sub GetAllUserIDbyReportID(ByVal ReportID As String)
            Dim collMISReportUser As New ICCollectionMISReportUsersCollection
            Dim objMISReportUser As New ICCollectionMISReportUsers

            collMISReportUser.Query.Where(collMISReportUser.Query.ReportID = ReportID.ToString())
            collMISReportUser.LoadAll()
            If collMISReportUser.Count > 0 Then
                For Each objMISReportUser In collMISReportUser
                    objMISReportUser.IsApproved = False
                Next
                collMISReportUser.Save()
            End If

        End Sub

        Public Shared Sub DeleteReport(ByVal Report As String, ByVal UserID As String, ByVal UserName As String)
            Dim objReport As New ICCollectionMISReport
            Dim PrevAt As String
          
            objReport.LoadByPrimaryKey(Report)
            PrevAt = "Collection MIS Report : [Code:  " & objReport.ReportID.ToString() & " ; Name:  " & objReport.ReportName.ToString() & " ; Report File Size:  " & objReport.ReportFileSize.ToString() & " ; Report File Type:  " & objReport.ReportFileType.ToString() & "]"

            objReport.MarkAsDeleted()
            objReport.Save()

            ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "Collection MIS Report", Report, UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub GetgvMISReportList(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim collICReport As New ICCollectionMISReportCollection

            If Not pagenumber = 0 Then
                collICReport.Query.Select(collICReport.Query.ReportID, collICReport.Query.ReportName, collICReport.Query.ReportType)
                collICReport.Query.OrderBy(collICReport.Query.ReportID.Descending)
                collICReport.Query.Load()
                rg.DataSource = collICReport

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collICReport.Query.es.PageNumber = 1
                collICReport.Query.es.PageSize = pagesize
                collICReport.Query.OrderBy(collICReport.Query.ReportID.Descending)
                collICReport.Query.Load()
                rg.DataSource = collICReport

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub
    End Class
End Namespace