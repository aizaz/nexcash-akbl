﻿Imports Telerik.Web.UI
Namespace IC

    Public Class ICSubSetController
        Public Shared Function AddSubSetForPrintLocations(ByVal objICSubSet As ICSubSet, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String) As Integer
            Dim objICSubSetForSave As New ICSubSet
            Dim objICSubSetFrom As New ICSubSet
            Dim StrAction As String = ""
            Dim SubSetID As Integer = 0
            objICSubSet.es.Connection.CommandTimeout = 3600
            objICSubSetFrom.es.Connection.CommandTimeout = 3600
            If IsUpDate = False Then
                objICSubSetForSave.CreatedBy = objICSubSet.CreatedBy
                objICSubSetForSave.CreatedDate = objICSubSet.CreatedDate
                objICSubSetForSave.Creater = objICSubSet.Creater
                objICSubSetForSave.CreationDate = objICSubSet.CreationDate
            Else
                objICSubSetForSave.LoadByPrimaryKey(objICSubSet.SubSetID)
                objICSubSetFrom.LoadByPrimaryKey(objICSubSet.SubSetID)
                objICSubSetForSave.CreatedBy = objICSubSet.CreatedBy
                objICSubSetForSave.CreatedDate = objICSubSet.CreatedDate
            End If
            objICSubSetForSave.SubSetTo = objICSubSet.SubSetTo
            objICSubSetForSave.SubSetFrom = objICSubSet.SubSetFrom
            objICSubSetForSave.MasterSeriesID = objICSubSet.MasterSeriesID
            objICSubSetForSave.OfficeID = objICSubSet.OfficeID
            objICSubSetForSave.CompanyCode = objICSubSet.CompanyCode
            objICSubSetForSave.AccountNumber = objICSubSet.AccountNumber
            objICSubSetForSave.BranchCode = objICSubSet.BranchCode
            objICSubSetForSave.Currency = objICSubSet.Currency
            objICSubSetForSave.Remarks = objICSubSet.Remarks
            objICSubSetForSave.Save()
            SubSetID = objICSubSetForSave.SubSetID
            If IsUpDate = False Then
                ICUtilities.AddAuditTrail("Sub set is assigned to office [ " & objICSubSetForSave.UpToICOfficeByOfficeID.OfficeName & " ] from [ " & objICSubSetForSave.SubSetFrom & " ] to [ " & objICSubSetForSave.SubSetTo & " ] of master series [ " & objICSubSetForSave.MasterSeriesID & " ] .", "Sub Set", objICSubSetForSave.SubSetID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")
            ElseIf IsUpDate = True Then
                StrAction = Nothing
                StrAction += "Sub Set [ " & objICSubSet.SubSetID & " ] Updated : Sub Set from [ " & objICSubSetFrom.SubSetFrom & " ] to [ " & objICSubSet.SubSetFrom & " ] ; "
                StrAction += "Sub Set To " & objICSubSetFrom.SubSetTo & " to " & objICSubSet.SubSetTo & " ; "
                StrAction += "Office from [ " & objICSubSetFrom.UpToICOfficeByOfficeID.OfficeName & " ] to [ " & objICSubSet.UpToICOfficeByOfficeID.OfficeName & " ] ; "
                StrAction += "Master series [ " & objICSubSetFrom.MasterSeriesID & " ] to [ " & objICSubSet.MasterSeriesID & " ] ; "
                StrAction += "Account from [ " & objICSubSetFrom.AccountNumber & "," & objICSubSetFrom.BranchCode & "," & objICSubSetFrom.Currency & " ] to [ " & objICSubSet.AccountNumber & "," & objICSubSet.BranchCode & "," & objICSubSet.Currency & " ] ; "
                StrAction += "Remarks from [ " & objICSubSetFrom.Remarks & " ] to [ " & objICSubSet.Remarks & " ]. "
                ICUtilities.AddAuditTrail(StrAction.ToString, "Sub Set", objICSubSetForSave.SubSetID.ToString, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If
            Return SubSetID
        End Function
        
        Public Shared Sub GetAllAssignedSubSetsToPrintLocationsForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal OfficeType As String, ByVal OfficeCode As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal AccountStrArr As String(), ByVal MasterSereisID As String)

            Dim qryObjICSubSet As New ICSubSetQuery("qryObjICSubSet")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICComapny As New ICCompanyQuery("qryObjICComapny")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICInstrumentCount As New ICInstrumentsQuery("qryObjICInstrument")
            Dim qryObjICInstrumentUsedCount As New ICInstrumentsQuery("qryObjICInstrumentUsedCount")
            Dim qryObjICInstrumentUnUsedCount As New ICInstrumentsQuery("qryObjICInstrumentUnUsedCount")
            Dim qryObjICMasterSeries As New ICMasterSeriesQuery("qryObjICMasterSeries")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim dt As New DataTable



            qryObjICSubSet.Select(qryObjICSubSet.SubSetID, qryObjICSubSet.SubSetFrom, qryObjICSubSet.SubSetTo, qryObjICComapny.CompanyName)
            qryObjICSubSet.Select((qryObjICMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String) + " - " + qryObjICMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String)).As("MasterSeriesID"))
            qryObjICSubSet.Select(qryObjICSubSet.AccountNumber, qryObjICOffice.OffceType, (qryObjICOffice.OfficeCode + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICSubSet.Select(qryObjICInstrumentCount.InstrumentNumber.Count.As("Series"))
            'qryObjICSubSet.Select((qryObjICInstrumentCount.[Select](qryObjICInstrumentCount.InstrumentNumber.Count).InnerJoin(qryObjICSubSet).On(qryObjICSubSet.SubSetID = qryObjICInstrumentCount.SubSetID).GroupBy(qryObjICSubSet.OfficeID)).As("Series"))
            qryObjICSubSet.Select((qryObjICInstrumentUsedCount.[Select](qryObjICInstrumentUsedCount.InstrumentNumber.Count).Where((qryObjICInstrumentUsedCount.IsAssigned.IsNotNull Or qryObjICInstrumentUsedCount.IsUSed = True) And qryObjICInstrumentUsedCount.SubSetID = qryObjICSubSet.SubSetID And qryObjICInstrumentUsedCount.OfficeID = qryObjICSubSet.OfficeID).As("Used")))
            qryObjICSubSet.Select((qryObjICInstrumentUnUsedCount.[Select](qryObjICInstrumentUnUsedCount.InstrumentNumber.Count).Where((qryObjICInstrumentUnUsedCount.IsUSed.IsNull And qryObjICInstrumentUnUsedCount.IsAssigned.IsNull) And qryObjICInstrumentUnUsedCount.SubSetID = qryObjICSubSet.SubSetID And qryObjICInstrumentUnUsedCount.OfficeID = qryObjICSubSet.OfficeID)).As("UnUsed"))

            qryObjICSubSet.InnerJoin(qryObjICOffice).On(qryObjICSubSet.OfficeID = qryObjICOffice.OfficeID)
            qryObjICSubSet.InnerJoin(qryObjICInstrumentCount).On(qryObjICSubSet.SubSetID = qryObjICInstrumentCount.SubSetID)

            qryObjICSubSet.InnerJoin(qryObjICAccounts).On(qryObjICSubSet.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICSubSet.BranchCode = qryObjICAccounts.BranchCode And qryObjICSubSet.Currency = qryObjICAccounts.Currency)
            qryObjICSubSet.InnerJoin(qryObjICComapny).On(qryObjICAccounts.CompanyCode = qryObjICComapny.CompanyCode)
            qryObjICSubSet.InnerJoin(qryObjICGroup).On(qryObjICComapny.GroupCode = qryObjICGroup.GroupCode)
            qryObjICSubSet.InnerJoin(qryObjICMasterSeries).On(qryObjICSubSet.MasterSeriesID = qryObjICMasterSeries.MasterSeriesID)
            If Not OfficeType.ToString = "" Then
                qryObjICSubSet.Where(qryObjICOffice.OffceType = OfficeType.ToString)
            End If
            If Not OfficeCode = "" Then
                qryObjICSubSet.Where(qryObjICOffice.OfficeID = OfficeCode)
            End If
            If Not CompanyCode = "" Then
                qryObjICSubSet.Where(qryObjICComapny.CompanyCode = CompanyCode)
            End If

            If Not GroupCode = "" Then
                qryObjICSubSet.Where(qryObjICGroup.GroupCode = GroupCode)
            End If

            If Not AccountStrArr Is Nothing Then
                qryObjICSubSet.Where(qryObjICAccounts.AccountNumber = AccountStrArr(0).ToString And qryObjICAccounts.BranchCode = AccountStrArr(1).ToString And qryObjICAccounts.Currency = AccountStrArr(2).ToString)
            End If
            If Not MasterSereisID = "" Then
                qryObjICSubSet.Where(qryObjICSubSet.MasterSeriesID = MasterSereisID)
            End If

            qryObjICSubSet.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            qryObjICSubSet.GroupBy(qryObjICSubSet.SubSetID, qryObjICSubSet.OfficeID, qryObjICSubSet.SubSetFrom, qryObjICSubSet.SubSetTo, qryObjICComapny.CompanyName, qryObjICMasterSeries.StartFrom.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICMasterSeries.EndsAt.Cast(EntitySpaces.DynamicQuery.esCastType.String), qryObjICSubSet.AccountNumber, qryObjICSubSet.MasterSeriesID, qryObjICOffice.OffceType, qryObjICOffice.OfficeCode, qryObjICOffice.OfficeName)
            qryObjICSubSet.OrderBy(qryObjICSubSet.SubSetID.Descending)
            dt = qryObjICSubSet.LoadDataTable


            If Not PageNumber = 0 Then
                qryObjICSubSet.es.PageNumber = PageNumber
                qryObjICSubSet.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICSubSet.es.PageNumber = 1
                qryObjICSubSet.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

        Public Shared Sub DeleteSubSet(ByVal SubSetID As String, ByVal UsersID As String, ByVal USersName As String)
            Dim objICSubSet As New ICSubSet
            Dim objICSubSetForDelet As New ICSubSet

            objICSubSet.es.Connection.CommandTimeout = 3600
            objICSubSetForDelet.es.Connection.CommandTimeout = 3600


            objICSubSet.LoadByPrimaryKey(SubSetID.ToString)
            objICSubSetForDelet.LoadByPrimaryKey(SubSetID.ToString)
            objICSubSetForDelet.MarkAsDeleted()
            objICSubSetForDelet.Save()
            ICUtilities.AddAuditTrail("Sub set : " & objICSubSet.SubSetID & " deleted", "Sub Set", objICSubSet.SubSetID.ToString, UsersID.ToString, USersName.ToString, "DELETE")

        End Sub
    End Class

End Namespace