﻿Namespace IC
    Public Class ICInstructionStatusController
        Public Shared Function GetAllDistinctStatus() As DataTable
            Dim dt As New DataTable
            Dim objICInstructionStatusColl As New ICInstructionStatusCollection
            objICInstructionStatusColl.es.Connection.CommandTimeout = 3600

            objICInstructionStatusColl.LoadAll()
            objICInstructionStatusColl.Query.es.Distinct = True
            objICInstructionStatusColl.Query.OrderBy(objICInstructionStatusColl.Query.StatusName.Ascending)
            dt = objICInstructionStatusColl.Query.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllDistinctStatusByExceptedStatus(ByVal ArrayListStatus As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim objICInstructionStatusColl As New ICInstructionStatusCollection
            objICInstructionStatusColl.es.Connection.CommandTimeout = 3600
            objICInstructionStatusColl.Query.Where(objICInstructionStatusColl.Query.StatusID.NotIn(ArrayListStatus))
            objICInstructionStatusColl.Query.OrderBy(objICInstructionStatusColl.Query.StatusName.Ascending)
            dt = objICInstructionStatusColl.Query.LoadDataTable
            Return dt
        End Function
    End Class
End Namespace
