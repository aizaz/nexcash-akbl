﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICApprovalRuleManagementController
        Public Shared Function AddApprovalRule(ByVal objICApprovalRule As ICApprovalRule, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal ActionString As String) As Integer
            Dim objICApprovalRuleSave As New ICApprovalRule


            If IsUpDate = False Then

                objICApprovalRuleSave.CreatedBy = objICApprovalRule.CreatedBy
                objICApprovalRuleSave.CreatedDate = objICApprovalRule.CreatedDate
                objICApprovalRuleSave.Creator = objICApprovalRule.Creator
                objICApprovalRuleSave.CreationDate = objICApprovalRule.CreationDate
            Else
                objICApprovalRuleSave.LoadByPrimaryKey(objICApprovalRule.ApprovalRuleID)
                objICApprovalRuleSave.CreatedBy = objICApprovalRule.CreatedBy
                objICApprovalRuleSave.CreatedDate = objICApprovalRule.CreatedDate
            End If
            objICApprovalRuleSave.ApprovalRuleName = objICApprovalRule.ApprovalRuleName
            objICApprovalRuleSave.FromAmount = objICApprovalRule.FromAmount
            objICApprovalRuleSave.ToAmount = objICApprovalRule.ToAmount
            objICApprovalRuleSave.PaymentNatureCode = objICApprovalRule.PaymentNatureCode
            objICApprovalRuleSave.CompanyCode = objICApprovalRule.CompanyCode
            objICApprovalRuleSave.IsActive = False
            objICApprovalRuleSave.CopyCount = objICApprovalRule.CopyCount
            objICApprovalRuleSave.Save()

            If IsUpDate = False Then
                ICUtilities.AddAuditTrail(ActionString, "Approval Rule", objICApprovalRuleSave.ApprovalRuleID, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpDate = True Then
                ICUtilities.AddAuditTrail(ActionString.ToString, "Approval Rule", objICApprovalRuleSave.ApprovalRuleID, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


            Return objICApprovalRuleSave.ApprovalRuleID
        End Function
        Public Shared Sub GetAllApprovalRuleForRadGrid(ByVal PaymentNatureCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String)
            Dim qryObjICAPPRule As New ICApprovalRuleQuery("qryObjICAPPRule")
            Dim qryObjICComapny As New ICCompanyQuery("qryObjICComapny")
            Dim dt As New DataTable
            qryObjICAPPRule.Select(qryObjICAPPRule.ApprovalRuleName, qryObjICAPPRule.FromAmount, qryObjICAPPRule.ToAmount, qryObjICAPPRule.IsActive, qryObjICAPPRule.ApprovalRuleID)
            qryObjICAPPRule.Select(qryObjICComapny.CompanyCode)
            qryObjICAPPRule.LeftJoin(qryObjICComapny).On(qryObjICAPPRule.CompanyCode = qryObjICComapny.CompanyCode)
            If PaymentNatureCode <> "0" Then
                qryObjICAPPRule.Where(qryObjICAPPRule.PaymentNatureCode = PaymentNatureCode.ToString)

            End If
            If GroupCode <> "0" Then
                qryObjICAPPRule.Where(qryObjICComapny.GroupCode = GroupCode.ToString)

            End If
            If CompanyCode <> "0" Then
                qryObjICAPPRule.Where(qryObjICComapny.CompanyCode = CompanyCode.ToString)

            End If
            dt = qryObjICAPPRule.LoadDataTable()
            If Not PageNumber = 0 Then
                qryObjICAPPRule.es.PageNumber = PageNumber
                qryObjICAPPRule.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAPPRule.es.PageNumber = 1
                qryObjICAPPRule.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub DeleteApprovalRule(ByVal ApprovalRuleID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICApprovalRule As New IC.ICApprovalRule
            objICApprovalRule.LoadByPrimaryKey(ApprovalRuleID)
            objICApprovalRule.MarkAsDeleted()
            ICUtilities.AddAuditTrail("Approval Rule : [ " & objICApprovalRule.ApprovalRuleName & " ], Payment Nature [ " & objICApprovalRule.PaymentNatureCode & " ] [ " & objICApprovalRule.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ " & objICApprovalRule.FromAmount & " ] [ " & objICApprovalRule.ToAmount & " ] deleted.", "Approval Rule", objICApprovalRule.ApprovalRuleID, UsersID.ToString, UsersName.ToString, "DELETE")
            objICApprovalRule.Save()
        End Sub
        Public Shared Function GetApprovalRuleCopyCount(ByVal ApprovalRuleID As String) As Integer
            Dim qryObjICApprovalRule As New ICApprovalRuleQuery("qryObjICApprovalRule")


            qryObjICApprovalRule.Select((qryObjICApprovalRule.CopyCount.Count + 1).As("Count"))
            qryObjICApprovalRule.Where(qryObjICApprovalRule.ApprovalRuleID = ApprovalRuleID)
            qryObjICApprovalRule.GroupBy(qryObjICApprovalRule.ApprovalRuleID)

            Return CInt(qryObjICApprovalRule.LoadDataTable.Rows(0)(0))
        End Function
        Public Shared Sub CopyApprovalRule(ByVal ApprovalRuleID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICApprovalRuleSave As New ICApprovalRule
            Dim objICApprovalRuleCopy As New ICApprovalRule
            Dim ActionString As String = ""
            Dim CopyCount As Integer
            Dim AddedApprovalRuleID As Integer



            objICApprovalRuleCopy.LoadByPrimaryKey(ApprovalRuleID)


            CopyCount = GetApprovalRuleCopyCount(ApprovalRuleID)



            objICApprovalRuleSave.ApprovalRuleName = objICApprovalRuleCopy.ApprovalRuleName & "_Copy" & CopyCount
            objICApprovalRuleSave.FromAmount = objICApprovalRuleCopy.FromAmount
            objICApprovalRuleSave.ToAmount = objICApprovalRuleCopy.ToAmount
            objICApprovalRuleSave.PaymentNatureCode = objICApprovalRuleCopy.PaymentNatureCode
            objICApprovalRuleSave.CompanyCode = objICApprovalRuleCopy.CompanyCode
            objICApprovalRuleSave.IsActive = objICApprovalRuleCopy.IsActive
            objICApprovalRuleSave.CreatedBy = UsersID
            objICApprovalRuleSave.CreatedDate = Date.Now
            objICApprovalRuleSave.Creator = UsersID
            objICApprovalRuleSave.CreationDate = Date.Now
            ActionString = "Approval Rule [ " & objICApprovalRuleSave.ApprovalRuleName.ToString() & " ], For Company [ " & objICApprovalRuleSave.CompanyCode.ToString() & " ]"
            ActionString += "[ " & objICApprovalRuleSave.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ], Payment Nature [ "
            ActionString += "" & objICApprovalRuleSave.PaymentNatureCode & " ][ " & objICApprovalRuleSave.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ],"
            ActionString += " From Amount [ " & objICApprovalRuleSave.FromAmount & " ] , To Amount [" & objICApprovalRuleSave.ToAmount & " ], IsActive [ "
            ActionString += "" & objICApprovalRuleSave.IsActive & " Copied."

            AddedApprovalRuleID = AddApprovalRule(objICApprovalRuleSave, False, UsersID, UsersName, ActionString)


            ActionString = ""
            ActionString = "Approval Rule [ " & objICApprovalRuleSave.ApprovalRuleID.ToString() & " ][ " & objICApprovalRuleSave.ApprovalRuleName.ToString() & " ]"
            ActionString += " copy count updated from [ " & objICApprovalRuleCopy.CopyCount & " ] to [ " & CopyCount & " ]"


            objICApprovalRuleCopy.CopyCount = CopyCount
            AddApprovalRule(objICApprovalRuleCopy, True, UsersID, UsersName, ActionString)

            AddApprovalConditionsByApprovalRuleID(ApprovalRuleID, AddedApprovalRuleID, UsersID, UsersName)



        End Sub
        Public Shared Function AddApprovalConditionsByApprovalRuleID(ByVal PrevApprovalRuleID As String, ByVal CurrentApprovalRuleID As String, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim collObjICApprovalRuleCond As New ICApprovalRuleConditionsCollection
            Dim objICApprovalRuleCondsave As ICApprovalRuleConditions
            Dim ApprovalRuleCondID As Integer
            Dim Result As Boolean = False
            collObjICApprovalRuleCond.Query.Where(collObjICApprovalRuleCond.Query.ApprovalRuleID = PrevApprovalRuleID)
            collObjICApprovalRuleCond.Query.Load()
            If collObjICApprovalRuleCond.Query.Load Then
                For Each objICApprovalRuleCond As ICApprovalRuleConditions In collObjICApprovalRuleCond
                    objICApprovalRuleCondsave = New ICApprovalRuleConditions
                    ApprovalRuleCondID = Nothing
                    ActionString = ""
                    objICApprovalRuleCondsave.ApprovalRuleID = CurrentApprovalRuleID
                    objICApprovalRuleCondsave.ConditionType = objICApprovalRuleCond.ConditionType
                    objICApprovalRuleCondsave.SelectionCriteria = objICApprovalRuleCond.SelectionCriteria
                    objICApprovalRuleCondsave.ApprovalGroupID = objICApprovalRuleCond.ApprovalGroupID
                    objICApprovalRuleCondsave.Creator = UserID
                    objICApprovalRuleCondsave.CreationDate = Date.Now
                    objICApprovalRuleCondsave.CreatedBy = UserID
                    objICApprovalRuleCondsave.CreatedDate = Date.Now
                    ApprovalRuleCondID = ICApprovalRuleConditionsController.AddApprovalRuleCondtions(objICApprovalRuleCondsave, False, UserID, objICApprovalRuleCondsave.UpToICUserByCreatedBy.UserName.ToString)
                    AddApprovalConditionsUsersByApprovalRuleConditionID(objICApprovalRuleCond.ApprovalRuleConditionID, ApprovalRuleCondID, UserID, UserName)
                Next
                Result = True
            End If
            Return Result
        End Function

        Public Shared Function AddApprovalConditionsUsersByApprovalRuleConditionID(ByVal PrevApprovalRuleCondtionID As String, ByVal CurrentApprovalRuleConditionID As String, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim collObjICApprovalRuleCondUser As New ICApprovalRuleConditionUsersCollection
            Dim objICApprovalRuleCondUsersave As ICApprovalRuleConditionUsers
            Dim ApprovalRuleCondID As Integer
            Dim Result As Boolean = False
            collObjICApprovalRuleCondUser.Query.Where(collObjICApprovalRuleCondUser.Query.ApprovaRuleCondID = PrevApprovalRuleCondtionID)
            collObjICApprovalRuleCondUser.Query.Load()
            If collObjICApprovalRuleCondUser.Query.Load Then
                For Each objICApprovalRuleCond As ICApprovalRuleConditionUsers In collObjICApprovalRuleCondUser
                    objICApprovalRuleCondUsersave = New ICApprovalRuleConditionUsers
                    ApprovalRuleCondID = Nothing
                    ActionString = ""
                    objICApprovalRuleCondUsersave.UserID = objICApprovalRuleCond.UserID
                    objICApprovalRuleCondUsersave.UserCount = objICApprovalRuleCond.UserCount
                    objICApprovalRuleCondUsersave.ApprovaRuleCondID = CurrentApprovalRuleConditionID
                    objICApprovalRuleCondUsersave.CreatedBy = UserID
                    objICApprovalRuleCondUsersave.CreatedDate = Date.Now
                    ApprovalRuleCondID = ICApprovalRuleConditionUserController.AddApprovalRuleCondtionsUser(objICApprovalRuleCondUsersave, False, UserID, UserName)
                    Result = True
                Next
            End If
            Return Result
        End Function
        Public Shared Sub ApproveApprovalRule(ByVal ApprovalRuleID As String, ByVal ApprovedBy As Integer, ByVal UserName As String, ByVal IsApproved As Boolean)
            Dim objICApprovalRule As New IC.ICApprovalRule
            Dim CurrentAt As String
            objICApprovalRule.LoadByPrimaryKey(ApprovalRuleID)
            objICApprovalRule.ApprovedBy = ApprovedBy
            objICApprovalRule.IsActive = IsApproved

            objICApprovalRule.ApprovedOn = Now
            objICApprovalRule.Save()

            CurrentAt = "Approval rule :[ID:  " & ApprovalRuleID & " ],title [" & objICApprovalRule.ApprovalRuleName & " ], is approve  [ " & objICApprovalRule.IsActive & " ] "
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Approval Rule", ApprovalRuleID, ApprovedBy.ToString(), UserName.ToString(), "APPROVE")



        End Sub


        Public Shared Function GetApprovalRuleCollectionByApprovalRuleID(ByVal ApprovalRuleIDList As ArrayList) As ICApprovalRuleCollection

            Dim collObjICAppRule As New ICApprovalRuleCollection


            collObjICAppRule.Query.Where(collObjICAppRule.Query.ApprovalRuleID.In(ApprovalRuleIDList) And collObjICAppRule.Query.IsActive = True)
            collObjICAppRule.Query.OrderBy(collObjICAppRule.Query.ApprovalRuleID.Ascending)
            collObjICAppRule.Query.Load()
            Return collObjICAppRule

        End Function
    End Class
End Namespace
