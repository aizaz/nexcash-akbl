﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICAccountPaymentNatureProductTypeController

        ''Farhan 15-April-2015
        Public Shared Sub AddAccountPaymentNatureProductType(ByVal ICAccountPaymentNatureProductType As ICAccountsPaymentNatureProductType, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim ICAccPayNatProTyp As New ICAccountsPaymentNatureProductType
            Dim APNPTypeID As Integer = 0
            ICAccPayNatProTyp.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                ICAccPayNatProTyp.CreateBy = ICAccountPaymentNatureProductType.CreateBy
                ICAccPayNatProTyp.CreateDate = ICAccountPaymentNatureProductType.CreateDate
                ICAccPayNatProTyp.Creater = ICAccountPaymentNatureProductType.Creater
                ICAccPayNatProTyp.CreationDate = ICAccountPaymentNatureProductType.CreationDate
            Else
                ICAccPayNatProTyp.LoadByPrimaryKey(ICAccountPaymentNatureProductType.AccountNumber, ICAccountPaymentNatureProductType.BranchCode, ICAccountPaymentNatureProductType.Currency, ICAccountPaymentNatureProductType.PaymentNatureCode, ICAccountPaymentNatureProductType.ProductTypeCode)
                ICAccPayNatProTyp.CreateBy = ICAccountPaymentNatureProductType.CreateBy
                ICAccPayNatProTyp.CreateDate = ICAccountPaymentNatureProductType.CreateDate
            End If
            ICAccPayNatProTyp.AccountNumber = ICAccountPaymentNatureProductType.AccountNumber
            ICAccPayNatProTyp.BranchCode = ICAccountPaymentNatureProductType.BranchCode
            ICAccPayNatProTyp.Currency = ICAccountPaymentNatureProductType.Currency
            ICAccPayNatProTyp.PaymentNatureCode = ICAccountPaymentNatureProductType.PaymentNatureCode
            ICAccPayNatProTyp.ProductTypeCode = ICAccountPaymentNatureProductType.ProductTypeCode
            ICAccPayNatProTyp.PackageId = ICAccountPaymentNatureProductType.PackageId
            ICAccPayNatProTyp.IsActive = True
            ICAccPayNatProTyp.IsApproved = False
            ICAccPayNatProTyp.Save()

            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Account payment nature product type with account number [ " & ICAccPayNatProTyp.AccountNumber & " ], product type [ " & ICAccPayNatProTyp.UpToICProductTypeByProductTypeCode.ProductTypeName & " ] , Payment nature [ " & ICAccPayNatProTyp.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] added.", "Account Payment Nature Product Type", ICAccPayNatProTyp.AccountNumber + "-" + ICAccPayNatProTyp.BranchCode + "-" + ICAccPayNatProTyp.Currency + "-" + ICAccPayNatProTyp.PaymentNatureCode + "-" + ICAccPayNatProTyp.ProductTypeCode, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpdate = True Then
                ICUtilities.AddAuditTrail("Account payment nature product type with account number [ " & ICAccPayNatProTyp.AccountNumber & " ], product type [ " & ICAccPayNatProTyp.UpToICProductTypeByProductTypeCode.ProductTypeName & " ] , Payment nature [ " & ICAccPayNatProTyp.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] updated.", "Account Payment Nature Product Type", ICAccPayNatProTyp.AccountNumber + "-" + ICAccPayNatProTyp.BranchCode + "-" + ICAccPayNatProTyp.Currency + "-" + ICAccPayNatProTyp.PaymentNatureCode + "-" + ICAccPayNatProTyp.ProductTypeCode, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If

        End Sub
        'Public Shared Sub AddAccountPaymentNatureProductType(ByVal ICAccountPaymentNatureProductType As ICAccountsPaymentNatureProductType, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
        '    Dim ICAccPayNatProTyp As New ICAccountsPaymentNatureProductType
        '    Dim APNPTypeID As Integer = 0
        '    ICAccPayNatProTyp.es.Connection.CommandTimeout = 3600

        '    If IsUpdate = False Then
        '        ICAccPayNatProTyp.CreateBy = ICAccountPaymentNatureProductType.CreateBy
        '        ICAccPayNatProTyp.CreateDate = ICAccountPaymentNatureProductType.CreateDate
        '        ICAccPayNatProTyp.Creater = ICAccountPaymentNatureProductType.Creater
        '        ICAccPayNatProTyp.CreationDate = ICAccountPaymentNatureProductType.CreationDate
        '    Else
        '        ICAccPayNatProTyp.LoadByPrimaryKey(ICAccountPaymentNatureProductType.AccountNumber, ICAccountPaymentNatureProductType.BranchCode, ICAccountPaymentNatureProductType.Currency, ICAccountPaymentNatureProductType.PaymentNatureCode, ICAccountPaymentNatureProductType.ProductTypeCode)
        '        ICAccPayNatProTyp.CreateBy = ICAccountPaymentNatureProductType.CreateBy
        '        ICAccPayNatProTyp.CreateDate = ICAccountPaymentNatureProductType.CreateDate
        '    End If
        '    ICAccPayNatProTyp.AccountNumber = ICAccountPaymentNatureProductType.AccountNumber
        '    ICAccPayNatProTyp.BranchCode = ICAccountPaymentNatureProductType.BranchCode
        '    ICAccPayNatProTyp.Currency = ICAccountPaymentNatureProductType.Currency
        '    ICAccPayNatProTyp.PaymentNatureCode = ICAccountPaymentNatureProductType.PaymentNatureCode
        '    ICAccPayNatProTyp.ProductTypeCode = ICAccountPaymentNatureProductType.ProductTypeCode
        '    ICAccPayNatProTyp.PackageId = ICAccountPaymentNatureProductType.PackageId
        '    ICAccPayNatProTyp.IsActive = True
        '    ICAccPayNatProTyp.IsApproved = True
        '    ICAccPayNatProTyp.Save()

        '    If IsUpdate = False Then
        '        ICUtilities.AddAuditTrail("Account payment nature product type with account number [ " & ICAccPayNatProTyp.AccountNumber & " ], product type [ " & ICAccPayNatProTyp.UpToICProductTypeByProductTypeCode.ProductTypeName & " ] , Payment nature [ " & ICAccPayNatProTyp.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] added.", "Account Payment Nature Product Type", ICAccPayNatProTyp.AccountNumber + "-" + ICAccPayNatProTyp.BranchCode + "-" + ICAccPayNatProTyp.Currency + "-" + ICAccPayNatProTyp.PaymentNatureCode + "-" + ICAccPayNatProTyp.ProductTypeCode, UsersID.ToString, UsersName.ToString, "ADD")
        '    ElseIf IsUpdate = True Then
        '        ICUtilities.AddAuditTrail("Account payment nature product type with account number [ " & ICAccPayNatProTyp.AccountNumber & " ], product type [ " & ICAccPayNatProTyp.UpToICProductTypeByProductTypeCode.ProductTypeName & " ] , Payment nature [ " & ICAccPayNatProTyp.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] updated.", "Account Payment Nature Product Type", ICAccPayNatProTyp.AccountNumber + "-" + ICAccPayNatProTyp.BranchCode + "-" + ICAccPayNatProTyp.Currency + "-" + ICAccPayNatProTyp.PaymentNatureCode + "-" + ICAccPayNatProTyp.ProductTypeCode, UsersID.ToString, UsersName.ToString, "UPDATE")
        '    End If

        'End Sub
        Public Shared Function GetAllTaggedAccountPaymentNatureByCompanyAndProductTypeCode(ByVal CompanyCode As String, ByVal ProductTypeCode As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            qryObjICAPNPType.Select((qryObjICAPNPType.AccountNumber + " - " + qryObjICPaymentNature.PaymentNatureName).As("PaymentNatureName"))
            qryObjICAPNPType.InnerJoin(qryObjICPaymentNature).On(qryObjICAPNPType.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAPNPType.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNPType.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPType.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAPNPType.Where(qryObjICAPNPType.ProductTypeCode = ProductTypeCode And qryObjICCompany.CompanyCode = CompanyCode)
            qryObjICAPNPType.es.Distinct = True
            dt = qryObjICAPNPType.LoadDataTable
            Return dt



        End Function
        Public Shared Function GetAllProductTypesByAccountAndPaymentNatureSecond(ByVal objICAPNature As ICAccountsPaymentNature) As DataTable
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICAPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAPaymentNature")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim dt As New DataTable
            qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.Where(qryObjICAPNPType.AccountNumber = objICAPNature.AccountNumber And qryObjICAPNPType.BranchCode = objICAPNature.BranchCode And qryObjICAPNPType.Currency = objICAPNature.Currency And qryObjICAPNPType.PaymentNatureCode = objICAPNature.PaymentNatureCode)
            qryObjICAPNPType.Where(qryObjICProductType.DisbursementMode = "DD" Or qryObjICProductType.DisbursementMode = "PO" Or qryObjICProductType.DisbursementMode = "Cheque")
            qryObjICAPNPType.OrderBy(qryObjICProductType.ProductTypeCode, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)

            dt = qryObjICAPNPType.LoadDataTable()


            Return dt
        End Function
        Public Shared Sub DeleteAccountPaymentNatureProductType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNPTypeForDelete As New ICAccountsPaymentNatureProductType

            objICAPNPTypeForDelete.es.Connection.CommandTimeout = 3600
            If objICAPNPTypeForDelete.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode) Then
                objICAPNPTypeForDelete.MarkAsDeleted()
                objICAPNPTypeForDelete.Save()
                ICUtilities.AddAuditTrail("Account payment nature product type with account number [ " & objICAPNPType.AccountNumber & " ], Branch Code [ " & objICAPNPType.BranchCode & " ], Currency [ " & objICAPNPType.Currency & " ], Product type [ " & objICAPNPType.ProductTypeCode & " ], Payment nature code [ " & objICAPNPType.PaymentNatureCode & " ]  deleted.", "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "-" + objICAPNPType.BranchCode + "-" + objICAPNPType.Currency + "-" + objICAPNPType.PaymentNatureCode + "-" + objICAPNPType.ProductTypeCode, UsersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub

        Public Shared Function CheckAccountPaymentNatureProductTypeDuplicate(ByVal ICAccountPaymentNatureProductType As ICAccountsPaymentNatureProductType) As Boolean
            Dim ICAccPayNatProTyp As New ICAccountsPaymentNatureProductTypeCollection
            ICAccPayNatProTyp.Query.Where(ICAccPayNatProTyp.Query.AccountNumber = ICAccountPaymentNatureProductType.AccountNumber And ICAccPayNatProTyp.Query.BranchCode = ICAccountPaymentNatureProductType.BranchCode And ICAccPayNatProTyp.Query.Currency = ICAccountPaymentNatureProductType.Currency And ICAccPayNatProTyp.Query.PaymentNatureCode = ICAccountPaymentNatureProductType.PaymentNatureCode And ICAccPayNatProTyp.Query.ProductTypeCode = ICAccountPaymentNatureProductType.ProductTypeCode And ICAccPayNatProTyp.Query.CompanyCode = ICAccountPaymentNatureProductType.CompanyCode)
            If ICAccPayNatProTyp.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function GetAllAccountPaymentNatureProductType(ByVal CompanyCode As String) As DataTable
            Dim ICAccPayNatProTyp As New ICAccountsPaymentNatureProductTypeQuery("qryICAPnPt")
            Dim ICAcc As New ICAccountsQuery("qryICAcc")
            Dim ICPayNat As New ICPaymentNatureQuery("qryICPn")
            Dim ICProTyp As New ICProductTypeQuery("qryICPt")

            ICAccPayNatProTyp.Select(ICAccPayNatProTyp.AccountsPaymentNatureProductTypeCode, ICAccPayNatProTyp.AccountNumber, ICAccPayNatProTyp.BranchCode, ICAccPayNatProTyp.Currency, ICAccPayNatProTyp.PaymentNatureCode, ICAccPayNatProTyp.ProductTypeCode, ICAccPayNatProTyp.CreateDate)
            ICAccPayNatProTyp.Select(ICPayNat.PaymentNatureName.As("PaymentNatureName"))
            ICAccPayNatProTyp.Select(ICProTyp.ProductTypeName.As("ProductTypeName"))
            ICAccPayNatProTyp.Select(ICAcc.AccountTitle.As("AccountTitle"))

            ICAccPayNatProTyp.Where(ICAccPayNatProTyp.CompanyCode = CompanyCode)

            ICAccPayNatProTyp.InnerJoin(ICAcc).On(ICAccPayNatProTyp.AccountNumber = ICAcc.AccountNumber And ICAccPayNatProTyp.BranchCode = ICAcc.BranchCode And ICAccPayNatProTyp.Currency = ICAcc.Currency)
            ICAccPayNatProTyp.InnerJoin(ICPayNat).On(ICAccPayNatProTyp.PaymentNatureCode = ICPayNat.PaymentNatureCode)
            ICAccPayNatProTyp.InnerJoin(ICProTyp).On(ICAccPayNatProTyp.ProductTypeCode = ICProTyp.ProductTypeCode)

            ICAccPayNatProTyp.OrderBy(ICPayNat.PaymentNatureName.Ascending, ICAccPayNatProTyp.AccountNumber.Ascending, ICProTyp.ProductTypeName.Ascending)

            Return ICAccPayNatProTyp.LoadDataTable()
        End Function
        'Public Shared Sub GetAllAccountPaymentNatureProductTypeForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As radgrid)
        '    Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
        '    Dim qryObjICAccountPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPaymentNature")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObICGroup")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
        '    Dim dt As New DataTable
        '    qryObjICAPNPType.Select(qryObjICAPNPType.AccountNumber, qryObjICAPNPType.BranchCode, qryObjICAPNPType.Currency, qryObjICAPNPType.ProductTypeCode)
        '    qryObjICAPNPType.Select(qryObjICAPNPType.PaymentNatureCode, qryObjICProductType.ProductTypeName, qryObjICCompany.CompanyName)
        '    qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICPaymentNature.PaymentNatureName)
        '    qryObjICAPNPType.InnerJoin(qryObjICAccountPaymentNature).On(qryObjICAPNPType.AccountNumber = qryObjICAccountPaymentNature.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccountPaymentNature.BranchCode And qryObjICAPNPType.Currency = qryObjICAccountPaymentNature.Currency And qryObjICAPNPType.PaymentNatureCode = qryObjICAccountPaymentNature.PaymentNatureCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPaymentNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAccountPaymentNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPaymentNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPaymentNature.Currency = qryObjICAccounts.Currency)
        '    qryObjICAPNPType.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    If Not CompanyCode.ToString = "" Then
        '        qryObjICAPNPType.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If Not GroupCode.ToString = "" Then
        '        qryObjICAPNPType.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
        '    End If

        '    dt = qryObjICAPNPType.LoadDataTable()
        '    If Not PageNumber = 0 Then
        '        qryObjICAPNPType.es.PageNumber = PageNumber
        '        qryObjICAPNPType.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICAPNPType.es.PageNumber = 1
        '        qryObjICAPNPType.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub


        ''Farhan 15-April-2015
        Public Shared Sub GetAllAccountPaymentNatureProductTypeForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICAccountPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPaymentNature")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICCompany As New ICCompanyQuery("qryObICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObICGroup")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim dt As New DataTable
            qryObjICAPNPType.Select(qryObjICAPNPType.ProductTypeCode)
            qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICAPNPType.Select(qryObjICAPNPType.IsApproved)

            qryObjICAPNPType.InnerJoin(qryObjICAccountPaymentNature).On(qryObjICAPNPType.AccountNumber = qryObjICAccountPaymentNature.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccountPaymentNature.BranchCode And qryObjICAPNPType.Currency = qryObjICAccountPaymentNature.Currency And qryObjICAPNPType.PaymentNatureCode = qryObjICAccountPaymentNature.PaymentNatureCode)
            qryObjICAPNPType.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPaymentNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAccountPaymentNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPaymentNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPaymentNature.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPType.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAPNPType.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.GroupBy(qryObjICAPNPType.ProductTypeCode, qryObjICProductType.ProductTypeName, qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName, qryObjICAPNPType.IsApproved)

            qryObjICAPNPType.OrderBy(qryObjICAPNPType.ProductTypeCode.Descending, qryObjICProductType.ProductTypeName.Descending, qryObjICCompany.CompanyCode.Descending, qryObjICCompany.CompanyName.Descending)

            If Not CompanyCode.ToString = "" Then
                qryObjICAPNPType.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If
            If Not GroupCode.ToString = "" Then
                qryObjICAPNPType.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If

            qryObjICAPNPType.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)

            dt = qryObjICAPNPType.LoadDataTable()
            If Not PageNumber = 0 Then
                qryObjICAPNPType.es.PageNumber = PageNumber
                qryObjICAPNPType.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICAPNPType.es.PageNumber = 1
                qryObjICAPNPType.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        'Public Shared Sub GetAllAccountPaymentNatureProductTypeForRadGrid(ByVal GroupCode As String, ByVal CompanyCode As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
        '    Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
        '    Dim qryObjICAccountPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPaymentNature")
        '    Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObICCompany")
        '    Dim qryObjICGroup As New ICGroupQuery("qryObICGroup")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
        '    Dim dt As New DataTable
        '    qryObjICAPNPType.Select(qryObjICAPNPType.ProductTypeCode)
        '    qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)

        '    qryObjICAPNPType.InnerJoin(qryObjICAccountPaymentNature).On(qryObjICAPNPType.AccountNumber = qryObjICAccountPaymentNature.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccountPaymentNature.BranchCode And qryObjICAPNPType.Currency = qryObjICAccountPaymentNature.Currency And qryObjICAPNPType.PaymentNatureCode = qryObjICAccountPaymentNature.PaymentNatureCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPaymentNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAccountPaymentNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPaymentNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPaymentNature.Currency = qryObjICAccounts.Currency)
        '    qryObjICAPNPType.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
        '    qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
        '    qryObjICAPNPType.GroupBy(qryObjICAPNPType.ProductTypeCode, qryObjICProductType.ProductTypeName, qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)

        '    qryObjICAPNPType.OrderBy(qryObjICAPNPType.ProductTypeCode.Descending, qryObjICProductType.ProductTypeName.Descending, qryObjICCompany.CompanyCode.Descending, qryObjICCompany.CompanyName.Descending)

        '    If Not CompanyCode.ToString = "" Then
        '        qryObjICAPNPType.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
        '    End If
        '    If Not GroupCode.ToString = "" Then
        '        qryObjICAPNPType.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
        '    End If

        '    dt = qryObjICAPNPType.LoadDataTable()
        '    If Not PageNumber = 0 Then
        '        qryObjICAPNPType.es.PageNumber = PageNumber
        '        qryObjICAPNPType.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    Else
        '        qryObjICAPNPType.es.PageNumber = 1
        '        qryObjICAPNPType.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If
        '    End If

        'End Sub
        Public Shared Function GetAllAccountPaymanetNatureProductTypeByProductTypeCodeForDelete(ByVal ProductTypeCode As String) As ICAccountsPaymentNatureProductTypeCollection
            Dim objICAPNPTypeColl As New ICAccountsPaymentNatureProductTypeCollection
            objICAPNPTypeColl.es.Connection.CommandTimeout = 3600


            objICAPNPTypeColl.Query.Where(objICAPNPTypeColl.Query.ProductTypeCode = ProductTypeCode.ToString)

            objICAPNPTypeColl.Query.Load()

            Return objICAPNPTypeColl
        End Function
        Public Shared Function GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ByVal ProductTypeCode As String, ByVal CompanyCode As String) As DataTable
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICCompany As New ICCompanyQuery("objICAPNPTypeColl")
            qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAPNPType.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNPType.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPType.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAPNPType.Where(qryObjICAPNPType.ProductTypeCode = ProductTypeCode And qryObjICCompany.CompanyCode = CompanyCode)

            Return qryObjICAPNPType.LoadDataTable
        End Function
        Public Shared Function GetAllProductTypesByAccountAndPaymentNature(ByVal objICAPNature As ICAccountsPaymentNature) As DataTable
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICAPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAPaymentNature")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim dt As New DataTable
            qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.Where(qryObjICAPNPType.AccountNumber = objICAPNature.AccountNumber And qryObjICAPNPType.BranchCode = objICAPNature.BranchCode And qryObjICAPNPType.Currency = objICAPNature.Currency And qryObjICAPNPType.PaymentNatureCode = objICAPNature.PaymentNatureCode)
            qryObjICAPNPType.Where(qryObjICAPNPType.IsApproved = True)
            qryObjICAPNPType.OrderBy(qryObjICProductType.ProductTypeCode, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)

            dt = qryObjICAPNPType.LoadDataTable()


            Return dt
        End Function
        Public Shared Function GetProductTypesTaggedWithCompanies(ByVal CompanyCode As String) As DataTable

            Dim qryObjICAPNPtype As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPtype")
            Dim qryObjICAPNarture As New ICAccountsPaymentNatureQuery("qryObjICAPNarture")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryOjICProductType As New ICProductTypeQuery("qryOjICProductType")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")


            qryObjICAPNPtype.Select(qryObjICAPNPtype.ProductTypeCode, qryOjICProductType.ProductTypeName)
            qryObjICAPNPtype.InnerJoin(qryObjICAPNarture).On(qryObjICAPNPtype.AccountNumber = qryObjICAPNarture.AccountNumber And qryObjICAPNPtype.BranchCode = qryObjICAPNarture.BranchCode And qryObjICAPNPtype.Currency = qryObjICAPNarture.Currency And qryObjICAPNPtype.PaymentNatureCode = qryObjICAPNarture.PaymentNatureCode)
            qryObjICAPNPtype.InnerJoin(qryObjICAccounts).On(qryObjICAPNarture.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNarture.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNarture.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPtype.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAPNPtype.InnerJoin(qryOjICProductType).On(qryObjICAPNPtype.ProductTypeCode = qryOjICProductType.ProductTypeCode)
            qryObjICAPNPtype.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            qryObjICAPNPtype.es.Distinct = True
            Return qryObjICAPNPtype.LoadDataTable()
        End Function

        Public Shared Function GetPrintingTemplatesByAccountPaymentNatureAndProductType(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal TemplateType As String, ByVal PrintLocationCode As String) As Integer

            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICAPNatureProductType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNatureProductType")
            Dim qryObjICAPNPTypeAndPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPTypeAndPrintLocation")
            Dim objICOffice As New ICOffice
            objICOffice.LoadByPrimaryKey(PrintLocationCode)
            objICOffice.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            Dim FileID As Integer = 0
            qryObjICAPNatureProductType.Select(qryObjICFiles.FileID)
            If objICOffice.OffceType = "Branch Office" Then
                If TemplateType = "Print" Then
                    qryObjICAPNatureProductType.InnerJoin(qryObjICAPNPTypeAndPrintLocation).On(qryObjICAPNatureProductType.AccountNumber = qryObjICAPNPTypeAndPrintLocation.AccountNumber And qryObjICAPNatureProductType.BranchCode = qryObjICAPNPTypeAndPrintLocation.BranchCode And qryObjICAPNatureProductType.Currency = qryObjICAPNPTypeAndPrintLocation.Currency And qryObjICAPNatureProductType.PaymentNatureCode = qryObjICAPNPTypeAndPrintLocation.PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = qryObjICAPNPTypeAndPrintLocation.ProductTypeCode)


                    qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.PrintTemplateFileIDForBank = qryObjICFiles.FileID)

                ElseIf TemplateType = "ReIssuance" Then
                    qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.ReIssuanceTemplateFileIDForBank = qryObjICFiles.FileID)
                    qryObjICAPNatureProductType.InnerJoin(qryObjICAPNPTypeAndPrintLocation).On(qryObjICAPNatureProductType.AccountNumber = qryObjICAPNPTypeAndPrintLocation.AccountNumber And qryObjICAPNatureProductType.BranchCode = qryObjICAPNPTypeAndPrintLocation.BranchCode And qryObjICAPNatureProductType.Currency = qryObjICAPNPTypeAndPrintLocation.Currency And qryObjICAPNatureProductType.PaymentNatureCode = qryObjICAPNPTypeAndPrintLocation.PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = qryObjICAPNPTypeAndPrintLocation.ProductTypeCode)
                End If

            ElseIf objICOffice.OffceType = "Company Office" Then
                If TemplateType = "Print" Then
                    qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.PrintTemplateIDForClient = qryObjICFiles.FileID)
                    qryObjICAPNatureProductType.InnerJoin(qryObjICAPNPTypeAndPrintLocation).On(qryObjICAPNatureProductType.AccountNumber = qryObjICAPNPTypeAndPrintLocation.AccountNumber And qryObjICAPNatureProductType.BranchCode = qryObjICAPNPTypeAndPrintLocation.BranchCode And qryObjICAPNatureProductType.Currency = qryObjICAPNPTypeAndPrintLocation.Currency And qryObjICAPNatureProductType.PaymentNatureCode = qryObjICAPNPTypeAndPrintLocation.PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = qryObjICAPNPTypeAndPrintLocation.ProductTypeCode)
                ElseIf TemplateType = "ReIssuance" Then
                    qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.ReIssuancePrintTemplateIDForClient = qryObjICFiles.FileID)
                    qryObjICAPNatureProductType.InnerJoin(qryObjICAPNPTypeAndPrintLocation).On(qryObjICAPNatureProductType.AccountNumber = qryObjICAPNPTypeAndPrintLocation.AccountNumber And qryObjICAPNatureProductType.BranchCode = qryObjICAPNPTypeAndPrintLocation.BranchCode And qryObjICAPNatureProductType.Currency = qryObjICAPNPTypeAndPrintLocation.Currency And qryObjICAPNatureProductType.PaymentNatureCode = qryObjICAPNPTypeAndPrintLocation.PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = qryObjICAPNPTypeAndPrintLocation.ProductTypeCode)
                End If
            End If
            qryObjICAPNatureProductType.Where(qryObjICAPNPTypeAndPrintLocation.OfficeID = PrintLocationCode And qryObjICAPNatureProductType.AccountNumber = AccountNumber And qryObjICAPNatureProductType.BranchCode = BranchCode And qryObjICAPNatureProductType.Currency = Currency And qryObjICAPNatureProductType.PaymentNatureCode = PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = ProductTypeCode)
            dt = qryObjICAPNatureProductType.LoadDataTable
            If dt.Rows.Count > 0 Then
                FileID = CInt(dt.Rows(0)(0))
            End If
            Return FileID
        End Function
        Public Shared Function GetPrintingTemplatesByAccountPaymentNatureAndProductTypeForCOTC(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal TemplateType As String) As Integer

            Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
            Dim qryObjICAPNatureProductType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNatureProductType")
            Dim qryObjICAPNPTypeAndPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPTypeAndPrintLocation")
            
            Dim dt As New DataTable
            Dim FileID As Integer = 0
            qryObjICAPNatureProductType.Select(qryObjICFiles.FileID)

            If TemplateType = "Print" Then



                qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.PrintTemplateFileIDForBank = qryObjICFiles.FileID)

            ElseIf TemplateType = "ReIssuance" Then
                qryObjICAPNatureProductType.InnerJoin(qryObjICFiles).On(qryObjICAPNatureProductType.ReIssuanceTemplateFileIDForBank = qryObjICFiles.FileID)

            End If


            qryObjICAPNatureProductType.Where(qryObjICAPNatureProductType.AccountNumber = AccountNumber And qryObjICAPNatureProductType.BranchCode = BranchCode And qryObjICAPNatureProductType.Currency = Currency And qryObjICAPNatureProductType.PaymentNatureCode = PaymentNatureCode And qryObjICAPNatureProductType.ProductTypeCode = ProductTypeCode)
            dt = qryObjICAPNatureProductType.LoadDataTable
            If dt.Rows.Count > 0 Then
                FileID = CInt(dt.Rows(0)(0))
            End If
            Return FileID
        End Function
        Public Shared Function GetAllProductTypesByAccountAndPaymentNatureForPrintLocationAmendmentQueue(ByVal CompanyCode As String) As DataTable
            Dim qryObjICProductType As New ICProductTypeQuery("qryObjICProductType")
            Dim qryObjICAPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAPaymentNature")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")

            Dim dt As New DataTable
            qryObjICAPNPType.Select(qryObjICProductType.ProductTypeName, qryObjICAPNPType.ProductTypeCode)
            qryObjICAPNPType.InnerJoin(qryObjICProductType).On(qryObjICAPNPType.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPType.InnerJoin(qryObjICAccounts).On(qryObjICAPNPType.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNPType.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNPType.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPType.Where(qryObjICAccounts.CompanyCode = CompanyCode)
            qryObjICAPNPType.Where(qryObjICProductType.DisbursementMode = "DD" Or qryObjICProductType.DisbursementMode = "PO" Or qryObjICProductType.DisbursementMode = "Cheque")
            qryObjICAPNPType.OrderBy(qryObjICProductType.ProductTypeCode, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
            qryObjICAPNPType.es.Distinct = True

            dt = qryObjICAPNPType.LoadDataTable()


            Return dt
        End Function
        Public Shared Function DeletePackageChargesByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim objICPackageChargesColl As New ICPackageChargesCollection
            Dim StrAuditTrail As String = Nothing
            Dim Result As Boolean = False

            objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.AccountNumber = objICAPNPType.AccountNumber, objICPackageChargesColl.Query.BranchCode = objICAPNPType.BranchCode)
            objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.Currency = objICAPNPType.Currency And objICPackageChargesColl.Query.ProductTypeCode = objICAPNPType.ProductTypeCode)
            objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode)
            objICPackageChargesColl.Query.OrderBy(objICPackageChargesColl.Query.ChargeId.Ascending)
            If objICPackageChargesColl.Query.Load Then
                For Each objICPackageCharge As ICPackageCharges In objICPackageChargesColl
                    StrAuditTrail = Nothing
                    StrAuditTrail = "Package Charge With ID : [ " & objICPackageCharge.ChargeId & " ] for account no [ " & objICPackageCharge.AccountNumber & " ]"
                    StrAuditTrail += "Branch Code : [ " & objICPackageCharge.BranchCode & " ] for currency[ " & objICPackageCharge.Currency & " ]"
                    StrAuditTrail += "Product Type Code : [ " & objICPackageCharge.ProductTypeCode & " ] for Payment nature code[ " & objICPackageCharge.PaymentNatureCode & " ]"
                    StrAuditTrail += " status [ " & objICPackageCharge.ChargeAmount & " ] from date [ " & objICPackageCharge.ChargeFromDate & " ] To date [ " & objICPackageCharge.ChargeToDate & " ]"
                    StrAuditTrail += " deleted"
                    objICPackageCharge.MarkAsDeleted()
                    objICPackageCharge.Save()
                    ICUtilities.AddAuditTrail(StrAuditTrail, "Package Charges", objICPackageCharge.ChargeId.ToString, UserID, UserName, "DELETE")
                Next
            End If
            Return True
        End Function
        Public Shared Sub ApproveAccountPNPT(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal ProductTypeCode As String, ByVal ApprovedBy As Integer, ByVal UserName As String, ByVal IsApproved As Boolean)
            Dim ICAccountPNPT As New IC.ICAccountsPaymentNatureProductType
            Dim CurrentAt As String
            ICAccountPNPT.LoadByPrimaryKey(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode)
            ICAccountPNPT.ApprovedBy = ApprovedBy
            ICAccountPNPT.IsApproved = IsApproved
            ICAccountPNPT.ApprovedOn = Now
            ICAccountPNPT.Save()

            CurrentAt = "Accounts Payment Nature Product Type :[Account Number:  " & ICAccountPNPT.AccountNumber.ToString() & " ], Branch Code [ " & ICAccountPNPT.BranchCode & " ], Currency [ " & ICAccountPNPT.Currency & " ] , Payment Nature Code [" & ICAccountPNPT.PaymentNatureCode.ToString & " ], is Active  [ " & ICAccountPNPT.IsActive.ToString & " ]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Client Account", ICAccountPNPT.AccountNumber.ToString + "-" + ICAccountPNPT.BranchCode + "-" + ICAccountPNPT.Currency.ToString() + "-" + ICAccountPNPT.PaymentNatureCode.ToString() + "-" + ICAccountPNPT.ProductTypeCode.ToString(), ApprovedBy.ToString(), UserName.ToString(), "APPROVE")

        End Sub
        ''Farah 30052015
        Public Shared Function GetAllAPNProductTypeByCompanyCodeandPNCode(ByVal CompanyCode As String, ByVal PaymentNatureCode As String) As DataTable
            Dim qryObjICAPNPtype As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPtype")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICProductType As New ICProductTypeQuery("qryOjICProductType")
            Dim dt As New DataTable
            qryObjICAPNPtype.es.Distinct = True
            qryObjICAPNPtype.Select(qryObjICAPNPtype.ProductTypeCode, qryObjICProductType.ProductTypeName)
            qryObjICAPNPtype.InnerJoin(qryObjICProductType).On(qryObjICAPNPtype.ProductTypeCode = qryObjICProductType.ProductTypeCode)
            qryObjICAPNPtype.InnerJoin(qryObjICAccounts).On(qryObjICAPNPtype.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAPNPtype.BranchCode = qryObjICAccounts.BranchCode And qryObjICAPNPtype.Currency = qryObjICAccounts.Currency)
            qryObjICAPNPtype.Where(qryObjICAccounts.CompanyCode = CompanyCode.ToString And qryObjICAPNPtype.PaymentNatureCode = PaymentNatureCode And qryObjICAPNPtype.IsActive = True And qryObjICAccounts.IsActive = True And qryObjICProductType.IsActive = True And qryObjICProductType.IsApproved = True)
            qryObjICAPNPtype.Where(qryObjICAccounts.IsApproved = True And qryObjICAccounts.IsActive = True And qryObjICAPNPtype.IsApproved = True)
            qryObjICAPNPtype.GroupBy(qryObjICProductType.DisbursementMode, qryObjICAPNPtype.ProductTypeCode, qryObjICProductType.ProductTypeName)
            dt = qryObjICAPNPtype.LoadDataTable()

            Return dt
        End Function
    End Class
End Namespace

