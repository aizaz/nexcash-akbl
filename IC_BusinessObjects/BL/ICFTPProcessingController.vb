﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICFTPProcessingController

        Public Shared Sub AddFTPSettings(ByVal FTPSettings As ICFTPSettings, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICFTP As New ICFTPSettings
            Dim prevobjICFTP As New ICFTPSettings

            Dim CurrentAt As String = ""
          

            objICFTP.es.Connection.CommandTimeout = 3600
            prevobjICFTP.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                objICFTP.CreatedBy = FTPSettings.CreatedBy
                objICFTP.CreatedDate = FTPSettings.CreatedDate
                objICFTP.Creater = FTPSettings.Creater
                objICFTP.CreationDate = FTPSettings.CreationDate
            Else
                objICFTP.LoadByPrimaryKey(FTPSettings.FTPSettingsID)
                objICFTP.CreatedBy = FTPSettings.CreatedBy
                objICFTP.CreatedDate = FTPSettings.CreatedDate
            End If

            objICFTP.AccountNumber = FTPSettings.AccountNumber
            objICFTP.BranchCode = FTPSettings.BranchCode
            objICFTP.Currency = FTPSettings.Currency
            objICFTP.PaymentNatureCode = FTPSettings.PaymentNatureCode
            objICFTP.GroupCode = FTPSettings.GroupCode
            objICFTP.UserID = FTPSettings.UserID
            objICFTP.FileUploadTemplateCode = FTPSettings.FileUploadTemplateCode
            objICFTP.FTPUserID = FTPSettings.FTPUserID
            objICFTP.FTPPassword = FTPSettings.FTPPassword
            objICFTP.FTPIPAddress = FTPSettings.FTPIPAddress
            objICFTP.FTPFolderName = FTPSettings.FTPFolderName


            objICFTP.Save()

            If (IsUpdate = False) Then
                CurrentAt = "FTPProcessing [Code:  " & objICFTP.FTPSettingsID.ToString() & " ;  AccountNumber:  " & objICFTP.AccountNumber.ToString() & " ; BranchCode:  " & objICFTP.BranchCode.ToString() & " ; Currency: " & objICFTP.Currency.ToString() & " ; PaymentNatureCode: " & objICFTP.PaymentNatureCode.ToString() & " ; GroupCode: " & objICFTP.GroupCode.ToString() & " ; UserID: " & objICFTP.UserID.ToString() & " ; FileUploadTemplateCode: " & objICFTP.FileUploadTemplateCode.ToString() & " ; FTPUserID: " & objICFTP.FTPUserID.ToString & " ; FTPPassword: " & objICFTP.FTPPassword.ToString & " ; FTPIPAddress: " & objICFTP.FTPIPAddress.ToString & " ; FTPSettingsID: " & objICFTP.FTPSettingsID.ToString & " ; FTPFolderName: " & objICFTP.FTPFolderName.ToString & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "FTPProcessing", objICFTP.FTPSettingsID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")

            Else
                CurrentAt = "FTPProcessing : Current Values [Code:  " & objICFTP.FTPSettingsID.ToString() & " ;  AccountNumber:  " & objICFTP.AccountNumber.ToString() & " ; BranchCode:  " & objICFTP.BranchCode.ToString() & " ; Currency: " & objICFTP.Currency.ToString() & " ; PaymentNatureCode: " & objICFTP.PaymentNatureCode.ToString() & " ; GroupCode: " & objICFTP.GroupCode.ToString() & " ; UserID: " & objICFTP.UserID.ToString() & " ; FileUploadTemplateCode: " & objICFTP.FileUploadTemplateCode.ToString() & " ; FTPUserID: " & objICFTP.FTPUserID.ToString & " ; FTPPassword: " & objICFTP.FTPPassword.ToString & " ; FTPIPAddress: " & objICFTP.FTPIPAddress.ToString & " ; FTPSettingsID: " & objICFTP.FTPSettingsID.ToString & " ; FTPFolderName: " & objICFTP.FTPFolderName.ToString & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Updated ", "FTPProcessing", objICFTP.FTPSettingsID.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")

            End If

        End Sub

        Public Shared Sub DeleteFTPSettings(ByVal objICFTPSetting As ICFTPSettings, ByVal UserID As String, ByVal UserName As String)
            Dim objFTPForDelete As New ICFTPSettings
            Dim PrevAt As String = ""
            objFTPForDelete.es.Connection.CommandTimeout = 3600
            If objFTPForDelete.LoadByPrimaryKey(objICFTPSetting.FTPSettingsID) Then
                PrevAt = "FTPProcessing [Code:  " & objICFTPSetting.FTPSettingsID.ToString() & " ;  AccountNumber:  " & objICFTPSetting.AccountNumber.ToString() & " ; BranchCode:  " & objICFTPSetting.BranchCode.ToString() & " ; Currency: " & objICFTPSetting.Currency.ToString() & " ; PaymentNatureCode: " & objICFTPSetting.PaymentNatureCode.ToString() & " ; GroupCode: " & objICFTPSetting.GroupCode.ToString() & " ; UserID: " & objICFTPSetting.UserID.ToString() & " ; FileUploadTemplateCode: " & objICFTPSetting.FileUploadTemplateCode.ToString() & " ; FTPUserID: " & objICFTPSetting.FTPUserID.ToString & " ; FTPPassword: " & objICFTPSetting.FTPPassword.ToString & " ; FTPIPAddress: " & objICFTPSetting.FTPIPAddress.ToString & " ; FTPSettingsID: " & objICFTPSetting.FTPSettingsID.ToString & " ; FTPFolderName: " & objICFTPSetting.FTPFolderName.ToString & "]"
                objFTPForDelete.MarkAsDeleted()
                objFTPForDelete.Save()
                ICUtilities.AddAuditTrail(PrevAt & " Deleted ", "FTPProcessing", objICFTPSetting.FTPSettingsID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If

        End Sub

        Public Shared Function GetFTPSettingsColl(ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PayNCode As String, ByVal TempID As String, ByVal GroupCode As String) As ICFTPSettingsCollection

            Dim collFTP As New ICFTPSettingsCollection
            collFTP.Query.Where(collFTP.Query.AccountNumber = AccNo And collFTP.Query.BranchCode = BranchCode.ToString)
            collFTP.Query.Where(collFTP.Query.Currency = Currency And collFTP.Query.PaymentNatureCode = PayNCode.ToString)
            collFTP.Query.Where(collFTP.Query.FileUploadTemplateCode = TempID.ToString And collFTP.Query.GroupCode = GroupCode.ToString)
            collFTP.Query.Load()
            Return collFTP
        End Function

        Public Shared Function GetICFTPSettingSingleObjectForUpdate(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String, ByVal TemplateID As String) As ICFTPSettings
            Dim objICFTPSettingColl As New ICFTPSettingsCollection
            objICFTPSettingColl.es.Connection.CommandTimeout = 3600

            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.AccountNumber = AccountNumber.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.BranchCode = BranchCode.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.Currency = Currency.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.PaymentNatureCode = PaymentNatureCode.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.FileUploadTemplateCode = TemplateID.ToString)

            objICFTPSettingColl.Query.Load()

            Return objICFTPSettingColl(0)

        End Function

        Public Shared Function GetICFTPSettingforDuplicate(ByVal FTPIPAddress As String, ByVal FTPUserID As String, ByVal FTPPassword As String, ByVal FTPFolderName As String) As Boolean   ' As ICFTPSettings
            Dim objICFTPSettingColl As New ICFTPSettingsCollection
            objICFTPSettingColl.es.Connection.CommandTimeout = 3600

            Dim rslt As Boolean = False

            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.FTPIPAddress = FTPIPAddress.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.FTPUserID = FTPUserID.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.FTPPassword = FTPPassword.ToString)
            objICFTPSettingColl.Query.Where(objICFTPSettingColl.Query.FTPFolderName = FTPFolderName.ToString)
            If objICFTPSettingColl(0).Query.Load Then
                Return rslt
            End If

        End Function

        Public Shared Sub GetFTPSettingTaggedUsersgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal Group As String, ByVal Company As String, ByVal AccPayNature As String, ByVal Template As String)

            Dim qryFTPSettings As New ICFTPSettingsQuery("qryFTPSettings")
           Dim qryCompany As New ICCompanyQuery("qryCompany")
            Dim qryGroup As New ICGroupQuery("qryGroup")
            Dim qryTemplate As New ICTemplateQuery("qryTemplate")
            Dim qryAccount As New ICAccountsQuery("qryAccPNature")
            Dim qryPayNature As New ICPaymentNatureQuery("qryPayNature")
            Dim dt As DataTable

            If Not pagenumber = 0 Then
                qryFTPSettings.Select((qryFTPSettings.FTPIPAddress + "-" + qryFTPSettings.FTPFolderName).As("FTPLocation"))
                qryFTPSettings.Select(qryFTPSettings.FTPSettingsID, qryFTPSettings.FTPUserID, qryFTPSettings.AccountNumber, qryFTPSettings.BranchCode, qryFTPSettings.Currency)
                qryFTPSettings.Select(qryFTPSettings.PaymentNatureCode, qryFTPSettings.FileUploadTemplateCode, qryTemplate.TemplateName)
                qryFTPSettings.Select(qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryFTPSettings.GroupCode)

          
                qryFTPSettings.InnerJoin(qryPayNature).On(qryFTPSettings.PaymentNatureCode = qryPayNature.PaymentNatureCode)
                qryFTPSettings.InnerJoin(qryTemplate).On(qryFTPSettings.FileUploadTemplateCode = qryTemplate.TemplateID)
                qryFTPSettings.InnerJoin(qryAccount).On(qryFTPSettings.AccountNumber = qryAccount.AccountNumber And qryFTPSettings.BranchCode = qryAccount.BranchCode And qryFTPSettings.Currency = qryAccount.Currency)
                qryFTPSettings.InnerJoin(qryGroup).On(qryFTPSettings.GroupCode = qryGroup.GroupCode)
                qryFTPSettings.InnerJoin(qryCompany).On(qryAccount.CompanyCode = qryCompany.CompanyCode)

                qryFTPSettings.GroupBy(qryFTPSettings.FTPIPAddress, qryFTPSettings.FTPFolderName, qryFTPSettings.FTPUserID)
                qryFTPSettings.GroupBy(qryFTPSettings.FTPSettingsID, qryFTPSettings.AccountNumber, qryFTPSettings.BranchCode, qryFTPSettings.Currency)
                qryFTPSettings.GroupBy(qryFTPSettings.PaymentNatureCode, qryFTPSettings.FileUploadTemplateCode, qryTemplate.TemplateName)
                qryFTPSettings.GroupBy(qryPayNature.PaymentNatureName, qryCompany.CompanyName, qryGroup.GroupName, qryAccount.CompanyCode, qryFTPSettings.GroupCode)

                If Group.ToString() <> "0" Then
                    qryFTPSettings.Where(qryGroup.GroupCode = Group.ToString())
                End If

                If Company.ToString() <> "0" Then
                    qryFTPSettings.Where(qryCompany.CompanyCode = Company.ToString())
                End If


                If AccPayNature.ToString() <> "0" Then
                   qryFTPSettings.Where(qryAccount.AccountNumber = AccPayNature.Split("-")(0).ToString() And qryAccount.BranchCode = AccPayNature.Split("-")(1).ToString() And qryAccount.Currency = AccPayNature.Split("-")(2).ToString())
                End If

                If Template.ToString() <> "0" Then
                    qryFTPSettings.Where(qryTemplate.TemplateID = Template.ToString())
                End If
                qryFTPSettings.Where(qryAccount.IsApproved = True And qryAccount.IsActive = True)
                qryFTPSettings.OrderBy(qryGroup.GroupName.Descending, qryCompany.CompanyName.Descending)
                dt = qryFTPSettings.LoadDataTable()
                rg.DataSource = dt

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                qryFTPSettings.es.PageNumber = 1
                qryFTPSettings.es.PageSize = pagesize
                dt = qryFTPSettings.LoadDataTable()
                rg.DataSource = dt


                If DoDataBind Then
                    rg.DataBind()
                End If


            End If
        End Sub
        Public Shared Function IsTemplateCodeTaggedWithUser(ByVal FileUploadTemplateCode As String, ByVal UserID As String) As Boolean
            Dim qryObjICFTPSetting As New ICFTPSettingsQuery("qryObjICEmailSetting")
            Dim qryObjICFUTemplate As New ICTemplateQuery("qryObjICFUTemplate")
            Dim Result As Boolean = False
            qryObjICFTPSetting.InnerJoin(qryObjICFUTemplate).On(qryObjICFTPSetting.FileUploadTemplateCode = qryObjICFUTemplate.TemplateID)
            qryObjICFTPSetting.Where(qryObjICFUTemplate.FileUploadTemplateCode = FileUploadTemplateCode And qryObjICFTPSetting.UserID = UserID)
            If qryObjICFTPSetting.LoadDataTable.Rows.Count > 0 Then
                Result = True
            End If


            Return Result



        End Function
#Region "Email and FTP Processing Work By Aizaz Ahemd [Dated: 12-Mar-2013]"
        Public Shared Function GetFTPSettingsDataTable() As DataTable
            Dim qryFTPSet As New ICFTPSettingsQuery("ftpset")
            Dim qryUser As New ICUserQuery("usr")
            Dim qryTemplate As New ICTemplateQuery("tmp")
            Dim qryOffice As New ICOfficeQuery("ofc")
            Dim qryGroup As New ICGroupQuery("grp")
            Dim qryCompany As New ICCompanyQuery("cmp")

            qryFTPSet.Select(qryFTPSet.UserID, qryUser.UserName, qryFTPSet.FTPIPAddress, qryFTPSet.FTPUserID, qryFTPSet.FTPPassword, qryFTPSet.FTPFolderName, qryFTPSet.FileUploadTemplateCode.As("TemplateID"), qryTemplate.FileUploadTemplateCode.As("TemplateCode"), qryFTPSet.AccountNumber, qryFTPSet.BranchCode, qryFTPSet.Currency, qryFTPSet.PaymentNatureCode, qryCompany.CompanyCode, qryCompany.CompanyName, qryGroup.GroupCode, qryGroup.GroupName)
            qryFTPSet.LeftJoin(qryUser).On(qryFTPSet.UserID = qryUser.UserID)
            qryFTPSet.LeftJoin(qryTemplate).On(qryFTPSet.FileUploadTemplateCode = qryTemplate.TemplateID)
            qryFTPSet.LeftJoin(qryOffice).On(qryUser.OfficeCode = qryOffice.OfficeID)
            qryFTPSet.LeftJoin(qryCompany).On(qryOffice.CompanyCode = qryCompany.CompanyCode)
            qryFTPSet.LeftJoin(qryGroup).On(qryCompany.GroupCode = qryGroup.GroupCode)







            qryFTPSet.OrderBy(qryFTPSet.UserID.Ascending)

            Return qryFTPSet.LoadDataTable()
        End Function

#End Region

    End Class
End Namespace

