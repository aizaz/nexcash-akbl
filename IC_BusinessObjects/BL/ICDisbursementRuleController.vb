﻿Namespace IC
    Public Class ICDisbursementRuleController


        Public Shared Sub AddDisbursementRules(ByRef cDisbursementRules As IC.ICDisbursementRule, ByVal isUpdate As Boolean)

            Dim objICDisbRule As New IC.ICDisbursementRule


            objICDisbRule.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then

                objICDisbRule.CreateBy = cDisbursementRules.CreateBy
                objICDisbRule.CreateDate = cDisbursementRules.CreateDate
                objICDisbRule.IsActive = True
            Else
                objICDisbRule.LoadByPrimaryKey(cDisbursementRules.DisbRuleID)
            End If
            objICDisbRule.CompanyCode = cDisbursementRules.CompanyCode
            'FRCDisbursementRules.DisbRuleName = cDisbursementRules.DisbRuleName
            objICDisbRule.FieldCondition = cDisbursementRules.FieldCondition
            objICDisbRule.FieldName = cDisbursementRules.FieldName
            objICDisbRule.DisbType = cDisbursementRules.DisbType
            objICDisbRule.FieldValue = cDisbursementRules.FieldValue
            objICDisbRule.Save()



        End Sub

        Public Shared Function CheckDuplicate(ByVal objICDisbRule As ICDisbursementRule) As Boolean
            Dim cDisbRule As New IC.ICDisbursementRule



            cDisbRule.es.Connection.CommandTimeout = 3600

            cDisbRule.Query.Where(cDisbRule.Query.FieldName = objICDisbRule.FieldName And cDisbRule.Query.FieldValue = objICDisbRule.FieldValue And cDisbRule.Query.FieldCondition = objICDisbRule.FieldCondition And cDisbRule.Query.DisbType = objICDisbRule.DisbType And cDisbRule.Query.CompanyCode = objICDisbRule.CompanyCode)
            If cDisbRule.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub DeleteDisbursementRules(ByVal DisbursementRulesCode As String)

            Dim objICDisbRule As New ICDisbursementRule


            objICDisbRule.es.Connection.CommandTimeout = 3600

            objICDisbRule.LoadByPrimaryKey(DisbursementRulesCode)
            objICDisbRule.MarkAsDeleted()
            objICDisbRule.Save()



        End Sub
        Public Shared Sub ApproveDisbursementRules(ByVal DisbursementRulesCode As String, ByVal isapproved As Boolean, ByVal ApprovedBy As String)

            Dim objICDisbRule As New ICDisbursementRule

            objICDisbRule.es.Connection.CommandTimeout = 3600


            objICDisbRule.LoadByPrimaryKey(DisbursementRulesCode)
            objICDisbRule.IsApproved = isapproved
            objICDisbRule.ApprovedBy = ApprovedBy
            objICDisbRule.ApprovedOn = Date.Now
            objICDisbRule.Save()



        End Sub
        Public Shared Function GetDisbursementRules() As ICDisbursementRuleCollection

            Dim objCollICDisbRuleColl As New ICDisbursementRuleCollection


            objCollICDisbRuleColl.es.Connection.CommandTimeout = 3600

            objCollICDisbRuleColl.LoadAll()
            Return objCollICDisbRuleColl



        End Function
        Public Shared Function GetDisbursementRulesByCompanyCodeAndDisbType(ByVal CompanyCode As String, ByVal DisbType As String) As ICDisbursementRuleCollection

            Dim objCollICDisbRuleColl As New ICDisbursementRuleCollection
            objCollICDisbRuleColl.es.Connection.CommandTimeout = 3600

            objCollICDisbRuleColl.Query.Where(objCollICDisbRuleColl.Query.CompanyCode = CompanyCode And objCollICDisbRuleColl.Query.DisbType = DisbType)
            objCollICDisbRuleColl.Query.Load()
            Return objCollICDisbRuleColl



        End Function
    End Class
End Namespace

