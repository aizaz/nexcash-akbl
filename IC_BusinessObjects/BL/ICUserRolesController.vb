﻿Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Common.Utilities
Imports Telerik.Web.UI
Namespace IC
    Public Class ICUserRolesController
        Public Shared Sub AssignRoleToUser(ByVal RoleID As Integer, ByVal PortalID As Integer, ByVal UserID As Integer)

            Dim cRole As New RoleController
            cRole.AddUserRole(PortalID, UserID, RoleID, Nothing)

            DataCache.ClearPortalCache(0, True)
        End Sub
        Public Shared Sub AddICUserRoles(ByVal ICUserRole As ICUserRoles, ByVal IsUpDate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal UserType As String, ByVal RoleType As String)
            Dim i As Integer = 0
            Dim objICUserRoles As New ICUserRoles
            Dim PKICUserRoles As String = Nothing

            PKICUserRoles = ICUserRole.UserID.ToString & "" & ICUserRole.RoleID.ToString
            objICUserRoles.es.Connection.CommandTimeout = 3600

            If IsUpDate = False Then
                'objICUserRoles.UserRolesID = ICUserRole.UserRolesID
                objICUserRoles.RoleID = ICUserRole.RoleID
                objICUserRoles.UserID = ICUserRole.UserID
                objICUserRoles.AssignedBy = ICUserRole.AssignedBy
                objICUserRoles.AssignedOn = ICUserRole.AssignedOn
                objICUserRoles.Creater = ICUserRole.Creater
                objICUserRoles.CreationDate = ICUserRole.CreationDate
                objICUserRoles.IsApproved = False
                objICUserRoles.Save()
                If UserType = "Bank User" And RoleType = "Bank Role" Then
                    ICUtilities.AddAuditTrail("Role : [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString() & " ] of type : [ " & RoleType.ToString & " ] Assigned to user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString() & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString() & " ] .", "Bank User Roles", PKICUserRoles, UsersID.ToString(), UsersName.ToString(), "ASSIGN")
                ElseIf UserType = "Bank User" And RoleType = "Company Role" Then
                    ICUtilities.AddAuditTrail("Role : [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString() & " ] of type : [ " & RoleType.ToString & " ] Assigned to user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString() & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString() & " ] .", "Bank User Roles", PKICUserRoles, UsersID.ToString(), UsersName.ToString(), "ASSIGN")
                ElseIf UserType = "Client User" And RoleType = "Company Role" Then
                    ICUtilities.AddAuditTrail("Role : [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString() & " ] of type : [ " & RoleType.ToString & " ] Assigned to user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString() & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString() & " ] .", "Client User Roles", PKICUserRoles, UsersID.ToString(), UsersName.ToString(), "ASSIGN")
                End If
            End If
        End Sub
       
        Public Shared Sub DeleteUserRoles(ByVal RoleID As Integer, ByVal UserID As Integer, ByVal UsersID As String, ByVal UsersName As String, ByVal UserType As String, ByVal RoleType As String)
            Dim objICUserRolesColl As New ICUserRolesCollection
            objICUserRolesColl.es.Connection.CommandTimeout = 3600
            objICUserRolesColl.Query.Where(objICUserRolesColl.Query.RoleID = RoleID And objICUserRolesColl.Query.UserID = UserID)
            objICUserRolesColl.Query.Load()
            If objICUserRolesColl.Query.Load() Then

                For Each objUserRole As ICUserRoles In objICUserRolesColl
                    Dim objICUserRoles2 As New ICUserRoles
                    Dim PKICUserRoles As String = Nothing


                    objICUserRoles2.es.Connection.CommandTimeout = 3600
                    objICUserRoles2.LoadByPrimaryKey(objUserRole.UserID, objUserRole.RoleID)
                    PKICUserRoles = objICUserRoles2.UserID.ToString & "" & objICUserRoles2.RoleID.ToString
                    objICUserRoles2.MarkAsDeleted()
                    objICUserRoles2.Save()
                    If UserType = "Bank User" And RoleType = "Bank Role" Then
                        ICUtilities.AddAuditTrail("Role: [ " & objUserRole.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] removed from user [ " & objUserRole.UpToICUserByUserID.UserID.ToString & "] [ " & objUserRole.UpToICUserByUserID.UserName.ToString & " ].", "Bank User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "DELETE")
                    ElseIf UserType = "Bank User" And RoleType = "Company Role" Then
                        ICUtilities.AddAuditTrail("Role: [ " & objUserRole.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] removed from user [ " & objUserRole.UpToICUserByUserID.UserID.ToString & "] [ " & objUserRole.UpToICUserByUserID.UserName.ToString & " ].", "Bank User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "DELETE")
                    ElseIf UserType = "Client User" And RoleType = "Company Role" Then
                        ICUtilities.AddAuditTrail("Role: [ " & objUserRole.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] removed from user [ " & objUserRole.UpToICUserByUserID.UserID.ToString & "] [ " & objUserRole.UpToICUserByUserID.UserName.ToString & " ].", "Client User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "DELETE")
                    End If


                Next
            End If
        End Sub
        Public Shared Sub GetAssignedUserRolesForRadGrid(ByVal UserCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryICUserRoles As New ICUserRolesQuery("qryICUserRoles")
            Dim qryObjICRoles As New ICRoleQuery("qryObjICRoles")
            Dim dt As New DataTable


            qryICUserRoles.Select(qryICUserRoles.RoleID, qryICUserRoles.UserID, qryICUserRoles.IsApproved, qryObjICRoles.RoleName)
            qryICUserRoles.InnerJoin(qryObjICRoles).On(qryICUserRoles.RoleID = qryObjICRoles.RoleID)
            qryICUserRoles.Where(qryICUserRoles.UserID = UserCode)
            qryICUserRoles.OrderBy(qryObjICRoles.RoleName.Descending)

            dt = qryICUserRoles.LoadDataTable

            If Not pagenumber = 0 Then
                qryICUserRoles.es.PageNumber = pagenumber

                qryICUserRoles.es.PageSize = pagesize


                rg.DataSource = dt


                If DoDataBind Then


                    rg.DataBind()

                End If
            Else


                qryICUserRoles.es.PageNumber = 1
                qryICUserRoles.es.PageSize = pagesize


                rg.DataSource = dt

                If DoDataBind Then


                    rg.DataBind()

                End If




            End If
        End Sub
        Public Shared Function GetAssignedUserRolesinDTByUserID(ByVal UserCode As String) As DataTable

            Dim qryICUserRoles As New ICUserRolesQuery("qryICUserRoles")
            Dim qryObjICRoles As New ICRoleQuery("qryObjICRoles")
            Dim dt As New DataTable


            qryICUserRoles.Select(qryICUserRoles.RoleID, qryICUserRoles.UserID, qryICUserRoles.IsApproved, qryObjICRoles.RoleName)
            qryICUserRoles.InnerJoin(qryObjICRoles).On(qryICUserRoles.RoleID = qryObjICRoles.RoleID)
            qryICUserRoles.Where(qryICUserRoles.UserID = UserCode)
            dt = qryICUserRoles.LoadDataTable
            Return dt
        End Function
        Public Shared Function CheckRoleAlReadyAssignToUser(ByVal UsersID As String, ByVal RoleID As String) As Boolean
            Dim Result As Boolean = False
            Dim objICUserRolesColl As New ICUserRolesCollection
            objICUserRolesColl.es.Connection.CommandTimeout = 3600

            objICUserRolesColl.Query.Where(objICUserRolesColl.Query.UserID = UsersID And objICUserRolesColl.Query.RoleID = RoleID)
            objICUserRolesColl.Query.Load()
            If objICUserRolesColl.Query.Load = True Then
                Result = True
            End If

            Return Result

        End Function
        Public Shared Sub ApproveICUserRoles(ByVal RoleID As Integer, ByVal UserIDForRole As Integer, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal UserType As String, ByVal RoleType As String)
            Dim objICUserRoles As New ICUserRoles
            Dim PKICUserRoles As String = Nothing

            PKICUserRoles = UserIDForRole.ToString & "" & RoleID.ToString
            objICUserRoles.es.Connection.CommandTimeout = 3600
            If objICUserRoles.LoadByPrimaryKey(UserIDForRole, RoleID) Then
                objICUserRoles.IsApproved = IsApproved
                objICUserRoles.ApprovedBy = UsersID
                objICUserRoles.ApprovedOn = Date.Now
                objICUserRoles.Save()
                If UserType = "Bank User" And RoleType = "Bank Role" Then
                    ICUtilities.AddAuditTrail("Role: [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] approved for user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString & " ].", "Bank User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "APPROVE")
                ElseIf UserType = "Bank User" And RoleType = "Company Role" Then
                    ICUtilities.AddAuditTrail("Role: [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] approved for user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString & " ].", "Bank User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "APPROVE")
                ElseIf UserType = "Client User" And RoleType = "Company Role" Then
                    ICUtilities.AddAuditTrail("Role: [ " & objICUserRoles.UpToICRoleByRoleID.RoleName.ToString & " ] of type : [ " & RoleType.ToString & " ] approved for user [ " & objICUserRoles.UpToICUserByUserID.UserID.ToString & " ] [ " & objICUserRoles.UpToICUserByUserID.UserName.ToString & " ].", "Client User Roles", PKICUserRoles, UsersID.ToString, UsersName.ToString, "APPROVE")
                End If

            End If
        End Sub
        Public Shared Function IsRightIsAssignedToRole(ByVal RoleID As String, ByVal RightName As String) As Boolean
            Dim Result As Boolean = False
            Dim objICRoleRightsColl As New ICRoleRightsCollection
            objICRoleRightsColl.es.Connection.CommandTimeout = 3600
            objICRoleRightsColl.Query.Where(objICRoleRightsColl.Query.RoleID = RoleID And objICRoleRightsColl.Query.RightName = RightName And objICRoleRightsColl.Query.IsApproved = True)

            If objICRoleRightsColl.Query.Load Then
                Result = True
            End If
            Return Result
        End Function

        Public Shared Function HasInstructionDetailRightsByUserId(ByVal UserCode As String) As Boolean

            Dim IsVerified As Boolean = False

            Try

                Dim qryICUserRoles As New ICUserRolesQuery("qryICUserRoles")
                Dim qryObjICRoleRights As New ICRoleRightsQuery("qryObjICRoleRights")
                Dim dt As New DataTable

                qryICUserRoles.Select(qryICUserRoles.RoleID, qryICUserRoles.UserID, qryICUserRoles.IsApproved)
                qryICUserRoles.InnerJoin(qryObjICRoleRights).On(qryICUserRoles.RoleID = qryObjICRoleRights.RoleID)
                qryICUserRoles.Where(qryObjICRoleRights.RightDetailName.Trim = "View Instruction Details")
                qryICUserRoles.Where(qryICUserRoles.UserID = UserCode And qryICUserRoles.IsApproved = True)

                dt = qryICUserRoles.LoadDataTable

                If dt.Rows.Count > 0 Then
                    IsVerified = True
                Else
                    IsVerified = False
                End If

            Catch ex As Exception
                IsVerified = False
            End Try

            Return IsVerified
        End Function



    End Class
End Namespace

