﻿Namespace IC
    Public Class ICScheduleInstructionController

        Public Shared Function AddScheduleInstruction(ByVal objICScheduleInstruction As ICScheduleTransactions, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICScheduleTransactionFoSave As New ICScheduleTransactions
            Dim objICCompany As New ICCompany
            objICCompany.LoadByPrimaryKey(objICScheduleInstruction.CompanyCode)
            Dim AuditAction As String = Nothing
            Dim ScheduleID As Integer = 0
            objICScheduleTransactionFoSave.CompanyCode = objICScheduleInstruction.CompanyCode
            objICScheduleTransactionFoSave.ClientAccountNo = objICScheduleInstruction.ClientAccountNo
            objICScheduleTransactionFoSave.ClientAccountBranchCode = objICScheduleInstruction.ClientAccountBranchCode
            objICScheduleTransactionFoSave.ClientAccountCurrency = objICScheduleInstruction.ClientAccountCurrency
            objICScheduleTransactionFoSave.PaymentNatureCode = objICScheduleInstruction.PaymentNatureCode
            objICScheduleTransactionFoSave.ProductTypeCode = objICScheduleInstruction.ProductTypeCode
            objICScheduleTransactionFoSave.BeneficiaryAccountNo = objICScheduleInstruction.BeneficiaryAccountNo
            objICScheduleTransactionFoSave.BeneficiaryAccountBranchCode = objICScheduleInstruction.BeneficiaryAccountBranchCode
            objICScheduleTransactionFoSave.BeneficiaryAccountCurrency = objICScheduleInstruction.BeneficiaryAccountCurrency
            objICScheduleTransactionFoSave.BeneficaryName = objICScheduleInstruction.BeneficaryName
            objICScheduleTransactionFoSave.TransactionCount = objICScheduleInstruction.TransactionCount
            objICScheduleTransactionFoSave.CreatedBy = objICScheduleInstruction.CreatedBy
            objICScheduleTransactionFoSave.CreatedDate = objICScheduleInstruction.CreatedDate
            objICScheduleTransactionFoSave.ScheduleTransactionFromDate = objICScheduleInstruction.ScheduleTransactionFromDate
            objICScheduleTransactionFoSave.ScheduleTransactionToDate = objICScheduleInstruction.ScheduleTransactionToDate
            objICScheduleTransactionFoSave.AmountPerInstruction = objICScheduleInstruction.AmountPerInstruction
            objICScheduleTransactionFoSave.TransactionTime = objICScheduleInstruction.TransactionTime
            objICScheduleTransactionFoSave.TrnsactionFrequency = objICScheduleInstruction.TrnsactionFrequency
            objICScheduleTransactionFoSave.DayType = objICScheduleInstruction.DayType
            objICScheduleTransactionFoSave.Save()
            ScheduleID = objICScheduleTransactionFoSave.ScheduleID
            AuditAction = Nothing
            AuditAction += "Schedule Transaction with ID [ " & ScheduleID & " ] for Client [ " & objICCompany.CompanyName & " ] for Beneficiary [ " & objICScheduleInstruction.BeneficaryName & " ]"
            AuditAction += " against Client Account No. [ " & objICScheduleInstruction.ClientAccountNo & " ], Payment Nature [ " & objICScheduleInstruction.PaymentNatureCode & " ]"
            AuditAction += " and Product Type [ " & objICScheduleInstruction.ProductTypeCode & " ] added. Action was taken by User [ " & UsersID & " ]"
            AuditAction += " [ " & UsersName & " ] "
            ICUtilities.AddAuditTrail(AuditAction, "Schedule Transaction", ScheduleID.ToString, UsersID, UsersName, "ADD")
            Return ScheduleID
        End Function

        Public Shared Function GetValueDateforRecurring(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Frequency As String, ByVal BestPosssible As String, ByVal DayType As String) As DataTable
            Dim drValueDate As DataRow
            Dim dtValueDate As New DataTable
            'Aizaz
            Dim NoOfDays As Integer = 0
            Dim cnt As Integer = 0
            Dim TempValueDate As Date = Nothing
            Dim TempValueDate2 As Date = Nothing
            Dim BestPossible2 As Date = Nothing
            Dim DayWeek As String = ""
            Dim MonthDiff As Integer = 0
            Dim ArrayLastDay As New ArrayList
            Dim i As Integer = 0
            dtValueDate.Columns.Add(New DataColumn("ValueDates", GetType(System.DateTime)))
            If Frequency.ToString() = "Daily" Then
                NoOfDays = EndDate.Subtract(StartDate).Days
                For cnt = 0 To NoOfDays
                    TempValueDate = StartDate.AddDays(cnt)
                    drValueDate = dtValueDate.NewRow()
                    drValueDate("ValueDates") = TempValueDate
                    dtValueDate.Rows.Add(drValueDate)
                Next
            ElseIf Frequency.ToString() = "Number Of Days" Then
                NoOfDays = EndDate.Subtract(StartDate).Days
                For cnt = 0 To NoOfDays
                    TempValueDate = StartDate.AddDays(cnt)
                    If TempValueDate.Day = BestPosssible Then
                        drValueDate = dtValueDate.NewRow()
                        drValueDate("ValueDates") = TempValueDate
                        dtValueDate.Rows.Add(drValueDate)
                    End If
                Next
            ElseIf Frequency.ToString() = "Monthly" Then
                If DayType = "First Day" Then
                    NoOfDays = EndDate.Subtract(StartDate).Days
                    For cnt = 0 To NoOfDays
                        TempValueDate = StartDate.AddDays(cnt)
                        If TempValueDate.Day = BestPosssible Then
                            drValueDate = dtValueDate.NewRow()
                            drValueDate("ValueDates") = TempValueDate
                            dtValueDate.Rows.Add(drValueDate)
                        End If
                    Next
                ElseIf DayType = "Last Day" Then
                    MonthDiff = CInt(BestPosssible)
                    NoOfDays = EndDate.Subtract(StartDate).Days

                    For cnt = 0 To NoOfDays
                        TempValueDate = StartDate.AddDays(cnt)
                        BestPosssible = New Date(TempValueDate.Year, TempValueDate.Month, 1).AddMonths(1).AddDays(-1).Day.ToString
                        If TempValueDate.Day = BestPosssible Then
                            drValueDate = dtValueDate.NewRow()
                            drValueDate("ValueDates") = TempValueDate
                            dtValueDate.Rows.Add(drValueDate)
                        End If
                    Next


                ElseIf DayType = "Specific Day" Then
                    NoOfDays = EndDate.Subtract(StartDate).Days
                    For cnt = 0 To NoOfDays
                        TempValueDate = StartDate.AddDays(cnt)
                        If TempValueDate.Day = BestPosssible Then
                            drValueDate = dtValueDate.NewRow()
                            drValueDate("ValueDates") = TempValueDate
                            dtValueDate.Rows.Add(drValueDate)
                        End If
                    Next
                End If
            End If
            Return dtValueDate
        End Function
        Public Shared Function GetBestPossibleDay(ByVal StartDate As Date, ByVal DayType As String, ByVal SpecificDay As String) As String
            Dim BestPossibleDay As String = Nothing
            If DayType = "First Day" Then
                BestPossibleDay = "1"
            ElseIf DayType = "Last Day" Then
                Dim LastDay As Date = New Date(StartDate.Year, StartDate.Month, StartDate.Day)
                BestPossibleDay = LastDay.AddMonths(1).AddDays(-1).Day.ToString
            ElseIf DayType = "Specific Day" Then
                BestPossibleDay = SpecificDay.ToString
            ElseIf DayType = "Number Of Days" Then
                BestPossibleDay = CDate(StartDate.AddDays(SpecificDay)).Day.ToString
            End If
            Return BestPossibleDay
        End Function
    End Class
End Namespace
