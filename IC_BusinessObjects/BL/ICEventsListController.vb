﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICEventsListController

        Public Shared Sub GetAllEventsListForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDatabind As Boolean, ByVal rg As RadGrid)
            Dim objICEventsListColl As New ICEventsCollection
            Dim dt As New DataTable
            objICEventsListColl.es.Connection.CommandTimeout = 3600


            objICEventsListColl.LoadAll()
            dt = objICEventsListColl.Query.LoadDataTable

            If Not PageNumber = 0 Then
                objICEventsListColl.Query.es.PageNumber = PageNumber
                objICEventsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDatabind = True Then
                    rg.DataBind()
                End If
            Else

                objICEventsListColl.Query.es.PageNumber = 1
                objICEventsListColl.Query.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDatabind = True Then
                    rg.DataBind()
                End If
            End If


        End Sub
       

    End Class
End Namespace
