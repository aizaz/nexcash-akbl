﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICCollectionProductTypeController

        Public Shared Function AddCollectionProductType(ByVal CollProductType As ICCollectionProductType, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As String
            Dim PrdctType As New ICCollectionProductType
            Dim prevPrdctType As New ICCollectionProductType

            Dim CurrentAt As String
            Dim PrevAt As String
            Dim IsActiveText As String
            Dim IsApprovedText As String

            PrdctType.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then
                PrdctType.CreateBy = CollProductType.CreateBy
                PrdctType.CreateDate = CollProductType.CreateDate

            Else
                PrdctType.LoadByPrimaryKey(CollProductType.ProductTypeCode)
                prevPrdctType.LoadByPrimaryKey(CollProductType.ProductTypeCode)
            End If


            PrdctType.ProductTypeCode = CollProductType.ProductTypeCode
            PrdctType.ProductTypeName = CollProductType.ProductTypeName
            PrdctType.DisbursementMode = CollProductType.DisbursementMode
            PrdctType.FloatDays = CollProductType.FloatDays


            PrdctType.IsActive = CollProductType.IsActive
            PrdctType.IsApproved = False

            PrdctType.Save()

            If PrdctType.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If PrdctType.IsApproved = True Then
                IsApprovedText = True
            Else
                IsApprovedText = False
            End If

            If (isUpdate = False) Then

                CurrentAt = "Collection Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Collection Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Collection Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else

                CurrentAt = "Collection Product Type : Current Values [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Collection Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApprovedText.ToString() & "]"
                PrevAt = "<br />Collection Product Type : Previous Values [Code:  " & prevPrdctType.ProductTypeCode.ToString() & " ; Name:  " & prevPrdctType.ProductTypeName.ToString() & " ; Collection Mode: " & prevPrdctType.DisbursementMode.ToString() & " ; IsActive:  " & prevPrdctType.IsActive.ToString() & " ; IsApproved:  " & prevPrdctType.IsApproved.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Collection Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

        End Function

        Public Shared Sub DeleteCollectionProductType(ByVal ProductTypeCode As String, ByVal UserID As String, ByVal UserName As String)

            Dim PrdctType As New ICCollectionProductType
            Dim DelPrdctType As New ICCollectionProductType
            Dim ProdctTypeName As String = ""
            Dim CurrentAt As String

            PrdctType.es.Connection.CommandTimeout = 3600

            PrdctType.LoadByPrimaryKey(ProductTypeCode)
            DelPrdctType.LoadByPrimaryKey(ProductTypeCode)
            ProdctTypeName = PrdctType.ProductTypeName.ToString()

            CurrentAt = "Collection Product Type : [Code:  " & DelPrdctType.ProductTypeCode & " ; Name:  " & DelPrdctType.ProductTypeName & " ; Collection Mode: " & DelPrdctType.DisbursementMode & " ; IsActive:  " & DelPrdctType.IsActive & " ; IsApproved:  " & DelPrdctType.IsApproved & "]"
            PrdctType.MarkAsDeleted()
            PrdctType.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Collection Product Type", DelPrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub

        Public Shared Sub ApproveCollectionProductType(ByVal ProductTypeCode As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim PrdctType As New ICCollectionProductType
            Dim CurrentAt As String

            PrdctType.es.Connection.CommandTimeout = 3600
            PrdctType.LoadByPrimaryKey(ProductTypeCode)
            PrdctType.IsApproved = IsApproved
            PrdctType.ApprovedBy = UserID
            PrdctType.ApprovedOn = Date.Now
            PrdctType.Save()

            CurrentAt = "Collection Product Type : [Code:  " & PrdctType.ProductTypeCode.ToString() & " ; Name:  " & PrdctType.ProductTypeName.ToString() & " ; Collection Mode: " & PrdctType.DisbursementMode.ToString() & " ; IsActive:  " & PrdctType.IsActive.ToString() & " ; IsApproved:  " & PrdctType.IsApproved.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Collection Product Type", PrdctType.ProductTypeCode.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")

        End Sub

        Public Shared Sub GetCollectionProductTypesgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim collProductType As New ICCollectionProductTypeCollection

            If Not pagenumber = 0 Then
                collProductType.Query.Select(collProductType.Query.ProductTypeCode, collProductType.Query.ProductTypeName, collProductType.Query.DisbursementMode, collProductType.Query.IsActive, collProductType.Query.IsApproved)
                collProductType.Query.OrderBy(collProductType.Query.ProductTypeName.Ascending)
                collProductType.Query.Load()
                rg.DataSource = collProductType

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else

                collProductType.Query.es.PageNumber = 1
                collProductType.Query.es.PageSize = pagesize
                collProductType.Query.OrderBy(collProductType.Query.ProductTypeName.Ascending)
                collProductType.Query.Load()
                rg.DataSource = collProductType


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub

        Public Shared Function GetAllCollectionProductTypesActiveAndApprove() As ICCollectionProductTypeCollection
            Dim collProductType As New ICCollectionProductTypeCollection
            collProductType.es.Connection.CommandTimeout = 3600
            collProductType.Query.Where(collProductType.Query.IsActive = True And collProductType.Query.IsApproved = True)
            collProductType.Query.OrderBy(collProductType.Query.ProductTypeName.Ascending)
            collProductType.Query.Load()
            Return collProductType
        End Function


    End Class
End Namespace