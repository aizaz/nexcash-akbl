﻿Namespace IC
    Public Class ICHolidayManagementController
        Public Shared Function SaveHoliday(ByVal ICHoliday As ICHolidayManagement, ByVal IsUpDate As Boolean) As String
            Dim cICHoliday As New ICHolidayManagement
            cICHoliday.es.Connection.CommandTimeout = 3600

            If IsUpDate = False Then

                cICHoliday.CreatedBy = ICHoliday.CreatedBy
                cICHoliday.CreateDate = ICHoliday.CreateDate
            Else
                cICHoliday.LoadByPrimaryKey(ICHoliday.HolidayID)
            End If
            cICHoliday.HolidayName = ICHoliday.HolidayName
            cICHoliday.HolidayDate = ICHoliday.HolidayDate
            cICHoliday.HolidayDescription = ICHoliday.HolidayDescription
            cICHoliday.Save()

            Return cICHoliday.HolidayID
        End Function
        Public Shared Sub DeleteHoliday(ByVal HolidayID As String)
            Dim cICHoliday As New ICHolidayManagement
            cICHoliday.es.Connection.CommandTimeout = 3600
            If cICHoliday.LoadByPrimaryKey(HolidayID) Then
                cICHoliday.MarkAsDeleted()
                cICHoliday.Save()
            End If
        End Sub
        Public Shared Function GetAllHolidays() As ICHolidayManagementCollection
            Dim cICHoliday As New ICHolidayManagementCollection
            cICHoliday.es.Connection.CommandTimeout = 3600
            cICHoliday.Query.OrderBy(cICHoliday.Query.HolidayDate.Ascending)
            cICHoliday.Query.Load()
            Return cICHoliday
        End Function
        Public Shared Function CheckDuplicateHoliday(ByVal cHoliday As ICHolidayManagement) As Boolean
            Dim cICHoliday As New ICHolidayManagement
            cICHoliday.es.Connection.CommandTimeout = 3600

            cICHoliday.Query.Where(cICHoliday.Query.HolidayName = cHoliday.HolidayName And cICHoliday.Query.HolidayDate = cHoliday.HolidayDate)


            If cICHoliday.Query.Load() Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

