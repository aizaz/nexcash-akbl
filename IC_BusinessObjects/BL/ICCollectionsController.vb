﻿Imports EntitySpaces.Core
Imports Telerik.Web.UI
Namespace IC
    Public Class ICCollectionsController
        Public Shared Function AddICCollections(ByVal objICCollections As ICCollections, ByVal IsUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As Integer
            Dim objICCollectionsForSave As New ICCollections
            Dim CollectionID As Integer = 0
            Dim ActionString As String = Nothing

            If IsUpdate = True Then
                objICCollectionsForSave.LoadByPrimaryKey(objICCollections.CollectionID.ToString)
            End If
            objICCollectionsForSave.PayersName = objICCollections.PayersName
            objICCollectionsForSave.CollectionDate = objICCollections.CollectionDate
            objICCollectionsForSave.PayersAccountNo = objICCollections.PayersAccountNo
            objICCollectionsForSave.PayersAccountBranchCode = objICCollections.PayersAccountBranchCode
            objICCollectionsForSave.PayersAccountCurrency = objICCollections.PayersAccountCurrency
            objICCollectionsForSave.PayersAccountType = objICCollections.PayersAccountType
            objICCollectionsForSave.CollectionAccountNo = objICCollections.CollectionAccountNo
            objICCollectionsForSave.CollectionAccountBranchCode = objICCollections.CollectionAccountBranchCode
            objICCollectionsForSave.CollectionAccountCurrency = objICCollections.CollectionAccountCurrency
            objICCollectionsForSave.CollectionAccountType = objICCollections.CollectionAccountType
            objICCollectionsForSave.Status = objICCollections.Status
            objICCollectionsForSave.LastStatus = objICCollections.LastStatus
            objICCollectionsForSave.CollectionAmount = objICCollections.CollectionAmount
            objICCollectionsForSave.CollectionRemarks = objICCollections.CollectionRemarks
            objICCollectionsForSave.CollectionLocationCode = objICCollections.CollectionLocationCode
            objICCollectionsForSave.CollectionCreatedBy = objICCollections.CollectionCreatedBy
            objICCollectionsForSave.CollectionMode = objICCollections.CollectionMode
            objICCollectionsForSave.CollectionClientCode = objICCollections.CollectionClientCode
            objICCollectionsForSave.CollectionNatureCode = objICCollections.CollectionNatureCode
            objICCollectionsForSave.CollectionDirectPayCreatedBy = objICCollections.CollectionDirectPayCreatedBy
            objICCollectionsForSave.CollectionDirectPayCreatedDate = objICCollections.CollectionDirectPayCreatedDate
            objICCollectionsForSave.ReferenceField1 = objICCollections.ReferenceField1
            objICCollectionsForSave.ReferenceField2 = objICCollections.ReferenceField2
            objICCollectionsForSave.ReferenceField3 = objICCollections.ReferenceField3
            objICCollectionsForSave.ReferenceField4 = objICCollections.ReferenceField4
            objICCollectionsForSave.ReferenceField5 = objICCollections.ReferenceField5
            objICCollectionsForSave.DueDate = objICCollections.DueDate
            objICCollectionsForSave.AmountAfterDueDate = objICCollections.AmountAfterDueDate
            objICCollectionsForSave.CollectionDirectPayApprovedBy = objICCollections.CollectionDirectPayApprovedBy
            objICCollectionsForSave.CollectionDirectPayApproveDateTime = objICCollections.CollectionDirectPayApproveDateTime
            objICCollectionsForSave.FileBatchNo = objICCollections.FileBatchNo
            objICCollectionsForSave.FileID = objICCollections.FileID
            objICCollectionsForSave.ReturnReason = objICCollections.ReturnReason
            objICCollectionsForSave.SkipReason = objICCollections.SkipReason
            objICCollectionsForSave.BankCode = objICCollections.BankCode
            objICCollectionsForSave.ClearingBranchCode = objICCollections.ClearingBranchCode
            objICCollectionsForSave.CollectionTransferedDate = objICCollections.CollectionTransferedDate
            objICCollectionsForSave.InstrumentNo = objICCollections.InstrumentNo
            objICCollectionsForSave.CollectionClearingType = objICCollections.CollectionClearingType
            objICCollectionsForSave.ClearingProcess = objICCollections.ClearingProcess
            objICCollectionsForSave.ClearingCBAccountType = objICCollections.ClearingCBAccountType
            objICCollectionsForSave.ClearingAccountNo = objICCollections.ClearingAccountNo
            objICCollectionsForSave.ClearingAccountBranchCode = objICCollections.ClearingAccountBranchCode
            objICCollectionsForSave.ClearingAccountCurrency = objICCollections.ClearingAccountCurrency
            objICCollectionsForSave.ClearingValueDate = objICCollections.ClearingValueDate
            objICCollectionsForSave.CollectionProductTypeCode = objICCollections.CollectionProductTypeCode
            objICCollectionsForSave.ClearingAccountBranchCode = objICCollections.ClearingAccountBranchCode
            objICCollectionsForSave.PaymentNatureCode = objICCollections.PaymentNatureCode
            objICCollectionsForSave.CreatedOfficeCode = objICCollections.CreatedOfficeCode
            objICCollectionsForSave.DirectPayRejectedBy = objICCollections.DirectPayRejectedBy
            objICCollectionsForSave.DirectPayRejectedDateTime = objICCollections.DirectPayRejectedDateTime

            objICCollectionsForSave.RoutingNo = objICCollections.RoutingNo
            objICCollectionsForSave.DrawnOnBranchCode = objICCollections.DrawnOnBranchCode
            objICCollectionsForSave.IsPrintedNextCash = objICCollections.IsPrintedNextCash
            objICCollectionsForSave.InstrumentDate = objICCollections.InstrumentDate


            objICCollectionsForSave.Save()
            CollectionID = objICCollectionsForSave.CollectionID
            ActionString = "Collection with ID [ " & CollectionID & " ] Amount [ " & objICCollectionsForSave.CollectionAmount & " ], Due Date [ " & objICCollectionsForSave.DueDate & " ] added."
            ActionString += " Action was taken by user [ " & UserID & " ] [ " & UserName & " ]."
            If IsUpdate = False Then
                ICUtilities.AddAuditTrail(ActionString, "Collection", CollectionID, UserID, UserName, "ADD")
            Else
                ICUtilities.AddAuditTrail(ActionString, "Collection", CollectionID, UserID, UserName, "UPDATE")
            End If
            Return CollectionID
        End Function
        Public Shared Function UpdateICCollectionsStatus(ByVal CollectionID As String, ByVal FromStatus As String, ByVal ToStatus As String, ByVal UserID As String, ByVal UserName As String) As Boolean

            Dim ActionStr As String = Nothing
            Dim objICCollections As New ICCollections

            objICCollections.LoadByPrimaryKey(CollectionID)
            objICCollections.Status = ToStatus
            objICCollections.LastStatus = FromStatus
            objICCollections.Save()
            ActionStr = "Collection with ID [ " & CollectionID & " ] status update from [ " & objICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName & " ] to "
            ActionStr += "status [ " & objICCollections.UpToICCollectionsStatusByLastStatus.CollectionStatusName & " ]. Action was taken by user [ " & UserID & " ][ " & UserName & " ]"
            ICUtilities.AddAuditTrail(ActionStr, "Collection", CollectionID, UserID, UserName, "UPDATE")
            Return True
        End Function
        Public Shared Function GetCollDetailsByReferenceFields(ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String) As DataTable
            Dim qryobjICCollections As New ICCollectionsQuery("qryobjICCollections")
            Dim qryobjICOffice As New ICOfficeQuery("qryobjICOffice")
            Dim dt As New DataTable

            qryobjICCollections.Select(qryobjICCollections.CollectionDate, qryobjICOffice.OfficeName, qryobjICCollections.CollectionAmount, qryobjICCollections.CollectionMode)
            qryobjICCollections.LeftJoin(qryobjICOffice).On(qryobjICCollections.CreatedOfficeCode = qryobjICOffice.OfficeID)
            If Ref1 IsNot Nothing Then
                qryobjICCollections.Where(qryobjICCollections.ReferenceField1 = Ref1)
            End If
            If Ref2 IsNot Nothing Then
                qryobjICCollections.Where(qryobjICCollections.ReferenceField2 = Ref2)
            End If
            If Ref3 IsNot Nothing Then
                qryobjICCollections.Where(qryobjICCollections.ReferenceField3 = Ref3)
            End If
            If Ref4 IsNot Nothing Then
                qryobjICCollections.Where(qryobjICCollections.ReferenceField4 = Ref4)
            End If
            If Ref5 IsNot Nothing Then
                qryobjICCollections.Where(qryobjICCollections.ReferenceField5 = Ref5)
            End If
            If qryobjICCollections.Load Then
                qryobjICCollections.OrderBy(qryobjICCollections.CollectionID.Ascending)

                dt = qryobjICCollections.LoadDataTable
            End If
            Return dt
        End Function
        Public Shared Sub GetAllCollectionForRadGridForAuthorizationDirectPay(ByVal AccountNumber As DataTable, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Status As String)
            Dim dt As DataTable
            Dim StrQuery As String = Nothing
            Dim WhereClasue As String = Nothing

            WhereClasue = " Where "
            StrQuery = "Select CollectionID,convert(date,CollectionDate) as CollectionDate,convert(date,DueDate) as DueDate,CollectionAmount,CollectionStatusName, "
            StrQuery += "isnull(PayersName,'-') as PayersName,AmountAfterDueDate,CollectionMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
            StrQuery += "CollectionAccountNo,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
            StrQuery += "IC_Bank.BankName,"
            StrQuery += "CONVERT(date,CollectionDirectPayCreatedDate) as CollectionDirectPayCreatedDate, "
            StrQuery += "isnull(PayersAccountNo,'-') as PayersAccountNo,"
            StrQuery += "CollectionProductTypeCode,"
            StrQuery += "DPCreator.UserName,ReferenceField1,ReferenceField2,ReferenceField3 "
            StrQuery += "from IC_Collections "
            StrQuery += "left join IC_Office on IC_Collections.CreatedOfficeCode=IC_Office.OfficeID "
            StrQuery += "left join IC_Bank on IC_Collections.BankCode=IC_Bank.BankCode "
            StrQuery += "left join IC_CollectionsStatus on IC_Collections.Status=IC_CollectionsStatus.CollectionStatusID "
            StrQuery += "left join IC_User as DPCreator on IC_Collections.CollectionDirectPayCreatedBy=DPCreator.userid "
            If AccountNumber.Rows.Count > 0 Then
                WhereClasue += " ( "
                WhereClasue += "PayersAccountNo +'" & "-" & "'+ PayersAccountBranchCode + '" & "-" & "'+ PayersAccountCurrency + '" & "-" & "' + PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                For Each dr As DataRow In AccountNumber.Rows

                    WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

                Next
                WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
                WhereClasue += ")"
                WhereClasue += " )"

            Else
                WhereClasue += " OR (IC_Collections.CollectionDirectPayCreatedBy=" & UsersID & " )"
            End If
            WhereClasue += " AND Status=" & Status

            If WhereClasue.Contains(" Where  AND ") = True Then
                WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
            End If
            StrQuery += WhereClasue
            StrQuery += " Order By CollectionID Desc"
            Dim utill As New esUtility()
            dt = New DataTable
            dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
            If Not CurrentPage = 0 Then

                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub
        Public Shared Sub UpdateICCollections(ByVal objICCollections As ICCollections, ByVal UserID As String, ByVal UserName As String)
            Dim objICCollectionsForSave As New ICCollections
            Dim CollectionID As Integer = 0
            Dim ActionString As String = Nothing

            objICCollectionsForSave.LoadByPrimaryKey(objICCollections.CollectionID.ToString)

            objICCollectionsForSave.PayersName = objICCollections.PayersName
            objICCollectionsForSave.CollectionDate = objICCollections.CollectionDate
            objICCollectionsForSave.PayersAccountNo = objICCollections.PayersAccountNo
            objICCollectionsForSave.PayersAccountBranchCode = objICCollections.PayersAccountBranchCode
            objICCollectionsForSave.PayersAccountCurrency = objICCollections.PayersAccountCurrency
            objICCollectionsForSave.PayersAccountType = objICCollections.PayersAccountType
            objICCollectionsForSave.CollectionAccountNo = objICCollections.CollectionAccountNo
            objICCollectionsForSave.CollectionAccountBranchCode = objICCollections.CollectionAccountBranchCode
            objICCollectionsForSave.CollectionAccountCurrency = objICCollections.CollectionAccountCurrency
            objICCollectionsForSave.CollectionAccountType = objICCollections.CollectionAccountType
            objICCollectionsForSave.Status = objICCollections.Status
            objICCollectionsForSave.LastStatus = objICCollections.LastStatus
            objICCollectionsForSave.CollectionAmount = objICCollections.CollectionAmount
            objICCollectionsForSave.CollectionRemarks = objICCollections.CollectionRemarks
            objICCollectionsForSave.CollectionLocationCode = objICCollections.CollectionLocationCode
            objICCollectionsForSave.CollectionCreatedBy = objICCollections.CollectionCreatedBy
            objICCollectionsForSave.CollectionMode = objICCollections.CollectionMode
            objICCollectionsForSave.CollectionClientCode = objICCollections.CollectionClientCode
            objICCollectionsForSave.CollectionNatureCode = objICCollections.CollectionNatureCode
            objICCollectionsForSave.CollectionDirectPayCreatedBy = objICCollections.CollectionDirectPayCreatedBy
            objICCollectionsForSave.CollectionDirectPayCreatedDate = objICCollections.CollectionDirectPayCreatedDate
            objICCollectionsForSave.ReferenceField1 = objICCollections.ReferenceField1
            objICCollectionsForSave.ReferenceField2 = objICCollections.ReferenceField2
            objICCollectionsForSave.ReferenceField3 = objICCollections.ReferenceField3
            objICCollectionsForSave.ReferenceField4 = objICCollections.ReferenceField4
            objICCollectionsForSave.ReferenceField5 = objICCollections.ReferenceField5
            objICCollectionsForSave.DueDate = objICCollections.DueDate
            objICCollectionsForSave.AmountAfterDueDate = objICCollections.AmountAfterDueDate
            objICCollectionsForSave.CollectionDirectPayApprovedBy = objICCollections.CollectionDirectPayApprovedBy
            objICCollectionsForSave.CollectionDirectPayApproveDateTime = objICCollections.CollectionDirectPayApproveDateTime
            objICCollectionsForSave.FileBatchNo = objICCollections.FileBatchNo
            objICCollectionsForSave.FileID = objICCollections.FileID
            objICCollectionsForSave.ReturnReason = objICCollections.ReturnReason
            objICCollectionsForSave.SkipReason = objICCollections.SkipReason
            objICCollectionsForSave.BankCode = objICCollections.BankCode
            objICCollectionsForSave.ClearingBranchCode = objICCollections.ClearingBranchCode
            objICCollectionsForSave.CollectionTransferedDate = objICCollections.CollectionTransferedDate
            objICCollectionsForSave.InstrumentNo = objICCollections.InstrumentNo
            objICCollectionsForSave.CollectionClearingType = objICCollections.CollectionClearingType
            objICCollectionsForSave.ClearingProcess = objICCollections.ClearingProcess
            objICCollectionsForSave.ClearingCBAccountType = objICCollections.ClearingCBAccountType
            objICCollectionsForSave.ClearingAccountNo = objICCollections.ClearingAccountNo
            objICCollectionsForSave.ClearingAccountBranchCode = objICCollections.ClearingAccountBranchCode
            objICCollectionsForSave.ClearingAccountCurrency = objICCollections.ClearingAccountCurrency
            objICCollectionsForSave.ClearingValueDate = objICCollections.ClearingValueDate
            objICCollectionsForSave.CollectionProductTypeCode = objICCollections.CollectionProductTypeCode
            objICCollectionsForSave.ClearingAccountBranchCode = objICCollections.ClearingAccountBranchCode
            objICCollectionsForSave.PaymentNatureCode = objICCollections.PaymentNatureCode
            objICCollectionsForSave.CreatedOfficeCode = objICCollections.CreatedOfficeCode
            objICCollectionsForSave.DirectPayRejectedBy = objICCollections.DirectPayRejectedBy
            objICCollectionsForSave.DirectPayRejectedDateTime = objICCollections.DirectPayRejectedDateTime
            objICCollectionsForSave.RoutingNo = objICCollections.RoutingNo
            objICCollectionsForSave.DrawnOnBranchCode = objICCollections.DrawnOnBranchCode
            objICCollectionsForSave.IsPrintedNextCash = objICCollections.IsPrintedNextCash
            objICCollectionsForSave.InstrumentDate = objICCollections.InstrumentDate
            objICCollectionsForSave.Save()
            CollectionID = objICCollectionsForSave.CollectionID
            ActionString = "Collection with ID [ " & CollectionID & " ] Amount [ " & objICCollectionsForSave.CollectionAmount & " ], Due Date [ " & objICCollectionsForSave.DueDate & " ] updated."
            ActionString += " Action was taken by user [ " & UserID & " ] [ " & UserName & " ]."

            ICUtilities.AddAuditTrail(ActionString, "Collection", CollectionID, UserID, UserName, "UPDATE")


        End Sub
        Public Shared Function VerifyEnteredAmount(ByVal objICCollectionNature As ICCollectionNature, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String, ByVal Amount As Double, ByVal RowCount As Integer) As String
            Dim ResponseMessage As String = "OK"
            Dim dtAmountAndDueDate As New DataTable
            Try
                dtAmountAndDueDate = ICCollectionSQLController.GetAmountAndDueDateDataByCollectionNature(objICCollectionNature, Ref1, Ref2, Ref3, Ref4, Ref5)
                If dtAmountAndDueDate.Rows.Count > 0 Then
                    If CDate(dtAmountAndDueDate.Rows(0)("DueDate")) > Date.Now.Date Then
                        If objICCollectionNature.IsPartialCollectionAllowed = True Then
                            If objICCollectionNature.AllowedPartialCollectionType = "Over" Then
                                If Not CDbl(dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate")) < Amount Then
                                    ResponseMessage = Nothing
                                    ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be greater than " & dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate").ToString("N2") & " <br />"
                                End If
                            ElseIf objICCollectionNature.AllowedPartialCollectionType = "Under" Then
                                If Not CDbl(dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate")) > Amount Then
                                    ResponseMessage = Nothing
                                    ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be less than " & dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate") & " <br />"
                                End If
                            End If
                        Else
                            If CDbl(dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate")) <> Amount Then
                                ResponseMessage = Nothing
                                ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be equal to " & dtAmountAndDueDate.Rows(0)("AmountBeforeDueDate") & " <br />"
                            End If
                        End If
                    Else
                        If objICCollectionNature.IsPartialCollectionAllowed = True Then
                            If objICCollectionNature.AllowedPartialCollectionType = "Over" Then
                                If Not CDbl(dtAmountAndDueDate.Rows(0)("AmountAfterDueDate")) < Amount Then
                                    ResponseMessage = Nothing
                                    ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be greater than " & dtAmountAndDueDate.Rows(0)("AmountAfterDueDate") & " <br />"
                                End If
                            ElseIf objICCollectionNature.AllowedPartialCollectionType = "Under" Then
                                If Not CDbl(dtAmountAndDueDate.Rows(0)("AmountAfterDueDate")) > Amount Then
                                    ResponseMessage = Nothing
                                    ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be less than " & dtAmountAndDueDate.Rows(0)("AmountAfterDueDate") & " <br />"
                                End If
                            End If
                        Else
                            If CDbl(dtAmountAndDueDate.Rows(0)("AmountAfterDueDate")) <> Amount Then
                                ResponseMessage = Nothing
                                ResponseMessage += "Invalid amount at row no. " & RowCount & ".Amount must be equal to " & dtAmountAndDueDate.Rows(0)("AmountAfterDueDate") & " <br />"
                            End If
                        End If
                    End If
                Else
                    ResponseMessage += "Unable to find due date, amount and amount after due date <br />"
                End If
            Catch ex As Exception
                ResponseMessage += ex.Message.ToString & "<br />"
            End Try
            Return ResponseMessage
        End Function
        Public Shared Function ReceiveCollections(ByVal ProductTypeCode As String, ByVal UserID As String, ByVal ObjICCollections As ICCollections, ByVal EntityType As String, ByVal IsInvoiceUpdate As Boolean, ByVal FromStatus As String) As String
            Dim objICCollProductType As New ICCollectionProductType
            Dim objICUser As New ICUser
            Dim TranStat As CBUtilities.TransactionStatus
            Dim FromAccount, Toaccount As CBUtilities.AccountType
            Dim RelatedID As String = Nothing
            Dim ReqSqlString As String = Nothing
            Dim dt As New DataTable
            objICCollProductType.LoadByPrimaryKey(ProductTypeCode)
            objICUser.LoadByPrimaryKey(UserID)
            Select Case objICCollProductType.DisbursementMode.ToString
                Case "Cash"
                    Try
                        RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                        If ObjICCollections.CollectionAccountType = "RB" Then
                            Toaccount = CBUtilities.AccountType.RB
                        Else
                            Toaccount = CBUtilities.AccountType.GL
                        End If
                        If ObjICCollections.ClearingCBAccountType = "RB" Then
                            FromAccount = CBUtilities.AccountType.RB
                        Else
                            FromAccount = CBUtilities.AccountType.GL
                        End If
                        TranStat = CBUtilities.OpenEndedFundsTransferCollection(ObjICCollections.ClearingAccountNo, ObjICCollections.ClearingAccountBranchCode, ObjICCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, FromAccount, ObjICCollections.CollectionAccountNo, ObjICCollections.CollectionAccountBranchCode, ObjICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, Toaccount, ObjICCollections.CollectionAmount, EntityType, RelatedID, objICCollProductType.OriginatingTransactionType, objICCollProductType.RespondingTransactionType, objICCollProductType.VoucherType, objICCollProductType.InstrumentType, objICCollProductType.ReversalTransactionType)
                        If TranStat = CBUtilities.TransactionStatus.Successfull Then
                            ObjICCollections.CollectionTransferedDate = Date.Now
                            ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            If IsInvoiceUpdate = True Then
                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            Else
                                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                End If
                                ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                            End If
                            Return "OK"
                        Else
                            Throw New Exception(CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat))
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                Case "Fund Transfer"
                    Try
                        RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                        If ObjICCollections.CollectionAccountType = "RB" Then
                            Toaccount = CBUtilities.AccountType.RB
                        Else
                            Toaccount = CBUtilities.AccountType.GL
                        End If
                        If ObjICCollections.PayersAccountType = "RB" Then
                            FromAccount = CBUtilities.AccountType.RB
                        Else
                            FromAccount = CBUtilities.AccountType.GL
                        End If
                        TranStat = CBUtilities.OpenEndedFundsTransferCollection(ObjICCollections.PayersAccountNo, ObjICCollections.PayersAccountBranchCode, ObjICCollections.PayersAccountCurrency, Nothing, Nothing, Nothing, FromAccount, ObjICCollections.CollectionAccountNo, ObjICCollections.CollectionAccountBranchCode, ObjICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, Toaccount, ObjICCollections.CollectionAmount, EntityType, RelatedID, objICCollProductType.OriginatingTransactionType, objICCollProductType.RespondingTransactionType, objICCollProductType.VoucherType, objICCollProductType.InstrumentType, objICCollProductType.ReversalTransactionType)
                        If TranStat = CBUtilities.TransactionStatus.Successfull Then
                            ObjICCollections.CollectionTransferedDate = Date.Now
                            ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            If IsInvoiceUpdate = True Then
                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            Else
                                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                End If
                                ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                            End If

                            Return "OK"
                        Else
                            Throw New Exception(CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat))
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                Case "Cheque"
                    If objICCollProductType.ClearingType = "Funds Transfer" Then
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                            If ObjICCollections.CollectionAccountType = "RB" Then
                                Toaccount = CBUtilities.AccountType.RB
                            Else
                                Toaccount = CBUtilities.AccountType.GL
                            End If
                            If ObjICCollections.PayersAccountType = "RB" Then
                                FromAccount = CBUtilities.AccountType.RB
                            Else
                                FromAccount = CBUtilities.AccountType.GL
                            End If
                            TranStat = CBUtilities.OpenEndedFundsTransferCollection(ObjICCollections.PayersAccountNo, ObjICCollections.PayersAccountBranchCode, ObjICCollections.PayersAccountCurrency, Nothing, Nothing, Nothing, FromAccount, ObjICCollections.CollectionAccountNo, ObjICCollections.CollectionAccountBranchCode, ObjICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, Toaccount, ObjICCollections.CollectionAmount, EntityType, RelatedID, objICCollProductType.OriginatingTransactionType, objICCollProductType.RespondingTransactionType, objICCollProductType.VoucherType, objICCollProductType.InstrumentType, objICCollProductType.ReversalTransactionType)
                            If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                ObjICCollections.CollectionTransferedDate = Date.Now
                                ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                If IsInvoiceUpdate = True Then
                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    Else
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    End If
                                    ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                                End If

                                Return "OK"
                            Else
                                Throw New Exception(CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat))
                            End If
                        Catch ex As Exception
                            Throw ex
                        End Try
                    Else
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                                ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                If IsInvoiceUpdate = True Then
                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    Else
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                End If
                                ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                                End If

                                Return "OK"
                           
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                Case "Pay Order"
                    If objICCollProductType.ClearingType = "Funds Transfer" Then
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                            If ObjICCollections.CollectionAccountType = "RB" Then
                                Toaccount = CBUtilities.AccountType.RB
                            Else
                                Toaccount = CBUtilities.AccountType.GL
                            End If
                            If ObjICCollections.ClearingCBAccountType = "RB" Then
                                FromAccount = CBUtilities.AccountType.RB
                            Else
                                FromAccount = CBUtilities.AccountType.GL
                            End If
                            TranStat = CBUtilities.OpenEndedFundsTransferCollection(ObjICCollections.ClearingAccountNo, ObjICCollections.ClearingAccountBranchCode, ObjICCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, FromAccount, ObjICCollections.CollectionAccountNo, ObjICCollections.CollectionAccountBranchCode, ObjICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, Toaccount, ObjICCollections.CollectionAmount, EntityType, RelatedID, objICCollProductType.OriginatingTransactionType, objICCollProductType.RespondingTransactionType, objICCollProductType.VoucherType, objICCollProductType.InstrumentType, objICCollProductType.ReversalTransactionType)
                            If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                ObjICCollections.CollectionTransferedDate = Date.Now
                                ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                If IsInvoiceUpdate = True Then
                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    Else
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    End If
                                    ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                                End If

                                Return "OK"
                            Else
                                Throw New Exception(CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat))
                            End If
                        Catch ex As Exception
                            Throw ex
                        End Try
                    Else
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                            ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            If IsInvoiceUpdate = True Then
                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            Else
                                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                End If
                                ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                            End If

                            Return "OK"

                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                Case "Demand Draft"
                    If objICCollProductType.ClearingType = "Funds Transfer" Then
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                            If ObjICCollections.CollectionAccountType = "RB" Then
                                Toaccount = CBUtilities.AccountType.RB
                            Else
                                Toaccount = CBUtilities.AccountType.GL
                            End If
                            If ObjICCollections.ClearingCBAccountType = "RB" Then
                                FromAccount = CBUtilities.AccountType.RB
                            Else
                                FromAccount = CBUtilities.AccountType.GL
                            End If
                            TranStat = CBUtilities.OpenEndedFundsTransferCollection(ObjICCollections.ClearingAccountNo, ObjICCollections.ClearingAccountBranchCode, ObjICCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, FromAccount, ObjICCollections.CollectionAccountNo, ObjICCollections.CollectionAccountBranchCode, ObjICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, Toaccount, ObjICCollections.CollectionAmount, EntityType, RelatedID, objICCollProductType.OriginatingTransactionType, objICCollProductType.RespondingTransactionType, objICCollProductType.VoucherType, objICCollProductType.InstrumentType, objICCollProductType.ReversalTransactionType)
                            If TranStat = CBUtilities.TransactionStatus.Successfull Then
                                ObjICCollections.CollectionTransferedDate = Date.Now
                                ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                If IsInvoiceUpdate = True Then
                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    Else
                                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                    End If
                                    ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                                End If

                                Return "OK"
                            Else
                                Throw New Exception(CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat))
                            End If
                        Catch ex As Exception
                            Throw ex
                        End Try
                    Else
                        Try
                            RelatedID = ObjICCollections.ReferenceField1 & "-" & ObjICCollections.ReferenceField2 & "-" & ObjICCollections.ReferenceField3 & "-" & ObjICCollections.ReferenceField4 & "-" & ObjICCollections.ReferenceField5
                            ICCollectionsController.AddICCollections(ObjICCollections, False, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            If IsInvoiceUpdate = True Then
                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, FromStatus, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, objICUser.UserID.ToString, objICUser.UserName.ToString)
                            Else
                                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Select", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(ReqSqlString) = False Then
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Insert", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                Else
                                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(ObjICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlString, objICUser.UserID.ToString, ObjICCollections.FileID, "Update", "", ObjICCollections.ReferenceField1, ObjICCollections.ReferenceField2, ObjICCollections.ReferenceField3, ObjICCollections.ReferenceField4, ObjICCollections.ReferenceField5, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, ObjICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                                End If
                                ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(ReqSqlString)
                            End If

                            Return "OK"

                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
            End Select
        End Function
        Public Shared Function GetValueDate(ByVal ValueDate As Date) As Date
            If ICHolidayManagementController.IsHoliday(ValueDate) = True Then
                GetValueDate(ValueDate.Date.AddDays(1).Date)
            End If
            ICWorkingDaysController.GetWorkingDayForValueDate(ValueDate)
            Return ValueDate.Date
        End Function
        Public Shared Function GetCollectionsForValueDateByBankCodeAndInstrumentNo(ByVal BankCode As String, ByVal InstrumentNo As String) As DataTable
            Dim qryObjICCollection As New ICCollectionsQuery("qryObjICCollection")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICStatus As New ICCollectionsStatusQuery("qryObjICStatus")
            Dim qryObjICCollProdutc As New ICCollectionProductTypeQuery("qryObjICCollProdutc")
            Dim qryObjICCollectionNature As New ICCollectionNatureQuery("qryObjICCollectionNature")
            qryObjICCollection.Select(qryObjICCollection.CollectionAmount, qryObjICCollection.ClearingValueDate, qryObjICBank.BankName, qryObjICStatus.CollectionStatusName, qryObjICCollection.InstrumentNo)
            qryObjICCollection.Select(qryObjICCollectionNature.CollectionNatureName, qryObjICCollProdutc.ProductTypeName, qryObjICCollProdutc.ClearingType, qryObjICCollection.CollectionID)
            qryObjICCollection.Select(qryObjICCollection.BankCode)
            qryObjICCollection.LeftJoin(qryObjICBank).On(qryObjICCollection.BankCode = qryObjICBank.BankCode)
            qryObjICCollection.LeftJoin(qryObjICStatus).On(qryObjICCollection.Status = qryObjICStatus.CollectionStatusID)
            qryObjICCollection.LeftJoin(qryObjICCollProdutc).On(qryObjICCollection.CollectionProductTypeCode = qryObjICCollProdutc.ProductTypeCode)
            qryObjICCollection.LeftJoin(qryObjICCollectionNature).On(qryObjICCollection.CollectionNatureCode = qryObjICCollectionNature.CollectionNatureCode)
            qryObjICCollection.Where(qryObjICCollection.BankCode = BankCode And qryObjICCollection.InstrumentNo = InstrumentNo)
            qryObjICCollection.OrderBy(qryObjICCollection.CollectionID.Ascending)
            qryObjICCollection.es.Top = 1
            Return qryObjICCollection.LoadDataTable
        End Function
        Public Shared Function ModifyValueDateByBankCodeAndInstrumentNo(ByVal BankCode As String, ByVal InstrumentNo As String, ByVal ValueDate As Date, ByVal UserID As String, ByVal UserName As String) As Boolean
            Dim ObjICCollections As New ICCollections
            Dim collObjICCollections As New ICCollectionsCollection
            Dim ActionString As String = Nothing
            Dim Result As Boolean = False
            collObjICCollections.Query.Where(collObjICCollections.Query.BankCode = BankCode And collObjICCollections.Query.InstrumentNo = InstrumentNo)
            collObjICCollections.Query.OrderBy(collObjICCollections.Query.CollectionID.Ascending)
            If collObjICCollections.Query.Load Then
                ObjICCollections = collObjICCollections(0)
                ActionString = "Value Date for Collection with ID [ " & ObjICCollections.CollectionID & " ] is modified from [ " & ObjICCollections.ClearingValueDate & " ] to"
                ActionString += " [ " & ValueDate & " ] . Action was taken by user [ " & UserID & " ] [ " & UserName & " ]"
                ObjICCollections.ClearingValueDate = ValueDate
                ObjICCollections.Save()
                ICUtilities.AddAuditTrail(ActionString, "Collection", ObjICCollections.CollectionID.ToString, UserID, UserName, "UPDATE")
                Result = True
            End If

            Return Result
        End Function
    End Class
End Namespace
