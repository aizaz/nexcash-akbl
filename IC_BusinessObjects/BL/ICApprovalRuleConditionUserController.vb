﻿Namespace IC
    Public Class ICApprovalRuleConditionUserController
        Public Shared Function AddApprovalRuleCondtionsUser(ByVal objICAPPRuleCondUser As ICApprovalRuleConditionUsers, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String) As Integer

            Dim objICApprovalCondUserSave As New ICApprovalRuleConditionUsers



            If IsUpdate = False Then

                objICApprovalCondUserSave.CreatedBy = objICAPPRuleCondUser.CreatedBy
                objICApprovalCondUserSave.CreatedDate = objICAPPRuleCondUser.CreatedDate
            Else
                objICApprovalCondUserSave.LoadByPrimaryKey(objICAPPRuleCondUser.ApprovalRuleConditionUserID)
                objICApprovalCondUserSave.CreatedBy = objICAPPRuleCondUser.CreatedBy
                objICApprovalCondUserSave.CreatedDate = objICAPPRuleCondUser.CreatedDate
            End If
            objICApprovalCondUserSave.UserID = objICAPPRuleCondUser.UserID
            objICApprovalCondUserSave.UserCount = objICAPPRuleCondUser.UserCount
            objICApprovalCondUserSave.ApprovaRuleCondID = objICAPPRuleCondUser.ApprovaRuleCondID
            objICApprovalCondUserSave.Save()

            If IsUpdate = False Then
                ActionString = "Approval Rule Condition User [ " & objICApprovalCondUserSave.UserID & " ], for condition [ " & objICApprovalCondUserSave.ApprovaRuleCondID & " ]"
                ActionString += " user count [ " & objICApprovalCondUserSave.UserCount & " ] "
                ActionString += " added."
                ICUtilities.AddAuditTrail(ActionString, "Approval Rule Condition User", objICApprovalCondUserSave.ApprovalRuleConditionUserID, UsersID.ToString, UsersName.ToString, "ADD")
            ElseIf IsUpdate = True Then
                ActionString = "Approval Rule Condition User [ " & objICApprovalCondUserSave.UserID & " ], for condition [ " & objICApprovalCondUserSave.ApprovaRuleCondID & " ]"
                ActionString += " user count [ " & objICApprovalCondUserSave.UserCount & " ] "
                ActionString += " updated."
                ICUtilities.AddAuditTrail(ActionString.ToString, "Approval Rule Condition User", objICApprovalCondUserSave.ApprovalRuleConditionUserID, UsersID.ToString, UsersName.ToString, "UPDATE")
            End If


            Return objICApprovalCondUserSave.ApprovalRuleConditionUserID


        End Function



        Public Shared Function GetApprovalRuleConditionsByApprovalRuleID(ByVal ApprovalRuleCondID As String) As ICApprovalRuleConditionUsersCollection
            Dim collObjICAPPRuleCondUser As New ICApprovalRuleConditionUsersCollection



            collObjICAPPRuleCondUser.Query.Where(collObjICAPPRuleCondUser.Query.ApprovaRuleCondID = ApprovalRuleCondID)
            collObjICAPPRuleCondUser.Query.OrderBy(collObjICAPPRuleCondUser.Query.ApprovalRuleConditionUserID.Ascending)
            collObjICAPPRuleCondUser.Query.Load()



            Return collObjICAPPRuleCondUser
        End Function
        Public Shared Function GetApprovalRuleConditionsByApprovalRuleIDAsDataTable(ByVal ApprovalRuleCondID As String) As DataTable
            Dim qryObjICAppRuleCondUser As New ICApprovalRuleConditionUsersQuery("qryObjICAppRuleCondUser")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            qryObjICAppRuleCondUser.Select(qryObjICAppRuleCondUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"))
            qryObjICAppRuleCondUser.LeftJoin(qryObjICUser).On(qryObjICAppRuleCondUser.UserID = qryObjICUser.UserID)
            qryObjICAppRuleCondUser.OrderBy(qryObjICUser.UserName.Ascending)




            Return qryObjICAppRuleCondUser.LoadDataTable
        End Function
        Public Shared Sub DeleteApprovalRuleCondition(ByVal ApprovalRuleCondUserID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICApprovalRuleCondUser As New IC.ICApprovalRuleConditionUsers
            Dim ActionString As String = ""

            objICApprovalRuleCondUser.LoadByPrimaryKey(ApprovalRuleCondUserID)
            objICApprovalRuleCondUser.MarkAsDeleted()
            ActionString = "Approval Rule Condition User : [ " & objICApprovalRuleCondUser.UserID & " ], for condition [ " & objICApprovalRuleCondUser.ApprovaRuleCondID & " ]"
            ActionString += " deleted."
            ICUtilities.AddAuditTrail(ActionString, "Approval Rule Condition User", objICApprovalRuleCondUser.ApprovalRuleConditionUserID, UsersID.ToString, UsersName.ToString, "DELETE")
            objICApprovalRuleCondUser.Save()
        End Sub
        Public Shared Function GetApprovalRuleCondUSersByApprovalRuleCondID(ByVal ApprovalRuleCondID As String, ByVal ComditionType As String, ByVal SelectionCriteria As String) As DataTable
            Dim qryObjICApprovalRuleCondUser As New IC.ICApprovalRuleConditionUsersQuery("qryObjICApprovalRuleCondUser")
            Dim qryObjICApprovalRuleCond As New IC.ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
            Dim ActionString As String = ""
            Dim dt As New DataTable

            qryObjICApprovalRuleCondUser.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICApprovalRuleCondUser.ApprovaRuleCondID = qryObjICApprovalRuleCond.ApprovalRuleConditionID)
            qryObjICApprovalRuleCondUser.Where(qryObjICApprovalRuleCondUser.ApprovaRuleCondID = ApprovalRuleCondID And qryObjICApprovalRuleCond.ConditionType = ComditionType)
            qryObjICApprovalRuleCondUser.Where(qryObjICApprovalRuleCond.SelectionCriteria = SelectionCriteria)
            qryObjICApprovalRuleCondUser.LoadDataTable()

            Return qryObjICApprovalRuleCondUser.LoadDataTable()

        End Function
        Public Shared Function GetApprovalRuleCondUSersByApprovalRuleCondIDAndAppGroupID(ByVal ApprovalRuleCondID As String, ByVal ComditionType As String, ByVal SelectionCriteria As String, ByVal ApprovalGroupID As String) As DataTable
            Dim qryObjICApprovalRuleCondUser As New IC.ICApprovalRuleConditionUsersQuery("qryObjICApprovalRuleCondUser")
            Dim qryObjICApprovalRuleCond As New IC.ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
            Dim ActionString As String = ""
            Dim dt As New DataTable

            qryObjICApprovalRuleCondUser.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICApprovalRuleCondUser.ApprovaRuleCondID = qryObjICApprovalRuleCond.ApprovalRuleConditionID)
            qryObjICApprovalRuleCondUser.Where(qryObjICApprovalRuleCondUser.ApprovaRuleCondID = ApprovalRuleCondID And qryObjICApprovalRuleCond.ConditionType = ComditionType)
            qryObjICApprovalRuleCondUser.Where(qryObjICApprovalRuleCond.SelectionCriteria = SelectionCriteria And qryObjICApprovalRuleCond.ApprovalGroupID = ApprovalGroupID)
            qryObjICApprovalRuleCondUser.LoadDataTable()

            Return qryObjICApprovalRuleCondUser.LoadDataTable()

        End Function



        Public Shared Function GetApprovalRuleConditionUsersByPNAndAmountAndCompnayCode(ByVal PNCode As String, ByVal Amount As Double, ByVal CompanyCode As String) As DataTable
            Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")
            Dim qryObjICAppRule As New ICApprovalRuleQuery("qryObjICAppRule")
            Dim qryObjICAppCond As New ICApprovalRuleConditionsQuery("qryObjICAppCond")
            Dim qryObjICAppCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppCondUsers")



            qryObjICAppCond.Select(qryObjICAppCondUsers.UserID.Distinct, qryObjICUsers.UserName, qryObjICUsers.DisplayName, qryObjICUsers.Email, qryObjICUsers.CellNo)
            qryObjICAppCond.InnerJoin(qryObjICAppRule).On(qryObjICAppCond.ApprovalRuleID = qryObjICAppRule.ApprovalRuleID)
            qryObjICAppCond.InnerJoin(qryObjICAppCondUsers).On(qryObjICAppCond.ApprovalRuleConditionID = qryObjICAppCondUsers.ApprovaRuleCondID)
            qryObjICAppCond.InnerJoin(qryObjICUsers).On(qryObjICAppCondUsers.UserID = qryObjICUsers.UserID)
            qryObjICAppCond.Where(qryObjICAppRule.PaymentNatureCode = PNCode And qryObjICAppRule.CompanyCode = CompanyCode)
            qryObjICAppCond.Where(qryObjICAppRule.FromAmount <= Amount And qryObjICAppRule.ToAmount >= Amount And qryObjICAppRule.IsActive = True)
            'qryObjICAppCondUsers.es.Distinct = True

            Return qryObjICAppCond.LoadDataTable
        End Function
        Public Shared Function IsUserTaggedWithCondition(ByVal UserID As String, ByVal ApprovalGroupID As String, ByVal ConditionID As String)
            Dim qryObjICCondusers As New ICApprovalRuleConditionUsersQuery("qryObjICCondusers")
            Dim qryObjICCondition As New ICApprovalRuleConditionsQuery("qryObjICCondition")

            qryObjICCondition.Select(qryObjICCondition.ApprovalGroupID)
            qryObjICCondition.InnerJoin(qryObjICCondusers).On(qryObjICCondition.ApprovalRuleConditionID = qryObjICCondusers.ApprovaRuleCondID)
            qryObjICCondition.Where(qryObjICCondusers.UserID = UserID And qryObjICCondition.ApprovalRuleConditionID = ConditionID And qryObjICCondition.ApprovalGroupID = ApprovalGroupID)
            If qryObjICCondition.LoadDataTable.Rows.Count > 0 Then


                Return True
            Else
                Return False

            End If
        End Function


    End Class
End Namespace
