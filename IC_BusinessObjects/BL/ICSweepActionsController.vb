﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICSweepActionsController
        Public Shared Function AddSweepAction(ByVal ICsweepAction As ICSweepAction, ByVal IsUpdate As Boolean, ByVal UsersID As String,
                                              ByVal UsersName As String, ByVal ActionType As String) As Integer

            Dim i As Integer = 0
            Dim objICSweepActions As New ICSweepAction
            Dim StrAction As String = Nothing
            If IsUpdate = False Then
                objICSweepActions.SweepActionCreationDate = ICsweepAction.SweepActionCreationDate
                objICSweepActions.SweepActionCreatedBy = ICsweepAction.SweepActionCreatedBy
            Else
                objICSweepActions.LoadByPrimaryKey(ICsweepAction.SweepActionID)
                objICSweepActions.SweepActionCreationDate = ICsweepAction.SweepActionCreationDate
                objICSweepActions.SweepActionCreatedBy = ICsweepAction.SweepActionCreatedBy
            End If
            objICSweepActions.FromAccountNo = ICsweepAction.FromAccountNo
            objICSweepActions.FromAccountBrCode = ICsweepAction.FromAccountBrCode
            objICSweepActions.FromAccountCurrency = ICsweepAction.FromAccountCurrency
            objICSweepActions.ToAccountNo = ICsweepAction.ToAccountNo
            objICSweepActions.ToAccountBrCode = ICsweepAction.ToAccountBrCode
            objICSweepActions.ToAccountCurrency = ICsweepAction.ToAccountCurrency
            objICSweepActions.AmountPerInstruction = ICsweepAction.AmountPerInstruction
            objICSweepActions.FromAccountType = ICsweepAction.FromAccountType
            objICSweepActions.ToAccountType = ICsweepAction.ToAccountType
            objICSweepActions.IsProcessed = ICsweepAction.IsProcessed
            objICSweepActions.IsActive = ICsweepAction.IsActive
            objICSweepActions.IsApproved = ICsweepAction.IsApproved
            objICSweepActions.CurrentStatus = ICsweepAction.CurrentStatus
            objICSweepActions.SweepActionFromDate = ICsweepAction.SweepActionFromDate
            objICSweepActions.SweepActionToDate = ICsweepAction.SweepActionToDate
            objICSweepActions.SweepActionFrequency = ICsweepAction.SweepActionFrequency
            objICSweepActions.SweepActionTitle = ICsweepAction.SweepActionTitle
            objICSweepActions.SweepActionTime = ICsweepAction.SweepActionTime
            objICSweepActions.SweepActionDayType = ICsweepAction.SweepActionDayType
            objICSweepActions.TotalInstructions = ICsweepAction.TotalInstructions
            objICSweepActions.GroupCode = ICsweepAction.GroupCode
            objICSweepActions.CompanyCode = ICsweepAction.CompanyCode
            objICSweepActions.Save()
            StrAction = "Sweep Action With ID : " & objICSweepActions.SweepActionID & " from account no : [ " & objICSweepActions.FromAccountNo & " ] to "
            StrAction += "account no [ " & objICSweepActions.ToAccountNo & " ] of total instructions [ " & objICSweepActions.TotalInstructions & " ]"
            StrAction += " with amount per instruction [ " & objICSweepActions.AmountPerInstruction & " "
            If IsUpdate = False Then
                StrAction += "] added."""
            Else
                StrAction += "] updated."""
            End If
            StrAction += "Action was taken by user "
            StrAction += " [ " & UsersID & " ] [ " & UsersName & " ] "
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action", objICSweepActions.SweepActionID.ToString, UsersID, UsersName, ActionType)
            Return objICSweepActions.SweepActionID
        End Function
        Public Shared Function DeleteSweepActionsbySweepActionID(ByVal SweepActionID As String, ByVal UsersID As String, ByVal UsersName As String,
                                                                 ByVal ActionType As String) As Boolean
            Dim objICSweepAction As New ICSweepAction
            Dim StrAction As String = Nothing
            Dim Result As Boolean = False
            objICSweepAction.LoadByPrimaryKey(SweepActionID)
            objICSweepAction.MarkAsDeleted()
            objICSweepAction.Save()
            StrAction = "Sweep Action With ID : " & objICSweepAction.SweepActionID & " from account no : [ " & objICSweepAction.FromAccountNo & " ] to "
            StrAction += "account no [ " & objICSweepAction.ToAccountNo & " ] of total instructions [ " & objICSweepAction.TotalInstructions & " ]"
            StrAction += " with amount per instruction [ " & objICSweepAction.AmountPerInstruction & " deleted. Action was taken by user "
            StrAction += " [ " & UsersID & " ] [ " & UsersName & " ] "
            ICUtilities.AddAuditTrail(StrAction, "Sweep Action", objICSweepAction.SweepActionID.ToString, UsersID, UsersName, ActionType)
            Result = False
            Return Result
        End Function
        Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer,
                                                                        ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String,
                                                                        ByVal dtAccountNo As DataTable, ByVal dtFromAccountNo As DataTable)


            Dim qryObjICSweepActions As New ICSweepActionQuery("qryObjICSweepActions")
            Dim qryObjICSweepActionsInstructions As New ICSweepActionInstructionsQuery("qryObjICSweepActionsInstructions")
            Dim ArrayAssignedAccounts As New ArrayList
            Dim dt As New DataTable
            qryObjICSweepActions.Select(qryObjICSweepActions.SweepActionID, qryObjICSweepActions.FromAccountNo, qryObjICSweepActions.ToAccountNo)
            qryObjICSweepActions.Select(qryObjICSweepActions.SweepActionFrequency, qryObjICSweepActions.TotalInstructions)
            'qryObjICSweepActions.Select((qryObjICSweepActionsInstructions.[Select](qryObjICSweepActionsInstructions.SweepActionInstructionID.Count).Where(qryObjICSweepActionsInstructions.SweepActionID = qryObjICSweepActions.SweepActionID)).As("TotalInstructions"))

            'qryObjICAccounts.OrderBy(qryObjICAccounts.AccountNumber.Descending, qryObjICAccounts.BranchCode.Descending, qryObjICAccounts.Currency.Descending, qryObjICAccounts.AccountTitle.Descending)
            qryObjICSweepActions.OrderBy(qryObjICSweepActions.SweepActionID.Descending)
            If Not GroupCode.ToString = "" Then
                qryObjICSweepActions.Where(qryObjICSweepActions.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode.ToString = "" Then
                qryObjICSweepActions.Where(qryObjICSweepActions.CompanyCode = CompanyCode.ToString)
            End If
            If dtAccountNo.Rows.Count > 0 Then
                For Each dr As DataRow In dtAccountNo.Rows
                    ArrayAssignedAccounts.Add(dr("AccountNumber"))
                Next

                qryObjICSweepActions.Where((qryObjICSweepActions.ToAccountNo + "-" + qryObjICSweepActions.ToAccountBrCode + "-" + qryObjICSweepActions.ToAccountCurrency).As("Match1").In(ArrayAssignedAccounts))

            End If
            If dtFromAccountNo.Rows.Count > 0 Then
                ArrayAssignedAccounts = New ArrayList
                For Each dr As DataRow In dtAccountNo.Rows
                    ArrayAssignedAccounts.Add(dr("AccountNumber"))
                Next

                qryObjICSweepActions.Where((qryObjICSweepActions.FromAccountNo + "-" + qryObjICSweepActions.FromAccountBrCode + "-" + qryObjICSweepActions.FromAccountCurrency).As("Match2").In(ArrayAssignedAccounts))
            End If

            If Not PageNumber = 0 Then
                qryObjICSweepActions.es.PageNumber = PageNumber
                qryObjICSweepActions.es.PageSize = PageSize
                rg.DataSource = qryObjICSweepActions.LoadDataTable
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICSweepActions.es.PageNumber = 1
                qryObjICSweepActions.es.PageSize = PageSize
                rg.DataSource = qryObjICSweepActions.LoadDataTable
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        Public Shared Function IsInstructionPaidAgainstSweepAction(ByVal SweepActionID As String) As Boolean
            Dim objICSWeepActionInstruction As New ICSweepActionInstructionsCollection
            Dim Result As Boolean = False


            objICSWeepActionInstruction.Query.Where(objICSWeepActionInstruction.Query.SweepActionID = SweepActionID And objICSWeepActionInstruction.Query.IsPaid = True)
            If objICSWeepActionInstruction.Query.Load Then
                Result = True
            End If

            Return Result
        End Function
    End Class
End Namespace
