﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICMasterSeriesController
        Public Shared Function GetAllMasterSeriesByAccnPaymentNaturePrdctTypeCode(ByVal AccntPayNAtureProductTypeCode As String) As ICMasterSeriesCollection
            Dim objICMAsterSeriesColl As New ICMasterSeriesCollection
            objICMAsterSeriesColl.es.Connection.CommandTimeout = 3600




            objICMAsterSeriesColl.Query.Where(objICMAsterSeriesColl.Query.AccountsPaymentNatureProductTypeCode = AccntPayNAtureProductTypeCode)
            objICMAsterSeriesColl.Query.OrderBy(objICMAsterSeriesColl.Query.MasterSeriesID.Descending)
            objICMAsterSeriesColl.Query.Load()


            Return objICMAsterSeriesColl

        End Function

        Public Shared Function GetIsMasterSeriesAssignedToLocation(ByVal MasterSeriesID As String) As Boolean

            Dim objICInstrumentColl As New ICInstrumentsCollection
            Dim Result As Boolean = False
            objICInstrumentColl.es.Connection.CommandTimeout = 3600
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.OfficeID.IsNotNull() Or objICInstrumentColl.Query.IsUSed.IsNotNull())
            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.MasterSeriesID = MasterSeriesID)

            If objICInstrumentColl.Query.Load = True Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function GetAllMasterSeriesByAccnNmberBranchCodeCurrency(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As ICMasterSeriesCollection
            Dim objICMAsterSeriesColl As New ICMasterSeriesCollection
            objICMAsterSeriesColl.es.Connection.CommandTimeout = 3600




            objICMAsterSeriesColl.Query.Where(objICMAsterSeriesColl.Query.AccountNumber = AccountNumber And objICMAsterSeriesColl.Query.BranchCode = BranchCode And objICMAsterSeriesColl.Query.Currency = Currency)
            objICMAsterSeriesColl.Query.OrderBy(objICMAsterSeriesColl.Query.MasterSeriesID.Descending)
            objICMAsterSeriesColl.Query.Load()


            Return objICMAsterSeriesColl

        End Function
        Public Shared Sub DeleteMasterSeries(ByVal MasterSereisID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICMasterSeries As New ICMasterSeries
            objICMasterSeries.es.Connection.CommandTimeout = 3600
            objICMasterSeries.LoadByPrimaryKey(MasterSereisID)
            objICMasterSeries.MarkAsDeleted()
            ICUtilities.AddAuditTrail("Master Series: ID " & MasterSereisID.ToString & " deleted.", "Master Series", MasterSereisID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
            objICMasterSeries.Save()
        End Sub
        Public Shared Sub GetAllMasterSeriesForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String)

            Dim qryObjMasterSeries As New ICMasterSeriesQuery("qryObjMasterSeries")
            Dim qryObjInstruments As New ICInstrumentsQuery("qryObjInstruments")
            Dim dt As New DataTable
            qryObjMasterSeries.Select(qryObjMasterSeries.MasterSeriesID, qryObjMasterSeries.StartFrom, qryObjMasterSeries.EndsAt, qryObjMasterSeries.IsPrePrinted)
            qryObjMasterSeries.Select(qryObjMasterSeries.AccountNumber, qryObjMasterSeries.BranchCode, qryObjMasterSeries.Currency)
            qryObjMasterSeries.Select(qryObjInstruments.InstrumentNumber.Count.Coalesce("'-'").As("TotalCount"))
            qryObjMasterSeries.InnerJoin(qryObjInstruments).On(qryObjMasterSeries.MasterSeriesID = qryObjInstruments.MasterSeriesID)
            qryObjMasterSeries.Where(qryObjMasterSeries.AccountNumber = AccountNumber.ToString And qryObjMasterSeries.BranchCode = BranchCode.ToString And qryObjMasterSeries.Currency = Currency.ToString)
            qryObjMasterSeries.GroupBy(qryObjMasterSeries.MasterSeriesID, qryObjInstruments.MasterSeriesID, qryObjMasterSeries.StartFrom, qryObjMasterSeries.EndsAt, qryObjMasterSeries.IsPrePrinted, qryObjMasterSeries.AccountNumber, qryObjMasterSeries.BranchCode, qryObjMasterSeries.Currency)
            qryObjMasterSeries.OrderBy(qryObjMasterSeries.MasterSeriesID.Descending)
            dt = qryObjMasterSeries.LoadDataTable
            If Not PageNumber = 0 Then
                qryObjMasterSeries.es.PageNumber = PageNumber
                qryObjMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                qryObjMasterSeries.es.PageNumber = 1
                qryObjMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If


            End If



        End Sub
        Public Shared Function AddMasterSeries(ByVal cMasterSeries As ICMasterSeries, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal StrAction As String) As Integer
            Dim objICMasterSeries As New ICMasterSeries
            Dim i As Integer = 0
            objICMasterSeries.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                objICMasterSeries.BranchCode = cMasterSeries.BranchCode
                objICMasterSeries.AccountNumber = cMasterSeries.AccountNumber
                objICMasterSeries.Currency = cMasterSeries.Currency
                objICMasterSeries.CreateBy = cMasterSeries.CreateBy
                objICMasterSeries.CreateDate = cMasterSeries.CreateDate
                objICMasterSeries.Creater = cMasterSeries.Creater
                objICMasterSeries.CreationDate = cMasterSeries.CreationDate
                objICMasterSeries.IsApprove = True
                objICMasterSeries.Approvedby = cMasterSeries.Approvedby
                objICMasterSeries.ApprovedOn = cMasterSeries.ApprovedOn
                objICMasterSeries.IsActive = True
            Else
                objICMasterSeries.LoadByPrimaryKey(cMasterSeries.MasterSeriesID)
                objICMasterSeries.CreateBy = cMasterSeries.CreateBy
                objICMasterSeries.CreateDate = cMasterSeries.CreateDate
            End If

            objICMasterSeries.StartFrom = cMasterSeries.StartFrom
            objICMasterSeries.EndsAt = cMasterSeries.EndsAt

            objICMasterSeries.IsPrePrinted = cMasterSeries.IsPrePrinted
            objICMasterSeries.Save()
            If Not objICMasterSeries.MasterSeriesID = 0 Then
                i = objICMasterSeries.MasterSeriesID
            Else
                i = 0
            End If
            If IsUpdate = False Then
                ICUtilities.AddAuditTrail("Master Series with ID [ " & i.ToString() & " ] for account number [ " & objICMasterSeries.AccountNumber & " ] start from [ " & objICMasterSeries.StartFrom & " ] and ends at [ " & objICMasterSeries.EndsAt & " ] Added", "Master Series", i.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
            Else
                ICUtilities.AddAuditTrail(StrAction.ToString, "Master Series", i.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")
            End If
            Return i
        End Function
        Public Shared Function CheckIsInstrumentNumberIsUsedByInstrumentNumber(ByVal InstrumentNumber As String) As Boolean
            Dim qryObjICInstrumentsColl As New ICInstrumentsCollection
            qryObjICInstrumentsColl.es.Connection.CommandTimeout = 3600
            Dim Result As Boolean = False

            qryObjICInstrumentsColl.Query.Where(qryObjICInstrumentsColl.Query.InstrumentNumber = InstrumentNumber)
            qryObjICInstrumentsColl.Query.Where(qryObjICInstrumentsColl.Query.IsUSed = True Or qryObjICInstrumentsColl.Query.IsAssigned = True)
            If qryObjICInstrumentsColl.Query.Load() Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function CheckIsInstrumentNumberBelongsToPrintLocation(ByVal InstrumentNumber As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal OfficeCode As String) As Boolean

            Dim qryObjICInstruments As New ICInstrumentsQuery("qryObjICInstruments")
            Dim qryObjICMasterSeries As New ICMasterSeriesQuery("qryObjICMasterSeries")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim Result As Boolean = False
            Dim dt As New DataTable
            qryObjICInstruments.Select(qryObjICInstruments.InstrumentNumber)
            qryObjICInstruments.InnerJoin(qryObjICMasterSeries).On(qryObjICInstruments.MasterSeriesID = qryObjICMasterSeries.MasterSeriesID)
            qryObjICInstruments.InnerJoin(qryObjICAccounts).On(qryObjICMasterSeries.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICMasterSeries.BranchCode = qryObjICAccounts.BranchCode And qryObjICMasterSeries.Currency = qryObjICAccounts.Currency)
            qryObjICInstruments.Where(qryObjICAccounts.AccountNumber = AccountNumber And qryObjICAccounts.BranchCode = BranchCode And qryObjICAccounts.Currency = Currency)
            qryObjICInstruments.Where(qryObjICInstruments.InstrumentNumber = InstrumentNumber And qryObjICInstruments.OfficeID = OfficeCode)
            dt = qryObjICInstruments.LoadDataTable
            If dt.Rows.Count > 0 Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Sub CheckMarkIsInstrumentNumberIsUsedByInstrumentNumber(ByVal InstrumentNumber As String, ByVal InstructionID As String, ByVal UsersID As String, ByVal UsersName As String)
            Dim qryObjICInstrumentsColl As New ICInstrumentsCollection
            Dim objICInstruction As ICInstruction
            Dim StrAction As String
            qryObjICInstrumentsColl.es.Connection.CommandTimeout = 3600

            Dim Result As Boolean = False

            qryObjICInstrumentsColl.Query.Where(qryObjICInstrumentsColl.Query.InstrumentNumber = InstrumentNumber And qryObjICInstrumentsColl.Query.IsUSed.IsNull)
            If qryObjICInstrumentsColl.Query.Load() Then
                For Each objICInstrument As ICInstruments In qryObjICInstrumentsColl
                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID)
                    objICInstruction.es.Connection.CommandTimeout = 3600
                    StrAction = Nothing
                    objICInstrument.IsAssigned = True
                    objICInstrument.Save()
                    StrAction += "Instrument number [ " & InstrumentNumber & " ] against instruction with ID [ " & objICInstruction.InstructionID & " ], "
                    StrAction += "account number [ " & objICInstruction.ClientAccountNo & ", branch code [ " & objICInstruction.ClientAccountBranchCode & " ], "
                    StrAction += "currency [ " & objICInstruction.ClientAccountCurrency & ", payment nature  [ " & objICInstruction.PaymentNatureCode & " ] ,"
                    StrAction += "product type [ " & objICInstruction.ProductTypeCode & " is assigned."
                    ICUtilities.AddAuditTrail(StrAction, "Instrument", objICInstrument.InstrumentsID.ToString, UsersID.ToString, UsersName.ToString, "ASSIGN")
                Next
            End If
        End Sub
    End Class
End Namespace

