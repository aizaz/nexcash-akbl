﻿Imports Telerik.Web.UI

Namespace IC
    Public Class ICInstructionAuditLogController
        Public Shared Sub GetAllInstructionsAuditLogForGV(ByVal FromInstID As String, ByVal ToInstID As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryObjICInstructionActivity As New ICInstructionActivityQuery("qryObjICInstruction")

            Dim qryObjICFromStatus As New ICInstructionStatusQuery("qryObjICStatus")
            Dim qryObjICToStatus As New ICInstructionStatusQuery("qryObjICToStatus")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim dt As New DataTable

            qryObjICInstructionActivity.Select(qryObjICInstructionActivity.InstructionActivityID.Coalesce("'-'"), qryObjICInstructionActivity.InstructionID.Coalesce("'-'"), qryObjICInstructionActivity.Action.Coalesce("'-'"))
            qryObjICInstructionActivity.Select(qryObjICUser.UserName.Coalesce("'-'"), qryObjICUser.DisplayName.Coalesce("'-'"), qryObjICInstructionActivity.ActionDate.Coalesce("'-'"))
            qryObjICInstructionActivity.Select(qryObjICInstructionActivity.UserIP.Coalesce("'-'"), qryObjICInstructionActivity.BatchNo.Coalesce("'-'"))
            qryObjICInstructionActivity.Select(qryObjICInstructionActivity.CompanyCode.Coalesce("'-'"), qryObjICGroup.GroupCode.Coalesce("'-'"))
            qryObjICInstructionActivity.Select(qryObjICFromStatus.StatusName.Coalesce("'-'").As("FromStatus"), qryObjICToStatus.StatusName.Coalesce("'-'").As("ToStatus"))

            qryObjICInstructionActivity.LeftJoin(qryObjICCompany).On(qryObjICInstructionActivity.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICInstructionActivity.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICInstructionActivity.LeftJoin(qryObjICUser).On(qryObjICInstructionActivity.ActionBy = qryObjICUser.UserID)
            qryObjICInstructionActivity.LeftJoin(qryObjICToStatus).On(qryObjICInstructionActivity.ToStatus = qryObjICToStatus.StatusID)
            qryObjICInstructionActivity.LeftJoin(qryObjICFromStatus).On(qryObjICInstructionActivity.FromStatus = qryObjICFromStatus.StatusID)

            If FromInstID <> "" And ToInstID.ToString <> "" Then
                qryObjICInstructionActivity.Where(qryObjICInstructionActivity.InstructionID.Between(FromInstID, ToInstID))
            ElseIf FromInstID <> "" And ToInstID = "" Then
                qryObjICInstructionActivity.Where(qryObjICInstructionActivity.InstructionID = FromInstID)
            ElseIf FromInstID = "" And ToInstID <> "" Then
                qryObjICInstructionActivity.Where(qryObjICInstructionActivity.InstructionID = ToInstID)
            End If
            If CompanyCode.ToString <> "" Then
                qryObjICInstructionActivity.Where(qryObjICInstructionActivity.CompanyCode = CompanyCode.ToString)
            End If
            If GroupCode.ToString <> "" Then
                qryObjICInstructionActivity.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
     

            qryObjICInstructionActivity.OrderBy(qryObjICInstructionActivity.InstructionActivityID, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)

            dt = qryObjICInstructionActivity.LoadDataTable
            If Not CurrentPage = 0 Then
                qryObjICInstructionActivity.es.PageNumber = CurrentPage
                qryObjICInstructionActivity.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICInstructionActivity.es.PageNumber = 1
                qryObjICInstructionActivity.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If

        End Sub

    End Class
End Namespace