﻿Imports System
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DotNetNuke.Entities
Imports DotNetNuke.Entities.Users
Imports DotNetNuke.Security.Roles
Namespace IC
    Public Class ICBankRoleRightsController
        '' Javed 25 November 2012
        Public Shared Sub DeleteRoleRight(ByVal RoleID As String, ByVal RightName As String, ByVal usersID As String, ByVal UsersName As String)

            Dim objICRoleRights As ICRoleRights
            Dim objICRoleRightsSecond As New ICRoleRights
            Dim objICRoleRightsColl As New ICRoleRightsCollection
            Dim objICRole As New ICRole
            Dim dt As New DataTable
            Dim StrRightDetailName As String = ""
            Dim StrRightName As String = ""
            Dim StrRoleRightID As String = ""


            objICRole.es.Connection.CommandTimeout = 3600

            objICRoleRightsColl.es.Connection.CommandTimeout = 3600
            objICRoleRightsSecond.es.Connection.CommandTimeout = 3600

            objICRole.LoadByPrimaryKey(RoleID.ToString())
            objICRoleRightsColl.Query.Where(objICRoleRightsColl.Query.RoleID = RoleID And objICRoleRightsColl.Query.RightName = RightName.ToString())
            dt = objICRoleRightsColl.Query.LoadDataTable
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows

                    objICRoleRights = New ICRoleRights
                    objICRoleRights.es.Connection.CommandTimeout = 3600
                    If objICRoleRights.LoadByPrimaryKey(dr.Item(0)) Then
                        StrRightDetailName = objICRoleRights.RightDetailName.ToString
                        StrRightName = objICRoleRights.RightName.ToString
                        StrRoleRightID = objICRoleRights.RoleRightID.ToString
                        objICRoleRights.MarkAsDeleted()
                        objICRoleRights.Save()
                    End If
                    ICUtilities.AddAuditTrail("Access of page [ " & StrRightName & " ] of right [ " & StrRightDetailName.ToString & " ] for Role [ " & objICRole.RoleName.ToString & " ] deleted. ", "Role Right", StrRoleRightID.ToString, usersID.ToString, UsersName.ToString, "DELETE")

                Next


            End If


        End Sub
        'Public Shared Sub DeleteRoleRight(ByVal RoleID As String, ByVal RightName As String)

        '    Dim objICBankRoleRights As New ICBankRoleRights
        '    Dim objICBankRoleRightsSecond As New ICBankRoleRights
        '    Dim objICBankRoleRightsColl As New ICBankRoleRightsCollection

        '    objICBankRoleRights.es.Connection.CommandTimeout = 3600
        '    objICBankRoleRightsColl.es.Connection.CommandTimeout = 3600
        '    objICBankRoleRightsSecond.es.Connection.CommandTimeout = 3600


        '    objICBankRoleRightsColl.Query.Where(objICBankRoleRightsColl.Query.RoleID = RoleID And objICBankRoleRightsColl.Query.RightName = RightName.ToString())
        '    objICBankRoleRightsColl.Query.Load()
        '    If objICBankRoleRightsColl.Query.Load() Then
        '        For Each objICBankRoleRights In objICBankRoleRightsColl
        '            If objICBankRoleRightsSecond.LoadByPrimaryKey(objICBankRoleRights.BankRoleRightsID) Then
        '                objICBankRoleRightsSecond.MarkAsDeleted()
        '                objICBankRoleRightsSecond.Save()
        '            End If

        '        Next


        '    End If


        'End Sub
        Public Shared Function GetRoleRightByRoleIDAndRightName(ByVal RoleID As String, ByVal RightName As String) As ICBankRoleRightsCollection

            Dim objICBankRoleRights As New ICBankRoleRights
            Dim objICBankRoleRightsSecond As New ICBankRoleRights
            Dim objICBankRoleRightsColl As New ICBankRoleRightsCollection

            objICBankRoleRights.es.Connection.CommandTimeout = 3600
            objICBankRoleRightsColl.es.Connection.CommandTimeout = 3600
            objICBankRoleRightsSecond.es.Connection.CommandTimeout = 3600


            objICBankRoleRightsColl.Query.Where(objICBankRoleRightsColl.Query.RoleID = RoleID And objICBankRoleRightsColl.Query.RightName = RightName.ToString())
            objICBankRoleRightsColl.Query.Load()
            Return objICBankRoleRightsColl


        End Function
        Public Shared Function ValidateRightsOnRole(ByVal PortalID As String, ByVal UserID As String, ByVal RightName As String) As ArrayList

            Dim ShowAdd, ShowEdit, ShowDelete, ShowApproved As Boolean
            Dim infoUser As New UserRoleInfo
            Dim cRole As New RoleController
            Dim ArrList As New ArrayList
            Dim arrlstRoleIDs As New ArrayList
            Dim objICBankRoleRightColl As New ICBankRoleRightsCollection
            objICBankRoleRightColl.es.Connection.CommandTimeout = 3600

            Dim ArrResult As New ArrayList

            ShowAdd = False
            ShowEdit = False
            ShowDelete = False
            ShowApproved = False

            ArrList = cRole.GetUserRoles(PortalID, UserID)
            For Each infoUser In ArrList
                arrlstRoleIDs.Add(infoUser.RoleID.ToString())
            Next

            If arrlstRoleIDs.Count > 0 Then
                objICBankRoleRightColl.Query.Where(objICBankRoleRightColl.Query.RoleID.In(arrlstRoleIDs), objICBankRoleRightColl.Query.RightName = RightName.ToString())
            Else
                objICBankRoleRightColl.LoadAll()
            End If


            If objICBankRoleRightColl.Query.Load() Then
                For Each rr As ICBankRoleRights In objICBankRoleRightColl
                    If rr.CanAdd = True Then
                        ShowAdd = True
                    End If
                    If rr.CanEdit = True Then
                        ShowEdit = True
                    End If
                    If rr.CanDelete = True Then
                        ShowDelete = True
                    End If
                    If rr.CanApprove = True Then
                        ShowApproved = True
                    End If
                Next
            End If

            ArrResult.Add(ShowAdd)
            ArrResult.Add(ShowEdit)
            ArrResult.Add(ShowDelete)
            ArrResult.Add(ShowApproved)

            Return ArrResult



        End Function

        Public Shared Function GetDistinctRightNameByRoleID(ByVal RoleID As String) As DataTable
            Dim objICRoleRightColl As New ICRoleRightsCollection
            objICRoleRightColl.es.Connection.CommandTimeout = 3600



            objICRoleRightColl.Query.Select(objICRoleRightColl.Query.RightName)
            objICRoleRightColl.Query.Where(objICRoleRightColl.Query.RoleID = RoleID)
            objICRoleRightColl.Query.es.Distinct = True
            Return objICRoleRightColl.Query.LoadDataTable()

        End Function
        Public Shared Function GetDistinctRightDetailNameByRightName(ByVal RightName As String, ByVal RoleID As String) As DataTable
            Dim objICRoleRightColl As New ICRoleRightsCollection
            objICRoleRightColl.es.Connection.CommandTimeout = 3600



            objICRoleRightColl.Query.Select(objICRoleRightColl.Query.RightDetailName)
            objICRoleRightColl.Query.Select(objICRoleRightColl.Query.RightValue.Case.When(objICRoleRightColl.Query.RightValue.IsNull).Then("False").When(objICRoleRightColl.Query.RightValue = False).Then("False").Else(objICRoleRightColl.Query.RightValue).End.As("RightValue"))
            objICRoleRightColl.Query.Select(objICRoleRightColl.Query.IsApproved)
            objICRoleRightColl.Query.Where(objICRoleRightColl.Query.RightName = RightName And objICRoleRightColl.Query.RoleID = RoleID)
            'objICRoleRightColl.Query.es.Distinct = True
            Return objICRoleRightColl.Query.LoadDataTable()

        End Function
        'Public Shared Function GetDistinctRightDetailNameByRightName(ByVal RightName As String, ByVal RoleID As String) As DataTable
        '    Dim objICRoleRightColl As New ICRoleRightsCollection
        '    objICRoleRightColl.es.Connection.CommandTimeout = 3600



        '    objICRoleRightColl.Query.Select(objICRoleRightColl.Query.RightDetailName, objICRoleRightColl.Query.RightValue, objICRoleRightColl.Query.IsApproved)
        '    objICRoleRightColl.Query.Where(objICRoleRightColl.Query.RightName = RightName And objICRoleRightColl.Query.RoleID = RoleID)
        '    'objICRoleRightColl.Query.es.Distinct = True
        '    Return objICRoleRightColl.Query.LoadDataTable()

        'End Function
        Public Shared Function GetRoleRightIDByRightNameAndRoleID(ByVal RightName As String, ByVal RoleID As String) As DataTable
            Dim objICRoleRightColl As New ICRoleRightsCollection
            objICRoleRightColl.es.Connection.CommandTimeout = 3600



            objICRoleRightColl.Query.Select(objICRoleRightColl.Query.RoleRightID, objICRoleRightColl.Query.CreatedBy, objICRoleRightColl.Query.IsApproved)
            objICRoleRightColl.Query.Where(objICRoleRightColl.Query.RightName = RightName And objICRoleRightColl.Query.RoleID = RoleID)
            'objICRoleRightColl.Query.es.Distinct = True
            Return objICRoleRightColl.Query.LoadDataTable()

        End Function
        Public Shared Sub ApproveBankRoleRights(ByVal BankRoleRightID As String, ByVal IsaPrroved As Boolean, ByVal ApprovedDate As DateTime, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICRoleRight As New ICRoleRights
            objICRoleRight.es.Connection.CommandTimeout = 3600


            If objICRoleRight.LoadByPrimaryKey(BankRoleRightID) Then

                objICRoleRight.IsApproved = IsaPrroved
                objICRoleRight.ApprovedBy = UsersID
                objICRoleRight.ApprovedOn = ApprovedDate
                objICRoleRight.Save()
                ICUtilities.AddAuditTrail("Access to page [ " & objICRoleRight.RightName & " ] for right [ " & objICRoleRight.RightDetailName.ToString & " ] for Role [ " & objICRoleRight.UpToICRoleByRoleID.RoleName.ToString & " ] approved.", "Role Right", objICRoleRight.RoleID.ToString, UsersID.ToString, UsersName.ToString, "APPROVE")

            End If

        End Sub

    End Class
End Namespace

