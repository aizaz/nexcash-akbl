﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC

    Public Class ICDDMasterSeriesController

        Public Shared Function AddDDMasterSeries(ByVal DDMasterSeries As ICDDMasterSeries, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As Integer
            Dim objICDDMasterSeries As New ICDDMasterSeries
            Dim prevobjICDDMasterSeries As New ICDDMasterSeries
            Dim IsActiveText As String
            Dim i As Integer = 0
            Dim CurrentAt As String = ""
            Dim PrevAt As String = ""

            objICDDMasterSeries.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                objICDDMasterSeries.CreatedBy = DDMasterSeries.CreatedBy
                objICDDMasterSeries.CreatedDate = DDMasterSeries.CreatedDate
                objICDDMasterSeries.Creater = DDMasterSeries.Creater
                objICDDMasterSeries.CreationDate = DDMasterSeries.CreationDate
            Else
                objICDDMasterSeries.LoadByPrimaryKey(DDMasterSeries.DDMasterSeriesID)
                prevobjICDDMasterSeries.LoadByPrimaryKey(DDMasterSeries.DDMasterSeriesID)
                objICDDMasterSeries.CreatedBy = DDMasterSeries.CreatedBy
                objICDDMasterSeries.CreatedDate = DDMasterSeries.CreatedDate
            End If

            objICDDMasterSeries.PreFix = DDMasterSeries.PreFix

            objICDDMasterSeries.StartFrom = DDMasterSeries.StartFrom
            objICDDMasterSeries.EndsAt = DDMasterSeries.EndsAt

            objICDDMasterSeries.IsActive = True

            If objICDDMasterSeries.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            objICDDMasterSeries.Save()

            If (isUpdate = False) Then
                CurrentAt = "DDMasterSeries [Code:  " & objICDDMasterSeries.DDMasterSeriesID.ToString() & " ; Prefix:  " & objICDDMasterSeries.PreFix.ToString() & " ; Start From:  " & objICDDMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICDDMasterSeries.EndsAt.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "DDMasterSeries", objICDDMasterSeries.DDMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

            Else
                CurrentAt = "DDMasterSeries : Current Values [Code:  " & objICDDMasterSeries.DDMasterSeriesID.ToString() & " ; Prefix:  " & objICDDMasterSeries.PreFix.ToString() & " ; Start From:  " & objICDDMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICDDMasterSeries.EndsAt.ToString() & " ; IsActive:  " & IsActiveText.ToString() & "]"
                PrevAt = "<br />DDMasterSeries : Previous Values [Code:  " & prevobjICDDMasterSeries.DDMasterSeriesID.ToString() & " ; Prefix:  " & prevobjICDDMasterSeries.PreFix.ToString() & " ; Start From:  " & prevobjICDDMasterSeries.StartFrom.ToString() & " ; Ends At:  " & prevobjICDDMasterSeries.EndsAt.ToString() & " ; IsActive:  " & prevobjICDDMasterSeries.IsActive.ToString() & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "DDMasterSeries", objICDDMasterSeries.DDMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

            End If

            If Not objICDDMasterSeries.DDMasterSeriesID = 0 Then
                i = objICDDMasterSeries.DDMasterSeriesID
            Else
                i = 0
            End If
            Return i
        End Function
        Public Shared Sub GetAllDDMasterSeriesForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryDDMasterSeries As New ICDDMasterSeriesQuery("qryDDMasterSeries")
            Dim qryDDInstruments As New ICDDInstrumentsQuery("qryDDInstruments")
            Dim qryDDInstrumentUsed As New ICDDInstrumentsQuery("qryDDInstrumentUsed")
            Dim qryDDInstrumentUnUsed As New ICDDInstrumentsQuery("qryDDInstrumentUnUsed")

            Dim dt As New DataTable
            qryDDMasterSeries.Select(qryDDMasterSeries.DDMasterSeriesID, qryDDMasterSeries.StartFrom, qryDDMasterSeries.EndsAt, qryDDMasterSeries.PreFix)
            qryDDMasterSeries.Select(qryDDInstruments.DDInstrumentNumber.Count.Coalesce("'-'").As("TotalCount"))

            qryDDMasterSeries.Select((qryDDInstrumentUsed.[Select](qryDDInstrumentUsed.DDInstrumentNumber.Count).Where((qryDDInstrumentUsed.IsUsed = True Or qryDDInstrumentUsed.IsAssigned = True) And qryDDInstrumentUsed.DDMasterSeriesID = qryDDMasterSeries.DDMasterSeriesID).As("IsUsed")))
            qryDDMasterSeries.Select((qryDDInstrumentUnUsed.[Select](qryDDInstrumentUnUsed.DDInstrumentNumber.Count).Where((qryDDInstrumentUnUsed.IsUsed = False And qryDDInstrumentUnUsed.IsAssigned.IsNull) And qryDDInstrumentUnUsed.DDMasterSeriesID = qryDDMasterSeries.DDMasterSeriesID).As("UsedOn")))

            qryDDMasterSeries.InnerJoin(qryDDInstruments).On(qryDDMasterSeries.DDMasterSeriesID = qryDDInstruments.DDMasterSeriesID)

            qryDDMasterSeries.GroupBy(qryDDMasterSeries.DDMasterSeriesID, qryDDMasterSeries.StartFrom, qryDDMasterSeries.EndsAt, qryDDMasterSeries.PreFix)

            dt = qryDDMasterSeries.LoadDataTable
            If Not PageNumber = 0 Then
                qryDDMasterSeries.es.PageNumber = PageNumber
                qryDDMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            Else
                qryDDMasterSeries.es.PageNumber = 1
                qryDDMasterSeries.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            End If

        End Sub

        'Public Shared Sub GetAllDDMasterSeriesForRadGrid(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        '    Dim qryDDMasterSeries As New ICDDMasterSeriesQuery("qryDDMasterSeries")
        '    Dim qryDDInstruments As New ICDDInstrumentsQuery("qryDDInstruments")
        '    Dim qryDDInstrumentUsed As New ICDDInstrumentsQuery("qryDDInstrumentUsed")
        '    Dim qryDDInstrumentUnUsed As New ICDDInstrumentsQuery("qryDDInstrumentUnUsed")

        '    Dim dt As New DataTable
        '    qryDDMasterSeries.Select(qryDDMasterSeries.DDMasterSeriesID, qryDDMasterSeries.StartFrom, qryDDMasterSeries.EndsAt, qryDDMasterSeries.PreFix)
        '    qryDDMasterSeries.Select(qryDDInstruments.DDInstrumentNumber.Count.Coalesce("'-'").As("TotalCount"))

        '    qryDDMasterSeries.Select((qryDDInstrumentUsed.[Select](qryDDInstrumentUsed.DDInstrumentNumber.Count).Where((qryDDInstrumentUsed.IsUsed = True Or qryDDInstrumentUsed.IsAssigned = True) And qryDDInstrumentUsed.DDMasterSeriesID = qryDDMasterSeries.DDMasterSeriesID And qryDDInstrumentUsed.OfficeCode = qryDDInstruments.OfficeCode And qryDDInstrumentUsed.SubsetFrom = qryDDInstruments.SubsetFrom And qryDDInstrumentUsed.SubsetTo = qryDDInstruments.SubsetTo).As("IsUsed")))
        '    qryDDMasterSeries.Select((qryDDInstrumentUnUsed.[Select](qryDDInstrumentUnUsed.DDInstrumentNumber.Count).Where((qryDDInstrumentUnUsed.IsUsed = False And qryDDInstrumentUnUsed.IsAssigned.IsNull) And qryDDInstrumentUnUsed.DDMasterSeriesID = qryDDMasterSeries.DDMasterSeriesID And qryDDInstrumentUnUsed.OfficeCode = qryDDInstruments.OfficeCode And qryDDInstrumentUnUsed.SubsetFrom = qryDDInstruments.SubsetFrom And qryDDInstrumentUnUsed.SubsetTo = qryDDInstruments.SubsetTo).As("UsedOn")))

        '    qryDDMasterSeries.InnerJoin(qryDDInstruments).On(qryDDMasterSeries.DDMasterSeriesID = qryDDInstruments.DDMasterSeriesID)

        '    qryDDMasterSeries.GroupBy(qryDDMasterSeries.DDMasterSeriesID, qryDDMasterSeries.StartFrom, qryDDMasterSeries.EndsAt, qryDDMasterSeries.PreFix)

        '    dt = qryDDMasterSeries.LoadDataTable
        '    If Not PageNumber = 0 Then
        '        qryDDMasterSeries.es.PageNumber = PageNumber
        '        qryDDMasterSeries.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If

        '    Else
        '        qryDDMasterSeries.es.PageNumber = 1
        '        qryDDMasterSeries.es.PageSize = PageSize
        '        rg.DataSource = dt
        '        If DoDataBind = True Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub

        Public Shared Sub DeleteDDMasterSeries(ByVal DDMasterSereisID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objICDDMasterSeries As New ICDDMasterSeries
            Dim prevobjICDDMasterSeries As New ICDDMasterSeries
            Dim CurrentAt As String = ""

            objICDDMasterSeries.es.Connection.CommandTimeout = 3600
            prevobjICDDMasterSeries.es.Connection.CommandTimeout = 3600

            objICDDMasterSeries.LoadByPrimaryKey(DDMasterSereisID)
            prevobjICDDMasterSeries.LoadByPrimaryKey(DDMasterSereisID)

            CurrentAt = "DDMasterSeries [Code:  " & objICDDMasterSeries.DDMasterSeriesID.ToString() & " ; Prefix:  " & objICDDMasterSeries.PreFix.ToString() & " ; Start From:  " & objICDDMasterSeries.StartFrom.ToString() & " ; Ends At:  " & objICDDMasterSeries.EndsAt.ToString() & " ; IsActive:  " & objICDDMasterSeries.IsActive.ToString() & "]"

            objICDDMasterSeries.MarkAsDeleted()
            objICDDMasterSeries.Save()

            ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "DDMasterSeries", prevobjICDDMasterSeries.DDMasterSeriesID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")

        End Sub
        ''22-10-2013
        Public Shared Function GetAllDDMasterSeries() As ICDDMasterSeriesCollection
            Dim objICDDMAsterSeriesColl As New ICDDMasterSeriesCollection
            objICDDMAsterSeriesColl.es.Connection.CommandTimeout = 3600
            objICDDMAsterSeriesColl.Query.OrderBy(objICDDMAsterSeriesColl.Query.DDMasterSeriesID.Descending)
            objICDDMAsterSeriesColl.Query.Load()
            Return objICDDMAsterSeriesColl

        End Function
    End Class
End Namespace
