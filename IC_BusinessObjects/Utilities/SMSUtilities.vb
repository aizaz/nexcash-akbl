﻿Imports ICBO.IC
Imports System
Imports System.Web
Imports DotNetNuke.Web.UI.WebControls
Imports DotNetNuke.HttpModules
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Net.Mime.MediaTypeNames
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Entities.Users

Public Class SMSUtilities
    Public Shared Sub UserCreated(ByVal User As ICUser, ByVal Password As String)
        If ICNotificationManagementController.IsUserTaggedWithEvent(User.UserID.ToString, "1", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\UserCreation.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        txt = txt.Replace("[UserID]", User.UserName)
        If User.AuthenticatedVia = "Active Directory" Then
            txt = txt.Replace("[UserPassword]", "Domain Password")
        Else
            txt = txt.Replace("[UserPassword]", Password)
        End If

        txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))
        Try
            ICUtilities.SendSMS(User.CellNo.ToString, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try


    End Sub
    Public Shared Sub PasswordChange(ByVal User As ICUser, ByVal UserPassWord As String)
        If ICNotificationManagementController.IsUserTaggedWithEvent(User.UserID.ToString, "2", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\PasswordChange.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        txt = txt.Replace("[UserPassword]", UserPassWord)
        Try
            ICUtilities.SendSMS(User.CellNo, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try


    End Sub
    Public Shared Sub PasswordChangeByAdministrator(ByVal User As ICUser, ByVal Password As String)
        If ICNotificationManagementController.IsUserTaggedWithEvent(User.UserID.ToString, "3", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\PasswordChangeByAdministrator.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        txt = txt.Replace("[UserPassword]", Password)
        Try
            ICUtilities.SendSMS(User.CellNo, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try



    End Sub
    Public Shared Sub UserUpdated(ByVal User As ICUser)
        If ICNotificationManagementController.IsUserTaggedWithEvent(User.UserID.ToString, "4", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\UserUpdated.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        Try
            ICUtilities.SendSMS(User.CellNo.ToString, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try


    End Sub
    Public Shared Sub UserInactivated(ByVal User As ICUser)
        If ICNotificationManagementController.IsUserTaggedWithEvent(User.UserID.ToString, "6", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\UserInActivated.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        txt = txt.Replace("[UserID]", User.UserName)
        Try
            ICUtilities.SendSMS(User.CellNo, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try

    End Sub

    Public Shared Sub PasswordReminder(ByVal User As ICUser)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\PasswordReminder.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.DisplayName)
        'txt = txt.Replace("[UserPassword]", User.Password)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        'ICUtilities.SendSMS(MobNo, txt)

    End Sub

    'Public Shared Sub UserInactivated()
    '    Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\UserInactivated.txt")
    '    Dim txt As String

    '    txt = reader.ReadToEnd
    '    reader.Close()
    '    reader.Dispose()

    '    '  ICUtilities.SendSMS(MobNo, txt)

    'End Sub
    ''Done APN Location
    Public Shared Sub Fileuploadedbyuser(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("7", False, APNLocation, "APNatureAndLocation")

        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileuploadedbyuser.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[UserID]", UserID)
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next



    End Sub
    ''Done APN Location
    Public Shared Sub Fileprocessedbyuserandparsed(ByVal TxnCount As String, ByVal UsersID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("8", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileprocessedbyuserandparsed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[UserID]", UsersID)
        txt = txt.Replace("[Count]", TxnCount)
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Fileprocessedbyuserandfailed(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("9", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileprocessedbyuserandfailed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[UserID]", UserID)
        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsverifiedbyuserandallareverified(ByVal dtInstructionInfo As DataTable, ByVal UserID As String)

        Dim PassTxnCount As Integer = Nothing
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            Dim txt As String
            PassTxnCount = Nothing
            Dim GrpCode As String = Nothing
            GrpCode = "APNLocation ='" & drGroupRow("APNLocation") & "'"
            Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
            For Each drCount As DataRow In distinctDT2
                PassTxnCount = PassTxnCount + CInt(drCount("Count"))
            Next
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("10", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Transactionsverifiedbyuserandallareverified.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[Count]", PassTxnCount.ToString)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsverifiedbyuserandallwerenotverified(ByVal UserID As String, ByVal dtInstructionInfo As DataTable, ByVal ArrayListPassStatus As ArrayList, ByVal ArrayListFailStatus As ArrayList, ByVal InstructionIDArraList As ArrayList)


        Dim PassTxnCount As String = Nothing
        Dim FailTxnCount As String = Nothing
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            Dim txt As String = Nothing
            PassTxnCount = Nothing
            FailTxnCount = Nothing
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArraList, ArrayListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArraList, ArrayListFailStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("11", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Transactionsverifiedbyuserandallwerenotverified.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[PassCount]", PassTxnCount)
            txt = txt.Replace("[FailCount]", FailTxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub SingleInstructioninputtedbyUser(ByVal InstructionID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("12", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\SingleInstructioninputtedbyUser.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        txt = txt.Replace("[InstructionID]", InstructionID)
        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub


    Public Shared Sub FileuploadedthroughemailandParsed(ByVal UsersID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("14", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileuploadedthroughemail,parsedandverified.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next
    End Sub

    Public Shared Sub Fileuploadedthroughemailparsedandverified(ByVal User As ICUser, ByVal File As ICFiles, ByVal Instruction As ICInstruction)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Fileuploadedthroughemail,parsedandverified.txt")
        'Dim txt As String
        'Dim FileDetail As String = ""
        'FileDetail = File.FileName.ToString() & " " & Instruction.ProductTypeCode.ToString() & " " & File.TransactionCount.ToString()

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[email user] ", User.UserID)
        'txt = txt.Replace("[Count]", File.TransactionCount)
        'txt = txt.Replace("[File Details]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        'ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub Fileuploadedthroughemailandfailed(ByVal User As ICUser, ByVal File As ICFiles, ByVal FailureReasons As String)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Fileuploadedthroughemailandfailed.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[email user] ", User.UserID)
        'txt = txt.Replace("[Reasons for failure]", FailureReasons)
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '  ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub Fileuploadedthroughemailandrejected(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("16", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileuploadedthroughemailandrejected.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub

    Public Shared Sub Fileuploadedthroughemailparsedandbutsomearenotverified(ByVal User As ICUser, ByVal File As ICFiles, ByVal Email As ICEmailSettings, ByVal Instruction As ICInstruction)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Fileuploadedthroughemailparsedandbutsomearenotverified.txt")
        'Dim txt As String
        'Dim FileDetail As String = ""
        'FileDetail = File.FileName.ToString() & " " & Instruction.ProductTypeCode.ToString() & " " & File.TransactionCount.ToString()

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[email user]", Email.UserID)
        'txt = txt.Replace("[Count]", File.TransactionCount)
        'txt = txt.Replace("[File Details]", FileDetail)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        ' ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub FileuploadedthroughFTPandparsed(ByVal UsersID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("19", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\FileuploadedthroughFTPandparsedandverified.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next


    End Sub

    Public Shared Sub FileuploadedthroughFTPparsedandverified(ByVal User As ICUser, ByVal File As ICFiles, ByVal FTP As ICFTPSettings, ByVal Instruction As ICInstruction)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/FileuploadedthroughFTPandparsedandverified.txt")
        'Dim txt As String
        'Dim FileDetail As String = ""
        'FileDetail = File.FileName.ToString() & " " & Instruction.ProductTypeCode.ToString() & " " & File.TransactionCount.ToString()

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[FTP user]", FTP.UserID)
        'txt = txt.Replace("[Count]", File.TransactionCount)
        'txt = txt.Replace("[File Details]", FileDetail)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '  ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub FileuploadedthroughFTPandrejected(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("21", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\FileuploadedthroughFTPandrejected.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()

        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub

    Public Shared Sub FileuploadedthroughFTPandfailed(ByVal User As ICUser, ByVal File As ICFiles, ByVal FTP As ICFTPSettings, ByVal FailureReasons As String)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/FileuploadedthroughFTPandfailed.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[FTP user]", FTP.UserID)
        'txt = txt.Replace("[Reasons for failure]", FailureReasons)
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub FileuploadedthroughFTPparsedandbutsomearenotverified(ByVal User As ICUser, ByVal File As ICFiles, ByVal FTP As ICFTPSettings, ByVal Instruction As ICInstruction)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/FileuploadedthroughFTPparsedandbutsomearenotverified.txt")
        'Dim txt As String
        'Dim FileDetail As String = ""
        'FileDetail = File.FileName.ToString() & " " & Instruction.ProductTypeCode.ToString() & " " & File.TransactionCount.ToString()

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[FileName]", File.FileName)
        'txt = txt.Replace("[FTP user]", FTP.UserID)
        'txt = txt.Replace("[Count]", File.TransactionCount)
        'txt = txt.Replace("[File Details]", FileDetail)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '  ICUtilities.SendSMS(MobNo, txt)

    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsapprovedbyprimaryapproverandthereisnoissueinapproval(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("23", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Transactionsapprovedbyprimaryapproverandthereisnoissueinapproval.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsapprovedbyprimaryapproverandthereisanissueinapproval(ByVal dtInstructionInfo As DataTable, ByVal TaggingType As String)


        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Transactionsapprovedbyprimaryapproverandthereisanissueinapproval.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("24", False, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsapprovedbyprimaryapproverandsometransactionsarenotapproved(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal FailArrListStatus As ArrayList, ByVal ArrListInstID As ArrayList, ByVal TaggingType As String)
        Dim PassTxnCount, FailTxnCount As String



        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")


        For Each drGroupRow As DataRow In distinctDT.Rows

            Dim dtSMSInfo As New DataTable
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Transactionsapprovedbyprimaryapproverandsometransactionsarenotapproved.txt")
            PassTxnCount = Nothing
            FailTxnCount = Nothing
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrListInstID, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)
            FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrListInstID, FailArrListStatus, drGroupRow("APNLocation").ToString, TaggingType)
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("25", False, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[FailCount]", FailTxnCount)
            txt = txt.Replace("[PassCount]", PassTxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next


    End Sub
    '' Done APN Location
    Public Shared Sub TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsareavailablefordisbursement(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim TxnCount As String = Nothing
            Dim txt As String
            Dim dtSMSInfo As New DataTable
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsareavailablefordisbursement.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("26", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsaredisbursedincaseofSkipPaymentQueue(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim dtSMSInfo As New DataTable
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsaredisbursed(incaseofSkipPaymentQueue).txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("27", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalbutalltxnsarenotdisbursedincaseofSkipPaymentQueue(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList, ByVal FailArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim PassTxnCount As String = Nothing
            Dim FailTxnCount As String = Nothing
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, FailArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim dtSMSInfo As New DataTable
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalbutalltxnsarenotdisbursedincaseofSkipPaymentQueue.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("28", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[FailCount]", FailTxnCount)
            txt = txt.Replace("[PassCount]", PassTxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsapprovedbySecondaryapproverandthereisanissueinapproval(ByVal dtInstructionInfo As DataTable, ByVal TaggingType As String)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In dtInstructionInfo.Rows
            Dim dtSMSInfo As New DataTable
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsapprovedbySecondaryapproverandthereisanissueinapproval.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("29", False, drGroupRow("APNLocation").ToString, TaggingType)
            Dim txt As String = Nothing
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsapprovedbySecondaryapproverandsometransactionsarenotapproved(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList, ByVal FailArrListStatus As ArrayList, ByVal TaggingType As String)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String = Nothing
            Dim dtSMSInfo As New DataTable
            Dim PassTxnCount As String = Nothing
            Dim FailTxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsapprovedbySecondaryapproverandsometransactionsarenotapproved.txt")
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, TaggingType)
            FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, FailArrListStatus, drGroupRow("APNLocation").ToString, TaggingType)
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("30", False, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[PassCount]", PassTxnCount)
            txt = txt.Replace("[FailCount]", FailTxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Transactionsdisbursedsuccessfully(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim PassTxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Transactionsdisbursedsuccessfully.txt")
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("31", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", PassTxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub

    Public Shared Sub Sometransactionswerenotdisbursed(ByVal dtInstructionInfo As DataTable, ByVal InstIDArrList As ArrayList, ByVal PassArrListStatus As ArrayList, ByVal FailArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String = Nothing
            Dim dtSMSInfo As New DataTable
            Dim PassTxnCount As String = Nothing
            Dim FailTxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Sometransactionswerenotdisbursed.txt")
            PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, PassArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstIDArrList, FailArrListStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("32", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[PassCount]", PassTxnCount)
            txt = txt.Replace("[FailCount]", FailTxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Notransactionsweredisbursed(ByVal dtInstructionInfo As DataTable)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String = Nothing
            Dim dtSMSInfo As New DataTable
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Notransactionsweredisbursed.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("33", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub

    Public Shared Sub Unavailabilityoffundsinclientaccount(ByVal AccountNo As String, ByVal dtInstInfo As DataTable)
        Dim distinctDT As DataTable = dtInstInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Unavailabilityoffundsinclientaccount.txt")
            Dim txt As String
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("34", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[AccountNo]", AccountNo)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub

    Public Shared Sub IRISaccountverificationfailure(ByVal User As ICUser, ByVal File As ICFiles, ByVal Instruction As ICInstruction)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/IRISaccountverificationfailure.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Transaction Detail]", File.FileName) & txt.Replace("[Transaction Detail]", Instruction.ProductTypeCode) & txt.Replace("[Transaction Detail]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub
    ''Done APN Location
    Public Shared Sub Printing(ByVal dtInstructionInfo As DataTable, ByVal PassArrListInstIDs As ArrayList, ByVal PassArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String
            Dim dtSMSInfo As New DataTable
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Printing.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("38", False, drGroupRow("APNLocation").ToString, "Printing")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(PassArrListInstIDs, PassArrListStatus, drGroupRow("APNLocation").ToString, "Printing")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    Public Shared Sub TxnAvailableForBankApproval(ByVal dtInstructionInfo As DataTable, ByVal PassArrListInstIDs As ArrayList, ByVal PassArrListStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String
            Dim dtSMSInfo As New DataTable
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnPendingBankApproval.txt")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("79", False, drGroupRow("APNLocation").ToString, "Printing")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(PassArrListInstIDs, PassArrListStatus, drGroupRow("APNLocation").ToString, "Printing")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsreadyforClearing(ByVal dtInstructionInfo As DataTable, ByVal ArrLstInstID As ArrayList, ByVal ArrLstStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String = Nothing
            Dim Count As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TransactionsreadyforClearing.txt")
            Count = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrLstInstID, ArrLstStatus, drGroupRow("APNLocation").ToString, "APNature")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", Count)
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("39", False, drGroupRow("APNLocation").ToString, "APNature")
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Transactionscleared(ByVal dtInstructionInfo As DataTable, ByVal ArrLstInstID As ArrayList, ByVal ArrLstStatus As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Transactionscleared.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrLstInstID, ArrLstStatus, drGroupRow("APNLocation").ToString, "APNature")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("40", False, drGroupRow("APNLocation").ToString, "APNature")
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next

        Next
    End Sub
    ''Done APN Location
    Public Shared Sub ReprintApprovalwhentxnsaresavedforreprint(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")


        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("41", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-printApproval[whentxnsaresavedforre-print].txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    ''Done APN Location
    Public Shared Sub ReprintApproved(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)

        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("42", False, drGroupRow("APNLocation").ToString, "Printing")
            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-printApproved.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "Printing")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub ReprintRejectedbyApprover(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("43", False, drGroupRow("APNLocation").ToString, "Printing")

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-printRejectedbyApprover.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "Printing")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next


    End Sub

    Public Shared Sub Txncancelledbyapproverbeforeapproval(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Txncancelledbyapproverbeforeapproval.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '  ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub Txncancelledbyapproverafterapproval(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Txncancelledbyapproverafterapproval.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub
    ''Done APN Location
    Public Shared Sub ReissuanceApprovalForPOOrDD(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")



        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("46", False, drGroupRow("APNLocation").ToString, TaggingType)


            Dim txt As String
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-issuanceApproval[whentxnsaresavedforre-issuance].txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)


            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub ReissuanceApprovalForCheque(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList, ByVal TaggingType As String)


        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("46", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-IssuanceApprovalForCheque.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, TaggingType)
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)


            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub

    Public Shared Sub ReissuanceApproved(ByVal dtInstructionInfo As DataTable, ByVal ALInstructiondID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal TaggingType As String)


        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("47", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-issuanceApproved.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructiondID, ALPassStatus, drGroupRow("APNLocation").ToString, TaggingType, "ReIssuance")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)


            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub ReissuanceRejectedbyApprover(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/ReissuanceRejectedbyApprover.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '  ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub TxnforReissuancecancelledbyapproverbeforeapproval(ByVal dtInstructionInfo As DataTable, ByVal ALInstrID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("49", False, drGroupRow("APNLocation").ToString, TaggingType)
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstrID, ALPassStatus, drGroupRow("APNLocation").ToString, TaggingType)
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnforRe-issuancecancelledbyapproverbeforeapproval.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnforReissuancecancelledbyapproverafterapproval(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnforReissuancecancelledbyapproverafterapproval.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        ' ICUtilities.SendSMS(MobNo, txt)

    End Sub
    ''Done APN Location
    Public Shared Sub TxnmarkedasSTALE(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("51", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnmarkedasStale.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TxnRevalidation(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("52", False, drGroupRow("APNLocation").ToString, "APNature")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNature")
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnRe-validation.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub RevalidationApproved(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("53", False, drGroupRow("APNLocation").ToString, "APNature")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNature")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-validationApproved.txt")
            Dim txt As String = Nothing
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub RevalidationRejectedbyApprover(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("54", False, drGroupRow("APNLocation").ToString, "APNature")
            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Re-validationRejectedbyApprover.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNature")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub

    Public Shared Sub TxnforRevaldiationcancelledbyapproverbeforeapproval(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnforRevaldiationcancelledbyapproverbeforeapproval.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub TxnforRevalidationcancelledbyapproverafterapproval(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnforRevalidationcancelledbyapproverafterapproval.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub Stopprinting(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Stopprinting.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        '   ICUtilities.SendSMS(MobNo, txt)

    End Sub

    Public Shared Sub Stoppayment(ByVal User As ICUser, ByVal Instruction As ICInstruction, ByVal File As ICFiles)
        'Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Stoppayment.txt")
        'Dim txt As String

        'txt = reader.ReadToEnd
        'reader.Close()
        'reader.Dispose()

        'txt = txt.Replace("[UserName]", User.UserName)
        'txt = txt.Replace("[Client Name]", Instruction.ClientName) & txt.Replace("[Account No.]", Instruction.ClientAccountNo) & txt.Replace("[Product Type]", Instruction.ProductTypeCode) & txt.Replace("[No. of transactions]", File.TransactionCount)
        'txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
        'txt = txt.Replace("[MessageSignature]", ICUtilities.GetSettingValue("MessageSignature"))

        ' ICUtilities.SendSMS(MobNo, txt)

    End Sub
    ''Done APN Location
    Public Shared Sub Standinginstruction(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("59", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Standinginstruction.txt")
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub standinginstructionprocessing(ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("60", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/standinginstructionprocessing.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub Cancelledtransactions(ByVal UserID As String, ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("61", False, drGroupRow("APNLocation").ToString, TaggingType)
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, TaggingType)
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Cancelledtransactions.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TxnsendforAmendmentByVerifier(ByVal UserID As String, ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("62", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnsendforAmendmentByverifier.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    ''Done APN Location
    Public Shared Sub TxnsendforAmendmentByAprover(ByVal UserID As String, ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("62", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnsendforAmendmentByPrimaryApprover.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub
    '' Done Apn Location
    Public Shared Sub Txnamended(ByVal UserID As String, ByVal dtInstructionInfo As DataTable, ByVal ArryListPassStatus As ArrayList, ByVal ArrayListInstID As ArrayList)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("63", False, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstID, ArryListPassStatus, drGroupRow("APNLocation").ToString, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/Txnamended.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[UserID]", UserID)
            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next
    End Sub


    '2FA_-_For_login
    Public Shared Sub TwoFACodeForLogin(ByVal Code As String, ByVal UserCellNo As String, ByVal UsersID As String)
        If ICNotificationManagementController.IsUserTaggedWithEvent(UsersID.ToString, "36", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/2FA-Forlogin.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[Code]", Code)



        Try
            ICUtilities.SendSMS(UserCellNo, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try

    End Sub
    '2FA_-_For_Approval	
    Public Shared Sub TwoFACodeForApproval(ByVal Code As String, ByVal UserCellNo As String, ByVal UsersID As String)
        If ICNotificationManagementController.IsUserTaggedWithEvent(UsersID.ToString, "37", True) = False Then
            Exit Sub
        End If
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/2FA-ForApproval.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[Code]", Code)



        Try
            ICUtilities.SendSMS(UserCellNo, txt)
        Catch ex As Exception
            If ex.Message.Contains("Unable to connect") Then
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            Else
                ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                Exit Sub
            End If
        End Try


    End Sub
    ''Status Change Queue
    Public Shared Sub TxnavailableForCancellationFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("64", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForCancellationApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)
            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    Public Shared Sub TxnaCancellationApprovedFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal dtInstructionInfoChequeue As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            Dim distinctDT3 As DataTable = dtInstructionInfoChequeue.DefaultView.ToTable(True, "APNLocation")
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("65", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnCancellationApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)
            If distinctDT3.Rows.Count = 0 Then
                txt = txt.Replace("  Funds have been reversed to company account.", "")
            End If
            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    Public Shared Sub TxnaAvailableForSettlementApprovalFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("66", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForSettlementApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    Public Shared Sub TxnSettlementApprovedFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("67", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnSettlementApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    Public Shared Sub TxnavailableForReversalApprovalFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("68", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForReversalApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub
    Public Shared Sub TxnaReversalApprovedFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("69", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnReversalApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    ''24-03-2014 Jawad
    Public Shared Sub PdcNotification()
        'Dim distinctDT As DataTable = dtInstInfo.DefaultView.ToTable(True, "APNLocation")
        'For Each drGroupRow As DataRow In distinctDT.Rows
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/PdcNotification.txt")
        Dim txt As String
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetAllUserTaggedForClearing("70", False)
        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        'txt = txt.Replace("[Cheque]", ChequeNo)

        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub
    Public Shared Sub SendRINToBeneficiaryForCOTCVIASMS(ByVal objICInstruction As ICInstruction)
        Try


            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/SendRINToBeneficiaryForCOTCVIASMS.txt")
            Dim txt As String
        
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txt = txt.Replace("[RIN]", objICInstruction.Rin)


            Try
                ICUtilities.SendSMS(objICInstruction.BeneficiaryMobile.ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
            Exit Sub
        End Try
    End Sub
    'created this method for send notification by sms to use in Post Dated Cheques Module
    Public Shared Sub PdcNotificationForSms()
        'Dim distinctDT As DataTable = dtInstInfo.DefaultView.ToTable(True, "APNLocation")
        'For Each drGroupRow As DataRow In distinctDT.Rows

        Dim dtChqNos As New DataTable
        Dim ChqDetail As String = Nothing



        '' function to get cheque no's due for today clearing and fill in a data table

        dtChqNos = ICPdcController.PDCNotification
        If dtChqNos.Rows.Count > 0 Then
            '' after dtChqNos load
            For Each drChqNos As DataRow In dtChqNos.Rows
                ChqDetail += drChqNos("ChequeNo") & "<br/>"
            Next
        Else
            Exit Sub
        End If

        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/PdcNotification.txt")
        Dim txt As String
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetAllUserTaggedForClearing("70", False)
        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        'txt = txt.Replace("[Cheque]", ChequeNo)

        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub

    'created this method for send notification by sms to use in Accounts Management Module
    Public Shared Sub SendDailyAccountBalanceVIASmsToTaggedUsers(ByVal AccountNo As String, ByVal Balance As String, ByVal BranchCode As String, ByVal Currency As String)
        'Dim distinctDT As DataTable = dtInstInfo.DefaultView.ToTable(True, "APNLocation")
        'For Each drGroupRow As DataRow In distinctDT.Rows
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TaggedClientUserForAccountBalance.txt")
        Dim txt As String
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetAllTaggedClientUserForAccountBalance("71", False, AccountNo, BranchCode, Currency)
        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[Account No]", AccountNo)
        txt = txt.Replace("[Balance]", CDbl(Balance).ToString("N2"))

        For Each dr As DataRow In dtSMSInfo.Rows
            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next

    End Sub
    Public Shared Sub FileuploadedbyuserThroughEmail(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("13", False, APNLocation, "APNatureAndLocation")

        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileuploadedthroughemailandparsed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[EmailUser]", UserID)



        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next



    End Sub
    Public Shared Sub FileuploadedbyuserThroughFTP(ByVal UserID As String, ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("18", False, APNLocation, "APNatureAndLocation")

        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\FileuploadedthroughFTPandparsed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        txt = txt.Replace("[FTPUser]", UserID)


        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next



    End Sub
    Public Shared Sub Fileuploadedthroughemailandfailed(ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("15", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\Fileuploadedthroughemailandfailed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next
    End Sub
    Public Shared Sub FileuploadedthroughFTPandfailed(ByVal APNLocation As String)
        Dim dtSMSInfo As New DataTable
        dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("20", False, APNLocation, "APNatureAndLocation")
        Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates\FileuploadedthroughFTPandfailed.txt")
        Dim txt As String

        txt = reader.ReadToEnd
        reader.Close()
        reader.Dispose()
        For Each dr As DataRow In dtSMSInfo.Rows

            Try
                ICUtilities.SendSMS(dr("CellNo").ToString, txt)
            Catch ex As Exception
                If ex.Message.Contains("Unable to connect") Then
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                Else
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End If
            End Try
        Next
    End Sub

    ''Status Change Queue New Status

    Public Shared Sub TxnaAvailableForUnholdApprovalFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("75", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForUnholdApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnaAvailableForRevertApprovalFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("77", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForRevertApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnHoldApprovedFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("74", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnHoldApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnUnholdApprovedFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("76", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnUnholdApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnRevertApprovedFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("78", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnRevertApprovedFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub

    Public Shared Sub TxnaAvailableForHoldApprovalFromStatusChnageQueue(ByVal dtInstructionInfo As DataTable, ByVal PassArryListStatus As ArrayList, ByVal InstructionIDArryList As ArrayList, ByVal TaggingType As String)
        Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
        For Each drGroupRow As DataRow In distinctDT.Rows
            Dim dtSMSInfo As New DataTable
            dtSMSInfo = ICNotificationManagementController.GetUsersForSMSorEMail2("73", False, drGroupRow("APNLocation").ToString, TaggingType)

            Dim txt As String = Nothing
            Dim TxnCount As String = Nothing
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "SMSTemplates/TxnAvailableForHoldApprovalFromStatusChnageQueue.txt")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(InstructionIDArryList, PassArryListStatus, drGroupRow("APNLocation").ToString, TaggingType)

            txt = txt.Replace("[Count]", TxnCount)

            For Each dr As DataRow In dtSMSInfo.Rows
                Try
                    ICUtilities.SendSMS(dr("CellNo").ToString, txt)
                Catch ex As Exception
                    If ex.Message.Contains("Unable to connect") Then
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    Else
                        ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                        Exit Sub
                    End If
                End Try
            Next
        Next

    End Sub



End Class
