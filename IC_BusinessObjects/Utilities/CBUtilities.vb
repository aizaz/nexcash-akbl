﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Security.Cryptography
Imports System.Net.Sockets
Imports System.Text
Imports ICBO
Imports ICBO.IC
Imports System
Imports Oracle.DataAccess.Client
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Telerik.Web.UI
'Imports PhoenixInterfaceComponent.Transactions



Public Class CBUtilities



    Public Enum dataelementtype
        A = 1
        N = 2
        AN = 3
        ANS = 4
        X = 5
        IMD = 6
        NR = 7
        NS = 8
    End Enum
    Public Enum AccountType
        DefaultAccount = 0
        Checking
        RB
        GL
    End Enum
    Public Enum ResponseCode
        Processed_Ok
        Limit_Exceeded
        Account_Not_Exist
        Account_Inactive
        Low_Balance
        PAN_Missing
        IMD_Missing
        Incorrect_Card_Data
        Card_Record_Not_Found
        Field_Error
        Duplicate_Transaction
        Bad_Transaction_Code
        Card_Status_Error
        System_Error
        Warm_Card
        Hot_Card
        Bad_Card_Status
        Unknown_Authorization_Mode
        Error_in_Transaction_Date
        Error_in_Currency_Code
        Transaction_Not_Allowed_on_this_IMD
        Transaction_Not_Allowed_on_this_Account
        Bad_Card_Cycle_Date
        Bad_Card_Cycle_Length
        Incorrect_PIN
        No_Account_Linked
        Original_Transaction_Not_Found
        Original_Transaction_Not_Authorized
        Original_Transaction_Already_Reversed
        Acquirer_Reversal
        Transaction_Code_Mismatched
        Bad_Transaction_Type
        Expiry_Date_Mismatched
        Track_2_Data_Mismatched
        Error_in_Currency_Conversion
        Bad_Amount
        Host_Status_Unknown
        Host_Not_Processing
        Host_Busy
        Host_Link_Down
        Transaction_Sent_to_Host
        Transaction_Timed_Out
        Transaction_Rejected_by_Host
        PIN_Retries_Exhausted
        HSM_Not_Responding
        Host_offline
        Destination_Not_Found
        Destination_Not_Registered
        Cash_Transaction_Not_Allowed
        No_Transaction_Not_Allowed
        Invalid_Account_Status
        Invalid_To_Account
        Refused_IMD
        Currency_Not_Allowed
        Transaction_Source_Not_Allowed
        Unknown_Transaction_Source
        Manual_Entry_Not_Allowed
        Refer_to_Issuer
        Invalid_Merchant
        Honor_With_ID
        Message_Format_Error
        Security_Violation
        Transaction_Frequency_Exceeded
        Incorrect_PIN_Length
        Error_in_Cash_Retract
        Faulty_Dispense
        Short_Dispense
        Customer_Record_Not_Found
        Issuer_Reversal
        Account_Locked
        PIN_Expired
        Permission_Denied
        Transaction_Rejected
        Original_Transaction_Rejected
        Bad_Expiry_Date
        Original_Amount_Incorrect
        Original_Data_Element_Mismatch
    End Enum
    Private Enum networkmessagetype
        'Network messages
        LOGON = 801
        LOGOFF = 802
        ECHOTEST = 803
    End Enum
    Private Enum transactionmessagetype
        'transaction messages
        ACCOUNT_STATUS
        FUNDS_TRANSFER
        TITLE_FETCH
        INTERBANK_FUNDS_TRANSFER
        BALANCE_INQUIRY
        FUNDS_TRANSFER_REVERSAL
        TRANSACTION_STATUS
        EOD_STATUS
        IBFTTRANSACTION_STATUS
        IBFTTITLE_FETCH
        BILL_INQUIRY
        BILL_PAYMENT
    End Enum
    Public Enum networkmessagefieldtype
        msgprotocol
        version
        fieldinerror
        msgtype
        transdttime
        dctype
        dcid
        cusid
        trancode
        trandt
        trantime
        retref
        cuspin
        chspecificdatafield
        chprivatedata
        authresid
        rescode
    End Enum
    Public Enum transactionmessagefieldtype
        msgprotocol
        version
        fieldinerror
        msgtype
        transdttime
        dctype
        dcid
        cusid
        trancode
        trandt
        trantime
        retref
        cuspin
        chspecificdatafield
        chprivatedata
        authresid
        rescode
        accountbankIMD
        accountnumber
        accounttype
        accountcurrency
        titleofaccount
        accountstatus
        availablebalance
        actualbalance
        withdrawallimit
        availablewithdrawallimit
        draftlimit
        limitexpirydate
        currencyname
        currencymnemonic
        currencydecimalpoints
        fromaccountnumber
        fromaccounttype
        fromaccountcurrency
        toaccountnumber
        toaccounttype
        toaccountcurrency
        transactionamount
        transactioncurrency
        frombankimd
        tobankimd
    End Enum


    Public Enum TransactionStatus

        Successfull
        UnSuccessfull
        Insufficient_Balance_In_From_Account
        From_Account_Not_Active
        To_Account_Not_Active
        Error_In_Fetching_Account_Balance
        Cross_Currency
        EOD
    End Enum

    Public Enum TransferType

        RB_TO_RB
        RB_TO_GL
        GL_TO_RB
        GL_TO_GL



    End Enum


    Private Shared objPhoenixComponentTXn As New PhoenixInterfaceComponent.Transactions





    ''Done 08-02-2014
    'Public Shared Function BalanceInquiry(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal Unit_ID As String) As Double
    '    Return 100000
    'End Function
    Public Shared Function BalanceInquiry(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal Unit_ID As String) As Double
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return 100000.0
        End If
        ''Return 0

        Dim BasicBankAccountNo As String = Nothing
        Dim StrResponseCode As String = Nothing
        Dim StrResponseMessage As String = Nothing
        Dim logID As Integer = 0
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        Dim objICSwtich As New IntelligenesSwitch.SwitchClient
        Try
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            logID = GetLogId(EntityType, RelatedId, Unit_ID & AccountNumber, AccountType.ToString, "", "", "", "", 0, transactionmessagetype.BALANCE_INQUIRY)
            ''Get BBAN 
            BasicBankAccountNo = GetBasicBankAccountNoFromIBAN(AccountNumber)
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            StrResponseCode = objICSwtich.FnBalanceEnquiry(Unit_ID, BasicBankAccountNo, AccountType, RelatedId, EntityType, "1", Auth_UserID, Auth_Password, Auth_IP)

            ''Verify Response Code else generate error message
            If StrResponseCode.ToString.Split("-")(0) = "00" Then
                UpdateCBLog(logID, "Complete", "OK " & CDbl(StrResponseCode.ToString.Split("-")(1)))
                Return CDbl(StrResponseCode.ToString.Split("-")(1))
            Else
                'StrResponseMessage = GetErrorMessageForTransactionFromResponseCode(StrResponseCode.ToString.Split("-")(0), "Business")
                Throw New Exception(StrResponseCode)
            End If
        Catch ex As Exception

            UpdateCBLog(logID, "Complete", "Error : " & ex.Message.ToString)

            Return -1
        End Try
    End Function

    'Public Shared Function TitleFetch(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String) As String
    '    Return "Test Account" & ";" & "0001" & ";" & "PKR"
    'End Function

    Public Shared Function TitleFetch(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String) As String
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return "Test Account" & ";" & "0001" & ";" & "PKR"
        End If
        Dim BasicBankAccountNo As String = Nothing
        Dim objICSwitch As New IntelligenesSwitch.SwitchClient
        Dim StrResponseCode As String = Nothing
        Dim StrResponseMessage As String = Nothing
        Dim AccounTtile As String = Nothing
        Dim logID As Integer = 0
        Dim Response As String = ""
        Dim FinalResponse As String() = Nothing
        Dim FinalAccountTitle As String = Nothing
        Dim FinalBranchCode As String = Nothing
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        Try
            logID = GetLogId(EntityType, RelatedId, AccountNumber, AccountType.ToString, "", "", "", "", 0, transactionmessagetype.TITLE_FETCH)


            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)


            BasicBankAccountNo = GetBasicBankAccountNoFromIBAN(AccountNumber)


            Response = objICSwitch.TitleFetchWithBranchCode(RelatedId, EntityType, "", BasicBankAccountNo, "1", AccountType, Auth_UserID, Auth_Password, Auth_IP)
            ' Verify Response Code else generate error message
            If Response.ToString.Split("-")(0) = "00" Then

                FinalResponse = Response.ToString.Split("|")
                If FinalResponse(0).ToString.Contains("00-") Then
                    FinalAccountTitle = FinalResponse(0).ToString.Replace("00-", "")
                Else
                    FinalAccountTitle = FinalResponse(0).ToString
                End If
                FinalBranchCode = FinalResponse(1).ToString
                AccounTtile = RemoveSpecialCharactersFromString(FinalAccountTitle) & ";" & FinalBranchCode & ";" & "PKR"
                UpdateCBLog(logID, "Complete", "OK " & AccounTtile)
                Return AccounTtile
            Else
                'StrResponseMessage = GetErrorMessageForTransactionFromResponseCode(Response.ToString.Split("-")(0), "Business")
                Throw New Exception(Response)
            End If
        Catch ex As Exception

            UpdateCBLog(logID, "Complete", "Error : " & ex.Message.ToString)
            Throw ex

        End Try

    End Function

    Public Shared Sub GetAuthenticationCredentials(ByRef Auth_ID As String, ByRef Auth_PassWord As String, ByRef Auth_IP As String)
        Auth_ID = Nothing
        Auth_IP = Nothing
        Auth_PassWord = Nothing
        Auth_ID = ConfigurationManager.AppSettings("NexCashAuthenticatedUserId").ToString
        Auth_IP = ConfigurationManager.AppSettings("NexCashAllowedIPAdresses").ToString
        Auth_PassWord = ConfigurationManager.AppSettings("NexCashAuthenticatedPassword").ToString
    End Sub
    '' IBFT Customization Javed 10-02-2014
    ''10-02-2014
    Public Shared Function IBFTTitleFetch(ByVal ToBankIMD As String, ByVal ToAccountNumber As String, ByVal AccountType As AccountType, ByVal entitytype As String, _
                                             ByVal relatedid As String, _
                                             ByVal sAccountType As String, _
                                             ByVal sAccountCurrency As String, _
                                             ByVal dTransactionAmount As Double, _
                                             ByVal sPIN As String, _
                                             ByRef sRRN As String, _
                                             ByVal CustomerID As String, _
                                              ByVal ToBankCode As String, _
                                             ByVal ToBankName As String, _
                                             ByVal FromAccountNo As String, _
                                             ByVal FromBankIMD As String) As String

        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return "IBFT Test Account"
        End If
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        Dim objICSwitch As New IntelligenesSwitch.SwitchClient
        Dim logID As Integer = 0
        Dim Title As String = ""


        Dim msgStr As String = ""
        Dim response As String = ""
        Dim _Balance As String = ""
        sRRN = Nothing
        Try

            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)

            'Get Log ID
            logID = GetLogId(entitytype, relatedid, FromAccountNo, AccountType.ToString, ToAccountNumber, Nothing, FromBankIMD, ToBankIMD, dTransactionAmount, transactionmessagetype.IBFTTITLE_FETCH)
            'Get Log ID for Retreival Response No
            sRRN = GetStanNo(logID)
            'Check IBAN

            'IBFT Title Fetch IBAN

            msgStr = objICSwitch.TitleFetchEuronet(ToBankIMD, ToAccountNumber, ToBankCode, ToBankCode, FromAccountNo, FromBankIMD, dTransactionAmount, sRRN, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)
            Title = msgStr.ToString.Trim
            'Check Reponse Code

            Return Title
        Catch ex As Exception
            UpdateCBLog(logID, "Error", "IBFT Title fetch of Account fails due to  " & ex.Message)
            Throw ex
        End Try
        'Return "Test Account"
    End Function
    ''' <summary>
    ''' Transfer funds to other bank account
    ''' </summary>
    ''' <param name="frombankIMD"></param>
    ''' request originating bank's IMD
    ''' <param name="fromaccountnumber"></param>
    ''' 
    ''' <param name="fromaccounttype"></param>
    ''' Account types i-e loan, current and Saving. Assigned codes will be used
    ''' 10,20,30
    ''' <param name="tobankIMD"></param>
    ''' <param name="toaccountnumber"></param>
    ''' <param name="toaccounttype"></param>
    ''' <param name="transactionamount"></param>
    ''' <param name="entitytype"></param>
    ''' <param name="relatedid"></param>
    ''' <param name="sCustomerID"></param>
    ''' <param name="sFromAccType"></param>
    ''' <param name="sFromAccountCurrency"></param>
    ''' <param name="sToAccountCurrency"></param>
    ''' <param name="sToAccType"></param>
    ''' <param name="sTransactionCurrency"></param>
    ''' <param name="sPIN"></param>
    ''' <param name="sFee"></param>
    ''' <param name="sParticular"></param>
    ''' <param name="sRRN"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InterbankFundsTransfer(ByVal frombankIMD As String, ByVal fromaccountnumber As String, ByVal fromaccounttype As AccountType, _
                                                  ByVal tobankIMD As String, ByVal toaccountnumber As String, ByVal toaccounttype As AccountType, _
                                                  ByVal transactionamount As String, ByVal entitytype As String, ByVal relatedid As String, _
                                                   ByVal sFromAccType As String, ByVal sFromAccountCurrency As String,
                                                   ByVal sToAccType As String, ByVal sPIN As String, ByVal sFee As String, ByVal TxnParticualr As String,
                                                  ByRef sRRN As String, ByVal CustomerID As String, ByVal ToBankName As String, ByVal ToBankCode As String) As Boolean
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return True
        End If
        'Return True
        Dim logID As Integer = 0
        Dim objInstruction As New ICInstruction
        Dim objSwitch As New IntelligenesSwitch.SwitchClient
        Dim StrAudtTrail As String = Nothing
        Dim response As String = ""
        Dim StanNo As String = Nothing
        Dim Result As Boolean = False
        Dim ResponseMessage As String = Nothing
        Dim Auth_UserID, Auth_IP, Auth_Password, StanAction As String
        Try
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            logID = GetLogId(entitytype, relatedid, fromaccountnumber, fromaccounttype.ToString, toaccountnumber, toaccounttype.ToString, frombankIMD, tobankIMD, transactionamount, transactionmessagetype.INTERBANK_FUNDS_TRANSFER)
            StanNo = Nothing
            StanNo = GetStanNo(logID)
            If entitytype.ToString() = "Instruction" Then
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(relatedid)

                'If ICInstructionController.GetAllInstructionByStatus("15", relatedid) = True Then
                '    StrAudtTrail = Nothing
                '    StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & relatedid & " ] due to instruction is already marked as Settled."
                '    ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", relatedid, Nothing, Nothing, "SKIP")
                '    Return True
                '    Exit Function
                'End If


                'OriginalTxnDateTime = CDate(objInstruction.CreateDate).ToString("yyyyMMddhhmmss")
            End If

            'Update Instruction Status as Pending CB
            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
            objInstruction = New ICInstruction
            objInstruction.LoadByPrimaryKey(relatedid)

            '' Do IBFT Transaction
            response = objSwitch.IBFTEuronet(tobankIMD, toaccountnumber, ToBankName, ToBankCode, fromaccountnumber, frombankIMD, transactionamount, StanNo, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)
            '  response = "OK"
            If response = "OK" Then
                UpdateCBLog(logID, "Complete", "OK")
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(relatedid)
                'Update Instruction Status to Last Status
                objInstruction.IBFTStan = StanNo
                StanAction = "Instruction with ID [ " & relatedid & " ] assigned STAN [ " & StanNo & " ]."
                ICInstructionController.UpDateInstruction(objInstruction, Nothing, Nothing, StanAction, "UPDATE")

                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(relatedid)
                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                Result = True
            Else
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(relatedid)
                UpdateCBLog(logID, "Complete", "Unable to Transfer Funds IBFT due to " & response.ToString)
                StrAudtTrail = Nothing
                StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in funds transfer."
                ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
                'Update Instruction Status to Last Status
                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                Result = False
            End If

            Return Result
        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message)
            objInstruction = New ICInstruction
            objInstruction.LoadByPrimaryKey(relatedid)
            'Update Instruction Status to Last Status
            StrAudtTrail = Nothing
            StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to " & ex.Message.ToString
            ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
            Return False
        End Try

    End Function



    ''Before Change API for IBFT '00' in Account Type
    'Public Shared Function InterbankFundsTransfer(ByVal frombankIMD As String, ByVal fromaccountnumber As String, ByVal fromaccounttype As AccountType, _
    '                                              ByVal tobankIMD As String, ByVal toaccountnumber As String, ByVal toaccounttype As AccountType, _
    '                                              ByVal transactionamount As String, ByVal entitytype As String, ByVal relatedid As String, _
    '                                               ByVal sFromAccType As String, ByVal sFromAccountCurrency As String,
    '                                               ByVal sToAccType As String, ByVal sPIN As String, ByVal sFee As String, ByVal TxnParticualr As String,
    '                                              ByRef sRRN As String, ByVal CustomerID As String) As Boolean


    '    Dim logID As Integer = 0
    '    Dim objInstruction As New ICInstruction
    '    Dim StrAudtTrail As String = Nothing
    '    Dim response As String = ""

    '    Dim OriginalTxnDateTime As String = Nothing
    '    Dim Result As Boolean = False
    '    Dim ResponseMessage As String = Nothing

    '    Try

    '        logID = GetLogId(entitytype, relatedid, fromaccountnumber, fromaccounttype.ToString, toaccountnumber, toaccounttype.ToString, frombankIMD, tobankIMD, transactionamount, transactionmessagetype.INTERBANK_FUNDS_TRANSFER)
    '        sRRN = Nothing
    '        sRRN = GetStanNo() & logID
    '        If entitytype.ToString() = "Instruction" Then
    '            objInstruction = New ICInstruction
    '            objInstruction.LoadByPrimaryKey(relatedid)
    '            'OriginalTxnDateTime = CDate(objInstruction.CreateDate).ToString("yyyyMMddhhmmss")
    '        End If
    '        OriginalTxnDateTime = CDate(Date.Now).ToString("yyyyMMddhhmmss")
    '        'Update Instruction Status as Pending CB
    '        ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
    '        objInstruction = New ICInstruction
    '        objInstruction.LoadByPrimaryKey(relatedid)
    '        If toaccountnumber.Length < 24 Then
    '            '' Do IBFT Transaction
    '            response = objPhoenixComponentTXn.DoInterBankFundsTransfer(CustomerID, OriginalTxnDateTime, frombankIMD, fromaccountnumber, sFromAccType, _
    '                                         "586", tobankIMD, toaccountnumber, sToAccType, "586", CDbl(objInstruction.Amount), _
    '                                         "586", _
    '                                         sPIN, sFee, TxnParticualr, sRRN)
    '            response = response.Substring(0, 2)
    '            If response = "00" Then
    '                UpdateCBLog(logID, "Complete", "OK")
    '                objInstruction = New ICInstruction
    '                objInstruction.LoadByPrimaryKey(relatedid)
    '                'Update Instruction Status to Last Status
    '                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                Result = True

    '            ElseIf response = "TO" Then
    '                UpdateCBLog(logID, "Error", "TO")
    '                If SingleTransactionStatusInquiry(CustomerID, OriginalTxnDateTime, sRRN, relatedid, entitytype, fromaccountnumber, toaccountnumber, frombankIMD, tobankIMD, fromaccounttype, toaccounttype) = "OK" Then
    '                    UpdateCBLog(logID, "Complete", "OK")
    '                    objInstruction = New ICInstruction
    '                    objInstruction.LoadByPrimaryKey(relatedid)
    '                    'Update Instruction Status to Last Status
    '                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                    Result = True
    '                Else
    '                    objInstruction = New ICInstruction
    '                    objInstruction.LoadByPrimaryKey(relatedid)
    '                    UpdateCBLog(logID, "Complete", "Unable to Transfer Funds IBFT due to " & GetResponseMessageFromResponseCode(response).ToString())
    '                    StrAudtTrail = Nothing
    '                    StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in funds transfer."
    '                    ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
    '                    'Update Instruction Status to Last Status
    '                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                    Result = False
    '                End If
    '            Else
    '                objInstruction = New ICInstruction
    '                objInstruction.LoadByPrimaryKey(relatedid)
    '                UpdateCBLog(logID, "Complete", "Unable to Transfer Funds IBFT due to " & GetResponseMessageFromResponseCode(response).ToString())
    '                StrAudtTrail = Nothing
    '                StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in funds transfer."
    '                ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
    '                'Update Instruction Status to Last Status
    '                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                Result = False
    '            End If
    '        Else
    '            response = objPhoenixComponentTXn.DoInterBankFundsTransfer(CustomerID, OriginalTxnDateTime, frombankIMD, fromaccountnumber, sFromAccType, _
    '                                     "586", tobankIMD, toaccountnumber, CDbl(objInstruction.Amount), "586", _
    '                                     sPIN, sFee, TxnParticualr, sRRN)
    '            response = response.Substring(0, 2)
    '            If response = "00" Then
    '                UpdateCBLog(logID, "Complete", "OK")
    '                objInstruction = New ICInstruction
    '                objInstruction.LoadByPrimaryKey(relatedid)
    '                'Update Instruction Status to Last Status
    '                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                Result = True
    '            ElseIf response = "TO" Then
    '                UpdateCBLog(logID, "Error", "TO")
    '                If SingleTransactionStatusInquiry(CustomerID, OriginalTxnDateTime, sRRN, relatedid, entitytype, fromaccountnumber, toaccountnumber, frombankIMD, tobankIMD, fromaccounttype, toaccounttype) = "OK" Then
    '                    UpdateCBLog(logID, "Complete", "OK")
    '                    objInstruction = New ICInstruction
    '                    objInstruction.LoadByPrimaryKey(relatedid)
    '                    'Update Instruction Status to Last Status
    '                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                    Result = True
    '                Else
    '                    objInstruction = New ICInstruction
    '                    objInstruction.LoadByPrimaryKey(relatedid)
    '                    UpdateCBLog(logID, "Complete", "Unable to Transfer Funds IBFT due to " & GetResponseMessageFromResponseCode(response).ToString())
    '                    StrAudtTrail = Nothing
    '                    StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in funds transfer."
    '                    ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
    '                    'Update Instruction Status to Last Status
    '                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                    Result = False
    '                End If
    '            Else
    '                objInstruction = New ICInstruction
    '                objInstruction.LoadByPrimaryKey(relatedid)
    '                ResponseMessage = GetResponseMessageFromResponseCode(response).ToString()
    '                UpdateCBLog(logID, "Complete", "Unable to Transfer Funds IBFT due to " & ResponseMessage)
    '                StrAudtTrail = Nothing
    '                StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in funds transfer due to " & ResponseMessage
    '                ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
    '                'Update Instruction Status to Last Status
    '                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '                Result = False
    '            End If
    '        End If
    '        Return Result
    '    Catch ex As Exception
    '        UpdateCBLog(logID, "Error", ex.Message)
    '        objInstruction = New ICInstruction
    '        objInstruction.LoadByPrimaryKey(relatedid)
    '        'Update Instruction Status to Last Status
    '        StrAudtTrail = Nothing
    '        StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to " & ex.Message.ToString
    '        ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
    '        ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
    '        Return False
    '    End Try
    '    ' Return True
    'End Function
    ''Al Baraka Bank Integration
    ''Done 08-Feb-2014
    'Public Shared Function AccountStatus(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal StatusInquiryType As String, ByVal InquiryLocation As String, ByVal UnitID As String) As String
    '    Return "Active"
    'End Function
    'Public Shared Function AccountStatusForClearing(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal VerificationType As String, ByVal UnitID As String) As String
    '    Return "Active"
    'End Function
    Public Shared Function AccountStatusForClearing(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal VerificationType As String, ByVal UnitID As String) As String
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return "Active"
        End If
        Dim objICSwitch As New IntelligenesSwitch.SwitchClient
        Dim StrResponseMessage As String = Nothing
        Dim Response As String = Nothing
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        Dim logID As Integer = 0
        Try
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            logID = GetLogId(EntityType, RelatedId, UnitID & AccountNumber, AccountType.ToString, "", "", "", "", 0, transactionmessagetype.ACCOUNT_STATUS)
            Response = objICSwitch.GetAccountStatus(UnitID, AccountNumber, AccountType, RelatedId, EntityType, "1", Auth_UserID, Auth_Password, Auth_IP)
            ''Verify Response Code else generate error message
            If Response.ToString.Split("-")(0) = "00" Then
                If Response.ToString.Split("-")(1) = "00" Then
                    UpdateCBLog(logID, "Complete", "OK [Normal]")
                    Return "Active"
                Else
                    UpdateCBLog(logID, "Complete", "OK " & Response.ToString.Split("-")(1).ToString)
                    Return Response.ToString.Split("-")(1).ToString
                End If
            Else
                StrResponseMessage = Response
                'StrResponseMessage = GetErrorMessageForTransactionFromResponseCode(Response.ToString.Split("-")(1), "Business")
                Throw New Exception(StrResponseMessage)
            End If
        Catch ex As Exception

            UpdateCBLog(logID, "Complete", "Error : " & ex.Message.ToString)
            Throw ex
        End Try
    End Function

    Public Shared Function AccountStatus(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String, ByVal StatusInquiryType As String, ByVal InquiryLocation As String, ByVal UnitID As String) As String
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return "Active"
        End If
        Dim objICSwitch As New IntelligenesSwitch.SwitchClient
        Dim StrResponseMessage As String = Nothing
        Dim Response As String = Nothing
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        Dim logID As Integer = 0
        Try
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            logID = GetLogId(EntityType, RelatedId, UnitID & AccountNumber, AccountType.ToString, "", "", "", "", 0, transactionmessagetype.ACCOUNT_STATUS)
            Response = objICSwitch.GetAccountStatus(UnitID, AccountNumber, AccountType, RelatedId, EntityType, "1", Auth_UserID, Auth_Password, Auth_IP)
            ''Verify Response Code else generate error message
            If Response.ToString.Split("-")(0) = "00" Then
                If Response.ToString.Split("-")(1) = "00" Then
                    UpdateCBLog(logID, "Complete", "OK [Normal]")
                    Return "Active"
                Else
                    UpdateCBLog(logID, "Complete", "OK " & Response.ToString.Split("-")(1).ToString)
                    Return Response.ToString.Split("-")(1).ToString & "-" & FRC_AccountStatus(Response.ToString.Split("-")(1).ToString)
                End If
            Else
                StrResponseMessage = Response
                'StrResponseMessage = GetErrorMessageForTransactionFromResponseCode(Response.ToString.Split("-")(1), "Business")
                Throw New Exception(StrResponseMessage)
            End If
        Catch ex As Exception

            UpdateCBLog(logID, "Complete", "Error : " & ex.Message.ToString)
            Throw ex
        End Try
    End Function
    Private Shared Function FRC_AccountStatus(ByVal responsecode As String) As String

        Select Case responsecode
            Case "00"
                Return "NORMAL ACCOUNT"
            Case "01"
                Return "DORMANT"
            Case "02"
                Return "INOPERATIVE ACCOUNT"
            Case "03"
                Return "CLOSED"
            Case "04"
                Return "DECEASED"
            Case "05"
                Return "IN LIQUIDATION"
            Case "06"
                Return "POSTING BLOCK"
            Case "07"
                Return "BLOCKED: UNDER LITIGATION"
            Case "30"
                Return "CREDIT BLOCK"
            Case "31"
                Return "CREDIT BLOCK DORMANT"
            Case "32"
                Return "CREDIT BLOCK INOPERATIVE"
            Case "40"
                Return "DEBIT BLOCK"
            Case "41"
                Return "DEBIT BLOCK DORMANT"
            Case "42"
                Return "DEBIT BLOCK INOPERATIVE"
            Case "43"
                Return "NON-CHECKING ACCOUNT"
            Case "44"
                Return "MATURED MINOR ACCOUNTS"
            Case Else
                Return "INVALID ACCOUNT STATUS"

        End Select
    End Function
    Public Shared Function SingleTransactionStatusInquiry(ByVal CustomerID As String, ByVal sOriginalTxnDateTime As String, ByVal sOriginalTxnRRN As String,
                                                          ByVal RelatedID As String, ByVal entitytype As String,
                                                          ByVal FromAccountNo As String, ByVal ToAccountNo As String, ByVal FromBnakIMD As String,
                                                          ByVal ToBankIMD As String, ByVal FromAccoutType As AccountType, ByVal ToAccoutType As AccountType) As String


        Dim logID As Integer = 0
        Dim msgStr As String = ""
        Dim response As String = ""
        Dim _Balance As String = ""
        Dim TxnStatusRRNo As String = Nothing
        Try
            logID = GetLogId(entitytype, RelatedID, FromAccountNo, FromAccoutType, ToAccountNo, ToAccoutType, FromBnakIMD, ToBankIMD, 0, transactionmessagetype.IBFTTRANSACTION_STATUS)
            TxnStatusRRNo = GetStanNo(logID) & logID.ToString
            response = objPhoenixComponentTXn.DoTransactionStatusInquiry(CustomerID, RelatedID, TxnStatusRRNo, sOriginalTxnDateTime, sOriginalTxnRRN, "10")
            response = response.Substring(0, 2)
            If response = "00" Then
                UpdateCBLog(logID, "Complete", "OK")
                _Balance = "OK"
                Return _Balance
            Else
                Dim responsemessage As String = GetResponseMessageFromResponseCode(response).ToString()
                Throw New System.Exception(responsemessage)
            End If
        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message)
            Throw ex
        End Try
    End Function
    Private Shared Function GetResponseMessageFromResponseCode(ByVal responsecode As String) As String

        Select Case responsecode
            Case "TO"
                Return "Time Out"
            Case "00"
                Return "Processed_Ok"
            Case "01"
                Return "Limit_Exceeded"
            Case "02"
                Return "Account_Not_Exist"
            Case "03"
                Return "Account_Inactive"
            Case "04"
                Return "Low_Balance"
            Case "05"
                Return "PAN_Missing"
            Case "06"
                Return "IMD_Missing"
            Case "07"
                Return "Incorrect_Card_Data"
            Case "08"
                Return "Card_Record_Not_Found"
            Case "09"
                Return "Field_Error"
            Case "10"
                Return "Duplicate_Transaction"
            Case "11"
                Return "Bad_Transaction_Code"
            Case "12"
                Return "Card_Status_Error"
            Case "13"
                Return "System_Error"
            Case "14"
                Return "Warm_Card"
            Case "15"
                Return "Hot_Card"
            Case "16"
                Return "Bad_Card_Status"
            Case "17"
                Return "Unknown_Authorization_Mode"
            Case "18"
                Return "Error_in_Transaction_Date"
            Case "19"
                Return "Error_in_Currency_Code"
            Case "20"
                Return "Transaction_Not_Allowed_on_this_IMD"
            Case "21"
                Return "Transaction_Not_Allowed_on_this_Account"
            Case "22"
                Return "Bad_Card_Cycle_Date"
            Case "23"
                Return "Bad_Card_Cycle_Length"
            Case "24"
                Return "Incorrect_PIN"
            Case "25"
                Return "System_Error"
            Case "26"
                Return "System_Error"
            Case "27"
                Return "System_Error"
            Case "28"
                Return "No_Account_Linked"
            Case "29"
                Return "System_Error"
            Case "30"
                Return "Original_Transaction_Not_Found"
            Case "31"
                Return "System_Error"
            Case "32"
                Return "System_Error"
            Case "33"
                Return "System_Error"
            Case "34"
                Return "Original_Transaction_Not_Authorized"
            Case "35"
                Return "Original_Transaction_Already_Reversed"
            Case "36"
                Return "Acquirer_Reversal"
            Case "37"
                Return "System_Error"
            Case "38"
                Return "Transaction_Code_Mismatched"
            Case "39"
                Return "Bad_Transaction_Type"
            Case "40"
                Return "System_Error"
            Case "41"
                Return "Expiry_Date_Mismatched"
            Case "45"
                Return "Track_2_Data_Mismatched"
            Case "46"
                Return "System_Error"
            Case "47"
                Return "Error_in_Currency_Conversion"
            Case "48"
                Return "Bad_Amount"
            Case "49"
                Return "System_Error"
            Case "50"
                Return "Host_Status_Unknown"
            Case "51"
                Return "Host_Not_Processing"
            Case "52"
                Return "Host_Busy"
            Case "53"
                Return "Host_Busy"
            Case "54"
                Return "Host_Busy"
            Case "55"
                Return "Host_Link_Down"
            Case "56"
                Return "Transaction_Sent_to_Host"
            Case "57"
                Return "System_Error"
            Case "58"
                Return "Transaction_Timed_Out"
            Case "59"
                Return "Transaction_Rejected_by_Host"
            Case "60"
                Return "PIN_Retries_Exhausted"
            Case "61"
                Return "HSM_Not_Responding"
            Case "62"
                Return "Host_offline"
            Case "63"
                Return "Destination_Not_Found"
            Case "64"
                Return "Destination_Not_Registered"
            Case "65"
                Return "Cash_Transaction_Not_Allowed"
            Case "66"
                Return "No_Transaction_Not_Allowed"
            Case "67"
                Return "Invalid_Account_Status"
            Case "68"
                Return "Invalid_To_Account"
            Case "69"
                Return "System_Error"
            Case "70"
                Return "Refused_IMD"
            Case "71"
                Return "System_Error"
            Case "72"
                Return "Currency_Not_Allowed"
            Case "73"
                Return "System_Error"
            Case "74"
                Return "Transaction_Source_Not_Allowed"
            Case "75"
                Return "Unknown_Transaction_Source"
            Case "76"
                Return "Manual_Entry_Not_Allowed"
            Case "77"
                Return "Refer_to_Issuer"
            Case "78"
                Return "Invalid_Merchant"
            Case "79"
                Return "Honor_With_ID"
            Case "80"
                Return "Message_Format_Error"
            Case "81"
                Return "Security_Violation"
            Case "82"
                Return "System_Error"
            Case "83"
                Return "System_Error"
            Case "84"
                Return "System_Error"
            Case "85"
                Return "Transaction_Frequency_Exceeded"
            Case "86"
                Return "Incorrect_PIN_Length"
            Case "87"
                Return "Error_in_Cash_Retract"
            Case "88"
                Return "Faulty_Dispense"
            Case "89"
                Return "Short_Dispense"
            Case "90"
                Return "Customer_Record_Not_Found"
            Case "91"
                Return "Issuer_Reversal"
            Case "92"
                Return "Account_Locked"
            Case "93"
                Return "PIN_Expired"
            Case "94"
                Return "Permission_Denied"
            Case "95"
                Return "Transaction_Rejected"
            Case "96"
                Return "Original_Transaction_Rejected"
            Case "97"
                Return "Bad_Expiry_Date"
            Case "98"
                Return "Original_Amount_Incorrect"
            Case "99"
                Return "Original_Data_Element_Mismatch"
            Case Else
                Return "Invalid_Response_Code"
        End Select




    End Function
    'Private Shared Function GetResponseMessageFromResponseCode(ByVal responsecode As String) As String

    '    Select Case responsecode
    '        Case "TO"
    '            Return "Time Out"
    '        Case "00"
    '            Return CBUtilities.ResponseCode.Processed_Ok
    '        Case "01"
    '            Return CBUtilities.ResponseCode.Limit_Exceeded
    '        Case "02"
    '            Return CBUtilities.ResponseCode.Account_Not_Exist
    '        Case "03"
    '            Return CBUtilities.ResponseCode.Account_Inactive
    '        Case "04"
    '            Return CBUtilities.ResponseCode.Low_Balance
    '        Case "05"
    '            Return CBUtilities.ResponseCode.PAN_Missing
    '        Case "06"
    '            Return CBUtilities.ResponseCode.IMD_Missing
    '        Case "07"
    '            Return CBUtilities.ResponseCode.Incorrect_Card_Data
    '        Case "08"
    '            Return CBUtilities.ResponseCode.Card_Record_Not_Found
    '        Case "09"
    '            Return CBUtilities.ResponseCode.Field_Error
    '        Case "10"
    '            Return CBUtilities.ResponseCode.Duplicate_Transaction
    '        Case "11"
    '            Return CBUtilities.ResponseCode.Bad_Transaction_Code
    '        Case "12"
    '            Return CBUtilities.ResponseCode.Card_Status_Error
    '        Case "13"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "14"
    '            Return CBUtilities.ResponseCode.Warm_Card
    '        Case "15"
    '            Return CBUtilities.ResponseCode.Hot_Card
    '        Case "16"
    '            Return CBUtilities.ResponseCode.Bad_Card_Status
    '        Case "17"
    '            Return CBUtilities.ResponseCode.Unknown_Authorization_Mode
    '        Case "18"
    '            Return CBUtilities.ResponseCode.Error_in_Transaction_Date
    '        Case "19"
    '            Return CBUtilities.ResponseCode.Error_in_Currency_Code
    '        Case "20"
    '            Return CBUtilities.ResponseCode.Transaction_Not_Allowed_on_this_IMD
    '        Case "21"
    '            Return CBUtilities.ResponseCode.Transaction_Not_Allowed_on_this_Account
    '        Case "22"
    '            Return CBUtilities.ResponseCode.Bad_Card_Cycle_Date
    '        Case "23"
    '            Return CBUtilities.ResponseCode.Bad_Card_Cycle_Length
    '        Case "24"
    '            Return CBUtilities.ResponseCode.Incorrect_PIN
    '        Case "25"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "26"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "27"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "28"
    '            Return CBUtilities.ResponseCode.No_Account_Linked
    '        Case "29"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "30"
    '            Return CBUtilities.ResponseCode.Original_Transaction_Not_Found
    '        Case "31"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "32"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "33"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "34"
    '            Return CBUtilities.ResponseCode.Original_Transaction_Not_Authorized
    '        Case "35"
    '            Return CBUtilities.ResponseCode.Original_Transaction_Already_Reversed
    '        Case "36"
    '            Return CBUtilities.ResponseCode.Acquirer_Reversal
    '        Case "37"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "38"
    '            Return CBUtilities.ResponseCode.Transaction_Code_Mismatched
    '        Case "39"
    '            Return CBUtilities.ResponseCode.Bad_Transaction_Type
    '        Case "40"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "41"
    '            Return CBUtilities.ResponseCode.Expiry_Date_Mismatched
    '        Case "45"
    '            Return CBUtilities.ResponseCode.Track_2_Data_Mismatched
    '        Case "46"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "47"
    '            Return CBUtilities.ResponseCode.Error_in_Currency_Conversion
    '        Case "48"
    '            Return CBUtilities.ResponseCode.Bad_Amount
    '        Case "49"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "50"
    '            Return CBUtilities.ResponseCode.Host_Status_Unknown
    '        Case "51"
    '            Return CBUtilities.ResponseCode.Host_Not_Processing
    '        Case "52"
    '            Return CBUtilities.ResponseCode.Host_Busy
    '        Case "53"
    '            Return CBUtilities.ResponseCode.Host_Busy
    '        Case "54"
    '            Return CBUtilities.ResponseCode.Host_Busy
    '        Case "55"
    '            Return CBUtilities.ResponseCode.Host_Link_Down
    '        Case "56"
    '            Return CBUtilities.ResponseCode.Transaction_Sent_to_Host
    '        Case "57"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "58"
    '            Return CBUtilities.ResponseCode.Transaction_Timed_Out
    '        Case "59"
    '            Return CBUtilities.ResponseCode.Transaction_Rejected_by_Host
    '        Case "60"
    '            Return CBUtilities.ResponseCode.PIN_Retries_Exhausted
    '        Case "61"
    '            Return CBUtilities.ResponseCode.HSM_Not_Responding
    '        Case "62"
    '            Return CBUtilities.ResponseCode.Host_offline
    '        Case "63"
    '            Return CBUtilities.ResponseCode.Destination_Not_Found
    '        Case "64"
    '            Return CBUtilities.ResponseCode.Destination_Not_Registered
    '        Case "65"
    '            Return CBUtilities.ResponseCode.Cash_Transaction_Not_Allowed
    '        Case "66"
    '            Return CBUtilities.ResponseCode.No_Transaction_Not_Allowed
    '        Case "67"
    '            Return CBUtilities.ResponseCode.Invalid_Account_Status
    '        Case "68"
    '            Return CBUtilities.ResponseCode.Invalid_To_Account
    '        Case "69"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "70"
    '            Return CBUtilities.ResponseCode.Refused_IMD
    '        Case "71"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "72"
    '            Return CBUtilities.ResponseCode.Currency_Not_Allowed
    '        Case "73"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "74"
    '            Return CBUtilities.ResponseCode.Transaction_Source_Not_Allowed
    '        Case "75"
    '            Return CBUtilities.ResponseCode.Unknown_Transaction_Source
    '        Case "76"
    '            Return CBUtilities.ResponseCode.Manual_Entry_Not_Allowed
    '        Case "77"
    '            Return CBUtilities.ResponseCode.Refer_to_Issuer
    '        Case "78"
    '            Return CBUtilities.ResponseCode.Invalid_Merchant
    '        Case "79"
    '            Return CBUtilities.ResponseCode.Honor_With_ID
    '        Case "80"
    '            Return CBUtilities.ResponseCode.Message_Format_Error
    '        Case "81"
    '            Return CBUtilities.ResponseCode.Security_Violation
    '        Case "82"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "83"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "84"
    '            Return CBUtilities.ResponseCode.System_Error
    '        Case "85"
    '            Return CBUtilities.ResponseCode.Transaction_Frequency_Exceeded
    '        Case "86"
    '            Return CBUtilities.ResponseCode.Incorrect_PIN_Length
    '        Case "87"
    '            Return CBUtilities.ResponseCode.Error_in_Cash_Retract
    '        Case "88"
    '            Return CBUtilities.ResponseCode.Faulty_Dispense
    '        Case "89"
    '            Return CBUtilities.ResponseCode.Short_Dispense
    '        Case "90"
    '            Return CBUtilities.ResponseCode.Customer_Record_Not_Found
    '        Case "91"
    '            Return CBUtilities.ResponseCode.Issuer_Reversal
    '        Case "92"
    '            Return CBUtilities.ResponseCode.Account_Locked
    '        Case "93"
    '            Return CBUtilities.ResponseCode.PIN_Expired
    '        Case "94"
    '            Return CBUtilities.ResponseCode.Permission_Denied
    '        Case "95"
    '            Return CBUtilities.ResponseCode.Transaction_Rejected
    '        Case "96"
    '            Return CBUtilities.ResponseCode.Original_Transaction_Rejected
    '        Case "97"
    '            Return CBUtilities.ResponseCode.Bad_Expiry_Date
    '        Case "98"
    '            Return CBUtilities.ResponseCode.Original_Amount_Incorrect
    '        Case "99"
    '            Return CBUtilities.ResponseCode.Original_Data_Element_Mismatch
    '    End Select




    'End Function
    Public Shared Function GetTitleFetch(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String) As String
        'Return "Umer Nawab"
        Dim logID As Integer = 0
        Try

            logID = GetLogId(EntityType, RelatedId, AccountNumber, CBUtilities.AccountType.RB, "", "", "", "", 0, transactionmessagetype.TITLE_FETCH)

            Dim constring As String = System.Configuration.ConfigurationManager.ConnectionStrings("Oracle").ConnectionString
            ' INPUT Parameters
            Dim inAccountNumber As New Oracle.DataAccess.Client.OracleParameter("PaCCT_NO", OracleDbType.Varchar2, ParameterDirection.Input)
            inAccountNumber.Value = AccountNumber.ToString()
            inAccountNumber.Size = 200
            ' OUTPUT Parameters
            Dim outAccountTitle As New Oracle.DataAccess.Client.OracleParameter("rAccountTile", OracleDbType.Varchar2, ParameterDirection.Output)
            outAccountTitle.Size = 200
            outAccountTitle.Value = Nothing
            Dim outBranchCode As New Oracle.DataAccess.Client.OracleParameter("rAccountBranch", OracleDbType.Varchar2, ParameterDirection.Output)
            outBranchCode.Size = 200
            outBranchCode.Value = Nothing
            Dim outCurrency As New Oracle.DataAccess.Client.OracleParameter("rAccountCurrency", OracleDbType.Varchar2, ParameterDirection.Output)
            outCurrency.Size = 200
            outCurrency.Value = Nothing
            Dim outStatus As New Oracle.DataAccess.Client.OracleParameter("rAccountStatus", OracleDbType.Varchar2, ParameterDirection.Output)
            outStatus.Size = 200
            outStatus.Value = Nothing
            Dim outBalance As New Oracle.DataAccess.Client.OracleParameter("rAccountAvailableBalance", OracleDbType.Decimal, ParameterDirection.Output)
            outBalance.Value = Nothing
            Dim outStopWithdrawl As New Oracle.DataAccess.Client.OracleParameter("rStopWithdrawalRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopWithdrawl.Size = 100
            'outStopWithdrawl.Value = "FALSE"
            Dim outFundHeld As New Oracle.DataAccess.Client.OracleParameter("rFundHeldRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outFundHeld.Size = 100
            'outFundHeld.Value = "FALSE"
            Dim outStopDeposit As New Oracle.DataAccess.Client.OracleParameter("rStopDepositRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopDeposit.Size = 100
            'outStopDeposit.Value = "FALSE"
            Dim outStopAll As New Oracle.DataAccess.Client.OracleParameter("rStopAll", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopAll.Size = 100
            'outStopAll.Value = "FALSE"
            Dim outErrorNo As New Oracle.DataAccess.Client.OracleParameter("perror_no", OracleDbType.Double, ParameterDirection.Output)
            outErrorNo.Value = Nothing
            Dim outErrorMsg As New Oracle.DataAccess.Client.OracleParameter("pmessage", OracleDbType.Varchar2, ParameterDirection.Output)
            outErrorMsg.Size = 200
            outErrorMsg.Value = Nothing

            Dim con As New OracleConnection(constring)
            Dim objCmd As New OracleCommand()
            objCmd.Connection = con
            objCmd.CommandText = "TM_GET_ACCOUNT"
            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.Parameters.Add(inAccountNumber)
            objCmd.Parameters.Add(outAccountTitle)
            objCmd.Parameters.Add(outBranchCode)
            objCmd.Parameters.Add(outCurrency)
            objCmd.Parameters.Add(outStatus)
            objCmd.Parameters.Add(outBalance)
            objCmd.Parameters.Add(outStopWithdrawl)
            objCmd.Parameters.Add(outFundHeld)
            objCmd.Parameters.Add(outStopDeposit)
            objCmd.Parameters.Add(outStopAll)
            objCmd.Parameters.Add(outErrorNo)
            objCmd.Parameters.Add(outErrorMsg)
            objCmd.CommandTimeout = 36000
            con.Open()
            objCmd.ExecuteNonQuery()
            If Not outErrorMsg.Value Is DBNull.Value Then
                If outErrorMsg.Value.ToString().Contains("no data found") Then
                    UpdateCBLog(logID, "Complete", "no data found")
                    Return "-;-;-"
                Else
                    Dim retAccountTitle As String = CType(outAccountTitle.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retBranchCode As String = CType(outBranchCode.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retCurrency As String = CType(outCurrency.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStatus As String = CType(outStatus.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retBalance As Double = CType(outBalance.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                    Dim retStopWithDrawl As String = CType(outStopWithdrawl.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retFundHeld As String = CType(outFundHeld.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStopDeposit As String = CType(outStopDeposit.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStopAll As String = CType(outStopAll.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retErrorNO As Integer = CType(outErrorNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                    Dim retErrorMsg As String = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value

                    If retErrorNO = 0 Then
                        UpdateCBLog(logID, "Complete", "OK [" & retAccountTitle.ToString() & ";" & retBranchCode.ToString() & ";" & retCurrency.ToString() & "]")
                        Return retAccountTitle.ToString() & ";" & retBranchCode.ToString() & ";" & retCurrency.ToString()
                    Else
                        UpdateCBLog(logID, "Error", retErrorMsg)
                        Throw New Exception(retErrorMsg)
                    End If
                End If
            Else
                UpdateCBLog(logID, "Complete", "Invalid Account Number")
                Return "-;-;-"
            End If
        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message)
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function GetBalanceInquiry(ByVal AccountNumber As String, ByVal AccountType As AccountType, ByVal EntityType As String, ByVal RelatedId As String) As Double
        Dim logID As Integer = 0
        Try

            logID = GetLogId(EntityType, RelatedId, AccountNumber, CBUtilities.AccountType.RB, "", "", "", "", 0, transactionmessagetype.BALANCE_INQUIRY)

            Dim constring As String = System.Configuration.ConfigurationManager.ConnectionStrings("Oracle").ConnectionString
            ' INPUT Parameters
            Dim inAccountNumber As New Oracle.DataAccess.Client.OracleParameter("PaCCT_NO", OracleDbType.Varchar2, ParameterDirection.Input)
            inAccountNumber.Value = AccountNumber.ToString()
            inAccountNumber.Size = 200
            ' OUTPUT Parameters
            Dim outAccountTitle As New Oracle.DataAccess.Client.OracleParameter("rAccountTile", OracleDbType.Varchar2, ParameterDirection.Output)
            outAccountTitle.Size = 200
            outAccountTitle.Value = Nothing
            Dim outBranchCode As New Oracle.DataAccess.Client.OracleParameter("rAccountBranch", OracleDbType.Varchar2, ParameterDirection.Output)
            outBranchCode.Size = 200
            outBranchCode.Value = Nothing
            Dim outCurrency As New Oracle.DataAccess.Client.OracleParameter("rAccountCurrency", OracleDbType.Varchar2, ParameterDirection.Output)
            outCurrency.Size = 200
            outCurrency.Value = Nothing
            Dim outStatus As New Oracle.DataAccess.Client.OracleParameter("rAccountStatus", OracleDbType.Varchar2, ParameterDirection.Output)
            outStatus.Size = 200
            outStatus.Value = Nothing
            Dim outBalance As New Oracle.DataAccess.Client.OracleParameter("rAccountAvailableBalance", OracleDbType.Decimal, ParameterDirection.Output)
            outBalance.Value = Nothing
            Dim outStopWithdrawl As New Oracle.DataAccess.Client.OracleParameter("rStopWithdrawalRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopWithdrawl.Size = 100
            'outStopWithdrawl.Value = "FALSE"
            Dim outFundHeld As New Oracle.DataAccess.Client.OracleParameter("rFundHeldRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outFundHeld.Size = 100
            'outFundHeld.Value = "FALSE"
            Dim outStopDeposit As New Oracle.DataAccess.Client.OracleParameter("rStopDepositRestraint", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopDeposit.Size = 100
            'outStopDeposit.Value = "FALSE"
            Dim outStopAll As New Oracle.DataAccess.Client.OracleParameter("rStopAll", OracleDbType.Varchar2, ParameterDirection.Output)
            outStopAll.Size = 100
            'outStopAll.Value = "FALSE"
            Dim outErrorNo As New Oracle.DataAccess.Client.OracleParameter("perror_no", OracleDbType.Double, ParameterDirection.Output)
            outErrorNo.Value = Nothing
            Dim outErrorMsg As New Oracle.DataAccess.Client.OracleParameter("pmessage", OracleDbType.Varchar2, ParameterDirection.Output)
            outErrorMsg.Size = 200
            outErrorMsg.Value = Nothing

            Dim con As New OracleConnection(constring)
            Dim objCmd As New OracleCommand()
            objCmd.Connection = con
            objCmd.CommandText = "TM_GET_ACCOUNT"
            objCmd.CommandType = CommandType.StoredProcedure
            objCmd.Parameters.Add(inAccountNumber)
            objCmd.Parameters.Add(outAccountTitle)
            objCmd.Parameters.Add(outBranchCode)
            objCmd.Parameters.Add(outCurrency)
            objCmd.Parameters.Add(outStatus)
            objCmd.Parameters.Add(outBalance)
            objCmd.Parameters.Add(outStopWithdrawl)
            objCmd.Parameters.Add(outFundHeld)
            objCmd.Parameters.Add(outStopDeposit)
            objCmd.Parameters.Add(outStopAll)
            objCmd.Parameters.Add(outErrorNo)
            objCmd.Parameters.Add(outErrorMsg)
            objCmd.CommandTimeout = 36000
            con.Open()
            objCmd.ExecuteNonQuery()
            If Not outErrorMsg.Value Is DBNull.Value Then
                If outErrorMsg.Value.ToString().Contains("no data found") Then
                    UpdateCBLog(logID, "Complete", "no data found")
                    Return 0
                Else
                    Dim retAccountTitle As String = CType(outAccountTitle.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retBranchCode As String = CType(outBranchCode.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retCurrency As String = CType(outCurrency.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStatus As String = CType(outStatus.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retBalance As Double = CType(outBalance.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                    Dim retStopWithDrawl As String = CType(outStopWithdrawl.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retFundHeld As String = CType(outFundHeld.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStopDeposit As String = CType(outStopDeposit.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retStopAll As String = CType(outStopAll.Value, Oracle.DataAccess.Types.OracleString).Value
                    Dim retErrorNO As Integer = CType(outErrorNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                    Dim retErrorMsg As String = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value

                    If retErrorNO = 0 Then
                        UpdateCBLog(logID, "Complete", "OK [" & retBalance & "]")
                        Return (retBalance * (-1))
                    Else
                        UpdateCBLog(logID, "Error", retErrorMsg)
                        Throw New Exception(retErrorMsg)
                    End If
                End If
            Else
                UpdateCBLog(logID, "Complete", "Invalid Account Number")
                Return 0
            End If
        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message)
            Throw New Exception(ex.Message)
        End Try
    End Function
    Private Shared Function GetLogId(ByVal entitytype As String, ByVal relatedid As String, ByVal fromaccountnumber As String, ByVal fromaccounttype As String, ByVal toaccountnumber As String, ByVal toaccounttype As String, ByVal frombankimd As String, ByVal tobankimd As String, ByVal transactionamount As String, ByVal messagetype As CBUtilities.transactionmessagetype) As Integer
        Dim log As New ICCBLog
        log.es.Connection.CommandTimeout = 3600

        log.LogDateTime = Now
        log.EntityType = entitytype
        log.RelatedId = relatedid
        log.Status = "Pending"

        If Not fromaccountnumber = "" Then
            log.Fromaccountnumber = fromaccountnumber
        End If
        If Not fromaccounttype = "" Then
            log.Fromaccounttype = fromaccounttype
        End If
        If Not toaccountnumber = "" Then
            log.Toaccountnumber = toaccountnumber
        End If
        If Not toaccounttype = "" Then
            log.Toaccounttype = toaccounttype
        End If
        If Not frombankimd = "" Then
            log.FrombankIMD = frombankimd
        End If
        If Not tobankimd = "" Then
            log.TobankIMD = tobankimd
        End If
        If Not transactionamount = "" Then
            log.Transactionamount = transactionamount
        End If
        log.MessageType = messagetype.ToString
        log.Save()
        Return log.LogId
    End Function
    Private Shared Sub UpdateCBLog(ByVal logid As Integer, ByVal status As String, ByVal errordesc As String)
        Dim log As New ICCBLog
        log.es.Connection.CommandTimeout = 3600
        log.LoadByPrimaryKey(logid)
        If Not status = "" Then
            log.Status = status
        End If
        If Not errordesc = "" Then
            log.ErrorDesc = errordesc
        End If
        log.Save()
    End Sub
    Public Shared Function TM_GET_ACCOUNT_OPENING_BALANCE(ByVal AccountNumber As String, ByVal Dated As Date) As Double
        Try
            Dim constring As String = System.Configuration.ConfigurationManager.ConnectionStrings("Oracle").ConnectionString
            ' RETURN VALUE Parameter
            Dim retValue As New Oracle.DataAccess.Client.OracleParameter("Return_Value", OracleDbType.Decimal, ParameterDirection.ReturnValue)

            ' INPUT Parameters
            Dim inAccountNumber As New Oracle.DataAccess.Client.OracleParameter("(pAccountNumber", OracleDbType.Varchar2, ParameterDirection.Input)
            inAccountNumber.Value = AccountNumber.ToString()
            inAccountNumber.Size = 200

            Dim inDate As New Oracle.DataAccess.Client.OracleParameter("pDate", OracleDbType.Date, ParameterDirection.Input)
            inDate.Value = Dated






            Dim con As New OracleConnection(constring)
            Dim objCmd As New OracleCommand()
            objCmd.Connection = con
            objCmd.CommandText = "TM_GET_ACCOUNT_OPENING_BALANCE"
            ' objCmd.BindByName = True
            objCmd.CommandType = CommandType.StoredProcedure

            objCmd.Parameters.Add(retValue)
            objCmd.Parameters.Add(inAccountNumber)
            objCmd.Parameters.Add(inDate)

            objCmd.CommandTimeout = 36000
            con.Open()
            objCmd.ExecuteNonQuery()
            If Not retValue.Value Is DBNull.Value Then
                Dim retReturnValue As Double = CType(retValue.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                Return retReturnValue
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    '' Javed Work For IBAN Validation
    Public Shared Function ValidateIBAN(ByVal IBAN As String) As Boolean
        Try

            Dim IBANNOSpaces As String = IBAN.Replace(" ", "")
            IBANNOSpaces = IBANNOSpaces.Replace("/", "")
            IBANNOSpaces = IBANNOSpaces.Replace(":", "")
            IBANNOSpaces = IBANNOSpaces.Replace("\", "")
            IBANNOSpaces = IBANNOSpaces.Replace("+", "")
            IBANNOSpaces = IBANNOSpaces.Replace("-", "")
            IBANNOSpaces = IBANNOSpaces.Replace("*", "")
            Dim IBANLength As Integer = IBANNOSpaces.Length
            Dim IBANpart1 As String = IBANNOSpaces.Substring(0, 4)
            Dim IBANpart2 As String = IBANNOSpaces.Substring(4, IBANLength - 4)
            Dim IBANCombined As String = IBANpart2.ToLower & IBANpart1.ToLower
            Dim TotalIntegralValueOfIBAN As String = ""
            For Each Letter As Char In IBANCombined
                If Not IsNumeric(Letter) Then
                    Dim LetterValue As String
                    Select Case Letter
                        Case "a"
                            LetterValue = 10
                        Case "b"
                            LetterValue = 11
                        Case "c"
                            LetterValue = 12
                        Case "d"
                            LetterValue = 13
                        Case "e"
                            LetterValue = 14
                        Case "f"
                            LetterValue = 15
                        Case "g"
                            LetterValue = 16
                        Case "h"
                            LetterValue = 17
                        Case "i"
                            LetterValue = 18
                        Case "j"
                            LetterValue = 19
                        Case "k"
                            LetterValue = 20
                        Case "l"
                            LetterValue = 21
                        Case "m"
                            LetterValue = 22
                        Case "n"
                            LetterValue = 23
                        Case "o"
                            LetterValue = 24
                        Case "p"
                            LetterValue = 25
                        Case "q"
                            LetterValue = 26
                        Case "r"
                            LetterValue = 27
                        Case "s"
                            LetterValue = 28
                        Case "t"
                            LetterValue = 29
                        Case "u"
                            LetterValue = 30
                        Case "v"
                            LetterValue = 31
                        Case "w"
                            LetterValue = 32
                        Case "x"
                            LetterValue = 33
                        Case "y"
                            LetterValue = 34
                        Case "z"
                            LetterValue = 35
                        Case " "
                            LetterValue = ""
                        Case Else
                            LetterValue = ""
                    End Select
                    TotalIntegralValueOfIBAN &= LetterValue
                Else
                    TotalIntegralValueOfIBAN &= Letter
                End If
            Next
            Dim TotalIBANLength As Integer = CInt(TotalIntegralValueOfIBAN.Length)
            Dim ModOfObtainedSubstring As Integer = Nothing
            Dim RemaindIBAN As String = Nothing
            Dim ObtainedIBAN As Integer = 1
            Dim IterationCounter As Integer = Nothing
            Dim FinalObtainedMod As Integer = Nothing
            If TotalIntegralValueOfIBAN.Length > 9 Then
                Do While Not ObtainedIBAN = Nothing
                    If Not TotalIBANLength = 0 Then
                        If IterationCounter = 0 Then
                            FinalObtainedMod = Nothing
                            ObtainedIBAN = 0
                            ObtainedIBAN = CInt(TotalIntegralValueOfIBAN.Substring(0, 9))
                            ModOfObtainedSubstring = CStr(ObtainedIBAN Mod 97)
                            RemaindIBAN = CStr(ModOfObtainedSubstring) & "" & TotalIntegralValueOfIBAN.Substring(9, TotalIntegralValueOfIBAN.Length - 9)
                            FinalObtainedMod = ModOfObtainedSubstring
                            TotalIBANLength = RemaindIBAN.Length
                            IterationCounter = 1
                            ModOfObtainedSubstring = Nothing
                            Continue Do
                        Else
                            ObtainedIBAN = 0
                            If RemaindIBAN.Length > 7 Then
                                FinalObtainedMod = Nothing
                                ObtainedIBAN = CInt(RemaindIBAN.Substring(0, 7))
                                ModOfObtainedSubstring = CStr(ObtainedIBAN Mod 97)
                                RemaindIBAN = CStr(ModOfObtainedSubstring) & "" & RemaindIBAN.Substring(7, RemaindIBAN.Length - 7)
                                FinalObtainedMod = ModOfObtainedSubstring
                                TotalIBANLength = RemaindIBAN.Length
                            Else
                                FinalObtainedMod = Nothing
                                ObtainedIBAN = CInt(RemaindIBAN.Substring(0, RemaindIBAN.Length))
                                ModOfObtainedSubstring = CStr(ObtainedIBAN Mod 97)
                                RemaindIBAN = CStr(ModOfObtainedSubstring) & "" & RemaindIBAN.Substring(0, RemaindIBAN.Length)
                                TotalIBANLength = RemaindIBAN.Length
                                FinalObtainedMod = ModOfObtainedSubstring
                                ObtainedIBAN = Nothing
                                Exit Do
                            End If
                            Continue Do
                        End If
                    Else
                        Exit Do
                    End If
                Loop
            Else
                FinalObtainedMod = CStr(CInt(TotalIntegralValueOfIBAN) Mod 97)
            End If
            'Dim IBANTotal As UInteger = CUInt(TotalIntegralValueOfIBAN)

            If FinalObtainedMod = 1 Then
                'OK

                Return True
            Else
                'NOT OK
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function GetBBANFromIBAN(ByVal IBAN As String) As String
        Try
            Dim IBANNOSpaces As String = IBAN.Replace(" ", "")
            IBANNOSpaces = IBANNOSpaces.Replace("/", "")
            IBANNOSpaces = IBANNOSpaces.Replace(":", "")
            IBANNOSpaces = IBANNOSpaces.Replace("\", "")
            IBANNOSpaces = IBANNOSpaces.Replace("+", "")
            IBANNOSpaces = IBANNOSpaces.Replace("-", "")
            IBANNOSpaces = IBANNOSpaces.Replace("*", "")
            Dim IBANLength As Integer = IBANNOSpaces.Length
            Dim IBANpart2 As String = IBANNOSpaces.Substring(8, IBANLength - 8)
            Dim SymOrTcss As String = IBANpart2.Substring(0, 2)
            If SymOrTcss.ToString() = "00" Then
                IBANpart2 = IBANpart2.Remove(0, 2)
            End If
            Return IBANpart2
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function IsNormalAccountNoOrIBAN(ByVal AccountNo As String) As Boolean
        Try
            Dim IBANNOSpaces As String = AccountNo.Replace(" ", "")
            IBANNOSpaces = IBANNOSpaces.Replace("/", "")
            IBANNOSpaces = IBANNOSpaces.Replace(":", "")
            IBANNOSpaces = IBANNOSpaces.Replace("\", "")
            IBANNOSpaces = IBANNOSpaces.Replace("+", "")
            IBANNOSpaces = IBANNOSpaces.Replace("-", "")
            IBANNOSpaces = IBANNOSpaces.Replace("*", "")
            Dim IBANLength As Integer = IBANNOSpaces.Length
            Dim IBANpart1 As String = IBANNOSpaces.Substring(0, 2)
            If IBANpart1.ToLower.ToString = "pk" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Public Shared Function FundsTransfer(ByRef StanNo As String, ByVal FromAcccountNumber As String, ByVal FromBranchCode As String,
    '                                   ByVal FromCurrency As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String,
    '                                   ByVal FromAccountType As AccountType, ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String,
    '                                   ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType,
    '                                   ByVal TransactionAmount As String, ByVal EntityType As String, ByVal RelatedID As String, ByVal FinalStatus As String,
    '                                   ByVal FailureStatus As String, ByVal UserID As String, Optional ByVal objInstructionColl As ICInstructionCollection = Nothing,
    '                                   Optional ByVal ProductTypeCode As String = Nothing, Optional ByVal DebitGLCode As String = Nothing,
    '                                   Optional ByVal CreditGLCode As String = Nothing, Optional ByVal ClearingType As String = Nothing,
    '                                   Optional ByVal ClearVIA As String = Nothing) As Boolean
    '    Return True
    'End Function
    Public Shared Function FundsTransfer(ByRef StanNo As String, ByVal FromAcccountNumber As String, ByVal FromBranchCode As String,
                                        ByVal FromCurrency As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String,
                                        ByVal FromAccountType As AccountType, ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String,
                                        ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType,
                                        ByVal TransactionAmount As String, ByVal EntityType As String, ByVal RelatedID As String, ByVal FinalStatus As String,
                                        ByVal FailureStatus As String, ByVal UserID As String, Optional ByVal objInstructionColl As ICInstructionCollection = Nothing,
                                        Optional ByVal ProductTypeCode As String = Nothing, Optional ByVal DebitGLCode As String = Nothing,
                                        Optional ByVal CreditGLCode As String = Nothing, Optional ByVal ClearingType As String = Nothing,
                                        Optional ByVal ClearVIA As String = Nothing) As Boolean
        Dim logID As Integer = 0
        Dim objInstruction As New ICInstruction
        Dim objProductType As New ICProductType
        Dim objICEODPending As ICPendingEODInstructions
        Dim CurrentStatus As String = ""

        Dim SkipReason As String = Nothing
        Dim BasicBankAccountNo As String = Nothing
        Dim BasicBankAccountNo2 As String = Nothing
        Dim OriginalFromAccountNo, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode As String
        Dim OriginalToAccountNo, ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode As String
        Dim DrGlCode, CrGlCode, InterBranchTaggedGL As String
        Dim OriginatingIDNo As Integer = 0
        Dim InstrumentDate As Date = Now.Date
        Dim PendingEODStatus As String = "47"
        Dim IsSecondLeg As Boolean = False
        Dim RespondingTranType As String = Nothing
        Dim OriginatingTranType As String = Nothing

        Try

            '' Transfer Type and Cross Currency Verification
            Dim TransferType As String = ""
            If EntityType.ToString() = "Instruction" Then
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                CurrentStatus = objInstruction.Status.ToString()
                If FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "RB" Then
                    TransferType = "RB to RB"
                    If FromCurrency.ToString().Trim().ToLower() <> ToCurrency.ToString().Trim().ToLower() Then
                        objInstruction = New ICInstruction
                        objInstruction.LoadByPrimaryKey(RelatedID)
                        SkipReason = Nothing
                        SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped since Cross currency transaction is not allowed."
                        objInstruction.SkipReason = SkipReason
                        ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                        Return False
                    End If
                ElseIf FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "GL" Then
                    TransferType = "RB to GL"
                    If FromCurrency.ToString().Trim().ToLower() <> ToCurrency.ToString().Trim().ToLower() Then
                        objInstruction = New ICInstruction
                        objInstruction.LoadByPrimaryKey(RelatedID)
                        SkipReason = Nothing
                        SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped since Cross currency transaction is not allowed."
                        objInstruction.SkipReason = SkipReason
                        ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                        Return False
                    End If
                ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "RB" Then
                    TransferType = "GL to RB"
                    If FromCurrency.ToString().Trim().ToLower() <> ToCurrency.ToString().Trim().ToLower() Then
                        objInstruction = New ICInstruction
                        objInstruction.LoadByPrimaryKey(RelatedID)
                        SkipReason = Nothing
                        SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped since Cross currency transaction is not allowed."
                        objInstruction.SkipReason = SkipReason
                        ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                        Return False
                    End If
                ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "GL" Then
                    TransferType = "GL to GL"
                    If FromCurrency.ToString().Trim().ToLower() <> ToCurrency.ToString().Trim().ToLower() Then
                        objInstruction = New ICInstruction
                        objInstruction.LoadByPrimaryKey(RelatedID)
                        SkipReason = Nothing
                        SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped since Cross currency transaction is not allowed."
                        objInstruction.SkipReason = SkipReason
                        ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                        Return False
                    End If
                End If
            Else
                If FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "RB" Then
                    TransferType = "RB to RB"

                ElseIf FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "GL" Then
                    TransferType = "RB to GL"

                ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "RB" Then
                    TransferType = "GL to RB"

                ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "GL" Then
                    TransferType = "GL to GL"

                End If
            End If

            '' Branch Code,Product Code and Scheme Code from RB Account
            '' Default Values in case of GL
            If FromAccountType.ToString = "RB" Then
                If FromAcccountNumber.Length > 15 Then
                    OriginalFromAccountNo = FromAcccountNumber
                    FromAccountBrCode = FromBranchCode
                    FromAccountProductCode = ""
                    FromAccountSchemeCode = ""
                Else
                    OriginalFromAccountNo = FromAcccountNumber
                    FromAccountBrCode = FromBranchCode
                    FromAccountProductCode = ""
                    FromAccountSchemeCode = ""
                End If
            Else
                OriginalFromAccountNo = FromAcccountNumber
                FromAccountBrCode = FromBranchCode
                FromAccountProductCode = ""
                FromAccountSchemeCode = ""
            End If
            If ToAccountType.ToString = "RB" Then
                If ToAccountNumber.Length > 15 Then
                    OriginalToAccountNo = ToAccountNumber
                    ToAccountBrCode = ToBranchCode
                    ToAccountProductCode = ""
                    ToAccountSchemeCode = ""
                Else
                    OriginalToAccountNo = ToAccountNumber
                    ToAccountBrCode = ToBranchCode
                    ToAccountProductCode = ""
                    ToAccountSchemeCode = ""
                End If
            Else
                OriginalToAccountNo = ToAccountNumber
                ToAccountBrCode = ToBranchCode
                ToAccountProductCode = ""
                ToAccountSchemeCode = ""
            End If



            '' Customer Debit GL in RB
            DrGlCode = ""
            '' SBL Contra GL for CMS
            InterBranchTaggedGL = ""
            '' Customer Credit GL in RB
            CrGlCode = ""



            If EntityType.ToString() = "Instruction" Then
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                Select Case TransferType.ToString()
                    Case "RB to RB"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode.ToString)
                        Try
                            '' If EOD
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                'Update EOD Instruction Status as Pending CB
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objICEODPending.OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    'objInstruction = New ICInstruction
                                    'objInstruction.LoadByPrimaryKey(RelatedID)
                                    'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                    SkipReason += " to Account No [ " & ToAccountNumber & " ] processing from EOD."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                    'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return False
                                End If

                            Else
                                If Not ClearingType Is Nothing Then
                                    OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)

                                Else
                                    OriginatingTranType = objProductType.OriginatingTransactionType

                                End If
                                'Update Instruction Status as Pending CB
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    Return False
                                End If

                            End If

                        Catch ex As Exception
                            If ex.Message.Contains("EOD: ") = True Then
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                If objInstruction.Status.ToString = "39" Then
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                End If

                                If objInstruction.Status.ToString = PendingEODStatus Then
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                    Return False

                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                    SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, False, OriginatingTranType, RespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                    Return False
                                End If
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                SkipReason += " to Account [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                Return False
                            End If
                        End Try
                    Case "RB to GL"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode.ToString)
                        Try
                            '' If EOD
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                'Update EOD Instruction Status as Pending CB
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objICEODPending.OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    'objInstruction = New ICInstruction
                                    'objInstruction.LoadByPrimaryKey(RelatedID)
                                    'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                    SkipReason += " to Account No [ " & ToAccountNumber & " ] processing from EOD."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                    'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return False
                                End If

                            Else
                                If Not ClearingType Is Nothing Then
                                    OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)

                                Else
                                    OriginatingTranType = objProductType.OriginatingTransactionType

                                End If
                                'Update Instruction Status as Pending CB
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    Return False
                                End If

                            End If

                        Catch ex As Exception
                            If ex.Message.Contains("EOD: ") = True Then
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                If objInstruction.Status.ToString = "39" Then
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                End If

                                If objInstruction.Status.ToString = PendingEODStatus Then
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                    Return False

                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                    SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, False, OriginatingTranType, RespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                    Return False
                                End If
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                SkipReason += " to Account [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                Return False
                            End If
                        End Try
                    Case "GL to RB"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode.ToString)
                        Try
                            '' If EOD
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                'Update EOD Instruction Status as Pending CB
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objICEODPending.OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    'objInstruction = New ICInstruction
                                    'objInstruction.LoadByPrimaryKey(RelatedID)
                                    'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                    SkipReason += " to Account No [ " & ToAccountNumber & " ] processing from EOD."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                    'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return False
                                End If

                            Else
                                If Not ClearingType Is Nothing Then
                                    OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)

                                Else
                                    OriginatingTranType = objProductType.OriginatingTransactionType

                                End If
                                'Update Instruction Status as Pending CB
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    Return False
                                End If

                            End If

                        Catch ex As Exception
                            If ex.Message.Contains("EOD: ") = True Then
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                If objInstruction.Status.ToString = "39" Then
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                End If

                                If objInstruction.Status.ToString = PendingEODStatus Then
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                    Return False

                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                    SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, False, OriginatingTranType, RespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                    Return False
                                End If
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                SkipReason += " to Account [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                Return False
                            End If
                        End Try
                    Case "GL to GL"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode.ToString)
                        Try
                            '' If EOD
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                'Update EOD Instruction Status as Pending CB
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objICEODPending.OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    'objInstruction = New ICInstruction
                                    'objInstruction.LoadByPrimaryKey(RelatedID)
                                    'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                    SkipReason += " to Account No [ " & ToAccountNumber & " ] processing from EOD."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                    'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                    Return False
                                End If

                            Else
                                If Not ClearingType Is Nothing Then
                                    OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)

                                Else
                                    OriginatingTranType = objProductType.OriginatingTransactionType

                                End If
                                'Update Instruction Status as Pending CB
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                ' Call DoTransaction to move funds from FromAccount to Contra GL Account
                                If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                    'Update Instruction Status to Last Status
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    Return True
                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    Return False
                                End If

                            End If

                        Catch ex As Exception
                            If ex.Message.Contains("EOD: ") = True Then
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                If objInstruction.Status.ToString = "39" Then
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                End If

                                If objInstruction.Status.ToString = PendingEODStatus Then
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    objICEODPending = New ICPendingEODInstructions
                                    objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                    ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                    Return False

                                Else
                                    ' Update Instruction Status as [Error]
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                    SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                    objInstruction.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                    objInstruction = New ICInstruction
                                    objInstruction.LoadByPrimaryKey(RelatedID)
                                    AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, False, OriginatingTranType, RespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                    Return False
                                End If
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & FromAcccountNumber & " ]"
                                SkipReason += " to Account [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                Return False
                            End If
                        End Try
                End Select
            Else
                Dim objICInstructionsForUpdate As ICInstruction
                Select Case TransferType.ToString()
                    Case "RB to RB"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(ProductTypeCode)
                        If Not ClearingType Is Nothing Then
                            OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)
                            RespondingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Responding", objProductType)
                        Else
                            OriginatingTranType = objProductType.OriginatingTransactionType
                            RespondingTranType = objProductType.RespondingTranType
                        End If
                        Try

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status as Pending CB
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            Next
                            ' Call DoTransaction to move funds from FromAccount to Account
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return True
                            Else
                                ' Update Instruction Status as [SKIP]
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objICInstructionsForUpdate.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return False
                            End If

                        Catch ex As Exception
                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                objICInstructionsForUpdate.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                'ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status.ToString, objICInstructionsForUpdate.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next
                            Return False

                        End Try
                    Case "RB to GL"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(ProductTypeCode)
                        If Not ClearingType Is Nothing Then
                            OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)
                            RespondingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Responding", objProductType)
                        Else
                            OriginatingTranType = objProductType.OriginatingTransactionType
                            RespondingTranType = objProductType.RespondingTranType
                        End If
                        Try

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status as Pending CB
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            Next
                            ' Call DoTransaction to move funds from FromAccount to Account
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return True
                            Else
                                ' Update Instruction Status as [SKIP]
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objICInstructionsForUpdate.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return False
                            End If

                        Catch ex As Exception
                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                objICInstructionsForUpdate.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                'ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status.ToString, objICInstructionsForUpdate.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next
                            Return False

                        End Try
                    Case "GL to RB"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(ProductTypeCode)
                        If Not ClearingType Is Nothing Then
                            OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)
                            RespondingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Responding", objProductType)
                        Else
                            OriginatingTranType = objProductType.OriginatingTransactionType
                            RespondingTranType = objProductType.RespondingTranType
                        End If
                        Try

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status as Pending CB
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            Next
                            ' Call DoTransaction to move funds from FromAccount to Account
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return True
                            Else
                                ' Update Instruction Status as [SKIP]
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objICInstructionsForUpdate.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return False
                            End If

                        Catch ex As Exception
                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                objICInstructionsForUpdate.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                'ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status.ToString, objICInstructionsForUpdate.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next
                            Return False

                        End Try
                    Case "GL to GL"
                        objProductType = New ICProductType
                        objProductType.LoadByPrimaryKey(ProductTypeCode)
                        If Not ClearingType Is Nothing Then
                            OriginatingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Originating", objProductType)
                            RespondingTranType = ICProductTypeController.GetTranTypeByClearingAndTransferType(ClearingType, ClearVIA, "Responding", objProductType)
                        Else
                            OriginatingTranType = objProductType.OriginatingTransactionType
                            RespondingTranType = objProductType.RespondingTranType
                        End If
                        Try

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status as Pending CB
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            Next
                            ' Call DoTransaction to move funds from FromAccount to Account
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, OriginatingTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return True
                            Else
                                ' Update Instruction Status as [SKIP]
                                For Each objICInstructions As ICInstruction In objInstructionColl
                                    'Update Instruction Status to Last Status
                                    objICInstructionsForUpdate = New ICInstruction
                                    objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                    SkipReason = Nothing
                                    SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                    SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                    objICInstructionsForUpdate.SkipReason = SkipReason
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                    ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                Next
                                Return False
                            End If

                        Catch ex As Exception
                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status, objICInstructionsForUpdate.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next

                            For Each objICInstructions As ICInstruction In objInstructionColl
                                'Update Instruction Status to Last Status
                                objICInstructionsForUpdate = New ICInstruction
                                objICInstructionsForUpdate.LoadByPrimaryKey(objICInstructions.InstructionID.ToString)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                objICInstructionsForUpdate.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objICInstructionsForUpdate, Nothing, Nothing)
                                'ICInstructionController.UpdateInstructionStatus(objICInstructionsForUpdate.InstructionID, objICInstructionsForUpdate.Status.ToString, objICInstructionsForUpdate.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Next
                            Return False

                        End Try
                End Select
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
        'Return True
    End Function


    'Public Shared Function Reversal(ByVal FromAcccountNumber As String, ByVal FromBranchCode As String, ByVal FromCurrency As String, ByVal FromClientNo As String,
    '                               ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromAccountType As AccountType,
    '                               ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String, ByVal ToClientNo As String,
    '                               ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType, ByVal TransactionAmount As String,
    '                               ByVal EntityType As String, ByVal RelatedID As String, ByVal FinalStatus As String, ByVal FailureStatus As String,
    '                               ByVal UsersID As String, Optional ByVal DebitGLCode As String = Nothing, Optional ByVal CreditGLCode As String = Nothing,
    '                               Optional ByVal CancelOrigTranType As String = Nothing, Optional ByVal CancelRepondingTranType As String = Nothing) As Boolean
    '    Return True
    'End Function


    Public Shared Function Reversal(ByVal FromAcccountNumber As String, ByVal FromBranchCode As String, ByVal FromCurrency As String, ByVal FromClientNo As String,
                                        ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromAccountType As AccountType,
                                        ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String, ByVal ToClientNo As String,
                                        ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType, ByVal TransactionAmount As String,
                                        ByVal EntityType As String, ByVal RelatedID As String, ByVal FinalStatus As String, ByVal FailureStatus As String,
                                        ByVal UsersID As String, Optional ByVal DebitGLCode As String = Nothing, Optional ByVal CreditGLCode As String = Nothing,
                                        Optional ByVal CancelOrigTranType As String = Nothing, Optional ByVal CancelRepondingTranType As String = Nothing) As Boolean
        'Return True
        Dim logID As Integer = 0
        Dim objInstruction As New ICInstruction
        Dim objProductType As New ICProductType
        Dim objICEODPending As ICPendingEODInstructions
        Dim CurrentStatus As String = ""
        Dim SkipReason As String = Nothing
        Dim BasicBankAccountNo As String = Nothing
        Dim BasicBankAccountNo2 As String = Nothing
        Dim OriginalFromAccountNo, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode As String
        Dim OriginalToAccountNo, ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode As String
        Dim DrGlCode, CrGlCode, InterBranchTaggedGL As String
        Dim OriginatingIDNo As Integer = 0
        Dim InstrumentDate As Date = Now.Date
        Dim PendingEODStatus As String = "47"
        Dim IsSecondLeg As Boolean = False
        Dim StanNo As String = Nothing

        Try
            Dim TransferType As String = ""
            If FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "RB" Then
                TransferType = "RB to RB"
            ElseIf FromAccountType.ToString() = "RB" And ToAccountType.ToString() = "GL" Then
                TransferType = "RB to GL"
            ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "RB" Then
                TransferType = "GL to RB"
            ElseIf FromAccountType.ToString() = "GL" And ToAccountType.ToString() = "GL" Then
                TransferType = "GL to GL"
            End If


            If FromAccountType.ToString = "RB" Then
                If FromAcccountNumber.Length > 15 Then
                    OriginalFromAccountNo = FromAcccountNumber

                    FromAccountBrCode = FromBranchCode
                    FromAccountProductCode = ""
                    FromAccountSchemeCode = ""

                Else
                    OriginalFromAccountNo = FromAcccountNumber

                    FromAccountBrCode = FromBranchCode
                    FromAccountProductCode = ""
                    FromAccountSchemeCode = ""
                End If
            Else
                OriginalFromAccountNo = FromAcccountNumber
                FromAccountBrCode = FromBranchCode
                FromAccountProductCode = ""
                FromAccountSchemeCode = ""
            End If
            If ToAccountType.ToString = "RB" Then
                If ToAccountNumber.Length > 15 Then
                    OriginalToAccountNo = ToAccountNumber
                    ToAccountBrCode = ToBranchCode
                    ToAccountProductCode = ""
                    ToAccountSchemeCode = ""
                Else
                    OriginalToAccountNo = ToAccountNumber
                    ToAccountBrCode = ToBranchCode
                    ToAccountProductCode = ""
                    ToAccountSchemeCode = ""
                End If
            Else
                OriginalToAccountNo = ToAccountNumber
                ToAccountBrCode = ToBranchCode
                ToAccountProductCode = ""
                ToAccountSchemeCode = ""
            End If



            '' Customer Debit GL in RB
            DrGlCode = ""
            '' SBL Contra GL for CMS
            InterBranchTaggedGL = ""
            '' Customer Credit GL in RB
            CrGlCode = ""

            If EntityType.ToString() = "Instruction" Then
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                CurrentStatus = objInstruction.Status.ToString()
            End If
            Select Case TransferType.ToString()
                Case "RB to RB"
                    objProductType = New ICProductType
                    objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode)
                    Try
                        If objInstruction.Status.ToString = PendingEODStatus Then
                            objICEODPending = New ICPendingEODInstructions
                            objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                            ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, CancelOrigTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "Y", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                'objInstruction = New ICInstruction
                                'objInstruction.LoadByPrimaryKey(RelatedID)
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ] processing from EOD."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return False
                            End If

                        Else
                            'Update Instruction Status as Pending CB
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, objInstruction.InstructionID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objProductType.POCancellationTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, objInstruction.Amount, objInstruction.InstructionID, "Y", OriginatingIDNo, "Instruction", FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No. [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return False
                            End If
                        End If

                    Catch ex As Exception
                        If ex.Message.Contains("EOD: ") Then
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If objInstruction.Status.ToString = "39" Then
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                            End If
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                Return False
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, True, objProductType.POCancellationTranType, objProductType.ReversalRespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                Return False
                            End If

                        Else
                            ' Update Instruction Status as [Error]
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            SkipReason = Nothing
                            SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                            SkipReason += " to Account No [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                            objInstruction.SkipReason = SkipReason
                            ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Return False
                        End If
                    End Try
                Case "RB to GL"
                    objProductType = New ICProductType
                    objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode)
                    Try
                        If objInstruction.Status.ToString = PendingEODStatus Then
                            objICEODPending = New ICPendingEODInstructions
                            objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                            ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, CancelOrigTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "Y", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                'objInstruction = New ICInstruction
                                'objInstruction.LoadByPrimaryKey(RelatedID)
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ] processing from EOD."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return False
                            End If

                        Else
                            'Update Instruction Status as Pending CB
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, objInstruction.InstructionID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objProductType.POCancellationTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, objInstruction.Amount, objInstruction.InstructionID, "Y", OriginatingIDNo, "Instruction", FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No. [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return False
                            End If
                        End If

                    Catch ex As Exception
                        If ex.Message.Contains("EOD: ") Then
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If objInstruction.Status.ToString = "39" Then
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                            End If
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                Return False
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, True, objProductType.POCancellationTranType, objProductType.ReversalRespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                Return False
                            End If

                        Else
                            ' Update Instruction Status as [Error]
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            SkipReason = Nothing
                            SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                            SkipReason += " to Account No [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                            objInstruction.SkipReason = SkipReason
                            ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Return False
                        End If
                    End Try

                Case "GL to RB"
                    objProductType = New ICProductType
                    objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode)
                    Try
                        If objInstruction.Status.ToString = PendingEODStatus Then
                            objICEODPending = New ICPendingEODInstructions
                            objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                            ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, CancelOrigTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "Y", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                'objInstruction = New ICInstruction
                                'objInstruction.LoadByPrimaryKey(RelatedID)
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ] processing from EOD."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return False
                            End If

                        Else
                            'Update Instruction Status as Pending CB
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, objInstruction.InstructionID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objProductType.POCancellationTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, objInstruction.Amount, objInstruction.InstructionID, "Y", OriginatingIDNo, "Instruction", FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No. [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return False
                            End If
                        End If

                    Catch ex As Exception
                        If ex.Message.Contains("EOD: ") Then
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If objInstruction.Status.ToString = "39" Then
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                            End If
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                Return False
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, True, objProductType.POCancellationTranType, objProductType.ReversalRespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                Return False
                            End If

                        Else
                            ' Update Instruction Status as [Error]
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            SkipReason = Nothing
                            SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                            SkipReason += " to Account No [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                            objInstruction.SkipReason = SkipReason
                            ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Return False
                        End If
                    End Try

                Case "GL to GL"
                    objProductType = New ICProductType
                    objProductType.LoadByPrimaryKey(objInstruction.ProductTypeCode)
                    Try
                        If objInstruction.Status.ToString = PendingEODStatus Then
                            objICEODPending = New ICPendingEODInstructions
                            objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                            ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, "39")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, RelatedID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, CancelOrigTranType, objProductType.VoucherType, objProductType.InstrumentType, DebitGLCode, CreditGLCode, objInstruction.InstrumentNo, CDbl(objInstruction.Amount), RelatedID, "Y", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                'objInstruction = New ICInstruction
                                'objInstruction.LoadByPrimaryKey(RelatedID)
                                'ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ] processing from EOD."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.DeletePendingEODInstruction(objInstruction.InstructionID.ToString)
                                'ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status.ToString, objICEODPending.LastStatus.ToString)
                                Return False
                            End If

                        Else
                            'Update Instruction Status as Pending CB
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If DoTransaction(StanNo, objInstruction.InstructionID, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode, FromAcccountNumber, "3", ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode, ToAccountNumber, objProductType.POCancellationTranType, objProductType.VoucherType, objProductType.InstrumentType, DrGlCode, CrGlCode, objInstruction.InstrumentNo, objInstruction.Amount, objInstruction.InstructionID, "Y", OriginatingIDNo, "Instruction", FromAccountType, ToAccountType, FromAccountBrCode) Then
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return True
                            Else
                                ' Update Instruction Status as [Error]
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No. [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                'Update Instruction Status to Last Status
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                Return False
                            End If
                        End If

                    Catch ex As Exception
                        If ex.Message.Contains("EOD: ") Then
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            If objInstruction.Status.ToString = "39" Then
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, objInstruction.LastStatus.ToString, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                            End If
                            If objInstruction.Status.ToString = PendingEODStatus Then
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer from  Account No [ " & OriginalFromAccountNo & " ]"
                                SkipReason += " to Account No [ " & OriginalToAccountNo & " ]"
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                objICEODPending = New ICPendingEODInstructions
                                objICEODPending = ICEODPendingInstructionController.GetPendingEODInstructionByInstructionID(RelatedID)
                                ICEODPendingInstructionController.UpdatePendingEODInstructionStatus(RelatedID, objICEODPending.Status, objICEODPending.LastStatus.ToString)
                                Return False
                            Else
                                ' Update Instruction Status as [Error]
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                SkipReason = Nothing
                                SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due  to  " & ex.Message.ToString & " and  fail in funds transfer while processing second leg from Account No [ " & OriginalFromAccountNo & " ] "
                                SkipReason += " To Account No [ " & OriginalToAccountNo & " ]."
                                objInstruction.SkipReason = SkipReason
                                ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, PendingEODStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                                objInstruction = New ICInstruction
                                objInstruction.LoadByPrimaryKey(RelatedID)
                                AddPendingEODInstruction(RelatedID, OriginalFromAccountNo, FromAccountType, FromCurrency, DrGlCode, InterBranchTaggedGL, objInstruction.Status.ToString, FinalStatus, FailureStatus, UserID, OriginalToAccountNo, ToAccountType, ToCurrency, objProductType.ProductTypeCode, False, True, objProductType.POCancellationTranType, objProductType.ReversalRespondingTranType, ToAccountBrCode, FromAccountBrCode)
                                Return False
                            End If

                        Else
                            ' Update Instruction Status as [Error]
                            objInstruction = New ICInstruction
                            objInstruction.LoadByPrimaryKey(RelatedID)
                            SkipReason = Nothing
                            SkipReason = "Instruction Skip: Instruction with ID [ " & objInstruction.InstructionID & " ] skipped due to fail in funds transfer on reversal from  Account No [ " & OriginalFromAccountNo & " ]"
                            SkipReason += " to Account No [ " & OriginalToAccountNo & " ] due to " & ex.Message.ToString
                            objInstruction.SkipReason = SkipReason
                            ICInstructionController.UpdateInstructionSkipReason(objInstruction, Nothing, Nothing)
                            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status.ToString, FailureStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                            Return False
                        End If
                    End Try
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Shared Function TM_RB_API_Deposit(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal TranCode As String, ByVal Amount As Double, ByVal TranDesc As String, ByVal DebitCreditIndicator As String, ByVal OrignRate As Double, ByVal CrosssRate As Double, ByVal Reference As String, ByVal ReferenceBank As String, ByVal ReferenceBranch As String, ByVal FloatDays As Integer) As Boolean
        Try

            ''IBAN Check

            Dim BasicBankAccountNoFromAccount As String = Nothing
            If IsNormalAccountNoOrIBAN(AccountNumber) = True Then
                BasicBankAccountNoFromAccount = GetBBANFromIBAN(AccountNumber.ToString)
                AccountNumber = Nothing
                If Not BasicBankAccountNoFromAccount Is Nothing Then
                    AccountNumber = BasicBankAccountNoFromAccount
                Else
                    Return False
                End If
            End If


            Dim constring As String = System.Configuration.ConfigurationManager.ConnectionStrings("Oracle").ConnectionString
            ' INPUT Parameters

            Dim inAccountNumber As New Oracle.DataAccess.Client.OracleParameter("Paccount_no", OracleDbType.Varchar2, ParameterDirection.Input)
            inAccountNumber.Value = AccountNumber.ToString()
            inAccountNumber.Size = 200

            Dim inBranchCode As New Oracle.DataAccess.Client.OracleParameter("pbranch", OracleDbType.Varchar2, ParameterDirection.Input)
            inBranchCode.Value = BranchCode.ToString()
            inBranchCode.Size = 200

            Dim inCurrency As New Oracle.DataAccess.Client.OracleParameter("pTRAN_ccy", OracleDbType.Varchar2, ParameterDirection.Input)
            inCurrency.Value = Currency.ToString()
            inCurrency.Size = 200

            Dim inTranType As New Oracle.DataAccess.Client.OracleParameter("ptran_type", OracleDbType.Varchar2, ParameterDirection.InputOutput)
            inTranType.Value = TranCode.ToString()
            inTranType.Size = 200

            Dim inAmount As New Oracle.DataAccess.Client.OracleParameter("pTRAN_AMT", OracleDbType.Decimal, ParameterDirection.Input)
            inAmount.Value = Amount

            Dim inDescription As New Oracle.DataAccess.Client.OracleParameter("ptran_desc", OracleDbType.Varchar2, ParameterDirection.Input)
            inDescription.Value = TranDesc.ToUpper.ToString()
            inDescription.Size = 200

            Dim inDebitCreditIndicator As New Oracle.DataAccess.Client.OracleParameter("pcr_dr_ind", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitCreditIndicator.Value = DebitCreditIndicator.ToString()
            inDebitCreditIndicator.Size = 1

            Dim inOrignRate As New Oracle.DataAccess.Client.OracleParameter("pORIG_RATE", OracleDbType.Decimal, ParameterDirection.Input)
            inOrignRate.Value = OrignRate

            Dim inCrossRate As New Oracle.DataAccess.Client.OracleParameter("pCROSS_RATE", OracleDbType.Decimal, ParameterDirection.Input)
            inCrossRate.Value = CrosssRate

            Dim inReference As New Oracle.DataAccess.Client.OracleParameter("pREFERENCE", OracleDbType.Varchar2, ParameterDirection.Input)
            inReference.Value = Reference
            inReference.Size = 200

            Dim inReferenceBank As New Oracle.DataAccess.Client.OracleParameter("pREFERENCE_BANK", OracleDbType.Varchar2, ParameterDirection.Input)
            inReferenceBank.Value = ReferenceBank.ToString()
            inReferenceBank.Size = 200

            Dim inReferenceBranch As New Oracle.DataAccess.Client.OracleParameter("pREFERENCE_BRANCH", OracleDbType.Varchar2, ParameterDirection.Input)
            inReferenceBranch.Value = ReferenceBranch.ToString()
            inReferenceBranch.Size = 200

            Dim inFloatDays As New Oracle.DataAccess.Client.OracleParameter("pFLOAT_DAYS", OracleDbType.Decimal, ParameterDirection.Input)
            inFloatDays.Value = FloatDays

            ' OUTPUT Parameters
            Dim outAvailableBalance As New Oracle.DataAccess.Client.OracleParameter("pavail_bal", OracleDbType.Double, ParameterDirection.Output)
            outAvailableBalance.Value = Nothing

            Dim outLedgerBalance As New Oracle.DataAccess.Client.OracleParameter("pledger_bal", OracleDbType.Double, ParameterDirection.Output)
            outLedgerBalance.Value = Nothing

            Dim outSequenceNo As New Oracle.DataAccess.Client.OracleParameter("ptxn_seq_no", OracleDbType.Double, ParameterDirection.Output)
            outSequenceNo.Value = Nothing

            Dim outDate As New Oracle.DataAccess.Client.OracleParameter("ptrdate", OracleDbType.Date, ParameterDirection.Output)
            outDate.Value = Nothing

            Dim outErrorNo As New Oracle.DataAccess.Client.OracleParameter("p_Error_No", OracleDbType.Double, ParameterDirection.Output)
            outErrorNo.Value = Nothing

            Dim outErrorMsg As New Oracle.DataAccess.Client.OracleParameter("p_Message", OracleDbType.Varchar2, ParameterDirection.Output)
            outErrorMsg.Size = 200
            outErrorMsg.Value = Nothing

            Dim con As New OracleConnection(constring)
            Dim objCmd As New OracleCommand()
            objCmd.Connection = con
            objCmd.CommandText = "TM_RB_API_DEPOSIT"
            objCmd.CommandType = CommandType.StoredProcedure

            objCmd.Parameters.Add(inAccountNumber)
            objCmd.Parameters.Add(inBranchCode)
            objCmd.Parameters.Add(inCurrency)
            objCmd.Parameters.Add(inTranType)
            objCmd.Parameters.Add(inAmount)
            objCmd.Parameters.Add(inDescription)
            objCmd.Parameters.Add(inDebitCreditIndicator)
            objCmd.Parameters.Add(inOrignRate)
            objCmd.Parameters.Add(inCrossRate)
            objCmd.Parameters.Add(inReference)
            objCmd.Parameters.Add(inReferenceBank)
            objCmd.Parameters.Add(inReferenceBranch)
            objCmd.Parameters.Add(inFloatDays)

            objCmd.Parameters.Add(outAvailableBalance)
            objCmd.Parameters.Add(outLedgerBalance)
            objCmd.Parameters.Add(outSequenceNo)
            objCmd.Parameters.Add(outDate)
            objCmd.Parameters.Add(outErrorNo)
            objCmd.Parameters.Add(outErrorMsg)

            objCmd.CommandTimeout = 36000
            con.Open()
            objCmd.ExecuteNonQuery()
            If Not outErrorMsg.Value Is DBNull.Value Then
                Dim i As Integer = Nothing
                If Not outSequenceNo.Status = OracleParameterStatus.NullFetched Then
                    Dim retSequenceNo As Integer = CType(outSequenceNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                End If
                Dim retErrorNO As Integer = 0
                If Not outErrorNo.Status = OracleParameterStatus.NullFetched Then
                    retErrorNO = CType(outErrorNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                End If
                Dim retErrorMsg As String = Nothing
                If Not outErrorMsg.Status = OracleParameterStatus.NullFetched Then
                    If Integer.TryParse(outErrorMsg.Value.ToString, i) Then
                        retErrorMsg = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value
                    Else
                        retErrorMsg = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value
                    End If
                End If


                If retErrorNO = 0 Then
                    Return True
                Else
                    Throw New Exception(retErrorMsg)
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Shared Function TM_CREATE_GL(ByVal FromAccountNumber As String, ByVal FromCurrency As String, ByVal FromBranchCode As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromReference As String, ByVal FromAmount As Double, ByVal FromBaseEquivalent As Double, ByVal FromNarrative As String, ByVal FromExchangeRateType As String, ByVal ToAccountNumber As String, ByVal ToCurrency As String, ByVal ToBranchCode As String, ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToReference As String, ByVal ToAmount As Double, ByVal ToNarrative As String) As Boolean



        Try


            ''IBAN Check

            Dim BasicBankAccountNoFromAccount As String = Nothing
            Dim BasicBankAccountNoToAccount As String = Nothing



            If IsNormalAccountNoOrIBAN(FromAccountNumber) = True Then
                BasicBankAccountNoFromAccount = GetBBANFromIBAN(FromAccountNumber.ToString)
                FromAccountNumber = Nothing
                If Not BasicBankAccountNoFromAccount Is Nothing Then
                    FromAccountNumber = BasicBankAccountNoFromAccount
                Else
                    Return False
                End If
            End If



            If IsNormalAccountNoOrIBAN(ToAccountNumber) = True Then
                BasicBankAccountNoToAccount = GetBBANFromIBAN(ToAccountNumber.ToString)
                ToAccountNumber = Nothing
                If Not BasicBankAccountNoToAccount Is Nothing Then
                    ToAccountNumber = BasicBankAccountNoToAccount
                Else
                    Return False
                End If
            End If


            Dim constring As String = System.Configuration.ConfigurationManager.ConnectionStrings("Oracle").ConnectionString
            ' RETURN VALUE Parameter
            Dim retValue As New Oracle.DataAccess.Client.OracleParameter("Return_Value", OracleDbType.Decimal, ParameterDirection.ReturnValue)

            ' INPUT Parameters
            Dim inCorrID As New Oracle.DataAccess.Client.OracleParameter("pCorr_id", OracleDbType.Varchar2, ParameterDirection.Input)
            inCorrID.Value = "CBS"
            inCorrID.Size = 200

            Dim inApplication As New Oracle.DataAccess.Client.OracleParameter("pApplication", OracleDbType.Varchar2, ParameterDirection.Input)
            inApplication.Value = "Al Baraka"
            inApplication.Size = 200

            Dim inWorkStation As New Oracle.DataAccess.Client.OracleParameter("pWorkstation", OracleDbType.Varchar2, ParameterDirection.Input)
            inWorkStation.Value = "Al Baraka"
            inWorkStation.Size = 200

            Dim inScreenNo As New Oracle.DataAccess.Client.OracleParameter("pScreen_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inScreenNo.Value = "CBS"
            inScreenNo.Size = 200

            Dim inPosition As New Oracle.DataAccess.Client.OracleParameter("pPosition", OracleDbType.Decimal, ParameterDirection.Input)
            inPosition.Value = 1

            Dim inGLPostingType As New Oracle.DataAccess.Client.OracleParameter("pGl_Posting_Type", OracleDbType.Varchar2, ParameterDirection.Input)
            inGLPostingType.Value = DBNull.Value
            inGLPostingType.Size = 200

            Dim inDebitTranKey As New Oracle.DataAccess.Client.OracleParameter("pDr_Tran_Key", OracleDbType.Decimal, ParameterDirection.Input)
            inDebitTranKey.Value = DBNull.Value

            Dim inDebitGLCode As New Oracle.DataAccess.Client.OracleParameter("pDr_Gl_Code", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitGLCode.Value = FromAccountNumber.ToString()
            inDebitGLCode.Size = 200

            Dim inDebitCurrency As New Oracle.DataAccess.Client.OracleParameter("pDr_Ccy", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitCurrency.Value = FromCurrency.ToString()
            inDebitCurrency.Size = 200

            Dim inDebitBranchCode As New Oracle.DataAccess.Client.OracleParameter("pDr_Branch", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitBranchCode.Value = FromBranchCode.ToString()
            inDebitBranchCode.Size = 200

            Dim inDebitClientNo As New Oracle.DataAccess.Client.OracleParameter("pDr_Client_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitClientNo.Value = FromClientNo.ToString()
            inDebitClientNo.Size = 200

            Dim inDebitSequenceNo As New Oracle.DataAccess.Client.OracleParameter("pDr_Seq_No", OracleDbType.Decimal, ParameterDirection.Input)
            inDebitSequenceNo.Value = FromSeqNo

            Dim inDebitProfitCentre As New Oracle.DataAccess.Client.OracleParameter("pDr_Profit_Centre", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitProfitCentre.Value = FromProfitCentre.ToString()
            inDebitProfitCentre.Size = 200

            Dim inDebitSourceModule As New Oracle.DataAccess.Client.OracleParameter("pDr_Source_Module", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitSourceModule.Value = "GL"
            inDebitSourceModule.Size = 200

            Dim inDebitSourceType As New Oracle.DataAccess.Client.OracleParameter("pDr_Source_Type", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitSourceType.Value = "IG"
            inDebitSourceType.Size = 200

            Dim inDebitTradeNo As New Oracle.DataAccess.Client.OracleParameter("pDr_Trade_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitTradeNo.Value = DBNull.Value
            inDebitTradeNo.Size = 200

            Dim inDebitReference As New Oracle.DataAccess.Client.OracleParameter("pDr_Reference", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitReference.Value = FromReference
            inDebitReference.Size = 200

            Dim inDebitPostDate As New Oracle.DataAccess.Client.OracleParameter("pDr_Post_Date", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitPostDate.Value = DBNull.Value
            inDebitPostDate.Size = 200

            Dim inDebitValueDate As New Oracle.DataAccess.Client.OracleParameter("pDr_Value_Date", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitValueDate.Value = DBNull.Value
            inDebitValueDate.Size = 200

            Dim inDebitAmount As New Oracle.DataAccess.Client.OracleParameter("pDr_Amount", OracleDbType.Decimal, ParameterDirection.Input)
            inDebitAmount.Value = FromAmount

            Dim inBaseEquivalent As New Oracle.DataAccess.Client.OracleParameter("pBase_Equivalent", OracleDbType.Decimal, ParameterDirection.Input)
            inBaseEquivalent.Value = FromBaseEquivalent

            Dim inDebitNarrative As New Oracle.DataAccess.Client.OracleParameter("pDr_Narrative", OracleDbType.Varchar2, ParameterDirection.Input)
            inDebitNarrative.Value = FromNarrative.ToString().ToUpper()
            inDebitNarrative.Size = 200

            Dim inDebitStatCode As New Oracle.DataAccess.Client.OracleParameter("pDr_Stat_Code", OracleDbType.Decimal, ParameterDirection.Input)
            inDebitStatCode.Value = DBNull.Value

            Dim inExchangeRateType As New Oracle.DataAccess.Client.OracleParameter("pExchange_Rate_Type", OracleDbType.Varchar2, ParameterDirection.Input)
            inExchangeRateType.Value = FromExchangeRateType.ToString()
            inExchangeRateType.Size = 200

            Dim inFeeFlag As New Oracle.DataAccess.Client.OracleParameter("pFee_Flag", OracleDbType.Varchar2, ParameterDirection.Input)
            inFeeFlag.Value = DBNull.Value
            inFeeFlag.Size = 200

            Dim inScType As New Oracle.DataAccess.Client.OracleParameter("pSc_Type", OracleDbType.Varchar2, ParameterDirection.Input)
            inScType.Value = DBNull.Value
            inScType.Size = 200

            Dim inFeeTaxFlag As New Oracle.DataAccess.Client.OracleParameter("pFee_Tax_Flag", OracleDbType.Varchar2, ParameterDirection.Input)
            inFeeTaxFlag.Value = DBNull.Value
            inFeeTaxFlag.Size = 200

            Dim inPostingNatureIndicator As New Oracle.DataAccess.Client.OracleParameter("pPosting_Nature_Ind", OracleDbType.Varchar2, ParameterDirection.Input)
            inPostingNatureIndicator.Value = DBNull.Value
            inPostingNatureIndicator.Size = 200




            Dim inCreditTranKey As New Oracle.DataAccess.Client.OracleParameter("pCr_Tran_Key", OracleDbType.Decimal, ParameterDirection.Input)
            inCreditTranKey.Value = DBNull.Value

            Dim inCreditGLCode As New Oracle.DataAccess.Client.OracleParameter("pCr_Gl_Code", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditGLCode.Value = ToAccountNumber.ToString()
            inCreditGLCode.Size = 200

            Dim inCreditCurrency As New Oracle.DataAccess.Client.OracleParameter("pCr_Ccy", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditCurrency.Value = ToCurrency.ToString()
            inCreditCurrency.Size = 200

            Dim inCreditBranchCode As New Oracle.DataAccess.Client.OracleParameter("pCr_Branch", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditBranchCode.Value = ToBranchCode.ToString()
            inCreditBranchCode.Size = 200

            Dim inCreditClientNo As New Oracle.DataAccess.Client.OracleParameter("pCr_Client_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditClientNo.Value = ToClientNo.ToString()
            inCreditClientNo.Size = 200

            Dim inCreditSequenceNo As New Oracle.DataAccess.Client.OracleParameter("pCr_Seq_No", OracleDbType.Decimal, ParameterDirection.Input)
            inCreditSequenceNo.Value = ToSeqNo

            Dim inCreditProfitCentre As New Oracle.DataAccess.Client.OracleParameter("pCr_Profit_Centre", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditProfitCentre.Value = ToProfitCentre.ToString()
            inCreditProfitCentre.Size = 200

            Dim inCreditSourceModule As New Oracle.DataAccess.Client.OracleParameter("pCr_Source_Module", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditSourceModule.Value = "GL"
            inCreditSourceModule.Size = 200

            Dim inCreditSourceType As New Oracle.DataAccess.Client.OracleParameter("pCr_Source_Type", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditSourceType.Value = "IG"
            inCreditSourceType.Size = 200

            Dim inCreditTradeNo As New Oracle.DataAccess.Client.OracleParameter("pCr_Trade_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditTradeNo.Value = DBNull.Value
            inCreditTradeNo.Size = 200

            Dim inCreditReference As New Oracle.DataAccess.Client.OracleParameter("pCr_Reference", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditReference.Value = ToReference
            inCreditReference.Size = 200

            Dim inCreditPostDate As New Oracle.DataAccess.Client.OracleParameter("pCr_Post_Date", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditPostDate.Value = DBNull.Value
            inCreditPostDate.Size = 200

            Dim inCreditValueDate As New Oracle.DataAccess.Client.OracleParameter("pCr_Value_Date", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditValueDate.Value = DBNull.Value
            inCreditValueDate.Size = 200

            Dim inCreditAmount As New Oracle.DataAccess.Client.OracleParameter("pCr_Amount", OracleDbType.Decimal, ParameterDirection.Input)
            inCreditAmount.Value = ToAmount

            Dim inCreditNarrative As New Oracle.DataAccess.Client.OracleParameter("pCr_Narrative", OracleDbType.Varchar2, ParameterDirection.Input)
            inCreditNarrative.Value = ToNarrative.ToString().ToUpper()
            inCreditNarrative.Size = 200

            Dim inCreditStatCode As New Oracle.DataAccess.Client.OracleParameter("pCr_Stat_Code", OracleDbType.Decimal, ParameterDirection.Input)
            inCreditStatCode.Value = DBNull.Value

            Dim inOrignRate As New Oracle.DataAccess.Client.OracleParameter("pOrig_Rate", OracleDbType.Decimal, ParameterDirection.Input)
            inOrignRate.Value = DBNull.Value

            Dim inCrossRate As New Oracle.DataAccess.Client.OracleParameter("pCross_Rate", OracleDbType.Decimal, ParameterDirection.Input)
            inCrossRate.Value = DBNull.Value

            Dim inCrossRateOrign As New Oracle.DataAccess.Client.OracleParameter("pCross_Rate_Origin", OracleDbType.Varchar2, ParameterDirection.Input)
            inCrossRateOrign.Value = DBNull.Value
            inCrossRateOrign.Size = 200

            Dim inCrossRateReferenceNo As New Oracle.DataAccess.Client.OracleParameter("pCross_Rate_Ref_No", OracleDbType.Varchar2, ParameterDirection.Input)
            inCrossRateReferenceNo.Value = DBNull.Value
            inCrossRateReferenceNo.Size = 200


            ' OUTPUT Parameters
            Dim outSequenceNo As New Oracle.DataAccess.Client.OracleParameter("p_SEQ_NO", OracleDbType.Decimal, ParameterDirection.Output)
            outSequenceNo.Value = DBNull.Value

            Dim outErrorNo As New Oracle.DataAccess.Client.OracleParameter("p_Error_No", OracleDbType.Varchar2, ParameterDirection.Output)
            outErrorNo.Value = DBNull.Value
            outErrorNo.Size = 200

            Dim outErrorMsg As New Oracle.DataAccess.Client.OracleParameter("p_Message", OracleDbType.Varchar2, ParameterDirection.Output)
            outErrorMsg.Size = 200
            outErrorMsg.Value = DBNull.Value







            Dim con As New OracleConnection(constring)
            Dim objCmd As New OracleCommand()
            objCmd.Connection = con
            objCmd.CommandText = "TM_CREATE_GL"
            objCmd.BindByName = True
            objCmd.CommandType = CommandType.StoredProcedure

            objCmd.Parameters.Add(retValue)

            objCmd.Parameters.Add(inCorrID)
            objCmd.Parameters.Add(inApplication)
            objCmd.Parameters.Add(inWorkStation)
            objCmd.Parameters.Add(inScreenNo)
            objCmd.Parameters.Add(inPosition)
            objCmd.Parameters.Add(inGLPostingType)
            objCmd.Parameters.Add(inDebitTranKey)
            objCmd.Parameters.Add(inDebitGLCode)
            objCmd.Parameters.Add(inDebitCurrency)
            objCmd.Parameters.Add(inDebitBranchCode)
            objCmd.Parameters.Add(inDebitClientNo)
            objCmd.Parameters.Add(inDebitSequenceNo)
            objCmd.Parameters.Add(inDebitProfitCentre)
            objCmd.Parameters.Add(inDebitSourceModule)
            objCmd.Parameters.Add(inDebitSourceType)
            objCmd.Parameters.Add(inDebitTradeNo)
            objCmd.Parameters.Add(inDebitReference)
            objCmd.Parameters.Add(inDebitPostDate)
            objCmd.Parameters.Add(inDebitValueDate)
            objCmd.Parameters.Add(inDebitAmount)
            objCmd.Parameters.Add(inBaseEquivalent)
            objCmd.Parameters.Add(inDebitNarrative)
            objCmd.Parameters.Add(inDebitStatCode)
            objCmd.Parameters.Add(inExchangeRateType)
            objCmd.Parameters.Add(inFeeFlag)
            objCmd.Parameters.Add(inScType)
            objCmd.Parameters.Add(inFeeTaxFlag)
            objCmd.Parameters.Add(inPostingNatureIndicator)
            objCmd.Parameters.Add(inCreditTranKey)
            objCmd.Parameters.Add(inCreditGLCode)
            objCmd.Parameters.Add(inCreditCurrency)
            objCmd.Parameters.Add(inCreditBranchCode)
            objCmd.Parameters.Add(inCreditClientNo)
            objCmd.Parameters.Add(inCreditSequenceNo)
            objCmd.Parameters.Add(inCreditProfitCentre)
            objCmd.Parameters.Add(inCreditSourceModule)
            objCmd.Parameters.Add(inCreditSourceType)
            objCmd.Parameters.Add(inCreditTradeNo)
            objCmd.Parameters.Add(inCreditReference)
            objCmd.Parameters.Add(inCreditPostDate)
            objCmd.Parameters.Add(inCreditValueDate)
            objCmd.Parameters.Add(inCreditAmount)
            objCmd.Parameters.Add(inCreditNarrative)
            objCmd.Parameters.Add(inCreditStatCode)
            objCmd.Parameters.Add(inOrignRate)
            objCmd.Parameters.Add(inCrossRate)
            objCmd.Parameters.Add(inCrossRateOrign)
            objCmd.Parameters.Add(inCrossRateReferenceNo)

            objCmd.Parameters.Add(outSequenceNo)
            objCmd.Parameters.Add(outErrorNo)
            objCmd.Parameters.Add(outErrorMsg)


            objCmd.CommandTimeout = 36000
            con.Open()
            objCmd.ExecuteNonQuery()
            If Not retValue.Value Is DBNull.Value Then
                Dim retReturnValue As Integer = CType(retValue.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                Dim i As Integer = Nothing
                If Not outSequenceNo.Status = OracleParameterStatus.NullFetched Then
                    Dim retSequenceNo As Integer = CType(outSequenceNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
                End If
                Dim retErrorNO As String = Nothing

                If Not outErrorNo.Status = OracleParameterStatus.NullFetched Then
                    If Integer.TryParse(outErrorNo.Value.ToString, i) Then

                        retErrorNO = CType(outErrorNo.Value, Oracle.DataAccess.Types.OracleString).Value
                    Else

                        retErrorNO = "0"
                    End If
                Else
                    retErrorNO = "0"
                End If
                Dim retErrorMsg As String = Nothing
                Dim j As Integer = Nothing
                If Not outErrorMsg.Status = OracleParameterStatus.NullFetched Then
                    If Integer.TryParse(outErrorMsg.Value.ToString, j) Then
                        retErrorMsg = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value
                    Else
                        retErrorMsg = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value
                    End If
                End If

                If retErrorNO = "0" Then
                    Return True
                Else
                    Throw New Exception(retErrorMsg)
                End If
            Else
                Return False
            End If
            'If Not retValue.Value Is DBNull.Value Then
            '    '  Dim retReturnValue As Integer = CType(retValue.Value, Oracle.DataAccess.Types.OracleDecimal).Value
            '    Dim retSequenceNo As Integer = CType(outSequenceNo.Value, Oracle.DataAccess.Types.OracleDecimal).Value
            '    Dim retErrorNO As String = CType(outErrorNo.Value, Oracle.DataAccess.Types.OracleString).Value
            '    Dim retErrorMsg As String = CType(outErrorMsg.Value, Oracle.DataAccess.Types.OracleString).Value
            '    If retErrorNO = "0" Then
            '        Return True
            '    Else
            '        Throw New Exception(retErrorMsg)
            '    End If
            'Else
            '    Return False
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function





    Public Shared Function GetFunctionOFIRIS(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim constring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString.ToString()
            Dim Consql As New SqlConnection(constring)
            Dim da As New SqlDataAdapter
            If Consql.State = ConnectionState.Closed Then
                Consql.Open()
            Else
                Consql.Close()
            End If

            Dim cmd As New SqlCommand("dbo.SP_CMS_CASH_MANAGEMENT_SOA", Consql)
            'cmd.CommandText = SqlDataSourceCommandType.StoredProcedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Branch_Code", BranchCode)
            cmd.Parameters.AddWithValue("@p", p)
            cmd.Parameters.AddWithValue("@s", s)
            cmd.Parameters.AddWithValue("@m", m)
            cmd.Parameters.AddWithValue("@dateStart", dateStart)
            cmd.Parameters.AddWithValue("@dateEnd", dateEnd)
            cmd.Parameters.AddWithValue("@opt", opt)
            cmd.Parameters.AddWithValue("@Amt_Range_From", amt_range_from)
            cmd.Parameters.AddWithValue("@Amt_Range_to", amt_range_to)
            cmd.Parameters.AddWithValue("@Trans_Desc", Trans_Desc.ToString())

            da.SelectCommand = cmd
            da.Fill(dt)
            Dim st As String = ""

            'While (sqldr.Read)
            '    st = sqldr(0)
            'End While
            cmd.ExecuteReader()

            If Consql.State = ConnectionState.Open Then
                Consql.Close()


            End If
            Return dt
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    'Public Shared Function GetFunctionOFIRIS(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
    '    Try
    '        Dim dt As New DataTable
    '        Dim constring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString.ToString()
    '        Dim Consql As New SqlConnection(constring)
    '        Dim da As New SqlDataAdapter
    '        If Consql.State = ConnectionState.Closed Then
    '            Consql.Open()
    '        Else
    '            Consql.Close()
    '        End If

    '        Dim cmd As New SqlCommand("dbo.ZZ_SP_PR_IB_PRISM_INTERNET_BANKING", Consql)
    '        'cmd.CommandText = SqlDataSourceCommandType.StoredProcedure
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@Branch_Code", BranchCode)
    '        cmd.Parameters.AddWithValue("@p", p)
    '        cmd.Parameters.AddWithValue("@s", s)
    '        cmd.Parameters.AddWithValue("@m", m)
    '        cmd.Parameters.AddWithValue("@dateStart", dateStart)
    '        cmd.Parameters.AddWithValue("@dateEnd", dateEnd)
    '        cmd.Parameters.AddWithValue("@opt", opt)
    '        cmd.Parameters.AddWithValue("@Amt_Range_From", amt_range_from)
    '        cmd.Parameters.AddWithValue("@Amt_Range_to", amt_range_to)
    '        cmd.Parameters.AddWithValue("@Trans_Desc", Trans_Desc.ToString())

    '        da.SelectCommand = cmd
    '        da.Fill(dt)
    '        Dim st As String = ""

    '        'While (sqldr.Read)
    '        '    st = sqldr(0)
    '        'End While
    '        cmd.ExecuteReader()

    '        If Consql.State = ConnectionState.Open Then
    '            Consql.Close()


    '        End If
    '        Return dt
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

    Public Shared Function RemoveSpecialCharactersFromString(ByVal StringToReplace As String) As String
        StringToReplace = StringToReplace.Replace(";", " ")
        StringToReplace = StringToReplace.Replace("-", " ")
        StringToReplace = StringToReplace.Replace("|", " ")

        Return StringToReplace
    End Function
    Public Shared Function GetErrorMessageForTransactionFromResponseCode(ByVal ResponseCode As String, ByVal ResponseCategory As String) As String
        If ResponseCategory = "Business" Then
            Select Case ResponseCode
                Case "0000"
                    Return "OK"
                    Exit Function
                Case "TO"
                    Return "Time Out"
                    Exit Function
                Case "0001"
                    Return "Invalid Account"
                    Exit Function
                Case "0002"
                    Return "Account not authorized"
                    Exit Function
                Case "0003"
                    Return "Account detail not authorized"
                    Exit Function
                Case "0004"
                    Return "Account Closed"
                    Exit Function
                Case "0005"
                    Return "Account Dormant"
                    Exit Function
                Case "0006"
                    Return "Account Deceased"
                    Exit Function
                Case "0007"
                    Return "Debit Freeze"
                    Exit Function
                Case "0008"
                    Return "Credit Freeze"
                    Exit Function
                Case "0009"
                    Return "Invalid Transaction Type"
                    Exit Function
                Case "0010"
                    Return "Invalid Voucher Type"
                    Exit Function
                Case "0011"
                    Return "Invalid System Code"
                    Exit Function
                Case "0012"
                    Return "Invalid GL Account"
                    Exit Function
                Case "0013"
                    Return "In Sufficient Balance, Transaction not allowed"
                    Exit Function
                Case "0014"
                    Return "Invalid or Bad Amount"
                    Exit Function
                Case "0015"
                    Return "Invalid Input parameters/values missing"
                    Exit Function
                Case "0016"
                    Return "Invalid Date"
                    Exit Function
                Case "9001"
                    Return "Authentication Failure"
                    Exit Function
                Case "9002"
                    Return "System Failure"
                    Exit Function
                Case "9003"
                    Return "Process Failure"
                    Exit Function
                Case "9004"
                    Return "Transaction Timeout"
                    Exit Function
                Case "9005"
                    Return "Transaction not found"
                    Exit Function
                Case "9999"
                    Return "Any unknown error occurred during web method call"
                    Exit Function
                Case Else
                    Return "Invalid Response Code"
                    Exit Function
            End Select
        ElseIf ResponseCategory = "AccountStatus" Then
            Select Case ResponseCode
                Case "0000"
                    Return "OK"
                    Exit Function
                Case "TO"
                    Return "Time Out"
                    Exit Function
                Case "0001"
                    Return "Normal"
                    Exit Function
                Case "0004"
                    Return "Dormant"
                    Exit Function
                Case "0007"
                    Return "Unclaimed"
                    Exit Function
                Case "0008"
                    Return "Closed"
                    Exit Function
                Case "9005"
                    Return "Transaction not found"
                    Exit Function
                Case "9999"
                    Return "New"
                    Exit Function
                Case Else
                    Return "Invalid Response Code"
                    Exit Function
            End Select
        ElseIf ResponseCategory = "Account Restraints" Then
            Select Case ResponseCode
                Case "0000"
                    Return "OK"
                    Exit Function
                Case "TO"
                    Return "Time Out"
                    Exit Function
                Case "0001"
                    Return "Zakat Exemption"
                    Exit Function
                Case "0002"
                    Return "Tax Exemption on Profit"
                    Exit Function
                Case "0024"
                    Return "Debit Freeze"
                    Exit Function
                Case "0025"
                    Return "Credit Freeze"
                    Exit Function
                Case "0027"
                    Return "Incidental Charges Exemption"
                    Exit Function
                Case "0030"
                    Return "Tax Exemption on Cash Withdrawal"
                    Exit Function
                Case "0034"
                    Return "Cheque Book Issuance Freeze"
                    Exit Function
                Case "0035"
                    Return "ATM Card Issuance Freeze"
                    Exit Function
                Case "0036"
                    Return "Outward Clearing Freeze"
                    Exit Function
                Case "0035"
                    Return "ATM Card Issuance Freeze"
                    Exit Function
                Case Else
                    Return "Invalid Response Code"
                    Exit Function
            End Select
        ElseIf ResponseCategory = "EOD" Then
            Select Case ResponseCode
                Case "01"
                    Return "In Process"
                    Exit Function
                Case "02"
                    Return "Not Process"
                    Exit Function
                Case "TO"
                    Return "Time Out"
                    Exit Function
                Case Else
                    Return "Invalid Response Code"
                    Exit Function
            End Select
        End If

    End Function
    Public Shared Function GetOrganixzationCode() As String

        Try
            Return ICUtilities.GetSettingValue("OrganizationCode").ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetTestClientServiceAuthHeaderCredentials() As BEAPITransactionService.BEAPIAuthHeader
        Dim objBEAPIAuthHeader As New BEAPITransactionService.BEAPIAuthHeader
        Try
            objBEAPIAuthHeader.SystemCode = ICUtilities.GetSettingValue("SystemCode").ToString
            'objBEAPIAuthHeader.AnyAttr = ""
            objBEAPIAuthHeader.UserID = ICUtilities.GetSettingValue("WebServiceUserID").ToString
            objBEAPIAuthHeader.Password = ICUtilities.GetSettingValue("WebServicePassword").ToString
            Return objBEAPIAuthHeader
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetBasicBankAccountNoFromIBAN(ByVal OriginalBankAccountNumber As String) As String
        Dim BasicBankAccountNo As String = Nothing
        Try
            If OriginalBankAccountNumber.Length = 24 Then
                BasicBankAccountNo = OriginalBankAccountNumber.Substring(OriginalBankAccountNumber.Length - 15, 15)
            Else
                BasicBankAccountNo = OriginalBankAccountNumber
            End If
            Return BasicBankAccountNo
        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function
    Public Shared Function GetStanNo(ByVal logid As String) As String
        Try

            Dim STAN As String = ""
            STAN = logid.ToString()
            If STAN.ToString().Length <= 6 Then
                STAN = GetPaddedString(STAN.ToString(), dataelementtype.N, 6)
            Else
                STAN = STAN.ToString().Substring(STAN.ToString().Length - 6)
                STAN = GetPaddedString(STAN.ToString(), dataelementtype.N, 6)
            End If
            Return STAN
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Shared Function GetPaddedString(ByVal str As String, ByVal dataelementtype As dataelementtype, ByVal totallength As Integer) As String



        Select Case dataelementtype


            Case dataelementtype.A

                Return str.PadRight(totallength)



            Case dataelementtype.AN

                Return str.PadRight(totallength)


            Case dataelementtype.ANS

                Return str.PadRight(totallength)


            Case dataelementtype.N

                Return str.PadLeft(totallength, "0")

            Case dataelementtype.NR

                Return str.PadRight(totallength, "0")




            Case dataelementtype.X



                Return str


            Case dataelementtype.IMD


                Return str.PadRight(totallength, "0")


                'Fahad Pardes Card New Changes 31/10/2014
            Case dataelementtype.NS

                Return str.PadLeft(totallength, " ")





        End Select





    End Function



    Public Shared Function DoTransaction(ByRef StanNo As String, ByVal TxnRefNo As String, ByVal FromAccountBranchCode As String, ByVal FromAccountProductCode As String, ByVal FromAccountSchemeCode As String, ByVal FromAccountNumber As String, ByVal RBIndiCator As String, ByVal ToAccountBranchCode As String, ByVal ToAccountProductCode As String, ByVal ToAccountSchemeCode As String, ByVal ToAccountNumber As String, ByVal TxnType As String, ByVal VoucherType As String, ByVal InstrumrntType As String, ByVal DrGLAccount As String, ByVal CrGLAccount As String, ByVal InstrumentNo As String, ByVal TxnAmount As Double, ByVal TxnNaration As String, ByVal IsReversal As String, ByRef OriginatingTranIDNo As Integer, ByVal EntityType As String, ByVal FromAccountType As AccountType, ByVal ToAccountType As AccountType, ByVal TransactionBranchCode As String) As Boolean
        Dim objICSwtich As New IntelligenesSwitch.SwitchClient
        Dim objICInstruction As New ICInstruction
        Dim FinalInstrumentNo As String = Nothing
        Dim CustomNarration As String = Nothing
        Dim FinalNarration As String = Nothing
        Dim RefFieldsData As String = Nothing
        Dim ClubID As String = Nothing
        'Dim PaddedClubID As String = Nothing
        Dim ResponseCode As String = Nothing
        Dim Auth_UserID, Auth_IP, Auth_Password As String
        If InstrumentNo Is Nothing Or InstrumentNo = "" Then
            FinalInstrumentNo = Nothing
        Else
            If InstrumentNo.Contains("PO-") Then
                FinalInstrumentNo = " : Instrument No " & InstrumentNo.Replace("-", " ")
            ElseIf InstrumentNo.Contains("DD-") Then
                FinalInstrumentNo = " : Instrument No " & InstrumentNo.Replace("-", " ")
            Else
                FinalInstrumentNo = " : Instrument No " & InstrumentNo
            End If
        End If

        If EntityType = "Instruction" Then
            objICInstruction.LoadByPrimaryKey(TxnRefNo)
            If objICInstruction.ClubID IsNot Nothing Then
                ClubID = "ClubedInstruction " & objICInstruction.ClubID.ToString & " "
            End If
        End If

        Dim logID As Integer = 0

        logID = GetLogId(EntityType, TxnRefNo, FromAccountBranchCode & FromAccountNumber, FromAccountType.ToString, ToAccountBranchCode & ToAccountNumber, ToAccountType.ToString, "", "", TxnAmount.ToString, transactionmessagetype.FUNDS_TRANSFER)
        Auth_UserID = ""
        Auth_IP = ""
        Auth_Password = ""
        Try

            If GetEODStatus() = "02" Then
                GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
                If EntityType = "Instruction" Then
                    'CustomNarration = ClubID & "" & EntityType & " " & TxnRefNo & ":" & Date.Now.ToString("yyyy")
                    CustomNarration = "@" & ClubID & "" & EntityType & " " & TxnRefNo & FinalInstrumentNo
                ElseIf EntityType = "ClubedInstruction" Then
                    CustomNarration = "@" & EntityType & " " & TxnRefNo & FinalInstrumentNo
                Else
                    CustomNarration = "@" & EntityType & " " & TxnRefNo
                    'CustomNarration = EntityType & " " & TxnRefNo & " " & ":" & Date.Now.ToString("yyyy")
                End If
                If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
                    ResponseCode = "00-OK"
                Else
                    ResponseCode = objICSwtich.fnFundsTransferMessage(FromAccountNumber, FromAccountBranchCode, FromAccountType, ToAccountNumber, ToAccountBranchCode, ToAccountType, TxnAmount, CustomNarration, "1", TxnRefNo, logID, EntityType, Auth_UserID, Auth_Password, Auth_IP)
                End If

                ' ResponseCode = "00-OK"

                '' Check Transaction ResponseCode
                If ResponseCode.ToString.Split("-")(0) = "00" Then
                    UpdateCBLog(logID, "Complete", "OK")
                    Return True
                Else
                    Result = ResponseCode
                    Throw New Exception(Result)
                End If
            Else
                Throw New Exception("EOD: " & GetErrorMessageForTransactionFromResponseCode(GetEODStatus.ToString, "EOD"))
            End If
        Catch ex As Exception
            UpdateCBLog(logID, "Complete", "Error: " & ex.Message.ToString & " " & ex.Message.ToString)
            Throw ex
        End Try
    End Function
    Private Shared Function GetMaxClubIDFromInstruction() As Integer
        Dim qryObjICInstruction As New ICInstructionQuery("qryObjICInstruction")
        Dim dt As New DataTable
        qryObjICInstruction.Select(qryObjICInstruction.ClubID.Case.When(qryObjICInstruction.ClubID.Max.IsNull).Then(1).Else((qryObjICInstruction.ClubID.Max) + 1).End.As("ClubID"))
        dt = qryObjICInstruction.LoadDataTable()
        Return CInt(dt(0)(0))
    End Function
    Public Shared Function GetEODStatus() As String

        Try


            If ICUtilities.GetSettingValue("EODStatus") = "In Process" Then
                Result = "EOD: In Process"

                Throw New Exception(Result)

            ElseIf ICUtilities.GetSettingValue("EODStatus").ToString = "Not In Process" Then

                Result = "02"
                Return Result

            End If
        Catch ex As Exception

            Throw ex
        End Try
    End Function
    'Public Shared Function GetEODStatus() As String
    '    Dim objBEAPITxnEODRes As New BEAPITransactionService.EODStatusReponse
    '    Dim objBEAPITestTxn As New BEAPITransactionService.TestServiceClient
    '    Dim objBEAPITxnAuthHeader As New BEAPITransactionService.BEAPIAuthHeader




    '    '' Parameterized EOD checking
    '    If CompareTimeForEOD() = False Then
    '        Return "02"
    '        Exit Function
    '    End If


    '    Dim Result As String = ""
    '    Dim logID As Integer = 0
    '    logID = GetLogId("EOD", "", "", Nothing, "", "", "", "", 0, transactionmessagetype.EOD_STATUS)
    '    Try
    '        ''Get Authorityb Header object
    '        objBEAPITxnAuthHeader = GetTestClientServiceAuthHeaderCredentials()
    '        ' objBEAPITxnEODRes = objBEAPITestTxn.GetEODStatus(objBEAPITxnAuthHeader)
    '        objBEAPITxnEODRes.ResponseCode = "0000"
    '        objBEAPITxnEODRes.EODStatus = "02"
    '        If objBEAPITxnEODRes.ResponseCode = "0000" Then
    '            If objBEAPITxnEODRes.EODStatus = "01" Then
    '                Result = "EOD: " & GetErrorMessageForTransactionFromResponseCode(objBEAPITxnEODRes.EODStatus.ToString, "EOD")
    '                Throw New Exception(Result)
    '                UpdateCBLog(logID, "Complete", "OK " & GetErrorMessageForTransactionFromResponseCode(objBEAPITxnEODRes.EODStatus.ToString, "EOD"))
    '            ElseIf objBEAPITxnEODRes.EODStatus = "02" Then
    '                UpdateCBLog(logID, "Complete", "OK " & GetErrorMessageForTransactionFromResponseCode(objBEAPITxnEODRes.EODStatus.ToString, "EOD"))
    '                Result = "02"
    '            Else
    '                Result = GetErrorMessageForTransactionFromResponseCode(objBEAPITxnEODRes.ResponseCode.ToString, "Business")
    '                Throw New Exception(Result)
    '            End If
    '        Else
    '            Result = GetErrorMessageForTransactionFromResponseCode(objBEAPITxnEODRes.ResponseCode.ToString, "Business")
    '            Throw New Exception(Result)
    '        End If

    '        Return Result
    '    Catch ex As Exception
    '        UpdateCBLog(logID, "Complete", ex.Message.ToString)
    '        Throw ex
    '    End Try
    'End Function
    Public Shared Function GetTransactionStatus(ByVal StanNo As String) As String
        Dim objBEAPITxnStatusReq As New BEAPITransactionService.TransactionStatusRequest
        Dim objBEAPITxnStatusRes As New BEAPITransactionService.TransactionStatusResponse
        Dim objBEAPITestTxn As New BEAPITransactionService.TestServiceClient
        Dim objBEAPITxnAuthHeader As New BEAPITransactionService.BEAPIAuthHeader
        Dim Result As String = ""
        Dim logID As Integer = 0
        logID = GetLogId("Transaction Status", "", "", Nothing, "", "", "", "", 0, transactionmessagetype.TRANSACTION_STATUS)
        Try
            ''Get Authorityb Header object
            objBEAPITxnAuthHeader = GetTestClientServiceAuthHeaderCredentials()
            ''
            '' set stan No
            objBEAPITxnStatusReq.STAN = StanNo

            objBEAPITxnStatusRes = objBEAPITestTxn.GetTransactionStatus(objBEAPITxnAuthHeader, objBEAPITxnStatusReq)


            If objBEAPITxnStatusRes.ResponseCode = "0000" Then
                UpdateCBLog(logID, "Complete", "Transaction Status: " & objBEAPITxnStatusRes.ResponseCodeDescription)
                Result = objBEAPITxnStatusRes.ResponseCodeDescription
            Else
                Result = GetErrorMessageForTransactionFromResponseCode(objBEAPITxnStatusRes.ResponseCode.ToString, "Business")
                Throw New Exception(Result)
            End If
            Return Result
        Catch ex As Exception
            UpdateCBLog(logID, "Complete", "Transaction Status: " & ex.Message.ToString)
            Throw ex
        End Try
    End Function
    Public Shared Function GetTaggedGLByProductCodeAndSchemeCode(ByVal ProductCode As String, ByVal SchemeCode As String) As String

        Return ""

    End Function
    Public Shared Sub AddPendingEODInstruction(ByVal InstructionID As String, ByVal FromOriginalAccountNo As String,
                                              ByVal FromAccountType As String, ByVal FromAccountCurrency As String, ByVal DrGlCode As String, ByVal CrGlCode As String,
                                              ByVal CurrentStatus As String, ByVal FinalStatus As String, ByVal FailureStatus As String, ByVal UserID As String,
                                              ByVal ToOriginalAccountNo As String, ByVal ToAccountType As String, ByVal ToAccountCurrency As String,
                                              ByVal ProductTypeCode As String, ByVal IsUpdate As Boolean, ByVal IsReversal As Boolean,
                                              ByVal OriginatingTranType As String, ByVal RespondingTranType As String, ByVal ToAccountBranchCode As String, ByVal FromAccountBranchCode As String)
        Dim objICEODInstructionForSave As New ICPendingEODInstructions
        Dim objICInstruction As New ICInstruction

        objICInstruction.LoadByPrimaryKey(InstructionID)

        Dim FromAccountNo, FromAccountBrCode, FromAccounProductCode, FromAccountSchemeCode, OriginalFromAccountNo As String
        Dim ToAccountNo, ToAccountBrCode, ToAccounProductCode, ToAccountSchemeCode, OriginalToAccountNo As String


        If FromAccountType.ToString = "2" Then
            'FromAccountNo = GetBasicBankAccountNoFromIBAN(FromOriginalAccountNo)
            'FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
            'FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
            'FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
            If FromOriginalAccountNo.Length > 15 Then
                OriginalFromAccountNo = FromOriginalAccountNo
                FromAccountNo = Nothing
                FromAccountNo = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
                FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
                FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
                FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
            Else
                OriginalFromAccountNo = FromOriginalAccountNo
                FromAccountNo = Nothing
                FromAccountNo = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
                FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
                FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
                FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
            End If
        Else
            OriginalFromAccountNo = FromOriginalAccountNo
            FromAccountBrCode = FromAccountBranchCode
            FromAccounProductCode = ""
            FromAccountSchemeCode = ""
        End If
        If ToAccountType.ToString = "2" Then
            'ToAccountNo = GetBasicBankAccountNoFromIBAN(ToOriginalAccountNo)
            'ToAccountBrCode = ToAccountNo.Substring(FromAccountNo.Length - 15, 4)
            'ToAccounProductCode = ToAccountNo.Substring(FromAccountNo.Length - 11, 2)
            'ToAccountSchemeCode = ToAccountNo.Substring(FromAccountNo.Length - 9, 2)

            If ToOriginalAccountNo.Length > 15 Then
                OriginalToAccountNo = ToOriginalAccountNo
                BasicBankAccountNo2 = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
                ToAccountBrCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 15, 4)
                ToAccounProductCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 11, 2)
                ToAccountSchemeCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 9, 2)
            Else
                OriginalToAccountNo = ToOriginalAccountNo
                BasicBankAccountNo2 = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
                ToAccountBrCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 15, 4)
                ToAccounProductCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 11, 2)
                ToAccountSchemeCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 9, 2)
            End If
        Else
            OriginalToAccountNo = ToOriginalAccountNo
            ToAccountBrCode = ToAccountBranchCode
            ToAccounProductCode = ""
            ToAccountSchemeCode = ""
        End If









        objICEODInstructionForSave.InstructionID = InstructionID
        objICEODInstructionForSave.FromAccountNo = FromOriginalAccountNo
        objICEODInstructionForSave.FromAccountBranchCode = FromAccountBrCode
        objICEODInstructionForSave.FromAccountSchemeCode = FromAccountSchemeCode
        objICEODInstructionForSave.FromAccountProductCode = FromAccounProductCode
        objICEODInstructionForSave.FromAccountCurrency = FromAccountCurrency
        objICEODInstructionForSave.FromAccountType = FromAccountType
        objICEODInstructionForSave.ToAccountNo = ToOriginalAccountNo
        objICEODInstructionForSave.ToAccountBranchCode = ToAccountBrCode
        objICEODInstructionForSave.ToAccountSchemeCode = ToAccountSchemeCode
        objICEODInstructionForSave.ToAccountProductCode = ToAccounProductCode
        objICEODInstructionForSave.ToAccountCurrency = ToAccountCurrency
        objICEODInstructionForSave.ToAccountType = ToAccountType
        objICEODInstructionForSave.Status = objICInstruction.Status
        objICEODInstructionForSave.LastStatus = objICInstruction.LastStatus
        objICEODInstructionForSave.FinalStatus = FinalStatus
        objICEODInstructionForSave.Amount = objICInstruction.Amount
        objICEODInstructionForSave.LastProcessDate = Date.Now
        objICEODInstructionForSave.DrGlCode = DrGlCode
        objICEODInstructionForSave.CrGlCode = CrGlCode
        objICEODInstructionForSave.ProductTypeCode = ProductTypeCode
        objICEODInstructionForSave.IsProcessed = False
        objICEODInstructionForSave.FailureStatus = FailureStatus
        objICEODInstructionForSave.UserID = UserID
        objICEODInstructionForSave.OriginatingTranType = OriginatingTranType
        objICEODInstructionForSave.RespondingTranType = RespondingTranType
        objICEODInstructionForSave.IsReversal = IsReversal
        ICEODPendingInstructionController.AddPendingEODInstruction(objICEODInstructionForSave, IsUpdate, UserID, Nothing)
    End Sub
    'Public Shared Sub AddPendingEODInstruction(ByVal InstructionID As String, ByVal FromOriginalAccountNo As String,
    '                                          ByVal FromAccountType As String, ByVal FromAccountCurrency As String, ByVal DrGlCode As String, ByVal CrGlCode As String,
    '                                          ByVal CurrentStatus As String, ByVal FinalStatus As String, ByVal FailureStatus As String, ByVal UserID As String,
    '                                          ByVal ToOriginalAccountNo As String, ByVal ToAccountType As String, ByVal ToAccountCurrency As String,
    '                                          ByVal ProductTypeCode As String, ByVal IsUpdate As Boolean, ByVal IsReversal As Boolean,
    '                                          ByVal OriginatingTranType As String, ByVal RespondingTranType As String, ByVal ToAccountBranchCode As String, ByVal FromAccountBranchCode As String)
    '    Dim objICEODInstructionForSave As New ICPendingEODInstructions
    '    Dim objICInstruction As New ICInstruction

    '    objICInstruction.LoadByPrimaryKey(InstructionID)

    '    Dim FromAccountNo, FromAccountBrCode, FromAccounProductCode, FromAccountSchemeCode As String
    '    Dim ToAccountNo, ToAccountBrCode, ToAccounProductCode, ToAccountSchemeCode As String


    '    If FromAccountType.ToString = "2" Then
    '        'FromAccountNo = GetBasicBankAccountNoFromIBAN(FromOriginalAccountNo)
    '        'FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
    '        'FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
    '        'FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
    '        If FromOriginalAccountNo.Length > 15 Then
    '            OriginalFromAccountNo = FromOriginalAccountNo
    '            FromAccountNo = Nothing
    '            FromAccountNo = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
    '            FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
    '            FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
    '            FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
    '        Else
    '            OriginalFromAccountNo = FromOriginalAccountNo
    '            FromAccountNo = Nothing
    '            FromAccountNo = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
    '            FromAccountBrCode = FromAccountNo.Substring(FromAccountNo.Length - 15, 4)
    '            FromAccounProductCode = FromAccountNo.Substring(FromAccountNo.Length - 11, 2)
    '            FromAccountSchemeCode = FromAccountNo.Substring(FromAccountNo.Length - 9, 2)
    '        End If
    '    Else
    '        OriginalFromAccountNo = FromOriginalAccountNo
    '        FromAccountBrCode = FromAccountBranchCode
    '        FromAccounProductCode = ""
    '        FromAccountSchemeCode = ""
    '    End If
    '    If ToAccountType.ToString = "2" Then
    '        'ToAccountNo = GetBasicBankAccountNoFromIBAN(ToOriginalAccountNo)
    '        'ToAccountBrCode = ToAccountNo.Substring(FromAccountNo.Length - 15, 4)
    '        'ToAccounProductCode = ToAccountNo.Substring(FromAccountNo.Length - 11, 2)
    '        'ToAccountSchemeCode = ToAccountNo.Substring(FromAccountNo.Length - 9, 2)


    '        If ToOriginalAccountNo.Length > 15 Then
    '            OriginalToAccountNo = ToOriginalAccountNo
    '            ToAccountNumber = Nothing
    '            ToAccountNo = OriginalToAccountNo
    '            BasicBankAccountNo2 = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
    '            ToAccountBrCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 15, 4)
    '            ToAccounProductCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 11, 2)
    '            ToAccountSchemeCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 9, 2)
    '        Else
    '            OriginalToAccountNo = ToOriginalAccountNo
    '            ToAccountNumber = Nothing
    '            BasicBankAccountNo2 = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
    '            ToAccountBrCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 15, 4)
    '            ToAccounProductCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 11, 2)
    '            ToAccountSchemeCode = BasicBankAccountNo2.Substring(BasicBankAccountNo2.Length - 9, 2)
    '        End If
    '    Else
    '        OriginalToAccountNo = ToOriginalAccountNo
    '        ToAccountBrCode = ToAccountBranchCode
    '        ToAccounProductCode = ""
    '        ToAccountSchemeCode = ""
    '    End If









    '    objICEODInstructionForSave.InstructionID = InstructionID
    '    objICEODInstructionForSave.FromAccountNo = FromOriginalAccountNo
    '    objICEODInstructionForSave.FromAccountBranchCode = FromAccountBrCode
    '    objICEODInstructionForSave.FromAccountSchemeCode = FromAccountSchemeCode
    '    objICEODInstructionForSave.FromAccountProductCode = FromAccounProductCode
    '    objICEODInstructionForSave.FromAccountCurrency = FromAccountCurrency
    '    objICEODInstructionForSave.FromAccountType = FromAccountType
    '    objICEODInstructionForSave.ToAccountNo = ToOriginalAccountNo
    '    objICEODInstructionForSave.ToAccountBranchCode = ToAccountBrCode
    '    objICEODInstructionForSave.ToAccountSchemeCode = ToAccountSchemeCode
    '    objICEODInstructionForSave.ToAccountProductCode = ToAccounProductCode
    '    objICEODInstructionForSave.ToAccountCurrency = ToAccountCurrency
    '    objICEODInstructionForSave.ToAccountType = ToAccountType
    '    objICEODInstructionForSave.Status = objICInstruction.Status
    '    objICEODInstructionForSave.LastStatus = objICInstruction.LastStatus
    '    objICEODInstructionForSave.FinalStatus = FinalStatus
    '    objICEODInstructionForSave.Amount = objICInstruction.Amount
    '    objICEODInstructionForSave.LastProcessDate = Date.Now
    '    objICEODInstructionForSave.DrGlCode = DrGlCode
    '    objICEODInstructionForSave.CrGlCode = CrGlCode
    '    objICEODInstructionForSave.ProductTypeCode = ProductTypeCode
    '    objICEODInstructionForSave.IsProcessed = False
    '    objICEODInstructionForSave.FailureStatus = FailureStatus
    '    objICEODInstructionForSave.UserID = UserID
    '    objICEODInstructionForSave.OriginatingTranType = OriginatingTranType
    '    objICEODInstructionForSave.RespondingTranType = RespondingTranType
    '    ICEODPendingInstructionController.AddPendingEODInstruction(objICEODInstructionForSave, IsUpdate, UserID, Nothing)
    'End Sub

    Public Shared Function OpenEndedFundsTransfer(ByVal FromAcccountNumber As String, ByVal FromBranchCode As String, ByVal FromCurrency As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromAccountType As AccountType, ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String, ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType, ByVal TransactionAmount As Double, ByVal EntityType As String, ByVal RelatedID As String) As TransactionStatus



        'step 1: check both account statuses


        Dim FromAccountStatus, ToAccountStatus As String

        Dim TransactionStatus As New TransactionStatus
        If FromAccountType = AccountType.RB Then
            FromAccountStatus = CBUtilities.AccountStatus(FromAcccountNumber, FromAccountType, EntityType, RelatedID, "Debit", "OEFT", FromBranchCode)
        Else
            FromAccountStatus = "Active"
        End If


        If Not FromAccountStatus = "Active" Then


            Return TransactionStatus.From_Account_Not_Active



        End If


        If ToAccountType = AccountType.RB Then
            ToAccountStatus = CBUtilities.AccountStatus(ToAccountNumber, ToAccountType, EntityType, RelatedID, "Credit", "OEFT", ToBranchCode)
        Else
            ToAccountStatus = "Active"
        End If


        If Not ToAccountStatus = "Active" Then


            Return TransactionStatus.To_Account_Not_Active



        End If
        If FromCurrency.ToString.ToLower <> ToCurrency.ToString.ToLower Then
            Return TransactionStatus.Cross_Currency
        End If


        'step 2: check from account balance

        Dim balance As Double

        If FromAccountType = AccountType.RB Then
            balance = CBUtilities.BalanceInquiry(FromAcccountNumber, FromAccountType, EntityType, RelatedID, FromBranchCode)

        End If



        If Not balance = -1 Then


            If balance < TransactionAmount Then


                Return TransactionStatus.Insufficient_Balance_In_From_Account



            Else

                'perform funds transfer
                TransactionStatus = PerformFundsTransfer(FromAcccountNumber, FromBranchCode, FromCurrency, Nothing, Nothing, Nothing, FromAccountType, ToAccountNumber, ToBranchCode, ToCurrency, Nothing, Nothing, Nothing, ToAccountType, TransactionAmount, EntityType, RelatedID)



                Return TransactionStatus

            End If



        Else


            'error

            Return TransactionStatus.Error_In_Fetching_Account_Balance

        End If
    End Function



    'Public Shared Function OpenEndedFundsTransfer(ByVal FromAcccountNumber As String, ByVal FromBranchCode As String, ByVal FromCurrency As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromAccountType As AccountType, ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String, ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType, ByVal TransactionAmount As Double, ByVal EntityType As String, ByVal RelatedID As String) As TransactionStatus



    '    'step 1: check both account statuses


    '    Dim FromAccountStatus, ToAccountStatus As String

    '    Dim TransactionStatus As New TransactionStatus
    '    FromAccountStatus = CBUtilities.AccountStatus(FromAcccountNumber, FromAccountType, EntityType, RelatedID, "Debit", "OEFT")


    '    If Not FromAccountStatus = "Active" Then


    '        Return TransactionStatus.From_Account_Not_Active



    '    End If


    '    ToAccountStatus = CBUtilities.AccountStatus(ToAccountNumber, ToAccountType, EntityType, RelatedID, "Credit", "OEFT")


    '    If Not ToAccountStatus = "Active" Then


    '        Return TransactionStatus.To_Account_Not_Active



    '    End If
    '    If FromCurrency.ToString.ToLower <> ToCurrency.ToString.ToLower Then
    '        Return TransactionStatus.Cross_Currency
    '    End If


    '    'step 2: check from account balance

    '    Dim balance As Double


    '    balance = CBUtilities.BalanceInquiry(FromAcccountNumber, FromAccountType, EntityType, RelatedID)


    '    If Not balance = -1 Then


    '        If balance < TransactionAmount Then


    '            Return TransactionStatus.Insufficient_Balance_In_From_Account



    '        Else

    '            'perform funds transfer
    '            TransactionStatus = PerformFundsTransfer(FromAcccountNumber, FromBranchCode, FromCurrency, Nothing, Nothing, Nothing, FromAccountType, ToAccountNumber, ToBranchCode, ToCurrency, Nothing, Nothing, Nothing, ToAccountType, TransactionAmount, EntityType, RelatedID)



    '            Return TransactionStatus

    '        End If



    '    Else


    '        'error

    '        Return TransactionStatus.Error_In_Fetching_Account_Balance

    '    End If
    'End Function




    Private Shared Function PerformFundsTransfer(ByVal FromAcccountNumber As String, ByVal FromBranchCode As String, ByVal FromCurrency As String, ByVal FromClientNo As String, ByVal FromSeqNo As String, ByVal FromProfitCentre As String, ByVal FromAccountType As AccountType, ByVal ToAccountNumber As String, ByVal ToBranchCode As String, ByVal ToCurrency As String, ByVal ToClientNo As String, ByVal ToSeqNo As String, ByVal ToProfitCentre As String, ByVal ToAccountType As AccountType, ByVal TransactionAmount As Double, ByVal EntityType As String, ByVal RelatedID As String) As TransactionStatus

        ''Voucher Type aND iNSTRUMENT tYPE?


        Dim InterBranchTaggedGL As String = Nothing
        Dim DebitGLCode As String = Nothing
        Dim CreditGLCode As String = Nothing
        Dim transfertype As TransferType
        Dim BasicBankAccountNo As String = Nothing
        Dim BasicBankAccountNo2 As String = Nothing
        Dim OriginalFromAccountNo, FromAccountBrCode, FromAccountProductCode, FromAccountSchemeCode As String
        Dim OriginalToAccountNo, ToAccountBrCode, ToAccountProductCode, ToAccountSchemeCode As String
        Dim OriginatingIDNo As Integer = 0
        Dim InstrumentDate As Date = Now.Date

        Dim IsSecondLeg As Boolean = False
        Dim RespondingTranType As String = Nothing
        Dim OriginatingTranType As String = Nothing
        Dim ReversalTranType As String = Nothing
        Dim VoucherType As String = Nothing
        Dim InstrumentType As String = Nothing
        Dim StanNo As String = Nothing





        ''Get MBM No/Account No length 7 in case of RB
        If FromAccountType.ToString = "RB" Then
            If FromAcccountNumber.Length > 15 Then
                OriginalFromAccountNo = FromAcccountNumber
                FromAcccountNumber = Nothing
                FromAcccountNumber = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
                FromAccountBrCode = FromBranchCode
                FromAccountProductCode = ""
                FromAccountSchemeCode = ""
            Else
                OriginalFromAccountNo = FromAcccountNumber
                FromAcccountNumber = Nothing
                FromAcccountNumber = GetBasicBankAccountNoFromIBAN(OriginalFromAccountNo)
                FromAccountBrCode = FromBranchCode
                FromAccountProductCode = ""
                FromAccountSchemeCode = ""
            End If
        Else
            OriginalFromAccountNo = FromAcccountNumber
            FromAccountBrCode = FromBranchCode
            FromAccountProductCode = ""
            FromAccountSchemeCode = ""
        End If
        If ToAccountType.ToString = "RB" Then
            If ToAccountNumber.Length > 15 Then
                OriginalToAccountNo = ToAccountNumber
                ToAccountNumber = Nothing
                ToAccountNumber = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
                ToAccountBrCode = ToBranchCode
                ToAccountProductCode = ""
                ToAccountSchemeCode = ""

            Else
                OriginalToAccountNo = ToAccountNumber
                ToAccountNumber = Nothing
                ToAccountNumber = GetBasicBankAccountNoFromIBAN(OriginalToAccountNo)
                ToAccountBrCode = ToBranchCode
                ToAccountProductCode = ""
                ToAccountSchemeCode = ""
            End If
        Else
            OriginalToAccountNo = ToAccountNumber
            ToAccountBrCode = ToBranchCode
            ToAccountProductCode = ""
            ToAccountSchemeCode = ""
        End If

        '' Customer Debit GL in RB
        DebitGLCode = GetTaggedGLByProductCodeAndSchemeCode(FromAccountProductCode, FromAccountSchemeCode)
        '' SBL Contra GL for CMS
        InterBranchTaggedGL = ""
        '' Customer Credit GL in RB
        CreditGLCode = GetTaggedGLByProductCodeAndSchemeCode(ToAccountProductCode, ToAccountSchemeCode)



        ''Get Tran Types FT from IC_Settings


        'OriginatingTranType = ICUtilities.GetSettingValue("FTOriginatingTranType")
        'RespondingTranType = ICUtilities.GetSettingValue("FTRespondingTranType")
        'ReversalTranType = ICUtilities.GetSettingValue("ReversalTranType")
        'VoucherType = ICUtilities.GetSettingValue("VoucherType")
        'InstrumentType = ICUtilities.GetSettingValue("InstrumentType")


        OriginatingTranType = ""
        RespondingTranType = ""
        ReversalTranType = ""
        VoucherType = ""
        InstrumentType = ""


        If FromAccountType = AccountType.GL And ToAccountType = AccountType.GL Then

            transfertype = CBUtilities.TransferType.GL_TO_GL



        End If


        If FromAccountType = AccountType.GL And ToAccountType = AccountType.RB Then

            transfertype = CBUtilities.TransferType.GL_TO_RB




        End If


        If FromAccountType = AccountType.RB And ToAccountType = AccountType.GL Then

            transfertype = CBUtilities.TransferType.RB_TO_GL





        End If

        If FromAccountType = AccountType.RB And ToAccountType = AccountType.RB Then

            transfertype = CBUtilities.TransferType.RB_TO_RB








        End If


        ''Do Transaction
        Select Case transfertype

            Case CBUtilities.TransferType.GL_TO_GL
                Try

                    If DoTransaction(StanNo, RelatedID, FromAccountBrCode, Nothing, Nothing, FromAcccountNumber, "0", ToAccountBrCode, Nothing, Nothing, ToAccountNumber, OriginatingTranType, VoucherType, InstrumentType, DebitGLCode, CreditGLCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                        Return TransactionStatus.Successfull

                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If


                Catch ex As Exception
                    If ex.Message.Contains("EOD: ") Then
                        Return TransactionStatus.EOD
                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If
                End Try
            Case CBUtilities.TransferType.GL_TO_RB
                Try

                    If DoTransaction(StanNo, RelatedID, FromAccountBrCode, Nothing, Nothing, FromAcccountNumber, "0", ToAccountBrCode, Nothing, Nothing, ToAccountNumber, OriginatingTranType, VoucherType, InstrumentType, DebitGLCode, CreditGLCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                        Return TransactionStatus.Successfull

                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If


                Catch ex As Exception
                    If ex.Message.Contains("EOD: ") Then
                        Return TransactionStatus.EOD
                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If
                End Try
            Case CBUtilities.TransferType.RB_TO_GL
                Try

                    If DoTransaction(StanNo, RelatedID, FromAccountBrCode, Nothing, Nothing, FromAcccountNumber, "0", ToAccountBrCode, Nothing, Nothing, ToAccountNumber, OriginatingTranType, VoucherType, InstrumentType, DebitGLCode, CreditGLCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                        Return TransactionStatus.Successfull

                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If


                Catch ex As Exception
                    If ex.Message.Contains("EOD: ") Then
                        Return TransactionStatus.EOD
                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If
                End Try
            Case CBUtilities.TransferType.RB_TO_RB
                Try

                    If DoTransaction(StanNo, RelatedID, FromAccountBrCode, Nothing, Nothing, FromAcccountNumber, "0", ToAccountBrCode, Nothing, Nothing, ToAccountNumber, OriginatingTranType, VoucherType, InstrumentType, DebitGLCode, CreditGLCode, Nothing, TransactionAmount, RelatedID, "N", OriginatingIDNo, EntityType, FromAccountType, ToAccountType, FromAccountBrCode) Then
                        Return TransactionStatus.Successfull

                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If


                Catch ex As Exception
                    If ex.Message.Contains("EOD: ") Then
                        Return TransactionStatus.EOD
                    Else
                        Return TransactionStatus.UnSuccessfull
                    End If
                End Try




        End Select



    End Function
    Public Shared Function GetErrorMessageFromOpenEndedFundsTransferResponseCode(ByVal ResponseStatus As TransactionStatus) As String
        Select Case ResponseStatus
            Case TransactionStatus.EOD
                Return "EOD"
            Case TransactionStatus.UnSuccessfull
                Return "Error"
            Case TransactionStatus.Successfull
                Return "OK"
            Case TransactionStatus.Insufficient_Balance_In_From_Account
                Return "Insufficient_Balance_In_From_Account"
            Case TransactionStatus.From_Account_Not_Active
                Return "From_Account_Not_Active"
            Case TransactionStatus.To_Account_Not_Active
                Return "To_Account_Not_Active"
            Case TransactionStatus.Error_In_Fetching_Account_Balance
                Return "Error_In_Fetching_Account_Balance"
            Case TransactionStatus.Cross_Currency
                Return "Cross_Currency"
            Case Else
                Return "Invalid_Transaction_Status"
        End Select
    End Function
    Public Shared Function CompareTimeForEOD() As Boolean
        Dim Result As Boolean = False
        Dim EODStartTimeValue As String = ICUtilities.GetSettingValue("EODStartTime")
        Dim EODEndTimeValue As String = ICUtilities.GetSettingValue("EODEndTime")

        Dim EODStartTime As DateTime = Convert.ToDateTime(EODStartTimeValue).Hour & ":" & Convert.ToDateTime(EODStartTimeValue).Minute
        Dim EODSEndTime As DateTime = Convert.ToDateTime(EODEndTimeValue).Hour & ":" & Convert.ToDateTime(EODEndTimeValue).Minute
        Dim CurrentTime As DateTime = Date.Now.Hour & ":" & Date.Now.Minute
        If CurrentTime >= EODStartTime And EODSEndTime <= CurrentTime Then
            Result = True
        End If

        Return Result
    End Function
    '' First Leg Reversal Work Javed 23 June 2014

    Public Shared Function AddICFirstLegRevInstruction(ByRef StanNo As String, ByVal TxnRefNo As String, ByVal FromAccountBranchCode As String, ByVal FromAccountProductCode As String, ByVal FromAccountSchemeCode As String, ByVal FromAccountNumber As String, ByVal RBIndiCator As String, ByVal ToAccountBranchCode As String, ByVal ToAccountProductCode As String, ByVal ToAccountSchemeCode As String, ByVal ToAccountNumber As String, ByVal TxnType As String, ByVal VoucherType As String, ByVal InstrumrntType As String, ByVal DrGLAccount As String, ByVal CrGLAccount As String, ByVal InstrumentNo As String, ByVal TxnAmount As Double, ByVal TxnNaration As String, ByVal IsReversal As String, ByRef OriginatingTranIDNo As Integer, ByVal EntityType As String, ByVal FromAccountType As AccountType, ByVal ToAccountType As AccountType, ByVal TransactionBranchCode As String, ByVal FromAccountCurrency As String, ByVal ToAccountCurrency As String, ByVal UsersID As String, ByVal UsersName As String, ByVal ErrorMessage As String, ByVal LastStatus As String, ByVal IsFirstLegReversed As Boolean, ByVal CurrentStatus As String, Optional ByVal ProductTypeCode As String = Nothing, Optional ByVal ReversalType As String = Nothing) As Boolean
        Dim objICFLRevForSave As New ICFirstLegReversedInstructions

        objICFLRevForSave.RelatedID = TxnRefNo
        objICFLRevForSave.EntityType = EntityType
        objICFLRevForSave.Amount = TxnAmount
        objICFLRevForSave.FromAccountNo = FromAccountNumber
        objICFLRevForSave.FromAccountBranchCode = FromAccountBranchCode
        objICFLRevForSave.FromAccountCurrency = FromAccountCurrency
        objICFLRevForSave.FromAccountProductCode = FromAccountProductCode
        objICFLRevForSave.FromAccountSchemeCode = FromAccountSchemeCode
        objICFLRevForSave.FromAccountType = FromAccountType
        objICFLRevForSave.ToAccountNo = ToAccountNumber
        objICFLRevForSave.ToAccountBranchCode = ToAccountBranchCode
        objICFLRevForSave.ToAccountCurrency = ToAccountCurrency
        objICFLRevForSave.ToAccountProductCode = ToAccountProductCode
        objICFLRevForSave.ToAccountSchemeCode = ToAccountSchemeCode
        objICFLRevForSave.ToAccountType = ToAccountType
        objICFLRevForSave.RBIndicator = RBIndiCator
        objICFLRevForSave.TxnType = TxnType
        objICFLRevForSave.VoucherType = VoucherType
        objICFLRevForSave.InstrumentType = InstrumrntType
        objICFLRevForSave.DebitGLCode = DrGLAccount
        objICFLRevForSave.CreditGLCode = CrGLAccount
        objICFLRevForSave.InstrumentNo = InstrumentNo
        objICFLRevForSave.IsReversal = IsReversal
        objICFLRevForSave.TransactionBranchCode = TransactionBranchCode
        objICFLRevForSave.Creator = UsersID
        objICFLRevForSave.CreationDate = Date.Now
        objICFLRevForSave.ErrorMessage = ErrorMessage
        objICFLRevForSave.LastStatus = LastStatus
        objICFLRevForSave.IsFirstLEgReversed = IsFirstLegReversed
        objICFLRevForSave.Status = CurrentStatus
        objICFLRevForSave.ProductTypeCode = ProductTypeCode
        objICFLRevForSave.ReversalType = ReversalType
        ICFirstLegReversedInstructionsController.AddICFirstLegRevInstruction(objICFLRevForSave, False, UsersID, UsersName)
        Return True
    End Function
    'Public Shared Function BillInquiry(ByVal EntityType As String, ByVal RelatedID As String, ByVal CustomerID As String, ByVal UBPCompanyCode As String, ByVal UBPReferenceNo As String, ByVal FlagToCalculateFee As String, ByVal BillAmount As String, ByRef RetRefNo As String, ByVal FromAccountBranchCode As String, ByVal FromAccountNo As String) As DataTable
    '    Dim logID As Integer = 0
    '    Try






    '        Dim msgStr As String = ""
    '        Dim response As String = ""
    '        Dim TotalAmount As String = ""
    '        Dim DueDate As String = ""
    '        Dim BillStatus As String = ""
    '        Dim Stan As String = Nothing
    '        Dim objICUBPCompany As New ICUBPSCompany
    '        Dim objSwitch As New IntelligenesSwitch.SwitchClient
    '        Dim dt As New DataTable
    '        Dim dr As DataRow
    '        RetRefNo = Nothing
    '        Dim Auth_UserID, Auth_IP, Auth_Password As String
    '        Auth_UserID = ""
    '        Auth_IP = ""
    '        Auth_Password = ""
    '        GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
    '        objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode)
    '        logID = GetLogId(EntityType, RelatedID, UBPReferenceNo, "", "", "", UBPCompanyCode, "", 0, transactionmessagetype.BILL_INQUIRY)
    '        Stan = GetStanNo()
    '        'response = objSwitch.BillInquiryEuronet(objICUBPCompany.CompanyCode, FromAccountBranchCode & FromAccountNo, UBPReferenceNo, BillAmount, Stan, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)
    '        If UBPReferenceNo.StartsWith("1") Then
    '            response = Date.Now.ToString("ddMMyyyy hhmmss").Replace(" ", "") & "|" & CDate(Date.Now).Month & CDate(Date.Now).Year & "|U|100.00|" & Date.Now.ToString() & "|110.00|Within due date subscriber"

    '        ElseIf UBPReferenceNo.StartsWith("2") Then

    '            response = Date.Now.ToString("ddMMyyyy hh:mm:ss").Replace(" ", "") & "|" & CDate(Date.Now).Month & CDate(Date.Now).Year & "|U|100.00|" & Date.Now.AddDays(-1).ToString() & "|110.00|After due date subscriber"

    '        End If
    '        If response <> "Timed Out" And Not response.Substring(0, 5).ToString.Contains("Error") Then
    '            dt.Columns.Add(New DataColumn("RetrievalReferenceNumber", GetType(System.String)))
    '            dt.Columns.Add(New DataColumn("ResponseResult", GetType(System.String)))
    '            dt.Columns.Add(New DataColumn("BillingMonth", GetType(System.String)))
    '            dt.Columns.Add(New DataColumn("AmountWithInDueDate", GetType(System.String))) ' AN 8
    '            dt.Columns.Add(New DataColumn("DueDate", GetType(System.String))) ' AN 24
    '            dt.Columns.Add(New DataColumn("BillStatus", GetType(System.String))) ' AN 30
    '            dt.Columns.Add(New DataColumn("AmountAfterDueDate", GetType(System.String)))
    '            dt.Columns.Add(New DataColumn("SubscriberName", GetType(System.String)))

    '            If response.Contains("|") Then
    '                If response.ToString.Split("|")(2) = "306" Then

    '                    Throw New Exception("Bill already paid")

    '                Else
    '                    dr = dt.NewRow()
    '                    dr("ResponseResult") = "Valid"
    '                    dr("RetrievalReferenceNumber") = response.ToString.Split("|")(0)
    '                    RetRefNo = response.ToString.Split("|")(0)
    '                    dr("BillingMonth") = response.ToString.Split("|")(1)
    '                    dr("AmountWithInDueDate") = response.ToString.Split("|")(3)
    '                    dr("DueDate") = response.ToString.Split("|")(4)
    '                    dr("BillStatus") = response.ToString.Split("|")(2)
    '                    dr("AmountAfterDueDate") = response.ToString.Split("|")(5)
    '                    dr("SubscriberName") = response.ToString.Split("|")(6)
    '                    dt.Rows.Add(dr)
    '                    Return dt
    '                End If

    '            Else
    '                Throw New Exception("Invalid response")
    '            End If

    '            ''subcriber name
    '            ''Checking on pipe separated
    '            ''306


    '            'objEuronetResponseMessage.RetrievalReferenceNumber.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillingMonth.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillStatus.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountBeforeDueDate.ToString()) / 100 & "|" & objEuronetResponseMessage.AdditionalInformation.DueDate.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountAfterDueDate.ToString()) / 100

    '        Else
    '            Throw New Exception(response)
    '        End If

    '    Catch ex As Exception
    '        UpdateCBLog(logID, "Error", ex.Message.ToString)
    '        Throw ex
    '    End Try
    'End Function
    Public Shared Function BillInquiry(ByVal EntityType As String, ByVal RelatedID As String, ByVal CustomerID As String, ByVal UBPCompanyCode As String, ByVal UBPReferenceNo As String, ByVal FlagToCalculateFee As String, ByVal BillAmount As String, ByRef RetRefNo As String, ByVal FromAccountBranchCode As String, ByVal FromAccountNo As String) As DataTable
        Dim logID As Integer = 0
        Try
            Dim msgStr As String = ""
            Dim response As String = ""
            Dim TotalAmount As String = ""
            Dim DueDate As String = ""
            Dim BillStatus As String = ""
            Dim Stan As String = Nothing
            Dim objICUBPCompany As New ICUBPSCompany

            Dim objSwitch As New IntelligenesSwitch.SwitchClient
            Dim dt As New DataTable
            Dim dr As DataRow
            RetRefNo = Nothing
            Dim Auth_UserID, Auth_IP, Auth_Password As String
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode)
            logID = GetLogId(EntityType, RelatedID, UBPReferenceNo, "", "", "", UBPCompanyCode, "", 0, transactionmessagetype.BILL_INQUIRY)
            Stan = GetStanNo(logID)
            response = objSwitch.BillInquiryEuronet(objICUBPCompany.CompanyCode, FromAccountBranchCode & FromAccountNo, UBPReferenceNo, BillAmount, Stan, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)
            If response <> "Timed Out" And Not response.Substring(0, 5).ToString.Contains("Error") Then
                dt.Columns.Add(New DataColumn("RetrievalReferenceNumber", GetType(System.String)))
                dt.Columns.Add(New DataColumn("ResponseResult", GetType(System.String)))
                dt.Columns.Add(New DataColumn("BillingMonth", GetType(System.String)))
                dt.Columns.Add(New DataColumn("AmountWithInDueDate", GetType(System.String))) ' AN 8
                dt.Columns.Add(New DataColumn("DueDate", GetType(System.DateTime))) ' AN 24
                dt.Columns.Add(New DataColumn("BillStatus", GetType(System.String))) ' AN 30
                dt.Columns.Add(New DataColumn("AmountAfterDueDate", GetType(System.String)))
                dt.Columns.Add(New DataColumn("SubscriberName", GetType(System.String)))

                If response.Contains("|") Then
                    If response.ToString.Split("|")(2) = "306" Then
                        UpdateCBLog(logID, "Complete", "Bill already paid")
                        Return dt
                        Throw New Exception("Bill already paid")

                    Else
                        dr = dt.NewRow()
                        dr("ResponseResult") = "Valid"
                        dr("RetrievalReferenceNumber") = response.ToString.Split("|")(0)
                        RetRefNo = response.ToString.Split("|")(0)
                        dr("BillingMonth") = response.ToString.Split("|")(1)
                        dr("AmountWithInDueDate") = response.ToString.Split("|")(3)
                        Dim DueDates As New Date(response.ToString.Split("|")(4).Substring(0, 4), response.ToString.Split("|")(4).Substring(4, 2), response.ToString.Split("|")(4).Substring(6, 2))
                        dr("DueDate") = DueDates
                        dr("BillStatus") = response.ToString.Split("|")(2)
                        dr("AmountAfterDueDate") = response.ToString.Split("|")(5)
                        dr("SubscriberName") = response.ToString.Split("|")(6)
                        dt.Rows.Add(dr)
                        UpdateCBLog(logID, "Complete", "OK")
                        Return dt
                    End If

                Else
                    UpdateCBLog(logID, "Error", "Invalid response : " & response)
                    Throw New Exception("Invalid response")
                End If

                ''subcriber name
                ''Checking on pipe separated
                ''306


                'objEuronetResponseMessage.RetrievalReferenceNumber.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillingMonth.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillStatus.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountBeforeDueDate.ToString()) / 100 & "|" & objEuronetResponseMessage.AdditionalInformation.DueDate.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountAfterDueDate.ToString()) / 100

            Else
                UpdateCBLog(logID, "Error", "Response : " & response)

                Throw New Exception(response)
            End If

        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message.ToString)
            Throw ex
        End Try
    End Function
    Public Shared Function BillInquiryForBene(ByVal EntityType As String, ByVal RelatedID As String, ByVal CustomerID As String, ByVal UBPCompanyCode As String, ByVal UBPReferenceNo As String, ByVal FlagToCalculateFee As String, ByVal BillAmount As String, ByRef RetRefNo As String, ByVal FromAccountBranchCode As String, ByVal FromAccountNo As String) As DataTable
        Dim logID As Integer = 0
        Try
            Dim msgStr As String = ""
            Dim response As String = ""
            Dim TotalAmount As String = ""
            Dim DueDate As String = ""
            Dim BillStatus As String = ""
            Dim Stan As String = Nothing
            Dim objICUBPCompany As New ICUBPSCompany
            Dim objSwitch As New IntelligenesSwitch.SwitchClient
            Dim dt As New DataTable
            Dim dr As DataRow
            RetRefNo = Nothing
            Dim Auth_UserID, Auth_IP, Auth_Password As String
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            objICUBPCompany.LoadByPrimaryKey(UBPCompanyCode)
            logID = GetLogId(EntityType, RelatedID, UBPReferenceNo, "", "", "", UBPCompanyCode, "", 0, transactionmessagetype.BILL_INQUIRY)
            Stan = GetStanNo(logID)
            response = objSwitch.BillInquiryEuronet(objICUBPCompany.CompanyCode, FromAccountBranchCode & FromAccountNo, UBPReferenceNo, BillAmount, Stan, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)
            'response = "001|" & Date.Now.Month & Date.Now.Year & "|000|100|" & Now.Date & "|110|Test Subscriber"
            If response <> "Timed Out" And Not response.Substring(0, 5).ToString.Contains("Error") Then
                dt.Columns.Add(New DataColumn("RetrievalReferenceNumber", GetType(System.String)))
                dt.Columns.Add(New DataColumn("ResponseResult", GetType(System.String)))
                dt.Columns.Add(New DataColumn("BillingMonth", GetType(System.String)))
                dt.Columns.Add(New DataColumn("AmountWithInDueDate", GetType(System.String))) ' AN 8
                dt.Columns.Add(New DataColumn("DueDate", GetType(System.DateTime))) ' AN 24
                dt.Columns.Add(New DataColumn("BillStatus", GetType(System.String))) ' AN 30
                dt.Columns.Add(New DataColumn("AmountAfterDueDate", GetType(System.String)))
                dt.Columns.Add(New DataColumn("SubscriberName", GetType(System.String)))

                If response.Contains("|") Then

                    dr = dt.NewRow()
                    dr("ResponseResult") = "Valid"
                    dr("RetrievalReferenceNumber") = response.ToString.Split("|")(0)
                    RetRefNo = response.ToString.Split("|")(0)
                    dr("BillingMonth") = response.ToString.Split("|")(1)
                    dr("AmountWithInDueDate") = response.ToString.Split("|")(3)
                    Dim DueDates As New Date(response.ToString.Split("|")(4).Substring(0, 4), response.ToString.Split("|")(4).Substring(4, 2), response.ToString.Split("|")(4).Substring(6, 2))
                    dr("DueDate") = DueDates
                    dr("BillStatus") = response.ToString.Split("|")(2)
                    dr("AmountAfterDueDate") = response.ToString.Split("|")(5)
                    dr("SubscriberName") = response.ToString.Split("|")(6)
                    dt.Rows.Add(dr)
                    UpdateCBLog(logID, "Complete", "OK")
                    Return dt


                Else
                    UpdateCBLog(logID, "Error", "Invalid response : " & response)
                    Throw New Exception("Invalid response")
                End If

                ''subcriber name
                ''Checking on pipe separated
                ''306


                'objEuronetResponseMessage.RetrievalReferenceNumber.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillingMonth.ToString() & "|" & objEuronetResponseMessage.AdditionalInformation.BillStatus.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountBeforeDueDate.ToString()) / 100 & "|" & objEuronetResponseMessage.AdditionalInformation.DueDate.ToString() & "|" & CDbl(objEuronetResponseMessage.AdditionalInformation.AmountAfterDueDate.ToString()) / 100

            Else
                UpdateCBLog(logID, "Error", "Response : " & response)
                Throw New Exception(response)
            End If

        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message.ToString)
            Throw ex
        End Try
    End Function

    Private Shared Function GetResponseMessageFromResponseCodeForBillInquiry(ByVal responsecode As String) As String

        Select Case responsecode
            Case "TO"
                Return "Time Out"
            Case "00"
                Return "Success"
            Case "86"
                Return "Malformed Index in relationship inquiry"
            Case "87"
                Return "Invalid Index"
            Case "88"
                Return "Customer Not found"
            Case "89"
                Return "Bill already paid"
            Case "90"
                Return "Transaction not supported"
            Case "91"
                Return "Utility Company Id not found"
            Case "92"
                Return "Relationships not found"
            Case "94"
                Return "Bill not found"
            Case "97"
                Return "Internal/Unspecified Error"
            Case Else
                Return "Invalid_Response_Code"
        End Select
    End Function
    Public Shared Function DoBillPaymentAdvice(ByVal CustomerID As String, ByVal UtitlityCompanyCode As String, ByVal UBPReferenceFieldNumber As String, _
                                              ByVal PayableAmount As String, ByVal FromAccountNo As String, ByVal FromAccountType As String, _
                                                 ByVal FromAccountCurrency As String, ByVal EntityType As String, ByVal RelatedID As String, _
                                                  ByVal sPIN As String, ByRef sRRN As String, ByVal FromCBAccountType As AccountType, ByVal RetreivalRefNo As String) As Boolean

        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return True
        End If
        Dim logID As Integer = 0
        Dim objInstruction As New ICInstruction
        Dim objICSwitch As New IntelligenesSwitch.SwitchClient
        Dim StrAudtTrail As String = Nothing
        Dim response As String = ""
        Dim AuthID As String = Nothing
        Dim OriginalTxnDateTime As String = Nothing
        Dim Result As Boolean = False
        Dim ResponseMessage As String = Nothing

        Try
            Dim Auth_UserID, Auth_IP, Auth_Password As String
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            logID = GetLogId(EntityType, RelatedID, FromAccountNo, FromCBAccountType, "", "", UtitlityCompanyCode, "", PayableAmount, transactionmessagetype.BILL_PAYMENT)
            sRRN = Nothing
            sRRN = GetStanNo(logID)
            If EntityType.ToString() = "Instruction" Then
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                'OriginalTxnDateTime = CDate(objInstruction.CreateDate).ToString("yyyyMMddhhmmss")
            End If
            OriginalTxnDateTime = CDate(Date.Now).ToString("yyyyMMddhhmmss")
            'Update Instruction Status as Pending CB
            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, "39", Nothing, Nothing, "", "PENDING CB")
            objInstruction = New ICInstruction
            objInstruction.LoadByPrimaryKey(RelatedID)

            '' Do IBFT Transaction
            response = objICSwitch.BillPaymentEuronet(UtitlityCompanyCode, FromAccountNo, UBPReferenceFieldNumber, PayableAmount, sRRN, "Hex", RetreivalRefNo, 1, Auth_UserID, Auth_Password, Auth_IP)
            'response = "OK"
            If response = "OK" Then
                UpdateCBLog(logID, "Complete", "OK")
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                'Update Instruction Status to Last Status
                StrAudtTrail = Nothing
                StrAudtTrail += "Instruction With ID [ " & objInstruction.InstructionID & " ] assigned STAN No [ " & sRRN & " ]"
                objInstruction.IBFTStan = sRRN
                ICInstructionController.UpDateInstruction(objInstruction, Nothing, Nothing, StrAudtTrail, "UPDATE")
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                Result = True
            Else
                objInstruction = New ICInstruction
                objInstruction.LoadByPrimaryKey(RelatedID)
                UpdateCBLog(logID, "Complete", "Unable to Bill Payment due to " & GetResponseMessageFromResponseCode(response).ToString())
                StrAudtTrail = Nothing
                StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to failure in Bill Payment."
                ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
                'Update Instruction Status to Last Status
                ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
                Result = False
            End If

            Return Result
        Catch ex As Exception
            UpdateCBLog(logID, "Error", ex.Message)
            objInstruction = New ICInstruction
            objInstruction.LoadByPrimaryKey(RelatedID)
            'Update Instruction Status to Last Status
            StrAudtTrail = Nothing
            StrAudtTrail += "Instruction Skipped: Instruction With ID [ " & objInstruction.InstructionID & " ] skipped due to " & ex.Message.ToString
            ICUtilities.AddAuditTrail(StrAudtTrail, "Instruction", objInstruction.InstructionID.ToString, Nothing, Nothing, "SKIP")
            ICInstructionController.UpdateInstructionStatus(objInstruction.InstructionID, objInstruction.Status, objInstruction.LastStatus, Nothing, Nothing, "", "REVERT TO PREVIOUS STATUS")
            Return False
        End Try

    End Function
End Class
