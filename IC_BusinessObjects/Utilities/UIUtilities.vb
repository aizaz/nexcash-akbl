﻿Imports System.Web
Imports Microsoft.VisualBasic
Imports ICBO.IC
Imports System
Namespace IC
    Public Enum Dialogmessagetype



        Success = 1
        Failure = 2
        Warning = 3




    End Enum

    Public Class UIUtilities

        Public Shared Sub ShowDialog(ByVal obj As  DotNetNuke.Entities.Modules.PortalModuleBase, ByVal title As String, ByVal message As String, ByVal msgtype As Dialogmessagetype)


            Dim scr As String

            Dim generalError As String() = ICUtilities.GetSettingValue("GeneralError").ToString().Split(",")

            If generalError.Any(Function(x) message.ToLower().Contains(x.ToLower())) Then
                message = ICUtilities.GetSettingValue("GeneralErrorMessage").ToString()
            End If

            message = message.Replace("'", "|")

            scr = "window.onload=function () {"



            scr &= "$('#dialog').dialog('open');"

            scr &= "$('#divmsg').html('" & message & "');"



            Dim img As String



            Select Case msgtype


                Case Dialogmessagetype.Failure


                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/error.png" & Chr(34) & "/>"



                Case Dialogmessagetype.Success



                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/success.png" & Chr(34) & "/>"





                Case Dialogmessagetype.Warning


                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/warning.png" & Chr(34) & "/>"



            End Select

            scr &= "$('#divimg').html('" & img & "');"

            scr &= "$('#dialog').dialog( 'option', 'title', '" & title & "' );"






            scr &= "}"




            obj.Page.ClientScript.RegisterClientScriptBlock(obj.GetType, obj.ClientID, scr, True)







        End Sub


        Public Shared Sub ShowDialog(ByVal obj As DotNetNuke.Entities.Modules.PortalModuleBase, ByVal title As String, ByVal message As String, ByVal msgtype As Dialogmessagetype, ByVal redirecturl As String)


            Dim generalError As String() = ICUtilities.GetSettingValue("GeneralError").ToString().Split(",")

            If generalError.Any(Function(x) message.ToLower().Contains(x.ToLower())) Then
                message = ICUtilities.GetSettingValue("GeneralErrorMessage").ToString()
            End If

            message.Replace(Environment.NewLine, "<BR/>")

            Dim scr As String


            scr = "window.onload=function () {"



            scr &= "$('#dialog').dialog('open');"

            scr &= "$('#divmsg').html('" & message & "');"



            Dim img As String



            Select Case msgtype


                Case Dialogmessagetype.Failure


                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/error.png" & Chr(34) & "/>"



                Case Dialogmessagetype.Success



                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/success.png" & Chr(34) & "/>"





                Case Dialogmessagetype.Warning


                    img = "<img src=" & Chr(34) & "/Portals/_default/skins/IC/image/warning.png" & Chr(34) & "/>"



            End Select

            scr &= "$('#divimg').html('" & img & "');"

            scr &= "$('#dialog').dialog( 'option', 'title', '" & title & "' );"


            scr &= "$('#dialog').bind( 'dialogclose', function(event, ui) {"


            '  scr &= "alert('here');"


            If HttpContext.Current.Request.UserAgent.Contains("MSIE") Then

                'scr &= "location.href='" & redirecturl & "';"

                scr &= "setTimeout('window.location.replace(" & Chr(34) & redirecturl & Chr(34) & ")',10);"










            Else


                scr &= "window.location='" & redirecturl & "';"





            End If







            scr &= "});"




            scr &= "}"




            obj.Page.ClientScript.RegisterClientScriptBlock(obj.GetType, obj.ClientID, scr, True)







        End Sub
    End Class
End Namespace
