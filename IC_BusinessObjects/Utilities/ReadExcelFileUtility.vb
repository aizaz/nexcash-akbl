﻿Imports System.IO
Imports Excel
Imports System.Data
Imports System
Public Class ReadExcelFileUtility
    Public Shared Function ReadExcelFile(ByVal TemplateFormat As String, ByVal FilePath As String, ByVal SheetName As String) As DataSet
        Dim stream As FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read)
        Dim dsResult As New DataSet
        Dim IsValidSheet As Boolean = False
        'xlsx
        If TemplateFormat.Trim().ToString() = ".xlsx" Then
            Dim excelReader As IExcelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
            excelReader.IsFirstRowAsColumnNames = True
            dsResult = excelReader.AsDataSet()
            excelReader.Close()
        ElseIf TemplateFormat.Trim().ToString() = ".xls" Then
            Dim excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
            excelReader.IsFirstRowAsColumnNames = True
            dsResult = excelReader.AsDataSet()
            excelReader.Close()
        End If

        If dsResult.Tables.Count > 0 Then
            Dim dt As New DataTable
            For Each dt In dsResult.Tables
                If dt.TableName.Trim().ToLower.ToString() = SheetName.Trim().ToLower.ToString() Then
                    IsValidSheet = True
                End If
            Next
        ElseIf dsResult.Tables.Count = 0 Then
            Throw New Exception("Empty file.")
        End If
        If IsValidSheet = True Then
            Return dsResult
        Else
            Throw New Exception(String.Format("{0} is not a valid name.", SheetName))
        End If
    End Function
End Class
