﻿Imports ICBO.IC
Imports System.Web
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Public Class EncryptDecryptUtilities

    ' C# Code, CipherMode.CBC
    ' CBC version need Initialization vector IV.45

    'Public Shared keyStr As String = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    Public Shared keyStr As String = "RTgesvgJUSWMOPxflppnqidrTBBdiilqhfzYEGHfglog"

    ''Sir Umer Work for Java and VB.Net Language compatible methods
    Public Shared Function Encrypt(ByVal PlainText As String) As String
        Dim aes As New RijndaelManaged()
        aes.BlockSize = 128
        aes.KeySize = 256

        ' It is equal in java 
        '' Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");    
        aes.Mode = CipherMode.CBC
        aes.Padding = PaddingMode.PKCS7

        Dim keyArr As Byte() = Convert.FromBase64String(keyStr)
        Dim KeyArrBytes32Value As Byte() = New Byte(31) {}
        Array.Copy(keyArr, KeyArrBytes32Value, 32)

        ' Initialization vector.   
        ' It could be any value or generated using a random number generator.
        Dim ivArr As Byte() = {1, 2, 3, 4, 5, 6, _
            6, 5, 4, 3, 2, 1, _
            7, 7, 7, 7}
        Dim IVBytes16Value As Byte() = New Byte(15) {}
        Array.Copy(ivArr, IVBytes16Value, 16)

        aes.Key = KeyArrBytes32Value
        aes.IV = IVBytes16Value

        Dim encrypto As ICryptoTransform = aes.CreateEncryptor()

        Dim plainTextByte As Byte() = ASCIIEncoding.UTF8.GetBytes(PlainText)
        Dim CipherText As Byte() = encrypto.TransformFinalBlock(plainTextByte, 0, plainTextByte.Length)
        Return Convert.ToBase64String(CipherText)

    End Function

    Public Shared Function Decrypt(ByVal CipherText As String) As String
        Dim aes As New RijndaelManaged()
        aes.BlockSize = 128
        aes.KeySize = 256

        aes.Mode = CipherMode.CBC
        aes.Padding = PaddingMode.PKCS7

        Dim keyArr As Byte() = Convert.FromBase64String(keyStr)
        Dim KeyArrBytes32Value As Byte() = New Byte(31) {}
        Array.Copy(keyArr, KeyArrBytes32Value, 32)

        ' Initialization vector.   
        ' It could be any value or generated using a random number generator.
        Dim ivArr As Byte() = {1, 2, 3, 4, 5, 6, _
            6, 5, 4, 3, 2, 1, _
            7, 7, 7, 7}
        Dim IVBytes16Value As Byte() = New Byte(15) {}
        Array.Copy(ivArr, IVBytes16Value, 16)

        aes.Key = KeyArrBytes32Value
        aes.IV = IVBytes16Value

        Dim decrypto As ICryptoTransform = aes.CreateDecryptor()

        Dim encryptedBytes As Byte() = Convert.FromBase64CharArray(CipherText.ToCharArray(), 0, CipherText.Length)
        Dim decryptedData As Byte() = decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length)
        Return ASCIIEncoding.UTF8.GetString(decryptedData)
    End Function

End Class
