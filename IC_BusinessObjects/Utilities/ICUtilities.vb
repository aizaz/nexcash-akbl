﻿Imports ICBO.IC
Imports System.Web
Imports DotNetNuke.Web.UI.WebControls
Imports DotNetNuke.HttpModules
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Net.Mime.MediaTypeNames
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Entities.Users
Imports System
Imports System.Net
Imports System.Net.Mail
Imports Microsoft.Exchange.WebServices.Data
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.IO
Imports System.Xml




Public Class ICUtilities
    Shared objICSendSMS As New IntelligenesSwitch.SwitchClient
    Public Shared Sub AddAuditTrail(ByVal action As String, ByVal auditType As String, ByVal relatedId As String, ByVal UserID As String, ByVal Username As String, ByVal ActionType As String)



        Dim objAt As New ICAuditTrail
        'objAt.AddNew()
        objAt.AuditAction = action
        objAt.AuditType = auditType
        objAt.RelatedID = relatedId
        objAt.AuditDate = Now
        objAt.UserID = UserID
        objAt.Username = Username
        objAt.ActionType = ActionType
        If Not HttpContext.Current Is Nothing Then
            objAt.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")

        End If

        'If Not GetCurrentUserCookie() Is Nothing Then

        '    objAt.Username = GetCurrentUserCookie.Item("username")



        'End If








        objAt.Save()




    End Sub
    Public Shared Function ValidateRightsOnRole(ByVal PortalID As String, ByVal UserID As String, ByVal RightName As String) As Hashtable

        Dim infoUser As New UserRoleInfo
        Dim cRole As New RoleController
        Dim ArrList As New ArrayList
        Dim arrlstRoleIDs As New ArrayList
        Dim qryRoleRights As New ICRoleRightsQuery("qrr")
        Dim dtRoleRights As New DataTable
        Dim dr As DataRow
        Dim collRight As New ICRightsCollection
        Dim objRight As New ICRights
        Dim ArrResult As New ArrayList
        Dim htRights As New Hashtable

        collRight.Query.Select(collRight.Query.RightName, collRight.Query.RightDetailName)
        collRight.Query.Where(collRight.Query.RightName = RightName.ToString())
        collRight.Query.Load()

        ArrList = cRole.GetUserRoles(PortalID, UserID)
        For Each infoUser In ArrList
            arrlstRoleIDs.Add(infoUser.RoleID.ToString())
        Next
        qryRoleRights.Select(qryRoleRights.RightDetailName.Distinct())
        If arrlstRoleIDs.Count > 0 Then

            qryRoleRights.Where(qryRoleRights.RoleID.In(arrlstRoleIDs), qryRoleRights.RightName = RightName.ToString(), qryRoleRights.RightValue = True)
        Else
            'RoleRightCollection.LoadAll()
        End If
        qryRoleRights.GroupBy(qryRoleRights.RightDetailName)

        dtRoleRights = qryRoleRights.LoadDataTable()

        If CheckIsAdminOrSuperUser(PortalID, UserID) = True Then
            For Each objRight In collRight
                htRights(objRight.RightDetailName.ToString()) = True
            Next
        Else
            For Each objRight In collRight
                htRights(objRight.RightDetailName.ToString()) = False
            Next
            For Each dr In dtRoleRights.Rows
                htRights(dr("RightDetailName").ToString()) = True
            Next
        End If
        Return htRights
    End Function
    Public Shared Function CheckIsAdminOrSuperUser(ByVal PortalID As String, ByVal UserID As String) As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim iUser As New UserInfo
        Dim usrCtrl As New UserController
        Dim i As Integer
        Result = False
        iUser = usrCtrl.GetUser(PortalID, UserID)
        str = userCtrl.GetPortalRolesByUser(UserID, PortalID)
        For i = 0 To str.Length - 1
            If Trim(str(i).ToString()) = "Administrators" Then
                Result = True
            End If
        Next
        If Result = False Then
            If iUser.IsSuperUser = True Then
                Result = True
            End If
        End If
        Return Result
    End Function
    Public Shared Function UploadFiletoDB(ByVal fu As FileUpload, ByVal entitytype As String, ByVal relatedid As String, ByVal FileName As String, ByVal UserID As String, ByVal UserName As String, ByVal ActionString As String, ByVal AuditType As String) As Integer



        Dim strfilename As String
        'strfilename = fu.PostedFile.FileName
        strfilename = FileName
        strfilename = System.IO.Path.GetFileName(strfilename)
        Dim size As String
        size = fu.PostedFile.ContentLength
        Dim maxsize, maxsizemb As Integer
        maxsize = 100097152



        maxsizemb = 100

        If size < maxsize Then

            Dim imageBytes(fu.PostedFile.InputStream.Length) As Byte
            fu.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)

            Dim icFiles As New IC.ICFiles

            'frcFiles.AddNew()

            icFiles.RelatedId = relatedid
            icFiles.FileType = entitytype
            icFiles.FileName = strfilename
            icFiles.FileSize = fu.PostedFile.ContentLength
            icFiles.FileData = imageBytes
            icFiles.FileMIMEType = fu.PostedFile.ContentType
            icFiles.FileLocation = "DB"
            icFiles.CreatedBy = UserID
            icFiles.Save()
            ICUtilities.AddAuditTrail(ActionString.ToString, AuditType.ToString, icFiles.FileID.ToString, UserID.ToString, UserName.ToString, "ADD")
            Return icFiles.FileID

        Else
            Throw New Exception("You are not allowed to upload a file more than" & maxsizemb & "MB in size")
            Return -1
        End If
    End Function
    Public Shared Function UploadFiletoDBForReport(ByVal objICFiles As ICFiles, ByVal entitytype As String, ByVal relatedid As String, ByVal FileName As String, ByVal UserID As String, ByVal UserName As String, ByVal ActionString As String, ByVal AuditType As String) As Integer



        Dim strfilename As String
        'strfilename = fu.PostedFile.FileName
        strfilename = FileName
        strfilename = System.IO.Path.GetFileName(strfilename)
        Dim size As String
        size = objICFiles.FileSize
        Dim maxsize, maxsizemb As Integer
        maxsize = 100097152



        maxsizemb = 100

        If size < maxsize Then


            Dim icFiles As New IC.ICFiles

            'frcFiles.AddNew()

            icFiles.RelatedId = relatedid
            icFiles.FileType = entitytype
            icFiles.FileName = strfilename
            icFiles.FileSize = objICFiles.FileSize
            icFiles.FileData = objICFiles.FileData
            icFiles.FileMIMEType = objICFiles.FileMIMEType
            icFiles.FileLocation = "DB"
            icFiles.CreatedBy = UserID
            icFiles.Save()
            ICUtilities.AddAuditTrail(ActionString.ToString, AuditType.ToString, icFiles.FileID.ToString, UserID.ToString, UserName.ToString, "ADD")
            Return icFiles.FileID

        Else
            Throw New Exception("You are not allowed to upload a file more than" & maxsizemb & "MB in size")
            Return -1
        End If
    End Function
    Public Shared Function UploadFiletoDBViaPath(ByVal path As String, ByVal entitytype As String, ByVal relatedid As String, ByVal FileName As String, ByVal UserID As String, ByVal UserName As String, ByVal ActionString As String, ByVal AuditType As String) As Integer


        Dim fileinfo As New IO.FileInfo(path)
        Dim strfilename As String = System.IO.Path.GetFileName(path)
        Dim imageBytes(fileinfo.Length) As Byte
        Dim reader As IO.FileStream = fileinfo.OpenRead
        reader.Read(imageBytes, 0, fileinfo.Length)
        reader.Close()
        reader.Dispose()
        Dim icFiles As New IC.ICFiles

        'frcFiles.AddNew()

        icFiles.RelatedId = relatedid
        icFiles.FileType = entitytype
        icFiles.FileName = strfilename
        icFiles.FileSize = fileinfo.Length
        icFiles.FileData = imageBytes
        icFiles.FileMIMEType = "application/octet-stream"
        icFiles.FileLocation = "DB"
        icFiles.CreatedBy = UserID
        icFiles.CreatedDate = Date.Now
        icFiles.Save()
        ICUtilities.AddAuditTrail(ActionString.ToString, AuditType.ToString, icFiles.FileID.ToString, UserID, UserName, "ADD")
        Return icFiles.FileID
    End Function
    Public Shared Function showpicturefromdb(ByVal UserType As String, ByVal id As Integer, ByVal maxwidth As Integer, ByVal maxheight As Integer) As String

        Dim imgwidth, imgheight As Integer

        If UserType = "Approval Limits User" Then


            Dim objICApprovalLimits As New ICLimitForApproval

            If objICApprovalLimits.LoadByPrimaryKey(id) Then

            End If



            Dim MemStream As New System.IO.MemoryStream


            MemStream.Write(objICApprovalLimits.UserSignatureImage, 0, objICApprovalLimits.FileSize)






            Dim img As System.Drawing.Image = System.Drawing.Image.FromStream(MemStream)



            imgheight = img.Height

            imgwidth = img.Width



            Dim html As String







            If imgwidth > maxwidth Or imgheight > maxheight Then



                'Determine what dimension is off by more

                Dim deltaWidth As Integer = imgwidth - maxwidth



                Dim deltaHeight As Integer = imgheight - maxheight



                Dim scaleFactor As Double



                If deltaHeight > deltaWidth Then

                    'Scale by the height

                    scaleFactor = maxheight / imgheight



                Else

                    'Scale by the Width



                    scaleFactor = maxwidth / imgwidth



                End If



                imgwidth *= scaleFactor

                imgheight *= scaleFactor



            End If





            Dim parenturl As String







            parenturl = ICUtilities.GetCurrentUrl()











            Dim fullimageurl As String = parenturl.ToString & "ShowimageFromDB.ashx?id=" & id & "&UserType=" & UserType

















            '   html &= "<a class='lightbox' href="" & fullimageurl & "">"

            html &= "<img class='imgBorder'  border=0 src='" & fullimageurl & "&w=" & imgwidth & "&h=" & imgheight & "' width=" & imgwidth & " height=" & imgheight & ">"

            '  html &= "</a>"







            img.Dispose()



            img = Nothing









            Return html

        End If

    End Function

    Public Shared Function GetCurrentUrl() As String





        Dim currenturl As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpContext.Current.Request.ApplicationPath





        If Not currenturl.Substring(currenturl.Length - 1) = "/" Then









            currenturl &= "/"



        End If





        Return currenturl









    End Function
    'Public Shared Function UploadFiletoDB(ByVal fu As FileUpload, ByVal entitytype As String, ByVal relatedid As String, ByVal FileName As String, ByVal UserID As String) As Integer



    '    Dim strfilename As String
    '    'strfilename = fu.PostedFile.FileName
    '    strfilename = FileName
    '    strfilename = System.IO.Path.GetFileName(strfilename)
    '    Dim size As String
    '    size = fu.PostedFile.ContentLength
    '    Dim maxsize, maxsizemb As Integer
    '    maxsize = 100097152



    '    maxsizemb = 100

    '    If size < maxsize Then
    '        Dim imageBytes(fu.PostedFile.InputStream.Length) As Byte
    '        fu.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)

    '        Dim icFiles As New IC.ICFiles

    '        'frcFiles.AddNew()

    '        icFiles.RelatedId = relatedid
    '        icFiles.FileType = entitytype
    '        icFiles.FileName = strfilename
    '        icFiles.FileSize = fu.PostedFile.ContentLength
    '        icFiles.FileData = imageBytes
    '        icFiles.FileMIMEType = fu.PostedFile.ContentType
    '        icFiles.FileLocation = "DB"
    '        icFiles.CreatedBy = UserID
    '        icFiles.Save()
    '        Return icFiles.FileID
    '    Else
    '        Throw New Exception("You are not allowed to upload a file more than" & maxsizemb & "MB in size")
    '        Return -1
    '    End If
    'End Function
    'Public Shared Function UploadFiletoDB(ByVal path As String, ByVal entitytype As String, ByVal relatedid As String) As Integer


    '    Dim fileinfo As New IO.FileInfo(path)

    '    Dim strfilename As String = System.IO.Path.GetFileName(path)







    '    Dim imageBytes(fileinfo.Length) As Byte



    '    Dim reader As IO.FileStream = fileinfo.OpenRead



    '    reader.Read(imageBytes, 0, fileinfo.Length)



    '    reader.Close()

    '    reader.Dispose()



    '    Dim frcFiles As New IC.ICFiles
    '    'frcFiles.AddNew()
    '    frcFiles.RelatedId = relatedid
    '    frcFiles.FileType = entitytype



    '    frcFiles.FileName = strfilename



    '    frcFiles.FileSize = fileinfo.Length



    '    frcFiles.FileData = imageBytes



    '    frcFiles.FileMIMEType = "application/octet-stream"



    '    frcFiles.FileLocation = "DB"









    '    frcFiles.Save()



    '    fileinfo.Delete()




    '    Return frcFiles.FileID





























    'End Function



    'Public Shared Function UploadFiletoDB(ByVal path As String, ByVal entitytype As String, ByVal relatedid As String, ByVal UserID As String) As Integer


    '    Dim fileinfo As New IO.FileInfo(path)

    '    Dim strfilename As String = System.IO.Path.GetFileName(path)







    '    Dim imageBytes(fileinfo.Length) As Byte



    '    Dim reader As IO.FileStream = fileinfo.OpenRead



    '    reader.Read(imageBytes, 0, fileinfo.Length)



    '    reader.Close()

    '    reader.Dispose()



    '    Dim frcFiles As New IC.ICFiles
    '    'frcFiles.AddNew()
    '    frcFiles.RelatedId = relatedid
    '    frcFiles.FileType = entitytype
    '    frcFiles.CreatedBy = UserID

    '    frcFiles.FileName = strfilename


    '    frcFiles.FileSize = fileinfo.Length



    '    frcFiles.FileData = imageBytes



    '    frcFiles.FileMIMEType = "application/octet-stream"



    '    frcFiles.FileLocation = "DB"









    '    frcFiles.Save()

    '    Return frcFiles.FileID
    'End Function

    'Public Shared Function ShowLogoFromDB(ByVal id As Integer, ByVal maxwidth As Integer, ByVal maxheight As Integer) As String

    '    Dim frcFiles As New IC.ICFiles

    '    If frcFiles.LoadByPrimaryKey(id) Then

    '    End If



    '    Dim MemStream As New System.IO.MemoryStream


    '    MemStream.Write(frcFiles.FileData, 0, frcFiles.FileSize)






    '    Dim img As Image = System.Drawing.Image.FromStream(MemStream)


    '    Dim html As String







    '    'If imgwidth > maxwidth Or imgheight > maxheight Then



    '    '    'Determine what dimension is off by more

    '    '    Dim deltaWidth As Integer = maxwidth



    '    '    Dim deltaHeight As Integer = maxheight



    '    '    Dim scaleFactor As Double



    '    '    If deltaHeight > deltaWidth Then

    '    '        'Scale by the height

    '    '        scaleFactor = maxheight / imgheight



    '    '    Else

    '    '        'Scale by the Width



    '    '        scaleFactor = maxwidth / imgwidth



    '    '    End If



    '    'imgwidth *= scaleFactor

    '    'imgheight *= scaleFactor



    '    'End If





    '    Dim parenturl As String







    '    parenturl = FRCUtilities.GetCurrentUrl()











    '    Dim fullimageurl As String = parenturl.ToString & "ShowimageFromDB.ashx?id=" & id

















    '    '   html &= "<a class='lightbox' href="" & fullimageurl & "">"

    '    html &= "<img class='imgBorder'  border=0 src='" & fullimageurl & "&w=" & maxwidth & "&h=" & maxheight & "' width=" & maxwidth & " height=" & maxheight & ">"

    '    '  html &= "</a>"







    '    img.Dispose()



    '    img = Nothing









    '    Return html











    'End Function





    'Public Shared Function showpicturefromdb(ByVal id As Integer, ByVal maxwidth As Integer, ByVal maxheight As Integer) As String

    '    Dim imgwidth, imgheight As Integer



    '    Dim frcFiles As New ICFiles

    '    If frcFiles.LoadByPrimaryKey(id) Then

    '    End If



    '    Dim MemStream As New System.IO.MemoryStream


    '    MemStream.Write(frcFiles.FileData, 0, frcFiles.FileSize)






    '    Dim img As System.Drawing.Image = System.Drawing.Image.FromStream(MemStream)


    '    imgheight = img.Height

    '    imgwidth = img.Width



    '    Dim html As String







    '    If imgwidth > maxwidth Or imgheight > maxheight Then



    '        'Determine what dimension is off by more

    '        Dim deltaWidth As Integer = imgwidth - maxwidth



    '        Dim deltaHeight As Integer = imgheight - maxheight



    '        Dim scaleFactor As Double



    '        If deltaHeight > deltaWidth Then

    '            'Scale by the height

    '            scaleFactor = maxheight / imgheight



    '        Else

    '            'Scale by the Width



    '            scaleFactor = maxwidth / imgwidth



    '        End If



    '        imgwidth *= scaleFactor

    '        imgheight *= scaleFactor



    '    End If





    '    Dim parenturl As String







    '    parenturl = FRCUtilities.GetCurrentUrl()











    '    Dim fullimageurl As String = parenturl.ToString & "ShowimageFromDB.ashx?id=" & id

















    '    '   html &= "<a class='lightbox' href="" & fullimageurl & "">"

    '    html &= "<img class='imgBorder'  border=0 src='" & fullimageurl & "&w=" & imgwidth & "&h=" & imgheight & "' width=" & imgwidth & " height=" & imgheight & ">"

    '    '  html &= "</a>"







    '    img.Dispose()



    '    img = Nothing









    '    Return html











    'End Function

    'Public Shared Function GetCurrentUrl() As String





    '    Dim currenturl As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpContext.Current.Request.ApplicationPath





    '    If Not currenturl.Substring(currenturl.Length - 1) = "/" Then









    '        currenturl &= "/"



    '    End If





    '    Return currenturl









    'End Function

    'Function SoundEx(ByVal WordString As String, ByVal LengthOption As Integer, ByVal CensusOption As Integer) As String

    '    Dim WordStr As String
    '    Dim b, b2, b3, FirstLetter As String
    '    Dim SoundExLen = "0"
    '    Dim i As Integer


    '    ' Sanity
    '    '
    '    If (CensusOption > 0) Then
    '        LengthOption = 4
    '    End If
    '    If (LengthOption > 0) Then
    '        SoundExLen = LengthOption
    '    End If
    '    If (SoundExLen > 10) Then
    '        SoundExLen = 10
    '    End If
    '    If (SoundExLen < 4) Then
    '        SoundExLen = 4
    '    End If
    '    If (Len(WordString) < 1) Then
    '        Exit Function
    '        'Exit Function
    '    End If


    '    ' Copy to WordStr
    '    ' and UpperCase
    '    '
    '    WordStr = UCase(WordString)


    '    ' Convert all non-alpha
    '    ' chars to spaces. (thanks John)
    '    '
    '    For i = 1 To Len(WordStr)
    '        b = Mid(WordStr, i, 1)
    '        If (Not (b Like "[A-Z]")) Then
    '            WordStr = Replace(WordStr, b, " ")
    '        End If
    '    Next i


    '    ' Remove leading and
    '    ' trailing spaces
    '    '
    '    WordStr = Trim(WordStr)

    '    ' sanity
    '    '
    '    If (Len(WordStr) < 1) Then
    '        Exit Function
    '    End If


    '    ' Perform our own multi-letter
    '    ' improvements
    '    '
    '    ' double letters will be effectively
    '    ' removed in a later step.
    '    '
    '    If (CensusOption < 1) Then
    '        b = Mid(WordStr, 1, 1)
    '        b2 = Mid(WordStr, 2, 1)
    '        If (b = "P" And b2 = "S") Then
    '            WordStr = Replace(WordStr, "PS", "S", 1, 1)
    '        End If
    '        If (b = "P" And b2 = "F") Then
    '            WordStr = Replace(WordStr, "PF", "F", 1, 1)
    '        End If

    '        WordStr = Replace(WordStr, "DG", "_G")
    '        WordStr = Replace(WordStr, "GH", "_H")
    '        WordStr = Replace(WordStr, "KN", "_N")
    '        WordStr = Replace(WordStr, "GN", "_N")
    '        WordStr = Replace(WordStr, "MB", "M_")
    '        WordStr = Replace(WordStr, "PH", "F_")
    '        WordStr = Replace(WordStr, "TCH", "_CH")
    '        WordStr = Replace(WordStr, "MPS", "M_S")
    '        WordStr = Replace(WordStr, "MPT", "M_T")
    '        WordStr = Replace(WordStr, "MPZ", "M_Z")

    '    End If
    '    ' end if(Not CensusOption)
    '    '

    '    ' Sqeeze out the extra _ letters
    '    ' from above (not strictly needed
    '    ' in VB but used in C code)
    '    '
    '    WordStr = Replace(WordStr, "_", "")



    '    ' This must be done AFTER our
    '    ' multi-letter replacements
    '    ' since they could change
    '    ' the first letter
    '    '
    '    FirstLetter = Mid(WordStr, 1, 1)

    '    ' in case first letter is
    '    ' an h, a w ...
    '    ' we'll change it to something
    '    ' that doesn't match anything
    '    '
    '    If (FirstLetter = "H" Or FirstLetter = "W") Then
    '        b = Mid(WordStr, 2)
    '        WordStr = "-" + b
    '    End If


    '    ' In properly done census
    '    ' SoundEx, the H and W will
    '    ' be squezed out before
    '    ' performing the test
    '    ' for adjacent digits
    '    ' (this differs from how
    '    ' 'real' vowels are handled)
    '    '
    '    If (CensusOption = 1) Then
    '        WordStr = Replace(WordStr, "H", ".")
    '        WordStr = Replace(WordStr, "W", ".")
    '    End If


    '    ' Perform classic SoundEx
    '    ' replacements
    '    ' Here, we use ';' instead of zero '0'
    '    ' because of MS strangeness with leading
    '    ' zeros in some applications.
    '    '
    '    WordStr = Replace(WordStr, "A", ";")
    '    WordStr = Replace(WordStr, "E", ";")
    '    WordStr = Replace(WordStr, "I", ";")
    '    WordStr = Replace(WordStr, "O", ";")
    '    WordStr = Replace(WordStr, "U", ";")
    '    WordStr = Replace(WordStr, "Y", ";")
    '    WordStr = Replace(WordStr, "H", ";")
    '    WordStr = Replace(WordStr, "W", ";")

    '    WordStr = Replace(WordStr, "B", "1")
    '    WordStr = Replace(WordStr, "P", "1")
    '    WordStr = Replace(WordStr, "F", "1")
    '    WordStr = Replace(WordStr, "V", "1")

    '    WordStr = Replace(WordStr, "C", "2")
    '    WordStr = Replace(WordStr, "S", "2")
    '    WordStr = Replace(WordStr, "G", "2")
    '    WordStr = Replace(WordStr, "J", "2")
    '    WordStr = Replace(WordStr, "K", "2")
    '    WordStr = Replace(WordStr, "Q", "2")
    '    WordStr = Replace(WordStr, "X", "2")
    '    WordStr = Replace(WordStr, "Z", "2")

    '    WordStr = Replace(WordStr, "D", "3")
    '    WordStr = Replace(WordStr, "T", "3")

    '    WordStr = Replace(WordStr, "L", "4")

    '    WordStr = Replace(WordStr, "M", "5")
    '    WordStr = Replace(WordStr, "N", "5")

    '    WordStr = Replace(WordStr, "R", "6")
    '    '
    '    ' End Clasic SoundEx replacements
    '    '



    '    ' In properly done census
    '    ' SoundEx, the H and W will
    '    ' be squezed out before
    '    ' performing the test
    '    ' for adjacent digits
    '    ' (this differs from how
    '    ' 'real' vowels are handled)
    '    '
    '    If (CensusOption = 1) Then
    '        WordStr = Replace(WordStr, ".", "")
    '    End If



    '    ' squeeze out extra equal adjacent digits
    '    ' (don't include first letter)
    '    '
    '    b = ""
    '    b2 = ""
    '    ' remove from v1.0c djr: b3 = Mid(WordStr, 1, 1)
    '    b3 = ""
    '    For i = 1 To Len(WordStr) ' i=1 (not 2) in v1.0c
    '        b = Mid(WordStr, i, 1)
    '        b2 = Mid(WordStr, (i + 1), 1)
    '        If (Not (b = b2)) Then
    '            b3 = b3 + b
    '        End If
    '    Next i

    '    WordStr = b3
    '    If (Len(WordStr) < 1) Then
    '        Exit Function
    '    End If



    '    ' squeeze out spaces and zeros (;)
    '    ' Leave the first letter code
    '    ' to be replaced below.
    '    ' (In case it made a zero)
    '    '
    '    WordStr = Replace(WordStr, " ", "")
    '    b = Mid(WordStr, 1, 1)
    '    WordStr = Replace(WordStr, ";", "")
    '    If (b = ";") Then ' only if it got removed above
    '        WordStr = b + WordStr
    '    End If



    '    ' Right pad with zero characters
    '    '
    '    b = SoundExLen.PadRight(0)
    '    'String(SoundExLen, "0")
    '    WordStr = WordStr + b


    '    ' Replace first digit with
    '    ' first letter
    '    '
    '    WordStr = Mid(WordStr, 2)
    '    WordStr = FirstLetter + WordStr

    '    ' Size to taste
    '    '
    '    WordStr = Mid(WordStr, 1, SoundExLen)


    '    ' Copy WordStr to SoundEx
    '    '
    '    SoundEx = WordStr

    'End Function
    'Public Shared Function GetData(ByVal filepath As String) As DataSet


    '    Dim constr As String
    '    Dim dbpath As String

    '    '  Dim filepath As String





    '    constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1'"



    '    Dim con As New OleDb.OleDbConnection(constr)

    '    Dim cmdstr As String

    '    cmdstr = "select * from [Sheet1$]"

    '    Dim cmd As New OleDb.OleDbCommand(cmdstr, con)

    '    Dim da As New OleDb.OleDbDataAdapter(cmd)

    '    Dim ds As New DataSet

    '    da.Fill(ds, "Inbox Report")

    '    If Not ds.Tables(0).Rows.Count = 0 Then

    '        Return ds

    '    Else

    '        Return Nothing



    '    End If

    'End Function
    'Public Shared Function ImportExcelXLS(ByVal FileName As String, ByVal hasHeaders As Boolean) As DataSet
    '    Dim HDR As String = If(hasHeaders, "Yes", "No")
    '    Dim constr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & "; Extended Properties='Excel 12.0 Xml; HDR=" & HDR & "; IMEX=1'"
    '    '    Dim FilePath As String = Server.MapPath("~/UploadedFiles/")

    '    ' Dim strConn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties='Excel 12.0 Xml; HDR=" & HDR & ";IMEX=1"""
    '    Dim OleDbDataAdapter As New OleDbDataAdapter()
    '    Dim output As New DataSet()

    '    Using conn As New OleDbConnection(constr)

    '        conn.Open()

    '        Dim schemaTable As DataTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})

    '        For Each schemaRow As DataRow In schemaTable.Rows
    '            Dim sheet As String = schemaRow("TABLE_NAME").ToString()

    '            Dim cmd As New OleDbCommand("SELECT * FROM [" & sheet & "]", conn)
    '            cmd.CommandType = CommandType.Text

    '            Dim outputTable As New DataTable(sheet)
    '            output.Tables.Add(outputTable)
    '            OleDbDataAdapter.SelectCommand = cmd
    '            OleDbDataAdapter.Fill(outputTable)
    '        Next
    '    End Using
    '    Return output
    'End Function
    'Public Shared Function ImportExceltxt(ByVal FilePath As String, ByVal FileName As String, ByVal hasHeaders As Boolean) As DataTable
    '    Dim HDR As String = If(hasHeaders, "Yes", "No")
    '    Dim ConStr As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & FilePath & ";Extended Properties=""Text;HDR=" & HDR & ";FMT=TabDelimited"""
    '    Dim tbl As New DataTable
    '    tbl.Columns.Add("col1", GetType(String))


    '    Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(FilePath)
    '    Dim line As String
    '    Dim aRow As DataRow
    '    Do
    '        line = myStream.ReadLine()
    '        If line Is Nothing Then
    '            Exit Do
    '        End If


    '        'Dim sAry As String() = Split(line, ";")  ' separate the fields of the current row         
    '        aRow = tbl.NewRow 'get a DataRow that has the required structure
    '        aRow(0) = line.ToString()  ' sAry(0)

    '        tbl.Rows.Add(aRow)
    '    Loop
    '    myStream.Close()
    '    'For Each aRow <strong class="highlight">In</strong> tbl.Rows
    '    '    Console.WriteLine("{0} {1} {2}", aRow(0), aRow(1), aRow(2))
    '    'Next

    '    'Dim conn As New OleDb.OleDbConnection(ConStr)
    '    'Dim ds As New DataSet
    '    'Dim da As New OleDb.OleDbDataAdapter("Select * from " + FileName, conn)
    '    'Try
    '    '    da.Fill(ds)
    '    'Catch ex As Exception

    '    'End Try

    '    'Dim strInputCount As String = dt.Rows(0).Item(0)
    '    'SqlConnection.Open()
    '    'SqlCommand.Connection = SqlConnection
    '    'SqlCommand.CommandText = "DELETE FROM dbo.OFACInputTotalResult"
    '    'SqlDataReader = SqlCommand.ExecuteReader
    '    'SqlDataReader.Close()
    '    'Dim sqlDataReader2 As SqlDataReader
    '    'SqlCommand.CommandText = "INSERT INTO dbo.OFACInputTotalResult ([Column 0]) VALUES ('" & strInputCount & "')"
    '    'sqlDataReader2 = SqlCommand.ExecuteReader
    '    'sqlDataReader2.Close()

    '    Return tbl
    'End Function
    'Public Shared Function CreateRTGSFile(ByVal msg As String, ByVal FileName As String) As String
    '    Dim apppath As String
    '    Dim path As String

    '    apppath = GetSettingValue("PhysicalApplicationPath") & "RTGSFiles"

    '    Dim dinfo As New IO.DirectoryInfo(apppath)

    '    If dinfo.Exists = False Then
    '        dinfo.Create()
    '    End If

    '    path = apppath & "\" & FileName.ToString() & ".txt"

    '    Dim finfo As New IO.FileInfo(path)
    '    If finfo.Exists Then
    '        Dim writer As System.IO.StreamWriter
    '        writer = IO.File.AppendText(path)
    '        writer.WriteLine(msg)
    '        writer.Flush()
    '        writer.Close()
    '    Else
    '        Dim writer As System.IO.StreamWriter
    '        writer = IO.File.CreateText(path)
    '        writer.WriteLine(msg)
    '        writer.Flush()
    '        writer.Close()
    '    End If

    '    Return path
    'End Function


    'Public Shared Sub GetFileFromDB(ByVal fileid As Integer)


    '    Dim att As New FRC.FRCFiles



    '    If att.LoadByPrimaryKey(fileid) Then






    '        ' Me.Title = att.AttachmentName



    '        HttpContext.Current.Response.Clear()






    '        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.FRCFileName)


    '        HttpContext.Current.Response.ContentType = "application/ms-excel"






    '        HttpContext.Current.Response.BinaryWrite(att.FRCFileData)

    '        HttpContext.Current.Response.Flush()

    '        HttpContext.Current.Response.End()







    '    End If

    'End Sub


    'Public Shared Sub UpdateLog(ByVal msg As String)



    '    Dim apppath As String





    '    apppath = GetSettingValue("PhysicalApplicationPath") & "Logs"











    '    Dim path As String


    '    Dim dinfo As New IO.DirectoryInfo(apppath)









    '    If dinfo.Exists = False Then


    '        dinfo.Create()




    '    End If



    '    path = apppath & "\log-" & Now.Day & "-" & Now.Month & "-" & Now.Year & ".txt"












    '    Dim value As String

    '    value = "******************************" & Now.ToString & Environment.NewLine & msg & Environment.NewLine & "******************************"










    '    Dim finfo As New IO.FileInfo(path)


    '    If finfo.Exists Then





    '        Dim writer As System.IO.StreamWriter

    '        writer = IO.File.AppendText(path)

    '        writer.WriteLine(value)

    '        writer.Flush()

    '        writer.Close()














    '    Else



    '        Dim writer As System.IO.StreamWriter
    '        writer = IO.File.CreateText(path)


    '        writer.WriteLine(value)



    '        writer.Flush()

    '        writer.Close()







    '    End If






    'End Sub



    'Public Shared Function GetSettingValue(ByVal SettingName As String) As String

    '    Try

    '        Dim setting As New FRC.FRCSettings

    '        setting.es.Connection.CommandTimeout = 36000


    '        If setting.LoadByPrimaryKey(SettingName) Then

    '            Return setting.SettingValue
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

    'Public Shared Sub SetSettingValue(ByVal SettingName As String, ByVal SettingValue As String)


    '    Try

    '        Dim setting As New FRC.FRCSettings




    '        setting.es.Connection.CommandTimeout = 36000


    '        If setting.LoadByPrimaryKey(SettingName) = False Then



    '            setting.AddNew()


    '            setting.SettingName = SettingName

    '            setting.SettingValue = SettingValue

    '            setting.Save()

    '        Else


    '            setting.SettingValue = SettingValue

    '            setting.Save()





    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub
    Public Shared Sub GetFileFromDB(ByVal fileid As Integer, ByVal IsFile As Boolean)
        If IsFile = True Then
            Dim att As New ICFiles
            If att.LoadByPrimaryKey(fileid) Then
                If att.FileName.Split(".")(1).Trim.ToString().ToLower() = "trdx" Then
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.FileName)
                    HttpContext.Current.Response.ContentType = "text/xml"
                    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
                    Dim doc As New XmlDocument
                    Dim xml As String = System.Text.Encoding.UTF8.GetString(att.FileData)
                    Dim i As Integer = 0
                    i = xml.IndexOf("<")
                    If i > 0 Then
                        xml = xml.Remove(0, i)
                    End If
                    doc.LoadXml(xml)
                    doc.Save(HttpContext.Current.Response.OutputStream)
                    HttpContext.Current.Response.Flush()
                    HttpContext.Current.Response.End()
                Else
                    HttpContext.Current.Response.Clear()
                    'HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.OriginalFileName)
                    'HttpContext.Current.Response.ContentType = "application/ms-excel"
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.OriginalFileName.Replace(" ", ""))
                    HttpContext.Current.Response.ContentType = att.FileMIMEType
                    HttpContext.Current.Response.BinaryWrite(att.FileData)
                    HttpContext.Current.Response.Flush()
                    HttpContext.Current.Response.End()
                End If
            End If
        Else
            Dim objICMISReport As New ICMISReports
            If objICMISReport.LoadByPrimaryKey(fileid) Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & objICMISReport.ReportName & ".trdx")
                HttpContext.Current.Response.ContentType = "text/xml"
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
                Dim doc As New XmlDocument
                Dim xml As String = System.Text.Encoding.UTF8.GetString(objICMISReport.ReportFileData)
                Dim i As Integer = 0
                i = xml.IndexOf("<")
                If i > 0 Then
                    xml = xml.Remove(0, i)
                End If
                doc.LoadXml(xml)
                doc.Save(HttpContext.Current.Response.OutputStream)
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

    'Public Shared Sub GetFileFromDB(ByVal fileid As Integer, ByVal IsFile As Boolean)
    '    If IsFile = True Then
    '        Dim att As New ICFiles
    '        If att.LoadByPrimaryKey(fileid) Then
    '            If att.FileName.Split(".")(1).Trim.ToString().ToLower() = "trdx" Then
    '                HttpContext.Current.Response.Clear()
    '                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.FileName)
    '                HttpContext.Current.Response.ContentType = "text/xml"
    '                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
    '                Dim doc As New XmlDocument
    '                Dim xml As String = System.Text.Encoding.UTF8.GetString(att.FileData)
    '                doc.LoadXml(xml)
    '                doc.Save(HttpContext.Current.Response.OutputStream)
    '                HttpContext.Current.Response.Flush()
    '                HttpContext.Current.Response.End()
    '            Else
    '                HttpContext.Current.Response.Clear()
    '                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.OriginalFileName)
    '                HttpContext.Current.Response.ContentType = "application/ms-excel"
    '                HttpContext.Current.Response.BinaryWrite(att.FileData)
    '                HttpContext.Current.Response.Flush()
    '                HttpContext.Current.Response.End()
    '            End If
    '        End If
    '    Else
    '        Dim objICMISReport As New ICMISReports
    '        If objICMISReport.LoadByPrimaryKey(fileid) Then
    '            HttpContext.Current.Response.Clear()
    '            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & objICMISReport.ReportName & ".trdx")
    '            HttpContext.Current.Response.ContentType = "text/xml"
    '            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
    '            Dim doc As New XmlDocument
    '            Dim xml As String = System.Text.Encoding.UTF8.GetString(objICMISReport.ReportFileData)
    '            doc.LoadXml(xml)
    '            doc.Save(HttpContext.Current.Response.OutputStream)
    '            HttpContext.Current.Response.Flush()
    '            HttpContext.Current.Response.End()
    '        End If
    '    End If
    'End Sub
    Public Shared Sub GetFileofMT940(ByVal MT940FileID As Integer)

        Dim att As New ICMT940Files
        If att.LoadByPrimaryKey(MT940FileID) Then

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.OriginalFileName)
            HttpContext.Current.Response.ContentType = "application/ms-excel"
            HttpContext.Current.Response.BinaryWrite(att.MT940FileData)
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.End()

        End If

    End Sub
    Public Shared Function GetSettingValue(ByVal SettingName As String) As String

        Try

            Dim setting As New ICSettings

            setting.es.Connection.CommandTimeout = 36000


            If setting.LoadByPrimaryKey(SettingName) Then

                Return setting.SettingValue
            Else
                Return ""
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Shared Sub SetSettingValue(ByVal SettingName As String, ByVal SettingValue As String)


        Try

            Dim setting As New ICSettings




            setting.es.Connection.CommandTimeout = 36000


            If setting.LoadByPrimaryKey(SettingName) = False Then






                setting.SettingName = SettingName

                setting.SettingValue = SettingValue

                setting.Save()

            Else


                setting.SettingValue = SettingValue

                setting.Save()





            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Public Shared Function SendEmail(ByVal EmailTo As String, ByVal subject As String, ByVal body As String) As Boolean
        If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
            Return True
        End If
        Try
            Dim UserName, Password, Server, Domain As String
            UserName = GetSettingValue("UserName").ToString()
            Password = EncryptDecryptUtilities.Decrypt(GetSettingValue("Password").ToString())
            Server = GetSettingValue("Server").ToString()
            Domain = GetSettingValue("Domain").ToString()

            Dim service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)
            service.Credentials = New NetworkCredential(UserName, Password, Domain)
            service.Url = New Uri("https://" & Server & "/EWS/Exchange.asmx")
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            Dim msg As New EmailMessage(service)
            msg.ToRecipients.Add(EmailTo)
            msg.Subject = subject
            msg.Body = New MessageBody(BodyType.HTML, body)
            msg.Send()
            msg = Nothing
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Shared Function SendEmail(ByVal EmailTo As String, ByVal subject As String, ByVal body As String) As Boolean

    '    Try
    '        Dim UserName, Password, Server, Domain As String
    '        UserName = GetSettingValue("UserName").ToString()
    '        Password = GetSettingValue("Password").ToString()
    '        Server = GetSettingValue("Server").ToString()
    '        Domain = GetSettingValue("Domain").ToString()

    '        Dim service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)
    '        service.Credentials = New NetworkCredential(UserName, Password, Domain)
    '        service.Url = New Uri("https://" & Server & "/EWS/Exchange.asmx")
    '        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
    '        Dim msg As New EmailMessage(service)
    '        msg.ToRecipients.Add(EmailTo)
    '        msg.Subject = subject
    '        msg.Body = New MessageBody(BodyType.HTML, body)
    '        msg.Send()
    '        msg = Nothing
    '        Return True
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Shared Function SendEmailViaExchange(ByVal EmailTo As String, ByVal subject As String, ByVal body As String) As Boolean
        Try
            Dim UserName, Password, Server, Domain As String
            UserName = GetSettingValue("UserName").ToString()
            Password = GetSettingValue("Password").ToString()
            Server = GetSettingValue("Server").ToString()
            Domain = GetSettingValue("Domain").ToString()

            Dim service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)
            service.Credentials = New NetworkCredential(UserName, Password, Domain)
            service.Url = New Uri("https://" & Server & "/EWS/Exchange.asmx")
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            Dim msg As New EmailMessage(service)
            msg.ToRecipients.Add(EmailTo)
            msg.Subject = subject
            msg.Body = New MessageBody(BodyType.HTML, body)
            msg.Send()
            msg = Nothing
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal policyerrors As SslPolicyErrors) As Boolean
        Return True
    End Function
    Public Shared Sub SendSMS(ByVal recipientnumber As String, ByVal messagetext As String)
        Try
            If ICUtilities.GetSettingValue("ApplicationMode") = "Test" Then
                Exit Sub
            End If
            Dim Auth_UserID, Auth_IP, Auth_Password As String
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            CBUtilities.GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            objICSendSMS.SendSMS(recipientnumber, messagetext, "1", Auth_UserID, Auth_Password, Auth_IP)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Shared Function GetMaximumMessageCount() As String
        Try
            Dim objICMBZOutBox As New MBZXSMSPropertiesQuery("objICMBZOutBox")
            'objICMBZOutBox.es2.Connection.Name = "DefaultConnection"
            objICMBZOutBox.es2.Connection.Name = "SMSConnection"
            objICMBZOutBox.Select(objICMBZOutBox.Messageid.Count.As("Count"))
            objICMBZOutBox.Where(objICMBZOutBox.Messageid.Like("CMSNC%") Or objICMBZOutBox.Messageid.Like("CMS%"))

            If objICMBZOutBox.LoadDataTable(0)(0).ToString = "0" Then
                Return "1"
            Else
                Return (objICMBZOutBox.LoadDataTable(0)(0) + 1).ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function CreateCode2FA(ByVal MaxLengh As Integer) As String
        Dim gen_array(62) As String
        gen_array(0) = "0"
        gen_array(1) = "1"
        gen_array(2) = "2"
        gen_array(3) = "3"
        gen_array(4) = "4"
        gen_array(5) = "5"
        gen_array(6) = "6"
        gen_array(7) = "7"
        gen_array(8) = "8"
        gen_array(9) = "9"
        gen_array(10) = "A"
        gen_array(11) = "B"
        gen_array(12) = "C"
        gen_array(13) = "D"
        gen_array(14) = "E"
        gen_array(15) = "F"
        gen_array(16) = "G"
        gen_array(17) = "H"
        gen_array(18) = "I"
        gen_array(19) = "J"
        gen_array(20) = "K"
        gen_array(21) = "L"
        gen_array(22) = "M"
        gen_array(23) = "N"
        gen_array(24) = "O"
        gen_array(25) = "P"
        gen_array(26) = "Q"
        gen_array(27) = "R"
        gen_array(28) = "S"
        gen_array(29) = "T"
        gen_array(30) = "U"
        gen_array(31) = "V"
        gen_array(32) = "W"
        gen_array(33) = "X"
        gen_array(34) = "Y"
        gen_array(35) = "Z"
        gen_array(36) = "a"
        gen_array(37) = "b"
        gen_array(38) = "c"
        gen_array(39) = "d"
        gen_array(40) = "e"
        gen_array(41) = "f"
        gen_array(42) = "g"
        gen_array(43) = "h"
        gen_array(44) = "i"
        gen_array(45) = "j"
        gen_array(46) = "k"
        gen_array(47) = "l"
        gen_array(48) = "m"
        gen_array(49) = "n"
        gen_array(50) = "o"
        gen_array(51) = "p"
        gen_array(52) = "q"
        gen_array(53) = "r"
        gen_array(54) = "s"
        gen_array(55) = "t"
        gen_array(56) = "u"
        gen_array(57) = "v"
        gen_array(58) = "w"
        gen_array(59) = "x"
        gen_array(60) = "y"
        gen_array(61) = "z"
        Dim r As New Random()
        Dim rNumber As Integer
        Dim rOutput As String
        Do While Len(rOutput) < MaxLengh
            rNumber = r.Next(0, 61)
            rOutput = rOutput & gen_array(rNumber)
        Loop
        Return rOutput
    End Function

    Public Shared Sub UpdateLog(ByVal msg As String)
        Dim apppath As String
        apppath = GetSettingValue("PhysicalApplicationPath") & "Logs"
        Dim path As String
        Dim dinfo As New IO.DirectoryInfo(apppath)
        If dinfo.Exists = False Then
            dinfo.Create()
        End If
        path = apppath & "\AutoProcessLog-" & Now.Day & "-" & Now.Month & "-" & Now.Year & ".txt"
        Dim value As String
        value = "******************************" & Now.ToString & Environment.NewLine & msg & Environment.NewLine & "******************************"
        Dim finfo As New IO.FileInfo(path)
        If finfo.Exists Then
            Dim writer As System.IO.StreamWriter
            writer = IO.File.AppendText(path)
            writer.WriteLine(value)
            writer.Flush()
            writer.Close()
        Else
            Dim writer As System.IO.StreamWriter
            writer = IO.File.CreateText(path)
            writer.WriteLine(value)
            writer.Flush()
            writer.Close()
        End If
    End Sub
    Public Shared Sub UpdateErrorLog(ByVal msg As String, ByVal ErrorAt As String)
        Dim apppath As String
        apppath = GetSettingValue("PhysicalApplicationPath") & "Errors"
        Dim path As String
        Dim dinfo As New IO.DirectoryInfo(apppath)
        If dinfo.Exists = False Then
            dinfo.Create()
        End If
        path = apppath & "\" & ErrorAt & "-" & Now.Day & "-" & Now.Month & "-" & Now.Year & ".txt"
        Dim value As String
        value = "******************************" & Now.ToString & Environment.NewLine & msg & Environment.NewLine & "******************************"
        Dim finfo As New IO.FileInfo(path)
        If finfo.Exists Then
            Dim writer As System.IO.StreamWriter
            writer = IO.File.AppendText(path)
            writer.WriteLine(value)
            writer.Flush()
            writer.Close()
        Else
            Dim writer As System.IO.StreamWriter
            writer = IO.File.CreateText(path)
            writer.WriteLine(value)
            writer.Flush()
            writer.Close()
        End If
    End Sub
    Public Shared Function GetCustomerIDCountFromICSettings() As String
        Dim CustomerID As String = Nothing
        Dim RRNo As String = Nothing
        CustomerID = GetSettingValue("CustomerID")
        CustomerID = (CDbl(CustomerID) + 1).ToString
        SetSettingValue("CustomerID", CustomerID)
        Return CustomerID
    End Function
    Public Shared Function IsValidIDentificationType(ByVal IDType As String) As Boolean
        Dim ArrayIDType As New ArrayList
        ArrayIDType.Add("cnic")
        ArrayIDType.Add("passport")
        ArrayIDType.Add("nicop")
        ArrayIDType.Add("driving license")


        If ArrayIDType.Contains(IDType.ToString.ToLower) Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ReadExcelFile(ByVal TemplateFormat As String, ByVal FilePath As String, ByVal SheetName As String) As DataSet
        Dim ConnStr As String = ""
        Dim cmdStr As String = ""
        Dim dsExcel As New DataSet
        SheetName = SheetName.Replace("$", "")
        'xlsx
        If TemplateFormat.Trim().ToString() = ".xlsx" Then
            Try
                dsExcel = ReadExcelFileUtility.ReadExcelFile(".xlsx", FilePath, SheetName)
                Return dsExcel
                Exit Function
            Catch ex As Exception
                Throw ex
            End Try
        ElseIf TemplateFormat.Trim().ToString() = ".xls" Then
            Try
                dsExcel = ReadExcelFileUtility.ReadExcelFile(".xls", FilePath, SheetName)
                Return dsExcel
                Exit Function
            Catch ex As Exception
                Throw ex
            End Try
        ElseIf TemplateFormat.Trim().ToString() = ".csv" Then
            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Text; HDR=Yes; FMT=Delimited'"
        End If
        Dim Conn As New OleDb.OleDbConnection(ConnStr)
        Try
            If TemplateFormat.Trim().ToString() = ".csv" Then
                cmdStr = "select * from " & FilePath & ""
            Else
                cmdStr = "select * from [" & SheetName.Trim.ToString() & "$]"
            End If
            Dim cmd As New OleDb.OleDbCommand(cmdStr, Conn)
            Dim da As New OleDb.OleDbDataAdapter(cmd)
            da.Fill(dsExcel, "ExcelFile")
            cmd.Dispose()
            da.Dispose()
            Conn.Close()
            Conn.Dispose()

            Return dsExcel
        Catch ex As Exception
            Conn.Close()
            Conn.Dispose()
            Throw ex
        End Try
    End Function
    Public Shared Sub SendErrorMessage(ByVal ErrorMessage As Exception)

        Dim IsNotificationSend As Boolean = False

        If Not (ICUtilities.GetSettingValue("UserIDForFailureEvent") Is Nothing Or ICUtilities.GetSettingValue("UserIDForFailureEvent").Trim = "") Then
            If Not (ICUtilities.GetSettingValue("FailureEventRunDate") Is Nothing Or ICUtilities.GetSettingValue("FailureEventRunDate").Trim = "") Then
                If ICUtilities.GetSettingValue("FailureEventRunDate") < DateTime.Now.ToString("dd-MM-yyyy") Then
                    IsNotificationSend = True
                End If
            Else
                IsNotificationSend = True
            End If

            If IsNotificationSend = True Then
                Dim ToEmailAddresses As String() = Nothing
                Dim strToEmail As String = String.Empty
                Dim Subject As String = ""
                Dim Body As String = ""

                Subject = "Error in AlBaraka CashTrack"
                Body = "Dear All,<br /> " &
                       "<br /> " &
                       "<br /> " &
                       "An error has occurred in AlBaraka CashTrack while marking instruction as STALE. For details:<br /> " &
                       "<br /> " &
                       "<p>" & ErrorMessage.ToString() & "</p> " &
                       "<br /> " &
                       "Regards<br /> " &
                       "<br /> " &
                       "Al Baraka Pakistan<br />"

                strToEmail = ICUtilities.GetSettingValue("UserIDForFailureEvent")
                If strToEmail.Contains(";") Then
                    ToEmailAddresses = strToEmail.ToString().Split(";")
                End If

                If Not ToEmailAddresses Is Nothing Then

                    ''length 2
                    For count As Integer = 0 To ToEmailAddresses.Length - 1
                        Try
                            ICUtilities.SendEmail(ToEmailAddresses(count), Subject, Body)
                            'count = count + 1
                        Catch ex As Exception
                            ICUtilities.SetSettingValue("FailureEventRunDate", Now.ToString("dd-MM-yyyy"))
                            ICUtilities.AddAuditTrail("Fail to send email on Stale Marking failure due to " & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
                            Exit Sub
                        End Try
                    Next
                Else
                    Try
                        ICUtilities.SendEmail(strToEmail, Subject, Body)
                    Catch ex As Exception
                        ICUtilities.SetSettingValue("FailureEventRunDate", Now.ToString("dd-MM-yyyy"))
                        ICUtilities.AddAuditTrail("Fail to send email on Stale Marking failure due to " & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
                        Exit Sub
                    End Try
                End If
                ICUtilities.SetSettingValue("FailureEventRunDate", Now.ToString("dd-MM-yyyy"))
            End If
        End If
    End Sub

    Public Shared Function AuthenticateRequest(ByVal request As HttpRequest, ByVal SessionAuthToken As String) As Boolean

        If SessionAuthToken Is Nothing Then
            Return False
        End If

        If request.Cookies("AuthToken") Is Nothing Then
            Return False
        End If

        If Not SessionAuthToken.Equals(request.Cookies("AuthToken").Value) Then
            Return False
        End If

        Return True

    End Function


End Class
