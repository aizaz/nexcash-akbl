﻿Imports ICBO.IC
Imports System.Web
Imports DotNetNuke.Web.UI.WebControls
Imports DotNetNuke.HttpModules
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Net.Mime.MediaTypeNames
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Entities.Users

Public Class EmailUtilities

    Private TransactionCount As String
    Private TransactionDetail As String
    Private Remarks As String
    Private ReasonForFailures As String
    Public Shared Sub UserCreated(ByVal User As ICUser, ByVal Password As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\UserCreation.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", User.DisplayName)
            txt = txt.Replace("[Al Baraka]", ICUtilities.GetSettingValue("EmailSignature"))
            txt = txt.Replace("[UserID]", User.UserName)
            If User.AuthenticatedVia = "Active Directory" Then
                txt = txt.Replace("[UserPassword]", "Domain Password")
            Else
                txt = txt.Replace("[UserPassword]", Password)
            End If
            txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = User.Email
            subject = "User Created"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try

    End Sub

    Public Shared Sub PasswordChange(ByVal User As ICUser, ByVal UserPassword As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\PasswordChange.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", User.DisplayName)
            txt = txt.Replace("[UserPassword]", UserPassword)
            txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = User.Email
            subject = "Password Change"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub PasswordChangeByAdministrator(ByVal User As ICUser, ByVal Password As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\PasswordChangeByAdministrator.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", User.UserName)
            txt = txt.Replace("[UserPassword]", Password)
            txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = User.Email
            subject = "Password Change By Administrator"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub UserUpdated(ByVal User As ICUser)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\UserUpdated.htm")

     


            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", User.DisplayName)
            txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = User.Email
            subject = "User Updated"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub PasswordReminder(ByVal User As ICUser)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\PasswordReminder.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", User.UserName)
            txt = txt.Replace("[UserPassword]", User.Password)
            txt = txt.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = User.Email
            subject = "Password Reminder"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub UserInactivated(ByVal objICUser As ICUser)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\UserInactivated.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            Dim subject As String
            Dim emailto As String


            txt = txt.Replace("[UserName]", objICUser.DisplayName)
            txt = txt.Replace("[UserID]", objICUser.UserName)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
            emailto = objICUser.Email
            subject = "User Inactivated"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub Fileuploadedbyuser(ByVal FileName As String, ByVal UsersID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("7", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileuploadedbyuser.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt
                txtMsg = txtMsg.Replace("[UserID]", UsersID)
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File upload update"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub Fileprocessedbyuserandparsed(ByVal File As ICFiles, ByVal UsersID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("8", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileprocessedbyuserandparsed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows

                txtMsg = ""
                txtMsg = txt
                txtMsg = txtMsg.Replace("[UserID]", UsersID)
                txtMsg = txtMsg.Replace("[FileName]", File.OriginalFileName)
                txtMsg = txtMsg.Replace("[Count]", File.TransactionCount)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File Processing Status"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub Fileprocessedbyuserandfailed(ByVal FileName As String, ByVal FailureReasons As String, ByVal UserID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("9", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileprocessedbyuserandfailed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt
                txtMsg = txtMsg.Replace("[UserID]", UserID)
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[Reasons for failure]", FailureReasons)
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File Processing Status"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location 
    Public Shared Sub AllTransactionsAreVerifiedbyUserandAllAreVerifiedFromVerificationQueue(ByVal dtInstructionInfo As DataTable, ByVal UserID As String)

        Try
            Dim dt1, dt2 As New DataTable
            Dim txt As String
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Transactionsverifiedbyuserandallareverified.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As New DataTable
            distinctDT = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows
                Dim TxnCount As Integer
                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim TxnDetail As String = Nothing
                For Each dr2 As DataRow In distinctDT2
                    TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    TxnCount = TxnCount + dr2("Count")
                Next
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("10", True, drGroupCode("APNLocation"), "APNatureAndLocation")
                Dim txtMsg As String = ""
                Dim dr As DataRow
                For Each dr In dtUserForEmail.Rows
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[Count]", TxnCount)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserID]", UserID)
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString)
                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Verification Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub AllTransactionsAreVerifiedbyUserandAllAreNotVerifiedFromVerificationQueue(ByVal dtInstructionInfo As DataTable, ByVal UserID As String, ByVal ALInstructionID As ArrayList, ByVal ALFailStatus As ArrayList, ByVal ALPassStatus As ArrayList)
        Try
            Dim TxnDetail As String = Nothing
            Dim PassTxnCount As Integer
            Dim FailTxnCount As Integer
            Dim dt1, dt2 As New DataTable
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Transactionsverifiedbyuserandallwerenotverified.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")

            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                PassTxnCount = Nothing
                FailTxnCount = Nothing
                PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPassStatus, drGroupCode("APNLocation"), "APNatureAndLocation")
                FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALFailStatus, drGroupCode("APNLocation"), "APNatureAndLocation")
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("11", True, drGroupCode("APNLocation"), "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                            'If dr2("Status").ToString = "7" Then
                            '    FailTxnCount = FailTxnCount + dr("Count")
                            'ElseIf dr2("Status").ToString = "7" Or dr2("Status").ToString = "10" Then
                            '    PassTxnCount = PassTxnCount + dr("Count")
                            'End If
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                            'If dr2("Status").ToString = "7" Then
                            '    FailTxnCount = FailTxnCount + dr("Count")
                            'ElseIf dr2("Status").ToString = "7" Or dr2("Status").ToString = "10" Then
                            '    PassTxnCount = PassTxnCount + dr("Count")
                            'End If
                        Next

                    End If
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserID]", UserID)
                    txtMsg = txtMsg.Replace("[PassCount]", PassTxnCount)
                    txtMsg = txtMsg.Replace("[FailCount]", FailTxnCount)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString)

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Verification Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next


        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try



    End Sub
    ''Done APN Location
    Public Shared Sub SingleInstructioninputtedbyUser(ByVal InstructionID As String, ByVal UserID As String, ByVal APNLocation As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\SingleInstructioninputtedbyUser.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()



            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("12", True, APNLocation, "APNatureAndLocation")
            For Each dr In dtUserForEmail.Rows

                txtMsg = ""
                txtMsg = txt
                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName"))
                txtMsg = txtMsg.Replace("[UserID]", UserID)
                txtMsg = txtMsg.Replace("[txn. No.]", InstructionID)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "Transaction Input"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    ''Done
    Public Shared Sub FileuploadedThroughEmailAndParsed(ByVal File As ICFiles, ByVal UsersID As String, ByVal APNLocation As String, ByVal PassTxnCount As Integer)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("14", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileuploadedthroughemail,parsedandverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", File.OriginalFileName)
                txtMsg = txtMsg.Replace("[email user]", UsersID)
                txtMsg = txtMsg.Replace("[Count]", PassTxnCount)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))


                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through email and parsed"

                subject = "File Processing Status"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileuploadedThroughEmailParsedandVerified(ByVal FileTxnDetail As String, ByVal PassTxnCount As Integer, ByVal FileName As String, ByVal UserName As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("14", True, "")

            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Fileuploadedthroughemail,parsedandverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim FileDetail As String = Nothing
            'For Each dr2 As DataRow In dtInstructionInfo.Rows
            '    FileDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
            'Next

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[email user] ", UserName)
                txtMsg = txtMsg.Replace("[Count]", PassTxnCount)
                txtMsg = txtMsg.Replace("[FileDetail]", FileTxnDetail)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through email parsed and verified"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileUploadedThroughEmailandFailed(ByVal FileName As String, ByVal FailureReasons As String, ByVal UserID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("15", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileuploadedthroughemailandfailed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt


                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[email user] ", UserID)
                txtMsg = txtMsg.Replace("[Reasons for failure]", FailureReasons)
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File Status Update"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileUploadedThroughEmailandRejected(ByVal FileName As String, ByVal FailureReasons As String, ByVal UserID As String, ByVal UserEmailAddress As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("16", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileuploadedthroughemailandrejected.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt



                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[email address]", UserEmailAddress)
                txtMsg = txtMsg.Replace("[FailureReasons]", FailureReasons)
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File status update"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileUploadedThroughEmailParsedandButSomeAreNotVerified(ByVal InstructionInfoString As String, ByVal FileName As String, ByVal UserName As String, ByVal PassCount As String, ByVal FailCount As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Fileuploadedthroughemailparsedandbutsomearenotverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("17", True, "")


            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt


                txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[email user]", UserName)
                txtMsg = txtMsg.Replace("[PassCount]", PassCount)
                txtMsg = txtMsg.Replace("[FailCount]", FailCount)
                txtMsg = txtMsg.Replace("[FileDetail]", InstructionInfoString)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through email parsed and but some are not verified"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileUploadedthroughFTPandparsed(ByVal File As ICFiles, ByVal UsersID As String, ByVal APNLocation As String, ByVal PassTxnCount As Integer)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("19", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\FileuploadedthroughFTP,parsedandverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", File.OriginalFileName)
                txtMsg = txtMsg.Replace("[FTP user]", UsersID)
                txtMsg = txtMsg.Replace("[Count]", PassTxnCount)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))


                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()

                subject = "File Processing Status"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileUploadedThroughFTPParsedAndVerified(ByVal FileDetail As String, ByVal FileName As String, ByVal FTPUserName As String, ByVal TxnCount As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("19", True, "")

            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/FileuploadedthroughFTP,parsedandverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow


            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt


                txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[FTP user]", FTPUserName)
                txtMsg = txtMsg.Replace("[Count]", TxnCount)
                txtMsg = txtMsg.Replace("[FileDetail]", FileDetail)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through FTP, parsed and verified"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileuploadedthroughFTPandrejected(ByVal FileName As String, ByVal FailureReasons As String, ByVal UserID As String, ByVal UserFTPAddress As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("21", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\FileuploadedthroughFTPandrejected.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt



                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[FTP address]", UserFTPAddress)
                txtMsg = txtMsg.Replace("[FailureReasons]", FailureReasons)
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File status update"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileuploadedthroughFTPandfailed(ByVal ReasonsForFailure As String, ByVal FileName As String, ByVal UserName As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("20", True, APNLocation, "APNatureAndLocation")

            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/FileuploadedthroughFTPandfailed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt


                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[FTP user]", UserName)
                txtMsg = txtMsg.Replace("[Reasons for failure]", ReasonsForFailure)
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File Status Update"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub FileuploadedthroughFTPParsedAndButSomeAreNotVerified(ByVal FileDetail As String, ByVal FileName As String, ByVal FTPUserName As String, ByVal TotalTxnCount As String, ByVal PassTxnCount As String, ByVal FailTxnCount As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("22", True, "")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/FileuploadedthroughFTP,parsedandbutsomearenotverified.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt


                txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[FTP user]", FTPUserName)
                txtMsg = txtMsg.Replace("[FileDetail]", FileDetail)
                txtMsg = txtMsg.Replace("[TotalCount]", TotalTxnCount)
                txtMsg = txtMsg.Replace("[PassCount]", PassTxnCount)
                txtMsg = txtMsg.Replace("[FailCount]", FailTxnCount)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through FTP parsed and but some are not verified"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedByPrimaryApproverAndThereIsnoIssueInApproval(ByVal dtInstructionInfo As DataTable, ByVal ArrayListStatus As ArrayList, ByVal ALInstructionID As ArrayList, ByVal UsersID As String, ByVal TaggingType As String)
        Try
            Dim drGroupCodeRows As DataRow()
            Dim TxnDetail As String = Nothing
            Dim TxnCount As Integer = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Transactionsapprovedbyprimaryapproverandthereisnoissueinapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("23", True, drGroupCode("APNLocation").ToString, TaggingType)
                TxnCount = Nothing
                TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ArrayListStatus, drGroupCode("APNLocation").ToString, TaggingType)



                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next
                    End If
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[Count]", TxnCount)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedByPrimaryApproverAndThereIsanIssueInApproval(ByVal dtInstructionInfo As DataTable, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Transactionsapprovedbyprimaryapproverandthereisanissueinapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drDistinctInfo As DataRow In distinctDT.Rows
                Dim dtUserForEmail As New DataTable



                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("24", True, drDistinctInfo("APNLocation").ToString, TaggingType)



                For Each dr In dtUserForEmail.Rows

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedByPrimaryApproverAndSomeTransactionsAreNotApproved(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionIDs As ArrayList, ByVal PassStatus As ArrayList, ByVal FailStatus As ArrayList, ByVal UsersID As String, ByVal TaggingType As String)
        Try
            Dim PassTxnCount As Integer = Nothing
            Dim FailTxnCount As Integer = Nothing
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Transactionsapprovedbyprimaryapproverandsometransactionsarenotapproved.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows
                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("25", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PassTxnCount = 0
                    FailTxnCount = 0
                    PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionIDs, PassStatus, drGroupCode("APNLocation").ToString, TaggingType)
                    FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionIDs, FailStatus, drGroupCode("APNLocation").ToString, TaggingType)
                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[PassCount]", PassTxnCount)
                    txtMsg = txtMsg.Replace("[FailCount]", FailTxnCount)
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try

    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedbySecondaryApproverAndThereIsNoIssueInApprovalAndTxnsAreAvailableForDisbursement(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UsersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PassTxnCount As Integer = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsareavailablefordisbursement.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("26", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PassTxnCount = 0
                    PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPassStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[Count]", PassTxnCount)
                    txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedBySecondaryApproverAndThereIsNoissueInapprovalAndTxnsAreDisbursedInCaseofSkipPaymentQueue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALStatus As ArrayList, ByVal UsersID As String)
        Try
            Dim PassTxnCount As Integer = Nothing
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalandtxnsaredisbursed(incaseofSkipPaymentQueue).htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                PassTxnCount = 0
                PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("27", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing

                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[Count]", PassTxnCount.ToString)
                    txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try

    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedBySecondaryApproverAndThereIsNoIssueInApprovalbutAllTxnsAreNotDisbursedIncaseofSkipPaymentQueue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal ALFailStatus As ArrayList)
        Try
            Dim TxnDetail As String = Nothing
            Dim PassTxnCount As Integer = 0
            Dim FailTxnCount As Integer = 0
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsapprovedbySecondaryapproverandthereisnoissueinapprovalbutalltxnsarenotdisbursed(incaseofSkipPaymentQueue).htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                PassTxnCount = 0
                FailTxnCount = 0
                PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPassStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALFailStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("28", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[PassCount]", PassTxnCount.ToString)
                    txtMsg = txtMsg.Replace("[FailCount]", FailTxnCount.ToString)
                    txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval and Disbursement Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedbySecondaryApproverkAndThereIsanIssueInApproval(ByVal dtGroupCode As DataTable, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsapprovedbySecondaryapproverandthereisanissueinapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtGroupCode.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("29", True, drGroupCode("APNLocation"), TaggingType)
                For Each dr In dtUserForEmail.Rows
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsApprovedBySecondaryApproverAndSomeTransactionsAreNotApproved(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPAssStats As ArrayList, ByVal ALFailStat As ArrayList, ByVal UsersID As String, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PassTxnCount As String = Nothing
            Dim FailTxnCount As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsapprovedbySecondaryapproverandsometransactionsarenotapproved.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                PassTxnCount = Nothing
                FailTxnCount = Nothing
                PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPAssStats, drGroupCode("APNLocation").ToString, TaggingType)
                FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALFailStat, drGroupCode("APNLocation").ToString, TaggingType)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("30", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If


                    txtMsg = ""
                    txtMsg = txt




                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[PassCount]", PassTxnCount)
                    txtMsg = txtMsg.Replace("[Failcount]", FailTxnCount)
                    txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Approval Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    'Done APN Location
    Public Shared Sub TransactionsDisbursedSuccessfully(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPAssStatus As ArrayList, ByVal USersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim TxnCount As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Transactionsdisbursedsuccessfully.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                TxnCount = Nothing
                TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPAssStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("31", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If



                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[Count]", TxnCount)
                    txtMsg = txtMsg.Replace("[UserID]", USersID)
                    txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction Disbursement Update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub SomeTransactionsWereNotDisbursed(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPAssStatus As ArrayList, ByVal ALFailStatus As ArrayList)
        Try
            Dim TxnDetail As String = Nothing
            Dim FailTxnCount As String = Nothing
            Dim PassTxnCount As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Sometransactionswerenotdisbursed.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                PassTxnCount = Nothing
                FailTxnCount = Nothing
                PassTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPAssStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                FailTxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALFailStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("32", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[FailCount]", FailTxnCount.ToString())
                    txtMsg = txtMsg.Replace("[Count]", PassTxnCount.ToString())
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction disbursement update"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub NoTransactionsWereDisbursed(ByVal dtGroupCode As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Notransactionsweredisbursed.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtGroupCode.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("33", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "No transactions were disbursed"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub UnavailabilityoOfFundsInClientAccount(ByVal ClientAccountNo As String, ByVal dtInstructionInfo As DataTable)
        Try
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drDistinct As DataRow In distinctDT.Rows
                Dim dtUserForEmail As New DataTable
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("34", True, drDistinct("APNLocation"), "APNatureAndLocation")
                Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Unavailabilityoffundsinclientaccount.htm")
                Dim txt As String
                Dim txtMsg As String = ""
                Dim dr As DataRow
                txt = reader.ReadToEnd
                reader.Close()
                reader.Dispose()
                txtMsg = txt
                For Each dr In dtUserForEmail.Rows
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[account no.]", ClientAccountNo)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Funds Unavailable in Account"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''After Core Banking
    Public Shared Sub IRISaccountverificationfailure(ByVal UserInfo As DataTable, ByVal File As ICFiles, ByVal Instruction As ICInstruction)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/IRISaccountverificationfailure.htm")
            Dim txt As String
            Dim TxnDetail As String = ""
            TxnDetail = File.FileName.ToString() & " " & Instruction.ProductTypeCode.ToString() & " " & File.TransactionCount.ToString()
            Dim txtMsg As String = ""
            Dim dr As DataRow
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In UserInfo.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                txtMsg = txtMsg.Replace("[Transaction Detail]", TxnDetail)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "IRIS account verification failure"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub TwoFACodeForUserToLogIn(ByVal UserName As String, ByVal UserEmail As String, ByVal TwoFACode As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/2FA-Forlogin.htm")
            Dim txt As String
            Dim Code2FA As String = ""

            Dim txtMsg As String = ""

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            txtMsg = ""
            txtMsg = txt

            txtMsg = txtMsg.Replace("[UserName]", UserName)
            txtMsg = txtMsg.Replace("[Code]", TwoFACode)
            txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = UserEmail
            subject = "2FA For login"

            ICUtilities.SendEmail(emailto, subject, txtMsg)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done
    Public Shared Sub TwoFACodeForUserToApproveTransactions(ByVal UserName As String, ByVal UserEmail As String, ByVal TwoFACode As String)
        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/2FA-ForApproval.htm")
            Dim txt As String
            Dim Code2FA As String = ""

            Dim txtMsg As String = ""

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            txtMsg = ""
            txtMsg = txt

            txtMsg = txtMsg.Replace("[UserName]", UserName)
            txtMsg = txtMsg.Replace("[Code]", TwoFACode)
            txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = UserEmail
            subject = "2FA For Approval"

            ICUtilities.SendEmail(emailto, subject, txtMsg)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    ''Done APN Location
    Public Shared Sub Printing(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Printing.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)


                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("38", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Available for Printing"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub TxnAvailableForBankApproval(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnPendingBankApproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)


                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("79", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Available for Approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsAreReadyForClearing(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TransactionsreadyforClearing.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("39", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeCode") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) ready for Clearing"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsCleared(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Transactionscleared.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("40", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeCode") & " - " & dr2("Amount") & " - " & dr2("InstrumentNo") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[TxnDetails]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Tnx(s) Cleared"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub ReprintApprovalWhenTxnsAreSavedForReprint(ByVal dtInstructionInfo As DataTable, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-printApproval[whentxnsaresavedforre-print].htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("41", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[TxnDetails]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Available for Re-print Approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try

    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsAreApprovedForReprint(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-printApproved.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("42", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())


                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Available for Printing After Approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TransactionsAreRejectedOnApprovalForReprint(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-printRejectedbyApprover.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("43", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Re-print Rejected by Approver"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done pending implementation and Verify the columns in dt when this function will invoke
    Public Shared Sub TransactionsAreCancelledOnApprovalForReprint(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Txncancelledbyapproverbeforeapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("44", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODDTxn += dr3("FileName") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn cancelled by approver before approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done pending implementation and Verify the columns in dt when this function will invoke
    Public Shared Sub TxnCancelledByApproverAfterApprovalOnRePrintQueue(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Txncancelledbyapproverafterapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("45", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODDTxn += dr3("FileName") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn cancelled by approver after approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub
    ''Done APN Location
    Public Shared Sub ReissuanceApprovalWhenTxnsSentFromAllowReIssuanceQueue(ByVal dtInstructionInfo As DataTable, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-issuanceApproval[whentxnsaresavedforre-issuance].htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)


                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("46", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TransactionDetails]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Re-issued txn(s) available for Approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub ReissuanceApproved(ByVal dtInstructionInfo As DataTable, ByVal ALInstID As ArrayList, ByVal ALPAssStatus As ArrayList, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-issuanceApproved.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)


                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("47", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Re-issued Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    
    Public Shared Sub ReissuanceRejectedbyApprover(ByVal dtInstructionInfo As DataTable, ByVal ALPassStatus As ArrayList, ByVal ALInstructID As ArrayList, ByVal TaggingType As String)
        Try
            Dim TxnDetail As String = Nothing

            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnforRe-issuancecancelledbyapproverbeforeapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)


                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("49", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[Txn-Details]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Re-issued txn cancelled by Approver"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    
    ''Done pending implementation and Verify the columns in dt when this function will invoke
    'Public Shared Sub TxnforReissuancecanCelledbyApproverOnReIssuanceQueue(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
    '    Dim dtUserForEmail As New DataTable

    '    dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("50", True)

    '    Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnforRe-issuancecancelledbyapproverbeforeapproval.htm")
    '    Dim txt As String
    '    Dim POorDD As String = ""
    '    Dim TxnDetail As String = Nothing
    '    Dim PODDTxn As String = Nothing
    '    For Each dr2 As DataRow In dtInstructionInfo.Rows
    '        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
    '    Next
    '    For Each dr3 As DataRow In dtPODDTxn.Rows
    '        PODDTxn += dr3("FileName") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
    '    Next
    '    Dim txtMsg As String = ""
    '    Dim dr As DataRow
    '    txt = reader.ReadToEnd
    '    reader.Close()
    '    reader.Dispose()
    '    txtMsg = txt
    '    For Each dr In dtUserForEmail.Rows
    '        txtMsg = ""
    '        txtMsg = txt

    '        txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
    '        txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
    '        txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
    '        txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
    '        txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

    '        Dim subject As String
    '        Dim emailto As String

    '        emailto = dr("Email").ToString()
    '        subject = "Txn for Re-issuance cancelled by approver before approval"

    '        ICUtilities.SendEmail(emailto, subject, txtMsg)
    '    Next
    'End Sub



    ''Done APN Location

    Public Shared Sub TxnforReissuancecanCelledbyApproverOnReIssuanceQueue(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnforRe-issuancecancelledbyapproverafterapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("50", True, drGroupCode("APNLocation").ToString, "Printing")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn for Re-issuance cancelled by approver after approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TxnmarkedasSTALE(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnmarkedasSTALE.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("51", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn marked as STALE"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TxnRevalidation(ByVal dtInstructionInfo As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnRe-validation.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("52", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) available for Revalidation Approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Apptoved
    Public Shared Sub RevalidationApproved(ByVal dtInstructionInfo As DataTable, ByVal USersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-validationApproved.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("53", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing


                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt


                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[UserID]", USersID)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Transaction available for clearing"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub RevalidationRejectedbyApprover(ByVal dtInstructionInfo As DataTable, ByVal dtPODD As DataTable, ByVal UsersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Re-validationRejectedbyApprover.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODD.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("54", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODD = Nothing
                    For Each dr3 As DataRow In distinctDT3
                        PODD += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODD)
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Re-validation Rejected by Approver"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TxnForRevaldiationCancelledByApproverFromApproveRevaliDationQueue(ByVal dtInstructionInfo As DataTable, ByVal UsersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnforRe-valdiationcancelledbyapproverbeforeapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows


                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("55", True, drGroupCode("APNLocation").ToString, "APNature")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    'txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn for Re-valdiation cancelled by approver before approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done pending
    Public Shared Sub TxnforRevalidationRejectedByApproverAfterapproval(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnforRe-validationcancelledbyapproverafterapproval.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "GroupCode")
            For Each drGroupCode As DataRow In distinctDT.Rows
                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "GroupCode = " & drGroupCode("GroupCode") & ""
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("56", True, drGroupCode("GroupCode").ToString)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODD = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODD += dr3("FileName") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODD)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn for Re-validation cancelled by approver after approval"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done pending
    Public Shared Sub Stopprinting(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Stopprinting.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "GroupCode")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "GroupCode = " & drGroupCode("GroupCode") & ""
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("57", True, drGroupCode("GroupCode").ToString)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt
                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[For PO/DD txn]", PODDTxn)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Stop printing"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub Stoppayment(ByVal dtInstructionInfo As DataTable, ByVal dtPODDTxn As DataTable)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Stoppayment.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "GroupCode")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "GroupCode = " & drGroupCode("GroupCode") & ""
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtPODDTxn.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail("58", True, drGroupCode("GroupCode").ToString)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing
                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    For Each dr3 As DataRow In distinctDT3
                        PODDTxn += dr3("FileName") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Transaction]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Stop payment"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub StandingInstruction(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UsersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim StandingOrderCount As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Standinginstruction.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                StandingOrderCount = Nothing
                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                StandingOrderCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ALInstructionID, ALPassStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("59", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing



                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next

                    End If
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[Count]", StandingOrderCount)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Standing Instructions Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub StandingInstructionProcessing(ByVal dtInstructionInfo As DataTable, ByVal ArrayListInstructionID As ArrayList, ByVal ArrayListStatus As ArrayList)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/standinginstructionprocessing.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows


                Dim dtUserForEmail As New DataTable

                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                Dim TxnCount As String = Nothing
                TxnCount = ICInstructionController.GetTotalCountOfInstructionsWithStatusAndAarrayListIDsAndAPNLocation(ArrayListInstructionID, ArrayListStatus, drGroupCode("APNLocation").ToString, "APNatureAndLocation")

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("60", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing



                    If dr("UserType").ToString = "Bank User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("StatusName") & "<br/>"
                        Next
                    ElseIf dr("UserType").ToString = "Client User" Then
                        For Each dr2 As DataRow In distinctDT2
                            TxnDetail += dr2("FileName") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & " - " & dr2("GroupHeader") & "<br/>"
                        Next
                    End If
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[Count]", TxnCount)
                    txtMsg = txtMsg.Replace("[TransactionDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Standing Instructions Ready for Processing"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub Cancelledtransactions(ByVal dtInstructionInfo As DataTable, ByVal UsersID As String, ByVal TaggingType As String)
        Try
            Dim drGroupCodeRows As DataRow()
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Cancelledtransactions.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows


                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("61", True, drGroupCode("APNLocation").ToString, TaggingType)
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Cancelled"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub TxnsendforAmendment(ByVal dtInstructionInfo As DataTable, ByVal UsersID As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnsendforAmendment.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows


                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("62", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UsersID)
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) Available for Amendment"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    ''Done APN Location
    Public Shared Sub Txnamended(ByVal dtInstructionInfo As DataTable, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODD As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/Txnamended.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows


                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation ='" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)

                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("63", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt




                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[TxnDetail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) ammendment complete"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    ''Status Change Queue Events
    Public Shared Sub TxnMarkAsCancelledFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnAvailableForCancellationApprovalFromStatusChnageQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("64", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Cancellation"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub
    Public Shared Sub TxnMarkAsSettlementApprovalFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnAvailableForSettlementApprovalFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("66", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Settlement"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub
    Public Shared Sub TxnMarkAsReversalApprovalFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnavailableForReversalApprovalFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("68", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)

                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Reversal"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next


        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try

    End Sub

    Public Shared Sub TxnMarkAsCancelledApprovedFromStatusChangequeue(ByVal dtInstructionInfoChequq As DataTable, ByVal dtInstructionInfoPO As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnaCancellationApprovedFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfoChequq.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfoChequq.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtInstructionInfoPO.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("65", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    If distinctDT3.Count > 0 Then
                        For Each dr3 As DataRow In distinctDT3
                            PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                        Next
                    End If

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    If Not PODDTxn Is Nothing Then
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", PODDTxn)
                    Else
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", "")
                        txtMsg = txtMsg.Replace("Funds have been reversed to company account.", "")
                    End If
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Cancelled Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub
    Public Shared Sub TxnMarkAsSettledApprovedFromStatusChangequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnaSettlementApprovedFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("69", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Settled Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub

    Public Shared Sub TxnMarkAsReversalApprovedFromStatusChangequeue(ByVal dtInstructionInfoCheque As DataTable, ByVal dtInstructionInfoPO As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnReversalApprovedFromStatusChnageQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfoCheque.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfoCheque.Select(GrpCode)
                Dim distinctDT3 As DataRow() = dtInstructionInfoPO.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("45", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    If distinctDT3.Count > 0 Then
                        For Each dr3 As DataRow In distinctDT3
                            PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                        Next
                    End If

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    If Not TxnDetail Is Nothing Then
                        txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    Else
                        txtMsg = txtMsg.Replace("[Txn-Detail]", "")
                        txtMsg = txtMsg.Replace(" Funds have been reversed to company account .", "")
                    End If
                    If Not PODDTxn Is Nothing Then
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", PODDTxn)
                    Else
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", "")
                        txtMsg = txtMsg.Replace(" Funds have been reversed to Payable account.", "")
                    End If
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Reversed Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try


    End Sub
    ''24-03-2014
    Public Shared Sub PdcNotification()
        Try
            'Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            'For Each drDistinct As DataRow In distinctDT.Rows
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetAllUserTaggedForClearing("70", True)
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/PdcNotification.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                'txtMsg = txtMsg.Replace("[Cheque]", ChequeNo)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "Post Date Cheque(s) are due For Clearing"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub SendRINToBeneficiaryForCOTCVIAEmail(ByVal objICInstruction As ICInstruction)
        Try
            
           
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/SendRINToBeneficiaryForCOTCVIAEmail.htm")
            Dim txt As String
            Dim txtMsg As String = ""

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt

            txtMsg = ""
            txtMsg = txt

            txtMsg = txtMsg.Replace("[UserName]", objICInstruction.BeneficiaryName.ToString)
            txtMsg = txtMsg.Replace("[RIN]", objICInstruction.Rin.ToString)
            'txtMsg = txtMsg.Replace("[Cheque]", ChequeNo)
            txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = objICInstruction.BeneficiaryEmail.ToString
            subject = "RIN for COTC"

            ICUtilities.SendEmail(emailto, subject, txtMsg)


        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
  
    'created this method for send notification by email to use in Post Dated Cheques Module
    Public Shared Sub PdcNotificationForEmail()
        Try
            'Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            'For Each drDistinct As DataRow In distinctDT.Rows
            Dim dtUserForEmail As New DataTable
            Dim dtChqNos As New DataTable
            Dim ChqDetail As String = Nothing



            '' function to get cheque no's due for today clearing and fill in a data table

            dtChqNos = ICPdcController.PDCNotification
            If dtChqNos.Rows.Count > 0 Then
                '' after dtChqNos load
                For Each drChqNos As DataRow In dtChqNos.Rows
                    ChqDetail += drChqNos("ChequeNo") & "<br/>"
                Next
            Else
                Exit Sub
            End If


            dtUserForEmail = ICNotificationManagementController.GetAllUserTaggedForClearing("70", True)
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/PdcNotification.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[Cheque]", ChqDetail)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "Post Date Cheque(s) are due For Clearing"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
            ''






        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    ' created this method for sending notification by email to use in Accounts Management Module
    Public Shared Sub SendDailyAccountBalanceVIAEmailToTaggedUsers(ByVal AccountNo As String, ByVal Balance As String, ByVal BranchCode As String, ByVal Currency As String)
        Try


            Dim dtUserForEmail As New DataTable


            ''Function to get tagged users
            dtUserForEmail = ICNotificationManagementController.GetAllTaggedClientUserForAccountBalance("71", True, AccountNo, BranchCode, Currency)
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TaggedClientUserForAccountBalance.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[Account No]", AccountNo)
                txtMsg = txtMsg.Replace("[Balance]", CDbl(Balance).ToString("N2"))
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "Daily Account Balance"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Function GenerateEmailsForComplains(ByVal ComplainType As String, ByVal UserID As String, ByVal IsManager As Boolean, ByVal ComplainSubject As String, ByVal AppLink As String)


        Try
            Select Case IsManager
                Case True

                    Dim dtUserForEmail As New DataTable


                    dtUserForEmail = ICNotificationManagementController.GetAllUserComplain("72", True)

                    For Each dr In dtUserForEmail.Rows

                        If ComplainType.ToString() = "New" Then
                            EmailNewComplainLoggedByUser(dr("UserName").ToString, dr("Email").ToString, ComplainSubject, AppLink)
                        ElseIf ComplainType.ToString() = "Reply" Then
                            EmailComplainUpdatedByUser(dr("UserName").ToString, dr("Email").ToString, ComplainSubject, AppLink)
                        End If
                    Next

                Case False

                    Dim cUser As New ICUser
                    cUser.es.Connection.CommandTimeout = 3600
                    cUser.LoadByPrimaryKey(UserID)

                    If ComplainType.ToString() = "Open" Then

                        EmailComplainManagerRepsondsToComplainAndStillOpen(cUser.UserName, cUser.Email, ComplainSubject, AppLink)
                    ElseIf ComplainType.ToString() = "Close" Then
                        EmailComplainManagerRepsondsToComplainAndClosed(cUser.UserName, cUser.Email, ComplainSubject, AppLink)
                    End If
            End Select
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")

        End Try
    End Function

    Private Shared Sub EmailComplainManagerRepsondsToComplainAndStillOpen(ByVal UserName As String, ByVal EmailAddress As String, ByVal ComplainSubject As String, ByVal ApplicationLink As String)
        Try

            Dim readre As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/EmailComplainResponseSendToUser.htm")
            Dim txt As String
            txt = readre.ReadToEnd
            readre.Close()
            readre.Dispose()

            txt = txt.Replace("[UserName]", UserName)

            txt = txt.Replace("[AppLink]", ApplicationLink)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            subject = "Alert: Complain Reponse and Status"
            ICUtilities.SendEmail(EmailAddress, subject, txt)


        Catch ex As Exception
            ICUtilities.UpdateLog("Error:" & ex.ToString)
        End Try
    End Sub

    Private Shared Sub EmailComplainManagerRepsondsToComplainAndClosed(ByVal UserName As String, ByVal EmailAddress As String, ByVal ComplainSubject As String, ByVal ApplicationLink As String)
        Try

            Dim readre As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/EmailComplainResponseSendToUserAndClosed.htm")
            Dim txt As String
            txt = readre.ReadToEnd
            readre.Close()
            readre.Dispose()

            txt = txt.Replace("[UserName]", UserName)

            txt = txt.Replace("[AppLink]", ApplicationLink)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            subject = "Alert: Complain Reponse"
            ICUtilities.SendEmail(EmailAddress, subject, txt)


        Catch ex As Exception
            ICUtilities.UpdateLog("Error:" & ex.ToString)
        End Try
    End Sub

    Private Shared Sub EmailNewComplainLoggedByUser(ByVal UserName As String, ByVal EmailAddress As String, ByVal ComplainSubject As String, ByVal ApplicationLink As String)
        Try

            Dim readre As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/EmailUserLogedNewComplain.htm")
            Dim txt As String
            txt = readre.ReadToEnd
            readre.Close()
            readre.Dispose()

            txt = txt.Replace("[UserName]", UserName)
            txt = txt.Replace("[subject]", ComplainSubject)
            txt = txt.Replace("[AppLink]", ApplicationLink)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            subject = "Alert:" & ComplainSubject.ToString()
            ICUtilities.SendEmail(EmailAddress, subject, txt)


        Catch ex As Exception
            ICUtilities.UpdateLog("Error:" & ex.ToString)
        End Try
    End Sub

    Private Shared Sub EmailComplainUpdatedByUser(ByVal UserName As String, ByVal EmailAddress As String, ByVal ComplainSubject As String, ByVal ApplicationLink As String)
        Try

            Dim readre As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/EmailUserRepondsToExistingComplain.htm")
            Dim txt As String
            txt = readre.ReadToEnd
            readre.Close()
            readre.Dispose()

            txt = txt.Replace("[UserName]", UserName)
            txt = txt.Replace("[subject]", ComplainSubject)
            txt = txt.Replace("[AppLink]", ApplicationLink)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            subject = "Alert:" & ComplainSubject.ToString()
            ICUtilities.SendEmail(EmailAddress, subject, txt)


        Catch ex As Exception
            ICUtilities.UpdateLog("Error:" & ex.ToString)
        End Try
    End Sub
    Public Shared Sub FileuploadedbyuserThroughFTP(ByVal FileName As String, ByVal UsersID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("18", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\FileuploadedthroughFTPandparsed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt

                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[FTP user]", UsersID)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through FTP"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub FileuploadedbyuserThroughEmail(ByVal FileName As String, ByVal UsersID As String, ByVal APNLocation As String)
        Try
            Dim dtUserForEmail As New DataTable
            dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("13", True, APNLocation, "APNatureAndLocation")
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\Fileuploadedthroughemailandparsed.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            For Each dr In dtUserForEmail.Rows
                txtMsg = ""
                txtMsg = txt



                txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
                txtMsg = txtMsg.Replace("[FileName]", FileName)
                txtMsg = txtMsg.Replace("[email user]", UsersID)
                txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
                txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                Dim subject As String
                Dim emailto As String

                emailto = dr("Email").ToString()
                subject = "File uploaded through email"

                ICUtilities.SendEmail(emailto, subject, txtMsg)
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub SendEmailOfLogOut(ByVal ObjICUser As ICUser)


        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\UserLogout.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", ObjICUser.DisplayName)
            txt = txt.Replace("[DateTime]", DateTime.Now)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = ObjICUser.Email
            subject = "Log Out"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub SendEmailOfLogIn(ByVal ObjICUser As ICUser)


        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\UserLogin.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()

            txt = txt.Replace("[UserName]", ObjICUser.DisplayName)
            txt = txt.Replace("[DateTime]", DateTime.Now)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = ObjICUser.Email
            subject = "Log In"

            ICUtilities.SendEmail(emailto, subject, txt)
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub



    ''Status Change Queue New Status

    Public Shared Sub TxnMarkAsHoldApprovalFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnavailableForHoldApprovalFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("73", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)

                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Hold"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub TxnMarkAsUnholdApprovalFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnavailableForUnholdApprovalFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("75", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)

                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Un-Hold"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub TxnMarkAsRevertApprovalFromStatusChnagequeue(ByVal dtInstructionInfo As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnavailableForRevertApprovalFromStatusChangeQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfo.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable


                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfo.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("77", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)

                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Txn(s) saved for Revert"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub TxnMarkAsHoldApprovedFromStatusChangequeue(ByVal dtInstructionInfoCheque As DataTable, ByVal dtInstructionInfoPO As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnHoldApprovedFromStatusChnageQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfoCheque.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfoCheque.Select(GrpCode)
                'Dim distinctDT3 As DataRow() = dtInstructionInfoPO.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("74", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    'If distinctDT3.Count > 0 Then
                    '    For Each dr3 As DataRow In distinctDT3
                    '        PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    '    Next
                    'End If

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    If Not TxnDetail Is Nothing Then
                        txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    Else
                        txtMsg = txtMsg.Replace("[Txn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to company account .", "")
                    End If
                    If Not PODDTxn Is Nothing Then
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", PODDTxn)
                    Else
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to Payable account.", "")
                    End If
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Hold Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub TxnMarkAsUnholdApprovedFromStatusChangequeue(ByVal dtInstructionInfoCheque As DataTable, ByVal dtInstructionInfoPO As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnUnholdApprovedFromStatusChnageQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfoCheque.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfoCheque.Select(GrpCode)
                'Dim distinctDT3 As DataRow() = dtInstructionInfoPO.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("76", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    'If distinctDT3.Count > 0 Then
                    '    For Each dr3 As DataRow In distinctDT3
                    '        PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    '    Next
                    'End If

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    If Not TxnDetail Is Nothing Then
                        txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    Else
                        txtMsg = txtMsg.Replace("[Txn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to company account .", "")
                    End If
                    If Not PODDTxn Is Nothing Then
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", PODDTxn)
                    Else
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to Payable account.", "")
                    End If
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Un-Hold Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub

    Public Shared Sub TxnMarkAsRevertApprovedFromStatusChangequeue(ByVal dtInstructionInfoCheque As DataTable, ByVal dtInstructionInfoPO As DataTable, ByVal ALInstructionID As ArrayList, ByVal ALPassStatus As ArrayList, ByVal UserName As String)
        Try
            Dim TxnDetail As String = Nothing
            Dim PODDTxn As String = Nothing
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TxnRevertApprovedFromStatusChnageQueue.htm")
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim distinctDT As DataTable = dtInstructionInfoCheque.DefaultView.ToTable(True, "APNLocation")
            For Each drGroupCode As DataRow In distinctDT.Rows

                Dim dtUserForEmail As New DataTable
                Dim GrpCode As String = Nothing
                GrpCode = "APNLocation = '" & drGroupCode("APNLocation") & "'"
                Dim distinctDT2 As DataRow() = dtInstructionInfoCheque.Select(GrpCode)
                'Dim distinctDT3 As DataRow() = dtInstructionInfoPO.Select(GrpCode)
                dtUserForEmail = ICNotificationManagementController.GetUsersForSMSorEMail2("78", True, drGroupCode("APNLocation").ToString, "APNatureAndLocation")
                For Each dr In dtUserForEmail.Rows
                    TxnDetail = Nothing
                    PODDTxn = Nothing

                    For Each dr2 As DataRow In distinctDT2
                        TxnDetail += dr2("CompanyName") & " - " & dr2("ClientAccountNo") & " - " & dr2("ProductTypeName") & " - " & dr2("Count") & "<br/>"
                    Next
                    'If distinctDT3.Count > 0 Then
                    '    For Each dr3 As DataRow In distinctDT3
                    '        PODDTxn += dr3("CompanyName") & " - " & dr3("ClientAccountNo") & " - " & dr3("ProductTypeName") & " - " & dr3("Count") & "<br/>"
                    '    Next
                    'End If

                    txtMsg = ""
                    txtMsg = txt

                    txtMsg = txtMsg.Replace("[UserName]", dr("UserName").ToString())
                    txtMsg = txtMsg.Replace("[UserID]", UserName)
                    txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    If Not TxnDetail Is Nothing Then
                        txtMsg = txtMsg.Replace("[Txn-Detail]", TxnDetail)
                    Else
                        txtMsg = txtMsg.Replace("[Txn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to company account .", "")
                    End If
                    If Not PODDTxn Is Nothing Then
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", PODDTxn)
                    Else
                        txtMsg = txtMsg.Replace("[POTxn-Detail]", "")
                        'txtMsg = txtMsg.Replace(" Funds have been reversed to Payable account.", "")
                    End If
                    txtMsg = txtMsg.Replace("[App-Link]", ICUtilities.GetSettingValue("ApplicationURL"))
                    txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

                    Dim subject As String
                    Dim emailto As String

                    emailto = dr("Email").ToString()
                    subject = "Revert Txn(s) Approved"

                    ICUtilities.SendEmail(emailto, subject, txtMsg)
                Next
            Next
        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    Public Shared Sub SendEmailToRelevantApproversWithApprovalRule(ByVal dt As DataTable, ByVal Count As String)
        Dim Subject As String = ""
        Dim MsgBody As String = ""
        For Each dr As DataRow In dt.Rows
            Subject = GetApprovalRuleTextViaTextType("Subject", "", "")
            MsgBody = GetApprovalRuleTextViaTextType("Body", dr("DisplayName"), Count)

            Try
                ICUtilities.SendEmail(dr("Email"), Subject, MsgBody)
            Catch ex As Exception
                ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
                Exit Sub
            End Try
        Next
    End Sub
    Public Shared Function GetApprovalRuleTextViaTextType(ByVal TextType As String, ByVal UserName As String, ByVal Count As String) As String
        Dim ReturnMessage As String = Nothing

        If TextType = "Subject" Then
            ReturnMessage = "Txn(s) Available for Approval"
        ElseIf TextType = "Body" Then
            ReturnMessage = "<p style='font-family:Calibri; font-size:10pt'>Dear " & UserName.ToString() & ",<br /><br />Transaction(s) [ " & Count & " ] are available for approval.<br /><br />Sincerely,<br />Al Baraka</p>"
        End If
        Return ReturnMessage
    End Function
End Class
