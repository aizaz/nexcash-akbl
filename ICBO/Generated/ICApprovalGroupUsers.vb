
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/20/2015 12:16:02 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ApprovalGroupUsers' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICApprovalGroupUsers))> _
	<XmlType("ICApprovalGroupUsers")> _	
	Partial Public Class ICApprovalGroupUsers 
		Inherits esICApprovalGroupUsers
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICApprovalGroupUsers()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal userApprovalGroupID As System.Int32)
			Dim obj As New ICApprovalGroupUsers()
			obj.UserApprovalGroupID = userApprovalGroupID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal userApprovalGroupID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICApprovalGroupUsers()
			obj.UserApprovalGroupID = userApprovalGroupID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICApprovalGroupUsersCollection")> _
	Partial Public Class ICApprovalGroupUsersCollection
		Inherits esICApprovalGroupUsersCollection
		Implements IEnumerable(Of ICApprovalGroupUsers)
	
		Public Function FindByPrimaryKey(ByVal userApprovalGroupID As System.Int32) As ICApprovalGroupUsers
			Return MyBase.SingleOrDefault(Function(e) e.UserApprovalGroupID.HasValue AndAlso e.UserApprovalGroupID.Value = userApprovalGroupID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICApprovalGroupUsers))> _
		Public Class ICApprovalGroupUsersCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICApprovalGroupUsersCollection)
			
			Public Shared Widening Operator CType(packet As ICApprovalGroupUsersCollectionWCFPacket) As ICApprovalGroupUsersCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICApprovalGroupUsersCollection) As ICApprovalGroupUsersCollectionWCFPacket
				Return New ICApprovalGroupUsersCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICApprovalGroupUsersQuery 
		Inherits esICApprovalGroupUsersQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICApprovalGroupUsersQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICApprovalGroupUsersQuery) As String
			Return ICApprovalGroupUsersQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICApprovalGroupUsersQuery
			Return DirectCast(ICApprovalGroupUsersQuery.SerializeHelper.FromXml(query, GetType(ICApprovalGroupUsersQuery)), ICApprovalGroupUsersQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICApprovalGroupUsers
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal userApprovalGroupID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userApprovalGroupID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userApprovalGroupID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal userApprovalGroupID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userApprovalGroupID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userApprovalGroupID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal userApprovalGroupID As System.Int32) As Boolean
		
			Dim query As New ICApprovalGroupUsersQuery()
			query.Where(query.UserApprovalGroupID = userApprovalGroupID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal userApprovalGroupID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UserApprovalGroupID", userApprovalGroupID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.UserApprovalGroupID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserApprovalGroupID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.UserApprovalGroupID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.UserApprovalGroupID, value) Then
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.UserApprovalGroupID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.ApprovalGroupID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalGroupID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.ApprovalGroupID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.ApprovalGroupID, value) Then
					Me._UpToICApprovalGroupManagementByApprovalGroupID = Nothing
					Me.OnPropertyChanged("UpToICApprovalGroupManagementByApprovalGroupID")
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.ApprovalGroupID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalGroupUsersMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalGroupUsersMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.Creator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creator As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.Creator)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupUsersMetadata.ColumnNames.Creator, value) Then
					Me._UpToICUserByCreator = Nothing
					Me.OnPropertyChanged("UpToICUserByCreator")
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.Creator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupUsers.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalGroupUsersMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalGroupUsersMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICApprovalGroupManagementByApprovalGroupID As ICApprovalGroupManagement
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreator As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UserApprovalGroupID"
							Me.str().UserApprovalGroupID = CType(value, string)
												
						Case "ApprovalGroupID"
							Me.str().ApprovalGroupID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creator"
							Me.str().Creator = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserApprovalGroupID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserApprovalGroupID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.UserApprovalGroupID)
							End If
						
						Case "ApprovalGroupID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalGroupID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.ApprovalGroupID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.UserID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creator"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creator = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.Creator)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalGroupUsersMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICApprovalGroupUsers)
				Me.entity = entity
			End Sub				
		
	
			Public Property UserApprovalGroupID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserApprovalGroupID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserApprovalGroupID = Nothing
					Else
						entity.UserApprovalGroupID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalGroupID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalGroupID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalGroupID = Nothing
					Else
						entity.ApprovalGroupID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creator As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creator
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creator = Nothing
					Else
						entity.Creator = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICApprovalGroupUsers
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalGroupUsersMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICApprovalGroupUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalGroupUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICApprovalGroupUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICApprovalGroupUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICApprovalGroupUsersQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICApprovalGroupUsersCollection
		Inherits esEntityCollection(Of ICApprovalGroupUsers)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalGroupUsersMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICApprovalGroupUsersCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICApprovalGroupUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalGroupUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICApprovalGroupUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICApprovalGroupUsersQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICApprovalGroupUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICApprovalGroupUsersQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICApprovalGroupUsersQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICApprovalGroupUsersQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICApprovalGroupUsersMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UserApprovalGroupID" 
					Return Me.UserApprovalGroupID
				Case "ApprovalGroupID" 
					Return Me.ApprovalGroupID
				Case "UserID" 
					Return Me.UserID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creator" 
					Return Me.Creator
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UserApprovalGroupID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.UserApprovalGroupID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalGroupID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.ApprovalGroupID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creator As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.Creator, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupUsersMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICApprovalGroupUsers 
		Inherits esICApprovalGroupUsers
		
	
		#Region "UpToICApprovalGroupManagementByApprovalGroupID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_ApprovalGroupManagement
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICApprovalGroupManagementByApprovalGroupID As ICApprovalGroupManagement
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICApprovalGroupManagementByApprovalGroupID Is Nothing _
						 AndAlso Not ApprovalGroupID.Equals(Nothing)  Then
						
					Me._UpToICApprovalGroupManagementByApprovalGroupID = New ICApprovalGroupManagement()
					Me._UpToICApprovalGroupManagementByApprovalGroupID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICApprovalGroupManagementByApprovalGroupID", Me._UpToICApprovalGroupManagementByApprovalGroupID)
					Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.Where(Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.ApprovalGroupID = Me.ApprovalGroupID)
					Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.Load()
				End If

				Return Me._UpToICApprovalGroupManagementByApprovalGroupID
			End Get
			
            Set(ByVal value As ICApprovalGroupManagement)
				Me.RemovePreSave("UpToICApprovalGroupManagementByApprovalGroupID")
				

				If value Is Nothing Then
				
					Me.ApprovalGroupID = Nothing
				
					Me._UpToICApprovalGroupManagementByApprovalGroupID = Nothing
				Else
				
					Me.ApprovalGroupID = value.ApprovalGroupID
					
					Me._UpToICApprovalGroupManagementByApprovalGroupID = value
					Me.SetPreSave("UpToICApprovalGroupManagementByApprovalGroupID", Me._UpToICApprovalGroupManagementByApprovalGroupID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreator - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User2
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreator As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreator Is Nothing _
						 AndAlso Not Creator.Equals(Nothing)  Then
						
					Me._UpToICUserByCreator = New ICUser()
					Me._UpToICUserByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
					Me._UpToICUserByCreator.Query.Where(Me._UpToICUserByCreator.Query.UserID = Me.Creator)
					Me._UpToICUserByCreator.Query.Load()
				End If

				Return Me._UpToICUserByCreator
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreator")
				

				If value Is Nothing Then
				
					Me.Creator = Nothing
				
					Me._UpToICUserByCreator = Nothing
				Else
				
					Me.Creator = value.UserID
					
					Me._UpToICUserByCreator = value
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICApprovalGroupManagementByApprovalGroupID Is Nothing Then
				Me.ApprovalGroupID = Me._UpToICApprovalGroupManagementByApprovalGroupID.ApprovalGroupID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICApprovalGroupUsersMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.UserApprovalGroupID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.UserApprovalGroupID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.ApprovalGroupID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.ApprovalGroupID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.UserID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.CreatedBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.CreatedDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.CreatedDate
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.Creator, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.Creator
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupUsersMetadata.ColumnNames.CreationDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalGroupUsersMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICApprovalGroupUsersMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UserApprovalGroupID As String = "UserApprovalGroupID"
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UserApprovalGroupID As String = "UserApprovalGroupID"
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICApprovalGroupUsersMetadata)
			
				If ICApprovalGroupUsersMetadata.mapDelegates Is Nothing Then
					ICApprovalGroupUsersMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICApprovalGroupUsersMetadata._meta Is Nothing Then
					ICApprovalGroupUsersMetadata._meta = New ICApprovalGroupUsersMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UserApprovalGroupID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovalGroupID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("date", "System.DateTime"))
				meta.AddTypeMap("Creator", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_ApprovalGroupUsers"
				meta.Destination = "IC_ApprovalGroupUsers"
				
				meta.spInsert = "proc_IC_ApprovalGroupUsersInsert"
				meta.spUpdate = "proc_IC_ApprovalGroupUsersUpdate"
				meta.spDelete = "proc_IC_ApprovalGroupUsersDelete"
				meta.spLoadAll = "proc_IC_ApprovalGroupUsersLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ApprovalGroupUsersLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICApprovalGroupUsersMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
