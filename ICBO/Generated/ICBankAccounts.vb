
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_BankAccounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBankAccounts))> _
	<XmlType("ICBankAccounts")> _	
	Partial Public Class ICBankAccounts 
		Inherits esICBankAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBankAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal principalBankAccountID As System.Int32)
			Dim obj As New ICBankAccounts()
			obj.PrincipalBankAccountID = principalBankAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal principalBankAccountID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBankAccounts()
			obj.PrincipalBankAccountID = principalBankAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBankAccountsCollection")> _
	Partial Public Class ICBankAccountsCollection
		Inherits esICBankAccountsCollection
		Implements IEnumerable(Of ICBankAccounts)
	
		Public Function FindByPrimaryKey(ByVal principalBankAccountID As System.Int32) As ICBankAccounts
			Return MyBase.SingleOrDefault(Function(e) e.PrincipalBankAccountID.HasValue AndAlso e.PrincipalBankAccountID.Value = principalBankAccountID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBankAccounts))> _
		Public Class ICBankAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBankAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICBankAccountsCollectionWCFPacket) As ICBankAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBankAccountsCollection) As ICBankAccountsCollectionWCFPacket
				Return New ICBankAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBankAccountsQuery 
		Inherits esICBankAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBankAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBankAccountsQuery) As String
			Return ICBankAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBankAccountsQuery
			Return DirectCast(ICBankAccountsQuery.SerializeHelper.FromXml(query, GetType(ICBankAccountsQuery)), ICBankAccountsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBankAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal principalBankAccountID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(principalBankAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(principalBankAccountID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal principalBankAccountID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(principalBankAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(principalBankAccountID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal principalBankAccountID As System.Int32) As Boolean
		
			Dim query As New ICBankAccountsQuery()
			query.Where(query.PrincipalBankAccountID = principalBankAccountID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal principalBankAccountID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("PrincipalBankAccountID", principalBankAccountID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_BankAccounts.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.AccountName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountName As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.AccountName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.AccountName, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.AccountName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.AccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.AccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.AccountTitle, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.AccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.PrincipalBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrincipalBankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.PrincipalBankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.PrincipalBankCode, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.PrincipalBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankAccountsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankAccountsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankAccountsMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankAccountsMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.IsStaleAccount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsStaleAccount As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsStaleAccount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankAccountsMetadata.ColumnNames.IsStaleAccount, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsStaleAccount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.AccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountType As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.AccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.AccountType, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.AccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.CBAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CBAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.CBAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.CBAccountType, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CBAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.ClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.ClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.ClientNo, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.SeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SeqNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.SeqNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.SeqNo, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.SeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.ProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICBankAccountsMetadata.ColumnNames.ProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankAccountsMetadata.ColumnNames.ProfitCentre, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.PrincipalBankAccountID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrincipalBankAccountID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.PrincipalBankAccountID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.PrincipalBankAccountID, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.PrincipalBankAccountID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankAccountsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankAccountsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BankAccounts.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankAccountsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankAccountsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "AccountName"
							Me.str().AccountName = CType(value, string)
												
						Case "AccountTitle"
							Me.str().AccountTitle = CType(value, string)
												
						Case "PrincipalBankCode"
							Me.str().PrincipalBankCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsStaleAccount"
							Me.str().IsStaleAccount = CType(value, string)
												
						Case "AccountType"
							Me.str().AccountType = CType(value, string)
												
						Case "CBAccountType"
							Me.str().CBAccountType = CType(value, string)
												
						Case "ClientNo"
							Me.str().ClientNo = CType(value, string)
												
						Case "SeqNo"
							Me.str().SeqNo = CType(value, string)
												
						Case "ProfitCentre"
							Me.str().ProfitCentre = CType(value, string)
												
						Case "PrincipalBankAccountID"
							Me.str().PrincipalBankAccountID = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "PrincipalBankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrincipalBankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.PrincipalBankCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsApproved)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsStaleAccount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsStaleAccount = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.IsStaleAccount)
							End If
						
						Case "SeqNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SeqNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.SeqNo)
							End If
						
						Case "PrincipalBankAccountID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrincipalBankAccountID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.PrincipalBankAccountID)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankAccountsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBankAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountName As System.String 
				Get
					Dim data_ As System.String = entity.AccountName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountName = Nothing
					Else
						entity.AccountName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.AccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountTitle = Nothing
					Else
						entity.AccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrincipalBankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrincipalBankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrincipalBankCode = Nothing
					Else
						entity.PrincipalBankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsStaleAccount As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsStaleAccount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsStaleAccount = Nothing
					Else
						entity.IsStaleAccount = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountType As System.String 
				Get
					Dim data_ As System.String = entity.AccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountType = Nothing
					Else
						entity.AccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CBAccountType As System.String 
				Get
					Dim data_ As System.String = entity.CBAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CBAccountType = Nothing
					Else
						entity.CBAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientNo As System.String 
				Get
					Dim data_ As System.String = entity.ClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientNo = Nothing
					Else
						entity.ClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SeqNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SeqNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SeqNo = Nothing
					Else
						entity.SeqNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.ProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProfitCentre = Nothing
					Else
						entity.ProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrincipalBankAccountID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrincipalBankAccountID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrincipalBankAccountID = Nothing
					Else
						entity.PrincipalBankAccountID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBankAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBankAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBankAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBankAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBankAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBankAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBankAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBankAccountsCollection
		Inherits esEntityCollection(Of ICBankAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBankAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBankAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBankAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBankAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBankAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBankAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBankAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBankAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBankAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBankAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBankAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "AccountName" 
					Return Me.AccountName
				Case "AccountTitle" 
					Return Me.AccountTitle
				Case "PrincipalBankCode" 
					Return Me.PrincipalBankCode
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApproved" 
					Return Me.IsApproved
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsStaleAccount" 
					Return Me.IsStaleAccount
				Case "AccountType" 
					Return Me.AccountType
				Case "CBAccountType" 
					Return Me.CBAccountType
				Case "ClientNo" 
					Return Me.ClientNo
				Case "SeqNo" 
					Return Me.SeqNo
				Case "ProfitCentre" 
					Return Me.ProfitCentre
				Case "PrincipalBankAccountID" 
					Return Me.PrincipalBankAccountID
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.AccountName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.AccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PrincipalBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.PrincipalBankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsStaleAccount As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.IsStaleAccount, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property AccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.AccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CBAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.CBAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.ClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.SeqNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.ProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PrincipalBankAccountID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.PrincipalBankAccountID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankAccountsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBankAccounts 
		Inherits esICBankAccounts
		
	
		#Region "ICGlobalChargesCollectionByPrincipalBankAccountID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICGlobalChargesCollectionByPrincipalBankAccountID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICBankAccounts.ICGlobalChargesCollectionByPrincipalBankAccountID_Delegate)
				map.PropertyName = "ICGlobalChargesCollectionByPrincipalBankAccountID"
				map.MyColumnName = "PrincipalBankAccountID"
				map.ParentColumnName = "PrincipalBankAccountID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICGlobalChargesCollectionByPrincipalBankAccountID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICBankAccountsQuery(data.NextAlias())
			
			Dim mee As ICGlobalChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICGlobalChargesQuery), New ICGlobalChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PrincipalBankAccountID = mee.PrincipalBankAccountID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_GlobalCharges_IC_BankAccounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICGlobalChargesCollectionByPrincipalBankAccountID As ICGlobalChargesCollection 
		
			Get
				If Me._ICGlobalChargesCollectionByPrincipalBankAccountID Is Nothing Then
					Me._ICGlobalChargesCollectionByPrincipalBankAccountID = New ICGlobalChargesCollection()
					Me._ICGlobalChargesCollectionByPrincipalBankAccountID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICGlobalChargesCollectionByPrincipalBankAccountID", Me._ICGlobalChargesCollectionByPrincipalBankAccountID)
				
					If Not Me.PrincipalBankAccountID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICGlobalChargesCollectionByPrincipalBankAccountID.Query.Where(Me._ICGlobalChargesCollectionByPrincipalBankAccountID.Query.PrincipalBankAccountID = Me.PrincipalBankAccountID)
							Me._ICGlobalChargesCollectionByPrincipalBankAccountID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICGlobalChargesCollectionByPrincipalBankAccountID.fks.Add(ICGlobalChargesMetadata.ColumnNames.PrincipalBankAccountID, Me.PrincipalBankAccountID)
					End If
				End If

				Return Me._ICGlobalChargesCollectionByPrincipalBankAccountID
			End Get
			
			Set(ByVal value As ICGlobalChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICGlobalChargesCollectionByPrincipalBankAccountID Is Nothing Then

					Me.RemovePostSave("ICGlobalChargesCollectionByPrincipalBankAccountID")
					Me._ICGlobalChargesCollectionByPrincipalBankAccountID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICGlobalChargesCollectionByPrincipalBankAccountID As ICGlobalChargesCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICGlobalChargesCollectionByPrincipalBankAccountID"
					coll = Me.ICGlobalChargesCollectionByPrincipalBankAccountID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICGlobalChargesCollectionByPrincipalBankAccountID", GetType(ICGlobalChargesCollection), New ICGlobalCharges()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICGlobalChargesCollectionByPrincipalBankAccountID Is Nothing Then
				Apply(Me._ICGlobalChargesCollectionByPrincipalBankAccountID, "PrincipalBankAccountID", Me.PrincipalBankAccountID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICBankAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.AccountName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.AccountName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.AccountTitle, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.AccountTitle
			c.CharacterMaxLength = 300
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.PrincipalBankCode, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.PrincipalBankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.IsApproved, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.CreatedDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.CreatedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.ApprovedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.ApprovedOn, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.IsStaleAccount, 12, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.IsStaleAccount
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.AccountType, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.AccountType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.CBAccountType, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.CBAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.ClientNo, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.ClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.SeqNo, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.SeqNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.ProfitCentre, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.ProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.PrincipalBankAccountID, 18, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.PrincipalBankAccountID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.Creater, 19, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankAccountsMetadata.ColumnNames.CreationDate, 20, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankAccountsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBankAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountName As String = "AccountName"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const PrincipalBankCode As String = "PrincipalBankCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsStaleAccount As String = "IsStaleAccount"
			 Public Const AccountType As String = "AccountType"
			 Public Const CBAccountType As String = "CBAccountType"
			 Public Const ClientNo As String = "ClientNo"
			 Public Const SeqNo As String = "SeqNo"
			 Public Const ProfitCentre As String = "ProfitCentre"
			 Public Const PrincipalBankAccountID As String = "PrincipalBankAccountID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountName As String = "AccountName"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const PrincipalBankCode As String = "PrincipalBankCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsStaleAccount As String = "IsStaleAccount"
			 Public Const AccountType As String = "AccountType"
			 Public Const CBAccountType As String = "CBAccountType"
			 Public Const ClientNo As String = "ClientNo"
			 Public Const SeqNo As String = "SeqNo"
			 Public Const ProfitCentre As String = "ProfitCentre"
			 Public Const PrincipalBankAccountID As String = "PrincipalBankAccountID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBankAccountsMetadata)
			
				If ICBankAccountsMetadata.mapDelegates Is Nothing Then
					ICBankAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBankAccountsMetadata._meta Is Nothing Then
					ICBankAccountsMetadata._meta = New ICBankAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PrincipalBankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsStaleAccount", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("AccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CBAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SeqNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PrincipalBankAccountID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_BankAccounts"
				meta.Destination = "IC_BankAccounts"
				
				meta.spInsert = "proc_IC_BankAccountsInsert"
				meta.spUpdate = "proc_IC_BankAccountsUpdate"
				meta.spDelete = "proc_IC_BankAccountsDelete"
				meta.spLoadAll = "proc_IC_BankAccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BankAccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBankAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
