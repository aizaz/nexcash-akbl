
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ScheduleTransactions' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICScheduleTransactions))> _
	<XmlType("ICScheduleTransactions")> _	
	Partial Public Class ICScheduleTransactions 
		Inherits esICScheduleTransactions
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICScheduleTransactions()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal scheduleID As System.Int32)
			Dim obj As New ICScheduleTransactions()
			obj.ScheduleID = scheduleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal scheduleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICScheduleTransactions()
			obj.ScheduleID = scheduleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICScheduleTransactionsCollection")> _
	Partial Public Class ICScheduleTransactionsCollection
		Inherits esICScheduleTransactionsCollection
		Implements IEnumerable(Of ICScheduleTransactions)
	
		Public Function FindByPrimaryKey(ByVal scheduleID As System.Int32) As ICScheduleTransactions
			Return MyBase.SingleOrDefault(Function(e) e.ScheduleID.HasValue AndAlso e.ScheduleID.Value = scheduleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICScheduleTransactions))> _
		Public Class ICScheduleTransactionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICScheduleTransactionsCollection)
			
			Public Shared Widening Operator CType(packet As ICScheduleTransactionsCollectionWCFPacket) As ICScheduleTransactionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICScheduleTransactionsCollection) As ICScheduleTransactionsCollectionWCFPacket
				Return New ICScheduleTransactionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICScheduleTransactionsQuery 
		Inherits esICScheduleTransactionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICScheduleTransactionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICScheduleTransactionsQuery) As String
			Return ICScheduleTransactionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICScheduleTransactionsQuery
			Return DirectCast(ICScheduleTransactionsQuery.SerializeHelper.FromXml(query, GetType(ICScheduleTransactionsQuery)), ICScheduleTransactionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICScheduleTransactions
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal scheduleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(scheduleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(scheduleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal scheduleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(scheduleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(scheduleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal scheduleID As System.Int32) As Boolean
		
			Dim query As New ICScheduleTransactionsQuery()
			query.Where(query.ScheduleID = scheduleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal scheduleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ScheduleID", scheduleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ScheduleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.ScheduleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.ScheduleID, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ClientAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountNo, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ClientAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ClientAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountBranchCode, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ClientAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ClientAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountCurrency, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ClientAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.BeneficiaryAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountNo, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.BeneficiaryAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountBranchCode, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.BeneficiaryAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountCurrency, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.BeneficaryName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficaryName As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficaryName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.BeneficaryName, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.BeneficaryName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.TransactionCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.TransactionCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.TransactionCount, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.TransactionCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICScheduleTransactionsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ScheduleTransactionFromDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleTransactionFromDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionFromDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionFromDate, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionFromDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.ScheduleTransactionToDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleTransactionToDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionToDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionToDate, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionToDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.AmountPerInstruction
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountPerInstruction As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICScheduleTransactionsMetadata.ColumnNames.AmountPerInstruction)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICScheduleTransactionsMetadata.ColumnNames.AmountPerInstruction, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.AmountPerInstruction)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.TransactionTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.TransactionTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICScheduleTransactionsMetadata.ColumnNames.TransactionTime, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.TransactionTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.TrnsactionFrequency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TrnsactionFrequency As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.TrnsactionFrequency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.TrnsactionFrequency, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.TrnsactionFrequency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ScheduleTransactions.DayType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DayType As System.String
			Get
				Return MyBase.GetSystemString(ICScheduleTransactionsMetadata.ColumnNames.DayType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICScheduleTransactionsMetadata.ColumnNames.DayType, value) Then
					OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.DayType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ScheduleID"
							Me.str().ScheduleID = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ClientAccountNo"
							Me.str().ClientAccountNo = CType(value, string)
												
						Case "ClientAccountBranchCode"
							Me.str().ClientAccountBranchCode = CType(value, string)
												
						Case "ClientAccountCurrency"
							Me.str().ClientAccountCurrency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "BeneficiaryAccountNo"
							Me.str().BeneficiaryAccountNo = CType(value, string)
												
						Case "BeneficiaryAccountBranchCode"
							Me.str().BeneficiaryAccountBranchCode = CType(value, string)
												
						Case "BeneficiaryAccountCurrency"
							Me.str().BeneficiaryAccountCurrency = CType(value, string)
												
						Case "BeneficaryName"
							Me.str().BeneficaryName = CType(value, string)
												
						Case "TransactionCount"
							Me.str().TransactionCount = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "ScheduleTransactionFromDate"
							Me.str().ScheduleTransactionFromDate = CType(value, string)
												
						Case "ScheduleTransactionToDate"
							Me.str().ScheduleTransactionToDate = CType(value, string)
												
						Case "AmountPerInstruction"
							Me.str().AmountPerInstruction = CType(value, string)
												
						Case "TransactionTime"
							Me.str().TransactionTime = CType(value, string)
												
						Case "TrnsactionFrequency"
							Me.str().TrnsactionFrequency = CType(value, string)
												
						Case "DayType"
							Me.str().DayType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ScheduleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ScheduleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "TransactionCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TransactionCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.TransactionCount)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "ScheduleTransactionFromDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ScheduleTransactionFromDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionFromDate)
							End If
						
						Case "ScheduleTransactionToDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ScheduleTransactionToDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionToDate)
							End If
						
						Case "AmountPerInstruction"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.AmountPerInstruction = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.AmountPerInstruction)
							End If
						
						Case "TransactionTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.TransactionTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICScheduleTransactionsMetadata.PropertyNames.TransactionTime)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICScheduleTransactions)
				Me.entity = entity
			End Sub				
		
	
			Public Property ScheduleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ScheduleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleID = Nothing
					Else
						entity.ScheduleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountNo = Nothing
					Else
						entity.ClientAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountBranchCode = Nothing
					Else
						entity.ClientAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountCurrency = Nothing
					Else
						entity.ClientAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountNo = Nothing
					Else
						entity.BeneficiaryAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountBranchCode = Nothing
					Else
						entity.BeneficiaryAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountCurrency = Nothing
					Else
						entity.BeneficiaryAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficaryName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficaryName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficaryName = Nothing
					Else
						entity.BeneficaryName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TransactionCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionCount = Nothing
					Else
						entity.TransactionCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ScheduleTransactionFromDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ScheduleTransactionFromDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleTransactionFromDate = Nothing
					Else
						entity.ScheduleTransactionFromDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ScheduleTransactionToDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ScheduleTransactionToDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleTransactionToDate = Nothing
					Else
						entity.ScheduleTransactionToDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountPerInstruction As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.AmountPerInstruction
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountPerInstruction = Nothing
					Else
						entity.AmountPerInstruction = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.TransactionTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionTime = Nothing
					Else
						entity.TransactionTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property TrnsactionFrequency As System.String 
				Get
					Dim data_ As System.String = entity.TrnsactionFrequency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TrnsactionFrequency = Nothing
					Else
						entity.TrnsactionFrequency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DayType As System.String 
				Get
					Dim data_ As System.String = entity.DayType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DayType = Nothing
					Else
						entity.DayType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICScheduleTransactions
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICScheduleTransactionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICScheduleTransactionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICScheduleTransactionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICScheduleTransactionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICScheduleTransactionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICScheduleTransactionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICScheduleTransactionsCollection
		Inherits esEntityCollection(Of ICScheduleTransactions)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICScheduleTransactionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICScheduleTransactionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICScheduleTransactionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICScheduleTransactionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICScheduleTransactionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICScheduleTransactionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICScheduleTransactionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICScheduleTransactionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICScheduleTransactionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICScheduleTransactionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICScheduleTransactionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ScheduleID" 
					Return Me.ScheduleID
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ClientAccountNo" 
					Return Me.ClientAccountNo
				Case "ClientAccountBranchCode" 
					Return Me.ClientAccountBranchCode
				Case "ClientAccountCurrency" 
					Return Me.ClientAccountCurrency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "BeneficiaryAccountNo" 
					Return Me.BeneficiaryAccountNo
				Case "BeneficiaryAccountBranchCode" 
					Return Me.BeneficiaryAccountBranchCode
				Case "BeneficiaryAccountCurrency" 
					Return Me.BeneficiaryAccountCurrency
				Case "BeneficaryName" 
					Return Me.BeneficaryName
				Case "TransactionCount" 
					Return Me.TransactionCount
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "ScheduleTransactionFromDate" 
					Return Me.ScheduleTransactionFromDate
				Case "ScheduleTransactionToDate" 
					Return Me.ScheduleTransactionToDate
				Case "AmountPerInstruction" 
					Return Me.AmountPerInstruction
				Case "TransactionTime" 
					Return Me.TransactionTime
				Case "TrnsactionFrequency" 
					Return Me.TrnsactionFrequency
				Case "DayType" 
					Return Me.DayType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ScheduleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ScheduleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ClientAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ClientAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ClientAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficaryName As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.BeneficaryName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.TransactionCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ScheduleTransactionFromDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionFromDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ScheduleTransactionToDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionToDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AmountPerInstruction As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.AmountPerInstruction, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.TransactionTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property TrnsactionFrequency As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.TrnsactionFrequency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DayType As esQueryItem
			Get
				Return New esQueryItem(Me, ICScheduleTransactionsMetadata.ColumnNames.DayType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICScheduleTransactions 
		Inherits esICScheduleTransactions
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICScheduleTransactionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ScheduleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ScheduleID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.CompanyCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountNo, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ClientAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountBranchCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ClientAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ClientAccountCurrency, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ClientAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.PaymentNatureCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ProductTypeCode, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountNo, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountBranchCode, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.BeneficiaryAccountCurrency, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.BeneficiaryAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.BeneficaryName, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.BeneficaryName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.TransactionCount, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.TransactionCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.CreatedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.CreatedDate, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionFromDate, 14, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionFromDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.ScheduleTransactionToDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.ScheduleTransactionToDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.AmountPerInstruction, 16, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.AmountPerInstruction
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.TransactionTime, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.TransactionTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.TrnsactionFrequency, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.TrnsactionFrequency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICScheduleTransactionsMetadata.ColumnNames.DayType, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICScheduleTransactionsMetadata.PropertyNames.DayType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICScheduleTransactionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ScheduleID As String = "ScheduleID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountBranchCode As String = "BeneficiaryAccountBranchCode"
			 Public Const BeneficiaryAccountCurrency As String = "BeneficiaryAccountCurrency"
			 Public Const BeneficaryName As String = "BeneficaryName"
			 Public Const TransactionCount As String = "TransactionCount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ScheduleTransactionFromDate As String = "ScheduleTransactionFromDate"
			 Public Const ScheduleTransactionToDate As String = "ScheduleTransactionToDate"
			 Public Const AmountPerInstruction As String = "AmountPerInstruction"
			 Public Const TransactionTime As String = "TransactionTime"
			 Public Const TrnsactionFrequency As String = "TrnsactionFrequency"
			 Public Const DayType As String = "DayType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ScheduleID As String = "ScheduleID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountBranchCode As String = "BeneficiaryAccountBranchCode"
			 Public Const BeneficiaryAccountCurrency As String = "BeneficiaryAccountCurrency"
			 Public Const BeneficaryName As String = "BeneficaryName"
			 Public Const TransactionCount As String = "TransactionCount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ScheduleTransactionFromDate As String = "ScheduleTransactionFromDate"
			 Public Const ScheduleTransactionToDate As String = "ScheduleTransactionToDate"
			 Public Const AmountPerInstruction As String = "AmountPerInstruction"
			 Public Const TransactionTime As String = "TransactionTime"
			 Public Const TrnsactionFrequency As String = "TrnsactionFrequency"
			 Public Const DayType As String = "DayType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICScheduleTransactionsMetadata)
			
				If ICScheduleTransactionsMetadata.mapDelegates Is Nothing Then
					ICScheduleTransactionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICScheduleTransactionsMetadata._meta Is Nothing Then
					ICScheduleTransactionsMetadata._meta = New ICScheduleTransactionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ScheduleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClientAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficaryName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TransactionCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ScheduleTransactionFromDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ScheduleTransactionToDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AmountPerInstruction", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("TransactionTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("TrnsactionFrequency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DayType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_ScheduleTransactions"
				meta.Destination = "IC_ScheduleTransactions"
				
				meta.spInsert = "proc_IC_ScheduleTransactionsInsert"
				meta.spUpdate = "proc_IC_ScheduleTransactionsUpdate"
				meta.spDelete = "proc_IC_ScheduleTransactionsDelete"
				meta.spLoadAll = "proc_IC_ScheduleTransactionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ScheduleTransactionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICScheduleTransactionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
