
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionAccountsCollectionNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionAccountsCollectionNature))> _
	<XmlType("ICCollectionAccountsCollectionNature")> _	
	Partial Public Class ICCollectionAccountsCollectionNature 
		Inherits esICCollectionAccountsCollectionNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionAccountsCollectionNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String)
			Dim obj As New ICCollectionAccountsCollectionNature()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.CollectionNatureCode = collectionNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionAccountsCollectionNature()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.CollectionNatureCode = collectionNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionAccountsCollectionNatureCollection")> _
	Partial Public Class ICCollectionAccountsCollectionNatureCollection
		Inherits esICCollectionAccountsCollectionNatureCollection
		Implements IEnumerable(Of ICCollectionAccountsCollectionNature)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String) As ICCollectionAccountsCollectionNature
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.CollectionNatureCode = collectionNatureCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionAccountsCollectionNature))> _
		Public Class ICCollectionAccountsCollectionNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionAccountsCollectionNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionAccountsCollectionNatureCollectionWCFPacket) As ICCollectionAccountsCollectionNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionAccountsCollectionNatureCollection) As ICCollectionAccountsCollectionNatureCollectionWCFPacket
				Return New ICCollectionAccountsCollectionNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionAccountsCollectionNatureQuery 
		Inherits esICCollectionAccountsCollectionNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionAccountsCollectionNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionAccountsCollectionNatureQuery) As String
			Return ICCollectionAccountsCollectionNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionAccountsCollectionNatureQuery
			Return DirectCast(ICCollectionAccountsCollectionNatureQuery.SerializeHelper.FromXml(query, GetType(ICCollectionAccountsCollectionNatureQuery)), ICCollectionAccountsCollectionNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, collectionNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, collectionNatureCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, collectionNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, collectionNatureCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String) As Boolean
		
			Dim query As New ICCollectionAccountsCollectionNatureQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.CollectionNatureCode = collectionNatureCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("CollectionNatureCode", collectionNatureCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICCollectionAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICCollectionAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.Currency, value) Then
					Me._UpToICCollectionAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CollectionNatureCode, value) Then
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
					Me.OnPropertyChanged("UpToICCollectionNatureByCollectionNatureCode")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNature.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionAccountsByAccountNumber As ICCollectionAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsActive)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionAccountsCollectionNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionAccountsCollectionNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionAccountsCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionAccountsCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionAccountsCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionAccountsCollectionNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNatureCollection
		Inherits esEntityCollection(Of ICCollectionAccountsCollectionNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionAccountsCollectionNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionAccountsCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionAccountsCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionAccountsCollectionNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionAccountsCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionAccountsCollectionNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionAccountsCollectionNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsActive" 
					Return Me.IsActive
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionAccountsCollectionNature 
		Inherits esICCollectionAccountsCollectionNature
		
	
		#Region "UpToICCollectionProductTypeCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionAccountsCollectionNature1
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICCollectionProductTypeCollection As ICCollectionProductTypeCollection
		
			Get
				If Me._UpToICCollectionProductTypeCollection Is Nothing Then
					Me._UpToICCollectionProductTypeCollection = New ICCollectionProductTypeCollection()
					Me._UpToICCollectionProductTypeCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICCollectionProductTypeCollection", Me._UpToICCollectionProductTypeCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.AccountNumber.Equals(Nothing) Then 
				
						Dim m As New ICCollectionProductTypeQuery("m")
						Dim j As New ICCollectionAccountsCollectionNatureProductTypeQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.ProductTypeCode = j.ProductTypeCode)
                        m.Where(j.AccountNumber = Me.AccountNumber)
                        m.Where(j.BranchCode = Me.BranchCode)
                        m.Where(j.Currency = Me.Currency)
                        m.Where(j.CollectionNatureCode = Me.CollectionNatureCode)

						Me._UpToICCollectionProductTypeCollection.Load(m)

					End If
				End If

				Return Me._UpToICCollectionProductTypeCollection
			End Get
			
			Set(ByVal value As ICCollectionProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICCollectionProductTypeCollection Is Nothing Then

					Me.RemovePostSave("UpToICCollectionProductTypeCollection")
					Me._UpToICCollectionProductTypeCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionAccountsCollectionNature1
		''' </summary>
		Public Sub AssociateICCollectionProductTypeCollection(entity As ICCollectionProductType)
			If Me._ICCollectionAccountsCollectionNatureProductTypeCollection Is Nothing Then
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection = New ICCollectionAccountsCollectionNatureProductTypeCollection()
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollection", Me._ICCollectionAccountsCollectionNatureProductTypeCollection)
			End If
			
			Dim obj As ICCollectionAccountsCollectionNatureProductType = Me._ICCollectionAccountsCollectionNatureProductTypeCollection.AddNew()
			obj.AccountNumber = Me.AccountNumber
			obj.BranchCode = Me.BranchCode
			obj.Currency = Me.Currency
			obj.CollectionNatureCode = Me.CollectionNatureCode
			obj.ProductTypeCode = entity.ProductTypeCode			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionAccountsCollectionNature1
		''' </summary>
		Public Sub DissociateICCollectionProductTypeCollection(entity As ICCollectionProductType)
			If Me._ICCollectionAccountsCollectionNatureProductTypeCollection Is Nothing Then
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection = new ICCollectionAccountsCollectionNatureProductTypeCollection()
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollection", Me._ICCollectionAccountsCollectionNatureProductTypeCollection)
			End If

			Dim obj As ICCollectionAccountsCollectionNatureProductType = Me._ICCollectionAccountsCollectionNatureProductTypeCollection.AddNew()
			obj.AccountNumber = Me.AccountNumber
			obj.BranchCode = Me.BranchCode
			obj.Currency = Me.Currency
			obj.CollectionNatureCode = Me.CollectionNatureCode
            obj.ProductTypeCode = entity.ProductTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICCollectionProductTypeCollection As ICCollectionProductTypeCollection
		Private _ICCollectionAccountsCollectionNatureProductTypeCollection As ICCollectionAccountsCollectionNatureProductTypeCollection
		#End Region

		#Region "ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionAccountsCollectionNature.ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,CollectionNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,CollectionNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionAccountsCollectionNatureQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsCollectionNatureProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsCollectionNatureProductTypeQuery), New ICCollectionAccountsCollectionNatureProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.CollectionNatureCode = mee.CollectionNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionAccountsCollectionNature1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber As ICCollectionAccountsCollectionNatureProductTypeCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber Is Nothing Then
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber = New ICCollectionAccountsCollectionNatureProductTypeCollection()
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber", Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.CollectionNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.CollectionNatureCode = Me.CollectionNatureCode)
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CollectionNatureCode, Me.CollectionNatureCode)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollectionNatureProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber")
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber As ICCollectionAccountsCollectionNatureProductTypeCollection
		#End Region

		#Region "UpToICCollectionAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNature_IC_CollectionAccounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionAccountsByAccountNumber As ICCollectionAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICCollectionAccountsByAccountNumber = New ICCollectionAccounts()
					Me._UpToICCollectionAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionAccountsByAccountNumber", Me._UpToICCollectionAccountsByAccountNumber)
					Me._UpToICCollectionAccountsByAccountNumber.Query.Where(Me._UpToICCollectionAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICCollectionAccountsByAccountNumber.Query.Where(Me._UpToICCollectionAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICCollectionAccountsByAccountNumber.Query.Where(Me._UpToICCollectionAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICCollectionAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICCollectionAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICCollectionAccounts)
				Me.RemovePreSave("UpToICCollectionAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICCollectionAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICCollectionAccountsByAccountNumber = value
					Me.SetPreSave("UpToICCollectionAccountsByAccountNumber", Me._UpToICCollectionAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionNatureByCollectionNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNature_IC_CollectionNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionNatureByCollectionNatureCode Is Nothing _
						 AndAlso Not CollectionNatureCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionNatureByCollectionNatureCode = New ICCollectionNature()
					Me._UpToICCollectionNatureByCollectionNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Where(Me._UpToICCollectionNatureByCollectionNatureCode.Query.CollectionNatureCode = Me.CollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Load()
				End If

				Return Me._UpToICCollectionNatureByCollectionNatureCode
			End Get
			
            Set(ByVal value As ICCollectionNature)
				Me.RemovePreSave("UpToICCollectionNatureByCollectionNatureCode")
				

				If value Is Nothing Then
				
					Me.CollectionNatureCode = Nothing
				
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
				Else
				
					Me.CollectionNatureCode = value.CollectionNatureCode
					
					Me._UpToICCollectionNatureByCollectionNatureCode = value
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber"
					coll = Me.ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionNatureProductTypeCollectionByAccountNumber", GetType(ICCollectionAccountsCollectionNatureProductTypeCollection), New ICCollectionAccountsCollectionNatureProductType()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionAccountsCollectionNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CollectionNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CollectionNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsApproved, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.IsActive, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionAccountsCollectionNatureMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionAccountsCollectionNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "isApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionAccountsCollectionNatureMetadata)
			
				If ICCollectionAccountsCollectionNatureMetadata.mapDelegates Is Nothing Then
					ICCollectionAccountsCollectionNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionAccountsCollectionNatureMetadata._meta Is Nothing Then
					ICCollectionAccountsCollectionNatureMetadata._meta = New ICCollectionAccountsCollectionNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_CollectionAccountsCollectionNature"
				meta.Destination = "IC_CollectionAccountsCollectionNature"
				
				meta.spInsert = "proc_IC_CollectionAccountsCollectionNatureInsert"
				meta.spUpdate = "proc_IC_CollectionAccountsCollectionNatureUpdate"
				meta.spDelete = "proc_IC_CollectionAccountsCollectionNatureDelete"
				meta.spLoadAll = "proc_IC_CollectionAccountsCollectionNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionAccountsCollectionNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionAccountsCollectionNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
