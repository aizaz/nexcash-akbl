
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/20/2014 11:56:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PendingEODInstructions' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPendingEODInstructions))> _
	<XmlType("ICPendingEODInstructions")> _	
	Partial Public Class ICPendingEODInstructions 
		Inherits esICPendingEODInstructions
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPendingEODInstructions()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal eODInstructionID As System.Int32)
			Dim obj As New ICPendingEODInstructions()
			obj.EODInstructionID = eODInstructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal eODInstructionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPendingEODInstructions()
			obj.EODInstructionID = eODInstructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPendingEODInstructionsCollection")> _
	Partial Public Class ICPendingEODInstructionsCollection
		Inherits esICPendingEODInstructionsCollection
		Implements IEnumerable(Of ICPendingEODInstructions)
	
		Public Function FindByPrimaryKey(ByVal eODInstructionID As System.Int32) As ICPendingEODInstructions
			Return MyBase.SingleOrDefault(Function(e) e.EODInstructionID.HasValue AndAlso e.EODInstructionID.Value = eODInstructionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPendingEODInstructions))> _
		Public Class ICPendingEODInstructionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPendingEODInstructionsCollection)
			
			Public Shared Widening Operator CType(packet As ICPendingEODInstructionsCollectionWCFPacket) As ICPendingEODInstructionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPendingEODInstructionsCollection) As ICPendingEODInstructionsCollectionWCFPacket
				Return New ICPendingEODInstructionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPendingEODInstructionsQuery 
		Inherits esICPendingEODInstructionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPendingEODInstructionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPendingEODInstructionsQuery) As String
			Return ICPendingEODInstructionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPendingEODInstructionsQuery
			Return DirectCast(ICPendingEODInstructionsQuery.SerializeHelper.FromXml(query, GetType(ICPendingEODInstructionsQuery)), ICPendingEODInstructionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPendingEODInstructions
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal eODInstructionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(eODInstructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(eODInstructionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal eODInstructionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(eODInstructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(eODInstructionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal eODInstructionID As System.Int32) As Boolean
		
			Dim query As New ICPendingEODInstructionsQuery()
			query.Where(query.EODInstructionID = eODInstructionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal eODInstructionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("EODInstructionID", eODInstructionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PendingEODInstructions.EODInstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EODInstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.EODInstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.EODInstructionID, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.EODInstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.InstructionID, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountNo, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountProductCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountProductCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountProductCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountProductCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountProductCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountBranchCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountSchemeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountSchemeCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountSchemeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountSchemeCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountSchemeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountCurrency, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FromAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountType, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FromAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountNo, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountProductCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountProductCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountProductCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountProductCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountProductCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountBranchCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountSchemeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountSchemeCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountSchemeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountSchemeCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountSchemeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountCurrency, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ToAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountType, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ToAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.DrGlCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DrGlCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.DrGlCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.DrGlCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.DrGlCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.CrGlCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CrGlCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.CrGlCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.CrGlCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.CrGlCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPendingEODInstructionsMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPendingEODInstructionsMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FinalStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FinalStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.FinalStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.FinalStatus, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FinalStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.LastStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.LastStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.LastStatus, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.LastStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.LastProcessDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastProcessDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPendingEODInstructionsMetadata.ColumnNames.LastProcessDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPendingEODInstructionsMetadata.ColumnNames.LastProcessDate, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.LastProcessDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.IsProcessed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsProcessed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsProcessed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsProcessed, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsProcessed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.IsReversalFromSecondLeg
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReversalFromSecondLeg As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsReversalFromSecondLeg)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsReversalFromSecondLeg, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsReversalFromSecondLeg)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.UserID, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.FailureStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FailureStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.FailureStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPendingEODInstructionsMetadata.ColumnNames.FailureStatus, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FailureStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.PaidDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaidDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPendingEODInstructionsMetadata.ColumnNames.PaidDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPendingEODInstructionsMetadata.ColumnNames.PaidDate, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.PaidDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.IsReversal
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReversal As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsReversal)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPendingEODInstructionsMetadata.ColumnNames.IsReversal, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsReversal)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.OriginatingTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OriginatingTranType As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.OriginatingTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.OriginatingTranType, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.OriginatingTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PendingEODInstructions.RespondingTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RespondingTranType As System.String
			Get
				Return MyBase.GetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.RespondingTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPendingEODInstructionsMetadata.ColumnNames.RespondingTranType, value) Then
					OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.RespondingTranType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "EODInstructionID"
							Me.str().EODInstructionID = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "FromAccountNo"
							Me.str().FromAccountNo = CType(value, string)
												
						Case "FromAccountProductCode"
							Me.str().FromAccountProductCode = CType(value, string)
												
						Case "FromAccountBranchCode"
							Me.str().FromAccountBranchCode = CType(value, string)
												
						Case "FromAccountSchemeCode"
							Me.str().FromAccountSchemeCode = CType(value, string)
												
						Case "FromAccountCurrency"
							Me.str().FromAccountCurrency = CType(value, string)
												
						Case "FromAccountType"
							Me.str().FromAccountType = CType(value, string)
												
						Case "ToAccountNo"
							Me.str().ToAccountNo = CType(value, string)
												
						Case "ToAccountProductCode"
							Me.str().ToAccountProductCode = CType(value, string)
												
						Case "ToAccountBranchCode"
							Me.str().ToAccountBranchCode = CType(value, string)
												
						Case "ToAccountSchemeCode"
							Me.str().ToAccountSchemeCode = CType(value, string)
												
						Case "ToAccountCurrency"
							Me.str().ToAccountCurrency = CType(value, string)
												
						Case "ToAccountType"
							Me.str().ToAccountType = CType(value, string)
												
						Case "DrGlCode"
							Me.str().DrGlCode = CType(value, string)
												
						Case "CrGlCode"
							Me.str().CrGlCode = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "FinalStatus"
							Me.str().FinalStatus = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "LastStatus"
							Me.str().LastStatus = CType(value, string)
												
						Case "LastProcessDate"
							Me.str().LastProcessDate = CType(value, string)
												
						Case "IsProcessed"
							Me.str().IsProcessed = CType(value, string)
												
						Case "IsReversalFromSecondLeg"
							Me.str().IsReversalFromSecondLeg = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "FailureStatus"
							Me.str().FailureStatus = CType(value, string)
												
						Case "PaidDate"
							Me.str().PaidDate = CType(value, string)
												
						Case "IsReversal"
							Me.str().IsReversal = CType(value, string)
												
						Case "OriginatingTranType"
							Me.str().OriginatingTranType = CType(value, string)
												
						Case "RespondingTranType"
							Me.str().RespondingTranType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "EODInstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.EODInstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.EODInstructionID)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.InstructionID)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.Amount)
							End If
						
						Case "FinalStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FinalStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FinalStatus)
							End If
						
						Case "Status"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Status = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.Status)
							End If
						
						Case "LastStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.LastStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.LastStatus)
							End If
						
						Case "LastProcessDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastProcessDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.LastProcessDate)
							End If
						
						Case "IsProcessed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsProcessed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsProcessed)
							End If
						
						Case "IsReversalFromSecondLeg"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReversalFromSecondLeg = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsReversalFromSecondLeg)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.UserID)
							End If
						
						Case "FailureStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FailureStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.FailureStatus)
							End If
						
						Case "PaidDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PaidDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.PaidDate)
							End If
						
						Case "IsReversal"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReversal = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPendingEODInstructionsMetadata.PropertyNames.IsReversal)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPendingEODInstructions)
				Me.entity = entity
			End Sub				
		
	
			Public Property EODInstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.EODInstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EODInstructionID = Nothing
					Else
						entity.EODInstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNo = Nothing
					Else
						entity.FromAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountProductCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountProductCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountProductCode = Nothing
					Else
						entity.FromAccountProductCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountBranchCode = Nothing
					Else
						entity.FromAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountSchemeCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountSchemeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountSchemeCode = Nothing
					Else
						entity.FromAccountSchemeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountCurrency = Nothing
					Else
						entity.FromAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountType As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountType = Nothing
					Else
						entity.FromAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNo = Nothing
					Else
						entity.ToAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountProductCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountProductCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountProductCode = Nothing
					Else
						entity.ToAccountProductCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountBranchCode = Nothing
					Else
						entity.ToAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountSchemeCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountSchemeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountSchemeCode = Nothing
					Else
						entity.ToAccountSchemeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountCurrency = Nothing
					Else
						entity.ToAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountType = Nothing
					Else
						entity.ToAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DrGlCode As System.String 
				Get
					Dim data_ As System.String = entity.DrGlCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DrGlCode = Nothing
					Else
						entity.DrGlCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CrGlCode As System.String 
				Get
					Dim data_ As System.String = entity.CrGlCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CrGlCode = Nothing
					Else
						entity.CrGlCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FinalStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FinalStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FinalStatus = Nothing
					Else
						entity.FinalStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Status
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.LastStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastStatus = Nothing
					Else
						entity.LastStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastProcessDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastProcessDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastProcessDate = Nothing
					Else
						entity.LastProcessDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsProcessed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsProcessed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsProcessed = Nothing
					Else
						entity.IsProcessed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReversalFromSecondLeg As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReversalFromSecondLeg
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReversalFromSecondLeg = Nothing
					Else
						entity.IsReversalFromSecondLeg = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FailureStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FailureStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FailureStatus = Nothing
					Else
						entity.FailureStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaidDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PaidDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaidDate = Nothing
					Else
						entity.PaidDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReversal As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReversal
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReversal = Nothing
					Else
						entity.IsReversal = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property OriginatingTranType As System.String 
				Get
					Dim data_ As System.String = entity.OriginatingTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OriginatingTranType = Nothing
					Else
						entity.OriginatingTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RespondingTranType As System.String 
				Get
					Dim data_ As System.String = entity.RespondingTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RespondingTranType = Nothing
					Else
						entity.RespondingTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPendingEODInstructions
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPendingEODInstructionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPendingEODInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPendingEODInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPendingEODInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPendingEODInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPendingEODInstructionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPendingEODInstructionsCollection
		Inherits esEntityCollection(Of ICPendingEODInstructions)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPendingEODInstructionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPendingEODInstructionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPendingEODInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPendingEODInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPendingEODInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPendingEODInstructionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPendingEODInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPendingEODInstructionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPendingEODInstructionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPendingEODInstructionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPendingEODInstructionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "EODInstructionID" 
					Return Me.EODInstructionID
				Case "InstructionID" 
					Return Me.InstructionID
				Case "FromAccountNo" 
					Return Me.FromAccountNo
				Case "FromAccountProductCode" 
					Return Me.FromAccountProductCode
				Case "FromAccountBranchCode" 
					Return Me.FromAccountBranchCode
				Case "FromAccountSchemeCode" 
					Return Me.FromAccountSchemeCode
				Case "FromAccountCurrency" 
					Return Me.FromAccountCurrency
				Case "FromAccountType" 
					Return Me.FromAccountType
				Case "ToAccountNo" 
					Return Me.ToAccountNo
				Case "ToAccountProductCode" 
					Return Me.ToAccountProductCode
				Case "ToAccountBranchCode" 
					Return Me.ToAccountBranchCode
				Case "ToAccountSchemeCode" 
					Return Me.ToAccountSchemeCode
				Case "ToAccountCurrency" 
					Return Me.ToAccountCurrency
				Case "ToAccountType" 
					Return Me.ToAccountType
				Case "DrGlCode" 
					Return Me.DrGlCode
				Case "CrGlCode" 
					Return Me.CrGlCode
				Case "Amount" 
					Return Me.Amount
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "FinalStatus" 
					Return Me.FinalStatus
				Case "Status" 
					Return Me.Status
				Case "LastStatus" 
					Return Me.LastStatus
				Case "LastProcessDate" 
					Return Me.LastProcessDate
				Case "IsProcessed" 
					Return Me.IsProcessed
				Case "IsReversalFromSecondLeg" 
					Return Me.IsReversalFromSecondLeg
				Case "UserID" 
					Return Me.UserID
				Case "FailureStatus" 
					Return Me.FailureStatus
				Case "PaidDate" 
					Return Me.PaidDate
				Case "IsReversal" 
					Return Me.IsReversal
				Case "OriginatingTranType" 
					Return Me.OriginatingTranType
				Case "RespondingTranType" 
					Return Me.RespondingTranType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property EODInstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.EODInstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountProductCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountProductCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountSchemeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountSchemeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FromAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountProductCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountProductCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountSchemeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountSchemeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ToAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DrGlCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.DrGlCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CrGlCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.CrGlCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FinalStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FinalStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.Status, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property LastStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.LastStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property LastProcessDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.LastProcessDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsProcessed As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.IsProcessed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsReversalFromSecondLeg As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.IsReversalFromSecondLeg, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FailureStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.FailureStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PaidDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.PaidDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsReversal As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.IsReversal, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property OriginatingTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.OriginatingTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RespondingTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPendingEODInstructionsMetadata.ColumnNames.RespondingTranType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPendingEODInstructions 
		Inherits esICPendingEODInstructions
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICPendingEODInstructionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.EODInstructionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.EODInstructionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.InstructionID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountNo, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountProductCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountProductCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountBranchCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountSchemeCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountSchemeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountCurrency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FromAccountType, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FromAccountType
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountNo, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountProductCode, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountProductCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountBranchCode, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountSchemeCode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountSchemeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountCurrency, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ToAccountType, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ToAccountType
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.DrGlCode, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.DrGlCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.CrGlCode, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.CrGlCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.Amount, 16, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.ProductTypeCode, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FinalStatus, 18, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FinalStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.Status, 19, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.Status
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.LastStatus, 20, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.LastStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.LastProcessDate, 21, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.LastProcessDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.IsProcessed, 22, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.IsProcessed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.IsReversalFromSecondLeg, 23, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.IsReversalFromSecondLeg
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.UserID, 24, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.FailureStatus, 25, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.FailureStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.PaidDate, 26, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.PaidDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.IsReversal, 27, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.IsReversal
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.OriginatingTranType, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.OriginatingTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPendingEODInstructionsMetadata.ColumnNames.RespondingTranType, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPendingEODInstructionsMetadata.PropertyNames.RespondingTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPendingEODInstructionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const EODInstructionID As String = "EODInstructionID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountProductCode As String = "FromAccountProductCode"
			 Public Const FromAccountBranchCode As String = "FromAccountBranchCode"
			 Public Const FromAccountSchemeCode As String = "FromAccountSchemeCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountProductCode As String = "ToAccountProductCode"
			 Public Const ToAccountBranchCode As String = "ToAccountBranchCode"
			 Public Const ToAccountSchemeCode As String = "ToAccountSchemeCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const DrGlCode As String = "DrGlCode"
			 Public Const CrGlCode As String = "CrGlCode"
			 Public Const Amount As String = "Amount"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const FinalStatus As String = "FinalStatus"
			 Public Const Status As String = "Status"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const LastProcessDate As String = "LastProcessDate"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsReversalFromSecondLeg As String = "IsReversalFromSecondLeg"
			 Public Const UserID As String = "UserID"
			 Public Const FailureStatus As String = "FailureStatus"
			 Public Const PaidDate As String = "PaidDate"
			 Public Const IsReversal As String = "IsReversal"
			 Public Const OriginatingTranType As String = "OriginatingTranType"
			 Public Const RespondingTranType As String = "RespondingTranType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const EODInstructionID As String = "EODInstructionID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountProductCode As String = "FromAccountProductCode"
			 Public Const FromAccountBranchCode As String = "FromAccountBranchCode"
			 Public Const FromAccountSchemeCode As String = "FromAccountSchemeCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountProductCode As String = "ToAccountProductCode"
			 Public Const ToAccountBranchCode As String = "ToAccountBranchCode"
			 Public Const ToAccountSchemeCode As String = "ToAccountSchemeCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const DrGlCode As String = "DrGlCode"
			 Public Const CrGlCode As String = "CrGlCode"
			 Public Const Amount As String = "Amount"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const FinalStatus As String = "FinalStatus"
			 Public Const Status As String = "Status"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const LastProcessDate As String = "LastProcessDate"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsReversalFromSecondLeg As String = "IsReversalFromSecondLeg"
			 Public Const UserID As String = "UserID"
			 Public Const FailureStatus As String = "FailureStatus"
			 Public Const PaidDate As String = "PaidDate"
			 Public Const IsReversal As String = "IsReversal"
			 Public Const OriginatingTranType As String = "OriginatingTranType"
			 Public Const RespondingTranType As String = "RespondingTranType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPendingEODInstructionsMetadata)
			
				If ICPendingEODInstructionsMetadata.mapDelegates Is Nothing Then
					ICPendingEODInstructionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPendingEODInstructionsMetadata._meta Is Nothing Then
					ICPendingEODInstructionsMetadata._meta = New ICPendingEODInstructionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("EODInstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FromAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountProductCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountSchemeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountProductCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountSchemeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DrGlCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CrGlCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Amount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FinalStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Status", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("LastStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("LastProcessDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsProcessed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsReversalFromSecondLeg", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FailureStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PaidDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsReversal", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("OriginatingTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RespondingTranType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_PendingEODInstructions"
				meta.Destination = "IC_PendingEODInstructions"
				
				meta.spInsert = "proc_IC_PendingEODInstructionsInsert"
				meta.spUpdate = "proc_IC_PendingEODInstructionsUpdate"
				meta.spDelete = "proc_IC_PendingEODInstructionsDelete"
				meta.spLoadAll = "proc_IC_PendingEODInstructionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PendingEODInstructionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPendingEODInstructionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
