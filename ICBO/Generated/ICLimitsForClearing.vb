
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:58 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_LimitsForClearing' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICLimitsForClearing))> _
	<XmlType("ICLimitsForClearing")> _	
	Partial Public Class ICLimitsForClearing 
		Inherits esICLimitsForClearing
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICLimitsForClearing()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal limitsForClearingID As System.Int32)
			Dim obj As New ICLimitsForClearing()
			obj.LimitsForClearingID = limitsForClearingID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal limitsForClearingID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICLimitsForClearing()
			obj.LimitsForClearingID = limitsForClearingID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICLimitsForClearingCollection")> _
	Partial Public Class ICLimitsForClearingCollection
		Inherits esICLimitsForClearingCollection
		Implements IEnumerable(Of ICLimitsForClearing)
	
		Public Function FindByPrimaryKey(ByVal limitsForClearingID As System.Int32) As ICLimitsForClearing
			Return MyBase.SingleOrDefault(Function(e) e.LimitsForClearingID.HasValue AndAlso e.LimitsForClearingID.Value = limitsForClearingID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICLimitsForClearing))> _
		Public Class ICLimitsForClearingCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICLimitsForClearingCollection)
			
			Public Shared Widening Operator CType(packet As ICLimitsForClearingCollectionWCFPacket) As ICLimitsForClearingCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICLimitsForClearingCollection) As ICLimitsForClearingCollectionWCFPacket
				Return New ICLimitsForClearingCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICLimitsForClearingQuery 
		Inherits esICLimitsForClearingQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICLimitsForClearingQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICLimitsForClearingQuery) As String
			Return ICLimitsForClearingQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICLimitsForClearingQuery
			Return DirectCast(ICLimitsForClearingQuery.SerializeHelper.FromXml(query, GetType(ICLimitsForClearingQuery)), ICLimitsForClearingQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICLimitsForClearing
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal limitsForClearingID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(limitsForClearingID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(limitsForClearingID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal limitsForClearingID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(limitsForClearingID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(limitsForClearingID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal limitsForClearingID As System.Int32) As Boolean
		
			Dim query As New ICLimitsForClearingQuery()
			query.Where(query.LimitsForClearingID = limitsForClearingID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal limitsForClearingID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("LimitsForClearingID", limitsForClearingID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_LimitsForClearing.LimitsForClearingID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LimitsForClearingID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.LimitsForClearingID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.LimitsForClearingID, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.LimitsForClearingID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.MakerLimits
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MakerLimits As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitsForClearingMetadata.ColumnNames.MakerLimits)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitsForClearingMetadata.ColumnNames.MakerLimits, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.MakerLimits)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.ApprovalLimits
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalLimits As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitsForClearingMetadata.ColumnNames.ApprovalLimits)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitsForClearingMetadata.ColumnNames.ApprovalLimits, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.ApprovalLimits)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICLimitsForClearingMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICLimitsForClearingMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitsForClearingMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitsForClearing.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICLimitsForClearingMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICLimitsForClearingMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "LimitsForClearingID"
							Me.str().LimitsForClearingID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "MakerLimits"
							Me.str().MakerLimits = CType(value, string)
												
						Case "ApprovalLimits"
							Me.str().ApprovalLimits = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "LimitsForClearingID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.LimitsForClearingID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.LimitsForClearingID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.UserID)
							End If
						
						Case "MakerLimits"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.MakerLimits = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.MakerLimits)
							End If
						
						Case "ApprovalLimits"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.ApprovalLimits = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.ApprovalLimits)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICLimitsForClearingMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICLimitsForClearing)
				Me.entity = entity
			End Sub				
		
	
			Public Property LimitsForClearingID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.LimitsForClearingID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LimitsForClearingID = Nothing
					Else
						entity.LimitsForClearingID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property MakerLimits As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.MakerLimits
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MakerLimits = Nothing
					Else
						entity.MakerLimits = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalLimits As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.ApprovalLimits
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalLimits = Nothing
					Else
						entity.ApprovalLimits = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICLimitsForClearing
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICLimitsForClearingMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICLimitsForClearingQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICLimitsForClearingQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICLimitsForClearingQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICLimitsForClearingQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICLimitsForClearingQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICLimitsForClearingCollection
		Inherits esEntityCollection(Of ICLimitsForClearing)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICLimitsForClearingMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICLimitsForClearingCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICLimitsForClearingQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICLimitsForClearingQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICLimitsForClearingQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICLimitsForClearingQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICLimitsForClearingQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICLimitsForClearingQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICLimitsForClearingQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICLimitsForClearingQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICLimitsForClearingMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "LimitsForClearingID" 
					Return Me.LimitsForClearingID
				Case "UserID" 
					Return Me.UserID
				Case "MakerLimits" 
					Return Me.MakerLimits
				Case "ApprovalLimits" 
					Return Me.ApprovalLimits
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property LimitsForClearingID As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.LimitsForClearingID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property MakerLimits As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.MakerLimits, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalLimits As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.ApprovalLimits, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitsForClearingMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICLimitsForClearing 
		Inherits esICLimitsForClearing
		
	
		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_LimitsForClearing_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICLimitsForClearingMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.LimitsForClearingID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.LimitsForClearingID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.UserID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.MakerLimits, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.MakerLimits
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.ApprovalLimits, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.ApprovalLimits
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.CreatedBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.CreatedDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.Creater, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitsForClearingMetadata.ColumnNames.CreationDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICLimitsForClearingMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICLimitsForClearingMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const LimitsForClearingID As String = "LimitsForClearingID"
			 Public Const UserID As String = "UserID"
			 Public Const MakerLimits As String = "MakerLimits"
			 Public Const ApprovalLimits As String = "ApprovalLimits"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const LimitsForClearingID As String = "LimitsForClearingID"
			 Public Const UserID As String = "UserID"
			 Public Const MakerLimits As String = "MakerLimits"
			 Public Const ApprovalLimits As String = "ApprovalLimits"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICLimitsForClearingMetadata)
			
				If ICLimitsForClearingMetadata.mapDelegates Is Nothing Then
					ICLimitsForClearingMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICLimitsForClearingMetadata._meta Is Nothing Then
					ICLimitsForClearingMetadata._meta = New ICLimitsForClearingMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("LimitsForClearingID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("MakerLimits", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("ApprovalLimits", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_LimitsForClearing"
				meta.Destination = "IC_LimitsForClearing"
				
				meta.spInsert = "proc_IC_LimitsForClearingInsert"
				meta.spUpdate = "proc_IC_LimitsForClearingUpdate"
				meta.spDelete = "proc_IC_LimitsForClearingDelete"
				meta.spLoadAll = "proc_IC_LimitsForClearingLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_LimitsForClearingLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICLimitsForClearingMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
