
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 5:15:19 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Province' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICProvince))> _
	<XmlType("ICProvince")> _	
	Partial Public Class ICProvince 
		Inherits esICProvince
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICProvince()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal provinceCode As System.Int32)
			Dim obj As New ICProvince()
			obj.ProvinceCode = provinceCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal provinceCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICProvince()
			obj.ProvinceCode = provinceCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICProvinceCollection")> _
	Partial Public Class ICProvinceCollection
		Inherits esICProvinceCollection
		Implements IEnumerable(Of ICProvince)
	
		Public Function FindByPrimaryKey(ByVal provinceCode As System.Int32) As ICProvince
			Return MyBase.SingleOrDefault(Function(e) e.ProvinceCode.HasValue AndAlso e.ProvinceCode.Value = provinceCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICProvince))> _
		Public Class ICProvinceCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICProvinceCollection)
			
			Public Shared Widening Operator CType(packet As ICProvinceCollectionWCFPacket) As ICProvinceCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICProvinceCollection) As ICProvinceCollectionWCFPacket
				Return New ICProvinceCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICProvinceQuery 
		Inherits esICProvinceQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICProvinceQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICProvinceQuery) As String
			Return ICProvinceQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICProvinceQuery
			Return DirectCast(ICProvinceQuery.SerializeHelper.FromXml(query, GetType(ICProvinceQuery)), ICProvinceQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICProvince
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal provinceCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(provinceCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(provinceCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal provinceCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(provinceCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(provinceCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal provinceCode As System.Int32) As Boolean
		
			Dim query As New ICProvinceQuery()
			query.Where(query.ProvinceCode = provinceCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal provinceCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ProvinceCode", provinceCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Province.ProvinceCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProvinceCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProvinceMetadata.ColumnNames.ProvinceCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProvinceMetadata.ColumnNames.ProvinceCode, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.ProvinceCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.ProvinceName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProvinceName As System.String
			Get
				Return MyBase.GetSystemString(ICProvinceMetadata.ColumnNames.ProvinceName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProvinceMetadata.ColumnNames.ProvinceName, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.ProvinceName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.CountryCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CountryCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProvinceMetadata.ColumnNames.CountryCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProvinceMetadata.ColumnNames.CountryCode, value) Then
					Me._UpToICCountryByCountryCode = Nothing
					Me.OnPropertyChanged("UpToICCountryByCountryCode")
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.CountryCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProvinceMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProvinceMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProvinceMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProvinceMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProvinceMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProvinceMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProvinceMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProvinceMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProvinceMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProvinceMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProvinceMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProvinceMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProvinceMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProvinceMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProvinceMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProvinceMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Province.DisplayCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisplayCode As System.String
			Get
				Return MyBase.GetSystemString(ICProvinceMetadata.ColumnNames.DisplayCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProvinceMetadata.ColumnNames.DisplayCode, value) Then
					OnPropertyChanged(ICProvinceMetadata.PropertyNames.DisplayCode)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCountryByCountryCode As ICCountry
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ProvinceCode"
							Me.str().ProvinceCode = CType(value, string)
												
						Case "ProvinceName"
							Me.str().ProvinceName = CType(value, string)
												
						Case "CountryCode"
							Me.str().CountryCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "DisplayCode"
							Me.str().DisplayCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ProvinceCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ProvinceCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.ProvinceCode)
							End If
						
						Case "CountryCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CountryCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.CountryCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreateDate)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProvinceMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICProvince)
				Me.entity = entity
			End Sub				
		
	
			Public Property ProvinceCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ProvinceCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProvinceCode = Nothing
					Else
						entity.ProvinceCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProvinceName As System.String 
				Get
					Dim data_ As System.String = entity.ProvinceName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProvinceName = Nothing
					Else
						entity.ProvinceName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CountryCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CountryCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CountryCode = Nothing
					Else
						entity.CountryCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisplayCode As System.String 
				Get
					Dim data_ As System.String = entity.DisplayCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisplayCode = Nothing
					Else
						entity.DisplayCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICProvince
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICProvinceMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICProvinceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICProvinceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICProvinceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICProvinceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICProvinceQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICProvinceCollection
		Inherits esEntityCollection(Of ICProvince)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICProvinceMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICProvinceCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICProvinceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICProvinceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICProvinceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICProvinceQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICProvinceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICProvinceQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICProvinceQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICProvinceQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICProvinceMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ProvinceCode" 
					Return Me.ProvinceCode
				Case "ProvinceName" 
					Return Me.ProvinceName
				Case "CountryCode" 
					Return Me.CountryCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "DisplayCode" 
					Return Me.DisplayCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ProvinceCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.ProvinceCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProvinceName As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.ProvinceName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CountryCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.CountryCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DisplayCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProvinceMetadata.ColumnNames.DisplayCode, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICProvince 
		Inherits esICProvince
		
	
		#Region "ICCityCollectionByProvinceCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCityCollectionByProvinceCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICProvince.ICCityCollectionByProvinceCode_Delegate)
				map.PropertyName = "ICCityCollectionByProvinceCode"
				map.MyColumnName = "ProvinceCode"
				map.ParentColumnName = "ProvinceCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCityCollectionByProvinceCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICProvinceQuery(data.NextAlias())
			
			Dim mee As ICCityQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCityQuery), New ICCityQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProvinceCode = mee.ProvinceCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_City_IC_Province1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCityCollectionByProvinceCode As ICCityCollection 
		
			Get
				If Me._ICCityCollectionByProvinceCode Is Nothing Then
					Me._ICCityCollectionByProvinceCode = New ICCityCollection()
					Me._ICCityCollectionByProvinceCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCityCollectionByProvinceCode", Me._ICCityCollectionByProvinceCode)
				
					If Not Me.ProvinceCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCityCollectionByProvinceCode.Query.Where(Me._ICCityCollectionByProvinceCode.Query.ProvinceCode = Me.ProvinceCode)
							Me._ICCityCollectionByProvinceCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCityCollectionByProvinceCode.fks.Add(ICCityMetadata.ColumnNames.ProvinceCode, Me.ProvinceCode)
					End If
				End If

				Return Me._ICCityCollectionByProvinceCode
			End Get
			
			Set(ByVal value As ICCityCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCityCollectionByProvinceCode Is Nothing Then

					Me.RemovePostSave("ICCityCollectionByProvinceCode")
					Me._ICCityCollectionByProvinceCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCityCollectionByProvinceCode As ICCityCollection
		#End Region

		#Region "UpToICCountryByCountryCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Province_IC_Country
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCountryByCountryCode As ICCountry
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCountryByCountryCode Is Nothing _
						 AndAlso Not CountryCode.Equals(Nothing)  Then
						
					Me._UpToICCountryByCountryCode = New ICCountry()
					Me._UpToICCountryByCountryCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCountryByCountryCode", Me._UpToICCountryByCountryCode)
					Me._UpToICCountryByCountryCode.Query.Where(Me._UpToICCountryByCountryCode.Query.CountryCode = Me.CountryCode)
					Me._UpToICCountryByCountryCode.Query.Load()
				End If

				Return Me._UpToICCountryByCountryCode
			End Get
			
            Set(ByVal value As ICCountry)
				Me.RemovePreSave("UpToICCountryByCountryCode")
				

				If value Is Nothing Then
				
					Me.CountryCode = Nothing
				
					Me._UpToICCountryByCountryCode = Nothing
				Else
				
					Me.CountryCode = value.CountryCode
					
					Me._UpToICCountryByCountryCode = value
					Me.SetPreSave("UpToICCountryByCountryCode", Me._UpToICCountryByCountryCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCityCollectionByProvinceCode"
					coll = Me.ICCityCollectionByProvinceCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCityCollectionByProvinceCode", GetType(ICCityCollection), New ICCity()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCountryByCountryCode Is Nothing Then
				Me.CountryCode = Me._UpToICCountryByCountryCode.CountryCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCityCollectionByProvinceCode Is Nothing Then
				Apply(Me._ICCityCollectionByProvinceCode, "ProvinceCode", Me.ProvinceCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICProvinceMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.ProvinceCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.ProvinceCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.ProvinceName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.ProvinceName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.CountryCode, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.CountryCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.IsActive, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.CreateBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.IsApproved, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.ApprovedOn, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.ApprovedBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProvinceMetadata.ColumnNames.DisplayCode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProvinceMetadata.PropertyNames.DisplayCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICProvinceMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ProvinceCode As String = "ProvinceCode"
			 Public Const ProvinceName As String = "ProvinceName"
			 Public Const CountryCode As String = "CountryCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ProvinceCode As String = "ProvinceCode"
			 Public Const ProvinceName As String = "ProvinceName"
			 Public Const CountryCode As String = "CountryCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICProvinceMetadata)
			
				If ICProvinceMetadata.mapDelegates Is Nothing Then
					ICProvinceMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICProvinceMetadata._meta Is Nothing Then
					ICProvinceMetadata._meta = New ICProvinceMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ProvinceCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProvinceName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CountryCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DisplayCode", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_Province"
				meta.Destination = "IC_Province"
				
				meta.spInsert = "proc_IC_ProvinceInsert"
				meta.spUpdate = "proc_IC_ProvinceUpdate"
				meta.spDelete = "proc_IC_ProvinceDelete"
				meta.spLoadAll = "proc_IC_ProvinceLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ProvinceLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICProvinceMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
