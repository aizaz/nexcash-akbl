
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/16/2014 11:29:58 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionValueDateClearingHistory' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionValueDateClearingHistory))> _
	<XmlType("ICCollectionValueDateClearingHistory")> _	
	Partial Public Class ICCollectionValueDateClearingHistory 
		Inherits esICCollectionValueDateClearingHistory
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionValueDateClearingHistory()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal historyID As System.Int32)
			Dim obj As New ICCollectionValueDateClearingHistory()
			obj.HistoryID = historyID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal historyID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionValueDateClearingHistory()
			obj.HistoryID = historyID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionValueDateClearingHistoryCollection")> _
	Partial Public Class ICCollectionValueDateClearingHistoryCollection
		Inherits esICCollectionValueDateClearingHistoryCollection
		Implements IEnumerable(Of ICCollectionValueDateClearingHistory)
	
		Public Function FindByPrimaryKey(ByVal historyID As System.Int32) As ICCollectionValueDateClearingHistory
			Return MyBase.SingleOrDefault(Function(e) e.HistoryID.HasValue AndAlso e.HistoryID.Value = historyID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionValueDateClearingHistory))> _
		Public Class ICCollectionValueDateClearingHistoryCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionValueDateClearingHistoryCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionValueDateClearingHistoryCollectionWCFPacket) As ICCollectionValueDateClearingHistoryCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionValueDateClearingHistoryCollection) As ICCollectionValueDateClearingHistoryCollectionWCFPacket
				Return New ICCollectionValueDateClearingHistoryCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionValueDateClearingHistoryQuery 
		Inherits esICCollectionValueDateClearingHistoryQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionValueDateClearingHistoryQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionValueDateClearingHistoryQuery) As String
			Return ICCollectionValueDateClearingHistoryQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionValueDateClearingHistoryQuery
			Return DirectCast(ICCollectionValueDateClearingHistoryQuery.SerializeHelper.FromXml(query, GetType(ICCollectionValueDateClearingHistoryQuery)), ICCollectionValueDateClearingHistoryQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionValueDateClearingHistory
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal historyID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(historyID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(historyID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal historyID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(historyID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(historyID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal historyID As System.Int32) As Boolean
		
			Dim query As New ICCollectionValueDateClearingHistoryQuery()
			query.Where(query.HistoryID = historyID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal historyID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("HistoryID", historyID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingHistory.HistoryID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HistoryID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.HistoryID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.HistoryID, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.HistoryID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingHistory.ExecutionDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExecutionDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutionDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutionDate, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.ExecutionDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingHistory.PassCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PassCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.PassCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.PassCount, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.PassCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingHistory.FailCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FailCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.FailCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.FailCount, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.FailCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingHistory.ExecutedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExecutedBy As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutedBy)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutedBy, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.ExecutedBy)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "HistoryID"
							Me.str().HistoryID = CType(value, string)
												
						Case "ExecutionDate"
							Me.str().ExecutionDate = CType(value, string)
												
						Case "PassCount"
							Me.str().PassCount = CType(value, string)
												
						Case "FailCount"
							Me.str().FailCount = CType(value, string)
												
						Case "ExecutedBy"
							Me.str().ExecutedBy = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "HistoryID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.HistoryID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.HistoryID)
							End If
						
						Case "ExecutionDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ExecutionDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.ExecutionDate)
							End If
						
						Case "PassCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PassCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.PassCount)
							End If
						
						Case "FailCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FailCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingHistoryMetadata.PropertyNames.FailCount)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionValueDateClearingHistory)
				Me.entity = entity
			End Sub				
		
	
			Public Property HistoryID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.HistoryID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HistoryID = Nothing
					Else
						entity.HistoryID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExecutionDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ExecutionDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExecutionDate = Nothing
					Else
						entity.ExecutionDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PassCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PassCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PassCount = Nothing
					Else
						entity.PassCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FailCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FailCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FailCount = Nothing
					Else
						entity.FailCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExecutedBy As System.String 
				Get
					Dim data_ As System.String = entity.ExecutedBy
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExecutedBy = Nothing
					Else
						entity.ExecutedBy = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionValueDateClearingHistory
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingHistoryMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionValueDateClearingHistoryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionValueDateClearingHistoryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionValueDateClearingHistoryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionValueDateClearingHistoryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionValueDateClearingHistoryQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionValueDateClearingHistoryCollection
		Inherits esEntityCollection(Of ICCollectionValueDateClearingHistory)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingHistoryMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionValueDateClearingHistoryCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionValueDateClearingHistoryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionValueDateClearingHistoryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionValueDateClearingHistoryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionValueDateClearingHistoryQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionValueDateClearingHistoryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionValueDateClearingHistoryQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionValueDateClearingHistoryQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionValueDateClearingHistoryQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingHistoryMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "HistoryID" 
					Return Me.HistoryID
				Case "ExecutionDate" 
					Return Me.ExecutionDate
				Case "PassCount" 
					Return Me.PassCount
				Case "FailCount" 
					Return Me.FailCount
				Case "ExecutedBy" 
					Return Me.ExecutedBy
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property HistoryID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingHistoryMetadata.ColumnNames.HistoryID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ExecutionDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutionDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PassCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingHistoryMetadata.ColumnNames.PassCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FailCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingHistoryMetadata.ColumnNames.FailCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ExecutedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutedBy, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionValueDateClearingHistory 
		Inherits esICCollectionValueDateClearingHistory
		
	
		#Region "ICCollectionValueDateClearingInstrumentsCollectionByHistoryID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionValueDateClearingInstrumentsCollectionByHistoryID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionValueDateClearingHistory.ICCollectionValueDateClearingInstrumentsCollectionByHistoryID_Delegate)
				map.PropertyName = "ICCollectionValueDateClearingInstrumentsCollectionByHistoryID"
				map.MyColumnName = "HistoryID"
				map.ParentColumnName = "HistoryID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionValueDateClearingInstrumentsCollectionByHistoryID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionValueDateClearingHistoryQuery(data.NextAlias())
			
			Dim mee As ICCollectionValueDateClearingInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionValueDateClearingInstrumentsQuery), New ICCollectionValueDateClearingInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.HistoryID = mee.HistoryID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionValueDateClearingInstruments_IC_CollectionValueDateClearingHistory
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionValueDateClearingInstrumentsCollectionByHistoryID As ICCollectionValueDateClearingInstrumentsCollection 
		
			Get
				If Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID Is Nothing Then
					Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID = New ICCollectionValueDateClearingInstrumentsCollection()
					Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionValueDateClearingInstrumentsCollectionByHistoryID", Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID)
				
					If Not Me.HistoryID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID.Query.Where(Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID.Query.HistoryID = Me.HistoryID)
							Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID.fks.Add(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.HistoryID, Me.HistoryID)
					End If
				End If

				Return Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID
			End Get
			
			Set(ByVal value As ICCollectionValueDateClearingInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID Is Nothing Then

					Me.RemovePostSave("ICCollectionValueDateClearingInstrumentsCollectionByHistoryID")
					Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionValueDateClearingInstrumentsCollectionByHistoryID As ICCollectionValueDateClearingInstrumentsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionValueDateClearingInstrumentsCollectionByHistoryID"
					coll = Me.ICCollectionValueDateClearingInstrumentsCollectionByHistoryID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionValueDateClearingInstrumentsCollectionByHistoryID", GetType(ICCollectionValueDateClearingInstrumentsCollection), New ICCollectionValueDateClearingInstruments()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID Is Nothing Then
				Apply(Me._ICCollectionValueDateClearingInstrumentsCollectionByHistoryID, "HistoryID", Me.HistoryID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionValueDateClearingHistoryMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.HistoryID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingHistoryMetadata.PropertyNames.HistoryID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutionDate, 1, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionValueDateClearingHistoryMetadata.PropertyNames.ExecutionDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.PassCount, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingHistoryMetadata.PropertyNames.PassCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.FailCount, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingHistoryMetadata.PropertyNames.FailCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingHistoryMetadata.ColumnNames.ExecutedBy, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionValueDateClearingHistoryMetadata.PropertyNames.ExecutedBy
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionValueDateClearingHistoryMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const HistoryID As String = "HistoryID"
			 Public Const ExecutionDate As String = "ExecutionDate"
			 Public Const PassCount As String = "PassCount"
			 Public Const FailCount As String = "FailCount"
			 Public Const ExecutedBy As String = "ExecutedBy"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const HistoryID As String = "HistoryID"
			 Public Const ExecutionDate As String = "ExecutionDate"
			 Public Const PassCount As String = "PassCount"
			 Public Const FailCount As String = "FailCount"
			 Public Const ExecutedBy As String = "ExecutedBy"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionValueDateClearingHistoryMetadata)
			
				If ICCollectionValueDateClearingHistoryMetadata.mapDelegates Is Nothing Then
					ICCollectionValueDateClearingHistoryMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionValueDateClearingHistoryMetadata._meta Is Nothing Then
					ICCollectionValueDateClearingHistoryMetadata._meta = New ICCollectionValueDateClearingHistoryMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("HistoryID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ExecutionDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PassCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FailCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ExecutedBy", new esTypeMap("nchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CollectionValueDateClearingHistory"
				meta.Destination = "IC_CollectionValueDateClearingHistory"
				
				meta.spInsert = "proc_IC_CollectionValueDateClearingHistoryInsert"
				meta.spUpdate = "proc_IC_CollectionValueDateClearingHistoryUpdate"
				meta.spDelete = "proc_IC_CollectionValueDateClearingHistoryDelete"
				meta.spLoadAll = "proc_IC_CollectionValueDateClearingHistoryLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionValueDateClearingHistoryLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionValueDateClearingHistoryMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
