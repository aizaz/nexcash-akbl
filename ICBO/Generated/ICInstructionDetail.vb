
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:57 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_InstructionDetail' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstructionDetail))> _
	<XmlType("ICInstructionDetail")> _	
	Partial Public Class ICInstructionDetail 
		Inherits esICInstructionDetail
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstructionDetail()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal instructionDetailID As System.Int32)
			Dim obj As New ICInstructionDetail()
			obj.InstructionDetailID = instructionDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal instructionDetailID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstructionDetail()
			obj.InstructionDetailID = instructionDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstructionDetailCollection")> _
	Partial Public Class ICInstructionDetailCollection
		Inherits esICInstructionDetailCollection
		Implements IEnumerable(Of ICInstructionDetail)
	
		Public Function FindByPrimaryKey(ByVal instructionDetailID As System.Int32) As ICInstructionDetail
			Return MyBase.SingleOrDefault(Function(e) e.InstructionDetailID.HasValue AndAlso e.InstructionDetailID.Value = instructionDetailID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstructionDetail))> _
		Public Class ICInstructionDetailCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstructionDetailCollection)
			
			Public Shared Widening Operator CType(packet As ICInstructionDetailCollectionWCFPacket) As ICInstructionDetailCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstructionDetailCollection) As ICInstructionDetailCollectionWCFPacket
				Return New ICInstructionDetailCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstructionDetailQuery 
		Inherits esICInstructionDetailQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstructionDetailQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstructionDetailQuery) As String
			Return ICInstructionDetailQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstructionDetailQuery
			Return DirectCast(ICInstructionDetailQuery.SerializeHelper.FromXml(query, GetType(ICInstructionDetailQuery)), ICInstructionDetailQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstructionDetail
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal instructionDetailID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionDetailID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal instructionDetailID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionDetailID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal instructionDetailID As System.Int32) As Boolean
		
			Dim query As New ICInstructionDetailQuery()
			query.Where(query.InstructionDetailID = instructionDetailID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal instructionDetailID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("InstructionDetailID", instructionDetailID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_InstructionDetail.InstructionDetailID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionDetailID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionDetailMetadata.ColumnNames.InstructionDetailID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionDetailMetadata.ColumnNames.InstructionDetailID, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.InstructionDetailID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionDetailMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionDetailMetadata.ColumnNames.InstructionID, value) Then
					Me._UpToICInstructionByInstructionID = Nothing
					Me.OnPropertyChanged("UpToICInstructionByInstructionID")
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt1 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt1)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt1, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt2 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt2)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt2, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt3 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt3)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt3, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_4
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt4 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt4)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt4, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt4)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_5
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt5 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt5)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt5, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt5)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_6
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt6 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt6)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt6, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt6)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt1 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt1, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt2 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt2, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt3 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt3)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt3, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_4
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt4 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt4)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt4, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt4)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_5
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt5 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt5)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt5, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt5)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_6
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt6 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt6)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt6, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt6)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_7
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt7 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt7)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt7, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt7)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_8
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt8 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt8)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt8, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt8)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_9
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt9 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt9)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt9, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt9)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_10
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt10 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt10)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt10, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt10)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_11
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt11 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt11)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt11, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt11)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_12
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt12 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt12)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt12, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt12)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_13
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt13 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt13)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt13, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt13)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_14
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt14 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt14)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt14, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt14)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_15
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt15 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt15)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt15, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt15)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_16
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt16 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt16)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt16, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt16)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_17
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt17 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt17)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt17, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt17)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_18
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt18 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt18)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt18, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt18)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_19
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt19 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt19)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt19, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt19)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_20
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt20 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt20)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt20, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt20)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailAmount, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneAccountNo, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneAddress, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneBankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneBankName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBankName, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneBankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneBranchAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneBranchAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchAddress, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchCode, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneBranchName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneBranchName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchName, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneCNIC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneCNIC As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCNIC)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCNIC, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneCNIC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneCountry
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneCountry As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCountry)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCountry, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneCountry)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneProvince
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneProvince As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneProvince)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneProvince, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneProvince)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneCity
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneCity As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCity)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneCity, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneCity)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneEmail
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneEmail As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneEmail)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneEmail, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneEmail)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BeneMobile
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBeneMobile As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneMobile)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBeneMobile, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBeneMobile)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_BenePhone
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailBenePhone As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBenePhone)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailBenePhone, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailBenePhone)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_InvoiceNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailInvoiceNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailInvoiceNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailInvoiceNo, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailInvoiceNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_IS_NO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailISNO As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionDetailMetadata.ColumnNames.DetailISNO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionDetailMetadata.ColumnNames.DetailISNO, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailISNO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_TXN_CODE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailTXNCODE As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionDetailMetadata.ColumnNames.DetailTXNCODE)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionDetailMetadata.ColumnNames.DetailTXNCODE, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailTXNCODE)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_DetailAmt
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailDetailAmt As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailDetailAmt)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailDetailAmt, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailDetailAmt)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText1 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText1, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText2 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText2, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText3 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText3)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText3, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_4
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText4 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText4)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText4, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText4)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_5
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText5 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText5)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText5, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText5)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_6
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText6 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText6)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText6, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText6)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_7
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText7 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText7)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText7, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText7)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_8
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText8 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText8)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText8, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText8)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_9
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText9 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText9)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText9, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText9)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_10
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText10 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText10)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText10, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText10)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_11
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText11 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText11)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText11, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText11)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_12
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText12 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText12)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText12, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText12)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_13
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText13 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText13)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText13, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText13)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_14
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText14 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText14)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText14, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText14)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_15
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText15 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText15)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText15, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText15)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_16
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText16 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText16)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText16, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText16)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_17
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText17 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText17)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText17, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText17)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_18
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText18 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText18)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText18, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText18)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_19
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText19 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText19)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText19, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText19)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_20
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText20 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText20)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText20, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText20)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt1 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt1)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt1, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt2 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt2)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt2, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt3 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt3)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt3, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_4
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt4 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt4)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt4, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt4)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_5
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt5 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt5)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt5, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt5)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_6
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt6 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt6)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt6, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt6)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_7
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt7 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt7)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt7, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt7)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_8
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt8 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt8)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt8, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt8)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_9
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt9 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt9)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt9, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt9)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_10
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt10 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt10)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt10, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt10)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_11
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt11 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt11)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt11, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt11)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_12
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt12 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt12)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt12, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt12)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_13
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt13 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt13)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt13, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt13)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_14
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt14 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt14)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt14, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt14)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_15
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt15 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt15)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt15, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt15)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_16
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt16 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt16)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt16, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt16)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_17
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt17 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt17)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt17, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt17)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_18
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt18 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt18)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt18, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt18)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_19
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt19 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt19)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt19, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt19)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_20
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt20 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt20)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt20, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt20)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_21
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt21 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt21)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt21, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt21)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_22
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt22 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt22)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt22, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt22)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_23
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt23 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt23)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt23, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt23)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_24
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt24 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt24)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt24, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt24)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_25
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt25 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt25)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt25, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt25)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_26
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt26 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt26)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt26, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt26)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_27
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt27 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt27)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt27, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt27)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_28
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt28 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt28)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt28, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt28)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_29
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt29 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt29)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt29, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt29)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_30
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt30 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt30)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt30, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt30)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_31
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt31 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt31)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt31, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt31)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_32
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt32 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt32)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt32, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt32)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_33
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt33 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt33)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt33, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt33)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_34
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt34 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt34)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt34, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt34)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_35
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt35 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt35)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt35, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt35)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_36
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt36 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt36)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt36, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt36)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_37
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt37 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt37)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt37, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt37)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_38
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt38 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt38)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt38, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt38)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_39
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt39 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt39)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt39, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt39)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_40
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt40 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt40)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt40, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt40)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_41
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt41 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt41)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt41, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt41)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_42
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt42 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt42)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt42, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt42)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_43
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt43 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt43)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt43, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt43)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_44
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt44 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt44)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt44, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt44)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_45
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt45 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt45)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt45, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt45)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_46
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt46 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt46)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt46, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt46)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_47
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt47 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt47)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt47, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt47)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_48
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt48 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt48)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt48, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt48)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_49
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt49 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt49)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt49, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt49)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Amt_50
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFAmt50 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt50)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt50, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt50)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_21
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText21 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText21)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText21, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText21)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_22
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText22 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText22)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText22, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText22)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_23
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText23 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText23)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText23, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText23)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_24
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText24 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText24)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText24, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText24)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_25
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText25 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText25)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText25, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText25)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_26
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText26 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText26)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText26, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText26)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_27
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText27 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText27)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText27, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText27)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_28
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText28 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText28)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText28, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText28)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_29
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText29 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText29)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText29, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText29)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_30
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText30 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText30)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText30, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText30)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_31
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText31 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText31)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText31, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText31)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_32
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText32 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText32)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText32, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText32)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_33
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText33 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText33)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText33, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText33)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_34
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText34 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText34)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText34, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText34)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_35
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText35 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText35)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText35, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText35)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_36
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText36 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText36)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText36, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText36)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_37
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText37 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText37)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText37, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText37)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_38
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText38 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText38)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText38, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText38)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_39
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText39 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText39)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText39, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText39)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_40
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText40 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText40)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText40, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText40)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_41
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText41 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText41)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText41, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText41)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_42
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText42 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText42)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText42, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText42)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_43
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText43 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText43)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText43, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText43)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_44
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText44 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText44)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText44, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText44)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_45
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText45 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText45)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText45, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText45)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_46
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText46 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText46)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText46, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText46)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_47
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText47 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText47)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText47, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText47)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_48
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText48 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText48)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText48, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText48)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_49
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText49 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText49)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText49, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText49)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_50
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText50 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText50)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText50, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText50)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_51
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText51 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText51)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText51, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText51)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_52
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText52 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText52)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText52, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText52)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_53
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText53 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText53)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText53, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText53)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_54
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText54 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText54)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText54, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText54)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_55
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText55 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText55)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText55, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText55)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_56
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText56 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText56)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText56, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText56)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_57
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText57 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText57)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText57, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText57)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_58
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText58 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText58)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText58, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText58)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_59
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText59 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText59)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText59, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText59)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_60
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText60 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText60)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText60, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText60)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_61
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText61 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText61)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText61, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText61)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_62
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText62 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText62)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText62, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText62)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_63
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText63 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText63)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText63, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText63)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_64
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText64 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText64)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText64, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText64)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_65
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText65 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText65)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText65, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText65)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_66
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText66 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText66)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText66, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText66)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_67
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText67 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText67)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText67, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText67)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_68
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText68 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText68)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText68, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText68)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_69
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText69 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText69)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText69, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText69)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_70
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText70 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText70)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText70, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText70)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_71
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText71 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText71)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText71, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText71)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_72
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText72 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText72)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText72, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText72)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_73
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText73 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText73)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText73, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText73)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_74
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText74 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText74)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText74, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText74)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_75
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText75 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText75)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText75, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText75)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_76
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText76 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText76)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText76, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText76)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_77
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText77 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText77)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText77, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText77)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_78
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText78 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText78)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText78, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText78)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_79
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText79 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText79)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText79, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText79)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_80
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText80 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText80)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText80, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText80)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_81
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText81 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText81)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText81, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText81)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_82
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText82 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText82)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText82, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText82)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_83
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText83 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText83)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText83, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText83)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_84
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText84 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText84)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText84, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText84)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_85
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText85 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText85)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText85, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText85)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_86
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText86 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText86)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText86, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText86)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_87
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText87 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText87)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText87, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText87)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_88
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText88 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText88)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText88, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText88)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_89
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText89 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText89)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText89, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText89)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_90
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText90 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText90)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText90, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText90)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_91
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText91 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText91)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText91, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText91)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_92
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText92 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText92)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText92, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText92)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_93
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText93 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText93)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText93, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText93)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_94
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText94 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText94)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText94, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText94)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_95
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText95 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText95)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText95, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText95)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_96
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText96 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText96)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText96, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText96)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_97
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText97 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText97)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText97, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText97)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_98
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText98 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText98)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText98, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText98)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_99
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText99 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText99)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText99, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText99)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.Detail_FF_Text_100
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailFFText100 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText100)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.DetailFFText100, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFText100)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_7
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt7 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt7)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt7, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt7)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_8
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt8 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt8)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt8, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt8)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_9
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt9 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt9)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt9, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt9)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_10
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt10 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt10)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt10, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt10)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_11
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt11 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt11)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt11, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt11)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_12
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt12 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt12)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt12, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt12)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_13
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt13 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt13)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt13, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt13)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_14
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt14 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt14)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt14, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt14)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_15
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt15 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt15)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt15, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt15)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_16
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt16 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt16)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt16, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt16)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_17
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt17 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt17)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt17, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt17)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_18
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt18 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt18)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt18, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt18)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_19
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt19 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt19)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt19, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt19)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_20
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt20 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt20)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt20, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt20)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_21
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt21 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt21)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt21, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt21)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_22
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt22 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt22)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt22, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt22)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_23
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt23 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt23)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt23, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt23)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_24
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt24 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt24)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt24, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt24)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_25
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt25 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt25)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt25, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt25)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_26
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt26 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt26)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt26, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt26)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_27
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt27 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt27)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt27, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt27)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_28
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt28 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt28)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt28, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt28)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_29
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt29 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt29)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt29, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt29)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_30
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt30 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt30)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt30, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt30)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_31
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt31 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt31)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt31, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt31)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_32
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt32 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt32)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt32, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt32)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_33
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt33 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt33)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt33, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt33)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_34
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt34 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt34)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt34, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt34)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_35
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt35 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt35)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt35, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt35)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_36
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt36 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt36)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt36, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt36)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_37
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt37 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt37)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt37, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt37)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_38
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt38 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt38)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt38, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt38)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_39
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt39 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt39)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt39, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt39)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_40
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt40 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt40)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt40, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt40)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_41
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt41 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt41)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt41, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt41)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_42
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt42 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt42)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt42, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt42)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_43
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt43 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt43)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt43, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt43)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_44
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt44 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt44)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt44, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt44)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_45
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt45 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt45)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt45, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt45)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_46
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt46 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt46)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt46, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt46)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_47
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt47 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt47)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt47, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt47)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_48
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt48 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt48)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt48, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt48)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_49
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt49 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt49)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt49, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt49)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_Amt_50
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFAmt50 As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt50)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionDetailMetadata.ColumnNames.FFAmt50, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt50)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_21
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt21 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt21)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt21, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt21)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_22
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt22 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt22)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt22, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt22)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_23
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt23 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt23)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt23, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt23)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_24
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt24 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt24)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt24, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt24)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_25
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt25 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt25)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt25, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt25)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_26
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt26 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt26)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt26, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt26)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_27
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt27 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt27)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt27, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt27)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_28
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt28 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt28)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt28, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt28)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_29
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt29 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt29)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt29, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt29)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_30
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt30 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt30)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt30, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt30)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_31
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt31 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt31)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt31, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt31)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_32
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt32 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt32)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt32, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt32)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_33
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt33 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt33)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt33, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt33)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_34
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt34 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt34)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt34, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt34)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_35
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt35 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt35)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt35, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt35)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_36
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt36 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt36)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt36, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt36)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_37
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt37 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt37)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt37, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt37)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_38
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt38 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt38)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt38, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt38)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_39
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt39 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt39)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt39, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt39)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_40
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt40 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt40)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt40, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt40)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_41
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt41 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt41)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt41, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt41)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_42
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt42 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt42)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt42, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt42)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_43
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt43 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt43)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt43, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt43)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_44
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt44 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt44)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt44, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt44)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_45
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt45 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt45)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt45, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt45)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_46
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt46 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt46)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt46, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt46)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_47
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt47 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt47)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt47, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt47)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_48
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt48 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt48)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt48, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt48)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_49
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt49 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt49)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt49, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt49)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_50
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt50 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt50)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt50, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt50)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_51
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt51 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt51)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt51, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt51)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_52
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt52 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt52)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt52, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt52)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_53
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt53 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt53)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt53, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt53)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_54
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt54 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt54)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt54, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt54)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_55
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt55 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt55)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt55, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt55)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_56
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt56 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt56)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt56, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt56)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_57
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt57 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt57)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt57, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt57)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_58
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt58 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt58)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt58, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt58)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_59
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt59 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt59)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt59, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt59)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_60
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt60 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt60)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt60, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt60)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_61
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt61 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt61)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt61, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt61)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_62
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt62 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt62)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt62, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt62)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_63
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt63 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt63)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt63, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt63)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_64
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt64 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt64)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt64, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt64)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_65
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt65 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt65)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt65, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt65)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_66
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt66 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt66)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt66, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt66)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_67
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt67 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt67)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt67, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt67)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_68
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt68 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt68)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt68, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt68)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_69
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt69 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt69)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt69, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt69)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_70
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt70 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt70)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt70, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt70)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_71
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt71 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt71)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt71, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt71)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_72
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt72 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt72)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt72, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt72)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_73
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt73 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt73)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt73, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt73)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_74
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt74 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt74)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt74, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt74)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_75
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt75 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt75)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt75, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt75)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_76
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt76 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt76)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt76, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt76)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_77
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt77 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt77)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt77, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt77)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_78
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt78 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt78)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt78, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt78)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_79
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt79 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt79)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt79, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt79)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_80
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt80 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt80)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt80, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt80)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_81
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt81 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt81)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt81, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt81)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_82
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt82 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt82)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt82, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt82)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_83
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt83 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt83)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt83, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt83)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_84
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt84 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt84)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt84, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt84)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_85
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt85 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt85)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt85, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt85)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_86
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt86 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt86)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt86, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt86)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_87
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt87 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt87)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt87, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt87)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_88
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt88 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt88)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt88, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt88)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_89
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt89 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt89)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt89, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt89)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_90
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt90 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt90)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt90, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt90)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_91
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt91 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt91)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt91, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt91)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_92
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt92 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt92)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt92, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt92)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_93
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt93 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt93)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt93, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt93)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_94
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt94 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt94)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt94, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt94)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_95
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt95 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt95)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt95, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt95)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_96
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt96 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt96)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt96, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt96)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_97
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt97 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt97)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt97, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt97)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_98
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt98 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt98)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt98, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt98)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_99
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt99 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt99)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt99, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt99)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionDetail.FF_txt_100
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FFTxt100 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt100)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionDetailMetadata.ColumnNames.FFTxt100, value) Then
					OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFTxt100)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionByInstructionID As ICInstruction
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "InstructionDetailID"
							Me.str().InstructionDetailID = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "FFAmt1"
							Me.str().FFAmt1 = CType(value, string)
												
						Case "FFAmt2"
							Me.str().FFAmt2 = CType(value, string)
												
						Case "FFAmt3"
							Me.str().FFAmt3 = CType(value, string)
												
						Case "FFAmt4"
							Me.str().FFAmt4 = CType(value, string)
												
						Case "FFAmt5"
							Me.str().FFAmt5 = CType(value, string)
												
						Case "FFAmt6"
							Me.str().FFAmt6 = CType(value, string)
												
						Case "FFTxt1"
							Me.str().FFTxt1 = CType(value, string)
												
						Case "FFTxt2"
							Me.str().FFTxt2 = CType(value, string)
												
						Case "FFTxt3"
							Me.str().FFTxt3 = CType(value, string)
												
						Case "FFTxt4"
							Me.str().FFTxt4 = CType(value, string)
												
						Case "FFTxt5"
							Me.str().FFTxt5 = CType(value, string)
												
						Case "FFTxt6"
							Me.str().FFTxt6 = CType(value, string)
												
						Case "FFTxt7"
							Me.str().FFTxt7 = CType(value, string)
												
						Case "FFTxt8"
							Me.str().FFTxt8 = CType(value, string)
												
						Case "FFTxt9"
							Me.str().FFTxt9 = CType(value, string)
												
						Case "FFTxt10"
							Me.str().FFTxt10 = CType(value, string)
												
						Case "FFTxt11"
							Me.str().FFTxt11 = CType(value, string)
												
						Case "FFTxt12"
							Me.str().FFTxt12 = CType(value, string)
												
						Case "FFTxt13"
							Me.str().FFTxt13 = CType(value, string)
												
						Case "FFTxt14"
							Me.str().FFTxt14 = CType(value, string)
												
						Case "FFTxt15"
							Me.str().FFTxt15 = CType(value, string)
												
						Case "FFTxt16"
							Me.str().FFTxt16 = CType(value, string)
												
						Case "FFTxt17"
							Me.str().FFTxt17 = CType(value, string)
												
						Case "FFTxt18"
							Me.str().FFTxt18 = CType(value, string)
												
						Case "FFTxt19"
							Me.str().FFTxt19 = CType(value, string)
												
						Case "FFTxt20"
							Me.str().FFTxt20 = CType(value, string)
												
						Case "DetailAmount"
							Me.str().DetailAmount = CType(value, string)
												
						Case "DetailBeneAccountNo"
							Me.str().DetailBeneAccountNo = CType(value, string)
												
						Case "DetailBeneAddress"
							Me.str().DetailBeneAddress = CType(value, string)
												
						Case "DetailBeneBankName"
							Me.str().DetailBeneBankName = CType(value, string)
												
						Case "DetailBeneBranchAddress"
							Me.str().DetailBeneBranchAddress = CType(value, string)
												
						Case "DetailBeneBranchCode"
							Me.str().DetailBeneBranchCode = CType(value, string)
												
						Case "DetailBeneBranchName"
							Me.str().DetailBeneBranchName = CType(value, string)
												
						Case "DetailBeneCNIC"
							Me.str().DetailBeneCNIC = CType(value, string)
												
						Case "DetailBeneCountry"
							Me.str().DetailBeneCountry = CType(value, string)
												
						Case "DetailBeneProvince"
							Me.str().DetailBeneProvince = CType(value, string)
												
						Case "DetailBeneCity"
							Me.str().DetailBeneCity = CType(value, string)
												
						Case "DetailBeneEmail"
							Me.str().DetailBeneEmail = CType(value, string)
												
						Case "DetailBeneMobile"
							Me.str().DetailBeneMobile = CType(value, string)
												
						Case "DetailBenePhone"
							Me.str().DetailBenePhone = CType(value, string)
												
						Case "DetailInvoiceNo"
							Me.str().DetailInvoiceNo = CType(value, string)
												
						Case "DetailISNO"
							Me.str().DetailISNO = CType(value, string)
												
						Case "DetailTXNCODE"
							Me.str().DetailTXNCODE = CType(value, string)
												
						Case "DetailDetailAmt"
							Me.str().DetailDetailAmt = CType(value, string)
												
						Case "DetailFFText1"
							Me.str().DetailFFText1 = CType(value, string)
												
						Case "DetailFFText2"
							Me.str().DetailFFText2 = CType(value, string)
												
						Case "DetailFFText3"
							Me.str().DetailFFText3 = CType(value, string)
												
						Case "DetailFFText4"
							Me.str().DetailFFText4 = CType(value, string)
												
						Case "DetailFFText5"
							Me.str().DetailFFText5 = CType(value, string)
												
						Case "DetailFFText6"
							Me.str().DetailFFText6 = CType(value, string)
												
						Case "DetailFFText7"
							Me.str().DetailFFText7 = CType(value, string)
												
						Case "DetailFFText8"
							Me.str().DetailFFText8 = CType(value, string)
												
						Case "DetailFFText9"
							Me.str().DetailFFText9 = CType(value, string)
												
						Case "DetailFFText10"
							Me.str().DetailFFText10 = CType(value, string)
												
						Case "DetailFFText11"
							Me.str().DetailFFText11 = CType(value, string)
												
						Case "DetailFFText12"
							Me.str().DetailFFText12 = CType(value, string)
												
						Case "DetailFFText13"
							Me.str().DetailFFText13 = CType(value, string)
												
						Case "DetailFFText14"
							Me.str().DetailFFText14 = CType(value, string)
												
						Case "DetailFFText15"
							Me.str().DetailFFText15 = CType(value, string)
												
						Case "DetailFFText16"
							Me.str().DetailFFText16 = CType(value, string)
												
						Case "DetailFFText17"
							Me.str().DetailFFText17 = CType(value, string)
												
						Case "DetailFFText18"
							Me.str().DetailFFText18 = CType(value, string)
												
						Case "DetailFFText19"
							Me.str().DetailFFText19 = CType(value, string)
												
						Case "DetailFFText20"
							Me.str().DetailFFText20 = CType(value, string)
												
						Case "DetailFFAmt1"
							Me.str().DetailFFAmt1 = CType(value, string)
												
						Case "DetailFFAmt2"
							Me.str().DetailFFAmt2 = CType(value, string)
												
						Case "DetailFFAmt3"
							Me.str().DetailFFAmt3 = CType(value, string)
												
						Case "DetailFFAmt4"
							Me.str().DetailFFAmt4 = CType(value, string)
												
						Case "DetailFFAmt5"
							Me.str().DetailFFAmt5 = CType(value, string)
												
						Case "DetailFFAmt6"
							Me.str().DetailFFAmt6 = CType(value, string)
												
						Case "DetailFFAmt7"
							Me.str().DetailFFAmt7 = CType(value, string)
												
						Case "DetailFFAmt8"
							Me.str().DetailFFAmt8 = CType(value, string)
												
						Case "DetailFFAmt9"
							Me.str().DetailFFAmt9 = CType(value, string)
												
						Case "DetailFFAmt10"
							Me.str().DetailFFAmt10 = CType(value, string)
												
						Case "DetailFFAmt11"
							Me.str().DetailFFAmt11 = CType(value, string)
												
						Case "DetailFFAmt12"
							Me.str().DetailFFAmt12 = CType(value, string)
												
						Case "DetailFFAmt13"
							Me.str().DetailFFAmt13 = CType(value, string)
												
						Case "DetailFFAmt14"
							Me.str().DetailFFAmt14 = CType(value, string)
												
						Case "DetailFFAmt15"
							Me.str().DetailFFAmt15 = CType(value, string)
												
						Case "DetailFFAmt16"
							Me.str().DetailFFAmt16 = CType(value, string)
												
						Case "DetailFFAmt17"
							Me.str().DetailFFAmt17 = CType(value, string)
												
						Case "DetailFFAmt18"
							Me.str().DetailFFAmt18 = CType(value, string)
												
						Case "DetailFFAmt19"
							Me.str().DetailFFAmt19 = CType(value, string)
												
						Case "DetailFFAmt20"
							Me.str().DetailFFAmt20 = CType(value, string)
												
						Case "DetailFFAmt21"
							Me.str().DetailFFAmt21 = CType(value, string)
												
						Case "DetailFFAmt22"
							Me.str().DetailFFAmt22 = CType(value, string)
												
						Case "DetailFFAmt23"
							Me.str().DetailFFAmt23 = CType(value, string)
												
						Case "DetailFFAmt24"
							Me.str().DetailFFAmt24 = CType(value, string)
												
						Case "DetailFFAmt25"
							Me.str().DetailFFAmt25 = CType(value, string)
												
						Case "DetailFFAmt26"
							Me.str().DetailFFAmt26 = CType(value, string)
												
						Case "DetailFFAmt27"
							Me.str().DetailFFAmt27 = CType(value, string)
												
						Case "DetailFFAmt28"
							Me.str().DetailFFAmt28 = CType(value, string)
												
						Case "DetailFFAmt29"
							Me.str().DetailFFAmt29 = CType(value, string)
												
						Case "DetailFFAmt30"
							Me.str().DetailFFAmt30 = CType(value, string)
												
						Case "DetailFFAmt31"
							Me.str().DetailFFAmt31 = CType(value, string)
												
						Case "DetailFFAmt32"
							Me.str().DetailFFAmt32 = CType(value, string)
												
						Case "DetailFFAmt33"
							Me.str().DetailFFAmt33 = CType(value, string)
												
						Case "DetailFFAmt34"
							Me.str().DetailFFAmt34 = CType(value, string)
												
						Case "DetailFFAmt35"
							Me.str().DetailFFAmt35 = CType(value, string)
												
						Case "DetailFFAmt36"
							Me.str().DetailFFAmt36 = CType(value, string)
												
						Case "DetailFFAmt37"
							Me.str().DetailFFAmt37 = CType(value, string)
												
						Case "DetailFFAmt38"
							Me.str().DetailFFAmt38 = CType(value, string)
												
						Case "DetailFFAmt39"
							Me.str().DetailFFAmt39 = CType(value, string)
												
						Case "DetailFFAmt40"
							Me.str().DetailFFAmt40 = CType(value, string)
												
						Case "DetailFFAmt41"
							Me.str().DetailFFAmt41 = CType(value, string)
												
						Case "DetailFFAmt42"
							Me.str().DetailFFAmt42 = CType(value, string)
												
						Case "DetailFFAmt43"
							Me.str().DetailFFAmt43 = CType(value, string)
												
						Case "DetailFFAmt44"
							Me.str().DetailFFAmt44 = CType(value, string)
												
						Case "DetailFFAmt45"
							Me.str().DetailFFAmt45 = CType(value, string)
												
						Case "DetailFFAmt46"
							Me.str().DetailFFAmt46 = CType(value, string)
												
						Case "DetailFFAmt47"
							Me.str().DetailFFAmt47 = CType(value, string)
												
						Case "DetailFFAmt48"
							Me.str().DetailFFAmt48 = CType(value, string)
												
						Case "DetailFFAmt49"
							Me.str().DetailFFAmt49 = CType(value, string)
												
						Case "DetailFFAmt50"
							Me.str().DetailFFAmt50 = CType(value, string)
												
						Case "DetailFFText21"
							Me.str().DetailFFText21 = CType(value, string)
												
						Case "DetailFFText22"
							Me.str().DetailFFText22 = CType(value, string)
												
						Case "DetailFFText23"
							Me.str().DetailFFText23 = CType(value, string)
												
						Case "DetailFFText24"
							Me.str().DetailFFText24 = CType(value, string)
												
						Case "DetailFFText25"
							Me.str().DetailFFText25 = CType(value, string)
												
						Case "DetailFFText26"
							Me.str().DetailFFText26 = CType(value, string)
												
						Case "DetailFFText27"
							Me.str().DetailFFText27 = CType(value, string)
												
						Case "DetailFFText28"
							Me.str().DetailFFText28 = CType(value, string)
												
						Case "DetailFFText29"
							Me.str().DetailFFText29 = CType(value, string)
												
						Case "DetailFFText30"
							Me.str().DetailFFText30 = CType(value, string)
												
						Case "DetailFFText31"
							Me.str().DetailFFText31 = CType(value, string)
												
						Case "DetailFFText32"
							Me.str().DetailFFText32 = CType(value, string)
												
						Case "DetailFFText33"
							Me.str().DetailFFText33 = CType(value, string)
												
						Case "DetailFFText34"
							Me.str().DetailFFText34 = CType(value, string)
												
						Case "DetailFFText35"
							Me.str().DetailFFText35 = CType(value, string)
												
						Case "DetailFFText36"
							Me.str().DetailFFText36 = CType(value, string)
												
						Case "DetailFFText37"
							Me.str().DetailFFText37 = CType(value, string)
												
						Case "DetailFFText38"
							Me.str().DetailFFText38 = CType(value, string)
												
						Case "DetailFFText39"
							Me.str().DetailFFText39 = CType(value, string)
												
						Case "DetailFFText40"
							Me.str().DetailFFText40 = CType(value, string)
												
						Case "DetailFFText41"
							Me.str().DetailFFText41 = CType(value, string)
												
						Case "DetailFFText42"
							Me.str().DetailFFText42 = CType(value, string)
												
						Case "DetailFFText43"
							Me.str().DetailFFText43 = CType(value, string)
												
						Case "DetailFFText44"
							Me.str().DetailFFText44 = CType(value, string)
												
						Case "DetailFFText45"
							Me.str().DetailFFText45 = CType(value, string)
												
						Case "DetailFFText46"
							Me.str().DetailFFText46 = CType(value, string)
												
						Case "DetailFFText47"
							Me.str().DetailFFText47 = CType(value, string)
												
						Case "DetailFFText48"
							Me.str().DetailFFText48 = CType(value, string)
												
						Case "DetailFFText49"
							Me.str().DetailFFText49 = CType(value, string)
												
						Case "DetailFFText50"
							Me.str().DetailFFText50 = CType(value, string)
												
						Case "DetailFFText51"
							Me.str().DetailFFText51 = CType(value, string)
												
						Case "DetailFFText52"
							Me.str().DetailFFText52 = CType(value, string)
												
						Case "DetailFFText53"
							Me.str().DetailFFText53 = CType(value, string)
												
						Case "DetailFFText54"
							Me.str().DetailFFText54 = CType(value, string)
												
						Case "DetailFFText55"
							Me.str().DetailFFText55 = CType(value, string)
												
						Case "DetailFFText56"
							Me.str().DetailFFText56 = CType(value, string)
												
						Case "DetailFFText57"
							Me.str().DetailFFText57 = CType(value, string)
												
						Case "DetailFFText58"
							Me.str().DetailFFText58 = CType(value, string)
												
						Case "DetailFFText59"
							Me.str().DetailFFText59 = CType(value, string)
												
						Case "DetailFFText60"
							Me.str().DetailFFText60 = CType(value, string)
												
						Case "DetailFFText61"
							Me.str().DetailFFText61 = CType(value, string)
												
						Case "DetailFFText62"
							Me.str().DetailFFText62 = CType(value, string)
												
						Case "DetailFFText63"
							Me.str().DetailFFText63 = CType(value, string)
												
						Case "DetailFFText64"
							Me.str().DetailFFText64 = CType(value, string)
												
						Case "DetailFFText65"
							Me.str().DetailFFText65 = CType(value, string)
												
						Case "DetailFFText66"
							Me.str().DetailFFText66 = CType(value, string)
												
						Case "DetailFFText67"
							Me.str().DetailFFText67 = CType(value, string)
												
						Case "DetailFFText68"
							Me.str().DetailFFText68 = CType(value, string)
												
						Case "DetailFFText69"
							Me.str().DetailFFText69 = CType(value, string)
												
						Case "DetailFFText70"
							Me.str().DetailFFText70 = CType(value, string)
												
						Case "DetailFFText71"
							Me.str().DetailFFText71 = CType(value, string)
												
						Case "DetailFFText72"
							Me.str().DetailFFText72 = CType(value, string)
												
						Case "DetailFFText73"
							Me.str().DetailFFText73 = CType(value, string)
												
						Case "DetailFFText74"
							Me.str().DetailFFText74 = CType(value, string)
												
						Case "DetailFFText75"
							Me.str().DetailFFText75 = CType(value, string)
												
						Case "DetailFFText76"
							Me.str().DetailFFText76 = CType(value, string)
												
						Case "DetailFFText77"
							Me.str().DetailFFText77 = CType(value, string)
												
						Case "DetailFFText78"
							Me.str().DetailFFText78 = CType(value, string)
												
						Case "DetailFFText79"
							Me.str().DetailFFText79 = CType(value, string)
												
						Case "DetailFFText80"
							Me.str().DetailFFText80 = CType(value, string)
												
						Case "DetailFFText81"
							Me.str().DetailFFText81 = CType(value, string)
												
						Case "DetailFFText82"
							Me.str().DetailFFText82 = CType(value, string)
												
						Case "DetailFFText83"
							Me.str().DetailFFText83 = CType(value, string)
												
						Case "DetailFFText84"
							Me.str().DetailFFText84 = CType(value, string)
												
						Case "DetailFFText85"
							Me.str().DetailFFText85 = CType(value, string)
												
						Case "DetailFFText86"
							Me.str().DetailFFText86 = CType(value, string)
												
						Case "DetailFFText87"
							Me.str().DetailFFText87 = CType(value, string)
												
						Case "DetailFFText88"
							Me.str().DetailFFText88 = CType(value, string)
												
						Case "DetailFFText89"
							Me.str().DetailFFText89 = CType(value, string)
												
						Case "DetailFFText90"
							Me.str().DetailFFText90 = CType(value, string)
												
						Case "DetailFFText91"
							Me.str().DetailFFText91 = CType(value, string)
												
						Case "DetailFFText92"
							Me.str().DetailFFText92 = CType(value, string)
												
						Case "DetailFFText93"
							Me.str().DetailFFText93 = CType(value, string)
												
						Case "DetailFFText94"
							Me.str().DetailFFText94 = CType(value, string)
												
						Case "DetailFFText95"
							Me.str().DetailFFText95 = CType(value, string)
												
						Case "DetailFFText96"
							Me.str().DetailFFText96 = CType(value, string)
												
						Case "DetailFFText97"
							Me.str().DetailFFText97 = CType(value, string)
												
						Case "DetailFFText98"
							Me.str().DetailFFText98 = CType(value, string)
												
						Case "DetailFFText99"
							Me.str().DetailFFText99 = CType(value, string)
												
						Case "DetailFFText100"
							Me.str().DetailFFText100 = CType(value, string)
												
						Case "FFAmt7"
							Me.str().FFAmt7 = CType(value, string)
												
						Case "FFAmt8"
							Me.str().FFAmt8 = CType(value, string)
												
						Case "FFAmt9"
							Me.str().FFAmt9 = CType(value, string)
												
						Case "FFAmt10"
							Me.str().FFAmt10 = CType(value, string)
												
						Case "FFAmt11"
							Me.str().FFAmt11 = CType(value, string)
												
						Case "FFAmt12"
							Me.str().FFAmt12 = CType(value, string)
												
						Case "FFAmt13"
							Me.str().FFAmt13 = CType(value, string)
												
						Case "FFAmt14"
							Me.str().FFAmt14 = CType(value, string)
												
						Case "FFAmt15"
							Me.str().FFAmt15 = CType(value, string)
												
						Case "FFAmt16"
							Me.str().FFAmt16 = CType(value, string)
												
						Case "FFAmt17"
							Me.str().FFAmt17 = CType(value, string)
												
						Case "FFAmt18"
							Me.str().FFAmt18 = CType(value, string)
												
						Case "FFAmt19"
							Me.str().FFAmt19 = CType(value, string)
												
						Case "FFAmt20"
							Me.str().FFAmt20 = CType(value, string)
												
						Case "FFAmt21"
							Me.str().FFAmt21 = CType(value, string)
												
						Case "FFAmt22"
							Me.str().FFAmt22 = CType(value, string)
												
						Case "FFAmt23"
							Me.str().FFAmt23 = CType(value, string)
												
						Case "FFAmt24"
							Me.str().FFAmt24 = CType(value, string)
												
						Case "FFAmt25"
							Me.str().FFAmt25 = CType(value, string)
												
						Case "FFAmt26"
							Me.str().FFAmt26 = CType(value, string)
												
						Case "FFAmt27"
							Me.str().FFAmt27 = CType(value, string)
												
						Case "FFAmt28"
							Me.str().FFAmt28 = CType(value, string)
												
						Case "FFAmt29"
							Me.str().FFAmt29 = CType(value, string)
												
						Case "FFAmt30"
							Me.str().FFAmt30 = CType(value, string)
												
						Case "FFAmt31"
							Me.str().FFAmt31 = CType(value, string)
												
						Case "FFAmt32"
							Me.str().FFAmt32 = CType(value, string)
												
						Case "FFAmt33"
							Me.str().FFAmt33 = CType(value, string)
												
						Case "FFAmt34"
							Me.str().FFAmt34 = CType(value, string)
												
						Case "FFAmt35"
							Me.str().FFAmt35 = CType(value, string)
												
						Case "FFAmt36"
							Me.str().FFAmt36 = CType(value, string)
												
						Case "FFAmt37"
							Me.str().FFAmt37 = CType(value, string)
												
						Case "FFAmt38"
							Me.str().FFAmt38 = CType(value, string)
												
						Case "FFAmt39"
							Me.str().FFAmt39 = CType(value, string)
												
						Case "FFAmt40"
							Me.str().FFAmt40 = CType(value, string)
												
						Case "FFAmt41"
							Me.str().FFAmt41 = CType(value, string)
												
						Case "FFAmt42"
							Me.str().FFAmt42 = CType(value, string)
												
						Case "FFAmt43"
							Me.str().FFAmt43 = CType(value, string)
												
						Case "FFAmt44"
							Me.str().FFAmt44 = CType(value, string)
												
						Case "FFAmt45"
							Me.str().FFAmt45 = CType(value, string)
												
						Case "FFAmt46"
							Me.str().FFAmt46 = CType(value, string)
												
						Case "FFAmt47"
							Me.str().FFAmt47 = CType(value, string)
												
						Case "FFAmt48"
							Me.str().FFAmt48 = CType(value, string)
												
						Case "FFAmt49"
							Me.str().FFAmt49 = CType(value, string)
												
						Case "FFAmt50"
							Me.str().FFAmt50 = CType(value, string)
												
						Case "FFTxt21"
							Me.str().FFTxt21 = CType(value, string)
												
						Case "FFTxt22"
							Me.str().FFTxt22 = CType(value, string)
												
						Case "FFTxt23"
							Me.str().FFTxt23 = CType(value, string)
												
						Case "FFTxt24"
							Me.str().FFTxt24 = CType(value, string)
												
						Case "FFTxt25"
							Me.str().FFTxt25 = CType(value, string)
												
						Case "FFTxt26"
							Me.str().FFTxt26 = CType(value, string)
												
						Case "FFTxt27"
							Me.str().FFTxt27 = CType(value, string)
												
						Case "FFTxt28"
							Me.str().FFTxt28 = CType(value, string)
												
						Case "FFTxt29"
							Me.str().FFTxt29 = CType(value, string)
												
						Case "FFTxt30"
							Me.str().FFTxt30 = CType(value, string)
												
						Case "FFTxt31"
							Me.str().FFTxt31 = CType(value, string)
												
						Case "FFTxt32"
							Me.str().FFTxt32 = CType(value, string)
												
						Case "FFTxt33"
							Me.str().FFTxt33 = CType(value, string)
												
						Case "FFTxt34"
							Me.str().FFTxt34 = CType(value, string)
												
						Case "FFTxt35"
							Me.str().FFTxt35 = CType(value, string)
												
						Case "FFTxt36"
							Me.str().FFTxt36 = CType(value, string)
												
						Case "FFTxt37"
							Me.str().FFTxt37 = CType(value, string)
												
						Case "FFTxt38"
							Me.str().FFTxt38 = CType(value, string)
												
						Case "FFTxt39"
							Me.str().FFTxt39 = CType(value, string)
												
						Case "FFTxt40"
							Me.str().FFTxt40 = CType(value, string)
												
						Case "FFTxt41"
							Me.str().FFTxt41 = CType(value, string)
												
						Case "FFTxt42"
							Me.str().FFTxt42 = CType(value, string)
												
						Case "FFTxt43"
							Me.str().FFTxt43 = CType(value, string)
												
						Case "FFTxt44"
							Me.str().FFTxt44 = CType(value, string)
												
						Case "FFTxt45"
							Me.str().FFTxt45 = CType(value, string)
												
						Case "FFTxt46"
							Me.str().FFTxt46 = CType(value, string)
												
						Case "FFTxt47"
							Me.str().FFTxt47 = CType(value, string)
												
						Case "FFTxt48"
							Me.str().FFTxt48 = CType(value, string)
												
						Case "FFTxt49"
							Me.str().FFTxt49 = CType(value, string)
												
						Case "FFTxt50"
							Me.str().FFTxt50 = CType(value, string)
												
						Case "FFTxt51"
							Me.str().FFTxt51 = CType(value, string)
												
						Case "FFTxt52"
							Me.str().FFTxt52 = CType(value, string)
												
						Case "FFTxt53"
							Me.str().FFTxt53 = CType(value, string)
												
						Case "FFTxt54"
							Me.str().FFTxt54 = CType(value, string)
												
						Case "FFTxt55"
							Me.str().FFTxt55 = CType(value, string)
												
						Case "FFTxt56"
							Me.str().FFTxt56 = CType(value, string)
												
						Case "FFTxt57"
							Me.str().FFTxt57 = CType(value, string)
												
						Case "FFTxt58"
							Me.str().FFTxt58 = CType(value, string)
												
						Case "FFTxt59"
							Me.str().FFTxt59 = CType(value, string)
												
						Case "FFTxt60"
							Me.str().FFTxt60 = CType(value, string)
												
						Case "FFTxt61"
							Me.str().FFTxt61 = CType(value, string)
												
						Case "FFTxt62"
							Me.str().FFTxt62 = CType(value, string)
												
						Case "FFTxt63"
							Me.str().FFTxt63 = CType(value, string)
												
						Case "FFTxt64"
							Me.str().FFTxt64 = CType(value, string)
												
						Case "FFTxt65"
							Me.str().FFTxt65 = CType(value, string)
												
						Case "FFTxt66"
							Me.str().FFTxt66 = CType(value, string)
												
						Case "FFTxt67"
							Me.str().FFTxt67 = CType(value, string)
												
						Case "FFTxt68"
							Me.str().FFTxt68 = CType(value, string)
												
						Case "FFTxt69"
							Me.str().FFTxt69 = CType(value, string)
												
						Case "FFTxt70"
							Me.str().FFTxt70 = CType(value, string)
												
						Case "FFTxt71"
							Me.str().FFTxt71 = CType(value, string)
												
						Case "FFTxt72"
							Me.str().FFTxt72 = CType(value, string)
												
						Case "FFTxt73"
							Me.str().FFTxt73 = CType(value, string)
												
						Case "FFTxt74"
							Me.str().FFTxt74 = CType(value, string)
												
						Case "FFTxt75"
							Me.str().FFTxt75 = CType(value, string)
												
						Case "FFTxt76"
							Me.str().FFTxt76 = CType(value, string)
												
						Case "FFTxt77"
							Me.str().FFTxt77 = CType(value, string)
												
						Case "FFTxt78"
							Me.str().FFTxt78 = CType(value, string)
												
						Case "FFTxt79"
							Me.str().FFTxt79 = CType(value, string)
												
						Case "FFTxt80"
							Me.str().FFTxt80 = CType(value, string)
												
						Case "FFTxt81"
							Me.str().FFTxt81 = CType(value, string)
												
						Case "FFTxt82"
							Me.str().FFTxt82 = CType(value, string)
												
						Case "FFTxt83"
							Me.str().FFTxt83 = CType(value, string)
												
						Case "FFTxt84"
							Me.str().FFTxt84 = CType(value, string)
												
						Case "FFTxt85"
							Me.str().FFTxt85 = CType(value, string)
												
						Case "FFTxt86"
							Me.str().FFTxt86 = CType(value, string)
												
						Case "FFTxt87"
							Me.str().FFTxt87 = CType(value, string)
												
						Case "FFTxt88"
							Me.str().FFTxt88 = CType(value, string)
												
						Case "FFTxt89"
							Me.str().FFTxt89 = CType(value, string)
												
						Case "FFTxt90"
							Me.str().FFTxt90 = CType(value, string)
												
						Case "FFTxt91"
							Me.str().FFTxt91 = CType(value, string)
												
						Case "FFTxt92"
							Me.str().FFTxt92 = CType(value, string)
												
						Case "FFTxt93"
							Me.str().FFTxt93 = CType(value, string)
												
						Case "FFTxt94"
							Me.str().FFTxt94 = CType(value, string)
												
						Case "FFTxt95"
							Me.str().FFTxt95 = CType(value, string)
												
						Case "FFTxt96"
							Me.str().FFTxt96 = CType(value, string)
												
						Case "FFTxt97"
							Me.str().FFTxt97 = CType(value, string)
												
						Case "FFTxt98"
							Me.str().FFTxt98 = CType(value, string)
												
						Case "FFTxt99"
							Me.str().FFTxt99 = CType(value, string)
												
						Case "FFTxt100"
							Me.str().FFTxt100 = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "InstructionDetailID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionDetailID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.InstructionDetailID)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.InstructionID)
							End If
						
						Case "FFAmt1"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt1 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt1)
							End If
						
						Case "FFAmt2"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt2 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt2)
							End If
						
						Case "FFAmt3"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt3 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt3)
							End If
						
						Case "FFAmt4"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt4 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt4)
							End If
						
						Case "FFAmt5"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt5 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt5)
							End If
						
						Case "FFAmt6"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt6 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt6)
							End If
						
						Case "DetailAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailAmount)
							End If
						
						Case "DetailISNO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DetailISNO = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailISNO)
							End If
						
						Case "DetailTXNCODE"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DetailTXNCODE = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailTXNCODE)
							End If
						
						Case "DetailDetailAmt"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailDetailAmt = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailDetailAmt)
							End If
						
						Case "DetailFFAmt1"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt1 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt1)
							End If
						
						Case "DetailFFAmt2"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt2 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt2)
							End If
						
						Case "DetailFFAmt3"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt3 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt3)
							End If
						
						Case "DetailFFAmt4"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt4 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt4)
							End If
						
						Case "DetailFFAmt5"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt5 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt5)
							End If
						
						Case "DetailFFAmt6"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt6 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt6)
							End If
						
						Case "DetailFFAmt7"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt7 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt7)
							End If
						
						Case "DetailFFAmt8"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt8 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt8)
							End If
						
						Case "DetailFFAmt9"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt9 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt9)
							End If
						
						Case "DetailFFAmt10"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt10 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt10)
							End If
						
						Case "DetailFFAmt11"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt11 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt11)
							End If
						
						Case "DetailFFAmt12"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt12 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt12)
							End If
						
						Case "DetailFFAmt13"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt13 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt13)
							End If
						
						Case "DetailFFAmt14"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt14 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt14)
							End If
						
						Case "DetailFFAmt15"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt15 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt15)
							End If
						
						Case "DetailFFAmt16"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt16 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt16)
							End If
						
						Case "DetailFFAmt17"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt17 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt17)
							End If
						
						Case "DetailFFAmt18"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt18 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt18)
							End If
						
						Case "DetailFFAmt19"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt19 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt19)
							End If
						
						Case "DetailFFAmt20"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt20 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt20)
							End If
						
						Case "DetailFFAmt21"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt21 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt21)
							End If
						
						Case "DetailFFAmt22"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt22 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt22)
							End If
						
						Case "DetailFFAmt23"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt23 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt23)
							End If
						
						Case "DetailFFAmt24"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt24 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt24)
							End If
						
						Case "DetailFFAmt25"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt25 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt25)
							End If
						
						Case "DetailFFAmt26"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt26 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt26)
							End If
						
						Case "DetailFFAmt27"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt27 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt27)
							End If
						
						Case "DetailFFAmt28"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt28 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt28)
							End If
						
						Case "DetailFFAmt29"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt29 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt29)
							End If
						
						Case "DetailFFAmt30"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt30 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt30)
							End If
						
						Case "DetailFFAmt31"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt31 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt31)
							End If
						
						Case "DetailFFAmt32"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt32 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt32)
							End If
						
						Case "DetailFFAmt33"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt33 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt33)
							End If
						
						Case "DetailFFAmt34"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt34 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt34)
							End If
						
						Case "DetailFFAmt35"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt35 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt35)
							End If
						
						Case "DetailFFAmt36"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt36 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt36)
							End If
						
						Case "DetailFFAmt37"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt37 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt37)
							End If
						
						Case "DetailFFAmt38"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt38 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt38)
							End If
						
						Case "DetailFFAmt39"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt39 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt39)
							End If
						
						Case "DetailFFAmt40"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt40 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt40)
							End If
						
						Case "DetailFFAmt41"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt41 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt41)
							End If
						
						Case "DetailFFAmt42"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt42 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt42)
							End If
						
						Case "DetailFFAmt43"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt43 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt43)
							End If
						
						Case "DetailFFAmt44"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt44 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt44)
							End If
						
						Case "DetailFFAmt45"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt45 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt45)
							End If
						
						Case "DetailFFAmt46"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt46 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt46)
							End If
						
						Case "DetailFFAmt47"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt47 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt47)
							End If
						
						Case "DetailFFAmt48"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt48 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt48)
							End If
						
						Case "DetailFFAmt49"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt49 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt49)
							End If
						
						Case "DetailFFAmt50"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailFFAmt50 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.DetailFFAmt50)
							End If
						
						Case "FFAmt7"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt7 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt7)
							End If
						
						Case "FFAmt8"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt8 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt8)
							End If
						
						Case "FFAmt9"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt9 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt9)
							End If
						
						Case "FFAmt10"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt10 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt10)
							End If
						
						Case "FFAmt11"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt11 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt11)
							End If
						
						Case "FFAmt12"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt12 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt12)
							End If
						
						Case "FFAmt13"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt13 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt13)
							End If
						
						Case "FFAmt14"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt14 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt14)
							End If
						
						Case "FFAmt15"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt15 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt15)
							End If
						
						Case "FFAmt16"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt16 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt16)
							End If
						
						Case "FFAmt17"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt17 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt17)
							End If
						
						Case "FFAmt18"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt18 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt18)
							End If
						
						Case "FFAmt19"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt19 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt19)
							End If
						
						Case "FFAmt20"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt20 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt20)
							End If
						
						Case "FFAmt21"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt21 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt21)
							End If
						
						Case "FFAmt22"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt22 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt22)
							End If
						
						Case "FFAmt23"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt23 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt23)
							End If
						
						Case "FFAmt24"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt24 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt24)
							End If
						
						Case "FFAmt25"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt25 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt25)
							End If
						
						Case "FFAmt26"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt26 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt26)
							End If
						
						Case "FFAmt27"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt27 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt27)
							End If
						
						Case "FFAmt28"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt28 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt28)
							End If
						
						Case "FFAmt29"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt29 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt29)
							End If
						
						Case "FFAmt30"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt30 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt30)
							End If
						
						Case "FFAmt31"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt31 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt31)
							End If
						
						Case "FFAmt32"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt32 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt32)
							End If
						
						Case "FFAmt33"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt33 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt33)
							End If
						
						Case "FFAmt34"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt34 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt34)
							End If
						
						Case "FFAmt35"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt35 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt35)
							End If
						
						Case "FFAmt36"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt36 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt36)
							End If
						
						Case "FFAmt37"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt37 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt37)
							End If
						
						Case "FFAmt38"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt38 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt38)
							End If
						
						Case "FFAmt39"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt39 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt39)
							End If
						
						Case "FFAmt40"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt40 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt40)
							End If
						
						Case "FFAmt41"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt41 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt41)
							End If
						
						Case "FFAmt42"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt42 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt42)
							End If
						
						Case "FFAmt43"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt43 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt43)
							End If
						
						Case "FFAmt44"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt44 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt44)
							End If
						
						Case "FFAmt45"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt45 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt45)
							End If
						
						Case "FFAmt46"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt46 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt46)
							End If
						
						Case "FFAmt47"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt47 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt47)
							End If
						
						Case "FFAmt48"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt48 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt48)
							End If
						
						Case "FFAmt49"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt49 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt49)
							End If
						
						Case "FFAmt50"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FFAmt50 = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionDetailMetadata.PropertyNames.FFAmt50)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstructionDetail)
				Me.entity = entity
			End Sub				
		
	
			Public Property InstructionDetailID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionDetailID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionDetailID = Nothing
					Else
						entity.InstructionDetailID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt1 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt1
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt1 = Nothing
					Else
						entity.FFAmt1 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt2 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt2
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt2 = Nothing
					Else
						entity.FFAmt2 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt3 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt3
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt3 = Nothing
					Else
						entity.FFAmt3 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt4 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt4
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt4 = Nothing
					Else
						entity.FFAmt4 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt5 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt5
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt5 = Nothing
					Else
						entity.FFAmt5 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt6 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt6
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt6 = Nothing
					Else
						entity.FFAmt6 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt1 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt1 = Nothing
					Else
						entity.FFTxt1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt2 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt2 = Nothing
					Else
						entity.FFTxt2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt3 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt3
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt3 = Nothing
					Else
						entity.FFTxt3 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt4 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt4
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt4 = Nothing
					Else
						entity.FFTxt4 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt5 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt5
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt5 = Nothing
					Else
						entity.FFTxt5 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt6 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt6
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt6 = Nothing
					Else
						entity.FFTxt6 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt7 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt7
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt7 = Nothing
					Else
						entity.FFTxt7 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt8 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt8
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt8 = Nothing
					Else
						entity.FFTxt8 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt9 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt9
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt9 = Nothing
					Else
						entity.FFTxt9 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt10 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt10
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt10 = Nothing
					Else
						entity.FFTxt10 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt11 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt11
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt11 = Nothing
					Else
						entity.FFTxt11 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt12 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt12
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt12 = Nothing
					Else
						entity.FFTxt12 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt13 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt13
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt13 = Nothing
					Else
						entity.FFTxt13 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt14 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt14
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt14 = Nothing
					Else
						entity.FFTxt14 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt15 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt15
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt15 = Nothing
					Else
						entity.FFTxt15 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt16 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt16
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt16 = Nothing
					Else
						entity.FFTxt16 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt17 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt17
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt17 = Nothing
					Else
						entity.FFTxt17 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt18 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt18
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt18 = Nothing
					Else
						entity.FFTxt18 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt19 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt19
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt19 = Nothing
					Else
						entity.FFTxt19 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt20 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt20
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt20 = Nothing
					Else
						entity.FFTxt20 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailAmount = Nothing
					Else
						entity.DetailAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneAccountNo = Nothing
					Else
						entity.DetailBeneAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneAddress As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneAddress = Nothing
					Else
						entity.DetailBeneAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneBankName As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneBankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneBankName = Nothing
					Else
						entity.DetailBeneBankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneBranchAddress As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneBranchAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneBranchAddress = Nothing
					Else
						entity.DetailBeneBranchAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneBranchCode = Nothing
					Else
						entity.DetailBeneBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneBranchName As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneBranchName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneBranchName = Nothing
					Else
						entity.DetailBeneBranchName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneCNIC As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneCNIC
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneCNIC = Nothing
					Else
						entity.DetailBeneCNIC = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneCountry As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneCountry
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneCountry = Nothing
					Else
						entity.DetailBeneCountry = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneProvince As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneProvince
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneProvince = Nothing
					Else
						entity.DetailBeneProvince = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneCity As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneCity
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneCity = Nothing
					Else
						entity.DetailBeneCity = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneEmail As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneEmail
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneEmail = Nothing
					Else
						entity.DetailBeneEmail = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBeneMobile As System.String 
				Get
					Dim data_ As System.String = entity.DetailBeneMobile
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBeneMobile = Nothing
					Else
						entity.DetailBeneMobile = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailBenePhone As System.String 
				Get
					Dim data_ As System.String = entity.DetailBenePhone
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailBenePhone = Nothing
					Else
						entity.DetailBenePhone = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailInvoiceNo As System.String 
				Get
					Dim data_ As System.String = entity.DetailInvoiceNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailInvoiceNo = Nothing
					Else
						entity.DetailInvoiceNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailISNO As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DetailISNO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailISNO = Nothing
					Else
						entity.DetailISNO = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailTXNCODE As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DetailTXNCODE
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailTXNCODE = Nothing
					Else
						entity.DetailTXNCODE = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailDetailAmt As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailDetailAmt
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailDetailAmt = Nothing
					Else
						entity.DetailDetailAmt = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText1 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText1 = Nothing
					Else
						entity.DetailFFText1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText2 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText2 = Nothing
					Else
						entity.DetailFFText2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText3 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText3
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText3 = Nothing
					Else
						entity.DetailFFText3 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText4 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText4
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText4 = Nothing
					Else
						entity.DetailFFText4 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText5 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText5
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText5 = Nothing
					Else
						entity.DetailFFText5 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText6 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText6
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText6 = Nothing
					Else
						entity.DetailFFText6 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText7 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText7
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText7 = Nothing
					Else
						entity.DetailFFText7 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText8 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText8
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText8 = Nothing
					Else
						entity.DetailFFText8 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText9 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText9
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText9 = Nothing
					Else
						entity.DetailFFText9 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText10 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText10
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText10 = Nothing
					Else
						entity.DetailFFText10 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText11 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText11
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText11 = Nothing
					Else
						entity.DetailFFText11 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText12 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText12
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText12 = Nothing
					Else
						entity.DetailFFText12 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText13 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText13
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText13 = Nothing
					Else
						entity.DetailFFText13 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText14 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText14
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText14 = Nothing
					Else
						entity.DetailFFText14 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText15 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText15
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText15 = Nothing
					Else
						entity.DetailFFText15 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText16 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText16
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText16 = Nothing
					Else
						entity.DetailFFText16 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText17 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText17
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText17 = Nothing
					Else
						entity.DetailFFText17 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText18 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText18
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText18 = Nothing
					Else
						entity.DetailFFText18 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText19 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText19
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText19 = Nothing
					Else
						entity.DetailFFText19 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText20 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText20
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText20 = Nothing
					Else
						entity.DetailFFText20 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt1 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt1
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt1 = Nothing
					Else
						entity.DetailFFAmt1 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt2 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt2
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt2 = Nothing
					Else
						entity.DetailFFAmt2 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt3 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt3
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt3 = Nothing
					Else
						entity.DetailFFAmt3 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt4 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt4
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt4 = Nothing
					Else
						entity.DetailFFAmt4 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt5 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt5
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt5 = Nothing
					Else
						entity.DetailFFAmt5 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt6 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt6
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt6 = Nothing
					Else
						entity.DetailFFAmt6 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt7 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt7
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt7 = Nothing
					Else
						entity.DetailFFAmt7 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt8 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt8
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt8 = Nothing
					Else
						entity.DetailFFAmt8 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt9 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt9
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt9 = Nothing
					Else
						entity.DetailFFAmt9 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt10 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt10
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt10 = Nothing
					Else
						entity.DetailFFAmt10 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt11 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt11
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt11 = Nothing
					Else
						entity.DetailFFAmt11 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt12 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt12
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt12 = Nothing
					Else
						entity.DetailFFAmt12 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt13 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt13
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt13 = Nothing
					Else
						entity.DetailFFAmt13 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt14 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt14
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt14 = Nothing
					Else
						entity.DetailFFAmt14 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt15 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt15
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt15 = Nothing
					Else
						entity.DetailFFAmt15 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt16 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt16
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt16 = Nothing
					Else
						entity.DetailFFAmt16 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt17 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt17
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt17 = Nothing
					Else
						entity.DetailFFAmt17 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt18 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt18
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt18 = Nothing
					Else
						entity.DetailFFAmt18 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt19 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt19
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt19 = Nothing
					Else
						entity.DetailFFAmt19 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt20 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt20
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt20 = Nothing
					Else
						entity.DetailFFAmt20 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt21 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt21
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt21 = Nothing
					Else
						entity.DetailFFAmt21 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt22 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt22
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt22 = Nothing
					Else
						entity.DetailFFAmt22 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt23 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt23
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt23 = Nothing
					Else
						entity.DetailFFAmt23 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt24 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt24
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt24 = Nothing
					Else
						entity.DetailFFAmt24 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt25 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt25
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt25 = Nothing
					Else
						entity.DetailFFAmt25 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt26 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt26
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt26 = Nothing
					Else
						entity.DetailFFAmt26 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt27 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt27
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt27 = Nothing
					Else
						entity.DetailFFAmt27 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt28 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt28
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt28 = Nothing
					Else
						entity.DetailFFAmt28 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt29 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt29
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt29 = Nothing
					Else
						entity.DetailFFAmt29 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt30 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt30
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt30 = Nothing
					Else
						entity.DetailFFAmt30 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt31 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt31
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt31 = Nothing
					Else
						entity.DetailFFAmt31 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt32 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt32
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt32 = Nothing
					Else
						entity.DetailFFAmt32 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt33 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt33
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt33 = Nothing
					Else
						entity.DetailFFAmt33 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt34 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt34
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt34 = Nothing
					Else
						entity.DetailFFAmt34 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt35 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt35
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt35 = Nothing
					Else
						entity.DetailFFAmt35 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt36 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt36
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt36 = Nothing
					Else
						entity.DetailFFAmt36 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt37 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt37
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt37 = Nothing
					Else
						entity.DetailFFAmt37 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt38 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt38
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt38 = Nothing
					Else
						entity.DetailFFAmt38 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt39 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt39
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt39 = Nothing
					Else
						entity.DetailFFAmt39 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt40 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt40
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt40 = Nothing
					Else
						entity.DetailFFAmt40 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt41 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt41
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt41 = Nothing
					Else
						entity.DetailFFAmt41 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt42 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt42
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt42 = Nothing
					Else
						entity.DetailFFAmt42 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt43 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt43
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt43 = Nothing
					Else
						entity.DetailFFAmt43 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt44 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt44
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt44 = Nothing
					Else
						entity.DetailFFAmt44 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt45 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt45
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt45 = Nothing
					Else
						entity.DetailFFAmt45 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt46 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt46
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt46 = Nothing
					Else
						entity.DetailFFAmt46 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt47 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt47
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt47 = Nothing
					Else
						entity.DetailFFAmt47 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt48 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt48
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt48 = Nothing
					Else
						entity.DetailFFAmt48 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt49 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt49
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt49 = Nothing
					Else
						entity.DetailFFAmt49 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFAmt50 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailFFAmt50
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFAmt50 = Nothing
					Else
						entity.DetailFFAmt50 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText21 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText21
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText21 = Nothing
					Else
						entity.DetailFFText21 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText22 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText22
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText22 = Nothing
					Else
						entity.DetailFFText22 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText23 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText23
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText23 = Nothing
					Else
						entity.DetailFFText23 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText24 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText24
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText24 = Nothing
					Else
						entity.DetailFFText24 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText25 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText25
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText25 = Nothing
					Else
						entity.DetailFFText25 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText26 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText26
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText26 = Nothing
					Else
						entity.DetailFFText26 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText27 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText27
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText27 = Nothing
					Else
						entity.DetailFFText27 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText28 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText28
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText28 = Nothing
					Else
						entity.DetailFFText28 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText29 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText29
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText29 = Nothing
					Else
						entity.DetailFFText29 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText30 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText30
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText30 = Nothing
					Else
						entity.DetailFFText30 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText31 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText31
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText31 = Nothing
					Else
						entity.DetailFFText31 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText32 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText32
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText32 = Nothing
					Else
						entity.DetailFFText32 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText33 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText33
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText33 = Nothing
					Else
						entity.DetailFFText33 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText34 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText34
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText34 = Nothing
					Else
						entity.DetailFFText34 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText35 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText35
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText35 = Nothing
					Else
						entity.DetailFFText35 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText36 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText36
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText36 = Nothing
					Else
						entity.DetailFFText36 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText37 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText37
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText37 = Nothing
					Else
						entity.DetailFFText37 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText38 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText38
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText38 = Nothing
					Else
						entity.DetailFFText38 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText39 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText39
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText39 = Nothing
					Else
						entity.DetailFFText39 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText40 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText40
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText40 = Nothing
					Else
						entity.DetailFFText40 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText41 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText41
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText41 = Nothing
					Else
						entity.DetailFFText41 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText42 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText42
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText42 = Nothing
					Else
						entity.DetailFFText42 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText43 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText43
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText43 = Nothing
					Else
						entity.DetailFFText43 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText44 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText44
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText44 = Nothing
					Else
						entity.DetailFFText44 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText45 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText45
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText45 = Nothing
					Else
						entity.DetailFFText45 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText46 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText46
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText46 = Nothing
					Else
						entity.DetailFFText46 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText47 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText47
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText47 = Nothing
					Else
						entity.DetailFFText47 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText48 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText48
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText48 = Nothing
					Else
						entity.DetailFFText48 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText49 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText49
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText49 = Nothing
					Else
						entity.DetailFFText49 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText50 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText50
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText50 = Nothing
					Else
						entity.DetailFFText50 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText51 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText51
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText51 = Nothing
					Else
						entity.DetailFFText51 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText52 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText52
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText52 = Nothing
					Else
						entity.DetailFFText52 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText53 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText53
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText53 = Nothing
					Else
						entity.DetailFFText53 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText54 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText54
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText54 = Nothing
					Else
						entity.DetailFFText54 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText55 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText55
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText55 = Nothing
					Else
						entity.DetailFFText55 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText56 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText56
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText56 = Nothing
					Else
						entity.DetailFFText56 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText57 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText57
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText57 = Nothing
					Else
						entity.DetailFFText57 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText58 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText58
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText58 = Nothing
					Else
						entity.DetailFFText58 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText59 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText59
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText59 = Nothing
					Else
						entity.DetailFFText59 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText60 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText60
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText60 = Nothing
					Else
						entity.DetailFFText60 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText61 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText61
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText61 = Nothing
					Else
						entity.DetailFFText61 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText62 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText62
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText62 = Nothing
					Else
						entity.DetailFFText62 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText63 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText63
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText63 = Nothing
					Else
						entity.DetailFFText63 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText64 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText64
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText64 = Nothing
					Else
						entity.DetailFFText64 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText65 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText65
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText65 = Nothing
					Else
						entity.DetailFFText65 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText66 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText66
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText66 = Nothing
					Else
						entity.DetailFFText66 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText67 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText67
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText67 = Nothing
					Else
						entity.DetailFFText67 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText68 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText68
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText68 = Nothing
					Else
						entity.DetailFFText68 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText69 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText69
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText69 = Nothing
					Else
						entity.DetailFFText69 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText70 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText70
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText70 = Nothing
					Else
						entity.DetailFFText70 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText71 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText71
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText71 = Nothing
					Else
						entity.DetailFFText71 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText72 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText72
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText72 = Nothing
					Else
						entity.DetailFFText72 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText73 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText73
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText73 = Nothing
					Else
						entity.DetailFFText73 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText74 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText74
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText74 = Nothing
					Else
						entity.DetailFFText74 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText75 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText75
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText75 = Nothing
					Else
						entity.DetailFFText75 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText76 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText76
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText76 = Nothing
					Else
						entity.DetailFFText76 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText77 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText77
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText77 = Nothing
					Else
						entity.DetailFFText77 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText78 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText78
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText78 = Nothing
					Else
						entity.DetailFFText78 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText79 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText79
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText79 = Nothing
					Else
						entity.DetailFFText79 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText80 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText80
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText80 = Nothing
					Else
						entity.DetailFFText80 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText81 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText81
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText81 = Nothing
					Else
						entity.DetailFFText81 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText82 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText82
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText82 = Nothing
					Else
						entity.DetailFFText82 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText83 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText83
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText83 = Nothing
					Else
						entity.DetailFFText83 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText84 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText84
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText84 = Nothing
					Else
						entity.DetailFFText84 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText85 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText85
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText85 = Nothing
					Else
						entity.DetailFFText85 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText86 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText86
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText86 = Nothing
					Else
						entity.DetailFFText86 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText87 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText87
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText87 = Nothing
					Else
						entity.DetailFFText87 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText88 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText88
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText88 = Nothing
					Else
						entity.DetailFFText88 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText89 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText89
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText89 = Nothing
					Else
						entity.DetailFFText89 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText90 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText90
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText90 = Nothing
					Else
						entity.DetailFFText90 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText91 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText91
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText91 = Nothing
					Else
						entity.DetailFFText91 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText92 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText92
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText92 = Nothing
					Else
						entity.DetailFFText92 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText93 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText93
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText93 = Nothing
					Else
						entity.DetailFFText93 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText94 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText94
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText94 = Nothing
					Else
						entity.DetailFFText94 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText95 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText95
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText95 = Nothing
					Else
						entity.DetailFFText95 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText96 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText96
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText96 = Nothing
					Else
						entity.DetailFFText96 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText97 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText97
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText97 = Nothing
					Else
						entity.DetailFFText97 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText98 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText98
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText98 = Nothing
					Else
						entity.DetailFFText98 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText99 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText99
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText99 = Nothing
					Else
						entity.DetailFFText99 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailFFText100 As System.String 
				Get
					Dim data_ As System.String = entity.DetailFFText100
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailFFText100 = Nothing
					Else
						entity.DetailFFText100 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt7 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt7
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt7 = Nothing
					Else
						entity.FFAmt7 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt8 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt8
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt8 = Nothing
					Else
						entity.FFAmt8 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt9 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt9
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt9 = Nothing
					Else
						entity.FFAmt9 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt10 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt10
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt10 = Nothing
					Else
						entity.FFAmt10 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt11 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt11
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt11 = Nothing
					Else
						entity.FFAmt11 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt12 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt12
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt12 = Nothing
					Else
						entity.FFAmt12 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt13 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt13
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt13 = Nothing
					Else
						entity.FFAmt13 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt14 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt14
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt14 = Nothing
					Else
						entity.FFAmt14 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt15 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt15
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt15 = Nothing
					Else
						entity.FFAmt15 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt16 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt16
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt16 = Nothing
					Else
						entity.FFAmt16 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt17 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt17
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt17 = Nothing
					Else
						entity.FFAmt17 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt18 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt18
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt18 = Nothing
					Else
						entity.FFAmt18 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt19 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt19
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt19 = Nothing
					Else
						entity.FFAmt19 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt20 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt20
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt20 = Nothing
					Else
						entity.FFAmt20 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt21 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt21
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt21 = Nothing
					Else
						entity.FFAmt21 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt22 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt22
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt22 = Nothing
					Else
						entity.FFAmt22 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt23 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt23
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt23 = Nothing
					Else
						entity.FFAmt23 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt24 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt24
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt24 = Nothing
					Else
						entity.FFAmt24 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt25 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt25
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt25 = Nothing
					Else
						entity.FFAmt25 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt26 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt26
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt26 = Nothing
					Else
						entity.FFAmt26 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt27 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt27
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt27 = Nothing
					Else
						entity.FFAmt27 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt28 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt28
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt28 = Nothing
					Else
						entity.FFAmt28 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt29 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt29
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt29 = Nothing
					Else
						entity.FFAmt29 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt30 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt30
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt30 = Nothing
					Else
						entity.FFAmt30 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt31 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt31
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt31 = Nothing
					Else
						entity.FFAmt31 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt32 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt32
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt32 = Nothing
					Else
						entity.FFAmt32 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt33 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt33
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt33 = Nothing
					Else
						entity.FFAmt33 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt34 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt34
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt34 = Nothing
					Else
						entity.FFAmt34 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt35 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt35
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt35 = Nothing
					Else
						entity.FFAmt35 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt36 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt36
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt36 = Nothing
					Else
						entity.FFAmt36 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt37 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt37
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt37 = Nothing
					Else
						entity.FFAmt37 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt38 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt38
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt38 = Nothing
					Else
						entity.FFAmt38 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt39 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt39
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt39 = Nothing
					Else
						entity.FFAmt39 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt40 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt40
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt40 = Nothing
					Else
						entity.FFAmt40 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt41 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt41
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt41 = Nothing
					Else
						entity.FFAmt41 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt42 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt42
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt42 = Nothing
					Else
						entity.FFAmt42 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt43 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt43
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt43 = Nothing
					Else
						entity.FFAmt43 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt44 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt44
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt44 = Nothing
					Else
						entity.FFAmt44 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt45 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt45
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt45 = Nothing
					Else
						entity.FFAmt45 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt46 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt46
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt46 = Nothing
					Else
						entity.FFAmt46 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt47 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt47
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt47 = Nothing
					Else
						entity.FFAmt47 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt48 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt48
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt48 = Nothing
					Else
						entity.FFAmt48 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt49 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt49
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt49 = Nothing
					Else
						entity.FFAmt49 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFAmt50 As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FFAmt50
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFAmt50 = Nothing
					Else
						entity.FFAmt50 = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt21 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt21
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt21 = Nothing
					Else
						entity.FFTxt21 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt22 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt22
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt22 = Nothing
					Else
						entity.FFTxt22 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt23 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt23
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt23 = Nothing
					Else
						entity.FFTxt23 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt24 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt24
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt24 = Nothing
					Else
						entity.FFTxt24 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt25 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt25
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt25 = Nothing
					Else
						entity.FFTxt25 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt26 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt26
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt26 = Nothing
					Else
						entity.FFTxt26 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt27 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt27
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt27 = Nothing
					Else
						entity.FFTxt27 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt28 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt28
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt28 = Nothing
					Else
						entity.FFTxt28 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt29 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt29
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt29 = Nothing
					Else
						entity.FFTxt29 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt30 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt30
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt30 = Nothing
					Else
						entity.FFTxt30 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt31 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt31
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt31 = Nothing
					Else
						entity.FFTxt31 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt32 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt32
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt32 = Nothing
					Else
						entity.FFTxt32 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt33 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt33
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt33 = Nothing
					Else
						entity.FFTxt33 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt34 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt34
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt34 = Nothing
					Else
						entity.FFTxt34 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt35 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt35
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt35 = Nothing
					Else
						entity.FFTxt35 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt36 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt36
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt36 = Nothing
					Else
						entity.FFTxt36 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt37 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt37
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt37 = Nothing
					Else
						entity.FFTxt37 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt38 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt38
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt38 = Nothing
					Else
						entity.FFTxt38 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt39 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt39
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt39 = Nothing
					Else
						entity.FFTxt39 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt40 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt40
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt40 = Nothing
					Else
						entity.FFTxt40 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt41 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt41
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt41 = Nothing
					Else
						entity.FFTxt41 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt42 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt42
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt42 = Nothing
					Else
						entity.FFTxt42 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt43 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt43
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt43 = Nothing
					Else
						entity.FFTxt43 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt44 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt44
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt44 = Nothing
					Else
						entity.FFTxt44 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt45 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt45
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt45 = Nothing
					Else
						entity.FFTxt45 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt46 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt46
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt46 = Nothing
					Else
						entity.FFTxt46 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt47 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt47
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt47 = Nothing
					Else
						entity.FFTxt47 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt48 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt48
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt48 = Nothing
					Else
						entity.FFTxt48 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt49 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt49
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt49 = Nothing
					Else
						entity.FFTxt49 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt50 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt50
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt50 = Nothing
					Else
						entity.FFTxt50 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt51 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt51
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt51 = Nothing
					Else
						entity.FFTxt51 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt52 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt52
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt52 = Nothing
					Else
						entity.FFTxt52 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt53 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt53
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt53 = Nothing
					Else
						entity.FFTxt53 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt54 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt54
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt54 = Nothing
					Else
						entity.FFTxt54 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt55 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt55
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt55 = Nothing
					Else
						entity.FFTxt55 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt56 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt56
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt56 = Nothing
					Else
						entity.FFTxt56 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt57 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt57
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt57 = Nothing
					Else
						entity.FFTxt57 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt58 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt58
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt58 = Nothing
					Else
						entity.FFTxt58 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt59 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt59
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt59 = Nothing
					Else
						entity.FFTxt59 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt60 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt60
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt60 = Nothing
					Else
						entity.FFTxt60 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt61 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt61
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt61 = Nothing
					Else
						entity.FFTxt61 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt62 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt62
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt62 = Nothing
					Else
						entity.FFTxt62 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt63 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt63
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt63 = Nothing
					Else
						entity.FFTxt63 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt64 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt64
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt64 = Nothing
					Else
						entity.FFTxt64 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt65 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt65
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt65 = Nothing
					Else
						entity.FFTxt65 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt66 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt66
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt66 = Nothing
					Else
						entity.FFTxt66 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt67 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt67
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt67 = Nothing
					Else
						entity.FFTxt67 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt68 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt68
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt68 = Nothing
					Else
						entity.FFTxt68 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt69 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt69
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt69 = Nothing
					Else
						entity.FFTxt69 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt70 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt70
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt70 = Nothing
					Else
						entity.FFTxt70 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt71 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt71
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt71 = Nothing
					Else
						entity.FFTxt71 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt72 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt72
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt72 = Nothing
					Else
						entity.FFTxt72 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt73 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt73
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt73 = Nothing
					Else
						entity.FFTxt73 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt74 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt74
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt74 = Nothing
					Else
						entity.FFTxt74 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt75 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt75
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt75 = Nothing
					Else
						entity.FFTxt75 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt76 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt76
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt76 = Nothing
					Else
						entity.FFTxt76 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt77 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt77
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt77 = Nothing
					Else
						entity.FFTxt77 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt78 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt78
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt78 = Nothing
					Else
						entity.FFTxt78 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt79 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt79
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt79 = Nothing
					Else
						entity.FFTxt79 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt80 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt80
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt80 = Nothing
					Else
						entity.FFTxt80 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt81 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt81
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt81 = Nothing
					Else
						entity.FFTxt81 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt82 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt82
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt82 = Nothing
					Else
						entity.FFTxt82 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt83 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt83
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt83 = Nothing
					Else
						entity.FFTxt83 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt84 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt84
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt84 = Nothing
					Else
						entity.FFTxt84 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt85 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt85
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt85 = Nothing
					Else
						entity.FFTxt85 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt86 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt86
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt86 = Nothing
					Else
						entity.FFTxt86 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt87 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt87
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt87 = Nothing
					Else
						entity.FFTxt87 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt88 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt88
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt88 = Nothing
					Else
						entity.FFTxt88 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt89 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt89
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt89 = Nothing
					Else
						entity.FFTxt89 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt90 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt90
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt90 = Nothing
					Else
						entity.FFTxt90 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt91 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt91
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt91 = Nothing
					Else
						entity.FFTxt91 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt92 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt92
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt92 = Nothing
					Else
						entity.FFTxt92 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt93 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt93
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt93 = Nothing
					Else
						entity.FFTxt93 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt94 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt94
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt94 = Nothing
					Else
						entity.FFTxt94 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt95 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt95
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt95 = Nothing
					Else
						entity.FFTxt95 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt96 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt96
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt96 = Nothing
					Else
						entity.FFTxt96 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt97 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt97
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt97 = Nothing
					Else
						entity.FFTxt97 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt98 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt98
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt98 = Nothing
					Else
						entity.FFTxt98 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt99 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt99
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt99 = Nothing
					Else
						entity.FFTxt99 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FFTxt100 As System.String 
				Get
					Dim data_ As System.String = entity.FFTxt100
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FFTxt100 = Nothing
					Else
						entity.FFTxt100 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstructionDetail
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionDetailMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstructionDetailQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionDetailQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstructionDetailQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstructionDetailQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstructionDetailQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstructionDetailCollection
		Inherits esEntityCollection(Of ICInstructionDetail)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionDetailMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstructionDetailCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstructionDetailQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionDetailQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstructionDetailQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstructionDetailQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstructionDetailQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstructionDetailQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstructionDetailQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstructionDetailQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstructionDetailMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "InstructionDetailID" 
					Return Me.InstructionDetailID
				Case "InstructionID" 
					Return Me.InstructionID
				Case "FFAmt1" 
					Return Me.FFAmt1
				Case "FFAmt2" 
					Return Me.FFAmt2
				Case "FFAmt3" 
					Return Me.FFAmt3
				Case "FFAmt4" 
					Return Me.FFAmt4
				Case "FFAmt5" 
					Return Me.FFAmt5
				Case "FFAmt6" 
					Return Me.FFAmt6
				Case "FFTxt1" 
					Return Me.FFTxt1
				Case "FFTxt2" 
					Return Me.FFTxt2
				Case "FFTxt3" 
					Return Me.FFTxt3
				Case "FFTxt4" 
					Return Me.FFTxt4
				Case "FFTxt5" 
					Return Me.FFTxt5
				Case "FFTxt6" 
					Return Me.FFTxt6
				Case "FFTxt7" 
					Return Me.FFTxt7
				Case "FFTxt8" 
					Return Me.FFTxt8
				Case "FFTxt9" 
					Return Me.FFTxt9
				Case "FFTxt10" 
					Return Me.FFTxt10
				Case "FFTxt11" 
					Return Me.FFTxt11
				Case "FFTxt12" 
					Return Me.FFTxt12
				Case "FFTxt13" 
					Return Me.FFTxt13
				Case "FFTxt14" 
					Return Me.FFTxt14
				Case "FFTxt15" 
					Return Me.FFTxt15
				Case "FFTxt16" 
					Return Me.FFTxt16
				Case "FFTxt17" 
					Return Me.FFTxt17
				Case "FFTxt18" 
					Return Me.FFTxt18
				Case "FFTxt19" 
					Return Me.FFTxt19
				Case "FFTxt20" 
					Return Me.FFTxt20
				Case "DetailAmount" 
					Return Me.DetailAmount
				Case "DetailBeneAccountNo" 
					Return Me.DetailBeneAccountNo
				Case "DetailBeneAddress" 
					Return Me.DetailBeneAddress
				Case "DetailBeneBankName" 
					Return Me.DetailBeneBankName
				Case "DetailBeneBranchAddress" 
					Return Me.DetailBeneBranchAddress
				Case "DetailBeneBranchCode" 
					Return Me.DetailBeneBranchCode
				Case "DetailBeneBranchName" 
					Return Me.DetailBeneBranchName
				Case "DetailBeneCNIC" 
					Return Me.DetailBeneCNIC
				Case "DetailBeneCountry" 
					Return Me.DetailBeneCountry
				Case "DetailBeneProvince" 
					Return Me.DetailBeneProvince
				Case "DetailBeneCity" 
					Return Me.DetailBeneCity
				Case "DetailBeneEmail" 
					Return Me.DetailBeneEmail
				Case "DetailBeneMobile" 
					Return Me.DetailBeneMobile
				Case "DetailBenePhone" 
					Return Me.DetailBenePhone
				Case "DetailInvoiceNo" 
					Return Me.DetailInvoiceNo
				Case "DetailISNO" 
					Return Me.DetailISNO
				Case "DetailTXNCODE" 
					Return Me.DetailTXNCODE
				Case "DetailDetailAmt" 
					Return Me.DetailDetailAmt
				Case "DetailFFText1" 
					Return Me.DetailFFText1
				Case "DetailFFText2" 
					Return Me.DetailFFText2
				Case "DetailFFText3" 
					Return Me.DetailFFText3
				Case "DetailFFText4" 
					Return Me.DetailFFText4
				Case "DetailFFText5" 
					Return Me.DetailFFText5
				Case "DetailFFText6" 
					Return Me.DetailFFText6
				Case "DetailFFText7" 
					Return Me.DetailFFText7
				Case "DetailFFText8" 
					Return Me.DetailFFText8
				Case "DetailFFText9" 
					Return Me.DetailFFText9
				Case "DetailFFText10" 
					Return Me.DetailFFText10
				Case "DetailFFText11" 
					Return Me.DetailFFText11
				Case "DetailFFText12" 
					Return Me.DetailFFText12
				Case "DetailFFText13" 
					Return Me.DetailFFText13
				Case "DetailFFText14" 
					Return Me.DetailFFText14
				Case "DetailFFText15" 
					Return Me.DetailFFText15
				Case "DetailFFText16" 
					Return Me.DetailFFText16
				Case "DetailFFText17" 
					Return Me.DetailFFText17
				Case "DetailFFText18" 
					Return Me.DetailFFText18
				Case "DetailFFText19" 
					Return Me.DetailFFText19
				Case "DetailFFText20" 
					Return Me.DetailFFText20
				Case "DetailFFAmt1" 
					Return Me.DetailFFAmt1
				Case "DetailFFAmt2" 
					Return Me.DetailFFAmt2
				Case "DetailFFAmt3" 
					Return Me.DetailFFAmt3
				Case "DetailFFAmt4" 
					Return Me.DetailFFAmt4
				Case "DetailFFAmt5" 
					Return Me.DetailFFAmt5
				Case "DetailFFAmt6" 
					Return Me.DetailFFAmt6
				Case "DetailFFAmt7" 
					Return Me.DetailFFAmt7
				Case "DetailFFAmt8" 
					Return Me.DetailFFAmt8
				Case "DetailFFAmt9" 
					Return Me.DetailFFAmt9
				Case "DetailFFAmt10" 
					Return Me.DetailFFAmt10
				Case "DetailFFAmt11" 
					Return Me.DetailFFAmt11
				Case "DetailFFAmt12" 
					Return Me.DetailFFAmt12
				Case "DetailFFAmt13" 
					Return Me.DetailFFAmt13
				Case "DetailFFAmt14" 
					Return Me.DetailFFAmt14
				Case "DetailFFAmt15" 
					Return Me.DetailFFAmt15
				Case "DetailFFAmt16" 
					Return Me.DetailFFAmt16
				Case "DetailFFAmt17" 
					Return Me.DetailFFAmt17
				Case "DetailFFAmt18" 
					Return Me.DetailFFAmt18
				Case "DetailFFAmt19" 
					Return Me.DetailFFAmt19
				Case "DetailFFAmt20" 
					Return Me.DetailFFAmt20
				Case "DetailFFAmt21" 
					Return Me.DetailFFAmt21
				Case "DetailFFAmt22" 
					Return Me.DetailFFAmt22
				Case "DetailFFAmt23" 
					Return Me.DetailFFAmt23
				Case "DetailFFAmt24" 
					Return Me.DetailFFAmt24
				Case "DetailFFAmt25" 
					Return Me.DetailFFAmt25
				Case "DetailFFAmt26" 
					Return Me.DetailFFAmt26
				Case "DetailFFAmt27" 
					Return Me.DetailFFAmt27
				Case "DetailFFAmt28" 
					Return Me.DetailFFAmt28
				Case "DetailFFAmt29" 
					Return Me.DetailFFAmt29
				Case "DetailFFAmt30" 
					Return Me.DetailFFAmt30
				Case "DetailFFAmt31" 
					Return Me.DetailFFAmt31
				Case "DetailFFAmt32" 
					Return Me.DetailFFAmt32
				Case "DetailFFAmt33" 
					Return Me.DetailFFAmt33
				Case "DetailFFAmt34" 
					Return Me.DetailFFAmt34
				Case "DetailFFAmt35" 
					Return Me.DetailFFAmt35
				Case "DetailFFAmt36" 
					Return Me.DetailFFAmt36
				Case "DetailFFAmt37" 
					Return Me.DetailFFAmt37
				Case "DetailFFAmt38" 
					Return Me.DetailFFAmt38
				Case "DetailFFAmt39" 
					Return Me.DetailFFAmt39
				Case "DetailFFAmt40" 
					Return Me.DetailFFAmt40
				Case "DetailFFAmt41" 
					Return Me.DetailFFAmt41
				Case "DetailFFAmt42" 
					Return Me.DetailFFAmt42
				Case "DetailFFAmt43" 
					Return Me.DetailFFAmt43
				Case "DetailFFAmt44" 
					Return Me.DetailFFAmt44
				Case "DetailFFAmt45" 
					Return Me.DetailFFAmt45
				Case "DetailFFAmt46" 
					Return Me.DetailFFAmt46
				Case "DetailFFAmt47" 
					Return Me.DetailFFAmt47
				Case "DetailFFAmt48" 
					Return Me.DetailFFAmt48
				Case "DetailFFAmt49" 
					Return Me.DetailFFAmt49
				Case "DetailFFAmt50" 
					Return Me.DetailFFAmt50
				Case "DetailFFText21" 
					Return Me.DetailFFText21
				Case "DetailFFText22" 
					Return Me.DetailFFText22
				Case "DetailFFText23" 
					Return Me.DetailFFText23
				Case "DetailFFText24" 
					Return Me.DetailFFText24
				Case "DetailFFText25" 
					Return Me.DetailFFText25
				Case "DetailFFText26" 
					Return Me.DetailFFText26
				Case "DetailFFText27" 
					Return Me.DetailFFText27
				Case "DetailFFText28" 
					Return Me.DetailFFText28
				Case "DetailFFText29" 
					Return Me.DetailFFText29
				Case "DetailFFText30" 
					Return Me.DetailFFText30
				Case "DetailFFText31" 
					Return Me.DetailFFText31
				Case "DetailFFText32" 
					Return Me.DetailFFText32
				Case "DetailFFText33" 
					Return Me.DetailFFText33
				Case "DetailFFText34" 
					Return Me.DetailFFText34
				Case "DetailFFText35" 
					Return Me.DetailFFText35
				Case "DetailFFText36" 
					Return Me.DetailFFText36
				Case "DetailFFText37" 
					Return Me.DetailFFText37
				Case "DetailFFText38" 
					Return Me.DetailFFText38
				Case "DetailFFText39" 
					Return Me.DetailFFText39
				Case "DetailFFText40" 
					Return Me.DetailFFText40
				Case "DetailFFText41" 
					Return Me.DetailFFText41
				Case "DetailFFText42" 
					Return Me.DetailFFText42
				Case "DetailFFText43" 
					Return Me.DetailFFText43
				Case "DetailFFText44" 
					Return Me.DetailFFText44
				Case "DetailFFText45" 
					Return Me.DetailFFText45
				Case "DetailFFText46" 
					Return Me.DetailFFText46
				Case "DetailFFText47" 
					Return Me.DetailFFText47
				Case "DetailFFText48" 
					Return Me.DetailFFText48
				Case "DetailFFText49" 
					Return Me.DetailFFText49
				Case "DetailFFText50" 
					Return Me.DetailFFText50
				Case "DetailFFText51" 
					Return Me.DetailFFText51
				Case "DetailFFText52" 
					Return Me.DetailFFText52
				Case "DetailFFText53" 
					Return Me.DetailFFText53
				Case "DetailFFText54" 
					Return Me.DetailFFText54
				Case "DetailFFText55" 
					Return Me.DetailFFText55
				Case "DetailFFText56" 
					Return Me.DetailFFText56
				Case "DetailFFText57" 
					Return Me.DetailFFText57
				Case "DetailFFText58" 
					Return Me.DetailFFText58
				Case "DetailFFText59" 
					Return Me.DetailFFText59
				Case "DetailFFText60" 
					Return Me.DetailFFText60
				Case "DetailFFText61" 
					Return Me.DetailFFText61
				Case "DetailFFText62" 
					Return Me.DetailFFText62
				Case "DetailFFText63" 
					Return Me.DetailFFText63
				Case "DetailFFText64" 
					Return Me.DetailFFText64
				Case "DetailFFText65" 
					Return Me.DetailFFText65
				Case "DetailFFText66" 
					Return Me.DetailFFText66
				Case "DetailFFText67" 
					Return Me.DetailFFText67
				Case "DetailFFText68" 
					Return Me.DetailFFText68
				Case "DetailFFText69" 
					Return Me.DetailFFText69
				Case "DetailFFText70" 
					Return Me.DetailFFText70
				Case "DetailFFText71" 
					Return Me.DetailFFText71
				Case "DetailFFText72" 
					Return Me.DetailFFText72
				Case "DetailFFText73" 
					Return Me.DetailFFText73
				Case "DetailFFText74" 
					Return Me.DetailFFText74
				Case "DetailFFText75" 
					Return Me.DetailFFText75
				Case "DetailFFText76" 
					Return Me.DetailFFText76
				Case "DetailFFText77" 
					Return Me.DetailFFText77
				Case "DetailFFText78" 
					Return Me.DetailFFText78
				Case "DetailFFText79" 
					Return Me.DetailFFText79
				Case "DetailFFText80" 
					Return Me.DetailFFText80
				Case "DetailFFText81" 
					Return Me.DetailFFText81
				Case "DetailFFText82" 
					Return Me.DetailFFText82
				Case "DetailFFText83" 
					Return Me.DetailFFText83
				Case "DetailFFText84" 
					Return Me.DetailFFText84
				Case "DetailFFText85" 
					Return Me.DetailFFText85
				Case "DetailFFText86" 
					Return Me.DetailFFText86
				Case "DetailFFText87" 
					Return Me.DetailFFText87
				Case "DetailFFText88" 
					Return Me.DetailFFText88
				Case "DetailFFText89" 
					Return Me.DetailFFText89
				Case "DetailFFText90" 
					Return Me.DetailFFText90
				Case "DetailFFText91" 
					Return Me.DetailFFText91
				Case "DetailFFText92" 
					Return Me.DetailFFText92
				Case "DetailFFText93" 
					Return Me.DetailFFText93
				Case "DetailFFText94" 
					Return Me.DetailFFText94
				Case "DetailFFText95" 
					Return Me.DetailFFText95
				Case "DetailFFText96" 
					Return Me.DetailFFText96
				Case "DetailFFText97" 
					Return Me.DetailFFText97
				Case "DetailFFText98" 
					Return Me.DetailFFText98
				Case "DetailFFText99" 
					Return Me.DetailFFText99
				Case "DetailFFText100" 
					Return Me.DetailFFText100
				Case "FFAmt7" 
					Return Me.FFAmt7
				Case "FFAmt8" 
					Return Me.FFAmt8
				Case "FFAmt9" 
					Return Me.FFAmt9
				Case "FFAmt10" 
					Return Me.FFAmt10
				Case "FFAmt11" 
					Return Me.FFAmt11
				Case "FFAmt12" 
					Return Me.FFAmt12
				Case "FFAmt13" 
					Return Me.FFAmt13
				Case "FFAmt14" 
					Return Me.FFAmt14
				Case "FFAmt15" 
					Return Me.FFAmt15
				Case "FFAmt16" 
					Return Me.FFAmt16
				Case "FFAmt17" 
					Return Me.FFAmt17
				Case "FFAmt18" 
					Return Me.FFAmt18
				Case "FFAmt19" 
					Return Me.FFAmt19
				Case "FFAmt20" 
					Return Me.FFAmt20
				Case "FFAmt21" 
					Return Me.FFAmt21
				Case "FFAmt22" 
					Return Me.FFAmt22
				Case "FFAmt23" 
					Return Me.FFAmt23
				Case "FFAmt24" 
					Return Me.FFAmt24
				Case "FFAmt25" 
					Return Me.FFAmt25
				Case "FFAmt26" 
					Return Me.FFAmt26
				Case "FFAmt27" 
					Return Me.FFAmt27
				Case "FFAmt28" 
					Return Me.FFAmt28
				Case "FFAmt29" 
					Return Me.FFAmt29
				Case "FFAmt30" 
					Return Me.FFAmt30
				Case "FFAmt31" 
					Return Me.FFAmt31
				Case "FFAmt32" 
					Return Me.FFAmt32
				Case "FFAmt33" 
					Return Me.FFAmt33
				Case "FFAmt34" 
					Return Me.FFAmt34
				Case "FFAmt35" 
					Return Me.FFAmt35
				Case "FFAmt36" 
					Return Me.FFAmt36
				Case "FFAmt37" 
					Return Me.FFAmt37
				Case "FFAmt38" 
					Return Me.FFAmt38
				Case "FFAmt39" 
					Return Me.FFAmt39
				Case "FFAmt40" 
					Return Me.FFAmt40
				Case "FFAmt41" 
					Return Me.FFAmt41
				Case "FFAmt42" 
					Return Me.FFAmt42
				Case "FFAmt43" 
					Return Me.FFAmt43
				Case "FFAmt44" 
					Return Me.FFAmt44
				Case "FFAmt45" 
					Return Me.FFAmt45
				Case "FFAmt46" 
					Return Me.FFAmt46
				Case "FFAmt47" 
					Return Me.FFAmt47
				Case "FFAmt48" 
					Return Me.FFAmt48
				Case "FFAmt49" 
					Return Me.FFAmt49
				Case "FFAmt50" 
					Return Me.FFAmt50
				Case "FFTxt21" 
					Return Me.FFTxt21
				Case "FFTxt22" 
					Return Me.FFTxt22
				Case "FFTxt23" 
					Return Me.FFTxt23
				Case "FFTxt24" 
					Return Me.FFTxt24
				Case "FFTxt25" 
					Return Me.FFTxt25
				Case "FFTxt26" 
					Return Me.FFTxt26
				Case "FFTxt27" 
					Return Me.FFTxt27
				Case "FFTxt28" 
					Return Me.FFTxt28
				Case "FFTxt29" 
					Return Me.FFTxt29
				Case "FFTxt30" 
					Return Me.FFTxt30
				Case "FFTxt31" 
					Return Me.FFTxt31
				Case "FFTxt32" 
					Return Me.FFTxt32
				Case "FFTxt33" 
					Return Me.FFTxt33
				Case "FFTxt34" 
					Return Me.FFTxt34
				Case "FFTxt35" 
					Return Me.FFTxt35
				Case "FFTxt36" 
					Return Me.FFTxt36
				Case "FFTxt37" 
					Return Me.FFTxt37
				Case "FFTxt38" 
					Return Me.FFTxt38
				Case "FFTxt39" 
					Return Me.FFTxt39
				Case "FFTxt40" 
					Return Me.FFTxt40
				Case "FFTxt41" 
					Return Me.FFTxt41
				Case "FFTxt42" 
					Return Me.FFTxt42
				Case "FFTxt43" 
					Return Me.FFTxt43
				Case "FFTxt44" 
					Return Me.FFTxt44
				Case "FFTxt45" 
					Return Me.FFTxt45
				Case "FFTxt46" 
					Return Me.FFTxt46
				Case "FFTxt47" 
					Return Me.FFTxt47
				Case "FFTxt48" 
					Return Me.FFTxt48
				Case "FFTxt49" 
					Return Me.FFTxt49
				Case "FFTxt50" 
					Return Me.FFTxt50
				Case "FFTxt51" 
					Return Me.FFTxt51
				Case "FFTxt52" 
					Return Me.FFTxt52
				Case "FFTxt53" 
					Return Me.FFTxt53
				Case "FFTxt54" 
					Return Me.FFTxt54
				Case "FFTxt55" 
					Return Me.FFTxt55
				Case "FFTxt56" 
					Return Me.FFTxt56
				Case "FFTxt57" 
					Return Me.FFTxt57
				Case "FFTxt58" 
					Return Me.FFTxt58
				Case "FFTxt59" 
					Return Me.FFTxt59
				Case "FFTxt60" 
					Return Me.FFTxt60
				Case "FFTxt61" 
					Return Me.FFTxt61
				Case "FFTxt62" 
					Return Me.FFTxt62
				Case "FFTxt63" 
					Return Me.FFTxt63
				Case "FFTxt64" 
					Return Me.FFTxt64
				Case "FFTxt65" 
					Return Me.FFTxt65
				Case "FFTxt66" 
					Return Me.FFTxt66
				Case "FFTxt67" 
					Return Me.FFTxt67
				Case "FFTxt68" 
					Return Me.FFTxt68
				Case "FFTxt69" 
					Return Me.FFTxt69
				Case "FFTxt70" 
					Return Me.FFTxt70
				Case "FFTxt71" 
					Return Me.FFTxt71
				Case "FFTxt72" 
					Return Me.FFTxt72
				Case "FFTxt73" 
					Return Me.FFTxt73
				Case "FFTxt74" 
					Return Me.FFTxt74
				Case "FFTxt75" 
					Return Me.FFTxt75
				Case "FFTxt76" 
					Return Me.FFTxt76
				Case "FFTxt77" 
					Return Me.FFTxt77
				Case "FFTxt78" 
					Return Me.FFTxt78
				Case "FFTxt79" 
					Return Me.FFTxt79
				Case "FFTxt80" 
					Return Me.FFTxt80
				Case "FFTxt81" 
					Return Me.FFTxt81
				Case "FFTxt82" 
					Return Me.FFTxt82
				Case "FFTxt83" 
					Return Me.FFTxt83
				Case "FFTxt84" 
					Return Me.FFTxt84
				Case "FFTxt85" 
					Return Me.FFTxt85
				Case "FFTxt86" 
					Return Me.FFTxt86
				Case "FFTxt87" 
					Return Me.FFTxt87
				Case "FFTxt88" 
					Return Me.FFTxt88
				Case "FFTxt89" 
					Return Me.FFTxt89
				Case "FFTxt90" 
					Return Me.FFTxt90
				Case "FFTxt91" 
					Return Me.FFTxt91
				Case "FFTxt92" 
					Return Me.FFTxt92
				Case "FFTxt93" 
					Return Me.FFTxt93
				Case "FFTxt94" 
					Return Me.FFTxt94
				Case "FFTxt95" 
					Return Me.FFTxt95
				Case "FFTxt96" 
					Return Me.FFTxt96
				Case "FFTxt97" 
					Return Me.FFTxt97
				Case "FFTxt98" 
					Return Me.FFTxt98
				Case "FFTxt99" 
					Return Me.FFTxt99
				Case "FFTxt100" 
					Return Me.FFTxt100
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property InstructionDetailID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.InstructionDetailID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt1, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt2, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt3, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt4 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt4, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt5 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt5, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt6 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt6, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt3, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt4 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt4, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt5 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt5, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt6 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt6, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt7 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt7, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt8 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt8, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt9 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt9, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt10 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt10, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt11 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt11, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt12 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt12, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt13 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt13, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt14 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt14, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt15 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt15, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt16 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt16, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt17 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt17, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt18 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt18, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt19 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt19, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt20 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt20, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneBankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneBankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneBranchAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneBranchName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneCNIC As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneCNIC, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneCountry As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneCountry, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneProvince As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneProvince, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneCity As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneCity, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneEmail As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneEmail, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBeneMobile As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBeneMobile, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailBenePhone As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailBenePhone, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailInvoiceNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailInvoiceNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailISNO As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailISNO, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DetailTXNCODE As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailTXNCODE, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DetailDetailAmt As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailDetailAmt, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText3, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText4 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText4, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText5 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText5, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText6 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText6, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText7 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText7, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText8 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText8, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText9 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText9, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText10 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText10, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText11 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText11, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText12 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText12, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText13 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText13, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText14 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText14, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText15 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText15, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText16 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText16, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText17 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText17, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText18 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText18, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText19 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText19, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText20 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText20, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt1, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt2, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt3, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt4 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt4, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt5 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt5, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt6 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt6, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt7 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt7, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt8 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt8, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt9 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt9, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt10 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt10, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt11 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt11, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt12 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt12, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt13 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt13, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt14 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt14, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt15 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt15, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt16 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt16, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt17 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt17, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt18 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt18, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt19 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt19, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt20 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt20, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt21 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt21, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt22 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt22, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt23 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt23, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt24 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt24, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt25 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt25, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt26 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt26, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt27 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt27, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt28 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt28, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt29 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt29, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt30 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt30, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt31 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt31, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt32 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt32, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt33 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt33, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt34 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt34, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt35 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt35, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt36 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt36, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt37 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt37, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt38 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt38, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt39 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt39, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt40 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt40, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt41 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt41, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt42 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt42, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt43 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt43, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt44 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt44, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt45 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt45, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt46 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt46, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt47 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt47, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt48 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt48, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt49 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt49, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFAmt50 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFAmt50, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText21 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText21, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText22 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText22, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText23 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText23, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText24 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText24, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText25 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText25, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText26 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText26, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText27 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText27, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText28 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText28, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText29 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText29, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText30 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText30, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText31 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText31, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText32 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText32, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText33 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText33, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText34 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText34, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText35 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText35, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText36 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText36, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText37 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText37, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText38 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText38, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText39 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText39, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText40 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText40, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText41 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText41, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText42 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText42, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText43 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText43, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText44 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText44, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText45 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText45, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText46 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText46, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText47 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText47, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText48 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText48, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText49 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText49, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText50 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText50, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText51 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText51, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText52 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText52, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText53 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText53, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText54 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText54, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText55 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText55, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText56 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText56, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText57 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText57, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText58 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText58, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText59 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText59, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText60 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText60, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText61 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText61, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText62 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText62, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText63 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText63, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText64 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText64, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText65 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText65, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText66 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText66, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText67 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText67, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText68 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText68, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText69 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText69, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText70 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText70, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText71 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText71, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText72 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText72, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText73 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText73, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText74 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText74, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText75 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText75, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText76 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText76, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText77 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText77, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText78 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText78, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText79 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText79, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText80 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText80, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText81 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText81, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText82 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText82, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText83 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText83, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText84 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText84, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText85 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText85, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText86 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText86, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText87 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText87, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText88 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText88, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText89 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText89, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText90 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText90, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText91 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText91, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText92 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText92, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText93 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText93, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText94 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText94, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText95 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText95, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText96 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText96, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText97 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText97, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText98 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText98, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText99 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText99, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailFFText100 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.DetailFFText100, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt7 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt7, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt8 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt8, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt9 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt9, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt10 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt10, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt11 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt11, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt12 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt12, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt13 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt13, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt14 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt14, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt15 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt15, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt16 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt16, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt17 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt17, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt18 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt18, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt19 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt19, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt20 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt20, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt21 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt21, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt22 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt22, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt23 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt23, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt24 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt24, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt25 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt25, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt26 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt26, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt27 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt27, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt28 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt28, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt29 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt29, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt30 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt30, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt31 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt31, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt32 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt32, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt33 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt33, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt34 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt34, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt35 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt35, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt36 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt36, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt37 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt37, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt38 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt38, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt39 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt39, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt40 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt40, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt41 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt41, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt42 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt42, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt43 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt43, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt44 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt44, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt45 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt45, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt46 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt46, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt47 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt47, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt48 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt48, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt49 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt49, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFAmt50 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFAmt50, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt21 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt21, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt22 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt22, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt23 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt23, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt24 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt24, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt25 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt25, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt26 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt26, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt27 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt27, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt28 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt28, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt29 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt29, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt30 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt30, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt31 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt31, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt32 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt32, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt33 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt33, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt34 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt34, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt35 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt35, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt36 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt36, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt37 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt37, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt38 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt38, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt39 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt39, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt40 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt40, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt41 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt41, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt42 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt42, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt43 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt43, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt44 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt44, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt45 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt45, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt46 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt46, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt47 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt47, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt48 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt48, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt49 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt49, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt50 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt50, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt51 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt51, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt52 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt52, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt53 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt53, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt54 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt54, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt55 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt55, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt56 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt56, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt57 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt57, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt58 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt58, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt59 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt59, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt60 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt60, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt61 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt61, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt62 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt62, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt63 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt63, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt64 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt64, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt65 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt65, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt66 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt66, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt67 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt67, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt68 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt68, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt69 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt69, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt70 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt70, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt71 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt71, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt72 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt72, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt73 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt73, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt74 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt74, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt75 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt75, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt76 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt76, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt77 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt77, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt78 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt78, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt79 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt79, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt80 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt80, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt81 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt81, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt82 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt82, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt83 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt83, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt84 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt84, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt85 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt85, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt86 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt86, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt87 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt87, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt88 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt88, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt89 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt89, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt90 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt90, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt91 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt91, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt92 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt92, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt93 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt93, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt94 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt94, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt95 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt95, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt96 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt96, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt97 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt97, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt98 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt98, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt99 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt99, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FFTxt100 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionDetailMetadata.ColumnNames.FFTxt100, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstructionDetail 
		Inherits esICInstructionDetail
		
	
		#Region "UpToICInstructionByInstructionID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_InstructionDetail_IC_Instruction
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionByInstructionID As ICInstruction
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionByInstructionID Is Nothing _
						 AndAlso Not InstructionID.Equals(Nothing)  Then
						
					Me._UpToICInstructionByInstructionID = New ICInstruction()
					Me._UpToICInstructionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
					Me._UpToICInstructionByInstructionID.Query.Where(Me._UpToICInstructionByInstructionID.Query.InstructionID = Me.InstructionID)
					Me._UpToICInstructionByInstructionID.Query.Load()
				End If

				Return Me._UpToICInstructionByInstructionID
			End Get
			
            Set(ByVal value As ICInstruction)
				Me.RemovePreSave("UpToICInstructionByInstructionID")
				

				If value Is Nothing Then
				
					Me.InstructionID = Nothing
				
					Me._UpToICInstructionByInstructionID = Nothing
				Else
				
					Me.InstructionID = value.InstructionID
					
					Me._UpToICInstructionByInstructionID = value
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionByInstructionID Is Nothing Then
				Me.InstructionID = Me._UpToICInstructionByInstructionID.InstructionID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstructionDetailMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.InstructionDetailID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.InstructionDetailID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.InstructionID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt1, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt1
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt2, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt2
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt3, 4, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt3
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt4, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt4
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt5, 6, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt5
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt6, 7, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt6
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt1, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt1
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt2, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt2
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt3, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt3
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt4, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt4
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt5, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt5
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt6, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt6
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt7, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt7
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt8, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt8
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt9, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt9
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt10, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt10
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt11, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt11
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt12, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt12
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt13, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt13
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt14, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt14
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt15, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt15
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt16, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt16
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt17, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt17
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt18, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt18
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt19, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt19
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt20, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt20
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailAmount, 28, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneAccountNo, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneAccountNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneAddress, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneBankName, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneBankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchAddress, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchCode, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchCode
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneBranchName, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneBranchName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneCNIC, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneCNIC
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneCountry, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneCountry
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneProvince, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneProvince
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneCity, 38, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneCity
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneEmail, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneEmail
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBeneMobile, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBeneMobile
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailBenePhone, 41, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailBenePhone
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailInvoiceNo, 42, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailInvoiceNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailISNO, 43, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailISNO
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailTXNCODE, 44, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailTXNCODE
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailDetailAmt, 45, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailDetailAmt
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText1, 46, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText1
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText2, 47, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText2
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText3, 48, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText3
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText4, 49, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText4
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText5, 50, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText5
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText6, 51, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText6
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText7, 52, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText7
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText8, 53, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText8
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText9, 54, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText9
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText10, 55, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText10
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText11, 56, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText11
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText12, 57, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText12
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText13, 58, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText13
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText14, 59, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText14
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText15, 60, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText15
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText16, 61, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText16
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText17, 62, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText17
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText18, 63, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText18
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText19, 64, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText19
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText20, 65, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText20
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt1, 66, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt1
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt2, 67, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt2
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt3, 68, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt3
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt4, 69, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt4
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt5, 70, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt5
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt6, 71, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt6
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt7, 72, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt7
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt8, 73, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt8
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt9, 74, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt9
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt10, 75, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt10
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt11, 76, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt11
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt12, 77, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt12
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt13, 78, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt13
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt14, 79, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt14
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt15, 80, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt15
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt16, 81, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt16
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt17, 82, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt17
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt18, 83, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt18
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt19, 84, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt19
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt20, 85, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt20
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt21, 86, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt21
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt22, 87, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt22
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt23, 88, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt23
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt24, 89, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt24
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt25, 90, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt25
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt26, 91, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt26
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt27, 92, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt27
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt28, 93, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt28
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt29, 94, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt29
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt30, 95, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt30
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt31, 96, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt31
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt32, 97, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt32
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt33, 98, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt33
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt34, 99, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt34
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt35, 100, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt35
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt36, 101, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt36
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt37, 102, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt37
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt38, 103, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt38
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt39, 104, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt39
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt40, 105, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt40
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt41, 106, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt41
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt42, 107, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt42
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt43, 108, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt43
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt44, 109, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt44
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt45, 110, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt45
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt46, 111, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt46
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt47, 112, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt47
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt48, 113, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt48
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt49, 114, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt49
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFAmt50, 115, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFAmt50
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText21, 116, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText21
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText22, 117, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText22
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText23, 118, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText23
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText24, 119, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText24
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText25, 120, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText25
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText26, 121, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText26
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText27, 122, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText27
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText28, 123, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText28
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText29, 124, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText29
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText30, 125, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText30
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText31, 126, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText31
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText32, 127, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText32
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText33, 128, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText33
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText34, 129, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText34
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText35, 130, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText35
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText36, 131, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText36
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText37, 132, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText37
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText38, 133, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText38
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText39, 134, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText39
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText40, 135, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText40
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText41, 136, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText41
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText42, 137, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText42
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText43, 138, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText43
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText44, 139, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText44
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText45, 140, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText45
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText46, 141, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText46
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText47, 142, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText47
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText48, 143, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText48
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText49, 144, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText49
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText50, 145, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText50
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText51, 146, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText51
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText52, 147, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText52
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText53, 148, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText53
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText54, 149, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText54
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText55, 150, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText55
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText56, 151, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText56
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText57, 152, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText57
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText58, 153, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText58
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText59, 154, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText59
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText60, 155, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText60
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText61, 156, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText61
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText62, 157, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText62
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText63, 158, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText63
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText64, 159, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText64
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText65, 160, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText65
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText66, 161, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText66
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText67, 162, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText67
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText68, 163, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText68
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText69, 164, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText69
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText70, 165, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText70
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText71, 166, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText71
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText72, 167, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText72
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText73, 168, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText73
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText74, 169, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText74
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText75, 170, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText75
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText76, 171, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText76
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText77, 172, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText77
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText78, 173, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText78
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText79, 174, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText79
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText80, 175, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText80
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText81, 176, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText81
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText82, 177, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText82
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText83, 178, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText83
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText84, 179, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText84
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText85, 180, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText85
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText86, 181, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText86
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText87, 182, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText87
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText88, 183, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText88
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText89, 184, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText89
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText90, 185, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText90
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText91, 186, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText91
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText92, 187, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText92
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText93, 188, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText93
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText94, 189, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText94
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText95, 190, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText95
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText96, 191, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText96
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText97, 192, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText97
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText98, 193, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText98
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText99, 194, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText99
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.DetailFFText100, 195, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.DetailFFText100
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt7, 196, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt7
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt8, 197, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt8
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt9, 198, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt9
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt10, 199, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt10
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt11, 200, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt11
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt12, 201, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt12
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt13, 202, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt13
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt14, 203, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt14
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt15, 204, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt15
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt16, 205, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt16
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt17, 206, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt17
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt18, 207, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt18
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt19, 208, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt19
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt20, 209, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt20
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt21, 210, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt21
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt22, 211, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt22
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt23, 212, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt23
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt24, 213, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt24
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt25, 214, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt25
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt26, 215, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt26
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt27, 216, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt27
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt28, 217, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt28
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt29, 218, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt29
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt30, 219, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt30
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt31, 220, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt31
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt32, 221, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt32
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt33, 222, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt33
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt34, 223, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt34
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt35, 224, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt35
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt36, 225, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt36
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt37, 226, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt37
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt38, 227, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt38
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt39, 228, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt39
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt40, 229, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt40
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt41, 230, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt41
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt42, 231, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt42
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt43, 232, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt43
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt44, 233, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt44
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt45, 234, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt45
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt46, 235, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt46
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt47, 236, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt47
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt48, 237, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt48
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt49, 238, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt49
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFAmt50, 239, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFAmt50
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt21, 240, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt21
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt22, 241, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt22
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt23, 242, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt23
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt24, 243, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt24
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt25, 244, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt25
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt26, 245, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt26
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt27, 246, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt27
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt28, 247, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt28
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt29, 248, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt29
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt30, 249, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt30
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt31, 250, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt31
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt32, 251, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt32
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt33, 252, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt33
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt34, 253, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt34
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt35, 254, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt35
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt36, 255, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt36
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt37, 256, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt37
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt38, 257, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt38
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt39, 258, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt39
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt40, 259, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt40
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt41, 260, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt41
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt42, 261, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt42
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt43, 262, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt43
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt44, 263, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt44
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt45, 264, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt45
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt46, 265, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt46
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt47, 266, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt47
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt48, 267, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt48
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt49, 268, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt49
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt50, 269, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt50
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt51, 270, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt51
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt52, 271, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt52
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt53, 272, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt53
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt54, 273, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt54
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt55, 274, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt55
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt56, 275, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt56
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt57, 276, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt57
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt58, 277, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt58
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt59, 278, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt59
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt60, 279, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt60
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt61, 280, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt61
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt62, 281, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt62
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt63, 282, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt63
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt64, 283, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt64
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt65, 284, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt65
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt66, 285, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt66
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt67, 286, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt67
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt68, 287, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt68
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt69, 288, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt69
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt70, 289, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt70
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt71, 290, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt71
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt72, 291, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt72
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt73, 292, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt73
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt74, 293, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt74
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt75, 294, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt75
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt76, 295, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt76
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt77, 296, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt77
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt78, 297, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt78
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt79, 298, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt79
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt80, 299, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt80
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt81, 300, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt81
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt82, 301, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt82
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt83, 302, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt83
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt84, 303, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt84
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt85, 304, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt85
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt86, 305, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt86
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt87, 306, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt87
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt88, 307, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt88
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt89, 308, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt89
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt90, 309, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt90
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt91, 310, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt91
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt92, 311, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt92
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt93, 312, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt93
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt94, 313, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt94
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt95, 314, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt95
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt96, 315, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt96
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt97, 316, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt97
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt98, 317, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt98
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt99, 318, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt99
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionDetailMetadata.ColumnNames.FFTxt100, 319, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionDetailMetadata.PropertyNames.FFTxt100
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstructionDetailMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const InstructionDetailID As String = "InstructionDetailID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FFAmt1 As String = "FF_Amt_1"
			 Public Const FFAmt2 As String = "FF_Amt_2"
			 Public Const FFAmt3 As String = "FF_Amt_3"
			 Public Const FFAmt4 As String = "FF_Amt_4"
			 Public Const FFAmt5 As String = "FF_Amt_5"
			 Public Const FFAmt6 As String = "FF_Amt_6"
			 Public Const FFTxt1 As String = "FF_txt_1"
			 Public Const FFTxt2 As String = "FF_txt_2"
			 Public Const FFTxt3 As String = "FF_txt_3"
			 Public Const FFTxt4 As String = "FF_txt_4"
			 Public Const FFTxt5 As String = "FF_txt_5"
			 Public Const FFTxt6 As String = "FF_txt_6"
			 Public Const FFTxt7 As String = "FF_txt_7"
			 Public Const FFTxt8 As String = "FF_txt_8"
			 Public Const FFTxt9 As String = "FF_txt_9"
			 Public Const FFTxt10 As String = "FF_txt_10"
			 Public Const FFTxt11 As String = "FF_txt_11"
			 Public Const FFTxt12 As String = "FF_txt_12"
			 Public Const FFTxt13 As String = "FF_txt_13"
			 Public Const FFTxt14 As String = "FF_txt_14"
			 Public Const FFTxt15 As String = "FF_txt_15"
			 Public Const FFTxt16 As String = "FF_txt_16"
			 Public Const FFTxt17 As String = "FF_txt_17"
			 Public Const FFTxt18 As String = "FF_txt_18"
			 Public Const FFTxt19 As String = "FF_txt_19"
			 Public Const FFTxt20 As String = "FF_txt_20"
			 Public Const DetailAmount As String = "Detail_Amount"
			 Public Const DetailBeneAccountNo As String = "Detail_BeneAccountNo"
			 Public Const DetailBeneAddress As String = "Detail_BeneAddress"
			 Public Const DetailBeneBankName As String = "Detail_BeneBankName"
			 Public Const DetailBeneBranchAddress As String = "Detail_BeneBranchAddress"
			 Public Const DetailBeneBranchCode As String = "Detail_BeneBranchCode"
			 Public Const DetailBeneBranchName As String = "Detail_BeneBranchName"
			 Public Const DetailBeneCNIC As String = "Detail_BeneCNIC"
			 Public Const DetailBeneCountry As String = "Detail_BeneCountry"
			 Public Const DetailBeneProvince As String = "Detail_BeneProvince"
			 Public Const DetailBeneCity As String = "Detail_BeneCity"
			 Public Const DetailBeneEmail As String = "Detail_BeneEmail"
			 Public Const DetailBeneMobile As String = "Detail_BeneMobile"
			 Public Const DetailBenePhone As String = "Detail_BenePhone"
			 Public Const DetailInvoiceNo As String = "Detail_InvoiceNo"
			 Public Const DetailISNO As String = "Detail_IS_NO"
			 Public Const DetailTXNCODE As String = "Detail_TXN_CODE"
			 Public Const DetailDetailAmt As String = "Detail_DetailAmt"
			 Public Const DetailFFText1 As String = "Detail_FF_Text_1"
			 Public Const DetailFFText2 As String = "Detail_FF_Text_2"
			 Public Const DetailFFText3 As String = "Detail_FF_Text_3"
			 Public Const DetailFFText4 As String = "Detail_FF_Text_4"
			 Public Const DetailFFText5 As String = "Detail_FF_Text_5"
			 Public Const DetailFFText6 As String = "Detail_FF_Text_6"
			 Public Const DetailFFText7 As String = "Detail_FF_Text_7"
			 Public Const DetailFFText8 As String = "Detail_FF_Text_8"
			 Public Const DetailFFText9 As String = "Detail_FF_Text_9"
			 Public Const DetailFFText10 As String = "Detail_FF_Text_10"
			 Public Const DetailFFText11 As String = "Detail_FF_Text_11"
			 Public Const DetailFFText12 As String = "Detail_FF_Text_12"
			 Public Const DetailFFText13 As String = "Detail_FF_Text_13"
			 Public Const DetailFFText14 As String = "Detail_FF_Text_14"
			 Public Const DetailFFText15 As String = "Detail_FF_Text_15"
			 Public Const DetailFFText16 As String = "Detail_FF_Text_16"
			 Public Const DetailFFText17 As String = "Detail_FF_Text_17"
			 Public Const DetailFFText18 As String = "Detail_FF_Text_18"
			 Public Const DetailFFText19 As String = "Detail_FF_Text_19"
			 Public Const DetailFFText20 As String = "Detail_FF_Text_20"
			 Public Const DetailFFAmt1 As String = "Detail_FF_Amt_1"
			 Public Const DetailFFAmt2 As String = "Detail_FF_Amt_2"
			 Public Const DetailFFAmt3 As String = "Detail_FF_Amt_3"
			 Public Const DetailFFAmt4 As String = "Detail_FF_Amt_4"
			 Public Const DetailFFAmt5 As String = "Detail_FF_Amt_5"
			 Public Const DetailFFAmt6 As String = "Detail_FF_Amt_6"
			 Public Const DetailFFAmt7 As String = "Detail_FF_Amt_7"
			 Public Const DetailFFAmt8 As String = "Detail_FF_Amt_8"
			 Public Const DetailFFAmt9 As String = "Detail_FF_Amt_9"
			 Public Const DetailFFAmt10 As String = "Detail_FF_Amt_10"
			 Public Const DetailFFAmt11 As String = "Detail_FF_Amt_11"
			 Public Const DetailFFAmt12 As String = "Detail_FF_Amt_12"
			 Public Const DetailFFAmt13 As String = "Detail_FF_Amt_13"
			 Public Const DetailFFAmt14 As String = "Detail_FF_Amt_14"
			 Public Const DetailFFAmt15 As String = "Detail_FF_Amt_15"
			 Public Const DetailFFAmt16 As String = "Detail_FF_Amt_16"
			 Public Const DetailFFAmt17 As String = "Detail_FF_Amt_17"
			 Public Const DetailFFAmt18 As String = "Detail_FF_Amt_18"
			 Public Const DetailFFAmt19 As String = "Detail_FF_Amt_19"
			 Public Const DetailFFAmt20 As String = "Detail_FF_Amt_20"
			 Public Const DetailFFAmt21 As String = "Detail_FF_Amt_21"
			 Public Const DetailFFAmt22 As String = "Detail_FF_Amt_22"
			 Public Const DetailFFAmt23 As String = "Detail_FF_Amt_23"
			 Public Const DetailFFAmt24 As String = "Detail_FF_Amt_24"
			 Public Const DetailFFAmt25 As String = "Detail_FF_Amt_25"
			 Public Const DetailFFAmt26 As String = "Detail_FF_Amt_26"
			 Public Const DetailFFAmt27 As String = "Detail_FF_Amt_27"
			 Public Const DetailFFAmt28 As String = "Detail_FF_Amt_28"
			 Public Const DetailFFAmt29 As String = "Detail_FF_Amt_29"
			 Public Const DetailFFAmt30 As String = "Detail_FF_Amt_30"
			 Public Const DetailFFAmt31 As String = "Detail_FF_Amt_31"
			 Public Const DetailFFAmt32 As String = "Detail_FF_Amt_32"
			 Public Const DetailFFAmt33 As String = "Detail_FF_Amt_33"
			 Public Const DetailFFAmt34 As String = "Detail_FF_Amt_34"
			 Public Const DetailFFAmt35 As String = "Detail_FF_Amt_35"
			 Public Const DetailFFAmt36 As String = "Detail_FF_Amt_36"
			 Public Const DetailFFAmt37 As String = "Detail_FF_Amt_37"
			 Public Const DetailFFAmt38 As String = "Detail_FF_Amt_38"
			 Public Const DetailFFAmt39 As String = "Detail_FF_Amt_39"
			 Public Const DetailFFAmt40 As String = "Detail_FF_Amt_40"
			 Public Const DetailFFAmt41 As String = "Detail_FF_Amt_41"
			 Public Const DetailFFAmt42 As String = "Detail_FF_Amt_42"
			 Public Const DetailFFAmt43 As String = "Detail_FF_Amt_43"
			 Public Const DetailFFAmt44 As String = "Detail_FF_Amt_44"
			 Public Const DetailFFAmt45 As String = "Detail_FF_Amt_45"
			 Public Const DetailFFAmt46 As String = "Detail_FF_Amt_46"
			 Public Const DetailFFAmt47 As String = "Detail_FF_Amt_47"
			 Public Const DetailFFAmt48 As String = "Detail_FF_Amt_48"
			 Public Const DetailFFAmt49 As String = "Detail_FF_Amt_49"
			 Public Const DetailFFAmt50 As String = "Detail_FF_Amt_50"
			 Public Const DetailFFText21 As String = "Detail_FF_Text_21"
			 Public Const DetailFFText22 As String = "Detail_FF_Text_22"
			 Public Const DetailFFText23 As String = "Detail_FF_Text_23"
			 Public Const DetailFFText24 As String = "Detail_FF_Text_24"
			 Public Const DetailFFText25 As String = "Detail_FF_Text_25"
			 Public Const DetailFFText26 As String = "Detail_FF_Text_26"
			 Public Const DetailFFText27 As String = "Detail_FF_Text_27"
			 Public Const DetailFFText28 As String = "Detail_FF_Text_28"
			 Public Const DetailFFText29 As String = "Detail_FF_Text_29"
			 Public Const DetailFFText30 As String = "Detail_FF_Text_30"
			 Public Const DetailFFText31 As String = "Detail_FF_Text_31"
			 Public Const DetailFFText32 As String = "Detail_FF_Text_32"
			 Public Const DetailFFText33 As String = "Detail_FF_Text_33"
			 Public Const DetailFFText34 As String = "Detail_FF_Text_34"
			 Public Const DetailFFText35 As String = "Detail_FF_Text_35"
			 Public Const DetailFFText36 As String = "Detail_FF_Text_36"
			 Public Const DetailFFText37 As String = "Detail_FF_Text_37"
			 Public Const DetailFFText38 As String = "Detail_FF_Text_38"
			 Public Const DetailFFText39 As String = "Detail_FF_Text_39"
			 Public Const DetailFFText40 As String = "Detail_FF_Text_40"
			 Public Const DetailFFText41 As String = "Detail_FF_Text_41"
			 Public Const DetailFFText42 As String = "Detail_FF_Text_42"
			 Public Const DetailFFText43 As String = "Detail_FF_Text_43"
			 Public Const DetailFFText44 As String = "Detail_FF_Text_44"
			 Public Const DetailFFText45 As String = "Detail_FF_Text_45"
			 Public Const DetailFFText46 As String = "Detail_FF_Text_46"
			 Public Const DetailFFText47 As String = "Detail_FF_Text_47"
			 Public Const DetailFFText48 As String = "Detail_FF_Text_48"
			 Public Const DetailFFText49 As String = "Detail_FF_Text_49"
			 Public Const DetailFFText50 As String = "Detail_FF_Text_50"
			 Public Const DetailFFText51 As String = "Detail_FF_Text_51"
			 Public Const DetailFFText52 As String = "Detail_FF_Text_52"
			 Public Const DetailFFText53 As String = "Detail_FF_Text_53"
			 Public Const DetailFFText54 As String = "Detail_FF_Text_54"
			 Public Const DetailFFText55 As String = "Detail_FF_Text_55"
			 Public Const DetailFFText56 As String = "Detail_FF_Text_56"
			 Public Const DetailFFText57 As String = "Detail_FF_Text_57"
			 Public Const DetailFFText58 As String = "Detail_FF_Text_58"
			 Public Const DetailFFText59 As String = "Detail_FF_Text_59"
			 Public Const DetailFFText60 As String = "Detail_FF_Text_60"
			 Public Const DetailFFText61 As String = "Detail_FF_Text_61"
			 Public Const DetailFFText62 As String = "Detail_FF_Text_62"
			 Public Const DetailFFText63 As String = "Detail_FF_Text_63"
			 Public Const DetailFFText64 As String = "Detail_FF_Text_64"
			 Public Const DetailFFText65 As String = "Detail_FF_Text_65"
			 Public Const DetailFFText66 As String = "Detail_FF_Text_66"
			 Public Const DetailFFText67 As String = "Detail_FF_Text_67"
			 Public Const DetailFFText68 As String = "Detail_FF_Text_68"
			 Public Const DetailFFText69 As String = "Detail_FF_Text_69"
			 Public Const DetailFFText70 As String = "Detail_FF_Text_70"
			 Public Const DetailFFText71 As String = "Detail_FF_Text_71"
			 Public Const DetailFFText72 As String = "Detail_FF_Text_72"
			 Public Const DetailFFText73 As String = "Detail_FF_Text_73"
			 Public Const DetailFFText74 As String = "Detail_FF_Text_74"
			 Public Const DetailFFText75 As String = "Detail_FF_Text_75"
			 Public Const DetailFFText76 As String = "Detail_FF_Text_76"
			 Public Const DetailFFText77 As String = "Detail_FF_Text_77"
			 Public Const DetailFFText78 As String = "Detail_FF_Text_78"
			 Public Const DetailFFText79 As String = "Detail_FF_Text_79"
			 Public Const DetailFFText80 As String = "Detail_FF_Text_80"
			 Public Const DetailFFText81 As String = "Detail_FF_Text_81"
			 Public Const DetailFFText82 As String = "Detail_FF_Text_82"
			 Public Const DetailFFText83 As String = "Detail_FF_Text_83"
			 Public Const DetailFFText84 As String = "Detail_FF_Text_84"
			 Public Const DetailFFText85 As String = "Detail_FF_Text_85"
			 Public Const DetailFFText86 As String = "Detail_FF_Text_86"
			 Public Const DetailFFText87 As String = "Detail_FF_Text_87"
			 Public Const DetailFFText88 As String = "Detail_FF_Text_88"
			 Public Const DetailFFText89 As String = "Detail_FF_Text_89"
			 Public Const DetailFFText90 As String = "Detail_FF_Text_90"
			 Public Const DetailFFText91 As String = "Detail_FF_Text_91"
			 Public Const DetailFFText92 As String = "Detail_FF_Text_92"
			 Public Const DetailFFText93 As String = "Detail_FF_Text_93"
			 Public Const DetailFFText94 As String = "Detail_FF_Text_94"
			 Public Const DetailFFText95 As String = "Detail_FF_Text_95"
			 Public Const DetailFFText96 As String = "Detail_FF_Text_96"
			 Public Const DetailFFText97 As String = "Detail_FF_Text_97"
			 Public Const DetailFFText98 As String = "Detail_FF_Text_98"
			 Public Const DetailFFText99 As String = "Detail_FF_Text_99"
			 Public Const DetailFFText100 As String = "Detail_FF_Text_100"
			 Public Const FFAmt7 As String = "FF_Amt_7"
			 Public Const FFAmt8 As String = "FF_Amt_8"
			 Public Const FFAmt9 As String = "FF_Amt_9"
			 Public Const FFAmt10 As String = "FF_Amt_10"
			 Public Const FFAmt11 As String = "FF_Amt_11"
			 Public Const FFAmt12 As String = "FF_Amt_12"
			 Public Const FFAmt13 As String = "FF_Amt_13"
			 Public Const FFAmt14 As String = "FF_Amt_14"
			 Public Const FFAmt15 As String = "FF_Amt_15"
			 Public Const FFAmt16 As String = "FF_Amt_16"
			 Public Const FFAmt17 As String = "FF_Amt_17"
			 Public Const FFAmt18 As String = "FF_Amt_18"
			 Public Const FFAmt19 As String = "FF_Amt_19"
			 Public Const FFAmt20 As String = "FF_Amt_20"
			 Public Const FFAmt21 As String = "FF_Amt_21"
			 Public Const FFAmt22 As String = "FF_Amt_22"
			 Public Const FFAmt23 As String = "FF_Amt_23"
			 Public Const FFAmt24 As String = "FF_Amt_24"
			 Public Const FFAmt25 As String = "FF_Amt_25"
			 Public Const FFAmt26 As String = "FF_Amt_26"
			 Public Const FFAmt27 As String = "FF_Amt_27"
			 Public Const FFAmt28 As String = "FF_Amt_28"
			 Public Const FFAmt29 As String = "FF_Amt_29"
			 Public Const FFAmt30 As String = "FF_Amt_30"
			 Public Const FFAmt31 As String = "FF_Amt_31"
			 Public Const FFAmt32 As String = "FF_Amt_32"
			 Public Const FFAmt33 As String = "FF_Amt_33"
			 Public Const FFAmt34 As String = "FF_Amt_34"
			 Public Const FFAmt35 As String = "FF_Amt_35"
			 Public Const FFAmt36 As String = "FF_Amt_36"
			 Public Const FFAmt37 As String = "FF_Amt_37"
			 Public Const FFAmt38 As String = "FF_Amt_38"
			 Public Const FFAmt39 As String = "FF_Amt_39"
			 Public Const FFAmt40 As String = "FF_Amt_40"
			 Public Const FFAmt41 As String = "FF_Amt_41"
			 Public Const FFAmt42 As String = "FF_Amt_42"
			 Public Const FFAmt43 As String = "FF_Amt_43"
			 Public Const FFAmt44 As String = "FF_Amt_44"
			 Public Const FFAmt45 As String = "FF_Amt_45"
			 Public Const FFAmt46 As String = "FF_Amt_46"
			 Public Const FFAmt47 As String = "FF_Amt_47"
			 Public Const FFAmt48 As String = "FF_Amt_48"
			 Public Const FFAmt49 As String = "FF_Amt_49"
			 Public Const FFAmt50 As String = "FF_Amt_50"
			 Public Const FFTxt21 As String = "FF_txt_21"
			 Public Const FFTxt22 As String = "FF_txt_22"
			 Public Const FFTxt23 As String = "FF_txt_23"
			 Public Const FFTxt24 As String = "FF_txt_24"
			 Public Const FFTxt25 As String = "FF_txt_25"
			 Public Const FFTxt26 As String = "FF_txt_26"
			 Public Const FFTxt27 As String = "FF_txt_27"
			 Public Const FFTxt28 As String = "FF_txt_28"
			 Public Const FFTxt29 As String = "FF_txt_29"
			 Public Const FFTxt30 As String = "FF_txt_30"
			 Public Const FFTxt31 As String = "FF_txt_31"
			 Public Const FFTxt32 As String = "FF_txt_32"
			 Public Const FFTxt33 As String = "FF_txt_33"
			 Public Const FFTxt34 As String = "FF_txt_34"
			 Public Const FFTxt35 As String = "FF_txt_35"
			 Public Const FFTxt36 As String = "FF_txt_36"
			 Public Const FFTxt37 As String = "FF_txt_37"
			 Public Const FFTxt38 As String = "FF_txt_38"
			 Public Const FFTxt39 As String = "FF_txt_39"
			 Public Const FFTxt40 As String = "FF_txt_40"
			 Public Const FFTxt41 As String = "FF_txt_41"
			 Public Const FFTxt42 As String = "FF_txt_42"
			 Public Const FFTxt43 As String = "FF_txt_43"
			 Public Const FFTxt44 As String = "FF_txt_44"
			 Public Const FFTxt45 As String = "FF_txt_45"
			 Public Const FFTxt46 As String = "FF_txt_46"
			 Public Const FFTxt47 As String = "FF_txt_47"
			 Public Const FFTxt48 As String = "FF_txt_48"
			 Public Const FFTxt49 As String = "FF_txt_49"
			 Public Const FFTxt50 As String = "FF_txt_50"
			 Public Const FFTxt51 As String = "FF_txt_51"
			 Public Const FFTxt52 As String = "FF_txt_52"
			 Public Const FFTxt53 As String = "FF_txt_53"
			 Public Const FFTxt54 As String = "FF_txt_54"
			 Public Const FFTxt55 As String = "FF_txt_55"
			 Public Const FFTxt56 As String = "FF_txt_56"
			 Public Const FFTxt57 As String = "FF_txt_57"
			 Public Const FFTxt58 As String = "FF_txt_58"
			 Public Const FFTxt59 As String = "FF_txt_59"
			 Public Const FFTxt60 As String = "FF_txt_60"
			 Public Const FFTxt61 As String = "FF_txt_61"
			 Public Const FFTxt62 As String = "FF_txt_62"
			 Public Const FFTxt63 As String = "FF_txt_63"
			 Public Const FFTxt64 As String = "FF_txt_64"
			 Public Const FFTxt65 As String = "FF_txt_65"
			 Public Const FFTxt66 As String = "FF_txt_66"
			 Public Const FFTxt67 As String = "FF_txt_67"
			 Public Const FFTxt68 As String = "FF_txt_68"
			 Public Const FFTxt69 As String = "FF_txt_69"
			 Public Const FFTxt70 As String = "FF_txt_70"
			 Public Const FFTxt71 As String = "FF_txt_71"
			 Public Const FFTxt72 As String = "FF_txt_72"
			 Public Const FFTxt73 As String = "FF_txt_73"
			 Public Const FFTxt74 As String = "FF_txt_74"
			 Public Const FFTxt75 As String = "FF_txt_75"
			 Public Const FFTxt76 As String = "FF_txt_76"
			 Public Const FFTxt77 As String = "FF_txt_77"
			 Public Const FFTxt78 As String = "FF_txt_78"
			 Public Const FFTxt79 As String = "FF_txt_79"
			 Public Const FFTxt80 As String = "FF_txt_80"
			 Public Const FFTxt81 As String = "FF_txt_81"
			 Public Const FFTxt82 As String = "FF_txt_82"
			 Public Const FFTxt83 As String = "FF_txt_83"
			 Public Const FFTxt84 As String = "FF_txt_84"
			 Public Const FFTxt85 As String = "FF_txt_85"
			 Public Const FFTxt86 As String = "FF_txt_86"
			 Public Const FFTxt87 As String = "FF_txt_87"
			 Public Const FFTxt88 As String = "FF_txt_88"
			 Public Const FFTxt89 As String = "FF_txt_89"
			 Public Const FFTxt90 As String = "FF_txt_90"
			 Public Const FFTxt91 As String = "FF_txt_91"
			 Public Const FFTxt92 As String = "FF_txt_92"
			 Public Const FFTxt93 As String = "FF_txt_93"
			 Public Const FFTxt94 As String = "FF_txt_94"
			 Public Const FFTxt95 As String = "FF_txt_95"
			 Public Const FFTxt96 As String = "FF_txt_96"
			 Public Const FFTxt97 As String = "FF_txt_97"
			 Public Const FFTxt98 As String = "FF_txt_98"
			 Public Const FFTxt99 As String = "FF_txt_99"
			 Public Const FFTxt100 As String = "FF_txt_100"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const InstructionDetailID As String = "InstructionDetailID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FFAmt1 As String = "FFAmt1"
			 Public Const FFAmt2 As String = "FFAmt2"
			 Public Const FFAmt3 As String = "FFAmt3"
			 Public Const FFAmt4 As String = "FFAmt4"
			 Public Const FFAmt5 As String = "FFAmt5"
			 Public Const FFAmt6 As String = "FFAmt6"
			 Public Const FFTxt1 As String = "FFTxt1"
			 Public Const FFTxt2 As String = "FFTxt2"
			 Public Const FFTxt3 As String = "FFTxt3"
			 Public Const FFTxt4 As String = "FFTxt4"
			 Public Const FFTxt5 As String = "FFTxt5"
			 Public Const FFTxt6 As String = "FFTxt6"
			 Public Const FFTxt7 As String = "FFTxt7"
			 Public Const FFTxt8 As String = "FFTxt8"
			 Public Const FFTxt9 As String = "FFTxt9"
			 Public Const FFTxt10 As String = "FFTxt10"
			 Public Const FFTxt11 As String = "FFTxt11"
			 Public Const FFTxt12 As String = "FFTxt12"
			 Public Const FFTxt13 As String = "FFTxt13"
			 Public Const FFTxt14 As String = "FFTxt14"
			 Public Const FFTxt15 As String = "FFTxt15"
			 Public Const FFTxt16 As String = "FFTxt16"
			 Public Const FFTxt17 As String = "FFTxt17"
			 Public Const FFTxt18 As String = "FFTxt18"
			 Public Const FFTxt19 As String = "FFTxt19"
			 Public Const FFTxt20 As String = "FFTxt20"
			 Public Const DetailAmount As String = "DetailAmount"
			 Public Const DetailBeneAccountNo As String = "DetailBeneAccountNo"
			 Public Const DetailBeneAddress As String = "DetailBeneAddress"
			 Public Const DetailBeneBankName As String = "DetailBeneBankName"
			 Public Const DetailBeneBranchAddress As String = "DetailBeneBranchAddress"
			 Public Const DetailBeneBranchCode As String = "DetailBeneBranchCode"
			 Public Const DetailBeneBranchName As String = "DetailBeneBranchName"
			 Public Const DetailBeneCNIC As String = "DetailBeneCNIC"
			 Public Const DetailBeneCountry As String = "DetailBeneCountry"
			 Public Const DetailBeneProvince As String = "DetailBeneProvince"
			 Public Const DetailBeneCity As String = "DetailBeneCity"
			 Public Const DetailBeneEmail As String = "DetailBeneEmail"
			 Public Const DetailBeneMobile As String = "DetailBeneMobile"
			 Public Const DetailBenePhone As String = "DetailBenePhone"
			 Public Const DetailInvoiceNo As String = "DetailInvoiceNo"
			 Public Const DetailISNO As String = "DetailISNO"
			 Public Const DetailTXNCODE As String = "DetailTXNCODE"
			 Public Const DetailDetailAmt As String = "DetailDetailAmt"
			 Public Const DetailFFText1 As String = "DetailFFText1"
			 Public Const DetailFFText2 As String = "DetailFFText2"
			 Public Const DetailFFText3 As String = "DetailFFText3"
			 Public Const DetailFFText4 As String = "DetailFFText4"
			 Public Const DetailFFText5 As String = "DetailFFText5"
			 Public Const DetailFFText6 As String = "DetailFFText6"
			 Public Const DetailFFText7 As String = "DetailFFText7"
			 Public Const DetailFFText8 As String = "DetailFFText8"
			 Public Const DetailFFText9 As String = "DetailFFText9"
			 Public Const DetailFFText10 As String = "DetailFFText10"
			 Public Const DetailFFText11 As String = "DetailFFText11"
			 Public Const DetailFFText12 As String = "DetailFFText12"
			 Public Const DetailFFText13 As String = "DetailFFText13"
			 Public Const DetailFFText14 As String = "DetailFFText14"
			 Public Const DetailFFText15 As String = "DetailFFText15"
			 Public Const DetailFFText16 As String = "DetailFFText16"
			 Public Const DetailFFText17 As String = "DetailFFText17"
			 Public Const DetailFFText18 As String = "DetailFFText18"
			 Public Const DetailFFText19 As String = "DetailFFText19"
			 Public Const DetailFFText20 As String = "DetailFFText20"
			 Public Const DetailFFAmt1 As String = "DetailFFAmt1"
			 Public Const DetailFFAmt2 As String = "DetailFFAmt2"
			 Public Const DetailFFAmt3 As String = "DetailFFAmt3"
			 Public Const DetailFFAmt4 As String = "DetailFFAmt4"
			 Public Const DetailFFAmt5 As String = "DetailFFAmt5"
			 Public Const DetailFFAmt6 As String = "DetailFFAmt6"
			 Public Const DetailFFAmt7 As String = "DetailFFAmt7"
			 Public Const DetailFFAmt8 As String = "DetailFFAmt8"
			 Public Const DetailFFAmt9 As String = "DetailFFAmt9"
			 Public Const DetailFFAmt10 As String = "DetailFFAmt10"
			 Public Const DetailFFAmt11 As String = "DetailFFAmt11"
			 Public Const DetailFFAmt12 As String = "DetailFFAmt12"
			 Public Const DetailFFAmt13 As String = "DetailFFAmt13"
			 Public Const DetailFFAmt14 As String = "DetailFFAmt14"
			 Public Const DetailFFAmt15 As String = "DetailFFAmt15"
			 Public Const DetailFFAmt16 As String = "DetailFFAmt16"
			 Public Const DetailFFAmt17 As String = "DetailFFAmt17"
			 Public Const DetailFFAmt18 As String = "DetailFFAmt18"
			 Public Const DetailFFAmt19 As String = "DetailFFAmt19"
			 Public Const DetailFFAmt20 As String = "DetailFFAmt20"
			 Public Const DetailFFAmt21 As String = "DetailFFAmt21"
			 Public Const DetailFFAmt22 As String = "DetailFFAmt22"
			 Public Const DetailFFAmt23 As String = "DetailFFAmt23"
			 Public Const DetailFFAmt24 As String = "DetailFFAmt24"
			 Public Const DetailFFAmt25 As String = "DetailFFAmt25"
			 Public Const DetailFFAmt26 As String = "DetailFFAmt26"
			 Public Const DetailFFAmt27 As String = "DetailFFAmt27"
			 Public Const DetailFFAmt28 As String = "DetailFFAmt28"
			 Public Const DetailFFAmt29 As String = "DetailFFAmt29"
			 Public Const DetailFFAmt30 As String = "DetailFFAmt30"
			 Public Const DetailFFAmt31 As String = "DetailFFAmt31"
			 Public Const DetailFFAmt32 As String = "DetailFFAmt32"
			 Public Const DetailFFAmt33 As String = "DetailFFAmt33"
			 Public Const DetailFFAmt34 As String = "DetailFFAmt34"
			 Public Const DetailFFAmt35 As String = "DetailFFAmt35"
			 Public Const DetailFFAmt36 As String = "DetailFFAmt36"
			 Public Const DetailFFAmt37 As String = "DetailFFAmt37"
			 Public Const DetailFFAmt38 As String = "DetailFFAmt38"
			 Public Const DetailFFAmt39 As String = "DetailFFAmt39"
			 Public Const DetailFFAmt40 As String = "DetailFFAmt40"
			 Public Const DetailFFAmt41 As String = "DetailFFAmt41"
			 Public Const DetailFFAmt42 As String = "DetailFFAmt42"
			 Public Const DetailFFAmt43 As String = "DetailFFAmt43"
			 Public Const DetailFFAmt44 As String = "DetailFFAmt44"
			 Public Const DetailFFAmt45 As String = "DetailFFAmt45"
			 Public Const DetailFFAmt46 As String = "DetailFFAmt46"
			 Public Const DetailFFAmt47 As String = "DetailFFAmt47"
			 Public Const DetailFFAmt48 As String = "DetailFFAmt48"
			 Public Const DetailFFAmt49 As String = "DetailFFAmt49"
			 Public Const DetailFFAmt50 As String = "DetailFFAmt50"
			 Public Const DetailFFText21 As String = "DetailFFText21"
			 Public Const DetailFFText22 As String = "DetailFFText22"
			 Public Const DetailFFText23 As String = "DetailFFText23"
			 Public Const DetailFFText24 As String = "DetailFFText24"
			 Public Const DetailFFText25 As String = "DetailFFText25"
			 Public Const DetailFFText26 As String = "DetailFFText26"
			 Public Const DetailFFText27 As String = "DetailFFText27"
			 Public Const DetailFFText28 As String = "DetailFFText28"
			 Public Const DetailFFText29 As String = "DetailFFText29"
			 Public Const DetailFFText30 As String = "DetailFFText30"
			 Public Const DetailFFText31 As String = "DetailFFText31"
			 Public Const DetailFFText32 As String = "DetailFFText32"
			 Public Const DetailFFText33 As String = "DetailFFText33"
			 Public Const DetailFFText34 As String = "DetailFFText34"
			 Public Const DetailFFText35 As String = "DetailFFText35"
			 Public Const DetailFFText36 As String = "DetailFFText36"
			 Public Const DetailFFText37 As String = "DetailFFText37"
			 Public Const DetailFFText38 As String = "DetailFFText38"
			 Public Const DetailFFText39 As String = "DetailFFText39"
			 Public Const DetailFFText40 As String = "DetailFFText40"
			 Public Const DetailFFText41 As String = "DetailFFText41"
			 Public Const DetailFFText42 As String = "DetailFFText42"
			 Public Const DetailFFText43 As String = "DetailFFText43"
			 Public Const DetailFFText44 As String = "DetailFFText44"
			 Public Const DetailFFText45 As String = "DetailFFText45"
			 Public Const DetailFFText46 As String = "DetailFFText46"
			 Public Const DetailFFText47 As String = "DetailFFText47"
			 Public Const DetailFFText48 As String = "DetailFFText48"
			 Public Const DetailFFText49 As String = "DetailFFText49"
			 Public Const DetailFFText50 As String = "DetailFFText50"
			 Public Const DetailFFText51 As String = "DetailFFText51"
			 Public Const DetailFFText52 As String = "DetailFFText52"
			 Public Const DetailFFText53 As String = "DetailFFText53"
			 Public Const DetailFFText54 As String = "DetailFFText54"
			 Public Const DetailFFText55 As String = "DetailFFText55"
			 Public Const DetailFFText56 As String = "DetailFFText56"
			 Public Const DetailFFText57 As String = "DetailFFText57"
			 Public Const DetailFFText58 As String = "DetailFFText58"
			 Public Const DetailFFText59 As String = "DetailFFText59"
			 Public Const DetailFFText60 As String = "DetailFFText60"
			 Public Const DetailFFText61 As String = "DetailFFText61"
			 Public Const DetailFFText62 As String = "DetailFFText62"
			 Public Const DetailFFText63 As String = "DetailFFText63"
			 Public Const DetailFFText64 As String = "DetailFFText64"
			 Public Const DetailFFText65 As String = "DetailFFText65"
			 Public Const DetailFFText66 As String = "DetailFFText66"
			 Public Const DetailFFText67 As String = "DetailFFText67"
			 Public Const DetailFFText68 As String = "DetailFFText68"
			 Public Const DetailFFText69 As String = "DetailFFText69"
			 Public Const DetailFFText70 As String = "DetailFFText70"
			 Public Const DetailFFText71 As String = "DetailFFText71"
			 Public Const DetailFFText72 As String = "DetailFFText72"
			 Public Const DetailFFText73 As String = "DetailFFText73"
			 Public Const DetailFFText74 As String = "DetailFFText74"
			 Public Const DetailFFText75 As String = "DetailFFText75"
			 Public Const DetailFFText76 As String = "DetailFFText76"
			 Public Const DetailFFText77 As String = "DetailFFText77"
			 Public Const DetailFFText78 As String = "DetailFFText78"
			 Public Const DetailFFText79 As String = "DetailFFText79"
			 Public Const DetailFFText80 As String = "DetailFFText80"
			 Public Const DetailFFText81 As String = "DetailFFText81"
			 Public Const DetailFFText82 As String = "DetailFFText82"
			 Public Const DetailFFText83 As String = "DetailFFText83"
			 Public Const DetailFFText84 As String = "DetailFFText84"
			 Public Const DetailFFText85 As String = "DetailFFText85"
			 Public Const DetailFFText86 As String = "DetailFFText86"
			 Public Const DetailFFText87 As String = "DetailFFText87"
			 Public Const DetailFFText88 As String = "DetailFFText88"
			 Public Const DetailFFText89 As String = "DetailFFText89"
			 Public Const DetailFFText90 As String = "DetailFFText90"
			 Public Const DetailFFText91 As String = "DetailFFText91"
			 Public Const DetailFFText92 As String = "DetailFFText92"
			 Public Const DetailFFText93 As String = "DetailFFText93"
			 Public Const DetailFFText94 As String = "DetailFFText94"
			 Public Const DetailFFText95 As String = "DetailFFText95"
			 Public Const DetailFFText96 As String = "DetailFFText96"
			 Public Const DetailFFText97 As String = "DetailFFText97"
			 Public Const DetailFFText98 As String = "DetailFFText98"
			 Public Const DetailFFText99 As String = "DetailFFText99"
			 Public Const DetailFFText100 As String = "DetailFFText100"
			 Public Const FFAmt7 As String = "FFAmt7"
			 Public Const FFAmt8 As String = "FFAmt8"
			 Public Const FFAmt9 As String = "FFAmt9"
			 Public Const FFAmt10 As String = "FFAmt10"
			 Public Const FFAmt11 As String = "FFAmt11"
			 Public Const FFAmt12 As String = "FFAmt12"
			 Public Const FFAmt13 As String = "FFAmt13"
			 Public Const FFAmt14 As String = "FFAmt14"
			 Public Const FFAmt15 As String = "FFAmt15"
			 Public Const FFAmt16 As String = "FFAmt16"
			 Public Const FFAmt17 As String = "FFAmt17"
			 Public Const FFAmt18 As String = "FFAmt18"
			 Public Const FFAmt19 As String = "FFAmt19"
			 Public Const FFAmt20 As String = "FFAmt20"
			 Public Const FFAmt21 As String = "FFAmt21"
			 Public Const FFAmt22 As String = "FFAmt22"
			 Public Const FFAmt23 As String = "FFAmt23"
			 Public Const FFAmt24 As String = "FFAmt24"
			 Public Const FFAmt25 As String = "FFAmt25"
			 Public Const FFAmt26 As String = "FFAmt26"
			 Public Const FFAmt27 As String = "FFAmt27"
			 Public Const FFAmt28 As String = "FFAmt28"
			 Public Const FFAmt29 As String = "FFAmt29"
			 Public Const FFAmt30 As String = "FFAmt30"
			 Public Const FFAmt31 As String = "FFAmt31"
			 Public Const FFAmt32 As String = "FFAmt32"
			 Public Const FFAmt33 As String = "FFAmt33"
			 Public Const FFAmt34 As String = "FFAmt34"
			 Public Const FFAmt35 As String = "FFAmt35"
			 Public Const FFAmt36 As String = "FFAmt36"
			 Public Const FFAmt37 As String = "FFAmt37"
			 Public Const FFAmt38 As String = "FFAmt38"
			 Public Const FFAmt39 As String = "FFAmt39"
			 Public Const FFAmt40 As String = "FFAmt40"
			 Public Const FFAmt41 As String = "FFAmt41"
			 Public Const FFAmt42 As String = "FFAmt42"
			 Public Const FFAmt43 As String = "FFAmt43"
			 Public Const FFAmt44 As String = "FFAmt44"
			 Public Const FFAmt45 As String = "FFAmt45"
			 Public Const FFAmt46 As String = "FFAmt46"
			 Public Const FFAmt47 As String = "FFAmt47"
			 Public Const FFAmt48 As String = "FFAmt48"
			 Public Const FFAmt49 As String = "FFAmt49"
			 Public Const FFAmt50 As String = "FFAmt50"
			 Public Const FFTxt21 As String = "FFTxt21"
			 Public Const FFTxt22 As String = "FFTxt22"
			 Public Const FFTxt23 As String = "FFTxt23"
			 Public Const FFTxt24 As String = "FFTxt24"
			 Public Const FFTxt25 As String = "FFTxt25"
			 Public Const FFTxt26 As String = "FFTxt26"
			 Public Const FFTxt27 As String = "FFTxt27"
			 Public Const FFTxt28 As String = "FFTxt28"
			 Public Const FFTxt29 As String = "FFTxt29"
			 Public Const FFTxt30 As String = "FFTxt30"
			 Public Const FFTxt31 As String = "FFTxt31"
			 Public Const FFTxt32 As String = "FFTxt32"
			 Public Const FFTxt33 As String = "FFTxt33"
			 Public Const FFTxt34 As String = "FFTxt34"
			 Public Const FFTxt35 As String = "FFTxt35"
			 Public Const FFTxt36 As String = "FFTxt36"
			 Public Const FFTxt37 As String = "FFTxt37"
			 Public Const FFTxt38 As String = "FFTxt38"
			 Public Const FFTxt39 As String = "FFTxt39"
			 Public Const FFTxt40 As String = "FFTxt40"
			 Public Const FFTxt41 As String = "FFTxt41"
			 Public Const FFTxt42 As String = "FFTxt42"
			 Public Const FFTxt43 As String = "FFTxt43"
			 Public Const FFTxt44 As String = "FFTxt44"
			 Public Const FFTxt45 As String = "FFTxt45"
			 Public Const FFTxt46 As String = "FFTxt46"
			 Public Const FFTxt47 As String = "FFTxt47"
			 Public Const FFTxt48 As String = "FFTxt48"
			 Public Const FFTxt49 As String = "FFTxt49"
			 Public Const FFTxt50 As String = "FFTxt50"
			 Public Const FFTxt51 As String = "FFTxt51"
			 Public Const FFTxt52 As String = "FFTxt52"
			 Public Const FFTxt53 As String = "FFTxt53"
			 Public Const FFTxt54 As String = "FFTxt54"
			 Public Const FFTxt55 As String = "FFTxt55"
			 Public Const FFTxt56 As String = "FFTxt56"
			 Public Const FFTxt57 As String = "FFTxt57"
			 Public Const FFTxt58 As String = "FFTxt58"
			 Public Const FFTxt59 As String = "FFTxt59"
			 Public Const FFTxt60 As String = "FFTxt60"
			 Public Const FFTxt61 As String = "FFTxt61"
			 Public Const FFTxt62 As String = "FFTxt62"
			 Public Const FFTxt63 As String = "FFTxt63"
			 Public Const FFTxt64 As String = "FFTxt64"
			 Public Const FFTxt65 As String = "FFTxt65"
			 Public Const FFTxt66 As String = "FFTxt66"
			 Public Const FFTxt67 As String = "FFTxt67"
			 Public Const FFTxt68 As String = "FFTxt68"
			 Public Const FFTxt69 As String = "FFTxt69"
			 Public Const FFTxt70 As String = "FFTxt70"
			 Public Const FFTxt71 As String = "FFTxt71"
			 Public Const FFTxt72 As String = "FFTxt72"
			 Public Const FFTxt73 As String = "FFTxt73"
			 Public Const FFTxt74 As String = "FFTxt74"
			 Public Const FFTxt75 As String = "FFTxt75"
			 Public Const FFTxt76 As String = "FFTxt76"
			 Public Const FFTxt77 As String = "FFTxt77"
			 Public Const FFTxt78 As String = "FFTxt78"
			 Public Const FFTxt79 As String = "FFTxt79"
			 Public Const FFTxt80 As String = "FFTxt80"
			 Public Const FFTxt81 As String = "FFTxt81"
			 Public Const FFTxt82 As String = "FFTxt82"
			 Public Const FFTxt83 As String = "FFTxt83"
			 Public Const FFTxt84 As String = "FFTxt84"
			 Public Const FFTxt85 As String = "FFTxt85"
			 Public Const FFTxt86 As String = "FFTxt86"
			 Public Const FFTxt87 As String = "FFTxt87"
			 Public Const FFTxt88 As String = "FFTxt88"
			 Public Const FFTxt89 As String = "FFTxt89"
			 Public Const FFTxt90 As String = "FFTxt90"
			 Public Const FFTxt91 As String = "FFTxt91"
			 Public Const FFTxt92 As String = "FFTxt92"
			 Public Const FFTxt93 As String = "FFTxt93"
			 Public Const FFTxt94 As String = "FFTxt94"
			 Public Const FFTxt95 As String = "FFTxt95"
			 Public Const FFTxt96 As String = "FFTxt96"
			 Public Const FFTxt97 As String = "FFTxt97"
			 Public Const FFTxt98 As String = "FFTxt98"
			 Public Const FFTxt99 As String = "FFTxt99"
			 Public Const FFTxt100 As String = "FFTxt100"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstructionDetailMetadata)
			
				If ICInstructionDetailMetadata.mapDelegates Is Nothing Then
					ICInstructionDetailMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstructionDetailMetadata._meta Is Nothing Then
					ICInstructionDetailMetadata._meta = New ICInstructionDetailMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("InstructionDetailID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FFAmt1", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt2", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt3", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt4", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt5", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt6", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFTxt1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt3", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt4", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt5", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt6", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt7", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt8", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt9", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt10", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt11", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt12", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt13", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt14", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt15", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt16", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt17", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt18", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt19", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt20", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailAmount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailBeneAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneBankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneBranchAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneBranchName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneCNIC", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneCountry", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneProvince", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneCity", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneEmail", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBeneMobile", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailBenePhone", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailInvoiceNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailISNO", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DetailTXNCODE", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DetailDetailAmt", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFText1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText3", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText4", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText5", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText6", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText7", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText8", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText9", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText10", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText11", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText12", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText13", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText14", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText15", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText16", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText17", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText18", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText19", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText20", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFAmt1", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt2", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt3", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt4", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt5", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt6", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt7", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt8", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt9", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt10", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt11", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt12", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt13", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt14", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt15", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt16", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt17", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt18", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt19", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt20", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt21", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt22", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt23", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt24", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt25", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt26", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt27", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt28", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt29", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt30", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt31", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt32", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt33", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt34", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt35", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt36", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt37", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt38", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt39", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt40", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt41", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt42", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt43", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt44", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt45", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt46", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt47", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt48", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt49", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFAmt50", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DetailFFText21", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText22", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText23", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText24", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText25", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText26", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText27", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText28", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText29", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText30", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText31", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText32", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText33", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText34", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText35", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText36", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText37", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText38", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText39", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText40", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText41", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText42", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText43", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText44", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText45", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText46", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText47", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText48", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText49", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText50", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText51", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText52", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText53", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText54", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText55", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText56", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText57", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText58", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText59", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText60", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText61", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText62", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText63", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText64", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText65", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText66", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText67", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText68", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText69", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText70", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText71", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText72", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText73", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText74", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText75", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText76", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText77", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText78", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText79", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText80", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText81", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText82", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText83", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText84", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText85", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText86", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText87", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText88", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText89", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText90", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText91", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText92", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText93", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText94", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText95", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText96", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText97", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText98", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText99", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailFFText100", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFAmt7", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt8", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt9", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt10", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt11", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt12", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt13", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt14", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt15", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt16", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt17", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt18", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt19", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt20", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt21", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt22", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt23", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt24", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt25", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt26", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt27", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt28", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt29", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt30", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt31", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt32", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt33", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt34", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt35", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt36", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt37", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt38", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt39", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt40", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt41", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt42", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt43", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt44", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt45", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt46", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt47", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt48", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt49", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFAmt50", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("FFTxt21", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt22", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt23", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt24", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt25", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt26", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt27", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt28", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt29", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt30", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt31", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt32", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt33", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt34", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt35", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt36", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt37", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt38", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt39", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt40", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt41", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt42", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt43", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt44", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt45", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt46", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt47", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt48", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt49", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt50", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt51", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt52", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt53", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt54", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt55", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt56", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt57", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt58", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt59", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt60", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt61", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt62", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt63", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt64", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt65", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt66", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt67", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt68", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt69", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt70", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt71", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt72", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt73", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt74", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt75", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt76", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt77", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt78", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt79", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt80", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt81", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt82", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt83", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt84", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt85", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt86", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt87", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt88", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt89", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt90", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt91", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt92", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt93", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt94", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt95", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt96", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt97", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt98", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt99", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FFTxt100", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_InstructionDetail"
				meta.Destination = "IC_InstructionDetail"
				
				meta.spInsert = "proc_IC_InstructionDetailInsert"
				meta.spUpdate = "proc_IC_InstructionDetailUpdate"
				meta.spDelete = "proc_IC_InstructionDetailDelete"
				meta.spLoadAll = "proc_IC_InstructionDetailLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstructionDetailLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstructionDetailMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
