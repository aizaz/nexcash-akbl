
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 27-Jun-2014 1:13:40 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_FirstLegReversedInstructions' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFirstLegReversedInstructions))> _
	<XmlType("ICFirstLegReversedInstructions")> _	
	Partial Public Class ICFirstLegReversedInstructions 
		Inherits esICFirstLegReversedInstructions
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFirstLegReversedInstructions()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal firstLegReversedID As System.Int32)
			Dim obj As New ICFirstLegReversedInstructions()
			obj.FirstLegReversedID = firstLegReversedID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal firstLegReversedID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFirstLegReversedInstructions()
			obj.FirstLegReversedID = firstLegReversedID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFirstLegReversedInstructionsCollection")> _
	Partial Public Class ICFirstLegReversedInstructionsCollection
		Inherits esICFirstLegReversedInstructionsCollection
		Implements IEnumerable(Of ICFirstLegReversedInstructions)
	
		Public Function FindByPrimaryKey(ByVal firstLegReversedID As System.Int32) As ICFirstLegReversedInstructions
			Return MyBase.SingleOrDefault(Function(e) e.FirstLegReversedID.HasValue AndAlso e.FirstLegReversedID.Value = firstLegReversedID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFirstLegReversedInstructions))> _
		Public Class ICFirstLegReversedInstructionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFirstLegReversedInstructionsCollection)
			
			Public Shared Widening Operator CType(packet As ICFirstLegReversedInstructionsCollectionWCFPacket) As ICFirstLegReversedInstructionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFirstLegReversedInstructionsCollection) As ICFirstLegReversedInstructionsCollectionWCFPacket
				Return New ICFirstLegReversedInstructionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFirstLegReversedInstructionsQuery 
		Inherits esICFirstLegReversedInstructionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFirstLegReversedInstructionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFirstLegReversedInstructionsQuery) As String
			Return ICFirstLegReversedInstructionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFirstLegReversedInstructionsQuery
			Return DirectCast(ICFirstLegReversedInstructionsQuery.SerializeHelper.FromXml(query, GetType(ICFirstLegReversedInstructionsQuery)), ICFirstLegReversedInstructionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFirstLegReversedInstructions
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal firstLegReversedID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(firstLegReversedID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(firstLegReversedID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal firstLegReversedID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(firstLegReversedID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(firstLegReversedID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal firstLegReversedID As System.Int32) As Boolean
		
			Dim query As New ICFirstLegReversedInstructionsQuery()
			query.Where(query.FirstLegReversedID = firstLegReversedID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal firstLegReversedID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FirstLegReversedID", firstLegReversedID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FirstLegReversedID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FirstLegReversedID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.FirstLegReversedID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.FirstLegReversedID, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FirstLegReversedID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.RelatedID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.RelatedID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.RelatedID, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.RelatedID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.EntityType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EntityType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.EntityType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.EntityType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.EntityType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICFirstLegReversedInstructionsMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICFirstLegReversedInstructionsMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountNo, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountBranchCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountCurrency, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountProductCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountProductCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountProductCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountProductCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountProductCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountSchemeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountSchemeCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountSchemeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountSchemeCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountSchemeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.FromAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountNo, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountBranchCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountCurrency, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountProductCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountProductCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountProductCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountProductCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountProductCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ToAccountSchemeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountSchemeCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountSchemeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountSchemeCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountSchemeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.RBIndicator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RBIndicator As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.RBIndicator)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.RBIndicator, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.RBIndicator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.TxnType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TxnType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.TxnType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.TxnType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.TxnType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.VoucherType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property VoucherType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.VoucherType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.VoucherType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.VoucherType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.InstrumentType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.InstrumentType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.DebitGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.DebitGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.DebitGLCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.DebitGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.CreditGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreditGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreditGLCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.CreditGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.InstrumentNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentNo As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentNo, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.InstrumentNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.IsReversal
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReversal As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsReversal)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsReversal, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.IsReversal)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.TransactionBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.TransactionBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.TransactionBranchCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.TransactionBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.Creator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creator As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.Creator)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFirstLegReversedInstructionsMetadata.ColumnNames.Creator, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.Creator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ErrorMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ErrorMessage As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ErrorMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ErrorMessage, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ErrorMessage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.IsFirstLEgReversed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsFirstLEgReversed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsFirstLEgReversed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsFirstLEgReversed, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.IsFirstLEgReversed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.LastStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastStatus As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.LastStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.LastStatus, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.LastStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FirstLegReversedInstructions.ReversalType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReversalType As System.String
			Get
				Return MyBase.GetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ReversalType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFirstLegReversedInstructionsMetadata.ColumnNames.ReversalType, value) Then
					OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.ReversalType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FirstLegReversedID"
							Me.str().FirstLegReversedID = CType(value, string)
												
						Case "RelatedID"
							Me.str().RelatedID = CType(value, string)
												
						Case "EntityType"
							Me.str().EntityType = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
												
						Case "FromAccountNo"
							Me.str().FromAccountNo = CType(value, string)
												
						Case "FromAccountBranchCode"
							Me.str().FromAccountBranchCode = CType(value, string)
												
						Case "FromAccountCurrency"
							Me.str().FromAccountCurrency = CType(value, string)
												
						Case "FromAccountProductCode"
							Me.str().FromAccountProductCode = CType(value, string)
												
						Case "FromAccountSchemeCode"
							Me.str().FromAccountSchemeCode = CType(value, string)
												
						Case "FromAccountType"
							Me.str().FromAccountType = CType(value, string)
												
						Case "ToAccountNo"
							Me.str().ToAccountNo = CType(value, string)
												
						Case "ToAccountBranchCode"
							Me.str().ToAccountBranchCode = CType(value, string)
												
						Case "ToAccountCurrency"
							Me.str().ToAccountCurrency = CType(value, string)
												
						Case "ToAccountType"
							Me.str().ToAccountType = CType(value, string)
												
						Case "ToAccountProductCode"
							Me.str().ToAccountProductCode = CType(value, string)
												
						Case "ToAccountSchemeCode"
							Me.str().ToAccountSchemeCode = CType(value, string)
												
						Case "RBIndicator"
							Me.str().RBIndicator = CType(value, string)
												
						Case "TxnType"
							Me.str().TxnType = CType(value, string)
												
						Case "VoucherType"
							Me.str().VoucherType = CType(value, string)
												
						Case "InstrumentType"
							Me.str().InstrumentType = CType(value, string)
												
						Case "DebitGLCode"
							Me.str().DebitGLCode = CType(value, string)
												
						Case "CreditGLCode"
							Me.str().CreditGLCode = CType(value, string)
												
						Case "InstrumentNo"
							Me.str().InstrumentNo = CType(value, string)
												
						Case "IsReversal"
							Me.str().IsReversal = CType(value, string)
												
						Case "TransactionBranchCode"
							Me.str().TransactionBranchCode = CType(value, string)
												
						Case "Creator"
							Me.str().Creator = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "ErrorMessage"
							Me.str().ErrorMessage = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "IsFirstLEgReversed"
							Me.str().IsFirstLEgReversed = CType(value, string)
												
						Case "LastStatus"
							Me.str().LastStatus = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "ReversalType"
							Me.str().ReversalType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FirstLegReversedID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FirstLegReversedID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.FirstLegReversedID)
							End If
						
						Case "RelatedID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RelatedID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.RelatedID)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.Amount)
							End If
						
						Case "Creator"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creator = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.Creator)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsFirstLEgReversed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsFirstLEgReversed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFirstLegReversedInstructionsMetadata.PropertyNames.IsFirstLEgReversed)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFirstLegReversedInstructions)
				Me.entity = entity
			End Sub				
		
	
			Public Property FirstLegReversedID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FirstLegReversedID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FirstLegReversedID = Nothing
					Else
						entity.FirstLegReversedID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RelatedID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedID = Nothing
					Else
						entity.RelatedID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property EntityType As System.String 
				Get
					Dim data_ As System.String = entity.EntityType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EntityType = Nothing
					Else
						entity.EntityType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNo = Nothing
					Else
						entity.FromAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountBranchCode = Nothing
					Else
						entity.FromAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountCurrency = Nothing
					Else
						entity.FromAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountProductCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountProductCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountProductCode = Nothing
					Else
						entity.FromAccountProductCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountSchemeCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountSchemeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountSchemeCode = Nothing
					Else
						entity.FromAccountSchemeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountType As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountType = Nothing
					Else
						entity.FromAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNo = Nothing
					Else
						entity.ToAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountBranchCode = Nothing
					Else
						entity.ToAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountCurrency = Nothing
					Else
						entity.ToAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountType = Nothing
					Else
						entity.ToAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountProductCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountProductCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountProductCode = Nothing
					Else
						entity.ToAccountProductCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountSchemeCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountSchemeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountSchemeCode = Nothing
					Else
						entity.ToAccountSchemeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RBIndicator As System.String 
				Get
					Dim data_ As System.String = entity.RBIndicator
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RBIndicator = Nothing
					Else
						entity.RBIndicator = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TxnType As System.String 
				Get
					Dim data_ As System.String = entity.TxnType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TxnType = Nothing
					Else
						entity.TxnType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property VoucherType As System.String 
				Get
					Dim data_ As System.String = entity.VoucherType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.VoucherType = Nothing
					Else
						entity.VoucherType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentType As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentType = Nothing
					Else
						entity.InstrumentType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitGLCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitGLCode = Nothing
					Else
						entity.DebitGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditGLCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditGLCode = Nothing
					Else
						entity.CreditGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentNo As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentNo = Nothing
					Else
						entity.InstrumentNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReversal As System.String 
				Get
					Dim data_ As System.String = entity.IsReversal
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReversal = Nothing
					Else
						entity.IsReversal = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.TransactionBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionBranchCode = Nothing
					Else
						entity.TransactionBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creator As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creator
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creator = Nothing
					Else
						entity.Creator = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ErrorMessage As System.String 
				Get
					Dim data_ As System.String = entity.ErrorMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ErrorMessage = Nothing
					Else
						entity.ErrorMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As System.String = entity.Status
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsFirstLEgReversed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsFirstLEgReversed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsFirstLEgReversed = Nothing
					Else
						entity.IsFirstLEgReversed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastStatus As System.String 
				Get
					Dim data_ As System.String = entity.LastStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastStatus = Nothing
					Else
						entity.LastStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReversalType As System.String 
				Get
					Dim data_ As System.String = entity.ReversalType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReversalType = Nothing
					Else
						entity.ReversalType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFirstLegReversedInstructions
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFirstLegReversedInstructionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFirstLegReversedInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFirstLegReversedInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFirstLegReversedInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFirstLegReversedInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFirstLegReversedInstructionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFirstLegReversedInstructionsCollection
		Inherits esEntityCollection(Of ICFirstLegReversedInstructions)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFirstLegReversedInstructionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFirstLegReversedInstructionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFirstLegReversedInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFirstLegReversedInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFirstLegReversedInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFirstLegReversedInstructionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFirstLegReversedInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFirstLegReversedInstructionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFirstLegReversedInstructionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFirstLegReversedInstructionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFirstLegReversedInstructionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FirstLegReversedID" 
					Return Me.FirstLegReversedID
				Case "RelatedID" 
					Return Me.RelatedID
				Case "EntityType" 
					Return Me.EntityType
				Case "Amount" 
					Return Me.Amount
				Case "FromAccountNo" 
					Return Me.FromAccountNo
				Case "FromAccountBranchCode" 
					Return Me.FromAccountBranchCode
				Case "FromAccountCurrency" 
					Return Me.FromAccountCurrency
				Case "FromAccountProductCode" 
					Return Me.FromAccountProductCode
				Case "FromAccountSchemeCode" 
					Return Me.FromAccountSchemeCode
				Case "FromAccountType" 
					Return Me.FromAccountType
				Case "ToAccountNo" 
					Return Me.ToAccountNo
				Case "ToAccountBranchCode" 
					Return Me.ToAccountBranchCode
				Case "ToAccountCurrency" 
					Return Me.ToAccountCurrency
				Case "ToAccountType" 
					Return Me.ToAccountType
				Case "ToAccountProductCode" 
					Return Me.ToAccountProductCode
				Case "ToAccountSchemeCode" 
					Return Me.ToAccountSchemeCode
				Case "RBIndicator" 
					Return Me.RBIndicator
				Case "TxnType" 
					Return Me.TxnType
				Case "VoucherType" 
					Return Me.VoucherType
				Case "InstrumentType" 
					Return Me.InstrumentType
				Case "DebitGLCode" 
					Return Me.DebitGLCode
				Case "CreditGLCode" 
					Return Me.CreditGLCode
				Case "InstrumentNo" 
					Return Me.InstrumentNo
				Case "IsReversal" 
					Return Me.IsReversal
				Case "TransactionBranchCode" 
					Return Me.TransactionBranchCode
				Case "Creator" 
					Return Me.Creator
				Case "CreationDate" 
					Return Me.CreationDate
				Case "ErrorMessage" 
					Return Me.ErrorMessage
				Case "Status" 
					Return Me.Status
				Case "IsFirstLEgReversed" 
					Return Me.IsFirstLEgReversed
				Case "LastStatus" 
					Return Me.LastStatus
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "ReversalType" 
					Return Me.ReversalType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FirstLegReversedID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FirstLegReversedID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.RelatedID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property EntityType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.EntityType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountProductCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountProductCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountSchemeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountSchemeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountProductCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountProductCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountSchemeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountSchemeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RBIndicator As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.RBIndicator, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TxnType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.TxnType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property VoucherType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.VoucherType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.DebitGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.CreditGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsReversal As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.IsReversal, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.TransactionBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creator As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.Creator, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ErrorMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ErrorMessage, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.Status, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsFirstLEgReversed As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.IsFirstLEgReversed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property LastStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.LastStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReversalType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFirstLegReversedInstructionsMetadata.ColumnNames.ReversalType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFirstLegReversedInstructions 
		Inherits esICFirstLegReversedInstructions
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICFirstLegReversedInstructionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FirstLegReversedID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FirstLegReversedID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.RelatedID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.RelatedID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.EntityType, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.EntityType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.Amount, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountNo, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountBranchCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountCurrency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountProductCode, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountProductCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountSchemeCode, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountSchemeCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.FromAccountType, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.FromAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountNo, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountBranchCode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountCurrency, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountType, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountProductCode, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountProductCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ToAccountSchemeCode, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ToAccountSchemeCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.RBIndicator, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.RBIndicator
			c.CharacterMaxLength = 5
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.TxnType, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.TxnType
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.VoucherType, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.VoucherType
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentType, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.InstrumentType
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.DebitGLCode, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.DebitGLCode
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreditGLCode, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.CreditGLCode
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.InstrumentNo, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.InstrumentNo
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsReversal, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.IsReversal
			c.CharacterMaxLength = 5
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.TransactionBranchCode, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.TransactionBranchCode
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.Creator, 25, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.Creator
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.CreationDate, 26, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ErrorMessage, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ErrorMessage
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.Status, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.Status
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.IsFirstLEgReversed, 29, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.IsFirstLEgReversed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.LastStatus, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.LastStatus
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ProductTypeCode, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFirstLegReversedInstructionsMetadata.ColumnNames.ReversalType, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFirstLegReversedInstructionsMetadata.PropertyNames.ReversalType
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFirstLegReversedInstructionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FirstLegReversedID As String = "FirstLegReversedID"
			 Public Const RelatedID As String = "RelatedID"
			 Public Const EntityType As String = "EntityType"
			 Public Const Amount As String = "Amount"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBranchCode As String = "FromAccountBranchCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const FromAccountProductCode As String = "FromAccountProductCode"
			 Public Const FromAccountSchemeCode As String = "FromAccountSchemeCode"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBranchCode As String = "ToAccountBranchCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const ToAccountProductCode As String = "ToAccountProductCode"
			 Public Const ToAccountSchemeCode As String = "ToAccountSchemeCode"
			 Public Const RBIndicator As String = "RBIndicator"
			 Public Const TxnType As String = "TxnType"
			 Public Const VoucherType As String = "VoucherType"
			 Public Const InstrumentType As String = "InstrumentType"
			 Public Const DebitGLCode As String = "DebitGLCode"
			 Public Const CreditGLCode As String = "CreditGLCode"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const IsReversal As String = "IsReversal"
			 Public Const TransactionBranchCode As String = "TransactionBranchCode"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const Status As String = "Status"
			 Public Const IsFirstLEgReversed As String = "IsFirstLEgReversed"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ReversalType As String = "ReversalType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FirstLegReversedID As String = "FirstLegReversedID"
			 Public Const RelatedID As String = "RelatedID"
			 Public Const EntityType As String = "EntityType"
			 Public Const Amount As String = "Amount"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBranchCode As String = "FromAccountBranchCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const FromAccountProductCode As String = "FromAccountProductCode"
			 Public Const FromAccountSchemeCode As String = "FromAccountSchemeCode"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBranchCode As String = "ToAccountBranchCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const ToAccountProductCode As String = "ToAccountProductCode"
			 Public Const ToAccountSchemeCode As String = "ToAccountSchemeCode"
			 Public Const RBIndicator As String = "RBIndicator"
			 Public Const TxnType As String = "TxnType"
			 Public Const VoucherType As String = "VoucherType"
			 Public Const InstrumentType As String = "InstrumentType"
			 Public Const DebitGLCode As String = "DebitGLCode"
			 Public Const CreditGLCode As String = "CreditGLCode"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const IsReversal As String = "IsReversal"
			 Public Const TransactionBranchCode As String = "TransactionBranchCode"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const Status As String = "Status"
			 Public Const IsFirstLEgReversed As String = "IsFirstLEgReversed"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ReversalType As String = "ReversalType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFirstLegReversedInstructionsMetadata)
			
				If ICFirstLegReversedInstructionsMetadata.mapDelegates Is Nothing Then
					ICFirstLegReversedInstructionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFirstLegReversedInstructionsMetadata._meta Is Nothing Then
					ICFirstLegReversedInstructionsMetadata._meta = New ICFirstLegReversedInstructionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FirstLegReversedID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RelatedID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("EntityType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Amount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("FromAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountProductCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountSchemeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountProductCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountSchemeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RBIndicator", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TxnType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("VoucherType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InstrumentType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InstrumentNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsReversal", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TransactionBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creator", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ErrorMessage", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsFirstLEgReversed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("LastStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReversalType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_FirstLegReversedInstructions"
				meta.Destination = "IC_FirstLegReversedInstructions"
				
				meta.spInsert = "proc_IC_FirstLegReversedInstructionsInsert"
				meta.spUpdate = "proc_IC_FirstLegReversedInstructionsUpdate"
				meta.spDelete = "proc_IC_FirstLegReversedInstructionsDelete"
				meta.spLoadAll = "proc_IC_FirstLegReversedInstructionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FirstLegReversedInstructionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFirstLegReversedInstructionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
