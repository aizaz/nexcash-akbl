
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:53 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountsPaymentNatureProductTypeTemplates' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountsPaymentNatureProductTypeTemplates))> _
	<XmlType("ICAccountsPaymentNatureProductTypeTemplates")> _	
	Partial Public Class ICAccountsPaymentNatureProductTypeTemplates 
		Inherits esICAccountsPaymentNatureProductTypeTemplates
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountsPaymentNatureProductTypeTemplates()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal aPNPTTemplateID As System.Int32)
			Dim obj As New ICAccountsPaymentNatureProductTypeTemplates()
			obj.APNPTTemplateID = aPNPTTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal aPNPTTemplateID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountsPaymentNatureProductTypeTemplates()
			obj.APNPTTemplateID = aPNPTTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountsPaymentNatureProductTypeTemplatesCollection")> _
	Partial Public Class ICAccountsPaymentNatureProductTypeTemplatesCollection
		Inherits esICAccountsPaymentNatureProductTypeTemplatesCollection
		Implements IEnumerable(Of ICAccountsPaymentNatureProductTypeTemplates)
	
		Public Function FindByPrimaryKey(ByVal aPNPTTemplateID As System.Int32) As ICAccountsPaymentNatureProductTypeTemplates
			Return MyBase.SingleOrDefault(Function(e) e.APNPTTemplateID.HasValue AndAlso e.APNPTTemplateID.Value = aPNPTTemplateID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountsPaymentNatureProductTypeTemplates))> _
		Public Class ICAccountsPaymentNatureProductTypeTemplatesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountsPaymentNatureProductTypeTemplatesCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountsPaymentNatureProductTypeTemplatesCollectionWCFPacket) As ICAccountsPaymentNatureProductTypeTemplatesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountsPaymentNatureProductTypeTemplatesCollection) As ICAccountsPaymentNatureProductTypeTemplatesCollectionWCFPacket
				Return New ICAccountsPaymentNatureProductTypeTemplatesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountsPaymentNatureProductTypeTemplatesQuery 
		Inherits esICAccountsPaymentNatureProductTypeTemplatesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountsPaymentNatureProductTypeTemplatesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountsPaymentNatureProductTypeTemplatesQuery) As String
			Return ICAccountsPaymentNatureProductTypeTemplatesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountsPaymentNatureProductTypeTemplatesQuery
			Return DirectCast(ICAccountsPaymentNatureProductTypeTemplatesQuery.SerializeHelper.FromXml(query, GetType(ICAccountsPaymentNatureProductTypeTemplatesQuery)), ICAccountsPaymentNatureProductTypeTemplatesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductTypeTemplates
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal aPNPTTemplateID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(aPNPTTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(aPNPTTemplateID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal aPNPTTemplateID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(aPNPTTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(aPNPTTemplateID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal aPNPTTemplateID As System.Int32) As Boolean
		
			Dim query As New ICAccountsPaymentNatureProductTypeTemplatesQuery()
			query.Where(query.APNPTTemplateID = aPNPTTemplateID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal aPNPTTemplateID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("APNPTTemplateID", aPNPTTemplateID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.ProductCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.ProductCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.ProductCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.ProductCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.APNPTTemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property APNPTTemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.APNPTTemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.APNPTTemplateID, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.APNPTTemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.TemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.TemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.TemplateID, value) Then
					Me._UpToICTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICTemplateByTemplateID")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.TemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductTypeTemplates.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICTemplateByTemplateID As ICTemplate
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "ProductCode"
							Me.str().ProductCode = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "APNPTTemplateID"
							Me.str().APNPTTemplateID = CType(value, string)
												
						Case "TemplateID"
							Me.str().TemplateID = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "APNPTTemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.APNPTTemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.APNPTTemplateID)
							End If
						
						Case "TemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.TemplateID)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountsPaymentNatureProductTypeTemplates)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductCode = Nothing
					Else
						entity.ProductCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property APNPTTemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.APNPTTemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.APNPTTemplateID = Nothing
					Else
						entity.APNPTTemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateID = Nothing
					Else
						entity.TemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountsPaymentNatureProductTypeTemplates
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeTemplatesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountsPaymentNatureProductTypeTemplatesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureProductTypeTemplatesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureProductTypeTemplatesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureProductTypeTemplatesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountsPaymentNatureProductTypeTemplatesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductTypeTemplatesCollection
		Inherits esEntityCollection(Of ICAccountsPaymentNatureProductTypeTemplates)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeTemplatesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountsPaymentNatureProductTypeTemplatesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountsPaymentNatureProductTypeTemplatesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureProductTypeTemplatesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureProductTypeTemplatesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountsPaymentNatureProductTypeTemplatesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureProductTypeTemplatesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountsPaymentNatureProductTypeTemplatesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountsPaymentNatureProductTypeTemplatesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductTypeTemplatesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeTemplatesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "ProductCode" 
					Return Me.ProductCode
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "APNPTTemplateID" 
					Return Me.APNPTTemplateID
				Case "TemplateID" 
					Return Me.TemplateID
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.ProductCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property APNPTTemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.APNPTTemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.TemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountsPaymentNatureProductTypeTemplates 
		Inherits esICAccountsPaymentNatureProductTypeTemplates
		
	
		#Region "UpToICAccountsPaymentNatureProductTypeByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductTypeTemplates_IC_AccountsPaymentNatureProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  AndAlso Not ProductCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = New ICAccountsPaymentNatureProductType()
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.ProductTypeCode = Me.ProductCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNatureProductType)
				Me.RemovePreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me.ProductCode = Nothing
				
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me.ProductCode = value.ProductTypeCode
					
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICTemplateByTemplateID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductTypeTemplates_IC_Template
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICTemplateByTemplateID As ICTemplate
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICTemplateByTemplateID Is Nothing _
						 AndAlso Not TemplateID.Equals(Nothing)  Then
						
					Me._UpToICTemplateByTemplateID = New ICTemplate()
					Me._UpToICTemplateByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
					Me._UpToICTemplateByTemplateID.Query.Where(Me._UpToICTemplateByTemplateID.Query.TemplateID = Me.TemplateID)
					Me._UpToICTemplateByTemplateID.Query.Load()
				End If

				Return Me._UpToICTemplateByTemplateID
			End Get
			
            Set(ByVal value As ICTemplate)
				Me.RemovePreSave("UpToICTemplateByTemplateID")
				

				If value Is Nothing Then
				
					Me.TemplateID = Nothing
				
					Me._UpToICTemplateByTemplateID = Nothing
				Else
				
					Me.TemplateID = value.TemplateID
					
					Me._UpToICTemplateByTemplateID = value
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICTemplateByTemplateID Is Nothing Then
				Me.TemplateID = Me._UpToICTemplateByTemplateID.TemplateID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountsPaymentNatureProductTypeTemplatesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.ProductCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.ProductCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedDate
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.APNPTTemplateID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.APNPTTemplateID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.TemplateID, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.TemplateID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeTemplatesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountsPaymentNatureProductTypeTemplatesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductCode As String = "ProductCode"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const APNPTTemplateID As String = "APNPTTemplateID"
			 Public Const TemplateID As String = "TemplateID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductCode As String = "ProductCode"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const APNPTTemplateID As String = "APNPTTemplateID"
			 Public Const TemplateID As String = "TemplateID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountsPaymentNatureProductTypeTemplatesMetadata)
			
				If ICAccountsPaymentNatureProductTypeTemplatesMetadata.mapDelegates Is Nothing Then
					ICAccountsPaymentNatureProductTypeTemplatesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountsPaymentNatureProductTypeTemplatesMetadata._meta Is Nothing Then
					ICAccountsPaymentNatureProductTypeTemplatesMetadata._meta = New ICAccountsPaymentNatureProductTypeTemplatesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("APNPTTemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_AccountsPaymentNatureProductTypeTemplates"
				meta.Destination = "IC_AccountsPaymentNatureProductTypeTemplates"
				
				meta.spInsert = "proc_IC_AccountsPaymentNatureProductTypeTemplatesInsert"
				meta.spUpdate = "proc_IC_AccountsPaymentNatureProductTypeTemplatesUpdate"
				meta.spDelete = "proc_IC_AccountsPaymentNatureProductTypeTemplatesDelete"
				meta.spLoadAll = "proc_IC_AccountsPaymentNatureProductTypeTemplatesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountsPaymentNatureProductTypeTemplatesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountsPaymentNatureProductTypeTemplatesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
