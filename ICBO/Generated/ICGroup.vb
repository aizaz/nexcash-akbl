
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Group' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICGroup))> _
	<XmlType("ICGroup")> _	
	Partial Public Class ICGroup 
		Inherits esICGroup
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICGroup()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal groupCode As System.Int32)
			Dim obj As New ICGroup()
			obj.GroupCode = groupCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal groupCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICGroup()
			obj.GroupCode = groupCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICGroupCollection")> _
	Partial Public Class ICGroupCollection
		Inherits esICGroupCollection
		Implements IEnumerable(Of ICGroup)
	
		Public Function FindByPrimaryKey(ByVal groupCode As System.Int32) As ICGroup
			Return MyBase.SingleOrDefault(Function(e) e.GroupCode.HasValue AndAlso e.GroupCode.Value = groupCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICGroup))> _
		Public Class ICGroupCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICGroupCollection)
			
			Public Shared Widening Operator CType(packet As ICGroupCollectionWCFPacket) As ICGroupCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICGroupCollection) As ICGroupCollectionWCFPacket
				Return New ICGroupCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICGroupQuery 
		Inherits esICGroupQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICGroupQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICGroupQuery) As String
			Return ICGroupQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICGroupQuery
			Return DirectCast(ICGroupQuery.SerializeHelper.FromXml(query, GetType(ICGroupQuery)), ICGroupQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICGroup
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal groupCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(groupCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(groupCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal groupCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(groupCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(groupCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal groupCode As System.Int32) As Boolean
		
			Dim query As New ICGroupQuery()
			query.Where(query.GroupCode = groupCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal groupCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("GroupCode", groupCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Group.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGroupMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGroupMetadata.ColumnNames.GroupCode, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.GroupName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupName As System.String
			Get
				Return MyBase.GetSystemString(ICGroupMetadata.ColumnNames.GroupName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICGroupMetadata.ColumnNames.GroupName, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.GroupName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGroupMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGroupMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICGroupMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICGroupMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICGroupMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICGroupMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGroupMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGroupMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Group.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICGroupMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICGroupMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICGroupMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "GroupName"
							Me.str().GroupName = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.GroupCode)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICGroupMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICGroup)
				Me.entity = entity
			End Sub				
		
	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupName As System.String 
				Get
					Dim data_ As System.String = entity.GroupName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupName = Nothing
					Else
						entity.GroupName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICGroup
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICGroupMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICGroupQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICGroupQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICGroupQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICGroupQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICGroupQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICGroupCollection
		Inherits esEntityCollection(Of ICGroup)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICGroupMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICGroupCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICGroupQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICGroupQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICGroupQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICGroupQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICGroupQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICGroupQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICGroupQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICGroupQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICGroupMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "GroupCode" 
					Return Me.GroupCode
				Case "GroupName" 
					Return Me.GroupName
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupName As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.GroupName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICGroupMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICGroup 
		Inherits esICGroup
		
	
		#Region "ICCompanyCollectionByGroupCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCompanyCollectionByGroupCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGroup.ICCompanyCollectionByGroupCode_Delegate)
				map.PropertyName = "ICCompanyCollectionByGroupCode"
				map.MyColumnName = "GroupCode"
				map.ParentColumnName = "GroupCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCompanyCollectionByGroupCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGroupQuery(data.NextAlias())
			
			Dim mee As ICCompanyQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCompanyQuery), New ICCompanyQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GroupCode = mee.GroupCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Company_IC_Group
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCompanyCollectionByGroupCode As ICCompanyCollection 
		
			Get
				If Me._ICCompanyCollectionByGroupCode Is Nothing Then
					Me._ICCompanyCollectionByGroupCode = New ICCompanyCollection()
					Me._ICCompanyCollectionByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCompanyCollectionByGroupCode", Me._ICCompanyCollectionByGroupCode)
				
					If Not Me.GroupCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCompanyCollectionByGroupCode.Query.Where(Me._ICCompanyCollectionByGroupCode.Query.GroupCode = Me.GroupCode)
							Me._ICCompanyCollectionByGroupCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCompanyCollectionByGroupCode.fks.Add(ICCompanyMetadata.ColumnNames.GroupCode, Me.GroupCode)
					End If
				End If

				Return Me._ICCompanyCollectionByGroupCode
			End Get
			
			Set(ByVal value As ICCompanyCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCompanyCollectionByGroupCode Is Nothing Then

					Me.RemovePostSave("ICCompanyCollectionByGroupCode")
					Me._ICCompanyCollectionByGroupCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCompanyCollectionByGroupCode As ICCompanyCollection
		#End Region

		#Region "ICEmailSettingsCollectionByGroupCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICEmailSettingsCollectionByGroupCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGroup.ICEmailSettingsCollectionByGroupCode_Delegate)
				map.PropertyName = "ICEmailSettingsCollectionByGroupCode"
				map.MyColumnName = "GroupCode"
				map.ParentColumnName = "GroupCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICEmailSettingsCollectionByGroupCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGroupQuery(data.NextAlias())
			
			Dim mee As ICEmailSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICEmailSettingsQuery), New ICEmailSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GroupCode = mee.GroupCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_EmailSettings_IC_Group
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICEmailSettingsCollectionByGroupCode As ICEmailSettingsCollection 
		
			Get
				If Me._ICEmailSettingsCollectionByGroupCode Is Nothing Then
					Me._ICEmailSettingsCollectionByGroupCode = New ICEmailSettingsCollection()
					Me._ICEmailSettingsCollectionByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICEmailSettingsCollectionByGroupCode", Me._ICEmailSettingsCollectionByGroupCode)
				
					If Not Me.GroupCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICEmailSettingsCollectionByGroupCode.Query.Where(Me._ICEmailSettingsCollectionByGroupCode.Query.GroupCode = Me.GroupCode)
							Me._ICEmailSettingsCollectionByGroupCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICEmailSettingsCollectionByGroupCode.fks.Add(ICEmailSettingsMetadata.ColumnNames.GroupCode, Me.GroupCode)
					End If
				End If

				Return Me._ICEmailSettingsCollectionByGroupCode
			End Get
			
			Set(ByVal value As ICEmailSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICEmailSettingsCollectionByGroupCode Is Nothing Then

					Me.RemovePostSave("ICEmailSettingsCollectionByGroupCode")
					Me._ICEmailSettingsCollectionByGroupCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICEmailSettingsCollectionByGroupCode As ICEmailSettingsCollection
		#End Region

		#Region "ICFTPSettingsCollectionByGroupCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFTPSettingsCollectionByGroupCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGroup.ICFTPSettingsCollectionByGroupCode_Delegate)
				map.PropertyName = "ICFTPSettingsCollectionByGroupCode"
				map.MyColumnName = "GroupCode"
				map.ParentColumnName = "GroupCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICFTPSettingsCollectionByGroupCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGroupQuery(data.NextAlias())
			
			Dim mee As ICFTPSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFTPSettingsQuery), New ICFTPSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GroupCode = mee.GroupCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FTPSettings_IC_Group
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFTPSettingsCollectionByGroupCode As ICFTPSettingsCollection 
		
			Get
				If Me._ICFTPSettingsCollectionByGroupCode Is Nothing Then
					Me._ICFTPSettingsCollectionByGroupCode = New ICFTPSettingsCollection()
					Me._ICFTPSettingsCollectionByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFTPSettingsCollectionByGroupCode", Me._ICFTPSettingsCollectionByGroupCode)
				
					If Not Me.GroupCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFTPSettingsCollectionByGroupCode.Query.Where(Me._ICFTPSettingsCollectionByGroupCode.Query.GroupCode = Me.GroupCode)
							Me._ICFTPSettingsCollectionByGroupCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFTPSettingsCollectionByGroupCode.fks.Add(ICFTPSettingsMetadata.ColumnNames.GroupCode, Me.GroupCode)
					End If
				End If

				Return Me._ICFTPSettingsCollectionByGroupCode
			End Get
			
			Set(ByVal value As ICFTPSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFTPSettingsCollectionByGroupCode Is Nothing Then

					Me.RemovePostSave("ICFTPSettingsCollectionByGroupCode")
					Me._ICFTPSettingsCollectionByGroupCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFTPSettingsCollectionByGroupCode As ICFTPSettingsCollection
		#End Region

		#Region "ICInstructionCollectionByGroupCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByGroupCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGroup.ICInstructionCollectionByGroupCode_Delegate)
				map.PropertyName = "ICInstructionCollectionByGroupCode"
				map.MyColumnName = "GroupCode"
				map.ParentColumnName = "GroupCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByGroupCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGroupQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GroupCode = mee.GroupCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_Group
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByGroupCode As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByGroupCode Is Nothing Then
					Me._ICInstructionCollectionByGroupCode = New ICInstructionCollection()
					Me._ICInstructionCollectionByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByGroupCode", Me._ICInstructionCollectionByGroupCode)
				
					If Not Me.GroupCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByGroupCode.Query.Where(Me._ICInstructionCollectionByGroupCode.Query.GroupCode = Me.GroupCode)
							Me._ICInstructionCollectionByGroupCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByGroupCode.fks.Add(ICInstructionMetadata.ColumnNames.GroupCode, Me.GroupCode)
					End If
				End If

				Return Me._ICInstructionCollectionByGroupCode
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByGroupCode Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByGroupCode")
					Me._ICInstructionCollectionByGroupCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByGroupCode As ICInstructionCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCompanyCollectionByGroupCode"
					coll = Me.ICCompanyCollectionByGroupCode
					Exit Select
				Case "ICEmailSettingsCollectionByGroupCode"
					coll = Me.ICEmailSettingsCollectionByGroupCode
					Exit Select
				Case "ICFTPSettingsCollectionByGroupCode"
					coll = Me.ICFTPSettingsCollectionByGroupCode
					Exit Select
				Case "ICInstructionCollectionByGroupCode"
					coll = Me.ICInstructionCollectionByGroupCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCompanyCollectionByGroupCode", GetType(ICCompanyCollection), New ICCompany()))
			props.Add(new esPropertyDescriptor(Me, "ICEmailSettingsCollectionByGroupCode", GetType(ICEmailSettingsCollection), New ICEmailSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICFTPSettingsCollectionByGroupCode", GetType(ICFTPSettingsCollection), New ICFTPSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByGroupCode", GetType(ICInstructionCollection), New ICInstruction()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCompanyCollectionByGroupCode Is Nothing Then
				Apply(Me._ICCompanyCollectionByGroupCode, "GroupCode", Me.GroupCode)
			End If
			
			If Not Me._ICEmailSettingsCollectionByGroupCode Is Nothing Then
				Apply(Me._ICEmailSettingsCollectionByGroupCode, "GroupCode", Me.GroupCode)
			End If
			
			If Not Me._ICFTPSettingsCollectionByGroupCode Is Nothing Then
				Apply(Me._ICFTPSettingsCollectionByGroupCode, "GroupCode", Me.GroupCode)
			End If
			
			If Not Me._ICInstructionCollectionByGroupCode Is Nothing Then
				Apply(Me._ICInstructionCollectionByGroupCode, "GroupCode", Me.GroupCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICGroupMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.GroupCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGroupMetadata.PropertyNames.GroupCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.GroupName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICGroupMetadata.PropertyNames.GroupName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.CreatedBy, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGroupMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.CreatedDate, 3, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICGroupMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICGroupMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.Creater, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGroupMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGroupMetadata.ColumnNames.CreationDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICGroupMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICGroupMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const GroupCode As String = "GroupCode"
			 Public Const GroupName As String = "GroupName"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const GroupCode As String = "GroupCode"
			 Public Const GroupName As String = "GroupName"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICGroupMetadata)
			
				If ICGroupMetadata.mapDelegates Is Nothing Then
					ICGroupMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICGroupMetadata._meta Is Nothing Then
					ICGroupMetadata._meta = New ICGroupMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Group"
				meta.Destination = "IC_Group"
				
				meta.spInsert = "proc_IC_GroupInsert"
				meta.spUpdate = "proc_IC_GroupUpdate"
				meta.spDelete = "proc_IC_GroupDelete"
				meta.spLoadAll = "proc_IC_GroupLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_GroupLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICGroupMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
