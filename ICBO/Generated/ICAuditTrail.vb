
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AuditTrail' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAuditTrail))> _
	<XmlType("ICAuditTrail")> _	
	Partial Public Class ICAuditTrail 
		Inherits esICAuditTrail
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAuditTrail()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal auditID As System.Int32)
			Dim obj As New ICAuditTrail()
			obj.AuditID = auditID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal auditID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAuditTrail()
			obj.AuditID = auditID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAuditTrailCollection")> _
	Partial Public Class ICAuditTrailCollection
		Inherits esICAuditTrailCollection
		Implements IEnumerable(Of ICAuditTrail)
	
		Public Function FindByPrimaryKey(ByVal auditID As System.Int32) As ICAuditTrail
			Return MyBase.SingleOrDefault(Function(e) e.AuditID.HasValue AndAlso e.AuditID.Value = auditID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAuditTrail))> _
		Public Class ICAuditTrailCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAuditTrailCollection)
			
			Public Shared Widening Operator CType(packet As ICAuditTrailCollectionWCFPacket) As ICAuditTrailCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAuditTrailCollection) As ICAuditTrailCollectionWCFPacket
				Return New ICAuditTrailCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAuditTrailQuery 
		Inherits esICAuditTrailQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAuditTrailQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAuditTrailQuery) As String
			Return ICAuditTrailQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAuditTrailQuery
			Return DirectCast(ICAuditTrailQuery.SerializeHelper.FromXml(query, GetType(ICAuditTrailQuery)), ICAuditTrailQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAuditTrail
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal auditID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(auditID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(auditID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal auditID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(auditID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(auditID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal auditID As System.Int32) As Boolean
		
			Dim query As New ICAuditTrailQuery()
			query.Where(query.AuditID = auditID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal auditID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AuditID", auditID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AuditTrail.AuditID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AuditID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAuditTrailMetadata.ColumnNames.AuditID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAuditTrailMetadata.ColumnNames.AuditID, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.AuditDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AuditDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAuditTrailMetadata.ColumnNames.AuditDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAuditTrailMetadata.ColumnNames.AuditDate, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.AuditAction
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AuditAction As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.AuditAction)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.AuditAction, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditAction)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.AuditType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AuditType As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.AuditType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.AuditType, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.RelatedID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedID As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.RelatedID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.RelatedID, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.RelatedID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.IPAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IPAddress As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.IPAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.IPAddress, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.IPAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.Username
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Username As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.Username)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.Username, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.Username)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAuditTrailMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAuditTrailMetadata.ColumnNames.UserID, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AuditTrail.ActionType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ActionType As System.String
			Get
				Return MyBase.GetSystemString(ICAuditTrailMetadata.ColumnNames.ActionType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAuditTrailMetadata.ColumnNames.ActionType, value) Then
					OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.ActionType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AuditID"
							Me.str().AuditID = CType(value, string)
												
						Case "AuditDate"
							Me.str().AuditDate = CType(value, string)
												
						Case "AuditAction"
							Me.str().AuditAction = CType(value, string)
												
						Case "AuditType"
							Me.str().AuditType = CType(value, string)
												
						Case "RelatedID"
							Me.str().RelatedID = CType(value, string)
												
						Case "IPAddress"
							Me.str().IPAddress = CType(value, string)
												
						Case "Username"
							Me.str().Username = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "ActionType"
							Me.str().ActionType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "AuditID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AuditID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditID)
							End If
						
						Case "AuditDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AuditDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.AuditDate)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAuditTrailMetadata.PropertyNames.UserID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAuditTrail)
				Me.entity = entity
			End Sub				
		
	
			Public Property AuditID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AuditID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AuditID = Nothing
					Else
						entity.AuditID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AuditDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AuditDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AuditDate = Nothing
					Else
						entity.AuditDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AuditAction As System.String 
				Get
					Dim data_ As System.String = entity.AuditAction
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AuditAction = Nothing
					Else
						entity.AuditAction = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AuditType As System.String 
				Get
					Dim data_ As System.String = entity.AuditType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AuditType = Nothing
					Else
						entity.AuditType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedID As System.String 
				Get
					Dim data_ As System.String = entity.RelatedID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedID = Nothing
					Else
						entity.RelatedID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IPAddress As System.String 
				Get
					Dim data_ As System.String = entity.IPAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IPAddress = Nothing
					Else
						entity.IPAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Username As System.String 
				Get
					Dim data_ As System.String = entity.Username
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Username = Nothing
					Else
						entity.Username = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ActionType As System.String 
				Get
					Dim data_ As System.String = entity.ActionType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ActionType = Nothing
					Else
						entity.ActionType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAuditTrail
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAuditTrailMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAuditTrailQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAuditTrailQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAuditTrailQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAuditTrailQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAuditTrailQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAuditTrailCollection
		Inherits esEntityCollection(Of ICAuditTrail)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAuditTrailMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAuditTrailCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAuditTrailQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAuditTrailQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAuditTrailQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAuditTrailQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAuditTrailQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAuditTrailQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAuditTrailQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAuditTrailQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAuditTrailMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AuditID" 
					Return Me.AuditID
				Case "AuditDate" 
					Return Me.AuditDate
				Case "AuditAction" 
					Return Me.AuditAction
				Case "AuditType" 
					Return Me.AuditType
				Case "RelatedID" 
					Return Me.RelatedID
				Case "IPAddress" 
					Return Me.IPAddress
				Case "Username" 
					Return Me.Username
				Case "UserID" 
					Return Me.UserID
				Case "ActionType" 
					Return Me.ActionType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AuditID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.AuditID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AuditDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.AuditDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AuditAction As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.AuditAction, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AuditType As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.AuditType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.RelatedID, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IPAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.IPAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Username As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.Username, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ActionType As esQueryItem
			Get
				Return New esQueryItem(Me, ICAuditTrailMetadata.ColumnNames.ActionType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAuditTrail 
		Inherits esICAuditTrail
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICAuditTrailMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.AuditID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.AuditID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.AuditDate, 1, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.AuditDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.AuditAction, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.AuditAction
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.AuditType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.AuditType
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.RelatedID, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.RelatedID
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.IPAddress, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.IPAddress
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.Username, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.Username
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.UserID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAuditTrailMetadata.ColumnNames.ActionType, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAuditTrailMetadata.PropertyNames.ActionType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAuditTrailMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AuditID As String = "AuditID"
			 Public Const AuditDate As String = "AuditDate"
			 Public Const AuditAction As String = "AuditAction"
			 Public Const AuditType As String = "AuditType"
			 Public Const RelatedID As String = "RelatedID"
			 Public Const IPAddress As String = "IPAddress"
			 Public Const Username As String = "Username"
			 Public Const UserID As String = "UserID"
			 Public Const ActionType As String = "ActionType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AuditID As String = "AuditID"
			 Public Const AuditDate As String = "AuditDate"
			 Public Const AuditAction As String = "AuditAction"
			 Public Const AuditType As String = "AuditType"
			 Public Const RelatedID As String = "RelatedID"
			 Public Const IPAddress As String = "IPAddress"
			 Public Const Username As String = "Username"
			 Public Const UserID As String = "UserID"
			 Public Const ActionType As String = "ActionType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAuditTrailMetadata)
			
				If ICAuditTrailMetadata.mapDelegates Is Nothing Then
					ICAuditTrailMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAuditTrailMetadata._meta Is Nothing Then
					ICAuditTrailMetadata._meta = New ICAuditTrailMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AuditID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AuditDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AuditAction", new esTypeMap("text", "System.String"))
				meta.AddTypeMap("AuditType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RelatedID", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IPAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Username", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ActionType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_AuditTrail"
				meta.Destination = "IC_AuditTrail"
				
				meta.spInsert = "proc_IC_AuditTrailInsert"
				meta.spUpdate = "proc_IC_AuditTrailUpdate"
				meta.spDelete = "proc_IC_AuditTrailDelete"
				meta.spLoadAll = "proc_IC_AuditTrailLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AuditTrailLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAuditTrailMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
