
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_WebService' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICWebService))> _
	<XmlType("ICWebService")> _	
	Partial Public Class ICWebService 
		Inherits esICWebService
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICWebService()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal webServiceID As System.Int32)
			Dim obj As New ICWebService()
			obj.WebServiceID = webServiceID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal webServiceID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICWebService()
			obj.WebServiceID = webServiceID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICWebServiceCollection")> _
	Partial Public Class ICWebServiceCollection
		Inherits esICWebServiceCollection
		Implements IEnumerable(Of ICWebService)
	
		Public Function FindByPrimaryKey(ByVal webServiceID As System.Int32) As ICWebService
			Return MyBase.SingleOrDefault(Function(e) e.WebServiceID.HasValue AndAlso e.WebServiceID.Value = webServiceID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICWebService))> _
		Public Class ICWebServiceCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICWebServiceCollection)
			
			Public Shared Widening Operator CType(packet As ICWebServiceCollectionWCFPacket) As ICWebServiceCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICWebServiceCollection) As ICWebServiceCollectionWCFPacket
				Return New ICWebServiceCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICWebServiceQuery 
		Inherits esICWebServiceQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICWebServiceQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICWebServiceQuery) As String
			Return ICWebServiceQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICWebServiceQuery
			Return DirectCast(ICWebServiceQuery.SerializeHelper.FromXml(query, GetType(ICWebServiceQuery)), ICWebServiceQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICWebService
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal webServiceID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(webServiceID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(webServiceID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal webServiceID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(webServiceID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(webServiceID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal webServiceID As System.Int32) As Boolean
		
			Dim query As New ICWebServiceQuery()
			query.Where(query.WebServiceID = webServiceID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal webServiceID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("WebServiceID", webServiceID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_WebService.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.WebServiceUserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property WebServiceUserID As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.WebServiceUserID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.WebServiceUserID, value) Then
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.WebServiceUserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.Password
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Password As System.String
			Get
				Return MyBase.GetSystemString(ICWebServiceMetadata.ColumnNames.Password)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWebServiceMetadata.ColumnNames.Password, value) Then
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.Password)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WebService.WebServiceID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property WebServiceID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICWebServiceMetadata.ColumnNames.WebServiceID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICWebServiceMetadata.ColumnNames.WebServiceID, value) Then
					OnPropertyChanged(ICWebServiceMetadata.PropertyNames.WebServiceID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "WebServiceUserID"
							Me.str().WebServiceUserID = CType(value, string)
												
						Case "Password"
							Me.str().Password = CType(value, string)
												
						Case "WebServiceID"
							Me.str().WebServiceID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "WebServiceID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.WebServiceID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICWebServiceMetadata.PropertyNames.WebServiceID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICWebService)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property WebServiceUserID As System.String 
				Get
					Dim data_ As System.String = entity.WebServiceUserID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.WebServiceUserID = Nothing
					Else
						entity.WebServiceUserID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Password As System.String 
				Get
					Dim data_ As System.String = entity.Password
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Password = Nothing
					Else
						entity.Password = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property WebServiceID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.WebServiceID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.WebServiceID = Nothing
					Else
						entity.WebServiceID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICWebService
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICWebServiceMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICWebServiceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICWebServiceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICWebServiceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICWebServiceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICWebServiceQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICWebServiceCollection
		Inherits esEntityCollection(Of ICWebService)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICWebServiceMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICWebServiceCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICWebServiceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICWebServiceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICWebServiceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICWebServiceQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICWebServiceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICWebServiceQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICWebServiceQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICWebServiceQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICWebServiceMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "WebServiceUserID" 
					Return Me.WebServiceUserID
				Case "Password" 
					Return Me.Password
				Case "WebServiceID" 
					Return Me.WebServiceID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property WebServiceUserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.WebServiceUserID, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Password As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.Password, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property WebServiceID As esQueryItem
			Get
				Return New esQueryItem(Me, ICWebServiceMetadata.ColumnNames.WebServiceID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICWebService 
		Inherits esICWebService
		
	
		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_WebService_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICWebServiceMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.WebServiceUserID, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.WebServiceUserID
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.Password, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.Password
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWebServiceMetadata.ColumnNames.WebServiceID, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICWebServiceMetadata.PropertyNames.WebServiceID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICWebServiceMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const WebServiceUserID As String = "WebServiceUserID"
			 Public Const Password As String = "Password"
			 Public Const WebServiceID As String = "WebServiceID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const WebServiceUserID As String = "WebServiceUserID"
			 Public Const Password As String = "Password"
			 Public Const WebServiceID As String = "WebServiceID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICWebServiceMetadata)
			
				If ICWebServiceMetadata.mapDelegates Is Nothing Then
					ICWebServiceMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICWebServiceMetadata._meta Is Nothing Then
					ICWebServiceMetadata._meta = New ICWebServiceMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("WebServiceUserID", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Password", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("WebServiceID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_WebService"
				meta.Destination = "IC_WebService"
				
				meta.spInsert = "proc_IC_WebServiceInsert"
				meta.spUpdate = "proc_IC_WebServiceUpdate"
				meta.spDelete = "proc_IC_WebServiceDelete"
				meta.spLoadAll = "proc_IC_WebServiceLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_WebServiceLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICWebServiceMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
