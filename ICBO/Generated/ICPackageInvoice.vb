
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 19-May-2014 12:48:32 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PackageInvoice' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPackageInvoice))> _
	<XmlType("ICPackageInvoice")> _	
	Partial Public Class ICPackageInvoice 
		Inherits esICPackageInvoice
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPackageInvoice()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal invoiceId As System.Int64)
			Dim obj As New ICPackageInvoice()
			obj.InvoiceId = invoiceId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal invoiceId As System.Int64, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPackageInvoice()
			obj.InvoiceId = invoiceId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPackageInvoiceCollection")> _
	Partial Public Class ICPackageInvoiceCollection
		Inherits esICPackageInvoiceCollection
		Implements IEnumerable(Of ICPackageInvoice)
	
		Public Function FindByPrimaryKey(ByVal invoiceId As System.Int64) As ICPackageInvoice
			Return MyBase.SingleOrDefault(Function(e) e.InvoiceId.HasValue AndAlso e.InvoiceId.Value = invoiceId)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPackageInvoice))> _
		Public Class ICPackageInvoiceCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPackageInvoiceCollection)
			
			Public Shared Widening Operator CType(packet As ICPackageInvoiceCollectionWCFPacket) As ICPackageInvoiceCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPackageInvoiceCollection) As ICPackageInvoiceCollectionWCFPacket
				Return New ICPackageInvoiceCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPackageInvoiceQuery 
		Inherits esICPackageInvoiceQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPackageInvoiceQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPackageInvoiceQuery) As String
			Return ICPackageInvoiceQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPackageInvoiceQuery
			Return DirectCast(ICPackageInvoiceQuery.SerializeHelper.FromXml(query, GetType(ICPackageInvoiceQuery)), ICPackageInvoiceQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPackageInvoice
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal invoiceId As System.Int64) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(invoiceId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(invoiceId)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal invoiceId As System.Int64) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(invoiceId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(invoiceId)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal invoiceId As System.Int64) As Boolean
		
			Dim query As New ICPackageInvoiceQuery()
			query.Where(query.InvoiceId = invoiceId)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal invoiceId As System.Int64) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("InvoiceId", invoiceId)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PackageInvoice.InvoiceId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceId As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICPackageInvoiceMetadata.ColumnNames.InvoiceId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICPackageInvoiceMetadata.ColumnNames.InvoiceId, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.InvoiceDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDate, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.InvoiceAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.InvoiceAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.InvoiceAmount, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.IsPaid
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPaid As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPackageInvoiceMetadata.ColumnNames.IsPaid)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPackageInvoiceMetadata.ColumnNames.IsPaid, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.IsPaid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.PaidAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaidAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.PaidAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.PaidAmount, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.PaidAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.PaymentDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.PaymentDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.PaymentDate, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.PaymentDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.DiscountAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DiscountAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.DiscountAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageInvoiceMetadata.ColumnNames.DiscountAmount, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.DiscountAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.InvoiceDateFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceDateFrom As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateFrom, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDateFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.InvoiceDateTo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceDateTo As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateTo)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateTo, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDateTo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeCreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeCreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedBy, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeCreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeCreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedOn, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPackageInvoiceMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPackageInvoiceMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeTransactionStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeTransactionStatus As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionStatus, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeTransactionStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ClientAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountNumber, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ClientAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ClientAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountBranchCode, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ClientAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ClientAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.ClientAccountCurrency, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ClientAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.BankAccountId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankAccountId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.BankAccountId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.BankAccountId, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.BankAccountId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedBy, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedOn, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.ChargeTransactionErrorMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeTransactionErrorMessage As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionErrorMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionErrorMessage, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeTransactionErrorMessage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageInvoice.Remarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Remarks As System.String
			Get
				Return MyBase.GetSystemString(ICPackageInvoiceMetadata.ColumnNames.Remarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageInvoiceMetadata.ColumnNames.Remarks, value) Then
					OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.Remarks)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "InvoiceId"
							Me.str().InvoiceId = CType(value, string)
												
						Case "InvoiceDate"
							Me.str().InvoiceDate = CType(value, string)
												
						Case "InvoiceAmount"
							Me.str().InvoiceAmount = CType(value, string)
												
						Case "IsPaid"
							Me.str().IsPaid = CType(value, string)
												
						Case "PaidAmount"
							Me.str().PaidAmount = CType(value, string)
												
						Case "PaymentDate"
							Me.str().PaymentDate = CType(value, string)
												
						Case "DiscountAmount"
							Me.str().DiscountAmount = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "InvoiceDateFrom"
							Me.str().InvoiceDateFrom = CType(value, string)
												
						Case "InvoiceDateTo"
							Me.str().InvoiceDateTo = CType(value, string)
												
						Case "ChargeCreatedBy"
							Me.str().ChargeCreatedBy = CType(value, string)
												
						Case "ChargeCreatedOn"
							Me.str().ChargeCreatedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ChargeTransactionStatus"
							Me.str().ChargeTransactionStatus = CType(value, string)
												
						Case "ClientAccountNumber"
							Me.str().ClientAccountNumber = CType(value, string)
												
						Case "ClientAccountBranchCode"
							Me.str().ClientAccountBranchCode = CType(value, string)
												
						Case "ClientAccountCurrency"
							Me.str().ClientAccountCurrency = CType(value, string)
												
						Case "BankAccountId"
							Me.str().BankAccountId = CType(value, string)
												
						Case "ChargeApprovedBy"
							Me.str().ChargeApprovedBy = CType(value, string)
												
						Case "ChargeApprovedOn"
							Me.str().ChargeApprovedOn = CType(value, string)
												
						Case "ChargeTransactionErrorMessage"
							Me.str().ChargeTransactionErrorMessage = CType(value, string)
												
						Case "Remarks"
							Me.str().Remarks = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "InvoiceId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.InvoiceId = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceId)
							End If
						
						Case "InvoiceDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.InvoiceDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDate)
							End If
						
						Case "InvoiceAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.InvoiceAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceAmount)
							End If
						
						Case "IsPaid"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPaid = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.IsPaid)
							End If
						
						Case "PaidAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.PaidAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.PaidAmount)
							End If
						
						Case "PaymentDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PaymentDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.PaymentDate)
							End If
						
						Case "DiscountAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DiscountAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.DiscountAmount)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "InvoiceDateFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.InvoiceDateFrom = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDateFrom)
							End If
						
						Case "InvoiceDateTo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.InvoiceDateTo = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.InvoiceDateTo)
							End If
						
						Case "ChargeCreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ChargeCreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedBy)
							End If
						
						Case "ChargeCreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ChargeCreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.IsApproved)
							End If
						
						Case "BankAccountId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankAccountId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.BankAccountId)
							End If
						
						Case "ChargeApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ChargeApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedBy)
							End If
						
						Case "ChargeApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ChargeApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPackageInvoice)
				Me.entity = entity
			End Sub				
		
	
			Public Property InvoiceId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.InvoiceId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceId = Nothing
					Else
						entity.InvoiceId = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.InvoiceDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceDate = Nothing
					Else
						entity.InvoiceDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.InvoiceAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceAmount = Nothing
					Else
						entity.InvoiceAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPaid As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPaid
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPaid = Nothing
					Else
						entity.IsPaid = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaidAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.PaidAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaidAmount = Nothing
					Else
						entity.PaidAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PaymentDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentDate = Nothing
					Else
						entity.PaymentDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DiscountAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DiscountAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DiscountAmount = Nothing
					Else
						entity.DiscountAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceDateFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.InvoiceDateFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceDateFrom = Nothing
					Else
						entity.InvoiceDateFrom = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceDateTo As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.InvoiceDateTo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceDateTo = Nothing
					Else
						entity.InvoiceDateTo = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeCreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ChargeCreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeCreatedBy = Nothing
					Else
						entity.ChargeCreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeCreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ChargeCreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeCreatedOn = Nothing
					Else
						entity.ChargeCreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeTransactionStatus As System.String 
				Get
					Dim data_ As System.String = entity.ChargeTransactionStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeTransactionStatus = Nothing
					Else
						entity.ChargeTransactionStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountNumber = Nothing
					Else
						entity.ClientAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountBranchCode = Nothing
					Else
						entity.ClientAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountCurrency = Nothing
					Else
						entity.ClientAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankAccountId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankAccountId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankAccountId = Nothing
					Else
						entity.BankAccountId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ChargeApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeApprovedBy = Nothing
					Else
						entity.ChargeApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ChargeApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeApprovedOn = Nothing
					Else
						entity.ChargeApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeTransactionErrorMessage As System.String 
				Get
					Dim data_ As System.String = entity.ChargeTransactionErrorMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeTransactionErrorMessage = Nothing
					Else
						entity.ChargeTransactionErrorMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Remarks As System.String 
				Get
					Dim data_ As System.String = entity.Remarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Remarks = Nothing
					Else
						entity.Remarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPackageInvoice
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageInvoiceMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPackageInvoiceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageInvoiceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPackageInvoiceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPackageInvoiceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPackageInvoiceQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPackageInvoiceCollection
		Inherits esEntityCollection(Of ICPackageInvoice)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageInvoiceMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPackageInvoiceCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPackageInvoiceQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageInvoiceQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPackageInvoiceQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPackageInvoiceQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPackageInvoiceQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPackageInvoiceQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPackageInvoiceQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPackageInvoiceQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPackageInvoiceMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "InvoiceId" 
					Return Me.InvoiceId
				Case "InvoiceDate" 
					Return Me.InvoiceDate
				Case "InvoiceAmount" 
					Return Me.InvoiceAmount
				Case "IsPaid" 
					Return Me.IsPaid
				Case "PaidAmount" 
					Return Me.PaidAmount
				Case "PaymentDate" 
					Return Me.PaymentDate
				Case "DiscountAmount" 
					Return Me.DiscountAmount
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "InvoiceDateFrom" 
					Return Me.InvoiceDateFrom
				Case "InvoiceDateTo" 
					Return Me.InvoiceDateTo
				Case "ChargeCreatedBy" 
					Return Me.ChargeCreatedBy
				Case "ChargeCreatedOn" 
					Return Me.ChargeCreatedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ChargeTransactionStatus" 
					Return Me.ChargeTransactionStatus
				Case "ClientAccountNumber" 
					Return Me.ClientAccountNumber
				Case "ClientAccountBranchCode" 
					Return Me.ClientAccountBranchCode
				Case "ClientAccountCurrency" 
					Return Me.ClientAccountCurrency
				Case "BankAccountId" 
					Return Me.BankAccountId
				Case "ChargeApprovedBy" 
					Return Me.ChargeApprovedBy
				Case "ChargeApprovedOn" 
					Return Me.ChargeApprovedOn
				Case "ChargeTransactionErrorMessage" 
					Return Me.ChargeTransactionErrorMessage
				Case "Remarks" 
					Return Me.Remarks
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property InvoiceId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.InvoiceId, esSystemType.Int64)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.InvoiceDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.InvoiceAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property IsPaid As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.IsPaid, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property PaidAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.PaidAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.PaymentDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DiscountAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.DiscountAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceDateFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.InvoiceDateFrom, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceDateTo As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.InvoiceDateTo, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeCreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeCreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeTransactionStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ClientAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ClientAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ClientAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankAccountId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.BankAccountId, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeTransactionErrorMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionErrorMessage, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Remarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageInvoiceMetadata.ColumnNames.Remarks, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPackageInvoice 
		Inherits esICPackageInvoice
		
	
		#Region "ICPackageChargesCollectionByInvoiceId - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageChargesCollectionByInvoiceId() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPackageInvoice.ICPackageChargesCollectionByInvoiceId_Delegate)
				map.PropertyName = "ICPackageChargesCollectionByInvoiceId"
				map.MyColumnName = "InvoiceId"
				map.ParentColumnName = "InvoiceId"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageChargesCollectionByInvoiceId_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPackageInvoiceQuery(data.NextAlias())
			
			Dim mee As ICPackageChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageChargesQuery), New ICPackageChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.InvoiceId = mee.InvoiceId)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges2
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageChargesCollectionByInvoiceId As ICPackageChargesCollection 
		
			Get
				If Me._ICPackageChargesCollectionByInvoiceId Is Nothing Then
					Me._ICPackageChargesCollectionByInvoiceId = New ICPackageChargesCollection()
					Me._ICPackageChargesCollectionByInvoiceId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageChargesCollectionByInvoiceId", Me._ICPackageChargesCollectionByInvoiceId)
				
					If Not Me.InvoiceId.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageChargesCollectionByInvoiceId.Query.Where(Me._ICPackageChargesCollectionByInvoiceId.Query.InvoiceId = Me.InvoiceId)
							Me._ICPackageChargesCollectionByInvoiceId.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageChargesCollectionByInvoiceId.fks.Add(ICPackageChargesMetadata.ColumnNames.InvoiceId, Me.InvoiceId)
					End If
				End If

				Return Me._ICPackageChargesCollectionByInvoiceId
			End Get
			
			Set(ByVal value As ICPackageChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageChargesCollectionByInvoiceId Is Nothing Then

					Me.RemovePostSave("ICPackageChargesCollectionByInvoiceId")
					Me._ICPackageChargesCollectionByInvoiceId = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageChargesCollectionByInvoiceId As ICPackageChargesCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageInvoice_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICPackageChargesCollectionByInvoiceId"
					coll = Me.ICPackageChargesCollectionByInvoiceId
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICPackageChargesCollectionByInvoiceId", GetType(ICPackageChargesCollection), New ICPackageCharges()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICPackageChargesCollectionByInvoiceId Is Nothing Then
				Apply(Me._ICPackageChargesCollectionByInvoiceId, "InvoiceId", Me.InvoiceId)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPackageInvoiceMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.InvoiceId, 0, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.InvoiceId
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 19
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.InvoiceDate, 1, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.InvoiceDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.InvoiceAmount, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.InvoiceAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.IsPaid, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.IsPaid
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.PaidAmount, 4, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.PaidAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.PaymentDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.PaymentDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.DiscountAmount, 6, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.DiscountAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.CompanyCode, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateFrom, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.InvoiceDateFrom
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.InvoiceDateTo, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.InvoiceDateTo
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeCreatedOn, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeCreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.IsApproved, 12, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionStatus, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeTransactionStatus
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ClientAccountNumber, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ClientAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ClientAccountBranchCode, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ClientAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ClientAccountCurrency, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ClientAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.BankAccountId, 17, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.BankAccountId
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedBy, 18, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeApprovedOn, 19, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.ChargeTransactionErrorMessage, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.ChargeTransactionErrorMessage
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageInvoiceMetadata.ColumnNames.Remarks, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageInvoiceMetadata.PropertyNames.Remarks
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPackageInvoiceMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const InvoiceId As String = "InvoiceId"
			 Public Const InvoiceDate As String = "InvoiceDate"
			 Public Const InvoiceAmount As String = "InvoiceAmount"
			 Public Const IsPaid As String = "IsPaid"
			 Public Const PaidAmount As String = "PaidAmount"
			 Public Const PaymentDate As String = "PaymentDate"
			 Public Const DiscountAmount As String = "DiscountAmount"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const InvoiceDateFrom As String = "InvoiceDateFrom"
			 Public Const InvoiceDateTo As String = "InvoiceDateTo"
			 Public Const ChargeCreatedBy As String = "ChargeCreatedBy"
			 Public Const ChargeCreatedOn As String = "ChargeCreatedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ChargeTransactionStatus As String = "ChargeTransactionStatus"
			 Public Const ClientAccountNumber As String = "ClientAccountNumber"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const BankAccountId As String = "BankAccountId"
			 Public Const ChargeApprovedBy As String = "ChargeApprovedBy"
			 Public Const ChargeApprovedOn As String = "ChargeApprovedOn"
			 Public Const ChargeTransactionErrorMessage As String = "ChargeTransactionErrorMessage"
			 Public Const Remarks As String = "Remarks"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const InvoiceId As String = "InvoiceId"
			 Public Const InvoiceDate As String = "InvoiceDate"
			 Public Const InvoiceAmount As String = "InvoiceAmount"
			 Public Const IsPaid As String = "IsPaid"
			 Public Const PaidAmount As String = "PaidAmount"
			 Public Const PaymentDate As String = "PaymentDate"
			 Public Const DiscountAmount As String = "DiscountAmount"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const InvoiceDateFrom As String = "InvoiceDateFrom"
			 Public Const InvoiceDateTo As String = "InvoiceDateTo"
			 Public Const ChargeCreatedBy As String = "ChargeCreatedBy"
			 Public Const ChargeCreatedOn As String = "ChargeCreatedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ChargeTransactionStatus As String = "ChargeTransactionStatus"
			 Public Const ClientAccountNumber As String = "ClientAccountNumber"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const BankAccountId As String = "BankAccountId"
			 Public Const ChargeApprovedBy As String = "ChargeApprovedBy"
			 Public Const ChargeApprovedOn As String = "ChargeApprovedOn"
			 Public Const ChargeTransactionErrorMessage As String = "ChargeTransactionErrorMessage"
			 Public Const Remarks As String = "Remarks"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPackageInvoiceMetadata)
			
				If ICPackageInvoiceMetadata.mapDelegates Is Nothing Then
					ICPackageInvoiceMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPackageInvoiceMetadata._meta Is Nothing Then
					ICPackageInvoiceMetadata._meta = New ICPackageInvoiceMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("InvoiceId", new esTypeMap("bigint", "System.Int64"))
				meta.AddTypeMap("InvoiceDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("InvoiceAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("IsPaid", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("PaidAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("PaymentDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DiscountAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InvoiceDateFrom", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("InvoiceDateTo", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ChargeCreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChargeCreatedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ChargeTransactionStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankAccountId", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChargeApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChargeApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ChargeTransactionErrorMessage", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Remarks", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_PackageInvoice"
				meta.Destination = "IC_PackageInvoice"
				
				meta.spInsert = "proc_IC_PackageInvoiceInsert"
				meta.spUpdate = "proc_IC_PackageInvoiceUpdate"
				meta.spDelete = "proc_IC_PackageInvoiceDelete"
				meta.spLoadAll = "proc_IC_PackageInvoiceLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PackageInvoiceLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPackageInvoiceMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
