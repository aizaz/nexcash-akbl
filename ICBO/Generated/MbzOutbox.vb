
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'MBZ_OUTBOX' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(MbzOutbox))> _
	<XmlType("MbzOutbox")> _	
	Partial Public Class MbzOutbox 
		Inherits esMbzOutbox
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New MbzOutbox()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal messageid As System.String)
			Dim obj As New MbzOutbox()
			obj.Messageid = messageid
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal messageid As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New MbzOutbox()
			obj.Messageid = messageid
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("MbzOutboxCollection")> _
	Partial Public Class MbzOutboxCollection
		Inherits esMbzOutboxCollection
		Implements IEnumerable(Of MbzOutbox)
	
		Public Function FindByPrimaryKey(ByVal messageid As System.String) As MbzOutbox
			Return MyBase.SingleOrDefault(Function(e) e.Messageid = messageid)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(MbzOutbox))> _
		Public Class MbzOutboxCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of MbzOutboxCollection)
			
			Public Shared Widening Operator CType(packet As MbzOutboxCollectionWCFPacket) As MbzOutboxCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As MbzOutboxCollection) As MbzOutboxCollectionWCFPacket
				Return New MbzOutboxCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class MbzOutboxQuery 
		Inherits esMbzOutboxQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "MbzOutboxQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As MbzOutboxQuery) As String
			Return MbzOutboxQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As MbzOutboxQuery
			Return DirectCast(MbzOutboxQuery.SerializeHelper.FromXml(query, GetType(MbzOutboxQuery)), MbzOutboxQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esMbzOutbox
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal messageid As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(messageid)
			Else
				Return LoadByPrimaryKeyStoredProcedure(messageid)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal messageid As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(messageid)
			Else
				Return LoadByPrimaryKeyStoredProcedure(messageid)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal messageid As System.String) As Boolean
		
			Dim query As New MbzOutboxQuery()
			query.Where(query.Messageid = messageid)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal messageid As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("Messageid", messageid)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to MBZ_OUTBOX.MESSAGEID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Messageid As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Messageid)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Messageid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Messageid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.MESSAGETEXT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Messagetext As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Messagetext)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Messagetext, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Messagetext)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.SENDERID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Senderid As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Senderid)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Senderid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Senderid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.DELIVERYREPORT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Deliveryreport As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Deliveryreport)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Deliveryreport, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Deliveryreport)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.FLASH
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Flash As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Flash)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Flash, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Flash)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.RECIPIENTID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Recipientid As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Recipientid)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Recipientid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Recipientid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.RETRYCOUNT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Retrycount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Retrycount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Retrycount, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Retrycount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.PRIORITY
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Priority As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Priority)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Priority, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Priority)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.USERID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Userid As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Userid)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Userid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Userid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ACCOUNTID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Accountid As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Accountid)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Accountid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Accountid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.INPROCESS
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Inprocess As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Inprocess)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Inprocess, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Inprocess)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.PROCESSSTARTEDAT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Processstartedat As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.Processstartedat)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.Processstartedat, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Processstartedat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.CREATEDAT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Createdat As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.Createdat)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.Createdat, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Createdat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.SCHEDULEDAT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Scheduledat As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.Scheduledat)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.Scheduledat, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Scheduledat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ALERTID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Alertid As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Alertid)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Alertid, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Alertid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ISENCRYPTED
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Isencrypted As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Isencrypted)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Isencrypted, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Isencrypted)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.REFERENCECOLUMN
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Referencecolumn As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.Referencecolumn)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.Referencecolumn, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Referencecolumn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_CREATEUSER
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtCreateuser As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.AdtCreateuser)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.AdtCreateuser, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtCreateuser)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_CREATEDATE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtCreatedate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtCreatedate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtCreatedate, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtCreatedate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_MODIFIEDUSER
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtModifieduser As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.AdtModifieduser)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.AdtModifieduser, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtModifieduser)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_MODIFIEDDATE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtModifieddate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtModifieddate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtModifieddate, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtModifieddate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_ROUTINGSTATUS
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtRoutingstatus As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.AdtRoutingstatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.AdtRoutingstatus, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtRoutingstatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_LASTROUTINGUSER
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtLastroutinguser As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.AdtLastroutinguser)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.AdtLastroutinguser, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtLastroutinguser)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.ADT_LASTROUTINGDATE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AdtLastroutingdate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtLastroutingdate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(MbzOutboxMetadata.ColumnNames.AdtLastroutingdate, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtLastroutingdate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.EmailStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EmailStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MbzOutboxMetadata.ColumnNames.EmailStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MbzOutboxMetadata.ColumnNames.EmailStatus, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.EmailStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_OUTBOX.CMN_ID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CmnId As System.String
			Get
				Return MyBase.GetSystemString(MbzOutboxMetadata.ColumnNames.CmnId)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MbzOutboxMetadata.ColumnNames.CmnId, value) Then
					OnPropertyChanged(MbzOutboxMetadata.PropertyNames.CmnId)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "Messageid"
							Me.str().Messageid = CType(value, string)
												
						Case "Messagetext"
							Me.str().Messagetext = CType(value, string)
												
						Case "Senderid"
							Me.str().Senderid = CType(value, string)
												
						Case "Deliveryreport"
							Me.str().Deliveryreport = CType(value, string)
												
						Case "Flash"
							Me.str().Flash = CType(value, string)
												
						Case "Recipientid"
							Me.str().Recipientid = CType(value, string)
												
						Case "Retrycount"
							Me.str().Retrycount = CType(value, string)
												
						Case "Priority"
							Me.str().Priority = CType(value, string)
												
						Case "Userid"
							Me.str().Userid = CType(value, string)
												
						Case "Accountid"
							Me.str().Accountid = CType(value, string)
												
						Case "Inprocess"
							Me.str().Inprocess = CType(value, string)
												
						Case "Processstartedat"
							Me.str().Processstartedat = CType(value, string)
												
						Case "Createdat"
							Me.str().Createdat = CType(value, string)
												
						Case "Scheduledat"
							Me.str().Scheduledat = CType(value, string)
												
						Case "Alertid"
							Me.str().Alertid = CType(value, string)
												
						Case "Isencrypted"
							Me.str().Isencrypted = CType(value, string)
												
						Case "Referencecolumn"
							Me.str().Referencecolumn = CType(value, string)
												
						Case "AdtCreateuser"
							Me.str().AdtCreateuser = CType(value, string)
												
						Case "AdtCreatedate"
							Me.str().AdtCreatedate = CType(value, string)
												
						Case "AdtModifieduser"
							Me.str().AdtModifieduser = CType(value, string)
												
						Case "AdtModifieddate"
							Me.str().AdtModifieddate = CType(value, string)
												
						Case "AdtRoutingstatus"
							Me.str().AdtRoutingstatus = CType(value, string)
												
						Case "AdtLastroutinguser"
							Me.str().AdtLastroutinguser = CType(value, string)
												
						Case "AdtLastroutingdate"
							Me.str().AdtLastroutingdate = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "EmailStatus"
							Me.str().EmailStatus = CType(value, string)
												
						Case "CmnId"
							Me.str().CmnId = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "Deliveryreport"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Deliveryreport = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Deliveryreport)
							End If
						
						Case "Flash"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Flash = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Flash)
							End If
						
						Case "Retrycount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Retrycount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Retrycount)
							End If
						
						Case "Priority"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Priority = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Priority)
							End If
						
						Case "Inprocess"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Inprocess = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Inprocess)
							End If
						
						Case "Processstartedat"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.Processstartedat = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Processstartedat)
							End If
						
						Case "Createdat"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.Createdat = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Createdat)
							End If
						
						Case "Scheduledat"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.Scheduledat = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Scheduledat)
							End If
						
						Case "Alertid"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Alertid = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Alertid)
							End If
						
						Case "Isencrypted"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Isencrypted = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Isencrypted)
							End If
						
						Case "AdtCreatedate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AdtCreatedate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtCreatedate)
							End If
						
						Case "AdtModifieddate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AdtModifieddate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtModifieddate)
							End If
						
						Case "AdtLastroutingdate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AdtLastroutingdate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.AdtLastroutingdate)
							End If
						
						Case "Status"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Status = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.Status)
							End If
						
						Case "EmailStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.EmailStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MbzOutboxMetadata.PropertyNames.EmailStatus)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esMbzOutbox)
				Me.entity = entity
			End Sub				
		
	
			Public Property Messageid As System.String 
				Get
					Dim data_ As System.String = entity.Messageid
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Messageid = Nothing
					Else
						entity.Messageid = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Messagetext As System.String 
				Get
					Dim data_ As System.String = entity.Messagetext
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Messagetext = Nothing
					Else
						entity.Messagetext = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Senderid As System.String 
				Get
					Dim data_ As System.String = entity.Senderid
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Senderid = Nothing
					Else
						entity.Senderid = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Deliveryreport As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Deliveryreport
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Deliveryreport = Nothing
					Else
						entity.Deliveryreport = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Flash As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Flash
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Flash = Nothing
					Else
						entity.Flash = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Recipientid As System.String 
				Get
					Dim data_ As System.String = entity.Recipientid
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Recipientid = Nothing
					Else
						entity.Recipientid = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Retrycount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Retrycount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Retrycount = Nothing
					Else
						entity.Retrycount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Priority As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Priority
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Priority = Nothing
					Else
						entity.Priority = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Userid As System.String 
				Get
					Dim data_ As System.String = entity.Userid
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Userid = Nothing
					Else
						entity.Userid = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Accountid As System.String 
				Get
					Dim data_ As System.String = entity.Accountid
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Accountid = Nothing
					Else
						entity.Accountid = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Inprocess As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Inprocess
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Inprocess = Nothing
					Else
						entity.Inprocess = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Processstartedat As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.Processstartedat
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Processstartedat = Nothing
					Else
						entity.Processstartedat = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Createdat As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.Createdat
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Createdat = Nothing
					Else
						entity.Createdat = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Scheduledat As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.Scheduledat
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Scheduledat = Nothing
					Else
						entity.Scheduledat = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Alertid As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Alertid
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Alertid = Nothing
					Else
						entity.Alertid = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Isencrypted As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Isencrypted
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Isencrypted = Nothing
					Else
						entity.Isencrypted = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Referencecolumn As System.String 
				Get
					Dim data_ As System.String = entity.Referencecolumn
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Referencecolumn = Nothing
					Else
						entity.Referencecolumn = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtCreateuser As System.String 
				Get
					Dim data_ As System.String = entity.AdtCreateuser
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtCreateuser = Nothing
					Else
						entity.AdtCreateuser = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtCreatedate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AdtCreatedate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtCreatedate = Nothing
					Else
						entity.AdtCreatedate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtModifieduser As System.String 
				Get
					Dim data_ As System.String = entity.AdtModifieduser
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtModifieduser = Nothing
					Else
						entity.AdtModifieduser = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtModifieddate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AdtModifieddate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtModifieddate = Nothing
					Else
						entity.AdtModifieddate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtRoutingstatus As System.String 
				Get
					Dim data_ As System.String = entity.AdtRoutingstatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtRoutingstatus = Nothing
					Else
						entity.AdtRoutingstatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtLastroutinguser As System.String 
				Get
					Dim data_ As System.String = entity.AdtLastroutinguser
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtLastroutinguser = Nothing
					Else
						entity.AdtLastroutinguser = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AdtLastroutingdate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AdtLastroutingdate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AdtLastroutingdate = Nothing
					Else
						entity.AdtLastroutingdate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Status
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property EmailStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.EmailStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EmailStatus = Nothing
					Else
						entity.EmailStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CmnId As System.String 
				Get
					Dim data_ As System.String = entity.CmnId
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CmnId = Nothing
					Else
						entity.CmnId = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esMbzOutbox
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return MbzOutboxMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As MbzOutboxQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New MbzOutboxQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As MbzOutboxQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As MbzOutboxQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As MbzOutboxQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esMbzOutboxCollection
		Inherits esEntityCollection(Of MbzOutbox)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return MbzOutboxMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "MbzOutboxCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As MbzOutboxQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New MbzOutboxQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As MbzOutboxQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New MbzOutboxQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As MbzOutboxQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, MbzOutboxQuery))
		End Sub
		
		#End Region
						
		Private m_query As MbzOutboxQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esMbzOutboxQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return MbzOutboxMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "Messageid" 
					Return Me.Messageid
				Case "Messagetext" 
					Return Me.Messagetext
				Case "Senderid" 
					Return Me.Senderid
				Case "Deliveryreport" 
					Return Me.Deliveryreport
				Case "Flash" 
					Return Me.Flash
				Case "Recipientid" 
					Return Me.Recipientid
				Case "Retrycount" 
					Return Me.Retrycount
				Case "Priority" 
					Return Me.Priority
				Case "Userid" 
					Return Me.Userid
				Case "Accountid" 
					Return Me.Accountid
				Case "Inprocess" 
					Return Me.Inprocess
				Case "Processstartedat" 
					Return Me.Processstartedat
				Case "Createdat" 
					Return Me.Createdat
				Case "Scheduledat" 
					Return Me.Scheduledat
				Case "Alertid" 
					Return Me.Alertid
				Case "Isencrypted" 
					Return Me.Isencrypted
				Case "Referencecolumn" 
					Return Me.Referencecolumn
				Case "AdtCreateuser" 
					Return Me.AdtCreateuser
				Case "AdtCreatedate" 
					Return Me.AdtCreatedate
				Case "AdtModifieduser" 
					Return Me.AdtModifieduser
				Case "AdtModifieddate" 
					Return Me.AdtModifieddate
				Case "AdtRoutingstatus" 
					Return Me.AdtRoutingstatus
				Case "AdtLastroutinguser" 
					Return Me.AdtLastroutinguser
				Case "AdtLastroutingdate" 
					Return Me.AdtLastroutingdate
				Case "Status" 
					Return Me.Status
				Case "EmailStatus" 
					Return Me.EmailStatus
				Case "CmnId" 
					Return Me.CmnId
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property Messageid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Messageid, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Messagetext As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Messagetext, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Senderid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Senderid, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Deliveryreport As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Deliveryreport, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Flash As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Flash, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Recipientid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Recipientid, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Retrycount As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Retrycount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Priority As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Priority, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Userid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Userid, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Accountid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Accountid, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Inprocess As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Inprocess, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Processstartedat As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Processstartedat, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Createdat As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Createdat, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Scheduledat As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Scheduledat, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Alertid As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Alertid, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Isencrypted As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Isencrypted, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Referencecolumn As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Referencecolumn, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AdtCreateuser As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtCreateuser, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AdtCreatedate As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtCreatedate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AdtModifieduser As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtModifieduser, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AdtModifieddate As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtModifieddate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AdtRoutingstatus As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtRoutingstatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AdtLastroutinguser As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtLastroutinguser, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AdtLastroutingdate As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.AdtLastroutingdate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.Status, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property EmailStatus As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.EmailStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CmnId As esQueryItem
			Get
				Return New esQueryItem(Me, MbzOutboxMetadata.ColumnNames.CmnId, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class MbzOutbox 
		Inherits esMbzOutbox
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class MbzOutboxMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Messageid, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Messageid
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 32
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Messagetext, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Messagetext
			c.CharacterMaxLength = 640
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Senderid, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Senderid
			c.CharacterMaxLength = 11
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Deliveryreport, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Deliveryreport
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Flash, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Flash
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Recipientid, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Recipientid
			c.CharacterMaxLength = 30
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Retrycount, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Retrycount
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Priority, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Priority
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Userid, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Userid
			c.CharacterMaxLength = 30
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Accountid, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Accountid
			c.CharacterMaxLength = 30
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Inprocess, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Inprocess
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Processstartedat, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Processstartedat
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Createdat, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Createdat
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Scheduledat, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Scheduledat
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Alertid, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Alertid
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Isencrypted, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Isencrypted
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Referencecolumn, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Referencecolumn
			c.CharacterMaxLength = 60
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtCreateuser, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtCreateuser
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtCreatedate, 18, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtCreatedate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtModifieduser, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtModifieduser
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtModifieddate, 20, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtModifieddate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtRoutingstatus, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtRoutingstatus
			c.CharacterMaxLength = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtLastroutinguser, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtLastroutinguser
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.AdtLastroutingdate, 23, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.AdtLastroutingdate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.Status, 24, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.Status
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.EmailStatus, 25, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.EmailStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MbzOutboxMetadata.ColumnNames.CmnId, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = MbzOutboxMetadata.PropertyNames.CmnId
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As MbzOutboxMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const Messageid As String = "MESSAGEID"
			 Public Const Messagetext As String = "MESSAGETEXT"
			 Public Const Senderid As String = "SENDERID"
			 Public Const Deliveryreport As String = "DELIVERYREPORT"
			 Public Const Flash As String = "FLASH"
			 Public Const Recipientid As String = "RECIPIENTID"
			 Public Const Retrycount As String = "RETRYCOUNT"
			 Public Const Priority As String = "PRIORITY"
			 Public Const Userid As String = "USERID"
			 Public Const Accountid As String = "ACCOUNTID"
			 Public Const Inprocess As String = "INPROCESS"
			 Public Const Processstartedat As String = "PROCESSSTARTEDAT"
			 Public Const Createdat As String = "CREATEDAT"
			 Public Const Scheduledat As String = "SCHEDULEDAT"
			 Public Const Alertid As String = "ALERTID"
			 Public Const Isencrypted As String = "ISENCRYPTED"
			 Public Const Referencecolumn As String = "REFERENCECOLUMN"
			 Public Const AdtCreateuser As String = "ADT_CREATEUSER"
			 Public Const AdtCreatedate As String = "ADT_CREATEDATE"
			 Public Const AdtModifieduser As String = "ADT_MODIFIEDUSER"
			 Public Const AdtModifieddate As String = "ADT_MODIFIEDDATE"
			 Public Const AdtRoutingstatus As String = "ADT_ROUTINGSTATUS"
			 Public Const AdtLastroutinguser As String = "ADT_LASTROUTINGUSER"
			 Public Const AdtLastroutingdate As String = "ADT_LASTROUTINGDATE"
			 Public Const Status As String = "Status"
			 Public Const EmailStatus As String = "EmailStatus"
			 Public Const CmnId As String = "CMN_ID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const Messageid As String = "Messageid"
			 Public Const Messagetext As String = "Messagetext"
			 Public Const Senderid As String = "Senderid"
			 Public Const Deliveryreport As String = "Deliveryreport"
			 Public Const Flash As String = "Flash"
			 Public Const Recipientid As String = "Recipientid"
			 Public Const Retrycount As String = "Retrycount"
			 Public Const Priority As String = "Priority"
			 Public Const Userid As String = "Userid"
			 Public Const Accountid As String = "Accountid"
			 Public Const Inprocess As String = "Inprocess"
			 Public Const Processstartedat As String = "Processstartedat"
			 Public Const Createdat As String = "Createdat"
			 Public Const Scheduledat As String = "Scheduledat"
			 Public Const Alertid As String = "Alertid"
			 Public Const Isencrypted As String = "Isencrypted"
			 Public Const Referencecolumn As String = "Referencecolumn"
			 Public Const AdtCreateuser As String = "AdtCreateuser"
			 Public Const AdtCreatedate As String = "AdtCreatedate"
			 Public Const AdtModifieduser As String = "AdtModifieduser"
			 Public Const AdtModifieddate As String = "AdtModifieddate"
			 Public Const AdtRoutingstatus As String = "AdtRoutingstatus"
			 Public Const AdtLastroutinguser As String = "AdtLastroutinguser"
			 Public Const AdtLastroutingdate As String = "AdtLastroutingdate"
			 Public Const Status As String = "Status"
			 Public Const EmailStatus As String = "EmailStatus"
			 Public Const CmnId As String = "CmnId"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(MbzOutboxMetadata)
			
				If MbzOutboxMetadata.mapDelegates Is Nothing Then
					MbzOutboxMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If MbzOutboxMetadata._meta Is Nothing Then
					MbzOutboxMetadata._meta = New MbzOutboxMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("Messageid", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Messagetext", new esTypeMap("nvarchar", "System.String"))
				meta.AddTypeMap("Senderid", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Deliveryreport", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Flash", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Recipientid", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Retrycount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Priority", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Userid", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Accountid", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Inprocess", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Processstartedat", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Createdat", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Scheduledat", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Alertid", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Isencrypted", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Referencecolumn", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AdtCreateuser", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AdtCreatedate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AdtModifieduser", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AdtModifieddate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AdtRoutingstatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AdtLastroutinguser", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AdtLastroutingdate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Status", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("EmailStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CmnId", new esTypeMap("nchar", "System.String"))			
				
				
				 
				meta.Source = "MBZ_OUTBOX"
				meta.Destination = "MBZ_OUTBOX"
				
				meta.spInsert = "proc_MBZ_OUTBOXInsert"
				meta.spUpdate = "proc_MBZ_OUTBOXUpdate"
				meta.spDelete = "proc_MBZ_OUTBOXDelete"
				meta.spLoadAll = "proc_MBZ_OUTBOXLoadAll"
				meta.spLoadByPrimaryKey = "proc_MBZ_OUTBOXLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As MbzOutboxMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
