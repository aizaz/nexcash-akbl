
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionInvoiceDataTemplate' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionInvoiceDataTemplate))> _
	<XmlType("ICCollectionInvoiceDataTemplate")> _	
	Partial Public Class ICCollectionInvoiceDataTemplate 
		Inherits esICCollectionInvoiceDataTemplate
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionInvoiceDataTemplate()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionInvoiceTemplateID As System.Int32)
			Dim obj As New ICCollectionInvoiceDataTemplate()
			obj.CollectionInvoiceTemplateID = collectionInvoiceTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionInvoiceTemplateID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionInvoiceDataTemplate()
			obj.CollectionInvoiceTemplateID = collectionInvoiceTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionInvoiceDataTemplateCollection")> _
	Partial Public Class ICCollectionInvoiceDataTemplateCollection
		Inherits esICCollectionInvoiceDataTemplateCollection
		Implements IEnumerable(Of ICCollectionInvoiceDataTemplate)
	
		Public Function FindByPrimaryKey(ByVal collectionInvoiceTemplateID As System.Int32) As ICCollectionInvoiceDataTemplate
			Return MyBase.SingleOrDefault(Function(e) e.CollectionInvoiceTemplateID.HasValue AndAlso e.CollectionInvoiceTemplateID.Value = collectionInvoiceTemplateID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionInvoiceDataTemplate))> _
		Public Class ICCollectionInvoiceDataTemplateCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionInvoiceDataTemplateCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionInvoiceDataTemplateCollectionWCFPacket) As ICCollectionInvoiceDataTemplateCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionInvoiceDataTemplateCollection) As ICCollectionInvoiceDataTemplateCollectionWCFPacket
				Return New ICCollectionInvoiceDataTemplateCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionInvoiceDataTemplateQuery 
		Inherits esICCollectionInvoiceDataTemplateQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionInvoiceDataTemplateQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionInvoiceDataTemplateQuery) As String
			Return ICCollectionInvoiceDataTemplateQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionInvoiceDataTemplateQuery
			Return DirectCast(ICCollectionInvoiceDataTemplateQuery.SerializeHelper.FromXml(query, GetType(ICCollectionInvoiceDataTemplateQuery)), ICCollectionInvoiceDataTemplateQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionInvoiceDataTemplate
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionInvoiceTemplateID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionInvoiceTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionInvoiceTemplateID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionInvoiceTemplateID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionInvoiceTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionInvoiceTemplateID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionInvoiceTemplateID As System.Int32) As Boolean
		
			Dim query As New ICCollectionInvoiceDataTemplateQuery()
			query.Where(query.CollectionInvoiceTemplateID = collectionInvoiceTemplateID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionInvoiceTemplateID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionInvoiceTemplateID", collectionInvoiceTemplateID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.CollectionInvoiceTemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionInvoiceTemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CollectionInvoiceTemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CollectionInvoiceTemplateID, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CollectionInvoiceTemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.TemplateName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateName, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.TemplateFormat
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateFormat As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateFormat)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateFormat, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateFormat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.SheetName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SheetName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.SheetName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.SheetName, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.SheetName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.FieldDelimiter
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDelimiter As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FieldDelimiter)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FieldDelimiter, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.FieldDelimiter)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.isFixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsFixLength As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsFixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsFixLength, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsFixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.FileUploadTemplateCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileUploadTemplateCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FileUploadTemplateCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FileUploadTemplateCode, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.FileUploadTemplateCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.HeaderIdentifier
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HeaderIdentifier As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.HeaderIdentifier)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.HeaderIdentifier, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.HeaderIdentifier)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.DetailIdentifier
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailIdentifier As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.DetailIdentifier)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.DetailIdentifier, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.DetailIdentifier)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionInvoiceDataTemplate.TemplateType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateType, value) Then
					OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionInvoiceTemplateID"
							Me.str().CollectionInvoiceTemplateID = CType(value, string)
												
						Case "TemplateName"
							Me.str().TemplateName = CType(value, string)
												
						Case "TemplateFormat"
							Me.str().TemplateFormat = CType(value, string)
												
						Case "SheetName"
							Me.str().SheetName = CType(value, string)
												
						Case "FieldDelimiter"
							Me.str().FieldDelimiter = CType(value, string)
												
						Case "IsFixLength"
							Me.str().IsFixLength = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "FileUploadTemplateCode"
							Me.str().FileUploadTemplateCode = CType(value, string)
												
						Case "HeaderIdentifier"
							Me.str().HeaderIdentifier = CType(value, string)
												
						Case "DetailIdentifier"
							Me.str().DetailIdentifier = CType(value, string)
												
						Case "TemplateType"
							Me.str().TemplateType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CollectionInvoiceTemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionInvoiceTemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CollectionInvoiceTemplateID)
							End If
						
						Case "IsFixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsFixLength = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsFixLength)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsApproved)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionInvoiceDataTemplate)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionInvoiceTemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionInvoiceTemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionInvoiceTemplateID = Nothing
					Else
						entity.CollectionInvoiceTemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateName As System.String 
				Get
					Dim data_ As System.String = entity.TemplateName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateName = Nothing
					Else
						entity.TemplateName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateFormat As System.String 
				Get
					Dim data_ As System.String = entity.TemplateFormat
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateFormat = Nothing
					Else
						entity.TemplateFormat = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SheetName As System.String 
				Get
					Dim data_ As System.String = entity.SheetName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SheetName = Nothing
					Else
						entity.SheetName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldDelimiter As System.String 
				Get
					Dim data_ As System.String = entity.FieldDelimiter
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDelimiter = Nothing
					Else
						entity.FieldDelimiter = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsFixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsFixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsFixLength = Nothing
					Else
						entity.IsFixLength = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileUploadTemplateCode As System.String 
				Get
					Dim data_ As System.String = entity.FileUploadTemplateCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileUploadTemplateCode = Nothing
					Else
						entity.FileUploadTemplateCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property HeaderIdentifier As System.String 
				Get
					Dim data_ As System.String = entity.HeaderIdentifier
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HeaderIdentifier = Nothing
					Else
						entity.HeaderIdentifier = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailIdentifier As System.String 
				Get
					Dim data_ As System.String = entity.DetailIdentifier
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailIdentifier = Nothing
					Else
						entity.DetailIdentifier = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateType As System.String 
				Get
					Dim data_ As System.String = entity.TemplateType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateType = Nothing
					Else
						entity.TemplateType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionInvoiceDataTemplate
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionInvoiceDataTemplateMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionInvoiceDataTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionInvoiceDataTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionInvoiceDataTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionInvoiceDataTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionInvoiceDataTemplateQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionInvoiceDataTemplateCollection
		Inherits esEntityCollection(Of ICCollectionInvoiceDataTemplate)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionInvoiceDataTemplateMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionInvoiceDataTemplateCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionInvoiceDataTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionInvoiceDataTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionInvoiceDataTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionInvoiceDataTemplateQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionInvoiceDataTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionInvoiceDataTemplateQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionInvoiceDataTemplateQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionInvoiceDataTemplateQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionInvoiceDataTemplateMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionInvoiceTemplateID" 
					Return Me.CollectionInvoiceTemplateID
				Case "TemplateName" 
					Return Me.TemplateName
				Case "TemplateFormat" 
					Return Me.TemplateFormat
				Case "SheetName" 
					Return Me.SheetName
				Case "FieldDelimiter" 
					Return Me.FieldDelimiter
				Case "IsFixLength" 
					Return Me.IsFixLength
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "FileUploadTemplateCode" 
					Return Me.FileUploadTemplateCode
				Case "HeaderIdentifier" 
					Return Me.HeaderIdentifier
				Case "DetailIdentifier" 
					Return Me.DetailIdentifier
				Case "TemplateType" 
					Return Me.TemplateType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionInvoiceTemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CollectionInvoiceTemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateFormat As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateFormat, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SheetName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.SheetName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldDelimiter As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FieldDelimiter, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsFixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsFixLength, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FileUploadTemplateCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FileUploadTemplateCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property HeaderIdentifier As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.HeaderIdentifier, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailIdentifier As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.DetailIdentifier, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionInvoiceDataTemplate 
		Inherits esICCollectionInvoiceDataTemplate
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionInvoiceDataTemplateMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CollectionInvoiceTemplateID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CollectionInvoiceTemplateID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateFormat, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateFormat
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.SheetName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.SheetName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FieldDelimiter, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.FieldDelimiter
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsFixLength, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsFixLength
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.CreateDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.ApprovedOn, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.IsApproved, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.FileUploadTemplateCode, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.FileUploadTemplateCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.HeaderIdentifier, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.HeaderIdentifier
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.DetailIdentifier, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.DetailIdentifier
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionInvoiceDataTemplateMetadata.ColumnNames.TemplateType, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionInvoiceDataTemplateMetadata.PropertyNames.TemplateType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionInvoiceDataTemplateMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionInvoiceTemplateID As String = "CollectionInvoiceTemplateID"
			 Public Const TemplateName As String = "TemplateName"
			 Public Const TemplateFormat As String = "TemplateFormat"
			 Public Const SheetName As String = "SheetName"
			 Public Const FieldDelimiter As String = "FieldDelimiter"
			 Public Const IsFixLength As String = "isFixLength"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "isApproved"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const HeaderIdentifier As String = "HeaderIdentifier"
			 Public Const DetailIdentifier As String = "DetailIdentifier"
			 Public Const TemplateType As String = "TemplateType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionInvoiceTemplateID As String = "CollectionInvoiceTemplateID"
			 Public Const TemplateName As String = "TemplateName"
			 Public Const TemplateFormat As String = "TemplateFormat"
			 Public Const SheetName As String = "SheetName"
			 Public Const FieldDelimiter As String = "FieldDelimiter"
			 Public Const IsFixLength As String = "IsFixLength"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const HeaderIdentifier As String = "HeaderIdentifier"
			 Public Const DetailIdentifier As String = "DetailIdentifier"
			 Public Const TemplateType As String = "TemplateType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionInvoiceDataTemplateMetadata)
			
				If ICCollectionInvoiceDataTemplateMetadata.mapDelegates Is Nothing Then
					ICCollectionInvoiceDataTemplateMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionInvoiceDataTemplateMetadata._meta Is Nothing Then
					ICCollectionInvoiceDataTemplateMetadata._meta = New ICCollectionInvoiceDataTemplateMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionInvoiceTemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TemplateName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateFormat", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SheetName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldDelimiter", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsFixLength", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FileUploadTemplateCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("HeaderIdentifier", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailIdentifier", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CollectionInvoiceDataTemplate"
				meta.Destination = "IC_CollectionInvoiceDataTemplate"
				
				meta.spInsert = "proc_IC_CollectionInvoiceDataTemplateInsert"
				meta.spUpdate = "proc_IC_CollectionInvoiceDataTemplateUpdate"
				meta.spDelete = "proc_IC_CollectionInvoiceDataTemplateDelete"
				meta.spLoadAll = "proc_IC_CollectionInvoiceDataTemplateLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionInvoiceDataTemplateLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionInvoiceDataTemplateMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
