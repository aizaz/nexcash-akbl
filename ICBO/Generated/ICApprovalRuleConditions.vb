
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/21/2015 11:37:52 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ApprovalRuleConditions' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICApprovalRuleConditions))> _
	<XmlType("ICApprovalRuleConditions")> _	
	Partial Public Class ICApprovalRuleConditions 
		Inherits esICApprovalRuleConditions
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICApprovalRuleConditions()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal approvalRuleConditionID As System.Int32)
			Dim obj As New ICApprovalRuleConditions()
			obj.ApprovalRuleConditionID = approvalRuleConditionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal approvalRuleConditionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICApprovalRuleConditions()
			obj.ApprovalRuleConditionID = approvalRuleConditionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICApprovalRuleConditionsCollection")> _
	Partial Public Class ICApprovalRuleConditionsCollection
		Inherits esICApprovalRuleConditionsCollection
		Implements IEnumerable(Of ICApprovalRuleConditions)
	
		Public Function FindByPrimaryKey(ByVal approvalRuleConditionID As System.Int32) As ICApprovalRuleConditions
			Return MyBase.SingleOrDefault(Function(e) e.ApprovalRuleConditionID.HasValue AndAlso e.ApprovalRuleConditionID.Value = approvalRuleConditionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICApprovalRuleConditions))> _
		Public Class ICApprovalRuleConditionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICApprovalRuleConditionsCollection)
			
			Public Shared Widening Operator CType(packet As ICApprovalRuleConditionsCollectionWCFPacket) As ICApprovalRuleConditionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICApprovalRuleConditionsCollection) As ICApprovalRuleConditionsCollectionWCFPacket
				Return New ICApprovalRuleConditionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICApprovalRuleConditionsQuery 
		Inherits esICApprovalRuleConditionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICApprovalRuleConditionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICApprovalRuleConditionsQuery) As String
			Return ICApprovalRuleConditionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICApprovalRuleConditionsQuery
			Return DirectCast(ICApprovalRuleConditionsQuery.SerializeHelper.FromXml(query, GetType(ICApprovalRuleConditionsQuery)), ICApprovalRuleConditionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRuleConditions
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal approvalRuleConditionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleConditionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleConditionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal approvalRuleConditionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleConditionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleConditionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal approvalRuleConditionID As System.Int32) As Boolean
		
			Dim query As New ICApprovalRuleConditionsQuery()
			query.Where(query.ApprovalRuleConditionID = approvalRuleConditionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal approvalRuleConditionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ApprovalRuleConditionID", approvalRuleConditionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.ApprovalRuleConditionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleConditionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleConditionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleConditionID, value) Then
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleConditionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.ApprovalRuleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleID, value) Then
					Me._UpToICApprovalRuleByApprovalRuleID = Nothing
					Me.OnPropertyChanged("UpToICApprovalRuleByApprovalRuleID")
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.SelectionCriteria
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SelectionCriteria As System.String
			Get
				Return MyBase.GetSystemString(ICApprovalRuleConditionsMetadata.ColumnNames.SelectionCriteria)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICApprovalRuleConditionsMetadata.ColumnNames.SelectionCriteria, value) Then
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.SelectionCriteria)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.ConditionType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ConditionType As System.String
			Get
				Return MyBase.GetSystemString(ICApprovalRuleConditionsMetadata.ColumnNames.ConditionType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICApprovalRuleConditionsMetadata.ColumnNames.ConditionType, value) Then
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ConditionType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.ApprovalGroupID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalGroupID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalGroupID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalGroupID, value) Then
					Me._UpToICApprovalGroupManagementByApprovalGroupID = Nothing
					Me.OnPropertyChanged("UpToICApprovalGroupManagementByApprovalGroupID")
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalGroupID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.Creator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creator As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.Creator)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.Creator, value) Then
					Me._UpToICUserByCreator = Nothing
					Me.OnPropertyChanged("UpToICUserByCreator")
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.Creator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleConditionsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleConditionsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditions.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICApprovalGroupManagementByApprovalGroupID As ICApprovalGroupManagement
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICApprovalRuleByApprovalRuleID As ICApprovalRule
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreator As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ApprovalRuleConditionID"
							Me.str().ApprovalRuleConditionID = CType(value, string)
												
						Case "ApprovalRuleID"
							Me.str().ApprovalRuleID = CType(value, string)
												
						Case "SelectionCriteria"
							Me.str().SelectionCriteria = CType(value, string)
												
						Case "ConditionType"
							Me.str().ConditionType = CType(value, string)
												
						Case "ApprovalGroupID"
							Me.str().ApprovalGroupID = CType(value, string)
												
						Case "Creator"
							Me.str().Creator = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ApprovalRuleConditionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalRuleConditionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleConditionID)
							End If
						
						Case "ApprovalRuleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalRuleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleID)
							End If
						
						Case "ApprovalGroupID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalGroupID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalGroupID)
							End If
						
						Case "Creator"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creator = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.Creator)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleConditionsMetadata.PropertyNames.CreatedDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICApprovalRuleConditions)
				Me.entity = entity
			End Sub				
		
	
			Public Property ApprovalRuleConditionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalRuleConditionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleConditionID = Nothing
					Else
						entity.ApprovalRuleConditionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalRuleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalRuleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleID = Nothing
					Else
						entity.ApprovalRuleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SelectionCriteria As System.String 
				Get
					Dim data_ As System.String = entity.SelectionCriteria
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SelectionCriteria = Nothing
					Else
						entity.SelectionCriteria = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ConditionType As System.String 
				Get
					Dim data_ As System.String = entity.ConditionType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ConditionType = Nothing
					Else
						entity.ConditionType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalGroupID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalGroupID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalGroupID = Nothing
					Else
						entity.ApprovalGroupID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creator As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creator
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creator = Nothing
					Else
						entity.Creator = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICApprovalRuleConditions
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICApprovalRuleConditionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleConditionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICApprovalRuleConditionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICApprovalRuleConditionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICApprovalRuleConditionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRuleConditionsCollection
		Inherits esEntityCollection(Of ICApprovalRuleConditions)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICApprovalRuleConditionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICApprovalRuleConditionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleConditionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICApprovalRuleConditionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICApprovalRuleConditionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICApprovalRuleConditionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICApprovalRuleConditionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICApprovalRuleConditionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICApprovalRuleConditionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ApprovalRuleConditionID" 
					Return Me.ApprovalRuleConditionID
				Case "ApprovalRuleID" 
					Return Me.ApprovalRuleID
				Case "SelectionCriteria" 
					Return Me.SelectionCriteria
				Case "ConditionType" 
					Return Me.ConditionType
				Case "ApprovalGroupID" 
					Return Me.ApprovalGroupID
				Case "Creator" 
					Return Me.Creator
				Case "CreationDate" 
					Return Me.CreationDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ApprovalRuleConditionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleConditionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalRuleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SelectionCriteria As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.SelectionCriteria, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ConditionType As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.ConditionType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalGroupID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalGroupID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creator As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.Creator, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICApprovalRuleConditions 
		Inherits esICApprovalRuleConditions
		
	
		#Region "ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICApprovalRuleConditions.ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID_Delegate)
				map.PropertyName = "ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID"
				map.MyColumnName = "ApprovaRuleCondID"
				map.ParentColumnName = "ApprovalRuleConditionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICApprovalRuleConditionsQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleConditionUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleConditionUsersQuery), New ICApprovalRuleConditionUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ApprovalRuleConditionID = mee.ApprovaRuleCondID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRuleConditionUsers_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID As ICApprovalRuleConditionUsersCollection 
		
			Get
				If Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID Is Nothing Then
					Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID = New ICApprovalRuleConditionUsersCollection()
					Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID", Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID)
				
					If Not Me.ApprovalRuleConditionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID.Query.Where(Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID.Query.ApprovaRuleCondID = Me.ApprovalRuleConditionID)
							Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID.fks.Add(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovaRuleCondID, Me.ApprovalRuleConditionID)
					End If
				End If

				Return Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID
			End Get
			
			Set(ByVal value As ICApprovalRuleConditionUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID")
					Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID As ICApprovalRuleConditionUsersCollection
		#End Region

		#Region "UpToICApprovalGroupManagementByApprovalGroupID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_ApprovalGroupManagement
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICApprovalGroupManagementByApprovalGroupID As ICApprovalGroupManagement
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICApprovalGroupManagementByApprovalGroupID Is Nothing _
						 AndAlso Not ApprovalGroupID.Equals(Nothing)  Then
						
					Me._UpToICApprovalGroupManagementByApprovalGroupID = New ICApprovalGroupManagement()
					Me._UpToICApprovalGroupManagementByApprovalGroupID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICApprovalGroupManagementByApprovalGroupID", Me._UpToICApprovalGroupManagementByApprovalGroupID)
					Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.Where(Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.ApprovalGroupID = Me.ApprovalGroupID)
					Me._UpToICApprovalGroupManagementByApprovalGroupID.Query.Load()
				End If

				Return Me._UpToICApprovalGroupManagementByApprovalGroupID
			End Get
			
            Set(ByVal value As ICApprovalGroupManagement)
				Me.RemovePreSave("UpToICApprovalGroupManagementByApprovalGroupID")
				

				If value Is Nothing Then
				
					Me.ApprovalGroupID = Nothing
				
					Me._UpToICApprovalGroupManagementByApprovalGroupID = Nothing
				Else
				
					Me.ApprovalGroupID = value.ApprovalGroupID
					
					Me._UpToICApprovalGroupManagementByApprovalGroupID = value
					Me.SetPreSave("UpToICApprovalGroupManagementByApprovalGroupID", Me._UpToICApprovalGroupManagementByApprovalGroupID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICApprovalRuleByApprovalRuleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_ApprovalRule
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICApprovalRuleByApprovalRuleID As ICApprovalRule
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICApprovalRuleByApprovalRuleID Is Nothing _
						 AndAlso Not ApprovalRuleID.Equals(Nothing)  Then
						
					Me._UpToICApprovalRuleByApprovalRuleID = New ICApprovalRule()
					Me._UpToICApprovalRuleByApprovalRuleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICApprovalRuleByApprovalRuleID", Me._UpToICApprovalRuleByApprovalRuleID)
					Me._UpToICApprovalRuleByApprovalRuleID.Query.Where(Me._UpToICApprovalRuleByApprovalRuleID.Query.ApprovalRuleID = Me.ApprovalRuleID)
					Me._UpToICApprovalRuleByApprovalRuleID.Query.Load()
				End If

				Return Me._UpToICApprovalRuleByApprovalRuleID
			End Get
			
            Set(ByVal value As ICApprovalRule)
				Me.RemovePreSave("UpToICApprovalRuleByApprovalRuleID")
				

				If value Is Nothing Then
				
					Me.ApprovalRuleID = Nothing
				
					Me._UpToICApprovalRuleByApprovalRuleID = Nothing
				Else
				
					Me.ApprovalRuleID = value.ApprovalRuleID
					
					Me._UpToICApprovalRuleByApprovalRuleID = value
					Me.SetPreSave("UpToICApprovalRuleByApprovalRuleID", Me._UpToICApprovalRuleByApprovalRuleID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreator - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreator As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreator Is Nothing _
						 AndAlso Not Creator.Equals(Nothing)  Then
						
					Me._UpToICUserByCreator = New ICUser()
					Me._UpToICUserByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
					Me._UpToICUserByCreator.Query.Where(Me._UpToICUserByCreator.Query.UserID = Me.Creator)
					Me._UpToICUserByCreator.Query.Load()
				End If

				Return Me._UpToICUserByCreator
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreator")
				

				If value Is Nothing Then
				
					Me.Creator = Nothing
				
					Me._UpToICUserByCreator = Nothing
				Else
				
					Me.Creator = value.UserID
					
					Me._UpToICUserByCreator = value
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID"
					coll = Me.ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID", GetType(ICApprovalRuleConditionUsersCollection), New ICApprovalRuleConditionUsers()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICApprovalGroupManagementByApprovalGroupID Is Nothing Then
				Me.ApprovalGroupID = Me._UpToICApprovalGroupManagementByApprovalGroupID.ApprovalGroupID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICApprovalRuleByApprovalRuleID Is Nothing Then
				Me.ApprovalRuleID = Me._UpToICApprovalRuleByApprovalRuleID.ApprovalRuleID
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID Is Nothing Then
				Apply(Me._ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID, "ApprovaRuleCondID", Me.ApprovalRuleConditionID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICApprovalRuleConditionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleConditionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleConditionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalRuleID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.SelectionCriteria, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.SelectionCriteria
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.ConditionType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.ConditionType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalGroupID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.ApprovalGroupID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.Creator, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.Creator
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.CreationDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleConditionsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICApprovalRuleConditionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ApprovalRuleConditionID As String = "ApprovalRuleConditionID"
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const SelectionCriteria As String = "SelectionCriteria"
			 Public Const ConditionType As String = "ConditionType"
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ApprovalRuleConditionID As String = "ApprovalRuleConditionID"
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const SelectionCriteria As String = "SelectionCriteria"
			 Public Const ConditionType As String = "ConditionType"
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICApprovalRuleConditionsMetadata)
			
				If ICApprovalRuleConditionsMetadata.mapDelegates Is Nothing Then
					ICApprovalRuleConditionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICApprovalRuleConditionsMetadata._meta Is Nothing Then
					ICApprovalRuleConditionsMetadata._meta = New ICApprovalRuleConditionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ApprovalRuleConditionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovalRuleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SelectionCriteria", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ConditionType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ApprovalGroupID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creator", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_ApprovalRuleConditions"
				meta.Destination = "IC_ApprovalRuleConditions"
				
				meta.spInsert = "proc_IC_ApprovalRuleConditionsInsert"
				meta.spUpdate = "proc_IC_ApprovalRuleConditionsUpdate"
				meta.spDelete = "proc_IC_ApprovalRuleConditionsDelete"
				meta.spLoadAll = "proc_IC_ApprovalRuleConditionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ApprovalRuleConditionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICApprovalRuleConditionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
