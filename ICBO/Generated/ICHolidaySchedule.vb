
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:40 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_HolidaySchedule' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICHolidaySchedule))> _
	<XmlType("ICHolidaySchedule")> _	
	Partial Public Class ICHolidaySchedule 
		Inherits esICHolidaySchedule
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICHolidaySchedule()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal holidayScheduleID As System.Int32)
			Dim obj As New ICHolidaySchedule()
			obj.HolidayScheduleID = holidayScheduleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal holidayScheduleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICHolidaySchedule()
			obj.HolidayScheduleID = holidayScheduleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICHolidayScheduleCollection")> _
	Partial Public Class ICHolidayScheduleCollection
		Inherits esICHolidayScheduleCollection
		Implements IEnumerable(Of ICHolidaySchedule)
	
		Public Function FindByPrimaryKey(ByVal holidayScheduleID As System.Int32) As ICHolidaySchedule
			Return MyBase.SingleOrDefault(Function(e) e.HolidayScheduleID.HasValue AndAlso e.HolidayScheduleID.Value = holidayScheduleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICHolidaySchedule))> _
		Public Class ICHolidayScheduleCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICHolidayScheduleCollection)
			
			Public Shared Widening Operator CType(packet As ICHolidayScheduleCollectionWCFPacket) As ICHolidayScheduleCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICHolidayScheduleCollection) As ICHolidayScheduleCollectionWCFPacket
				Return New ICHolidayScheduleCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICHolidayScheduleQuery 
		Inherits esICHolidayScheduleQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICHolidayScheduleQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICHolidayScheduleQuery) As String
			Return ICHolidayScheduleQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICHolidayScheduleQuery
			Return DirectCast(ICHolidayScheduleQuery.SerializeHelper.FromXml(query, GetType(ICHolidayScheduleQuery)), ICHolidayScheduleQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICHolidaySchedule
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal holidayScheduleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(holidayScheduleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(holidayScheduleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal holidayScheduleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(holidayScheduleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(holidayScheduleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal holidayScheduleID As System.Int32) As Boolean
		
			Dim query As New ICHolidayScheduleQuery()
			query.Where(query.HolidayScheduleID = holidayScheduleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal holidayScheduleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("HolidayScheduleID", holidayScheduleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_HolidaySchedule.HolidayScheduleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayScheduleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICHolidayScheduleMetadata.ColumnNames.HolidayScheduleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICHolidayScheduleMetadata.ColumnNames.HolidayScheduleID, value) Then
					OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.HolidayScheduleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidaySchedule.HolidayID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICHolidayScheduleMetadata.ColumnNames.HolidayID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICHolidayScheduleMetadata.ColumnNames.HolidayID, value) Then
					Me._UpToICHolidayManagementByHolidayID = Nothing
					Me.OnPropertyChanged("UpToICHolidayManagementByHolidayID")
					OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.HolidayID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidaySchedule.ScheduleDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICHolidayScheduleMetadata.ColumnNames.ScheduleDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICHolidayScheduleMetadata.ColumnNames.ScheduleDate, value) Then
					OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.ScheduleDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICHolidayManagementByHolidayID As ICHolidayManagement
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "HolidayScheduleID"
							Me.str().HolidayScheduleID = CType(value, string)
												
						Case "HolidayID"
							Me.str().HolidayID = CType(value, string)
												
						Case "ScheduleDate"
							Me.str().ScheduleDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "HolidayScheduleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.HolidayScheduleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.HolidayScheduleID)
							End If
						
						Case "HolidayID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.HolidayID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.HolidayID)
							End If
						
						Case "ScheduleDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ScheduleDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICHolidayScheduleMetadata.PropertyNames.ScheduleDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICHolidaySchedule)
				Me.entity = entity
			End Sub				
		
	
			Public Property HolidayScheduleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.HolidayScheduleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayScheduleID = Nothing
					Else
						entity.HolidayScheduleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property HolidayID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.HolidayID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayID = Nothing
					Else
						entity.HolidayID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ScheduleDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ScheduleDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleDate = Nothing
					Else
						entity.ScheduleDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICHolidaySchedule
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICHolidayScheduleMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICHolidayScheduleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICHolidayScheduleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICHolidayScheduleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICHolidayScheduleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICHolidayScheduleQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICHolidayScheduleCollection
		Inherits esEntityCollection(Of ICHolidaySchedule)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICHolidayScheduleMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICHolidayScheduleCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICHolidayScheduleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICHolidayScheduleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICHolidayScheduleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICHolidayScheduleQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICHolidayScheduleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICHolidayScheduleQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICHolidayScheduleQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICHolidayScheduleQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICHolidayScheduleMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "HolidayScheduleID" 
					Return Me.HolidayScheduleID
				Case "HolidayID" 
					Return Me.HolidayID
				Case "ScheduleDate" 
					Return Me.ScheduleDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property HolidayScheduleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayScheduleMetadata.ColumnNames.HolidayScheduleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property HolidayID As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayScheduleMetadata.ColumnNames.HolidayID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ScheduleDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayScheduleMetadata.ColumnNames.ScheduleDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICHolidaySchedule 
		Inherits esICHolidaySchedule
		
	
		#Region "UpToICHolidayManagementByHolidayID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_HolidaySchedule_IC_HolidayManagement
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICHolidayManagementByHolidayID As ICHolidayManagement
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICHolidayManagementByHolidayID Is Nothing _
						 AndAlso Not HolidayID.Equals(Nothing)  Then
						
					Me._UpToICHolidayManagementByHolidayID = New ICHolidayManagement()
					Me._UpToICHolidayManagementByHolidayID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICHolidayManagementByHolidayID", Me._UpToICHolidayManagementByHolidayID)
					Me._UpToICHolidayManagementByHolidayID.Query.Where(Me._UpToICHolidayManagementByHolidayID.Query.HolidayID = Me.HolidayID)
					Me._UpToICHolidayManagementByHolidayID.Query.Load()
				End If

				Return Me._UpToICHolidayManagementByHolidayID
			End Get
			
            Set(ByVal value As ICHolidayManagement)
				Me.RemovePreSave("UpToICHolidayManagementByHolidayID")
				

				If value Is Nothing Then
				
					Me.HolidayID = Nothing
				
					Me._UpToICHolidayManagementByHolidayID = Nothing
				Else
				
					Me.HolidayID = value.HolidayID
					
					Me._UpToICHolidayManagementByHolidayID = value
					Me.SetPreSave("UpToICHolidayManagementByHolidayID", Me._UpToICHolidayManagementByHolidayID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICHolidayManagementByHolidayID Is Nothing Then
				Me.HolidayID = Me._UpToICHolidayManagementByHolidayID.HolidayID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICHolidayScheduleMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICHolidayScheduleMetadata.ColumnNames.HolidayScheduleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICHolidayScheduleMetadata.PropertyNames.HolidayScheduleID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayScheduleMetadata.ColumnNames.HolidayID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICHolidayScheduleMetadata.PropertyNames.HolidayID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayScheduleMetadata.ColumnNames.ScheduleDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICHolidayScheduleMetadata.PropertyNames.ScheduleDate
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICHolidayScheduleMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const HolidayScheduleID As String = "HolidayScheduleID"
			 Public Const HolidayID As String = "HolidayID"
			 Public Const ScheduleDate As String = "ScheduleDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const HolidayScheduleID As String = "HolidayScheduleID"
			 Public Const HolidayID As String = "HolidayID"
			 Public Const ScheduleDate As String = "ScheduleDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICHolidayScheduleMetadata)
			
				If ICHolidayScheduleMetadata.mapDelegates Is Nothing Then
					ICHolidayScheduleMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICHolidayScheduleMetadata._meta Is Nothing Then
					ICHolidayScheduleMetadata._meta = New ICHolidayScheduleMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("HolidayScheduleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("HolidayID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ScheduleDate", new esTypeMap("date", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_HolidaySchedule"
				meta.Destination = "IC_HolidaySchedule"
				
				meta.spInsert = "proc_IC_HolidayScheduleInsert"
				meta.spUpdate = "proc_IC_HolidayScheduleUpdate"
				meta.spDelete = "proc_IC_HolidayScheduleDelete"
				meta.spLoadAll = "proc_IC_HolidayScheduleLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_HolidayScheduleLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICHolidayScheduleMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
