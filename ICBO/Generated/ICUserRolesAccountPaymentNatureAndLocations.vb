
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_UserRolesAccountPaymentNatureAndLocations' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUserRolesAccountPaymentNatureAndLocations))> _
	<XmlType("ICUserRolesAccountPaymentNatureAndLocations")> _	
	Partial Public Class ICUserRolesAccountPaymentNatureAndLocations 
		Inherits esICUserRolesAccountPaymentNatureAndLocations
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUserRolesAccountPaymentNatureAndLocations()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32)
			Dim obj As New ICUserRolesAccountPaymentNatureAndLocations()
			obj.UserID = userID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.OfficeID = officeID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUserRolesAccountPaymentNatureAndLocations()
			obj.UserID = userID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.OfficeID = officeID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUserRolesAccountPaymentNatureAndLocationsCollection")> _
	Partial Public Class ICUserRolesAccountPaymentNatureAndLocationsCollection
		Inherits esICUserRolesAccountPaymentNatureAndLocationsCollection
		Implements IEnumerable(Of ICUserRolesAccountPaymentNatureAndLocations)
	
		Public Function FindByPrimaryKey(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32) As ICUserRolesAccountPaymentNatureAndLocations
			Return MyBase.SingleOrDefault(Function(e) e.UserID.HasValue AndAlso e.UserID.Value = userID And e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.PaymentNatureCode = paymentNatureCode And e.OfficeID.HasValue AndAlso e.OfficeID.Value = officeID And e.RoleID.HasValue AndAlso e.RoleID.Value = roleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUserRolesAccountPaymentNatureAndLocations))> _
		Public Class ICUserRolesAccountPaymentNatureAndLocationsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUserRolesAccountPaymentNatureAndLocationsCollection)
			
			Public Shared Widening Operator CType(packet As ICUserRolesAccountPaymentNatureAndLocationsCollectionWCFPacket) As ICUserRolesAccountPaymentNatureAndLocationsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUserRolesAccountPaymentNatureAndLocationsCollection) As ICUserRolesAccountPaymentNatureAndLocationsCollectionWCFPacket
				Return New ICUserRolesAccountPaymentNatureAndLocationsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUserRolesAccountPaymentNatureAndLocationsQuery 
		Inherits esICUserRolesAccountPaymentNatureAndLocationsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUserRolesAccountPaymentNatureAndLocationsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUserRolesAccountPaymentNatureAndLocationsQuery) As String
			Return ICUserRolesAccountPaymentNatureAndLocationsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUserRolesAccountPaymentNatureAndLocationsQuery
			Return DirectCast(ICUserRolesAccountPaymentNatureAndLocationsQuery.SerializeHelper.FromXml(query, GetType(ICUserRolesAccountPaymentNatureAndLocationsQuery)), ICUserRolesAccountPaymentNatureAndLocationsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesAccountPaymentNatureAndLocations
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID, accountNumber, branchCode, currency, paymentNatureCode, officeID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID, accountNumber, branchCode, currency, paymentNatureCode, officeID, roleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID, accountNumber, branchCode, currency, paymentNatureCode, officeID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID, accountNumber, branchCode, currency, paymentNatureCode, officeID, roleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim query As New ICUserRolesAccountPaymentNatureAndLocationsQuery()
			query.Where(query.UserID = userID And query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.PaymentNatureCode = paymentNatureCode And query.OfficeID = officeID And query.RoleID = roleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal officeID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UserID", userID)
						parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("PaymentNatureCode", paymentNatureCode)
						parms.Add("OfficeID", officeID)
						parms.Add("RoleID", roleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Currency, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.OfficeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.OfficeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.OfficeID, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.OfficeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.IsApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.IsApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.IsApprove, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.IsApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.ApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedDate, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
					Me.OnPropertyChanged("UpToICUserRolesAccountAndPaymentNatureByRoleID")
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountPaymentNatureAndLocations.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserRolesAccountAndPaymentNatureByRoleID As ICUserRolesAccountAndPaymentNature
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "OfficeID"
							Me.str().OfficeID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsApprove"
							Me.str().IsApprove = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedDate"
							Me.str().ApprovedDate = CType(value, string)
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.UserID)
							End If
						
						Case "OfficeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.OfficeID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.IsApprove)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedDate)
							End If
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.RoleID)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUserRolesAccountPaymentNatureAndLocations)
				Me.entity = entity
			End Sub				
		
	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeID = Nothing
					Else
						entity.OfficeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApprove = Nothing
					Else
						entity.IsApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedDate = Nothing
					Else
						entity.ApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUserRolesAccountPaymentNatureAndLocations
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountPaymentNatureAndLocationsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUserRolesAccountPaymentNatureAndLocationsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesAccountPaymentNatureAndLocationsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUserRolesAccountPaymentNatureAndLocationsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUserRolesAccountPaymentNatureAndLocationsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUserRolesAccountPaymentNatureAndLocationsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesAccountPaymentNatureAndLocationsCollection
		Inherits esEntityCollection(Of ICUserRolesAccountPaymentNatureAndLocations)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountPaymentNatureAndLocationsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUserRolesAccountPaymentNatureAndLocationsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUserRolesAccountPaymentNatureAndLocationsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesAccountPaymentNatureAndLocationsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUserRolesAccountPaymentNatureAndLocationsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUserRolesAccountPaymentNatureAndLocationsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUserRolesAccountPaymentNatureAndLocationsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUserRolesAccountPaymentNatureAndLocationsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUserRolesAccountPaymentNatureAndLocationsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUserRolesAccountPaymentNatureAndLocationsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountPaymentNatureAndLocationsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UserID" 
					Return Me.UserID
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "OfficeID" 
					Return Me.OfficeID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsApprove" 
					Return Me.IsApprove
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedDate" 
					Return Me.ApprovedDate
				Case "RoleID" 
					Return Me.RoleID
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.OfficeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.IsApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUserRolesAccountPaymentNatureAndLocations 
		Inherits esICUserRolesAccountPaymentNatureAndLocations
		
	
		#Region "UpToICUserRolesAccountAndPaymentNatureByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesAccountPaymentNatureAndLocations_IC_UserRolesAccountAndPaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserRolesAccountAndPaymentNatureByRoleID As ICUserRolesAccountAndPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserRolesAccountAndPaymentNatureByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  AndAlso Not UserID.Equals(Nothing)  AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = New ICUserRolesAccountAndPaymentNature()
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserRolesAccountAndPaymentNatureByRoleID", Me._UpToICUserRolesAccountAndPaymentNatureByRoleID)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.RolesID = Me.RoleID)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.UserID = Me.UserID)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.BranchCode = Me.BranchCode)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Where(Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Currency = Me.Currency)
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID.Query.Load()
				End If

				Return Me._UpToICUserRolesAccountAndPaymentNatureByRoleID
			End Get
			
            Set(ByVal value As ICUserRolesAccountAndPaymentNature)
				Me.RemovePreSave("UpToICUserRolesAccountAndPaymentNatureByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me.UserID = Nothing
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = Nothing
				Else
				
					Me.RoleID = value.RolesID
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me.UserID = value.UserID
					
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICUserRolesAccountAndPaymentNatureByRoleID = value
					Me.SetPreSave("UpToICUserRolesAccountAndPaymentNatureByRoleID", Me._UpToICUserRolesAccountAndPaymentNatureByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICUserRolesAccountPaymentNatureAndLocationsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.UserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.UserID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.AccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.BranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.PaymentNatureCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.OfficeID, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.OfficeID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreatedDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.IsApprove, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.IsApprove
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.ApprovedDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.ApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.RoleID, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.RoleID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Creater, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.CreationDate, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountPaymentNatureAndLocationsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUserRolesAccountPaymentNatureAndLocationsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const RoleID As String = "RoleID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const RoleID As String = "RoleID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUserRolesAccountPaymentNatureAndLocationsMetadata)
			
				If ICUserRolesAccountPaymentNatureAndLocationsMetadata.mapDelegates Is Nothing Then
					ICUserRolesAccountPaymentNatureAndLocationsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUserRolesAccountPaymentNatureAndLocationsMetadata._meta Is Nothing Then
					ICUserRolesAccountPaymentNatureAndLocationsMetadata._meta = New ICUserRolesAccountPaymentNatureAndLocationsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OfficeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_UserRolesAccountPaymentNatureAndLocations"
				meta.Destination = "IC_UserRolesAccountPaymentNatureAndLocations"
				
				meta.spInsert = "proc_IC_UserRolesAccountPaymentNatureAndLocationsInsert"
				meta.spUpdate = "proc_IC_UserRolesAccountPaymentNatureAndLocationsUpdate"
				meta.spDelete = "proc_IC_UserRolesAccountPaymentNatureAndLocationsDelete"
				meta.spLoadAll = "proc_IC_UserRolesAccountPaymentNatureAndLocationsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UserRolesAccountPaymentNatureAndLocationsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUserRolesAccountPaymentNatureAndLocationsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
