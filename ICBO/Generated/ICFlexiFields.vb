
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_FlexiFields' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFlexiFields))> _
	<XmlType("ICFlexiFields")> _	
	Partial Public Class ICFlexiFields 
		Inherits esICFlexiFields
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFlexiFields()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fieldID As System.Int32)
			Dim obj As New ICFlexiFields()
			obj.FieldID = fieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fieldID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFlexiFields()
			obj.FieldID = fieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFlexiFieldsCollection")> _
	Partial Public Class ICFlexiFieldsCollection
		Inherits esICFlexiFieldsCollection
		Implements IEnumerable(Of ICFlexiFields)
	
		Public Function FindByPrimaryKey(ByVal fieldID As System.Int32) As ICFlexiFields
			Return MyBase.SingleOrDefault(Function(e) e.FieldID.HasValue AndAlso e.FieldID.Value = fieldID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFlexiFields))> _
		Public Class ICFlexiFieldsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFlexiFieldsCollection)
			
			Public Shared Widening Operator CType(packet As ICFlexiFieldsCollectionWCFPacket) As ICFlexiFieldsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFlexiFieldsCollection) As ICFlexiFieldsCollectionWCFPacket
				Return New ICFlexiFieldsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFlexiFieldsQuery 
		Inherits esICFlexiFieldsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFlexiFieldsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFlexiFieldsQuery) As String
			Return ICFlexiFieldsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFlexiFieldsQuery
			Return DirectCast(ICFlexiFieldsQuery.SerializeHelper.FromXml(query, GetType(ICFlexiFieldsQuery)), ICFlexiFieldsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFlexiFields
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fieldID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fieldID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fieldID As System.Int32) As Boolean
		
			Dim query As New ICFlexiFieldsQuery()
			query.Where(query.FieldID = fieldID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fieldID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FieldID", fieldID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_FlexiFields.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.FieldTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldTitle As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.FieldTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.FieldTitle, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.ValExp
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ValExp As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.ValExp)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.ValExp, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.ValExp)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.ListOfValues
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ListOfValues As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.ListOfValues)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.ListOfValues, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.ListOfValues)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.IsRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsRequired, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.IsEnabled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsEnabled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsEnabled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsEnabled, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsEnabled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.FieldOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldOrder, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.FieldLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.FieldLength, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.HelpText
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HelpText As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.HelpText)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.HelpText, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.HelpText)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.DefaultValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DefaultValue As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldsMetadata.ColumnNames.DefaultValue)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldsMetadata.ColumnNames.DefaultValue, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.DefaultValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.CreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFlexiFieldsMetadata.ColumnNames.CreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFlexiFieldsMetadata.ColumnNames.CreatedOn, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreatedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFlexiFieldsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFields.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFlexiFieldsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFlexiFieldsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "FieldTitle"
							Me.str().FieldTitle = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "ValExp"
							Me.str().ValExp = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ListOfValues"
							Me.str().ListOfValues = CType(value, string)
												
						Case "IsRequired"
							Me.str().IsRequired = CType(value, string)
												
						Case "IsEnabled"
							Me.str().IsEnabled = CType(value, string)
												
						Case "FieldOrder"
							Me.str().FieldOrder = CType(value, string)
												
						Case "FieldLength"
							Me.str().FieldLength = CType(value, string)
												
						Case "HelpText"
							Me.str().HelpText = CType(value, string)
												
						Case "DefaultValue"
							Me.str().DefaultValue = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedOn"
							Me.str().CreatedOn = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "IsRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsRequired)
							End If
						
						Case "IsEnabled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsEnabled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsEnabled)
							End If
						
						Case "FieldOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldOrder)
							End If
						
						Case "FieldLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.FieldLength)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreatedOn)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFlexiFieldsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFlexiFields)
				Me.entity = entity
			End Sub				
		
	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldTitle As System.String 
				Get
					Dim data_ As System.String = entity.FieldTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldTitle = Nothing
					Else
						entity.FieldTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ValExp As System.String 
				Get
					Dim data_ As System.String = entity.ValExp
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ValExp = Nothing
					Else
						entity.ValExp = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ListOfValues As System.String 
				Get
					Dim data_ As System.String = entity.ListOfValues
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ListOfValues = Nothing
					Else
						entity.ListOfValues = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequired = Nothing
					Else
						entity.IsRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsEnabled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsEnabled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsEnabled = Nothing
					Else
						entity.IsEnabled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldOrder = Nothing
					Else
						entity.FieldOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldLength = Nothing
					Else
						entity.FieldLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property HelpText As System.String 
				Get
					Dim data_ As System.String = entity.HelpText
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HelpText = Nothing
					Else
						entity.HelpText = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DefaultValue As System.String 
				Get
					Dim data_ As System.String = entity.DefaultValue
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DefaultValue = Nothing
					Else
						entity.DefaultValue = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOn = Nothing
					Else
						entity.CreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFlexiFields
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFlexiFieldsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFlexiFieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFlexiFieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFlexiFieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFlexiFieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFlexiFieldsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFlexiFieldsCollection
		Inherits esEntityCollection(Of ICFlexiFields)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFlexiFieldsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFlexiFieldsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFlexiFieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFlexiFieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFlexiFieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFlexiFieldsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFlexiFieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFlexiFieldsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFlexiFieldsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFlexiFieldsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFlexiFieldsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FieldID" 
					Return Me.FieldID
				Case "FieldTitle" 
					Return Me.FieldTitle
				Case "FieldType" 
					Return Me.FieldType
				Case "ValExp" 
					Return Me.ValExp
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ListOfValues" 
					Return Me.ListOfValues
				Case "IsRequired" 
					Return Me.IsRequired
				Case "IsEnabled" 
					Return Me.IsEnabled
				Case "FieldOrder" 
					Return Me.FieldOrder
				Case "FieldLength" 
					Return Me.FieldLength
				Case "HelpText" 
					Return Me.HelpText
				Case "DefaultValue" 
					Return Me.DefaultValue
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedOn" 
					Return Me.CreatedOn
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.FieldTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ValExp As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.ValExp, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ListOfValues As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.ListOfValues, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.IsRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsEnabled As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.IsEnabled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FieldOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.FieldOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.FieldLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property HelpText As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.HelpText, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DefaultValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.DefaultValue, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.CreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFlexiFields 
		Inherits esICFlexiFields
		
	
		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FlexiFields_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICFlexiFieldsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.FieldID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.FieldID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.FieldTitle, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.FieldTitle
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.FieldType, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.ValExp, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.ValExp
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.CompanyCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.ListOfValues, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.ListOfValues
			c.CharacterMaxLength = 1000
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.IsRequired, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.IsRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.IsEnabled, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.IsEnabled
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.FieldOrder, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.FieldOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.FieldLength, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.FieldLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.HelpText, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.HelpText
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.DefaultValue, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.DefaultValue
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.CreatedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.CreatedOn, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.CreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.IsActive, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.Creater, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldsMetadata.ColumnNames.CreationDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFlexiFieldsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFlexiFieldsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldTitle As String = "FieldTitle"
			 Public Const FieldType As String = "FieldType"
			 Public Const ValExp As String = "ValExp"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ListOfValues As String = "ListOfValues"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsEnabled As String = "IsEnabled"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldLength As String = "FieldLength"
			 Public Const HelpText As String = "HelpText"
			 Public Const DefaultValue As String = "DefaultValue"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldTitle As String = "FieldTitle"
			 Public Const FieldType As String = "FieldType"
			 Public Const ValExp As String = "ValExp"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ListOfValues As String = "ListOfValues"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsEnabled As String = "IsEnabled"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldLength As String = "FieldLength"
			 Public Const HelpText As String = "HelpText"
			 Public Const DefaultValue As String = "DefaultValue"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFlexiFieldsMetadata)
			
				If ICFlexiFieldsMetadata.mapDelegates Is Nothing Then
					ICFlexiFieldsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFlexiFieldsMetadata._meta Is Nothing Then
					ICFlexiFieldsMetadata._meta = New ICFlexiFieldsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ValExp", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ListOfValues", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsEnabled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FieldOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("HelpText", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DefaultValue", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_FlexiFields"
				meta.Destination = "IC_FlexiFields"
				
				meta.spInsert = "proc_IC_FlexiFieldsInsert"
				meta.spUpdate = "proc_IC_FlexiFieldsUpdate"
				meta.spDelete = "proc_IC_FlexiFieldsDelete"
				meta.spLoadAll = "proc_IC_FlexiFieldsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FlexiFieldsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFlexiFieldsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
