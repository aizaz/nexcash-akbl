
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CBAccounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCBAccounts))> _
	<XmlType("ICCBAccounts")> _	
	Partial Public Class ICCBAccounts 
		Inherits esICCBAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCBAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String)
			Dim obj As New ICCBAccounts()
			obj.AccountNumber = accountNumber
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCBAccounts()
			obj.AccountNumber = accountNumber
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCBAccountsCollection")> _
	Partial Public Class ICCBAccountsCollection
		Inherits esICCBAccountsCollection
		Implements IEnumerable(Of ICCBAccounts)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String) As ICCBAccounts
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCBAccounts))> _
		Public Class ICCBAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCBAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICCBAccountsCollectionWCFPacket) As ICCBAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCBAccountsCollection) As ICCBAccountsCollectionWCFPacket
				Return New ICCBAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCBAccountsQuery 
		Inherits esICCBAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCBAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCBAccountsQuery) As String
			Return ICCBAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCBAccountsQuery
			Return DirectCast(ICCBAccountsQuery.SerializeHelper.FromXml(query, GetType(ICCBAccountsQuery)), ICCBAccountsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCBAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String) As Boolean
		
			Dim query As New ICCBAccountsQuery()
			query.Where(query.AccountNumber = accountNumber)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CBAccounts.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBAccountsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBAccountsMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBAccounts.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCBAccountsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBAccountsMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBAccounts.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICCBAccountsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBAccountsMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBAccounts.AccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICCBAccountsMetadata.ColumnNames.AccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBAccountsMetadata.ColumnNames.AccountTitle, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.AccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBAccounts.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As System.String
			Get
				Return MyBase.GetSystemString(ICCBAccountsMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBAccountsMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBAccounts.Balance
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Balance As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICCBAccountsMetadata.ColumnNames.Balance)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICCBAccountsMetadata.ColumnNames.Balance, value) Then
					OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.Balance)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "AccountTitle"
							Me.str().AccountTitle = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "Balance"
							Me.str().Balance = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "Balance"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Balance = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICCBAccountsMetadata.PropertyNames.Balance)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCBAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.AccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountTitle = Nothing
					Else
						entity.AccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As System.String = entity.Status
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Balance As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Balance
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Balance = Nothing
					Else
						entity.Balance = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCBAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCBAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCBAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCBAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCBAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCBAccountsCollection
		Inherits esEntityCollection(Of ICCBAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCBAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCBAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCBAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCBAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCBAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCBAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCBAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCBAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCBAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "AccountTitle" 
					Return Me.AccountTitle
				Case "Status" 
					Return Me.Status
				Case "Balance" 
					Return Me.Balance
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.AccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.Status, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Balance As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBAccountsMetadata.ColumnNames.Balance, esSystemType.Decimal)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCBAccounts 
		Inherits esICCBAccounts
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCBAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 300
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.AccountTitle, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.AccountTitle
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.Status, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.Status
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBAccountsMetadata.ColumnNames.Balance, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICCBAccountsMetadata.PropertyNames.Balance
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCBAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const Status As String = "Status"
			 Public Const Balance As String = "Balance"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const Status As String = "Status"
			 Public Const Balance As String = "Balance"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCBAccountsMetadata)
			
				If ICCBAccountsMetadata.mapDelegates Is Nothing Then
					ICCBAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCBAccountsMetadata._meta Is Nothing Then
					ICCBAccountsMetadata._meta = New ICCBAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Balance", new esTypeMap("numeric", "System.Decimal"))			
				
				
				 
				meta.Source = "IC_CBAccounts"
				meta.Destination = "IC_CBAccounts"
				
				meta.spInsert = "proc_IC_CBAccountsInsert"
				meta.spUpdate = "proc_IC_CBAccountsUpdate"
				meta.spDelete = "proc_IC_CBAccountsDelete"
				meta.spLoadAll = "proc_IC_CBAccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CBAccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCBAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
