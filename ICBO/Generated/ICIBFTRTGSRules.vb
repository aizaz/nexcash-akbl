
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_IBFT_RTGS_Rules' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICIBFTRTGSRules))> _
	<XmlType("ICIBFTRTGSRules")> _	
	Partial Public Class ICIBFTRTGSRules 
		Inherits esICIBFTRTGSRules
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICIBFTRTGSRules()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal iBFTRTGSRuleID As System.Int32)
			Dim obj As New ICIBFTRTGSRules()
			obj.IBFTRTGSRuleID = iBFTRTGSRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal iBFTRTGSRuleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICIBFTRTGSRules()
			obj.IBFTRTGSRuleID = iBFTRTGSRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICIBFTRTGSRulesCollection")> _
	Partial Public Class ICIBFTRTGSRulesCollection
		Inherits esICIBFTRTGSRulesCollection
		Implements IEnumerable(Of ICIBFTRTGSRules)
	
		Public Function FindByPrimaryKey(ByVal iBFTRTGSRuleID As System.Int32) As ICIBFTRTGSRules
			Return MyBase.SingleOrDefault(Function(e) e.IBFTRTGSRuleID.HasValue AndAlso e.IBFTRTGSRuleID.Value = iBFTRTGSRuleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICIBFTRTGSRules))> _
		Public Class ICIBFTRTGSRulesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICIBFTRTGSRulesCollection)
			
			Public Shared Widening Operator CType(packet As ICIBFTRTGSRulesCollectionWCFPacket) As ICIBFTRTGSRulesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICIBFTRTGSRulesCollection) As ICIBFTRTGSRulesCollectionWCFPacket
				Return New ICIBFTRTGSRulesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICIBFTRTGSRulesQuery 
		Inherits esICIBFTRTGSRulesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICIBFTRTGSRulesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICIBFTRTGSRulesQuery) As String
			Return ICIBFTRTGSRulesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICIBFTRTGSRulesQuery
			Return DirectCast(ICIBFTRTGSRulesQuery.SerializeHelper.FromXml(query, GetType(ICIBFTRTGSRulesQuery)), ICIBFTRTGSRulesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICIBFTRTGSRules
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal iBFTRTGSRuleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(iBFTRTGSRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(iBFTRTGSRuleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal iBFTRTGSRuleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(iBFTRTGSRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(iBFTRTGSRuleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal iBFTRTGSRuleID As System.Int32) As Boolean
		
			Dim query As New ICIBFTRTGSRulesQuery()
			query.Where(query.IBFTRTGSRuleID = iBFTRTGSRuleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal iBFTRTGSRuleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("IBFTRTGSRuleID", iBFTRTGSRuleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.IBFT_RTGSRuleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IBFTRTGSRuleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleID, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IBFTRTGSRuleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.IBFT_RTGSRuleName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IBFTRTGSRuleName As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleName, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IBFTRTGSRuleName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.FieldValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldValue As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldValue)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldValue, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.FieldValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.FieldCondition
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldCondition As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldCondition)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.FieldCondition, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.FieldCondition)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.DisbType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbType As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.DisbType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.DisbType, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.DisbType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As System.String
			Get
				Return MyBase.GetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICIBFTRTGSRulesMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICIBFTRTGSRulesMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICIBFTRTGSRulesMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICIBFTRTGSRulesMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICIBFTRTGSRulesMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICIBFTRTGSRulesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_IBFT_RTGS_Rules.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICIBFTRTGSRulesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "IBFTRTGSRuleID"
							Me.str().IBFTRTGSRuleID = CType(value, string)
												
						Case "IBFTRTGSRuleName"
							Me.str().IBFTRTGSRuleName = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldValue"
							Me.str().FieldValue = CType(value, string)
												
						Case "FieldCondition"
							Me.str().FieldCondition = CType(value, string)
												
						Case "DisbType"
							Me.str().DisbType = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "IBFTRTGSRuleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.IBFTRTGSRuleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IBFTRTGSRuleID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.IsApproved)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICIBFTRTGSRulesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICIBFTRTGSRules)
				Me.entity = entity
			End Sub				
		
	
			Public Property IBFTRTGSRuleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.IBFTRTGSRuleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IBFTRTGSRuleID = Nothing
					Else
						entity.IBFTRTGSRuleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IBFTRTGSRuleName As System.String 
				Get
					Dim data_ As System.String = entity.IBFTRTGSRuleName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IBFTRTGSRuleName = Nothing
					Else
						entity.IBFTRTGSRuleName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldValue As System.String 
				Get
					Dim data_ As System.String = entity.FieldValue
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldValue = Nothing
					Else
						entity.FieldValue = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldCondition As System.String 
				Get
					Dim data_ As System.String = entity.FieldCondition
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldCondition = Nothing
					Else
						entity.FieldCondition = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbType As System.String 
				Get
					Dim data_ As System.String = entity.DisbType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbType = Nothing
					Else
						entity.DisbType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As System.String = entity.CompanyCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICIBFTRTGSRules
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICIBFTRTGSRulesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICIBFTRTGSRulesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICIBFTRTGSRulesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICIBFTRTGSRulesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICIBFTRTGSRulesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICIBFTRTGSRulesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICIBFTRTGSRulesCollection
		Inherits esEntityCollection(Of ICIBFTRTGSRules)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICIBFTRTGSRulesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICIBFTRTGSRulesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICIBFTRTGSRulesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICIBFTRTGSRulesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICIBFTRTGSRulesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICIBFTRTGSRulesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICIBFTRTGSRulesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICIBFTRTGSRulesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICIBFTRTGSRulesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICIBFTRTGSRulesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICIBFTRTGSRulesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "IBFTRTGSRuleID" 
					Return Me.IBFTRTGSRuleID
				Case "IBFTRTGSRuleName" 
					Return Me.IBFTRTGSRuleName
				Case "FieldName" 
					Return Me.FieldName
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldValue" 
					Return Me.FieldValue
				Case "FieldCondition" 
					Return Me.FieldCondition
				Case "DisbType" 
					Return Me.DisbType
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property IBFTRTGSRuleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IBFTRTGSRuleName As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.FieldValue, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldCondition As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.FieldCondition, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbType As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.DisbType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.CompanyCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICIBFTRTGSRulesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICIBFTRTGSRules 
		Inherits esICIBFTRTGSRules
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICIBFTRTGSRulesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.IBFTRTGSRuleID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.IBFTRTGSRuleName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.IBFTRTGSRuleName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.FieldName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.FieldType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.FieldValue, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.FieldValue
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.FieldCondition, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.FieldCondition
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.DisbType, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.DisbType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.CompanyCode, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.CompanyCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.IsActive, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.CreateBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.CreateDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedBy, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.ApprovedOn, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.IsApproved, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.Creater, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICIBFTRTGSRulesMetadata.ColumnNames.CreationDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICIBFTRTGSRulesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICIBFTRTGSRulesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const IBFTRTGSRuleID As String = "IBFT_RTGSRuleID"
			 Public Const IBFTRTGSRuleName As String = "IBFT_RTGSRuleName"
			 Public Const FieldName As String = "FieldName"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldValue As String = "FieldValue"
			 Public Const FieldCondition As String = "FieldCondition"
			 Public Const DisbType As String = "DisbType"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "isApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const IBFTRTGSRuleID As String = "IBFTRTGSRuleID"
			 Public Const IBFTRTGSRuleName As String = "IBFTRTGSRuleName"
			 Public Const FieldName As String = "FieldName"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldValue As String = "FieldValue"
			 Public Const FieldCondition As String = "FieldCondition"
			 Public Const DisbType As String = "DisbType"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICIBFTRTGSRulesMetadata)
			
				If ICIBFTRTGSRulesMetadata.mapDelegates Is Nothing Then
					ICIBFTRTGSRulesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICIBFTRTGSRulesMetadata._meta Is Nothing Then
					ICIBFTRTGSRulesMetadata._meta = New ICIBFTRTGSRulesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("IBFTRTGSRuleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IBFTRTGSRuleName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldValue", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldCondition", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_IBFT_RTGS_Rules"
				meta.Destination = "IC_IBFT_RTGS_Rules"
				
				meta.spInsert = "proc_IC_IBFT_RTGS_RulesInsert"
				meta.spUpdate = "proc_IC_IBFT_RTGS_RulesUpdate"
				meta.spDelete = "proc_IC_IBFT_RTGS_RulesDelete"
				meta.spLoadAll = "proc_IC_IBFT_RTGS_RulesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_IBFT_RTGS_RulesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICIBFTRTGSRulesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
