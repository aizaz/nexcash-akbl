
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_DDInstruments' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICDDInstruments))> _
	<XmlType("ICDDInstruments")> _	
	Partial Public Class ICDDInstruments 
		Inherits esICDDInstruments
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICDDInstruments()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal dDInstrumentsID As System.Int32)
			Dim obj As New ICDDInstruments()
			obj.DDInstrumentsID = dDInstrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal dDInstrumentsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICDDInstruments()
			obj.DDInstrumentsID = dDInstrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICDDInstrumentsCollection")> _
	Partial Public Class ICDDInstrumentsCollection
		Inherits esICDDInstrumentsCollection
		Implements IEnumerable(Of ICDDInstruments)
	
		Public Function FindByPrimaryKey(ByVal dDInstrumentsID As System.Int32) As ICDDInstruments
			Return MyBase.SingleOrDefault(Function(e) e.DDInstrumentsID.HasValue AndAlso e.DDInstrumentsID.Value = dDInstrumentsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICDDInstruments))> _
		Public Class ICDDInstrumentsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICDDInstrumentsCollection)
			
			Public Shared Widening Operator CType(packet As ICDDInstrumentsCollectionWCFPacket) As ICDDInstrumentsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICDDInstrumentsCollection) As ICDDInstrumentsCollectionWCFPacket
				Return New ICDDInstrumentsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICDDInstrumentsQuery 
		Inherits esICDDInstrumentsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICDDInstrumentsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICDDInstrumentsQuery) As String
			Return ICDDInstrumentsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICDDInstrumentsQuery
			Return DirectCast(ICDDInstrumentsQuery.SerializeHelper.FromXml(query, GetType(ICDDInstrumentsQuery)), ICDDInstrumentsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICDDInstruments
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal dDInstrumentsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDInstrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDInstrumentsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal dDInstrumentsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDInstrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDInstrumentsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal dDInstrumentsID As System.Int32) As Boolean
		
			Dim query As New ICDDInstrumentsQuery()
			query.Where(query.DDInstrumentsID = dDInstrumentsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal dDInstrumentsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DDInstrumentsID", dDInstrumentsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_DDInstruments.DDInstrumentsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDInstrumentsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentsID, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDInstrumentsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.DDInstrumentNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDInstrumentNumber As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentNumber)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentNumber, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDInstrumentNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.DDMasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDMasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.DDMasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.DDMasterSeriesID, value) Then
					Me._UpToICDDMasterSeriesByDDMasterSeriesID = Nothing
					Me.OnPropertyChanged("UpToICDDMasterSeriesByDDMasterSeriesID")
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDMasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.PreFix
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreFix As System.String
			Get
				Return MyBase.GetSystemString(ICDDInstrumentsMetadata.ColumnNames.PreFix)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDInstrumentsMetadata.ColumnNames.PreFix, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.PreFix)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.IsUsed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsUsed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICDDInstrumentsMetadata.ColumnNames.IsUsed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICDDInstrumentsMetadata.ColumnNames.IsUsed, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.IsUsed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.UsedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UsedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.UsedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.UsedOn, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.UsedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.IsAssigned
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAssigned As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICDDInstrumentsMetadata.ColumnNames.IsAssigned)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICDDInstrumentsMetadata.ColumnNames.IsAssigned, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.IsAssigned)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDDInstrumentsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.SubsetFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubsetFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.SubsetFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.SubsetFrom, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.SubsetFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.SubsetTo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubsetTo As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.SubsetTo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICDDInstrumentsMetadata.ColumnNames.SubsetTo, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.SubsetTo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDInstruments.OfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.OfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDInstrumentsMetadata.ColumnNames.OfficeCode, value) Then
					OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.OfficeCode)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICDDMasterSeriesByDDMasterSeriesID As ICDDMasterSeries
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DDInstrumentsID"
							Me.str().DDInstrumentsID = CType(value, string)
												
						Case "DDInstrumentNumber"
							Me.str().DDInstrumentNumber = CType(value, string)
												
						Case "DDMasterSeriesID"
							Me.str().DDMasterSeriesID = CType(value, string)
												
						Case "PreFix"
							Me.str().PreFix = CType(value, string)
												
						Case "IsUsed"
							Me.str().IsUsed = CType(value, string)
												
						Case "UsedOn"
							Me.str().UsedOn = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsAssigned"
							Me.str().IsAssigned = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "SubsetFrom"
							Me.str().SubsetFrom = CType(value, string)
												
						Case "SubsetTo"
							Me.str().SubsetTo = CType(value, string)
												
						Case "OfficeCode"
							Me.str().OfficeCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "DDInstrumentsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DDInstrumentsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDInstrumentsID)
							End If
						
						Case "DDInstrumentNumber"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DDInstrumentNumber = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDInstrumentNumber)
							End If
						
						Case "DDMasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DDMasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.DDMasterSeriesID)
							End If
						
						Case "IsUsed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsUsed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.IsUsed)
							End If
						
						Case "UsedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.UsedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.UsedOn)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsAssigned"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAssigned = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.IsAssigned)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "SubsetFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubsetFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.SubsetFrom)
							End If
						
						Case "SubsetTo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubsetTo = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.SubsetTo)
							End If
						
						Case "OfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDInstrumentsMetadata.PropertyNames.OfficeCode)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICDDInstruments)
				Me.entity = entity
			End Sub				
		
	
			Public Property DDInstrumentsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DDInstrumentsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDInstrumentsID = Nothing
					Else
						entity.DDInstrumentsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDInstrumentNumber As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DDInstrumentNumber
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDInstrumentNumber = Nothing
					Else
						entity.DDInstrumentNumber = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDMasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DDMasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDMasterSeriesID = Nothing
					Else
						entity.DDMasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreFix As System.String 
				Get
					Dim data_ As System.String = entity.PreFix
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreFix = Nothing
					Else
						entity.PreFix = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsUsed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsUsed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsUsed = Nothing
					Else
						entity.IsUsed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property UsedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.UsedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UsedOn = Nothing
					Else
						entity.UsedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAssigned As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAssigned
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAssigned = Nothing
					Else
						entity.IsAssigned = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubsetFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubsetFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubsetFrom = Nothing
					Else
						entity.SubsetFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubsetTo As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubsetTo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubsetTo = Nothing
					Else
						entity.SubsetTo = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeCode = Nothing
					Else
						entity.OfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICDDInstruments
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDInstrumentsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICDDInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICDDInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICDDInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICDDInstrumentsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICDDInstrumentsCollection
		Inherits esEntityCollection(Of ICDDInstruments)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDInstrumentsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICDDInstrumentsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICDDInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICDDInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICDDInstrumentsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICDDInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICDDInstrumentsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICDDInstrumentsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICDDInstrumentsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICDDInstrumentsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DDInstrumentsID" 
					Return Me.DDInstrumentsID
				Case "DDInstrumentNumber" 
					Return Me.DDInstrumentNumber
				Case "DDMasterSeriesID" 
					Return Me.DDMasterSeriesID
				Case "PreFix" 
					Return Me.PreFix
				Case "IsUsed" 
					Return Me.IsUsed
				Case "UsedOn" 
					Return Me.UsedOn
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsAssigned" 
					Return Me.IsAssigned
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "SubsetFrom" 
					Return Me.SubsetFrom
				Case "SubsetTo" 
					Return Me.SubsetTo
				Case "OfficeCode" 
					Return Me.OfficeCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DDInstrumentsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.DDInstrumentsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DDInstrumentNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.DDInstrumentNumber, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DDMasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.DDMasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PreFix As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.PreFix, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsUsed As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.IsUsed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property UsedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.UsedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAssigned As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.IsAssigned, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SubsetFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.SubsetFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property SubsetTo As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.SubsetTo, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDInstrumentsMetadata.ColumnNames.OfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICDDInstruments 
		Inherits esICDDInstruments
		
	
		#Region "UpToICDDMasterSeriesByDDMasterSeriesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_DDInstruments_IC_DDMasterSeries
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICDDMasterSeriesByDDMasterSeriesID As ICDDMasterSeries
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICDDMasterSeriesByDDMasterSeriesID Is Nothing _
						 AndAlso Not DDMasterSeriesID.Equals(Nothing)  Then
						
					Me._UpToICDDMasterSeriesByDDMasterSeriesID = New ICDDMasterSeries()
					Me._UpToICDDMasterSeriesByDDMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICDDMasterSeriesByDDMasterSeriesID", Me._UpToICDDMasterSeriesByDDMasterSeriesID)
					Me._UpToICDDMasterSeriesByDDMasterSeriesID.Query.Where(Me._UpToICDDMasterSeriesByDDMasterSeriesID.Query.DDMasterSeriesID = Me.DDMasterSeriesID)
					Me._UpToICDDMasterSeriesByDDMasterSeriesID.Query.Load()
				End If

				Return Me._UpToICDDMasterSeriesByDDMasterSeriesID
			End Get
			
            Set(ByVal value As ICDDMasterSeries)
				Me.RemovePreSave("UpToICDDMasterSeriesByDDMasterSeriesID")
				

				If value Is Nothing Then
				
					Me.DDMasterSeriesID = Nothing
				
					Me._UpToICDDMasterSeriesByDDMasterSeriesID = Nothing
				Else
				
					Me.DDMasterSeriesID = value.DDMasterSeriesID
					
					Me._UpToICDDMasterSeriesByDDMasterSeriesID = value
					Me.SetPreSave("UpToICDDMasterSeriesByDDMasterSeriesID", Me._UpToICDDMasterSeriesByDDMasterSeriesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_DDInstruments_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICDDMasterSeriesByDDMasterSeriesID Is Nothing Then
				Me.DDMasterSeriesID = Me._UpToICDDMasterSeriesByDDMasterSeriesID.DDMasterSeriesID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICDDInstrumentsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentsID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.DDInstrumentsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.DDInstrumentNumber, 1, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.DDInstrumentNumber
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.DDMasterSeriesID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.DDMasterSeriesID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.PreFix, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.PreFix
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.IsUsed, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.IsUsed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.UsedOn, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.UsedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.CreatedDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.IsAssigned, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.IsAssigned
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.SubsetFrom, 11, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.SubsetFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.SubsetTo, 12, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.SubsetTo
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDInstrumentsMetadata.ColumnNames.OfficeCode, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDInstrumentsMetadata.PropertyNames.OfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICDDInstrumentsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DDInstrumentsID As String = "DDInstrumentsID"
			 Public Const DDInstrumentNumber As String = "DDInstrumentNumber"
			 Public Const DDMasterSeriesID As String = "DDMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const IsUsed As String = "IsUsed"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const SubsetFrom As String = "SubsetFrom"
			 Public Const SubsetTo As String = "SubsetTo"
			 Public Const OfficeCode As String = "OfficeCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DDInstrumentsID As String = "DDInstrumentsID"
			 Public Const DDInstrumentNumber As String = "DDInstrumentNumber"
			 Public Const DDMasterSeriesID As String = "DDMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const IsUsed As String = "IsUsed"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const SubsetFrom As String = "SubsetFrom"
			 Public Const SubsetTo As String = "SubsetTo"
			 Public Const OfficeCode As String = "OfficeCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICDDInstrumentsMetadata)
			
				If ICDDInstrumentsMetadata.mapDelegates Is Nothing Then
					ICDDInstrumentsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICDDInstrumentsMetadata._meta Is Nothing Then
					ICDDInstrumentsMetadata._meta = New ICDDInstrumentsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DDInstrumentsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DDInstrumentNumber", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DDMasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PreFix", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsUsed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("UsedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAssigned", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SubsetFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("SubsetTo", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("OfficeCode", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_DDInstruments"
				meta.Destination = "IC_DDInstruments"
				
				meta.spInsert = "proc_IC_DDInstrumentsInsert"
				meta.spUpdate = "proc_IC_DDInstrumentsUpdate"
				meta.spDelete = "proc_IC_DDInstrumentsDelete"
				meta.spLoadAll = "proc_IC_DDInstrumentsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_DDInstrumentsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICDDInstrumentsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
