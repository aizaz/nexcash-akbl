
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:40 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionSettings' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionSettings))> _
	<XmlType("ICCollectionSettings")> _	
	Partial Public Class ICCollectionSettings 
		Inherits esICCollectionSettings
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionSettings()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal settingName As System.String)
			Dim obj As New ICCollectionSettings()
			obj.SettingName = settingName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal settingName As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionSettings()
			obj.SettingName = settingName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionSettingsCollection")> _
	Partial Public Class ICCollectionSettingsCollection
		Inherits esICCollectionSettingsCollection
		Implements IEnumerable(Of ICCollectionSettings)
	
		Public Function FindByPrimaryKey(ByVal settingName As System.String) As ICCollectionSettings
			Return MyBase.SingleOrDefault(Function(e) e.SettingName = settingName)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionSettings))> _
		Public Class ICCollectionSettingsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionSettingsCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionSettingsCollectionWCFPacket) As ICCollectionSettingsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionSettingsCollection) As ICCollectionSettingsCollectionWCFPacket
				Return New ICCollectionSettingsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionSettingsQuery 
		Inherits esICCollectionSettingsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionSettingsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionSettingsQuery) As String
			Return ICCollectionSettingsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionSettingsQuery
			Return DirectCast(ICCollectionSettingsQuery.SerializeHelper.FromXml(query, GetType(ICCollectionSettingsQuery)), ICCollectionSettingsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionSettings
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal settingName As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(settingName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(settingName)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal settingName As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(settingName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(settingName)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal settingName As System.String) As Boolean
		
			Dim query As New ICCollectionSettingsQuery()
			query.Where(query.SettingName = settingName)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal settingName As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SettingName", settingName)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionSettings.SettingName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SettingName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionSettingsMetadata.ColumnNames.SettingName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionSettingsMetadata.ColumnNames.SettingName, value) Then
					OnPropertyChanged(ICCollectionSettingsMetadata.PropertyNames.SettingName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionSettings.SettingValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SettingValue As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionSettingsMetadata.ColumnNames.SettingValue)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionSettingsMetadata.ColumnNames.SettingValue, value) Then
					OnPropertyChanged(ICCollectionSettingsMetadata.PropertyNames.SettingValue)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SettingName"
							Me.str().SettingName = CType(value, string)
												
						Case "SettingValue"
							Me.str().SettingValue = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionSettings)
				Me.entity = entity
			End Sub				
		
	
			Public Property SettingName As System.String 
				Get
					Dim data_ As System.String = entity.SettingName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SettingName = Nothing
					Else
						entity.SettingName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SettingValue As System.String 
				Get
					Dim data_ As System.String = entity.SettingValue
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SettingValue = Nothing
					Else
						entity.SettingValue = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionSettings
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionSettingsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionSettingsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionSettingsCollection
		Inherits esEntityCollection(Of ICCollectionSettings)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionSettingsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionSettingsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionSettingsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionSettingsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionSettingsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionSettingsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionSettingsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SettingName" 
					Return Me.SettingName
				Case "SettingValue" 
					Return Me.SettingValue
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SettingName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionSettingsMetadata.ColumnNames.SettingName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SettingValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionSettingsMetadata.ColumnNames.SettingValue, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionSettings 
		Inherits esICCollectionSettings
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionSettingsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionSettingsMetadata.ColumnNames.SettingName, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionSettingsMetadata.PropertyNames.SettingName
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionSettingsMetadata.ColumnNames.SettingValue, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionSettingsMetadata.PropertyNames.SettingValue
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionSettingsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SettingName As String = "SettingName"
			 Public Const SettingValue As String = "SettingValue"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SettingName As String = "SettingName"
			 Public Const SettingValue As String = "SettingValue"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionSettingsMetadata)
			
				If ICCollectionSettingsMetadata.mapDelegates Is Nothing Then
					ICCollectionSettingsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionSettingsMetadata._meta Is Nothing Then
					ICCollectionSettingsMetadata._meta = New ICCollectionSettingsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SettingName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SettingValue", new esTypeMap("text", "System.String"))			
				
				
				 
				meta.Source = "IC_CollectionSettings"
				meta.Destination = "IC_CollectionSettings"
				
				meta.spInsert = "proc_IC_CollectionSettingsInsert"
				meta.spUpdate = "proc_IC_CollectionSettingsUpdate"
				meta.spDelete = "proc_IC_CollectionSettingsDelete"
				meta.spLoadAll = "proc_IC_CollectionSettingsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionSettingsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionSettingsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
