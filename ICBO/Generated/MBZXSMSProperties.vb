
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'MBZ_XSMSProperties' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(MBZXSMSProperties))> _
	<XmlType("MBZXSMSProperties")> _	
	Partial Public Class MBZXSMSProperties 
		Inherits esMBZXSMSProperties
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New MBZXSMSProperties()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal transactionID As System.Int32)
			Dim obj As New MBZXSMSProperties()
			obj.TransactionID = transactionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal transactionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New MBZXSMSProperties()
			obj.TransactionID = transactionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("MBZXSMSPropertiesCollection")> _
	Partial Public Class MBZXSMSPropertiesCollection
		Inherits esMBZXSMSPropertiesCollection
		Implements IEnumerable(Of MBZXSMSProperties)
	
		Public Function FindByPrimaryKey(ByVal transactionID As System.Int32) As MBZXSMSProperties
			Return MyBase.SingleOrDefault(Function(e) e.TransactionID.HasValue AndAlso e.TransactionID.Value = transactionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(MBZXSMSProperties))> _
		Public Class MBZXSMSPropertiesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of MBZXSMSPropertiesCollection)
			
			Public Shared Widening Operator CType(packet As MBZXSMSPropertiesCollectionWCFPacket) As MBZXSMSPropertiesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As MBZXSMSPropertiesCollection) As MBZXSMSPropertiesCollectionWCFPacket
				Return New MBZXSMSPropertiesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class MBZXSMSPropertiesQuery 
		Inherits esMBZXSMSPropertiesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "MBZXSMSPropertiesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As MBZXSMSPropertiesQuery) As String
			Return MBZXSMSPropertiesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As MBZXSMSPropertiesQuery
			Return DirectCast(MBZXSMSPropertiesQuery.SerializeHelper.FromXml(query, GetType(MBZXSMSPropertiesQuery)), MBZXSMSPropertiesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esMBZXSMSProperties
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal transactionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(transactionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(transactionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal transactionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(transactionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(transactionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal transactionID As System.Int32) As Boolean
		
			Dim query As New MBZXSMSPropertiesQuery()
			query.Where(query.TransactionID = transactionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal transactionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("TransactionID", transactionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to MBZ_XSMSProperties.MessageID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MessageID As System.String
			Get
				Return MyBase.GetSystemString(MBZXSMSPropertiesMetadata.ColumnNames.MessageID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(MBZXSMSPropertiesMetadata.ColumnNames.MessageID, value) Then
					OnPropertyChanged(MBZXSMSPropertiesMetadata.PropertyNames.MessageID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_XSMSProperties.TemplateCategory
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateCategory As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MBZXSMSPropertiesMetadata.ColumnNames.TemplateCategory)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MBZXSMSPropertiesMetadata.ColumnNames.TemplateCategory, value) Then
					OnPropertyChanged(MBZXSMSPropertiesMetadata.PropertyNames.TemplateCategory)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to MBZ_XSMSProperties.TransactionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(MBZXSMSPropertiesMetadata.ColumnNames.TransactionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(MBZXSMSPropertiesMetadata.ColumnNames.TransactionID, value) Then
					OnPropertyChanged(MBZXSMSPropertiesMetadata.PropertyNames.TransactionID)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "MessageID"
							Me.str().MessageID = CType(value, string)
												
						Case "TemplateCategory"
							Me.str().TemplateCategory = CType(value, string)
												
						Case "TransactionID"
							Me.str().TransactionID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "TemplateCategory"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateCategory = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MBZXSMSPropertiesMetadata.PropertyNames.TemplateCategory)
							End If
						
						Case "TransactionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TransactionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(MBZXSMSPropertiesMetadata.PropertyNames.TransactionID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esMBZXSMSProperties)
				Me.entity = entity
			End Sub				
		
	
			Public Property MessageID As System.String 
				Get
					Dim data_ As System.String = entity.MessageID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MessageID = Nothing
					Else
						entity.MessageID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateCategory As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateCategory
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateCategory = Nothing
					Else
						entity.TemplateCategory = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TransactionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionID = Nothing
					Else
						entity.TransactionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esMBZXSMSProperties
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return MBZXSMSPropertiesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As MBZXSMSPropertiesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New MBZXSMSPropertiesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As MBZXSMSPropertiesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As MBZXSMSPropertiesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As MBZXSMSPropertiesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esMBZXSMSPropertiesCollection
		Inherits esEntityCollection(Of MBZXSMSProperties)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return MBZXSMSPropertiesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "MBZXSMSPropertiesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As MBZXSMSPropertiesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New MBZXSMSPropertiesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As MBZXSMSPropertiesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New MBZXSMSPropertiesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As MBZXSMSPropertiesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, MBZXSMSPropertiesQuery))
		End Sub
		
		#End Region
						
		Private m_query As MBZXSMSPropertiesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esMBZXSMSPropertiesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return MBZXSMSPropertiesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "MessageID" 
					Return Me.MessageID
				Case "TemplateCategory" 
					Return Me.TemplateCategory
				Case "TransactionID" 
					Return Me.TransactionID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property MessageID As esQueryItem
			Get
				Return New esQueryItem(Me, MBZXSMSPropertiesMetadata.ColumnNames.MessageID, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateCategory As esQueryItem
			Get
				Return New esQueryItem(Me, MBZXSMSPropertiesMetadata.ColumnNames.TemplateCategory, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionID As esQueryItem
			Get
				Return New esQueryItem(Me, MBZXSMSPropertiesMetadata.ColumnNames.TransactionID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class MBZXSMSProperties 
		Inherits esMBZXSMSProperties
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class MBZXSMSPropertiesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(MBZXSMSPropertiesMetadata.ColumnNames.MessageID, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = MBZXSMSPropertiesMetadata.PropertyNames.MessageID
			c.CharacterMaxLength = 32
			m_columns.Add(c)
				
			c = New esColumnMetadata(MBZXSMSPropertiesMetadata.ColumnNames.TemplateCategory, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MBZXSMSPropertiesMetadata.PropertyNames.TemplateCategory
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(MBZXSMSPropertiesMetadata.ColumnNames.TransactionID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = MBZXSMSPropertiesMetadata.PropertyNames.TransactionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As MBZXSMSPropertiesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const MessageID As String = "MessageID"
			 Public Const TemplateCategory As String = "TemplateCategory"
			 Public Const TransactionID As String = "TransactionID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const MessageID As String = "MessageID"
			 Public Const TemplateCategory As String = "TemplateCategory"
			 Public Const TransactionID As String = "TransactionID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(MBZXSMSPropertiesMetadata)
			
				If MBZXSMSPropertiesMetadata.mapDelegates Is Nothing Then
					MBZXSMSPropertiesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If MBZXSMSPropertiesMetadata._meta Is Nothing Then
					MBZXSMSPropertiesMetadata._meta = New MBZXSMSPropertiesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("MessageID", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateCategory", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TransactionID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "MBZ_XSMSProperties"
				meta.Destination = "MBZ_XSMSProperties"
				
				meta.spInsert = "proc_MBZ_XSMSPropertiesInsert"
				meta.spUpdate = "proc_MBZ_XSMSPropertiesUpdate"
				meta.spDelete = "proc_MBZ_XSMSPropertiesDelete"
				meta.spLoadAll = "proc_MBZ_XSMSPropertiesLoadAll"
				meta.spLoadByPrimaryKey = "proc_MBZ_XSMSPropertiesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As MBZXSMSPropertiesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
