
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_UserRoles' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUserRoles))> _
	<XmlType("ICUserRoles")> _	
	Partial Public Class ICUserRoles 
		Inherits esICUserRoles
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUserRoles()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal userID As System.Int32, ByVal roleID As System.Int32)
			Dim obj As New ICUserRoles()
			obj.UserID = userID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal userID As System.Int32, ByVal roleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUserRoles()
			obj.UserID = userID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUserRolesCollection")> _
	Partial Public Class ICUserRolesCollection
		Inherits esICUserRolesCollection
		Implements IEnumerable(Of ICUserRoles)
	
		Public Function FindByPrimaryKey(ByVal userID As System.Int32, ByVal roleID As System.Int32) As ICUserRoles
			Return MyBase.SingleOrDefault(Function(e) e.UserID.HasValue AndAlso e.UserID.Value = userID And e.RoleID.HasValue AndAlso e.RoleID.Value = roleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUserRoles))> _
		Public Class ICUserRolesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUserRolesCollection)
			
			Public Shared Widening Operator CType(packet As ICUserRolesCollectionWCFPacket) As ICUserRolesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUserRolesCollection) As ICUserRolesCollectionWCFPacket
				Return New ICUserRolesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUserRolesQuery 
		Inherits esICUserRolesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUserRolesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUserRolesQuery) As String
			Return ICUserRolesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUserRolesQuery
			Return DirectCast(ICUserRolesQuery.SerializeHelper.FromXml(query, GetType(ICUserRolesQuery)), ICUserRolesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUserRoles
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID, roleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID, roleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim query As New ICUserRolesQuery()
			query.Where(query.UserID = userID And query.RoleID = roleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UserID", userID)
						parms.Add("RoleID", roleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_UserRoles.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICRoleByRoleID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRoleID")
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.AssignedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AssignedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesMetadata.ColumnNames.AssignedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesMetadata.ColumnNames.AssignedBy, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.AssignedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.AssignedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AssignedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesMetadata.ColumnNames.AssignedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesMetadata.ColumnNames.AssignedOn, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.AssignedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserRolesMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserRolesMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRoles.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICUserRolesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRoleID As ICRole
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "AssignedBy"
							Me.str().AssignedBy = CType(value, string)
												
						Case "AssignedOn"
							Me.str().AssignedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.UserID)
							End If
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.RoleID)
							End If
						
						Case "AssignedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AssignedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.AssignedBy)
							End If
						
						Case "AssignedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AssignedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.AssignedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUserRoles)
				Me.entity = entity
			End Sub				
		
	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AssignedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AssignedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AssignedBy = Nothing
					Else
						entity.AssignedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AssignedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AssignedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AssignedOn = Nothing
					Else
						entity.AssignedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUserRoles
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUserRolesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUserRolesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUserRolesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUserRolesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesCollection
		Inherits esEntityCollection(Of ICUserRoles)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUserRolesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUserRolesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUserRolesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUserRolesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUserRolesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUserRolesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUserRolesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUserRolesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUserRolesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UserID" 
					Return Me.UserID
				Case "RoleID" 
					Return Me.RoleID
				Case "AssignedBy" 
					Return Me.AssignedBy
				Case "AssignedOn" 
					Return Me.AssignedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AssignedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.AssignedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AssignedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.AssignedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUserRoles 
		Inherits esICUserRoles
		
	
		#Region "UpToICRoleByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRoles_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRoleID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRoleID = New ICRole()
					Me._UpToICRoleByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
					Me._UpToICRoleByRoleID.Query.Where(Me._UpToICRoleByRoleID.Query.RoleID = Me.RoleID)
					Me._UpToICRoleByRoleID.Query.Load()
				End If

				Return Me._UpToICRoleByRoleID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me._UpToICRoleByRoleID = Nothing
				Else
				
					Me.RoleID = value.RoleID
					
					Me._UpToICRoleByRoleID = value
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRoles_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICUserRolesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.UserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.UserID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.RoleID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.RoleID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.AssignedBy, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.AssignedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.AssignedOn, 3, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.AssignedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.IsApproved, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.ApprovedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.ApprovedOn, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.Creater, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesMetadata.ColumnNames.CreationDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUserRolesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UserID As String = "UserID"
			 Public Const RoleID As String = "RoleID"
			 Public Const AssignedBy As String = "AssignedBy"
			 Public Const AssignedOn As String = "AssignedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UserID As String = "UserID"
			 Public Const RoleID As String = "RoleID"
			 Public Const AssignedBy As String = "AssignedBy"
			 Public Const AssignedOn As String = "AssignedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUserRolesMetadata)
			
				If ICUserRolesMetadata.mapDelegates Is Nothing Then
					ICUserRolesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUserRolesMetadata._meta Is Nothing Then
					ICUserRolesMetadata._meta = New ICUserRolesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AssignedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AssignedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_UserRoles"
				meta.Destination = "IC_UserRoles"
				
				meta.spInsert = "proc_IC_UserRolesInsert"
				meta.spUpdate = "proc_IC_UserRolesUpdate"
				meta.spDelete = "proc_IC_UserRolesDelete"
				meta.spLoadAll = "proc_IC_UserRolesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UserRolesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUserRolesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
