
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_POMasterSeries' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPOMasterSeries))> _
	<XmlType("ICPOMasterSeries")> _	
	Partial Public Class ICPOMasterSeries 
		Inherits esICPOMasterSeries
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPOMasterSeries()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal pOMasterSeriesID As System.Int32)
			Dim obj As New ICPOMasterSeries()
			obj.POMasterSeriesID = pOMasterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal pOMasterSeriesID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPOMasterSeries()
			obj.POMasterSeriesID = pOMasterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPOMasterSeriesCollection")> _
	Partial Public Class ICPOMasterSeriesCollection
		Inherits esICPOMasterSeriesCollection
		Implements IEnumerable(Of ICPOMasterSeries)
	
		Public Function FindByPrimaryKey(ByVal pOMasterSeriesID As System.Int32) As ICPOMasterSeries
			Return MyBase.SingleOrDefault(Function(e) e.POMasterSeriesID.HasValue AndAlso e.POMasterSeriesID.Value = pOMasterSeriesID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPOMasterSeries))> _
		Public Class ICPOMasterSeriesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPOMasterSeriesCollection)
			
			Public Shared Widening Operator CType(packet As ICPOMasterSeriesCollectionWCFPacket) As ICPOMasterSeriesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPOMasterSeriesCollection) As ICPOMasterSeriesCollectionWCFPacket
				Return New ICPOMasterSeriesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPOMasterSeriesQuery 
		Inherits esICPOMasterSeriesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPOMasterSeriesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPOMasterSeriesQuery) As String
			Return ICPOMasterSeriesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPOMasterSeriesQuery
			Return DirectCast(ICPOMasterSeriesQuery.SerializeHelper.FromXml(query, GetType(ICPOMasterSeriesQuery)), ICPOMasterSeriesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPOMasterSeries
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal pOMasterSeriesID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(pOMasterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(pOMasterSeriesID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal pOMasterSeriesID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(pOMasterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(pOMasterSeriesID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal pOMasterSeriesID As System.Int32) As Boolean
		
			Dim query As New ICPOMasterSeriesQuery()
			query.Where(query.POMasterSeriesID = pOMasterSeriesID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal pOMasterSeriesID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("POMasterSeriesID", pOMasterSeriesID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_POMasterSeries.POMasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property POMasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.POMasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.POMasterSeriesID, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.POMasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.PreFix
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreFix As System.String
			Get
				Return MyBase.GetSystemString(ICPOMasterSeriesMetadata.ColumnNames.PreFix)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPOMasterSeriesMetadata.ColumnNames.PreFix, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.PreFix)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.StartFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StartFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPOMasterSeriesMetadata.ColumnNames.StartFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPOMasterSeriesMetadata.ColumnNames.StartFrom, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.StartFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.EndsAt
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EndsAt As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPOMasterSeriesMetadata.ColumnNames.EndsAt)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPOMasterSeriesMetadata.ColumnNames.EndsAt, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.EndsAt)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPOMasterSeriesMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPOMasterSeriesMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPOMasterSeriesMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPOMasterSeriesMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOMasterSeriesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POMasterSeries.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPOMasterSeriesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPOMasterSeriesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "POMasterSeriesID"
							Me.str().POMasterSeriesID = CType(value, string)
												
						Case "PreFix"
							Me.str().PreFix = CType(value, string)
												
						Case "StartFrom"
							Me.str().StartFrom = CType(value, string)
												
						Case "EndsAt"
							Me.str().EndsAt = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "POMasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.POMasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.POMasterSeriesID)
							End If
						
						Case "StartFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.StartFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.StartFrom)
							End If
						
						Case "EndsAt"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.EndsAt = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.EndsAt)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPOMasterSeriesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPOMasterSeries)
				Me.entity = entity
			End Sub				
		
	
			Public Property POMasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.POMasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.POMasterSeriesID = Nothing
					Else
						entity.POMasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreFix As System.String 
				Get
					Dim data_ As System.String = entity.PreFix
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreFix = Nothing
					Else
						entity.PreFix = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property StartFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.StartFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StartFrom = Nothing
					Else
						entity.StartFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property EndsAt As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.EndsAt
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EndsAt = Nothing
					Else
						entity.EndsAt = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPOMasterSeries
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPOMasterSeriesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPOMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPOMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPOMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPOMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPOMasterSeriesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPOMasterSeriesCollection
		Inherits esEntityCollection(Of ICPOMasterSeries)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPOMasterSeriesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPOMasterSeriesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPOMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPOMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPOMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPOMasterSeriesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPOMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPOMasterSeriesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPOMasterSeriesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPOMasterSeriesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPOMasterSeriesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "POMasterSeriesID" 
					Return Me.POMasterSeriesID
				Case "PreFix" 
					Return Me.PreFix
				Case "StartFrom" 
					Return Me.StartFrom
				Case "EndsAt" 
					Return Me.EndsAt
				Case "IsActive" 
					Return Me.IsActive
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property POMasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.POMasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PreFix As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.PreFix, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property StartFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.StartFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property EndsAt As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.EndsAt, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOMasterSeriesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPOMasterSeries 
		Inherits esICPOMasterSeries
		
	
		#Region "ICPOInstrumentsCollectionByPOMasterSeriesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPOInstrumentsCollectionByPOMasterSeriesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPOMasterSeries.ICPOInstrumentsCollectionByPOMasterSeriesID_Delegate)
				map.PropertyName = "ICPOInstrumentsCollectionByPOMasterSeriesID"
				map.MyColumnName = "POMasterSeriesID"
				map.ParentColumnName = "POMasterSeriesID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPOInstrumentsCollectionByPOMasterSeriesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPOMasterSeriesQuery(data.NextAlias())
			
			Dim mee As ICPOInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPOInstrumentsQuery), New ICPOInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.POMasterSeriesID = mee.POMasterSeriesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_POInstruments_IC_POMasterSeries
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPOInstrumentsCollectionByPOMasterSeriesID As ICPOInstrumentsCollection 
		
			Get
				If Me._ICPOInstrumentsCollectionByPOMasterSeriesID Is Nothing Then
					Me._ICPOInstrumentsCollectionByPOMasterSeriesID = New ICPOInstrumentsCollection()
					Me._ICPOInstrumentsCollectionByPOMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPOInstrumentsCollectionByPOMasterSeriesID", Me._ICPOInstrumentsCollectionByPOMasterSeriesID)
				
					If Not Me.POMasterSeriesID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPOInstrumentsCollectionByPOMasterSeriesID.Query.Where(Me._ICPOInstrumentsCollectionByPOMasterSeriesID.Query.POMasterSeriesID = Me.POMasterSeriesID)
							Me._ICPOInstrumentsCollectionByPOMasterSeriesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPOInstrumentsCollectionByPOMasterSeriesID.fks.Add(ICPOInstrumentsMetadata.ColumnNames.POMasterSeriesID, Me.POMasterSeriesID)
					End If
				End If

				Return Me._ICPOInstrumentsCollectionByPOMasterSeriesID
			End Get
			
			Set(ByVal value As ICPOInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPOInstrumentsCollectionByPOMasterSeriesID Is Nothing Then

					Me.RemovePostSave("ICPOInstrumentsCollectionByPOMasterSeriesID")
					Me._ICPOInstrumentsCollectionByPOMasterSeriesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPOInstrumentsCollectionByPOMasterSeriesID As ICPOInstrumentsCollection
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_POMasterSeries_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICPOInstrumentsCollectionByPOMasterSeriesID"
					coll = Me.ICPOInstrumentsCollectionByPOMasterSeriesID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICPOInstrumentsCollectionByPOMasterSeriesID", GetType(ICPOInstrumentsCollection), New ICPOInstruments()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICPOInstrumentsCollectionByPOMasterSeriesID Is Nothing Then
				Apply(Me._ICPOInstrumentsCollectionByPOMasterSeriesID, "POMasterSeriesID", Me.POMasterSeriesID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPOMasterSeriesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.POMasterSeriesID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.POMasterSeriesID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.PreFix, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.PreFix
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.StartFrom, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.StartFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.EndsAt, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.EndsAt
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.Creater, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOMasterSeriesMetadata.ColumnNames.CreationDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPOMasterSeriesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPOMasterSeriesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const POMasterSeriesID As String = "POMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const POMasterSeriesID As String = "POMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPOMasterSeriesMetadata)
			
				If ICPOMasterSeriesMetadata.mapDelegates Is Nothing Then
					ICPOMasterSeriesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPOMasterSeriesMetadata._meta Is Nothing Then
					ICPOMasterSeriesMetadata._meta = New ICPOMasterSeriesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("POMasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PreFix", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("StartFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("EndsAt", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_POMasterSeries"
				meta.Destination = "IC_POMasterSeries"
				
				meta.spInsert = "proc_IC_POMasterSeriesInsert"
				meta.spUpdate = "proc_IC_POMasterSeriesUpdate"
				meta.spDelete = "proc_IC_POMasterSeriesDelete"
				meta.spLoadAll = "proc_IC_POMasterSeriesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_POMasterSeriesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPOMasterSeriesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
