
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/26/2014 10:35:48 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ProductType' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICProductType))> _
	<XmlType("ICProductType")> _	
	Partial Public Class ICProductType 
		Inherits esICProductType
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICProductType()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal productTypeCode As System.String)
			Dim obj As New ICProductType()
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal productTypeCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICProductType()
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICProductTypeCollection")> _
	Partial Public Class ICProductTypeCollection
		Inherits esICProductTypeCollection
		Implements IEnumerable(Of ICProductType)
	
		Public Function FindByPrimaryKey(ByVal productTypeCode As System.String) As ICProductType
			Return MyBase.SingleOrDefault(Function(e) e.ProductTypeCode = productTypeCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICProductType))> _
		Public Class ICProductTypeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICProductTypeCollection)
			
			Public Shared Widening Operator CType(packet As ICProductTypeCollectionWCFPacket) As ICProductTypeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICProductTypeCollection) As ICProductTypeCollectionWCFPacket
				Return New ICProductTypeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICProductTypeQuery 
		Inherits esICProductTypeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICProductTypeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICProductTypeQuery) As String
			Return ICProductTypeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICProductTypeQuery
			Return DirectCast(ICProductTypeQuery.SerializeHelper.FromXml(query, GetType(ICProductTypeQuery)), ICProductTypeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICProductType
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal productTypeCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(productTypeCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal productTypeCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(productTypeCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal productTypeCode As System.String) As Boolean
		
			Dim query As New ICProductTypeQuery()
			query.Where(query.ProductTypeCode = productTypeCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal productTypeCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ProductTypeCode", productTypeCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ProductType.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ProductTypeName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeName As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ProductTypeName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ProductTypeName, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ProductTypeName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.IsSundaryAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsSundaryAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsSundaryAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsSundaryAllow, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsSundaryAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DisbursementMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursementMode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DisbursementMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DisbursementMode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DisbursementMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProductTypeMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProductTypeMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProductTypeMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProductTypeMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProductTypeMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProductTypeMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProductTypeMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProductTypeMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountNumber, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.NonPrincipalBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NonPrincipalBankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProductTypeMetadata.ColumnNames.NonPrincipalBankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProductTypeMetadata.ColumnNames.NonPrincipalBankCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.NonPrincipalBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.IsFunded
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsFunded As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsFunded)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICProductTypeMetadata.ColumnNames.IsFunded, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsFunded)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountNumber, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitTranCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitTranCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitTranCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitTranCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitTranCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalTranCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalTranCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalTranCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalTranCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalTranCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditTranCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditTranCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditTranCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditTranCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditTranCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalTranCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalTranCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalTranCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalTranCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalTranCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitGLCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalGLCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.DebitReversalProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DebitReversalProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.DebitReversalProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.DebitReversalProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditGLCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalGLCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalGLCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalGLCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalGLCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalGLCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalBranchCode, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalCurrency, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreditReversalProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreditReversalProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.CreditReversalProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreditReversalProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.PayableAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.PayableAccountType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.PayableAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountSeqNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountClientNo, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.FundedAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FundedAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.FundedAccountProfitCentre, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.FundedAccountProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICProductTypeMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICProductTypeMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICProductTypeMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICProductTypeMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.RespondingTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RespondingTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.RespondingTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.RespondingTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.RespondingTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.OriginatingTransactionType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OriginatingTransactionType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.OriginatingTransactionType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.OriginatingTransactionType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.OriginatingTransactionType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.VoucherType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property VoucherType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.VoucherType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.VoucherType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.VoucherType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.InstrumentType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.InstrumentType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.InstrumentType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.InstrumentType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ReversalTransactionType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReversalTransactionType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ReversalTransactionType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ReversalTransactionType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ReversalTransactionType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingSameDayOrgTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingSameDayOrgTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingSameDayOrgTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingSameDayOrgTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingSameDayOrgTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingInterCityOrgTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingInterCityOrgTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingInterCityOrgTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingInterCityOrgTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingInterCityOrgTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingNormalOrgTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalOrgTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingNormalOrgTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingNormalOrgTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingNormalOrgTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingSameDayResTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingSameDayResTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingSameDayResTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingSameDayResTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingSameDayResTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingInterCityResTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingInterCityResTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingInterCityResTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingInterCityResTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingInterCityResTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingNormalResTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalResTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingNormalResTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingNormalResTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingNormalResTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.POCancellationTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property POCancellationTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.POCancellationTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.POCancellationTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.POCancellationTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingFTOrgTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingFTOrgTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingFTOrgTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingFTOrgTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingFTOrgTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ClearingFTRespTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingFTRespTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ClearingFTRespTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ClearingFTRespTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ClearingFTRespTranType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ProductType.ReversalRespondingTranType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReversalRespondingTranType As System.String
			Get
				Return MyBase.GetSystemString(ICProductTypeMetadata.ColumnNames.ReversalRespondingTranType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICProductTypeMetadata.ColumnNames.ReversalRespondingTranType, value) Then
					OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ReversalRespondingTranType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "ProductTypeName"
							Me.str().ProductTypeName = CType(value, string)
												
						Case "IsSundaryAllow"
							Me.str().IsSundaryAllow = CType(value, string)
												
						Case "DisbursementMode"
							Me.str().DisbursementMode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "PayableAccountNumber"
							Me.str().PayableAccountNumber = CType(value, string)
												
						Case "PayableAccountBranchCode"
							Me.str().PayableAccountBranchCode = CType(value, string)
												
						Case "PayableAccountCurrency"
							Me.str().PayableAccountCurrency = CType(value, string)
												
						Case "NonPrincipalBankCode"
							Me.str().NonPrincipalBankCode = CType(value, string)
												
						Case "IsFunded"
							Me.str().IsFunded = CType(value, string)
												
						Case "FundedAccountNumber"
							Me.str().FundedAccountNumber = CType(value, string)
												
						Case "FundedAccountBranchCode"
							Me.str().FundedAccountBranchCode = CType(value, string)
												
						Case "FundedAccountCurrency"
							Me.str().FundedAccountCurrency = CType(value, string)
												
						Case "DebitTranCode"
							Me.str().DebitTranCode = CType(value, string)
												
						Case "DebitReversalTranCode"
							Me.str().DebitReversalTranCode = CType(value, string)
												
						Case "CreditTranCode"
							Me.str().CreditTranCode = CType(value, string)
												
						Case "CreditReversalTranCode"
							Me.str().CreditReversalTranCode = CType(value, string)
												
						Case "DebitGLCode"
							Me.str().DebitGLCode = CType(value, string)
												
						Case "DebitBranchCode"
							Me.str().DebitBranchCode = CType(value, string)
												
						Case "DebitCurrency"
							Me.str().DebitCurrency = CType(value, string)
												
						Case "DebitClientNo"
							Me.str().DebitClientNo = CType(value, string)
												
						Case "DebitSeqNo"
							Me.str().DebitSeqNo = CType(value, string)
												
						Case "DebitProfitCentre"
							Me.str().DebitProfitCentre = CType(value, string)
												
						Case "DebitReversalGLCode"
							Me.str().DebitReversalGLCode = CType(value, string)
												
						Case "DebitReversalBranchCode"
							Me.str().DebitReversalBranchCode = CType(value, string)
												
						Case "DebitReversalCurrency"
							Me.str().DebitReversalCurrency = CType(value, string)
												
						Case "DebitReversalClientNo"
							Me.str().DebitReversalClientNo = CType(value, string)
												
						Case "DebitReversalSeqNo"
							Me.str().DebitReversalSeqNo = CType(value, string)
												
						Case "DebitReversalProfitCentre"
							Me.str().DebitReversalProfitCentre = CType(value, string)
												
						Case "CreditGLCode"
							Me.str().CreditGLCode = CType(value, string)
												
						Case "CreditBranchCode"
							Me.str().CreditBranchCode = CType(value, string)
												
						Case "CreditCurrency"
							Me.str().CreditCurrency = CType(value, string)
												
						Case "CreditClientNo"
							Me.str().CreditClientNo = CType(value, string)
												
						Case "CreditSeqNo"
							Me.str().CreditSeqNo = CType(value, string)
												
						Case "CreditProfitCentre"
							Me.str().CreditProfitCentre = CType(value, string)
												
						Case "CreditReversalGLCode"
							Me.str().CreditReversalGLCode = CType(value, string)
												
						Case "CreditReversalBranchCode"
							Me.str().CreditReversalBranchCode = CType(value, string)
												
						Case "CreditReversalCurrency"
							Me.str().CreditReversalCurrency = CType(value, string)
												
						Case "CreditReversalClientNo"
							Me.str().CreditReversalClientNo = CType(value, string)
												
						Case "CreditReversalSeqNo"
							Me.str().CreditReversalSeqNo = CType(value, string)
												
						Case "CreditReversalProfitCentre"
							Me.str().CreditReversalProfitCentre = CType(value, string)
												
						Case "PayableAccountSeqNo"
							Me.str().PayableAccountSeqNo = CType(value, string)
												
						Case "PayableAccountClientNo"
							Me.str().PayableAccountClientNo = CType(value, string)
												
						Case "PayableAccountProfitCentre"
							Me.str().PayableAccountProfitCentre = CType(value, string)
												
						Case "PayableAccountType"
							Me.str().PayableAccountType = CType(value, string)
												
						Case "FundedAccountType"
							Me.str().FundedAccountType = CType(value, string)
												
						Case "FundedAccountSeqNo"
							Me.str().FundedAccountSeqNo = CType(value, string)
												
						Case "FundedAccountClientNo"
							Me.str().FundedAccountClientNo = CType(value, string)
												
						Case "FundedAccountProfitCentre"
							Me.str().FundedAccountProfitCentre = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "RespondingTranType"
							Me.str().RespondingTranType = CType(value, string)
												
						Case "OriginatingTransactionType"
							Me.str().OriginatingTransactionType = CType(value, string)
												
						Case "VoucherType"
							Me.str().VoucherType = CType(value, string)
												
						Case "InstrumentType"
							Me.str().InstrumentType = CType(value, string)
												
						Case "ReversalTransactionType"
							Me.str().ReversalTransactionType = CType(value, string)
												
						Case "ClearingSameDayOrgTranType"
							Me.str().ClearingSameDayOrgTranType = CType(value, string)
												
						Case "ClearingInterCityOrgTranType"
							Me.str().ClearingInterCityOrgTranType = CType(value, string)
												
						Case "ClearingNormalOrgTranType"
							Me.str().ClearingNormalOrgTranType = CType(value, string)
												
						Case "ClearingSameDayResTranType"
							Me.str().ClearingSameDayResTranType = CType(value, string)
												
						Case "ClearingInterCityResTranType"
							Me.str().ClearingInterCityResTranType = CType(value, string)
												
						Case "ClearingNormalResTranType"
							Me.str().ClearingNormalResTranType = CType(value, string)
												
						Case "POCancellationTranType"
							Me.str().POCancellationTranType = CType(value, string)
												
						Case "ClearingFTOrgTranType"
							Me.str().ClearingFTOrgTranType = CType(value, string)
												
						Case "ClearingFTRespTranType"
							Me.str().ClearingFTRespTranType = CType(value, string)
												
						Case "ReversalRespondingTranType"
							Me.str().ReversalRespondingTranType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "IsSundaryAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsSundaryAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsSundaryAllow)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "NonPrincipalBankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.NonPrincipalBankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.NonPrincipalBankCode)
							End If
						
						Case "IsFunded"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsFunded = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.IsFunded)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICProductTypeMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICProductType)
				Me.entity = entity
			End Sub				
		
	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeName As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeName = Nothing
					Else
						entity.ProductTypeName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsSundaryAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsSundaryAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsSundaryAllow = Nothing
					Else
						entity.IsSundaryAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursementMode As System.String 
				Get
					Dim data_ As System.String = entity.DisbursementMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursementMode = Nothing
					Else
						entity.DisbursementMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountNumber = Nothing
					Else
						entity.PayableAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountBranchCode = Nothing
					Else
						entity.PayableAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountCurrency = Nothing
					Else
						entity.PayableAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property NonPrincipalBankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.NonPrincipalBankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NonPrincipalBankCode = Nothing
					Else
						entity.NonPrincipalBankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsFunded As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsFunded
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsFunded = Nothing
					Else
						entity.IsFunded = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountNumber = Nothing
					Else
						entity.FundedAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountBranchCode = Nothing
					Else
						entity.FundedAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountCurrency = Nothing
					Else
						entity.FundedAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitTranCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitTranCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitTranCode = Nothing
					Else
						entity.DebitTranCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalTranCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalTranCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalTranCode = Nothing
					Else
						entity.DebitReversalTranCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditTranCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditTranCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditTranCode = Nothing
					Else
						entity.CreditTranCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalTranCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalTranCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalTranCode = Nothing
					Else
						entity.CreditReversalTranCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitGLCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitGLCode = Nothing
					Else
						entity.DebitGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitBranchCode = Nothing
					Else
						entity.DebitBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitCurrency As System.String 
				Get
					Dim data_ As System.String = entity.DebitCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitCurrency = Nothing
					Else
						entity.DebitCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitClientNo As System.String 
				Get
					Dim data_ As System.String = entity.DebitClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitClientNo = Nothing
					Else
						entity.DebitClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.DebitSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitSeqNo = Nothing
					Else
						entity.DebitSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.DebitProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitProfitCentre = Nothing
					Else
						entity.DebitProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalGLCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalGLCode = Nothing
					Else
						entity.DebitReversalGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalBranchCode = Nothing
					Else
						entity.DebitReversalBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalCurrency As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalCurrency = Nothing
					Else
						entity.DebitReversalCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalClientNo As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalClientNo = Nothing
					Else
						entity.DebitReversalClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalSeqNo = Nothing
					Else
						entity.DebitReversalSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DebitReversalProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.DebitReversalProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DebitReversalProfitCentre = Nothing
					Else
						entity.DebitReversalProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditGLCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditGLCode = Nothing
					Else
						entity.CreditGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditBranchCode = Nothing
					Else
						entity.CreditBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditCurrency As System.String 
				Get
					Dim data_ As System.String = entity.CreditCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditCurrency = Nothing
					Else
						entity.CreditCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditClientNo As System.String 
				Get
					Dim data_ As System.String = entity.CreditClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditClientNo = Nothing
					Else
						entity.CreditClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.CreditSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditSeqNo = Nothing
					Else
						entity.CreditSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.CreditProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditProfitCentre = Nothing
					Else
						entity.CreditProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalGLCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalGLCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalGLCode = Nothing
					Else
						entity.CreditReversalGLCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalBranchCode = Nothing
					Else
						entity.CreditReversalBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalCurrency As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalCurrency = Nothing
					Else
						entity.CreditReversalCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalClientNo As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalClientNo = Nothing
					Else
						entity.CreditReversalClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalSeqNo = Nothing
					Else
						entity.CreditReversalSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreditReversalProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.CreditReversalProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreditReversalProfitCentre = Nothing
					Else
						entity.CreditReversalProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountSeqNo = Nothing
					Else
						entity.PayableAccountSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountClientNo = Nothing
					Else
						entity.PayableAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountProfitCentre = Nothing
					Else
						entity.PayableAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountType As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountType = Nothing
					Else
						entity.PayableAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountType As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountType = Nothing
					Else
						entity.FundedAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountSeqNo = Nothing
					Else
						entity.FundedAccountSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountClientNo = Nothing
					Else
						entity.FundedAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FundedAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.FundedAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FundedAccountProfitCentre = Nothing
					Else
						entity.FundedAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property RespondingTranType As System.String 
				Get
					Dim data_ As System.String = entity.RespondingTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RespondingTranType = Nothing
					Else
						entity.RespondingTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OriginatingTransactionType As System.String 
				Get
					Dim data_ As System.String = entity.OriginatingTransactionType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OriginatingTransactionType = Nothing
					Else
						entity.OriginatingTransactionType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property VoucherType As System.String 
				Get
					Dim data_ As System.String = entity.VoucherType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.VoucherType = Nothing
					Else
						entity.VoucherType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentType As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentType = Nothing
					Else
						entity.InstrumentType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReversalTransactionType As System.String 
				Get
					Dim data_ As System.String = entity.ReversalTransactionType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReversalTransactionType = Nothing
					Else
						entity.ReversalTransactionType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingSameDayOrgTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingSameDayOrgTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingSameDayOrgTranType = Nothing
					Else
						entity.ClearingSameDayOrgTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingInterCityOrgTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingInterCityOrgTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingInterCityOrgTranType = Nothing
					Else
						entity.ClearingInterCityOrgTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalOrgTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalOrgTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalOrgTranType = Nothing
					Else
						entity.ClearingNormalOrgTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingSameDayResTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingSameDayResTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingSameDayResTranType = Nothing
					Else
						entity.ClearingSameDayResTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingInterCityResTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingInterCityResTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingInterCityResTranType = Nothing
					Else
						entity.ClearingInterCityResTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalResTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalResTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalResTranType = Nothing
					Else
						entity.ClearingNormalResTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property POCancellationTranType As System.String 
				Get
					Dim data_ As System.String = entity.POCancellationTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.POCancellationTranType = Nothing
					Else
						entity.POCancellationTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingFTOrgTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingFTOrgTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingFTOrgTranType = Nothing
					Else
						entity.ClearingFTOrgTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingFTRespTranType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingFTRespTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingFTRespTranType = Nothing
					Else
						entity.ClearingFTRespTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReversalRespondingTranType As System.String 
				Get
					Dim data_ As System.String = entity.ReversalRespondingTranType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReversalRespondingTranType = Nothing
					Else
						entity.ReversalRespondingTranType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICProductType
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICProductTypeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICProductTypeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICProductTypeCollection
		Inherits esEntityCollection(Of ICProductType)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICProductTypeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICProductTypeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICProductTypeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICProductTypeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICProductTypeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICProductTypeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICProductTypeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "ProductTypeName" 
					Return Me.ProductTypeName
				Case "IsSundaryAllow" 
					Return Me.IsSundaryAllow
				Case "DisbursementMode" 
					Return Me.DisbursementMode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "PayableAccountNumber" 
					Return Me.PayableAccountNumber
				Case "PayableAccountBranchCode" 
					Return Me.PayableAccountBranchCode
				Case "PayableAccountCurrency" 
					Return Me.PayableAccountCurrency
				Case "NonPrincipalBankCode" 
					Return Me.NonPrincipalBankCode
				Case "IsFunded" 
					Return Me.IsFunded
				Case "FundedAccountNumber" 
					Return Me.FundedAccountNumber
				Case "FundedAccountBranchCode" 
					Return Me.FundedAccountBranchCode
				Case "FundedAccountCurrency" 
					Return Me.FundedAccountCurrency
				Case "DebitTranCode" 
					Return Me.DebitTranCode
				Case "DebitReversalTranCode" 
					Return Me.DebitReversalTranCode
				Case "CreditTranCode" 
					Return Me.CreditTranCode
				Case "CreditReversalTranCode" 
					Return Me.CreditReversalTranCode
				Case "DebitGLCode" 
					Return Me.DebitGLCode
				Case "DebitBranchCode" 
					Return Me.DebitBranchCode
				Case "DebitCurrency" 
					Return Me.DebitCurrency
				Case "DebitClientNo" 
					Return Me.DebitClientNo
				Case "DebitSeqNo" 
					Return Me.DebitSeqNo
				Case "DebitProfitCentre" 
					Return Me.DebitProfitCentre
				Case "DebitReversalGLCode" 
					Return Me.DebitReversalGLCode
				Case "DebitReversalBranchCode" 
					Return Me.DebitReversalBranchCode
				Case "DebitReversalCurrency" 
					Return Me.DebitReversalCurrency
				Case "DebitReversalClientNo" 
					Return Me.DebitReversalClientNo
				Case "DebitReversalSeqNo" 
					Return Me.DebitReversalSeqNo
				Case "DebitReversalProfitCentre" 
					Return Me.DebitReversalProfitCentre
				Case "CreditGLCode" 
					Return Me.CreditGLCode
				Case "CreditBranchCode" 
					Return Me.CreditBranchCode
				Case "CreditCurrency" 
					Return Me.CreditCurrency
				Case "CreditClientNo" 
					Return Me.CreditClientNo
				Case "CreditSeqNo" 
					Return Me.CreditSeqNo
				Case "CreditProfitCentre" 
					Return Me.CreditProfitCentre
				Case "CreditReversalGLCode" 
					Return Me.CreditReversalGLCode
				Case "CreditReversalBranchCode" 
					Return Me.CreditReversalBranchCode
				Case "CreditReversalCurrency" 
					Return Me.CreditReversalCurrency
				Case "CreditReversalClientNo" 
					Return Me.CreditReversalClientNo
				Case "CreditReversalSeqNo" 
					Return Me.CreditReversalSeqNo
				Case "CreditReversalProfitCentre" 
					Return Me.CreditReversalProfitCentre
				Case "PayableAccountSeqNo" 
					Return Me.PayableAccountSeqNo
				Case "PayableAccountClientNo" 
					Return Me.PayableAccountClientNo
				Case "PayableAccountProfitCentre" 
					Return Me.PayableAccountProfitCentre
				Case "PayableAccountType" 
					Return Me.PayableAccountType
				Case "FundedAccountType" 
					Return Me.FundedAccountType
				Case "FundedAccountSeqNo" 
					Return Me.FundedAccountSeqNo
				Case "FundedAccountClientNo" 
					Return Me.FundedAccountClientNo
				Case "FundedAccountProfitCentre" 
					Return Me.FundedAccountProfitCentre
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "RespondingTranType" 
					Return Me.RespondingTranType
				Case "OriginatingTransactionType" 
					Return Me.OriginatingTransactionType
				Case "VoucherType" 
					Return Me.VoucherType
				Case "InstrumentType" 
					Return Me.InstrumentType
				Case "ReversalTransactionType" 
					Return Me.ReversalTransactionType
				Case "ClearingSameDayOrgTranType" 
					Return Me.ClearingSameDayOrgTranType
				Case "ClearingInterCityOrgTranType" 
					Return Me.ClearingInterCityOrgTranType
				Case "ClearingNormalOrgTranType" 
					Return Me.ClearingNormalOrgTranType
				Case "ClearingSameDayResTranType" 
					Return Me.ClearingSameDayResTranType
				Case "ClearingInterCityResTranType" 
					Return Me.ClearingInterCityResTranType
				Case "ClearingNormalResTranType" 
					Return Me.ClearingNormalResTranType
				Case "POCancellationTranType" 
					Return Me.POCancellationTranType
				Case "ClearingFTOrgTranType" 
					Return Me.ClearingFTOrgTranType
				Case "ClearingFTRespTranType" 
					Return Me.ClearingFTRespTranType
				Case "ReversalRespondingTranType" 
					Return Me.ReversalRespondingTranType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeName As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ProductTypeName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsSundaryAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.IsSundaryAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursementMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DisbursementMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property NonPrincipalBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.NonPrincipalBankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsFunded As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.IsFunded, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitTranCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitTranCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalTranCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalTranCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditTranCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditTranCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalTranCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalTranCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DebitReversalProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.DebitReversalProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalGLCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalGLCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreditReversalProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreditReversalProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.PayableAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FundedAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.FundedAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property RespondingTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.RespondingTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OriginatingTransactionType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.OriginatingTransactionType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property VoucherType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.VoucherType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.InstrumentType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReversalTransactionType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ReversalTransactionType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingSameDayOrgTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingSameDayOrgTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingInterCityOrgTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingInterCityOrgTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalOrgTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingNormalOrgTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingSameDayResTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingSameDayResTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingInterCityResTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingInterCityResTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalResTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingNormalResTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property POCancellationTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.POCancellationTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingFTOrgTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingFTOrgTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingFTRespTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ClearingFTRespTranType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReversalRespondingTranType As esQueryItem
			Get
				Return New esQueryItem(Me, ICProductTypeMetadata.ColumnNames.ReversalRespondingTranType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICProductType 
		Inherits esICProductType
		
	
		#Region "ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICProductType.ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode"
				map.MyColumnName = "ProductTypeCode"
				map.ParentColumnName = "ProductTypeCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICProductTypeQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureProductTypeQuery), New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_ProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode As ICAccountsPaymentNatureProductTypeCollection 
		
			Get
				If Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode Is Nothing Then
					Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode = New ICAccountsPaymentNatureProductTypeCollection()
					Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode", Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode)
				
					If Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode")
					Me._ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode As ICAccountsPaymentNatureProductTypeCollection
		#End Region

		#Region "ICAmendmentsRightsManagementCollectionByProductTypeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAmendmentsRightsManagementCollectionByProductTypeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICProductType.ICAmendmentsRightsManagementCollectionByProductTypeCode_Delegate)
				map.PropertyName = "ICAmendmentsRightsManagementCollectionByProductTypeCode"
				map.MyColumnName = "ProductTypeCode"
				map.ParentColumnName = "ProductTypeCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAmendmentsRightsManagementCollectionByProductTypeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICProductTypeQuery(data.NextAlias())
			
			Dim mee As ICAmendmentsRightsManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAmendmentsRightsManagementQuery), New ICAmendmentsRightsManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_ProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAmendmentsRightsManagementCollectionByProductTypeCode As ICAmendmentsRightsManagementCollection 
		
			Get
				If Me._ICAmendmentsRightsManagementCollectionByProductTypeCode Is Nothing Then
					Me._ICAmendmentsRightsManagementCollectionByProductTypeCode = New ICAmendmentsRightsManagementCollection()
					Me._ICAmendmentsRightsManagementCollectionByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAmendmentsRightsManagementCollectionByProductTypeCode", Me._ICAmendmentsRightsManagementCollectionByProductTypeCode)
				
					If Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAmendmentsRightsManagementCollectionByProductTypeCode.Query.Where(Me._ICAmendmentsRightsManagementCollectionByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICAmendmentsRightsManagementCollectionByProductTypeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAmendmentsRightsManagementCollectionByProductTypeCode.fks.Add(ICAmendmentsRightsManagementMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICAmendmentsRightsManagementCollectionByProductTypeCode
			End Get
			
			Set(ByVal value As ICAmendmentsRightsManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAmendmentsRightsManagementCollectionByProductTypeCode Is Nothing Then

					Me.RemovePostSave("ICAmendmentsRightsManagementCollectionByProductTypeCode")
					Me._ICAmendmentsRightsManagementCollectionByProductTypeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAmendmentsRightsManagementCollectionByProductTypeCode As ICAmendmentsRightsManagementCollection
		#End Region

		#Region "ICDDPayableAccountsCollectionByProductTypeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICDDPayableAccountsCollectionByProductTypeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICProductType.ICDDPayableAccountsCollectionByProductTypeCode_Delegate)
				map.PropertyName = "ICDDPayableAccountsCollectionByProductTypeCode"
				map.MyColumnName = "ProductTypeCode"
				map.ParentColumnName = "ProductTypeCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICDDPayableAccountsCollectionByProductTypeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICProductTypeQuery(data.NextAlias())
			
			Dim mee As ICDDPayableAccountsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICDDPayableAccountsQuery), New ICDDPayableAccountsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_DDPayableAccounts_IC_ProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICDDPayableAccountsCollectionByProductTypeCode As ICDDPayableAccountsCollection 
		
			Get
				If Me._ICDDPayableAccountsCollectionByProductTypeCode Is Nothing Then
					Me._ICDDPayableAccountsCollectionByProductTypeCode = New ICDDPayableAccountsCollection()
					Me._ICDDPayableAccountsCollectionByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICDDPayableAccountsCollectionByProductTypeCode", Me._ICDDPayableAccountsCollectionByProductTypeCode)
				
					If Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICDDPayableAccountsCollectionByProductTypeCode.Query.Where(Me._ICDDPayableAccountsCollectionByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICDDPayableAccountsCollectionByProductTypeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICDDPayableAccountsCollectionByProductTypeCode.fks.Add(ICDDPayableAccountsMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICDDPayableAccountsCollectionByProductTypeCode
			End Get
			
			Set(ByVal value As ICDDPayableAccountsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICDDPayableAccountsCollectionByProductTypeCode Is Nothing Then

					Me.RemovePostSave("ICDDPayableAccountsCollectionByProductTypeCode")
					Me._ICDDPayableAccountsCollectionByProductTypeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICDDPayableAccountsCollectionByProductTypeCode As ICDDPayableAccountsCollection
		#End Region

		#Region "ICInstructionCollectionByProductTypeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByProductTypeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICProductType.ICInstructionCollectionByProductTypeCode_Delegate)
				map.PropertyName = "ICInstructionCollectionByProductTypeCode"
				map.MyColumnName = "ProductTypeCode"
				map.ParentColumnName = "ProductTypeCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByProductTypeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICProductTypeQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_ProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByProductTypeCode As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByProductTypeCode Is Nothing Then
					Me._ICInstructionCollectionByProductTypeCode = New ICInstructionCollection()
					Me._ICInstructionCollectionByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByProductTypeCode", Me._ICInstructionCollectionByProductTypeCode)
				
					If Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByProductTypeCode.Query.Where(Me._ICInstructionCollectionByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICInstructionCollectionByProductTypeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByProductTypeCode.fks.Add(ICInstructionMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICInstructionCollectionByProductTypeCode
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByProductTypeCode Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByProductTypeCode")
					Me._ICInstructionCollectionByProductTypeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByProductTypeCode As ICInstructionCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode"
					coll = Me.ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode
					Exit Select
				Case "ICAmendmentsRightsManagementCollectionByProductTypeCode"
					coll = Me.ICAmendmentsRightsManagementCollectionByProductTypeCode
					Exit Select
				Case "ICDDPayableAccountsCollectionByProductTypeCode"
					coll = Me.ICDDPayableAccountsCollectionByProductTypeCode
					Exit Select
				Case "ICInstructionCollectionByProductTypeCode"
					coll = Me.ICInstructionCollectionByProductTypeCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureProductTypeCollectionByProductTypeCode", GetType(ICAccountsPaymentNatureProductTypeCollection), New ICAccountsPaymentNatureProductType()))
			props.Add(new esPropertyDescriptor(Me, "ICAmendmentsRightsManagementCollectionByProductTypeCode", GetType(ICAmendmentsRightsManagementCollection), New ICAmendmentsRightsManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICDDPayableAccountsCollectionByProductTypeCode", GetType(ICDDPayableAccountsCollection), New ICDDPayableAccounts()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByProductTypeCode", GetType(ICInstructionCollection), New ICInstruction()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICProductTypeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ProductTypeCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ProductTypeCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ProductTypeName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ProductTypeName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.IsSundaryAllow, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.IsSundaryAllow
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DisbursementMode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DisbursementMode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ApprovedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.IsApproved, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ApprovedOn, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountNumber, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountBranchCode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountCurrency, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.NonPrincipalBankCode, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.NonPrincipalBankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.IsFunded, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.IsFunded
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountNumber, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountBranchCode, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountCurrency, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitTranCode, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitTranCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalTranCode, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalTranCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditTranCode, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditTranCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalTranCode, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalTranCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitGLCode, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitGLCode
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitBranchCode, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitCurrency, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitClientNo, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitSeqNo, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitProfitCentre, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalGLCode, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalGLCode
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalBranchCode, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalCurrency, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalClientNo, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalSeqNo, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.DebitReversalProfitCentre, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.DebitReversalProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditGLCode, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditGLCode
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditBranchCode, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditCurrency, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditClientNo, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditSeqNo, 38, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditProfitCentre, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalGLCode, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalGLCode
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalBranchCode, 41, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalCurrency, 42, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalClientNo, 43, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalSeqNo, 44, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreditReversalProfitCentre, 45, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreditReversalProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountSeqNo, 46, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountSeqNo
			c.CharacterMaxLength = 50
			c.HasDefault = True
			c.Default = "((1))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountClientNo, 47, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountProfitCentre, 48, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.PayableAccountType, 49, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.PayableAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountType, 50, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountSeqNo, 51, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountClientNo, 52, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.FundedAccountProfitCentre, 53, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.FundedAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.Creater, 54, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.CreationDate, 55, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.RespondingTranType, 56, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.RespondingTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.OriginatingTransactionType, 57, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.OriginatingTransactionType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.VoucherType, 58, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.VoucherType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.InstrumentType, 59, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.InstrumentType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ReversalTransactionType, 60, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ReversalTransactionType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingSameDayOrgTranType, 61, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingSameDayOrgTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingInterCityOrgTranType, 62, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingInterCityOrgTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingNormalOrgTranType, 63, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingNormalOrgTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingSameDayResTranType, 64, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingSameDayResTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingInterCityResTranType, 65, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingInterCityResTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingNormalResTranType, 66, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingNormalResTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.POCancellationTranType, 67, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.POCancellationTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingFTOrgTranType, 68, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingFTOrgTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ClearingFTRespTranType, 69, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ClearingFTRespTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICProductTypeMetadata.ColumnNames.ReversalRespondingTranType, 70, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICProductTypeMetadata.PropertyNames.ReversalRespondingTranType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICProductTypeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ProductTypeName As String = "ProductTypeName"
			 Public Const IsSundaryAllow As String = "IsSundaryAllow"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const NonPrincipalBankCode As String = "NonPrincipalBankCode"
			 Public Const IsFunded As String = "IsFunded"
			 Public Const FundedAccountNumber As String = "FundedAccountNumber"
			 Public Const FundedAccountBranchCode As String = "FundedAccountBranchCode"
			 Public Const FundedAccountCurrency As String = "FundedAccountCurrency"
			 Public Const DebitTranCode As String = "DebitTranCode"
			 Public Const DebitReversalTranCode As String = "DebitReversalTranCode"
			 Public Const CreditTranCode As String = "CreditTranCode"
			 Public Const CreditReversalTranCode As String = "CreditReversalTranCode"
			 Public Const DebitGLCode As String = "DebitGLCode"
			 Public Const DebitBranchCode As String = "DebitBranchCode"
			 Public Const DebitCurrency As String = "DebitCurrency"
			 Public Const DebitClientNo As String = "DebitClientNo"
			 Public Const DebitSeqNo As String = "DebitSeqNo"
			 Public Const DebitProfitCentre As String = "DebitProfitCentre"
			 Public Const DebitReversalGLCode As String = "DebitReversalGLCode"
			 Public Const DebitReversalBranchCode As String = "DebitReversalBranchCode"
			 Public Const DebitReversalCurrency As String = "DebitReversalCurrency"
			 Public Const DebitReversalClientNo As String = "DebitReversalClientNo"
			 Public Const DebitReversalSeqNo As String = "DebitReversalSeqNo"
			 Public Const DebitReversalProfitCentre As String = "DebitReversalProfitCentre"
			 Public Const CreditGLCode As String = "CreditGLCode"
			 Public Const CreditBranchCode As String = "CreditBranchCode"
			 Public Const CreditCurrency As String = "CreditCurrency"
			 Public Const CreditClientNo As String = "CreditClientNo"
			 Public Const CreditSeqNo As String = "CreditSeqNo"
			 Public Const CreditProfitCentre As String = "CreditProfitCentre"
			 Public Const CreditReversalGLCode As String = "CreditReversalGLCode"
			 Public Const CreditReversalBranchCode As String = "CreditReversalBranchCode"
			 Public Const CreditReversalCurrency As String = "CreditReversalCurrency"
			 Public Const CreditReversalClientNo As String = "CreditReversalClientNo"
			 Public Const CreditReversalSeqNo As String = "CreditReversalSeqNo"
			 Public Const CreditReversalProfitCentre As String = "CreditReversalProfitCentre"
			 Public Const PayableAccountSeqNo As String = "PayableAccountSeqNo"
			 Public Const PayableAccountClientNo As String = "PayableAccountClientNo"
			 Public Const PayableAccountProfitCentre As String = "PayableAccountProfitCentre"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const FundedAccountType As String = "FundedAccountType"
			 Public Const FundedAccountSeqNo As String = "FundedAccountSeqNo"
			 Public Const FundedAccountClientNo As String = "FundedAccountClientNo"
			 Public Const FundedAccountProfitCentre As String = "FundedAccountProfitCentre"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const RespondingTranType As String = "RespondingTranType"
			 Public Const OriginatingTransactionType As String = "OriginatingTransactionType"
			 Public Const VoucherType As String = "VoucherType"
			 Public Const InstrumentType As String = "InstrumentType"
			 Public Const ReversalTransactionType As String = "ReversalTransactionType"
			 Public Const ClearingSameDayOrgTranType As String = "ClearingSameDayOrgTranType"
			 Public Const ClearingInterCityOrgTranType As String = "ClearingInterCityOrgTranType"
			 Public Const ClearingNormalOrgTranType As String = "ClearingNormalOrgTranType"
			 Public Const ClearingSameDayResTranType As String = "ClearingSameDayResTranType"
			 Public Const ClearingInterCityResTranType As String = "ClearingInterCityResTranType"
			 Public Const ClearingNormalResTranType As String = "ClearingNormalResTranType"
			 Public Const POCancellationTranType As String = "POCancellationTranType"
			 Public Const ClearingFTOrgTranType As String = "ClearingFTOrgTranType"
			 Public Const ClearingFTRespTranType As String = "ClearingFTRespTranType"
			 Public Const ReversalRespondingTranType As String = "ReversalRespondingTranType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ProductTypeName As String = "ProductTypeName"
			 Public Const IsSundaryAllow As String = "IsSundaryAllow"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const NonPrincipalBankCode As String = "NonPrincipalBankCode"
			 Public Const IsFunded As String = "IsFunded"
			 Public Const FundedAccountNumber As String = "FundedAccountNumber"
			 Public Const FundedAccountBranchCode As String = "FundedAccountBranchCode"
			 Public Const FundedAccountCurrency As String = "FundedAccountCurrency"
			 Public Const DebitTranCode As String = "DebitTranCode"
			 Public Const DebitReversalTranCode As String = "DebitReversalTranCode"
			 Public Const CreditTranCode As String = "CreditTranCode"
			 Public Const CreditReversalTranCode As String = "CreditReversalTranCode"
			 Public Const DebitGLCode As String = "DebitGLCode"
			 Public Const DebitBranchCode As String = "DebitBranchCode"
			 Public Const DebitCurrency As String = "DebitCurrency"
			 Public Const DebitClientNo As String = "DebitClientNo"
			 Public Const DebitSeqNo As String = "DebitSeqNo"
			 Public Const DebitProfitCentre As String = "DebitProfitCentre"
			 Public Const DebitReversalGLCode As String = "DebitReversalGLCode"
			 Public Const DebitReversalBranchCode As String = "DebitReversalBranchCode"
			 Public Const DebitReversalCurrency As String = "DebitReversalCurrency"
			 Public Const DebitReversalClientNo As String = "DebitReversalClientNo"
			 Public Const DebitReversalSeqNo As String = "DebitReversalSeqNo"
			 Public Const DebitReversalProfitCentre As String = "DebitReversalProfitCentre"
			 Public Const CreditGLCode As String = "CreditGLCode"
			 Public Const CreditBranchCode As String = "CreditBranchCode"
			 Public Const CreditCurrency As String = "CreditCurrency"
			 Public Const CreditClientNo As String = "CreditClientNo"
			 Public Const CreditSeqNo As String = "CreditSeqNo"
			 Public Const CreditProfitCentre As String = "CreditProfitCentre"
			 Public Const CreditReversalGLCode As String = "CreditReversalGLCode"
			 Public Const CreditReversalBranchCode As String = "CreditReversalBranchCode"
			 Public Const CreditReversalCurrency As String = "CreditReversalCurrency"
			 Public Const CreditReversalClientNo As String = "CreditReversalClientNo"
			 Public Const CreditReversalSeqNo As String = "CreditReversalSeqNo"
			 Public Const CreditReversalProfitCentre As String = "CreditReversalProfitCentre"
			 Public Const PayableAccountSeqNo As String = "PayableAccountSeqNo"
			 Public Const PayableAccountClientNo As String = "PayableAccountClientNo"
			 Public Const PayableAccountProfitCentre As String = "PayableAccountProfitCentre"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const FundedAccountType As String = "FundedAccountType"
			 Public Const FundedAccountSeqNo As String = "FundedAccountSeqNo"
			 Public Const FundedAccountClientNo As String = "FundedAccountClientNo"
			 Public Const FundedAccountProfitCentre As String = "FundedAccountProfitCentre"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const RespondingTranType As String = "RespondingTranType"
			 Public Const OriginatingTransactionType As String = "OriginatingTransactionType"
			 Public Const VoucherType As String = "VoucherType"
			 Public Const InstrumentType As String = "InstrumentType"
			 Public Const ReversalTransactionType As String = "ReversalTransactionType"
			 Public Const ClearingSameDayOrgTranType As String = "ClearingSameDayOrgTranType"
			 Public Const ClearingInterCityOrgTranType As String = "ClearingInterCityOrgTranType"
			 Public Const ClearingNormalOrgTranType As String = "ClearingNormalOrgTranType"
			 Public Const ClearingSameDayResTranType As String = "ClearingSameDayResTranType"
			 Public Const ClearingInterCityResTranType As String = "ClearingInterCityResTranType"
			 Public Const ClearingNormalResTranType As String = "ClearingNormalResTranType"
			 Public Const POCancellationTranType As String = "POCancellationTranType"
			 Public Const ClearingFTOrgTranType As String = "ClearingFTOrgTranType"
			 Public Const ClearingFTRespTranType As String = "ClearingFTRespTranType"
			 Public Const ReversalRespondingTranType As String = "ReversalRespondingTranType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICProductTypeMetadata)
			
				If ICProductTypeMetadata.mapDelegates Is Nothing Then
					ICProductTypeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICProductTypeMetadata._meta Is Nothing Then
					ICProductTypeMetadata._meta = New ICProductTypeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsSundaryAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("DisbursementMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PayableAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("NonPrincipalBankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsFunded", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FundedAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitTranCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalTranCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditTranCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalTranCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DebitReversalProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalGLCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreditReversalProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FundedAccountProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("RespondingTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OriginatingTransactionType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("VoucherType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InstrumentType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReversalTransactionType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingSameDayOrgTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingInterCityOrgTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalOrgTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingSameDayResTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingInterCityResTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalResTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("POCancellationTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingFTOrgTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingFTRespTranType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReversalRespondingTranType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_ProductType"
				meta.Destination = "IC_ProductType"
				
				meta.spInsert = "proc_IC_ProductTypeInsert"
				meta.spUpdate = "proc_IC_ProductTypeUpdate"
				meta.spDelete = "proc_IC_ProductTypeDelete"
				meta.spLoadAll = "proc_IC_ProductTypeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ProductTypeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICProductTypeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
