
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/30/2015 3:59:47 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_BeneProductType' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBeneProductType))> _
	<XmlType("ICBeneProductType")> _	
	Partial Public Class ICBeneProductType 
		Inherits esICBeneProductType
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBeneProductType()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal benePTID As System.Int32)
			Dim obj As New ICBeneProductType()
			obj.BenePTID = benePTID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal benePTID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBeneProductType()
			obj.BenePTID = benePTID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBeneProductTypeCollection")> _
	Partial Public Class ICBeneProductTypeCollection
		Inherits esICBeneProductTypeCollection
		Implements IEnumerable(Of ICBeneProductType)
	
		Public Function FindByPrimaryKey(ByVal benePTID As System.Int32) As ICBeneProductType
			Return MyBase.SingleOrDefault(Function(e) e.BenePTID.HasValue AndAlso e.BenePTID.Value = benePTID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBeneProductType))> _
		Public Class ICBeneProductTypeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBeneProductTypeCollection)
			
			Public Shared Widening Operator CType(packet As ICBeneProductTypeCollectionWCFPacket) As ICBeneProductTypeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBeneProductTypeCollection) As ICBeneProductTypeCollectionWCFPacket
				Return New ICBeneProductTypeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBeneProductTypeQuery 
		Inherits esICBeneProductTypeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBeneProductTypeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBeneProductTypeQuery) As String
			Return ICBeneProductTypeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBeneProductTypeQuery
			Return DirectCast(ICBeneProductTypeQuery.SerializeHelper.FromXml(query, GetType(ICBeneProductTypeQuery)), ICBeneProductTypeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBeneProductType
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal benePTID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(benePTID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(benePTID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal benePTID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(benePTID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(benePTID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal benePTID As System.Int32) As Boolean
		
			Dim query As New ICBeneProductTypeQuery()
			query.Where(query.BenePTID = benePTID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal benePTID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BenePTID", benePTID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_BeneProductType.BenePTID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BenePTID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.BenePTID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.BenePTID, value) Then
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.BenePTID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneProductType.BeneID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.BeneID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.BeneID, value) Then
					Me._UpToICBeneByBeneID = Nothing
					Me.OnPropertyChanged("UpToICBeneByBeneID")
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.BeneID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneProductType.BeneficiaryCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneProductTypeMetadata.ColumnNames.BeneficiaryCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneProductTypeMetadata.ColumnNames.BeneficiaryCode, value) Then
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.BeneficiaryCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneProductType.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneProductTypeMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneProductTypeMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneProductType.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneProductTypeMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneProductType.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneProductTypeMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneProductTypeMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBeneByBeneID As ICBene
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BenePTID"
							Me.str().BenePTID = CType(value, string)
												
						Case "BeneID"
							Me.str().BeneID = CType(value, string)
												
						Case "BeneficiaryCode"
							Me.str().BeneficiaryCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BenePTID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BenePTID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.BenePTID)
							End If
						
						Case "BeneID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BeneID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.BeneID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneProductTypeMetadata.PropertyNames.CreatedDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBeneProductType)
				Me.entity = entity
			End Sub				
		
	
			Public Property BenePTID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BenePTID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BenePTID = Nothing
					Else
						entity.BenePTID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BeneID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneID = Nothing
					Else
						entity.BeneID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCode = Nothing
					Else
						entity.BeneficiaryCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBeneProductType
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneProductTypeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBeneProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBeneProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBeneProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBeneProductTypeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBeneProductTypeCollection
		Inherits esEntityCollection(Of ICBeneProductType)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneProductTypeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBeneProductTypeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBeneProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBeneProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBeneProductTypeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBeneProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBeneProductTypeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBeneProductTypeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBeneProductTypeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBeneProductTypeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BenePTID" 
					Return Me.BenePTID
				Case "BeneID" 
					Return Me.BeneID
				Case "BeneficiaryCode" 
					Return Me.BeneficiaryCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BenePTID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.BenePTID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.BeneID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.BeneficiaryCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneProductTypeMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBeneProductType 
		Inherits esICBeneProductType
		
	
		#Region "UpToICBeneByBeneID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_BeneProductType_IC_Bene
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBeneByBeneID As ICBene
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBeneByBeneID Is Nothing _
						 AndAlso Not BeneID.Equals(Nothing)  Then
						
					Me._UpToICBeneByBeneID = New ICBene()
					Me._UpToICBeneByBeneID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBeneByBeneID", Me._UpToICBeneByBeneID)
					Me._UpToICBeneByBeneID.Query.Where(Me._UpToICBeneByBeneID.Query.BeneID = Me.BeneID)
					Me._UpToICBeneByBeneID.Query.Load()
				End If

				Return Me._UpToICBeneByBeneID
			End Get
			
            Set(ByVal value As ICBene)
				Me.RemovePreSave("UpToICBeneByBeneID")
				

				If value Is Nothing Then
				
					Me.BeneID = Nothing
				
					Me._UpToICBeneByBeneID = Nothing
				Else
				
					Me.BeneID = value.BeneID
					
					Me._UpToICBeneByBeneID = value
					Me.SetPreSave("UpToICBeneByBeneID", Me._UpToICBeneByBeneID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICBeneByBeneID Is Nothing Then
				Me.BeneID = Me._UpToICBeneByBeneID.BeneID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICBeneProductTypeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.BenePTID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.BenePTID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.BeneID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.BeneID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.BeneficiaryCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.BeneficiaryCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.ProductTypeCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.CreatedBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneProductTypeMetadata.ColumnNames.CreatedDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneProductTypeMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBeneProductTypeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BenePTID As String = "BenePTID"
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BenePTID As String = "BenePTID"
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBeneProductTypeMetadata)
			
				If ICBeneProductTypeMetadata.mapDelegates Is Nothing Then
					ICBeneProductTypeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBeneProductTypeMetadata._meta Is Nothing Then
					ICBeneProductTypeMetadata._meta = New ICBeneProductTypeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BenePTID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneficiaryCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_BeneProductType"
				meta.Destination = "IC_BeneProductType"
				
				meta.spInsert = "proc_IC_BeneProductTypeInsert"
				meta.spUpdate = "proc_IC_BeneProductTypeUpdate"
				meta.spDelete = "proc_IC_BeneProductTypeDelete"
				meta.spLoadAll = "proc_IC_BeneProductTypeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BeneProductTypeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBeneProductTypeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
