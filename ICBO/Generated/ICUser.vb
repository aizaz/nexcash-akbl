
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/21/2015 11:37:52 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_User' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUser))> _
	<XmlType("ICUser")> _	
	Partial Public Class ICUser 
		Inherits esICUser
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUser()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal userID As System.Int32)
			Dim obj As New ICUser()
			obj.UserID = userID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal userID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUser()
			obj.UserID = userID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUserCollection")> _
	Partial Public Class ICUserCollection
		Inherits esICUserCollection
		Implements IEnumerable(Of ICUser)
	
		Public Function FindByPrimaryKey(ByVal userID As System.Int32) As ICUser
			Return MyBase.SingleOrDefault(Function(e) e.UserID.HasValue AndAlso e.UserID.Value = userID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUser))> _
		Public Class ICUserCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUserCollection)
			
			Public Shared Widening Operator CType(packet As ICUserCollectionWCFPacket) As ICUserCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUserCollection) As ICUserCollectionWCFPacket
				Return New ICUserCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUserQuery 
		Inherits esICUserQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUserQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUserQuery) As String
			Return ICUserQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUserQuery
			Return DirectCast(ICUserQuery.SerializeHelper.FromXml(query, GetType(ICUserQuery)), ICUserQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUser
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal userID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal userID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal userID As System.Int32) As Boolean
		
			Dim query As New ICUserQuery()
			query.Where(query.UserID = userID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal userID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UserID", userID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_User.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserMetadata.ColumnNames.UserID, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.UserName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserName As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.UserName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.UserName, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.UserName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.DisplayName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisplayName As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.DisplayName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.DisplayName, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.DisplayName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Password
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Password As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.Password)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.Password, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Password)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.OfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserMetadata.ColumnNames.OfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserMetadata.ColumnNames.OfficeCode, value) Then
					Me._UpToICOfficeByOfficeCode = Nothing
					Me.OnPropertyChanged("UpToICOfficeByOfficeCode")
					OnPropertyChanged(ICUserMetadata.PropertyNames.OfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.PasswordExpiryDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PasswordExpiryDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.PasswordExpiryDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.PasswordExpiryDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.PasswordExpiryDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.CellNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CellNo As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.CellNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.CellNo, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.CellNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.PhoneNo1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhoneNo1 As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.PhoneNo1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.PhoneNo1, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.PhoneNo1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.PhoneNo2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhoneNo2 As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.PhoneNo2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.PhoneNo2, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.PhoneNo2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Email
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Email As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.Email)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.Email, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Email)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsICAdmin
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsICAdmin As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsICAdmin)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsICAdmin, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsICAdmin)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.LastActivityDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastActivityDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.LastActivityDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.LastActivityDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.LastActivityDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.LastLogInDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastLogInDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.LastLogInDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.LastLogInDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.LastLogInDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.LastIPAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastIPAddress As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.LastIPAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.LastIPAddress, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.LastIPAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Location
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Location As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.Location)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.Location, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Location)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.UserType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserType As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.UserType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.UserType, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.UserType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsBlocked
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsBlocked As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsBlocked)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsBlocked, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsBlocked)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.LastPasswordChangedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastPasswordChangedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.LastPasswordChangedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.LastPasswordChangedDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.LastPasswordChangedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Department
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Department As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.Department)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.Department, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Department)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Is2FARequiredOnLogin
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Is2FARequiredOnLogin As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.Is2FARequiredOnLogin)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.Is2FARequiredOnLogin, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Is2FARequiredOnLogin)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Is2FARequiredOnApproval
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Is2FARequiredOnApproval As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.Is2FARequiredOnApproval)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.Is2FARequiredOnApproval, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Is2FARequiredOnApproval)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsTwoFAVIASMSAllowForLogin
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsTwoFAVIASMSAllowForLogin As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForLogin)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForLogin, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForLogin)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsTwoFAVIAEmailAllowForLogin
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsTwoFAVIAEmailAllowForLogin As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForLogin)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForLogin, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForLogin)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.EmployeeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EmployeeCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.EmployeeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.EmployeeCode, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.EmployeeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsTwoFAVIASMSAllowForApproval
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsTwoFAVIASMSAllowForApproval As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForApproval)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForApproval, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForApproval)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsTwoFAVIAEmailAllowForApproval
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsTwoFAVIAEmailAllowForApproval As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForApproval)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForApproval, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForApproval)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.AuthenticatedVia
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AuthenticatedVia As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.AuthenticatedVia)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.AuthenticatedVia, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.AuthenticatedVia)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsNotificationSent
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsNotificationSent As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsNotificationSent)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsNotificationSent, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsNotificationSent)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.IsLoggedIn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsLoggedIn As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserMetadata.ColumnNames.IsLoggedIn)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserMetadata.ColumnNames.IsLoggedIn, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.IsLoggedIn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_User.ClearingBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserMetadata.ColumnNames.ClearingBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserMetadata.ColumnNames.ClearingBranchCode, value) Then
					OnPropertyChanged(ICUserMetadata.PropertyNames.ClearingBranchCode)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByOfficeCode As ICOffice
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "UserName"
							Me.str().UserName = CType(value, string)
												
						Case "DisplayName"
							Me.str().DisplayName = CType(value, string)
												
						Case "Password"
							Me.str().Password = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "OfficeCode"
							Me.str().OfficeCode = CType(value, string)
												
						Case "PasswordExpiryDate"
							Me.str().PasswordExpiryDate = CType(value, string)
												
						Case "CellNo"
							Me.str().CellNo = CType(value, string)
												
						Case "PhoneNo1"
							Me.str().PhoneNo1 = CType(value, string)
												
						Case "PhoneNo2"
							Me.str().PhoneNo2 = CType(value, string)
												
						Case "Email"
							Me.str().Email = CType(value, string)
												
						Case "IsICAdmin"
							Me.str().IsICAdmin = CType(value, string)
												
						Case "LastActivityDate"
							Me.str().LastActivityDate = CType(value, string)
												
						Case "LastLogInDate"
							Me.str().LastLogInDate = CType(value, string)
												
						Case "LastIPAddress"
							Me.str().LastIPAddress = CType(value, string)
												
						Case "Location"
							Me.str().Location = CType(value, string)
												
						Case "UserType"
							Me.str().UserType = CType(value, string)
												
						Case "IsBlocked"
							Me.str().IsBlocked = CType(value, string)
												
						Case "LastPasswordChangedDate"
							Me.str().LastPasswordChangedDate = CType(value, string)
												
						Case "Department"
							Me.str().Department = CType(value, string)
												
						Case "Is2FARequiredOnLogin"
							Me.str().Is2FARequiredOnLogin = CType(value, string)
												
						Case "Is2FARequiredOnApproval"
							Me.str().Is2FARequiredOnApproval = CType(value, string)
												
						Case "IsTwoFAVIASMSAllowForLogin"
							Me.str().IsTwoFAVIASMSAllowForLogin = CType(value, string)
												
						Case "IsTwoFAVIAEmailAllowForLogin"
							Me.str().IsTwoFAVIAEmailAllowForLogin = CType(value, string)
												
						Case "EmployeeCode"
							Me.str().EmployeeCode = CType(value, string)
												
						Case "IsTwoFAVIASMSAllowForApproval"
							Me.str().IsTwoFAVIASMSAllowForApproval = CType(value, string)
												
						Case "IsTwoFAVIAEmailAllowForApproval"
							Me.str().IsTwoFAVIAEmailAllowForApproval = CType(value, string)
												
						Case "AuthenticatedVia"
							Me.str().AuthenticatedVia = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsNotificationSent"
							Me.str().IsNotificationSent = CType(value, string)
												
						Case "IsLoggedIn"
							Me.str().IsLoggedIn = CType(value, string)
												
						Case "ClearingBranchCode"
							Me.str().ClearingBranchCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserMetadata.PropertyNames.UserID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsApproved)
							End If
						
						Case "OfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserMetadata.PropertyNames.OfficeCode)
							End If
						
						Case "PasswordExpiryDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PasswordExpiryDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.PasswordExpiryDate)
							End If
						
						Case "IsICAdmin"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsICAdmin = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsICAdmin)
							End If
						
						Case "LastActivityDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastActivityDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.LastActivityDate)
							End If
						
						Case "LastLogInDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastLogInDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.LastLogInDate)
							End If
						
						Case "IsBlocked"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsBlocked = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsBlocked)
							End If
						
						Case "LastPasswordChangedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastPasswordChangedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.LastPasswordChangedDate)
							End If
						
						Case "Is2FARequiredOnLogin"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.Is2FARequiredOnLogin = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.Is2FARequiredOnLogin)
							End If
						
						Case "Is2FARequiredOnApproval"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.Is2FARequiredOnApproval = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.Is2FARequiredOnApproval)
							End If
						
						Case "IsTwoFAVIASMSAllowForLogin"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsTwoFAVIASMSAllowForLogin = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForLogin)
							End If
						
						Case "IsTwoFAVIAEmailAllowForLogin"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsTwoFAVIAEmailAllowForLogin = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForLogin)
							End If
						
						Case "IsTwoFAVIASMSAllowForApproval"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsTwoFAVIASMSAllowForApproval = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForApproval)
							End If
						
						Case "IsTwoFAVIAEmailAllowForApproval"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsTwoFAVIAEmailAllowForApproval = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForApproval)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsNotificationSent"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsNotificationSent = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsNotificationSent)
							End If
						
						Case "IsLoggedIn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsLoggedIn = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserMetadata.PropertyNames.IsLoggedIn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUser)
				Me.entity = entity
			End Sub				
		
	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserName As System.String 
				Get
					Dim data_ As System.String = entity.UserName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserName = Nothing
					Else
						entity.UserName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisplayName As System.String 
				Get
					Dim data_ As System.String = entity.DisplayName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisplayName = Nothing
					Else
						entity.DisplayName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Password As System.String 
				Get
					Dim data_ As System.String = entity.Password
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Password = Nothing
					Else
						entity.Password = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeCode = Nothing
					Else
						entity.OfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PasswordExpiryDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PasswordExpiryDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PasswordExpiryDate = Nothing
					Else
						entity.PasswordExpiryDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CellNo As System.String 
				Get
					Dim data_ As System.String = entity.CellNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CellNo = Nothing
					Else
						entity.CellNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhoneNo1 As System.String 
				Get
					Dim data_ As System.String = entity.PhoneNo1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhoneNo1 = Nothing
					Else
						entity.PhoneNo1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhoneNo2 As System.String 
				Get
					Dim data_ As System.String = entity.PhoneNo2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhoneNo2 = Nothing
					Else
						entity.PhoneNo2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Email As System.String 
				Get
					Dim data_ As System.String = entity.Email
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Email = Nothing
					Else
						entity.Email = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsICAdmin As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsICAdmin
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsICAdmin = Nothing
					Else
						entity.IsICAdmin = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastActivityDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastActivityDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastActivityDate = Nothing
					Else
						entity.LastActivityDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastLogInDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastLogInDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastLogInDate = Nothing
					Else
						entity.LastLogInDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastIPAddress As System.String 
				Get
					Dim data_ As System.String = entity.LastIPAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastIPAddress = Nothing
					Else
						entity.LastIPAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Location As System.String 
				Get
					Dim data_ As System.String = entity.Location
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Location = Nothing
					Else
						entity.Location = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserType As System.String 
				Get
					Dim data_ As System.String = entity.UserType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserType = Nothing
					Else
						entity.UserType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsBlocked As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsBlocked
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsBlocked = Nothing
					Else
						entity.IsBlocked = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastPasswordChangedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastPasswordChangedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastPasswordChangedDate = Nothing
					Else
						entity.LastPasswordChangedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Department As System.String 
				Get
					Dim data_ As System.String = entity.Department
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Department = Nothing
					Else
						entity.Department = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Is2FARequiredOnLogin As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.Is2FARequiredOnLogin
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Is2FARequiredOnLogin = Nothing
					Else
						entity.Is2FARequiredOnLogin = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Is2FARequiredOnApproval As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.Is2FARequiredOnApproval
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Is2FARequiredOnApproval = Nothing
					Else
						entity.Is2FARequiredOnApproval = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsTwoFAVIASMSAllowForLogin As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsTwoFAVIASMSAllowForLogin
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsTwoFAVIASMSAllowForLogin = Nothing
					Else
						entity.IsTwoFAVIASMSAllowForLogin = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsTwoFAVIAEmailAllowForLogin As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsTwoFAVIAEmailAllowForLogin
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsTwoFAVIAEmailAllowForLogin = Nothing
					Else
						entity.IsTwoFAVIAEmailAllowForLogin = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property EmployeeCode As System.String 
				Get
					Dim data_ As System.String = entity.EmployeeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EmployeeCode = Nothing
					Else
						entity.EmployeeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsTwoFAVIASMSAllowForApproval As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsTwoFAVIASMSAllowForApproval
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsTwoFAVIASMSAllowForApproval = Nothing
					Else
						entity.IsTwoFAVIASMSAllowForApproval = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsTwoFAVIAEmailAllowForApproval As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsTwoFAVIAEmailAllowForApproval
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsTwoFAVIAEmailAllowForApproval = Nothing
					Else
						entity.IsTwoFAVIAEmailAllowForApproval = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property AuthenticatedVia As System.String 
				Get
					Dim data_ As System.String = entity.AuthenticatedVia
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AuthenticatedVia = Nothing
					Else
						entity.AuthenticatedVia = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsNotificationSent As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsNotificationSent
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsNotificationSent = Nothing
					Else
						entity.IsNotificationSent = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsLoggedIn As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsLoggedIn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsLoggedIn = Nothing
					Else
						entity.IsLoggedIn = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClearingBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingBranchCode = Nothing
					Else
						entity.ClearingBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUser
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUserQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUserQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUserQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUserQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUserCollection
		Inherits esEntityCollection(Of ICUser)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUserCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUserQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUserQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUserQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUserQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUserQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUserQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUserQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUserMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UserID" 
					Return Me.UserID
				Case "UserName" 
					Return Me.UserName
				Case "DisplayName" 
					Return Me.DisplayName
				Case "Password" 
					Return Me.Password
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "OfficeCode" 
					Return Me.OfficeCode
				Case "PasswordExpiryDate" 
					Return Me.PasswordExpiryDate
				Case "CellNo" 
					Return Me.CellNo
				Case "PhoneNo1" 
					Return Me.PhoneNo1
				Case "PhoneNo2" 
					Return Me.PhoneNo2
				Case "Email" 
					Return Me.Email
				Case "IsICAdmin" 
					Return Me.IsICAdmin
				Case "LastActivityDate" 
					Return Me.LastActivityDate
				Case "LastLogInDate" 
					Return Me.LastLogInDate
				Case "LastIPAddress" 
					Return Me.LastIPAddress
				Case "Location" 
					Return Me.Location
				Case "UserType" 
					Return Me.UserType
				Case "IsBlocked" 
					Return Me.IsBlocked
				Case "LastPasswordChangedDate" 
					Return Me.LastPasswordChangedDate
				Case "Department" 
					Return Me.Department
				Case "Is2FARequiredOnLogin" 
					Return Me.Is2FARequiredOnLogin
				Case "Is2FARequiredOnApproval" 
					Return Me.Is2FARequiredOnApproval
				Case "IsTwoFAVIASMSAllowForLogin" 
					Return Me.IsTwoFAVIASMSAllowForLogin
				Case "IsTwoFAVIAEmailAllowForLogin" 
					Return Me.IsTwoFAVIAEmailAllowForLogin
				Case "EmployeeCode" 
					Return Me.EmployeeCode
				Case "IsTwoFAVIASMSAllowForApproval" 
					Return Me.IsTwoFAVIASMSAllowForApproval
				Case "IsTwoFAVIAEmailAllowForApproval" 
					Return Me.IsTwoFAVIAEmailAllowForApproval
				Case "AuthenticatedVia" 
					Return Me.AuthenticatedVia
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsNotificationSent" 
					Return Me.IsNotificationSent
				Case "IsLoggedIn" 
					Return Me.IsLoggedIn
				Case "ClearingBranchCode" 
					Return Me.ClearingBranchCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserName As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.UserName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisplayName As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.DisplayName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Password As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Password, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.OfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PasswordExpiryDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.PasswordExpiryDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CellNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.CellNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PhoneNo1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.PhoneNo1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PhoneNo2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.PhoneNo2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Email As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Email, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsICAdmin As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsICAdmin, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property LastActivityDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.LastActivityDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property LastLogInDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.LastLogInDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property LastIPAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.LastIPAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Location As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Location, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UserType As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.UserType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsBlocked As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsBlocked, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property LastPasswordChangedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.LastPasswordChangedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Department As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Department, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Is2FARequiredOnLogin As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Is2FARequiredOnLogin, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Is2FARequiredOnApproval As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Is2FARequiredOnApproval, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsTwoFAVIASMSAllowForLogin As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForLogin, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsTwoFAVIAEmailAllowForLogin As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForLogin, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property EmployeeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.EmployeeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsTwoFAVIASMSAllowForApproval As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForApproval, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsTwoFAVIAEmailAllowForApproval As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForApproval, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property AuthenticatedVia As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.AuthenticatedVia, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsNotificationSent As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsNotificationSent, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsLoggedIn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.IsLoggedIn, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserMetadata.ColumnNames.ClearingBranchCode, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUser 
		Inherits esICUser
		
	
		#Region "ICAccountsCollectionByCreateBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsCollectionByCreateBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICAccountsCollectionByCreateBy_Delegate)
				map.PropertyName = "ICAccountsCollectionByCreateBy"
				map.MyColumnName = "CreateBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsCollectionByCreateBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICAccountsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsQuery), New ICAccountsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreateBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Accounts_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsCollectionByCreateBy As ICAccountsCollection 
		
			Get
				If Me._ICAccountsCollectionByCreateBy Is Nothing Then
					Me._ICAccountsCollectionByCreateBy = New ICAccountsCollection()
					Me._ICAccountsCollectionByCreateBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsCollectionByCreateBy", Me._ICAccountsCollectionByCreateBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsCollectionByCreateBy.Query.Where(Me._ICAccountsCollectionByCreateBy.Query.CreateBy = Me.UserID)
							Me._ICAccountsCollectionByCreateBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsCollectionByCreateBy.fks.Add(ICAccountsMetadata.ColumnNames.CreateBy, Me.UserID)
					End If
				End If

				Return Me._ICAccountsCollectionByCreateBy
			End Get
			
			Set(ByVal value As ICAccountsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsCollectionByCreateBy Is Nothing Then

					Me.RemovePostSave("ICAccountsCollectionByCreateBy")
					Me._ICAccountsCollectionByCreateBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsCollectionByCreateBy As ICAccountsCollection
		#End Region

		#Region "ICAmendmentsRightsManagementCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAmendmentsRightsManagementCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICAmendmentsRightsManagementCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICAmendmentsRightsManagementCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAmendmentsRightsManagementCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICAmendmentsRightsManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAmendmentsRightsManagementQuery), New ICAmendmentsRightsManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAmendmentsRightsManagementCollectionByCreatedBy As ICAmendmentsRightsManagementCollection 
		
			Get
				If Me._ICAmendmentsRightsManagementCollectionByCreatedBy Is Nothing Then
					Me._ICAmendmentsRightsManagementCollectionByCreatedBy = New ICAmendmentsRightsManagementCollection()
					Me._ICAmendmentsRightsManagementCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAmendmentsRightsManagementCollectionByCreatedBy", Me._ICAmendmentsRightsManagementCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAmendmentsRightsManagementCollectionByCreatedBy.Query.Where(Me._ICAmendmentsRightsManagementCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICAmendmentsRightsManagementCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAmendmentsRightsManagementCollectionByCreatedBy.fks.Add(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICAmendmentsRightsManagementCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICAmendmentsRightsManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAmendmentsRightsManagementCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICAmendmentsRightsManagementCollectionByCreatedBy")
					Me._ICAmendmentsRightsManagementCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAmendmentsRightsManagementCollectionByCreatedBy As ICAmendmentsRightsManagementCollection
		#End Region

		#Region "ICApprovalGroupManagementCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupManagementCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalGroupManagementCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICApprovalGroupManagementCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupManagementCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupManagementQuery), New ICApprovalGroupManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupManagement_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupManagementCollectionByCreatedBy As ICApprovalGroupManagementCollection 
		
			Get
				If Me._ICApprovalGroupManagementCollectionByCreatedBy Is Nothing Then
					Me._ICApprovalGroupManagementCollectionByCreatedBy = New ICApprovalGroupManagementCollection()
					Me._ICApprovalGroupManagementCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupManagementCollectionByCreatedBy", Me._ICApprovalGroupManagementCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupManagementCollectionByCreatedBy.Query.Where(Me._ICApprovalGroupManagementCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICApprovalGroupManagementCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupManagementCollectionByCreatedBy.fks.Add(ICApprovalGroupManagementMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICApprovalGroupManagementCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICApprovalGroupManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupManagementCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupManagementCollectionByCreatedBy")
					Me._ICApprovalGroupManagementCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupManagementCollectionByCreatedBy As ICApprovalGroupManagementCollection
		#End Region

		#Region "ICApprovalGroupUsersCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupUsersCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalGroupUsersCollectionByUserID_Delegate)
				map.PropertyName = "ICApprovalGroupUsersCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupUsersCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupUsersQuery), New ICApprovalGroupUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupUsersCollectionByUserID As ICApprovalGroupUsersCollection 
		
			Get
				If Me._ICApprovalGroupUsersCollectionByUserID Is Nothing Then
					Me._ICApprovalGroupUsersCollectionByUserID = New ICApprovalGroupUsersCollection()
					Me._ICApprovalGroupUsersCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupUsersCollectionByUserID", Me._ICApprovalGroupUsersCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupUsersCollectionByUserID.Query.Where(Me._ICApprovalGroupUsersCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICApprovalGroupUsersCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupUsersCollectionByUserID.fks.Add(ICApprovalGroupUsersMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICApprovalGroupUsersCollectionByUserID
			End Get
			
			Set(ByVal value As ICApprovalGroupUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupUsersCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupUsersCollectionByUserID")
					Me._ICApprovalGroupUsersCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupUsersCollectionByUserID As ICApprovalGroupUsersCollection
		#End Region

		#Region "ICApprovalGroupUsersCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupUsersCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalGroupUsersCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICApprovalGroupUsersCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupUsersCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupUsersQuery), New ICApprovalGroupUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupUsersCollectionByCreatedBy As ICApprovalGroupUsersCollection 
		
			Get
				If Me._ICApprovalGroupUsersCollectionByCreatedBy Is Nothing Then
					Me._ICApprovalGroupUsersCollectionByCreatedBy = New ICApprovalGroupUsersCollection()
					Me._ICApprovalGroupUsersCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupUsersCollectionByCreatedBy", Me._ICApprovalGroupUsersCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupUsersCollectionByCreatedBy.Query.Where(Me._ICApprovalGroupUsersCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICApprovalGroupUsersCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupUsersCollectionByCreatedBy.fks.Add(ICApprovalGroupUsersMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICApprovalGroupUsersCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICApprovalGroupUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupUsersCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupUsersCollectionByCreatedBy")
					Me._ICApprovalGroupUsersCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupUsersCollectionByCreatedBy As ICApprovalGroupUsersCollection
		#End Region

		#Region "ICApprovalGroupUsersCollectionByCreator - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupUsersCollectionByCreator() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalGroupUsersCollectionByCreator_Delegate)
				map.PropertyName = "ICApprovalGroupUsersCollectionByCreator"
				map.MyColumnName = "Creator"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupUsersCollectionByCreator_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupUsersQuery), New ICApprovalGroupUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.Creator)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_User2
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupUsersCollectionByCreator As ICApprovalGroupUsersCollection 
		
			Get
				If Me._ICApprovalGroupUsersCollectionByCreator Is Nothing Then
					Me._ICApprovalGroupUsersCollectionByCreator = New ICApprovalGroupUsersCollection()
					Me._ICApprovalGroupUsersCollectionByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupUsersCollectionByCreator", Me._ICApprovalGroupUsersCollectionByCreator)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupUsersCollectionByCreator.Query.Where(Me._ICApprovalGroupUsersCollectionByCreator.Query.Creator = Me.UserID)
							Me._ICApprovalGroupUsersCollectionByCreator.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupUsersCollectionByCreator.fks.Add(ICApprovalGroupUsersMetadata.ColumnNames.Creator, Me.UserID)
					End If
				End If

				Return Me._ICApprovalGroupUsersCollectionByCreator
			End Get
			
			Set(ByVal value As ICApprovalGroupUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupUsersCollectionByCreator Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupUsersCollectionByCreator")
					Me._ICApprovalGroupUsersCollectionByCreator = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupUsersCollectionByCreator As ICApprovalGroupUsersCollection
		#End Region

		#Region "ICApprovalRuleCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalRuleCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICApprovalRuleCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleQuery), New ICApprovalRuleQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleCollectionByCreatedBy As ICApprovalRuleCollection 
		
			Get
				If Me._ICApprovalRuleCollectionByCreatedBy Is Nothing Then
					Me._ICApprovalRuleCollectionByCreatedBy = New ICApprovalRuleCollection()
					Me._ICApprovalRuleCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleCollectionByCreatedBy", Me._ICApprovalRuleCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleCollectionByCreatedBy.Query.Where(Me._ICApprovalRuleCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICApprovalRuleCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleCollectionByCreatedBy.fks.Add(ICApprovalRuleMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICApprovalRuleCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICApprovalRuleCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleCollectionByCreatedBy")
					Me._ICApprovalRuleCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleCollectionByCreatedBy As ICApprovalRuleCollection
		#End Region

		#Region "ICApprovalRuleCollectionByCreator - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleCollectionByCreator() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalRuleCollectionByCreator_Delegate)
				map.PropertyName = "ICApprovalRuleCollectionByCreator"
				map.MyColumnName = "Creator"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleCollectionByCreator_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleQuery), New ICApprovalRuleQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.Creator)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleCollectionByCreator As ICApprovalRuleCollection 
		
			Get
				If Me._ICApprovalRuleCollectionByCreator Is Nothing Then
					Me._ICApprovalRuleCollectionByCreator = New ICApprovalRuleCollection()
					Me._ICApprovalRuleCollectionByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleCollectionByCreator", Me._ICApprovalRuleCollectionByCreator)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleCollectionByCreator.Query.Where(Me._ICApprovalRuleCollectionByCreator.Query.Creator = Me.UserID)
							Me._ICApprovalRuleCollectionByCreator.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleCollectionByCreator.fks.Add(ICApprovalRuleMetadata.ColumnNames.Creator, Me.UserID)
					End If
				End If

				Return Me._ICApprovalRuleCollectionByCreator
			End Get
			
			Set(ByVal value As ICApprovalRuleCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleCollectionByCreator Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleCollectionByCreator")
					Me._ICApprovalRuleCollectionByCreator = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleCollectionByCreator As ICApprovalRuleCollection
		#End Region

		#Region "ICApprovalRuleConditionsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleConditionsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalRuleConditionsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICApprovalRuleConditionsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleConditionsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleConditionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleConditionsQuery), New ICApprovalRuleConditionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleConditionsCollectionByCreatedBy As ICApprovalRuleConditionsCollection 
		
			Get
				If Me._ICApprovalRuleConditionsCollectionByCreatedBy Is Nothing Then
					Me._ICApprovalRuleConditionsCollectionByCreatedBy = New ICApprovalRuleConditionsCollection()
					Me._ICApprovalRuleConditionsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleConditionsCollectionByCreatedBy", Me._ICApprovalRuleConditionsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleConditionsCollectionByCreatedBy.Query.Where(Me._ICApprovalRuleConditionsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICApprovalRuleConditionsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleConditionsCollectionByCreatedBy.fks.Add(ICApprovalRuleConditionsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICApprovalRuleConditionsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICApprovalRuleConditionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleConditionsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleConditionsCollectionByCreatedBy")
					Me._ICApprovalRuleConditionsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleConditionsCollectionByCreatedBy As ICApprovalRuleConditionsCollection
		#End Region

		#Region "ICApprovalRuleConditionsCollectionByCreator - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleConditionsCollectionByCreator() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICApprovalRuleConditionsCollectionByCreator_Delegate)
				map.PropertyName = "ICApprovalRuleConditionsCollectionByCreator"
				map.MyColumnName = "Creator"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleConditionsCollectionByCreator_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleConditionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleConditionsQuery), New ICApprovalRuleConditionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.Creator)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleConditionsCollectionByCreator As ICApprovalRuleConditionsCollection 
		
			Get
				If Me._ICApprovalRuleConditionsCollectionByCreator Is Nothing Then
					Me._ICApprovalRuleConditionsCollectionByCreator = New ICApprovalRuleConditionsCollection()
					Me._ICApprovalRuleConditionsCollectionByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleConditionsCollectionByCreator", Me._ICApprovalRuleConditionsCollectionByCreator)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleConditionsCollectionByCreator.Query.Where(Me._ICApprovalRuleConditionsCollectionByCreator.Query.Creator = Me.UserID)
							Me._ICApprovalRuleConditionsCollectionByCreator.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleConditionsCollectionByCreator.fks.Add(ICApprovalRuleConditionsMetadata.ColumnNames.Creator, Me.UserID)
					End If
				End If

				Return Me._ICApprovalRuleConditionsCollectionByCreator
			End Get
			
			Set(ByVal value As ICApprovalRuleConditionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleConditionsCollectionByCreator Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleConditionsCollectionByCreator")
					Me._ICApprovalRuleConditionsCollectionByCreator = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleConditionsCollectionByCreator As ICApprovalRuleConditionsCollection
		#End Region

		#Region "ICCollectionAccountsCollectionByCreateBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionByCreateBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICCollectionAccountsCollectionByCreateBy_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionByCreateBy"
				map.MyColumnName = "CreateBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionByCreateBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsQuery), New ICCollectionAccountsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreateBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccounts_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionByCreateBy As ICCollectionAccountsCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionByCreateBy Is Nothing Then
					Me._ICCollectionAccountsCollectionByCreateBy = New ICCollectionAccountsCollection()
					Me._ICCollectionAccountsCollectionByCreateBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionByCreateBy", Me._ICCollectionAccountsCollectionByCreateBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionByCreateBy.Query.Where(Me._ICCollectionAccountsCollectionByCreateBy.Query.CreateBy = Me.UserID)
							Me._ICCollectionAccountsCollectionByCreateBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionByCreateBy.fks.Add(ICCollectionAccountsMetadata.ColumnNames.CreateBy, Me.UserID)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionByCreateBy
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionByCreateBy Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionByCreateBy")
					Me._ICCollectionAccountsCollectionByCreateBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionByCreateBy As ICCollectionAccountsCollection
		#End Region

		#Region "ICDDInstrumentsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICDDInstrumentsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICDDInstrumentsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICDDInstrumentsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICDDInstrumentsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICDDInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICDDInstrumentsQuery), New ICDDInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_DDInstruments_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICDDInstrumentsCollectionByCreatedBy As ICDDInstrumentsCollection 
		
			Get
				If Me._ICDDInstrumentsCollectionByCreatedBy Is Nothing Then
					Me._ICDDInstrumentsCollectionByCreatedBy = New ICDDInstrumentsCollection()
					Me._ICDDInstrumentsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICDDInstrumentsCollectionByCreatedBy", Me._ICDDInstrumentsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICDDInstrumentsCollectionByCreatedBy.Query.Where(Me._ICDDInstrumentsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICDDInstrumentsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICDDInstrumentsCollectionByCreatedBy.fks.Add(ICDDInstrumentsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICDDInstrumentsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICDDInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICDDInstrumentsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICDDInstrumentsCollectionByCreatedBy")
					Me._ICDDInstrumentsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICDDInstrumentsCollectionByCreatedBy As ICDDInstrumentsCollection
		#End Region

		#Region "ICDDMasterSeriesCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICDDMasterSeriesCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICDDMasterSeriesCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICDDMasterSeriesCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICDDMasterSeriesCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICDDMasterSeriesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICDDMasterSeriesQuery), New ICDDMasterSeriesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_DDMasterSeries_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICDDMasterSeriesCollectionByCreatedBy As ICDDMasterSeriesCollection 
		
			Get
				If Me._ICDDMasterSeriesCollectionByCreatedBy Is Nothing Then
					Me._ICDDMasterSeriesCollectionByCreatedBy = New ICDDMasterSeriesCollection()
					Me._ICDDMasterSeriesCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICDDMasterSeriesCollectionByCreatedBy", Me._ICDDMasterSeriesCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICDDMasterSeriesCollectionByCreatedBy.Query.Where(Me._ICDDMasterSeriesCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICDDMasterSeriesCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICDDMasterSeriesCollectionByCreatedBy.fks.Add(ICDDMasterSeriesMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICDDMasterSeriesCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICDDMasterSeriesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICDDMasterSeriesCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICDDMasterSeriesCollectionByCreatedBy")
					Me._ICDDMasterSeriesCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICDDMasterSeriesCollectionByCreatedBy As ICDDMasterSeriesCollection
		#End Region

		#Region "ICEmailSettingsCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICEmailSettingsCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICEmailSettingsCollectionByUserID_Delegate)
				map.PropertyName = "ICEmailSettingsCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICEmailSettingsCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICEmailSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICEmailSettingsQuery), New ICEmailSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_EmailSettings_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICEmailSettingsCollectionByUserID As ICEmailSettingsCollection 
		
			Get
				If Me._ICEmailSettingsCollectionByUserID Is Nothing Then
					Me._ICEmailSettingsCollectionByUserID = New ICEmailSettingsCollection()
					Me._ICEmailSettingsCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICEmailSettingsCollectionByUserID", Me._ICEmailSettingsCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICEmailSettingsCollectionByUserID.Query.Where(Me._ICEmailSettingsCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICEmailSettingsCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICEmailSettingsCollectionByUserID.fks.Add(ICEmailSettingsMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICEmailSettingsCollectionByUserID
			End Get
			
			Set(ByVal value As ICEmailSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICEmailSettingsCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICEmailSettingsCollectionByUserID")
					Me._ICEmailSettingsCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICEmailSettingsCollectionByUserID As ICEmailSettingsCollection
		#End Region

		#Region "ICEmailSettingsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICEmailSettingsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICEmailSettingsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICEmailSettingsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICEmailSettingsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICEmailSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICEmailSettingsQuery), New ICEmailSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_EmailSettings_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICEmailSettingsCollectionByCreatedBy As ICEmailSettingsCollection 
		
			Get
				If Me._ICEmailSettingsCollectionByCreatedBy Is Nothing Then
					Me._ICEmailSettingsCollectionByCreatedBy = New ICEmailSettingsCollection()
					Me._ICEmailSettingsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICEmailSettingsCollectionByCreatedBy", Me._ICEmailSettingsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICEmailSettingsCollectionByCreatedBy.Query.Where(Me._ICEmailSettingsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICEmailSettingsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICEmailSettingsCollectionByCreatedBy.fks.Add(ICEmailSettingsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICEmailSettingsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICEmailSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICEmailSettingsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICEmailSettingsCollectionByCreatedBy")
					Me._ICEmailSettingsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICEmailSettingsCollectionByCreatedBy As ICEmailSettingsCollection
		#End Region

		#Region "ICFTPSettingsCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFTPSettingsCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICFTPSettingsCollectionByUserID_Delegate)
				map.PropertyName = "ICFTPSettingsCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICFTPSettingsCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICFTPSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFTPSettingsQuery), New ICFTPSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FTPSettings_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFTPSettingsCollectionByUserID As ICFTPSettingsCollection 
		
			Get
				If Me._ICFTPSettingsCollectionByUserID Is Nothing Then
					Me._ICFTPSettingsCollectionByUserID = New ICFTPSettingsCollection()
					Me._ICFTPSettingsCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFTPSettingsCollectionByUserID", Me._ICFTPSettingsCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFTPSettingsCollectionByUserID.Query.Where(Me._ICFTPSettingsCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICFTPSettingsCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFTPSettingsCollectionByUserID.fks.Add(ICFTPSettingsMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICFTPSettingsCollectionByUserID
			End Get
			
			Set(ByVal value As ICFTPSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFTPSettingsCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICFTPSettingsCollectionByUserID")
					Me._ICFTPSettingsCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFTPSettingsCollectionByUserID As ICFTPSettingsCollection
		#End Region

		#Region "ICFTPSettingsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFTPSettingsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICFTPSettingsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICFTPSettingsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICFTPSettingsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICFTPSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFTPSettingsQuery), New ICFTPSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FTPSettings_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFTPSettingsCollectionByCreatedBy As ICFTPSettingsCollection 
		
			Get
				If Me._ICFTPSettingsCollectionByCreatedBy Is Nothing Then
					Me._ICFTPSettingsCollectionByCreatedBy = New ICFTPSettingsCollection()
					Me._ICFTPSettingsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFTPSettingsCollectionByCreatedBy", Me._ICFTPSettingsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFTPSettingsCollectionByCreatedBy.Query.Where(Me._ICFTPSettingsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICFTPSettingsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFTPSettingsCollectionByCreatedBy.fks.Add(ICFTPSettingsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICFTPSettingsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICFTPSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFTPSettingsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICFTPSettingsCollectionByCreatedBy")
					Me._ICFTPSettingsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFTPSettingsCollectionByCreatedBy As ICFTPSettingsCollection
		#End Region

		#Region "ICInstructionCollectionByClientPrimaryApprovedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByClientPrimaryApprovedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICInstructionCollectionByClientPrimaryApprovedBy_Delegate)
				map.PropertyName = "ICInstructionCollectionByClientPrimaryApprovedBy"
				map.MyColumnName = "ClientPrimaryApprovedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByClientPrimaryApprovedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.ClientPrimaryApprovedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByClientPrimaryApprovedBy As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByClientPrimaryApprovedBy Is Nothing Then
					Me._ICInstructionCollectionByClientPrimaryApprovedBy = New ICInstructionCollection()
					Me._ICInstructionCollectionByClientPrimaryApprovedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByClientPrimaryApprovedBy", Me._ICInstructionCollectionByClientPrimaryApprovedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByClientPrimaryApprovedBy.Query.Where(Me._ICInstructionCollectionByClientPrimaryApprovedBy.Query.ClientPrimaryApprovedBy = Me.UserID)
							Me._ICInstructionCollectionByClientPrimaryApprovedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByClientPrimaryApprovedBy.fks.Add(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedBy, Me.UserID)
					End If
				End If

				Return Me._ICInstructionCollectionByClientPrimaryApprovedBy
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByClientPrimaryApprovedBy Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByClientPrimaryApprovedBy")
					Me._ICInstructionCollectionByClientPrimaryApprovedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByClientPrimaryApprovedBy As ICInstructionCollection
		#End Region

		#Region "ICInstructionCollectionByClientSecondaryApprovedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByClientSecondaryApprovedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICInstructionCollectionByClientSecondaryApprovedBy_Delegate)
				map.PropertyName = "ICInstructionCollectionByClientSecondaryApprovedBy"
				map.MyColumnName = "ClientSecondaryApprovedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByClientSecondaryApprovedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.ClientSecondaryApprovedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByClientSecondaryApprovedBy As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByClientSecondaryApprovedBy Is Nothing Then
					Me._ICInstructionCollectionByClientSecondaryApprovedBy = New ICInstructionCollection()
					Me._ICInstructionCollectionByClientSecondaryApprovedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByClientSecondaryApprovedBy", Me._ICInstructionCollectionByClientSecondaryApprovedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByClientSecondaryApprovedBy.Query.Where(Me._ICInstructionCollectionByClientSecondaryApprovedBy.Query.ClientSecondaryApprovedBy = Me.UserID)
							Me._ICInstructionCollectionByClientSecondaryApprovedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByClientSecondaryApprovedBy.fks.Add(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovedBy, Me.UserID)
					End If
				End If

				Return Me._ICInstructionCollectionByClientSecondaryApprovedBy
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByClientSecondaryApprovedBy Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByClientSecondaryApprovedBy")
					Me._ICInstructionCollectionByClientSecondaryApprovedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByClientSecondaryApprovedBy As ICInstructionCollection
		#End Region

		#Region "ICInstructionApprovalLogCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionApprovalLogCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICInstructionApprovalLogCollectionByUserID_Delegate)
				map.PropertyName = "ICInstructionApprovalLogCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionApprovalLogCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICInstructionApprovalLogQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionApprovalLogQuery), New ICInstructionApprovalLogQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionApprovalLogCollectionByUserID As ICInstructionApprovalLogCollection 
		
			Get
				If Me._ICInstructionApprovalLogCollectionByUserID Is Nothing Then
					Me._ICInstructionApprovalLogCollectionByUserID = New ICInstructionApprovalLogCollection()
					Me._ICInstructionApprovalLogCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionApprovalLogCollectionByUserID", Me._ICInstructionApprovalLogCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionApprovalLogCollectionByUserID.Query.Where(Me._ICInstructionApprovalLogCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICInstructionApprovalLogCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionApprovalLogCollectionByUserID.fks.Add(ICInstructionApprovalLogMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICInstructionApprovalLogCollectionByUserID
			End Get
			
			Set(ByVal value As ICInstructionApprovalLogCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionApprovalLogCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICInstructionApprovalLogCollectionByUserID")
					Me._ICInstructionApprovalLogCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionApprovalLogCollectionByUserID As ICInstructionApprovalLogCollection
		#End Region

		#Region "ICInstrumentsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstrumentsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICInstrumentsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICInstrumentsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstrumentsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstrumentsQuery), New ICInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruments_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstrumentsCollectionByCreatedBy As ICInstrumentsCollection 
		
			Get
				If Me._ICInstrumentsCollectionByCreatedBy Is Nothing Then
					Me._ICInstrumentsCollectionByCreatedBy = New ICInstrumentsCollection()
					Me._ICInstrumentsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstrumentsCollectionByCreatedBy", Me._ICInstrumentsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstrumentsCollectionByCreatedBy.Query.Where(Me._ICInstrumentsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICInstrumentsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstrumentsCollectionByCreatedBy.fks.Add(ICInstrumentsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICInstrumentsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstrumentsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICInstrumentsCollectionByCreatedBy")
					Me._ICInstrumentsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstrumentsCollectionByCreatedBy As ICInstrumentsCollection
		#End Region

		#Region "ICLimitForApprovalCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICLimitForApprovalCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICLimitForApprovalCollectionByUserID_Delegate)
				map.PropertyName = "ICLimitForApprovalCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICLimitForApprovalCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICLimitForApprovalQuery = If(data.You IsNot Nothing, TryCast(data.You, ICLimitForApprovalQuery), New ICLimitForApprovalQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_LimitForApproval_IC_User2
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICLimitForApprovalCollectionByUserID As ICLimitForApprovalCollection 
		
			Get
				If Me._ICLimitForApprovalCollectionByUserID Is Nothing Then
					Me._ICLimitForApprovalCollectionByUserID = New ICLimitForApprovalCollection()
					Me._ICLimitForApprovalCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICLimitForApprovalCollectionByUserID", Me._ICLimitForApprovalCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICLimitForApprovalCollectionByUserID.Query.Where(Me._ICLimitForApprovalCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICLimitForApprovalCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICLimitForApprovalCollectionByUserID.fks.Add(ICLimitForApprovalMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICLimitForApprovalCollectionByUserID
			End Get
			
			Set(ByVal value As ICLimitForApprovalCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICLimitForApprovalCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICLimitForApprovalCollectionByUserID")
					Me._ICLimitForApprovalCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICLimitForApprovalCollectionByUserID As ICLimitForApprovalCollection
		#End Region

		#Region "ICLimitsForClearingCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICLimitsForClearingCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICLimitsForClearingCollectionByUserID_Delegate)
				map.PropertyName = "ICLimitsForClearingCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICLimitsForClearingCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICLimitsForClearingQuery = If(data.You IsNot Nothing, TryCast(data.You, ICLimitsForClearingQuery), New ICLimitsForClearingQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_LimitsForClearing_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICLimitsForClearingCollectionByUserID As ICLimitsForClearingCollection 
		
			Get
				If Me._ICLimitsForClearingCollectionByUserID Is Nothing Then
					Me._ICLimitsForClearingCollectionByUserID = New ICLimitsForClearingCollection()
					Me._ICLimitsForClearingCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICLimitsForClearingCollectionByUserID", Me._ICLimitsForClearingCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICLimitsForClearingCollectionByUserID.Query.Where(Me._ICLimitsForClearingCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICLimitsForClearingCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICLimitsForClearingCollectionByUserID.fks.Add(ICLimitsForClearingMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICLimitsForClearingCollectionByUserID
			End Get
			
			Set(ByVal value As ICLimitsForClearingCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICLimitsForClearingCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICLimitsForClearingCollectionByUserID")
					Me._ICLimitsForClearingCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICLimitsForClearingCollectionByUserID As ICLimitsForClearingCollection
		#End Region

		#Region "ICMISReportsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICMISReportsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICMISReportsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICMISReportsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICMISReportsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICMISReportsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICMISReportsQuery), New ICMISReportsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MISReports_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICMISReportsCollectionByCreatedBy As ICMISReportsCollection 
		
			Get
				If Me._ICMISReportsCollectionByCreatedBy Is Nothing Then
					Me._ICMISReportsCollectionByCreatedBy = New ICMISReportsCollection()
					Me._ICMISReportsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICMISReportsCollectionByCreatedBy", Me._ICMISReportsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICMISReportsCollectionByCreatedBy.Query.Where(Me._ICMISReportsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICMISReportsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICMISReportsCollectionByCreatedBy.fks.Add(ICMISReportsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICMISReportsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICMISReportsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICMISReportsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICMISReportsCollectionByCreatedBy")
					Me._ICMISReportsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICMISReportsCollectionByCreatedBy As ICMISReportsCollection
		#End Region

		#Region "ICMISReportUsersCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICMISReportUsersCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICMISReportUsersCollectionByUserID_Delegate)
				map.PropertyName = "ICMISReportUsersCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICMISReportUsersCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICMISReportUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICMISReportUsersQuery), New ICMISReportUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MISReportUsers_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICMISReportUsersCollectionByUserID As ICMISReportUsersCollection 
		
			Get
				If Me._ICMISReportUsersCollectionByUserID Is Nothing Then
					Me._ICMISReportUsersCollectionByUserID = New ICMISReportUsersCollection()
					Me._ICMISReportUsersCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICMISReportUsersCollectionByUserID", Me._ICMISReportUsersCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICMISReportUsersCollectionByUserID.Query.Where(Me._ICMISReportUsersCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICMISReportUsersCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICMISReportUsersCollectionByUserID.fks.Add(ICMISReportUsersMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICMISReportUsersCollectionByUserID
			End Get
			
			Set(ByVal value As ICMISReportUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICMISReportUsersCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICMISReportUsersCollectionByUserID")
					Me._ICMISReportUsersCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICMISReportUsersCollectionByUserID As ICMISReportUsersCollection
		#End Region

		#Region "ICMISReportUsersCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICMISReportUsersCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICMISReportUsersCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICMISReportUsersCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICMISReportUsersCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICMISReportUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICMISReportUsersQuery), New ICMISReportUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MISReportUsers_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICMISReportUsersCollectionByCreatedBy As ICMISReportUsersCollection 
		
			Get
				If Me._ICMISReportUsersCollectionByCreatedBy Is Nothing Then
					Me._ICMISReportUsersCollectionByCreatedBy = New ICMISReportUsersCollection()
					Me._ICMISReportUsersCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICMISReportUsersCollectionByCreatedBy", Me._ICMISReportUsersCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICMISReportUsersCollectionByCreatedBy.Query.Where(Me._ICMISReportUsersCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICMISReportUsersCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICMISReportUsersCollectionByCreatedBy.fks.Add(ICMISReportUsersMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICMISReportUsersCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICMISReportUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICMISReportUsersCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICMISReportUsersCollectionByCreatedBy")
					Me._ICMISReportUsersCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICMISReportUsersCollectionByCreatedBy As ICMISReportUsersCollection
		#End Region

		#Region "ICNotificationManagementCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICNotificationManagementCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICNotificationManagementCollectionByUserID_Delegate)
				map.PropertyName = "ICNotificationManagementCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICNotificationManagementCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICNotificationManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICNotificationManagementQuery), New ICNotificationManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICNotificationManagementCollectionByUserID As ICNotificationManagementCollection 
		
			Get
				If Me._ICNotificationManagementCollectionByUserID Is Nothing Then
					Me._ICNotificationManagementCollectionByUserID = New ICNotificationManagementCollection()
					Me._ICNotificationManagementCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICNotificationManagementCollectionByUserID", Me._ICNotificationManagementCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICNotificationManagementCollectionByUserID.Query.Where(Me._ICNotificationManagementCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICNotificationManagementCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICNotificationManagementCollectionByUserID.fks.Add(ICNotificationManagementMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICNotificationManagementCollectionByUserID
			End Get
			
			Set(ByVal value As ICNotificationManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICNotificationManagementCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICNotificationManagementCollectionByUserID")
					Me._ICNotificationManagementCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICNotificationManagementCollectionByUserID As ICNotificationManagementCollection
		#End Region

		#Region "ICNotificationManagementCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICNotificationManagementCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICNotificationManagementCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICNotificationManagementCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICNotificationManagementCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICNotificationManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICNotificationManagementQuery), New ICNotificationManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_User1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICNotificationManagementCollectionByCreatedBy As ICNotificationManagementCollection 
		
			Get
				If Me._ICNotificationManagementCollectionByCreatedBy Is Nothing Then
					Me._ICNotificationManagementCollectionByCreatedBy = New ICNotificationManagementCollection()
					Me._ICNotificationManagementCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICNotificationManagementCollectionByCreatedBy", Me._ICNotificationManagementCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICNotificationManagementCollectionByCreatedBy.Query.Where(Me._ICNotificationManagementCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICNotificationManagementCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICNotificationManagementCollectionByCreatedBy.fks.Add(ICNotificationManagementMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICNotificationManagementCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICNotificationManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICNotificationManagementCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICNotificationManagementCollectionByCreatedBy")
					Me._ICNotificationManagementCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICNotificationManagementCollectionByCreatedBy As ICNotificationManagementCollection
		#End Region

		#Region "ICPOInstrumentsCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPOInstrumentsCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICPOInstrumentsCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICPOInstrumentsCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPOInstrumentsCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICPOInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPOInstrumentsQuery), New ICPOInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_POInstruments_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPOInstrumentsCollectionByCreatedBy As ICPOInstrumentsCollection 
		
			Get
				If Me._ICPOInstrumentsCollectionByCreatedBy Is Nothing Then
					Me._ICPOInstrumentsCollectionByCreatedBy = New ICPOInstrumentsCollection()
					Me._ICPOInstrumentsCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPOInstrumentsCollectionByCreatedBy", Me._ICPOInstrumentsCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPOInstrumentsCollectionByCreatedBy.Query.Where(Me._ICPOInstrumentsCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICPOInstrumentsCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPOInstrumentsCollectionByCreatedBy.fks.Add(ICPOInstrumentsMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICPOInstrumentsCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICPOInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPOInstrumentsCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICPOInstrumentsCollectionByCreatedBy")
					Me._ICPOInstrumentsCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPOInstrumentsCollectionByCreatedBy As ICPOInstrumentsCollection
		#End Region

		#Region "ICPOMasterSeriesCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPOMasterSeriesCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICPOMasterSeriesCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICPOMasterSeriesCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPOMasterSeriesCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICPOMasterSeriesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPOMasterSeriesQuery), New ICPOMasterSeriesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_POMasterSeries_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPOMasterSeriesCollectionByCreatedBy As ICPOMasterSeriesCollection 
		
			Get
				If Me._ICPOMasterSeriesCollectionByCreatedBy Is Nothing Then
					Me._ICPOMasterSeriesCollectionByCreatedBy = New ICPOMasterSeriesCollection()
					Me._ICPOMasterSeriesCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPOMasterSeriesCollectionByCreatedBy", Me._ICPOMasterSeriesCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPOMasterSeriesCollectionByCreatedBy.Query.Where(Me._ICPOMasterSeriesCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICPOMasterSeriesCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPOMasterSeriesCollectionByCreatedBy.fks.Add(ICPOMasterSeriesMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICPOMasterSeriesCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICPOMasterSeriesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPOMasterSeriesCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICPOMasterSeriesCollectionByCreatedBy")
					Me._ICPOMasterSeriesCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPOMasterSeriesCollectionByCreatedBy As ICPOMasterSeriesCollection
		#End Region

		#Region "ICSubSetCollectionByCreatedBy - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSetCollectionByCreatedBy() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICSubSetCollectionByCreatedBy_Delegate)
				map.PropertyName = "ICSubSetCollectionByCreatedBy"
				map.MyColumnName = "CreatedBy"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSetCollectionByCreatedBy_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICSubSetQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSetQuery), New ICSubSetQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.CreatedBy)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSet_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSetCollectionByCreatedBy As ICSubSetCollection 
		
			Get
				If Me._ICSubSetCollectionByCreatedBy Is Nothing Then
					Me._ICSubSetCollectionByCreatedBy = New ICSubSetCollection()
					Me._ICSubSetCollectionByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSetCollectionByCreatedBy", Me._ICSubSetCollectionByCreatedBy)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSetCollectionByCreatedBy.Query.Where(Me._ICSubSetCollectionByCreatedBy.Query.CreatedBy = Me.UserID)
							Me._ICSubSetCollectionByCreatedBy.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSetCollectionByCreatedBy.fks.Add(ICSubSetMetadata.ColumnNames.CreatedBy, Me.UserID)
					End If
				End If

				Return Me._ICSubSetCollectionByCreatedBy
			End Get
			
			Set(ByVal value As ICSubSetCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSetCollectionByCreatedBy Is Nothing Then

					Me.RemovePostSave("ICSubSetCollectionByCreatedBy")
					Me._ICSubSetCollectionByCreatedBy = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSetCollectionByCreatedBy As ICSubSetCollection
		#End Region

		#Region "ICTaggedClientUserForAccountCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICTaggedClientUserForAccountCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICTaggedClientUserForAccountCollectionByUserID_Delegate)
				map.PropertyName = "ICTaggedClientUserForAccountCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICTaggedClientUserForAccountCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICTaggedClientUserForAccountQuery = If(data.You IsNot Nothing, TryCast(data.You, ICTaggedClientUserForAccountQuery), New ICTaggedClientUserForAccountQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_TaggedUser_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICTaggedClientUserForAccountCollectionByUserID As ICTaggedClientUserForAccountCollection 
		
			Get
				If Me._ICTaggedClientUserForAccountCollectionByUserID Is Nothing Then
					Me._ICTaggedClientUserForAccountCollectionByUserID = New ICTaggedClientUserForAccountCollection()
					Me._ICTaggedClientUserForAccountCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICTaggedClientUserForAccountCollectionByUserID", Me._ICTaggedClientUserForAccountCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICTaggedClientUserForAccountCollectionByUserID.Query.Where(Me._ICTaggedClientUserForAccountCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICTaggedClientUserForAccountCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICTaggedClientUserForAccountCollectionByUserID.fks.Add(ICTaggedClientUserForAccountMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICTaggedClientUserForAccountCollectionByUserID
			End Get
			
			Set(ByVal value As ICTaggedClientUserForAccountCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICTaggedClientUserForAccountCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICTaggedClientUserForAccountCollectionByUserID")
					Me._ICTaggedClientUserForAccountCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICTaggedClientUserForAccountCollectionByUserID As ICTaggedClientUserForAccountCollection
		#End Region

		#Region "UpToICRoleCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_UserRoles_IC_User
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICRoleCollection As ICRoleCollection
		
			Get
				If Me._UpToICRoleCollection Is Nothing Then
					Me._UpToICRoleCollection = New ICRoleCollection()
					Me._UpToICRoleCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICRoleCollection", Me._UpToICRoleCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.UserID.Equals(Nothing) Then 
				
						Dim m As New ICRoleQuery("m")
						Dim j As New ICUserRolesQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.RoleID = j.RoleID)
                        m.Where(j.UserID = Me.UserID)

						Me._UpToICRoleCollection.Load(m)

					End If
				End If

				Return Me._UpToICRoleCollection
			End Get
			
			Set(ByVal value As ICRoleCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICRoleCollection Is Nothing Then

					Me.RemovePostSave("UpToICRoleCollection")
					Me._UpToICRoleCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_UserRoles_IC_User
		''' </summary>
		Public Sub AssociateICRoleCollection(entity As ICRole)
			If Me._ICUserRolesCollection Is Nothing Then
				Me._ICUserRolesCollection = New ICUserRolesCollection()
				Me._ICUserRolesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICUserRolesCollection", Me._ICUserRolesCollection)
			End If
			
			Dim obj As ICUserRoles = Me._ICUserRolesCollection.AddNew()
			obj.UserID = Me.UserID
			obj.RoleID = entity.RoleID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_UserRoles_IC_User
		''' </summary>
		Public Sub DissociateICRoleCollection(entity As ICRole)
			If Me._ICUserRolesCollection Is Nothing Then
				Me._ICUserRolesCollection = new ICUserRolesCollection()
				Me._ICUserRolesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICUserRolesCollection", Me._ICUserRolesCollection)
			End If

			Dim obj As ICUserRoles = Me._ICUserRolesCollection.AddNew()
			obj.UserID = Me.UserID
            obj.RoleID = entity.RoleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICRoleCollection As ICRoleCollection
		Private _ICUserRolesCollection As ICUserRolesCollection
		#End Region

		#Region "ICUserRolesCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICUserRolesCollectionByUserID_Delegate)
				map.PropertyName = "ICUserRolesCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICUserRolesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesQuery), New ICUserRolesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRoles_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesCollectionByUserID As ICUserRolesCollection 
		
			Get
				If Me._ICUserRolesCollectionByUserID Is Nothing Then
					Me._ICUserRolesCollectionByUserID = New ICUserRolesCollection()
					Me._ICUserRolesCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesCollectionByUserID", Me._ICUserRolesCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesCollectionByUserID.Query.Where(Me._ICUserRolesCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICUserRolesCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesCollectionByUserID.fks.Add(ICUserRolesMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICUserRolesCollectionByUserID
			End Get
			
			Set(ByVal value As ICUserRolesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICUserRolesCollectionByUserID")
					Me._ICUserRolesCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesCollectionByUserID As ICUserRolesCollection
		#End Region

		#Region "ICUserRolesAccountAndPaymentNatureCollectionByUserID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesAccountAndPaymentNatureCollectionByUserID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUser.ICUserRolesAccountAndPaymentNatureCollectionByUserID_Delegate)
				map.PropertyName = "ICUserRolesAccountAndPaymentNatureCollectionByUserID"
				map.MyColumnName = "UserID"
				map.ParentColumnName = "UserID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesAccountAndPaymentNatureCollectionByUserID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserQuery(data.NextAlias())
			
			Dim mee As ICUserRolesAccountAndPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesAccountAndPaymentNatureQuery), New ICUserRolesAccountAndPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.UserID = mee.UserID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_User
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesAccountAndPaymentNatureCollectionByUserID As ICUserRolesAccountAndPaymentNatureCollection 
		
			Get
				If Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID Is Nothing Then
					Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID = New ICUserRolesAccountAndPaymentNatureCollection()
					Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesAccountAndPaymentNatureCollectionByUserID", Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID)
				
					If Not Me.UserID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID.Query.UserID = Me.UserID)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.UserID, Me.UserID)
					End If
				End If

				Return Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID
			End Get
			
			Set(ByVal value As ICUserRolesAccountAndPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID Is Nothing Then

					Me.RemovePostSave("ICUserRolesAccountAndPaymentNatureCollectionByUserID")
					Me._ICUserRolesAccountAndPaymentNatureCollectionByUserID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesAccountAndPaymentNatureCollectionByUserID As ICUserRolesAccountAndPaymentNatureCollection
		#End Region

		#Region "UpToICOfficeByOfficeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_User_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByOfficeCode As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByOfficeCode Is Nothing _
						 AndAlso Not OfficeCode.Equals(Nothing)  Then
						
					Me._UpToICOfficeByOfficeCode = New ICOffice()
					Me._UpToICOfficeByOfficeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByOfficeCode", Me._UpToICOfficeByOfficeCode)
					Me._UpToICOfficeByOfficeCode.Query.Where(Me._UpToICOfficeByOfficeCode.Query.OfficeID = Me.OfficeCode)
					Me._UpToICOfficeByOfficeCode.Query.Load()
				End If

				Return Me._UpToICOfficeByOfficeCode
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByOfficeCode")
				

				If value Is Nothing Then
				
					Me.OfficeCode = Nothing
				
					Me._UpToICOfficeByOfficeCode = Nothing
				Else
				
					Me.OfficeCode = value.OfficeID
					
					Me._UpToICOfficeByOfficeCode = value
					Me.SetPreSave("UpToICOfficeByOfficeCode", Me._UpToICOfficeByOfficeCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountsCollectionByCreateBy"
					coll = Me.ICAccountsCollectionByCreateBy
					Exit Select
				Case "ICAmendmentsRightsManagementCollectionByCreatedBy"
					coll = Me.ICAmendmentsRightsManagementCollectionByCreatedBy
					Exit Select
				Case "ICApprovalGroupManagementCollectionByCreatedBy"
					coll = Me.ICApprovalGroupManagementCollectionByCreatedBy
					Exit Select
				Case "ICApprovalGroupUsersCollectionByUserID"
					coll = Me.ICApprovalGroupUsersCollectionByUserID
					Exit Select
				Case "ICApprovalGroupUsersCollectionByCreatedBy"
					coll = Me.ICApprovalGroupUsersCollectionByCreatedBy
					Exit Select
				Case "ICApprovalGroupUsersCollectionByCreator"
					coll = Me.ICApprovalGroupUsersCollectionByCreator
					Exit Select
				Case "ICApprovalRuleCollectionByCreatedBy"
					coll = Me.ICApprovalRuleCollectionByCreatedBy
					Exit Select
				Case "ICApprovalRuleCollectionByCreator"
					coll = Me.ICApprovalRuleCollectionByCreator
					Exit Select
				Case "ICApprovalRuleConditionsCollectionByCreatedBy"
					coll = Me.ICApprovalRuleConditionsCollectionByCreatedBy
					Exit Select
				Case "ICApprovalRuleConditionsCollectionByCreator"
					coll = Me.ICApprovalRuleConditionsCollectionByCreator
					Exit Select
				Case "ICCollectionAccountsCollectionByCreateBy"
					coll = Me.ICCollectionAccountsCollectionByCreateBy
					Exit Select
				Case "ICDDInstrumentsCollectionByCreatedBy"
					coll = Me.ICDDInstrumentsCollectionByCreatedBy
					Exit Select
				Case "ICDDMasterSeriesCollectionByCreatedBy"
					coll = Me.ICDDMasterSeriesCollectionByCreatedBy
					Exit Select
				Case "ICEmailSettingsCollectionByUserID"
					coll = Me.ICEmailSettingsCollectionByUserID
					Exit Select
				Case "ICEmailSettingsCollectionByCreatedBy"
					coll = Me.ICEmailSettingsCollectionByCreatedBy
					Exit Select
				Case "ICFTPSettingsCollectionByUserID"
					coll = Me.ICFTPSettingsCollectionByUserID
					Exit Select
				Case "ICFTPSettingsCollectionByCreatedBy"
					coll = Me.ICFTPSettingsCollectionByCreatedBy
					Exit Select
				Case "ICInstructionCollectionByClientPrimaryApprovedBy"
					coll = Me.ICInstructionCollectionByClientPrimaryApprovedBy
					Exit Select
				Case "ICInstructionCollectionByClientSecondaryApprovedBy"
					coll = Me.ICInstructionCollectionByClientSecondaryApprovedBy
					Exit Select
				Case "ICInstructionApprovalLogCollectionByUserID"
					coll = Me.ICInstructionApprovalLogCollectionByUserID
					Exit Select
				Case "ICInstrumentsCollectionByCreatedBy"
					coll = Me.ICInstrumentsCollectionByCreatedBy
					Exit Select
				Case "ICLimitForApprovalCollectionByUserID"
					coll = Me.ICLimitForApprovalCollectionByUserID
					Exit Select
				Case "ICLimitsForClearingCollectionByUserID"
					coll = Me.ICLimitsForClearingCollectionByUserID
					Exit Select
				Case "ICMISReportsCollectionByCreatedBy"
					coll = Me.ICMISReportsCollectionByCreatedBy
					Exit Select
				Case "ICMISReportUsersCollectionByUserID"
					coll = Me.ICMISReportUsersCollectionByUserID
					Exit Select
				Case "ICMISReportUsersCollectionByCreatedBy"
					coll = Me.ICMISReportUsersCollectionByCreatedBy
					Exit Select
				Case "ICNotificationManagementCollectionByUserID"
					coll = Me.ICNotificationManagementCollectionByUserID
					Exit Select
				Case "ICNotificationManagementCollectionByCreatedBy"
					coll = Me.ICNotificationManagementCollectionByCreatedBy
					Exit Select
				Case "ICPOInstrumentsCollectionByCreatedBy"
					coll = Me.ICPOInstrumentsCollectionByCreatedBy
					Exit Select
				Case "ICPOMasterSeriesCollectionByCreatedBy"
					coll = Me.ICPOMasterSeriesCollectionByCreatedBy
					Exit Select
				Case "ICSubSetCollectionByCreatedBy"
					coll = Me.ICSubSetCollectionByCreatedBy
					Exit Select
				Case "ICTaggedClientUserForAccountCollectionByUserID"
					coll = Me.ICTaggedClientUserForAccountCollectionByUserID
					Exit Select
				Case "ICUserRolesCollectionByUserID"
					coll = Me.ICUserRolesCollectionByUserID
					Exit Select
				Case "ICUserRolesAccountAndPaymentNatureCollectionByUserID"
					coll = Me.ICUserRolesAccountAndPaymentNatureCollectionByUserID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountsCollectionByCreateBy", GetType(ICAccountsCollection), New ICAccounts()))
			props.Add(new esPropertyDescriptor(Me, "ICAmendmentsRightsManagementCollectionByCreatedBy", GetType(ICAmendmentsRightsManagementCollection), New ICAmendmentsRightsManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupManagementCollectionByCreatedBy", GetType(ICApprovalGroupManagementCollection), New ICApprovalGroupManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupUsersCollectionByUserID", GetType(ICApprovalGroupUsersCollection), New ICApprovalGroupUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupUsersCollectionByCreatedBy", GetType(ICApprovalGroupUsersCollection), New ICApprovalGroupUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupUsersCollectionByCreator", GetType(ICApprovalGroupUsersCollection), New ICApprovalGroupUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleCollectionByCreatedBy", GetType(ICApprovalRuleCollection), New ICApprovalRule()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleCollectionByCreator", GetType(ICApprovalRuleCollection), New ICApprovalRule()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleConditionsCollectionByCreatedBy", GetType(ICApprovalRuleConditionsCollection), New ICApprovalRuleConditions()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleConditionsCollectionByCreator", GetType(ICApprovalRuleConditionsCollection), New ICApprovalRuleConditions()))
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionByCreateBy", GetType(ICCollectionAccountsCollection), New ICCollectionAccounts()))
			props.Add(new esPropertyDescriptor(Me, "ICDDInstrumentsCollectionByCreatedBy", GetType(ICDDInstrumentsCollection), New ICDDInstruments()))
			props.Add(new esPropertyDescriptor(Me, "ICDDMasterSeriesCollectionByCreatedBy", GetType(ICDDMasterSeriesCollection), New ICDDMasterSeries()))
			props.Add(new esPropertyDescriptor(Me, "ICEmailSettingsCollectionByUserID", GetType(ICEmailSettingsCollection), New ICEmailSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICEmailSettingsCollectionByCreatedBy", GetType(ICEmailSettingsCollection), New ICEmailSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICFTPSettingsCollectionByUserID", GetType(ICFTPSettingsCollection), New ICFTPSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICFTPSettingsCollectionByCreatedBy", GetType(ICFTPSettingsCollection), New ICFTPSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByClientPrimaryApprovedBy", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByClientSecondaryApprovedBy", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionApprovalLogCollectionByUserID", GetType(ICInstructionApprovalLogCollection), New ICInstructionApprovalLog()))
			props.Add(new esPropertyDescriptor(Me, "ICInstrumentsCollectionByCreatedBy", GetType(ICInstrumentsCollection), New ICInstruments()))
			props.Add(new esPropertyDescriptor(Me, "ICLimitForApprovalCollectionByUserID", GetType(ICLimitForApprovalCollection), New ICLimitForApproval()))
			props.Add(new esPropertyDescriptor(Me, "ICLimitsForClearingCollectionByUserID", GetType(ICLimitsForClearingCollection), New ICLimitsForClearing()))
			props.Add(new esPropertyDescriptor(Me, "ICMISReportsCollectionByCreatedBy", GetType(ICMISReportsCollection), New ICMISReports()))
			props.Add(new esPropertyDescriptor(Me, "ICMISReportUsersCollectionByUserID", GetType(ICMISReportUsersCollection), New ICMISReportUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICMISReportUsersCollectionByCreatedBy", GetType(ICMISReportUsersCollection), New ICMISReportUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICNotificationManagementCollectionByUserID", GetType(ICNotificationManagementCollection), New ICNotificationManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICNotificationManagementCollectionByCreatedBy", GetType(ICNotificationManagementCollection), New ICNotificationManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICPOInstrumentsCollectionByCreatedBy", GetType(ICPOInstrumentsCollection), New ICPOInstruments()))
			props.Add(new esPropertyDescriptor(Me, "ICPOMasterSeriesCollectionByCreatedBy", GetType(ICPOMasterSeriesCollection), New ICPOMasterSeries()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSetCollectionByCreatedBy", GetType(ICSubSetCollection), New ICSubSet()))
			props.Add(new esPropertyDescriptor(Me, "ICTaggedClientUserForAccountCollectionByUserID", GetType(ICTaggedClientUserForAccountCollection), New ICTaggedClientUserForAccount()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesCollectionByUserID", GetType(ICUserRolesCollection), New ICUserRoles()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesAccountAndPaymentNatureCollectionByUserID", GetType(ICUserRolesAccountAndPaymentNatureCollection), New ICUserRolesAccountAndPaymentNature()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByOfficeCode Is Nothing Then
				Me.OfficeCode = Me._UpToICOfficeByOfficeCode.OfficeID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICUserMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUserMetadata.ColumnNames.UserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserMetadata.PropertyNames.UserID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.UserName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.UserName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.DisplayName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.DisplayName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Password, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.Password
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.ApprovedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsApproved, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.OfficeCode, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserMetadata.PropertyNames.OfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.PasswordExpiryDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.PasswordExpiryDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.CellNo, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.CellNo
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.PhoneNo1, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.PhoneNo1
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.PhoneNo2, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.PhoneNo2
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Email, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.Email
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsICAdmin, 16, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsICAdmin
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.LastActivityDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.LastActivityDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.LastLogInDate, 18, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.LastLogInDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.LastIPAddress, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.LastIPAddress
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Location, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.Location
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.UserType, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.UserType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsBlocked, 22, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsBlocked
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.LastPasswordChangedDate, 23, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.LastPasswordChangedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Department, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.Department
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Is2FARequiredOnLogin, 25, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.Is2FARequiredOnLogin
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Is2FARequiredOnApproval, 26, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.Is2FARequiredOnApproval
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForLogin, 27, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForLogin
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForLogin, 28, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForLogin
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.EmployeeCode, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.EmployeeCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsTwoFAVIASMSAllowForApproval, 30, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsTwoFAVIASMSAllowForApproval
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsTwoFAVIAEmailAllowForApproval, 31, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsTwoFAVIAEmailAllowForApproval
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.AuthenticatedVia, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.AuthenticatedVia
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.Creater, 33, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.CreationDate, 34, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsNotificationSent, 35, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsNotificationSent
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.IsLoggedIn, 36, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserMetadata.PropertyNames.IsLoggedIn
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserMetadata.ColumnNames.ClearingBranchCode, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserMetadata.PropertyNames.ClearingBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUserMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UserID As String = "UserID"
			 Public Const UserName As String = "UserName"
			 Public Const DisplayName As String = "DisplayName"
			 Public Const Password As String = "Password"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const PasswordExpiryDate As String = "PasswordExpiryDate"
			 Public Const CellNo As String = "CellNo"
			 Public Const PhoneNo1 As String = "PhoneNo1"
			 Public Const PhoneNo2 As String = "PhoneNo2"
			 Public Const Email As String = "Email"
			 Public Const IsICAdmin As String = "IsICAdmin"
			 Public Const LastActivityDate As String = "LastActivityDate"
			 Public Const LastLogInDate As String = "LastLogInDate"
			 Public Const LastIPAddress As String = "LastIPAddress"
			 Public Const Location As String = "Location"
			 Public Const UserType As String = "UserType"
			 Public Const IsBlocked As String = "IsBlocked"
			 Public Const LastPasswordChangedDate As String = "LastPasswordChangedDate"
			 Public Const Department As String = "Department"
			 Public Const Is2FARequiredOnLogin As String = "Is2FARequiredOnLogin"
			 Public Const Is2FARequiredOnApproval As String = "Is2FARequiredOnApproval"
			 Public Const IsTwoFAVIASMSAllowForLogin As String = "IsTwoFAVIASMSAllowForLogin"
			 Public Const IsTwoFAVIAEmailAllowForLogin As String = "IsTwoFAVIAEmailAllowForLogin"
			 Public Const EmployeeCode As String = "EmployeeCode"
			 Public Const IsTwoFAVIASMSAllowForApproval As String = "IsTwoFAVIASMSAllowForApproval"
			 Public Const IsTwoFAVIAEmailAllowForApproval As String = "IsTwoFAVIAEmailAllowForApproval"
			 Public Const AuthenticatedVia As String = "AuthenticatedVia"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsNotificationSent As String = "IsNotificationSent"
			 Public Const IsLoggedIn As String = "IsLoggedIn"
			 Public Const ClearingBranchCode As String = "ClearingBranchCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UserID As String = "UserID"
			 Public Const UserName As String = "UserName"
			 Public Const DisplayName As String = "DisplayName"
			 Public Const Password As String = "Password"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const PasswordExpiryDate As String = "PasswordExpiryDate"
			 Public Const CellNo As String = "CellNo"
			 Public Const PhoneNo1 As String = "PhoneNo1"
			 Public Const PhoneNo2 As String = "PhoneNo2"
			 Public Const Email As String = "Email"
			 Public Const IsICAdmin As String = "IsICAdmin"
			 Public Const LastActivityDate As String = "LastActivityDate"
			 Public Const LastLogInDate As String = "LastLogInDate"
			 Public Const LastIPAddress As String = "LastIPAddress"
			 Public Const Location As String = "Location"
			 Public Const UserType As String = "UserType"
			 Public Const IsBlocked As String = "IsBlocked"
			 Public Const LastPasswordChangedDate As String = "LastPasswordChangedDate"
			 Public Const Department As String = "Department"
			 Public Const Is2FARequiredOnLogin As String = "Is2FARequiredOnLogin"
			 Public Const Is2FARequiredOnApproval As String = "Is2FARequiredOnApproval"
			 Public Const IsTwoFAVIASMSAllowForLogin As String = "IsTwoFAVIASMSAllowForLogin"
			 Public Const IsTwoFAVIAEmailAllowForLogin As String = "IsTwoFAVIAEmailAllowForLogin"
			 Public Const EmployeeCode As String = "EmployeeCode"
			 Public Const IsTwoFAVIASMSAllowForApproval As String = "IsTwoFAVIASMSAllowForApproval"
			 Public Const IsTwoFAVIAEmailAllowForApproval As String = "IsTwoFAVIAEmailAllowForApproval"
			 Public Const AuthenticatedVia As String = "AuthenticatedVia"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsNotificationSent As String = "IsNotificationSent"
			 Public Const IsLoggedIn As String = "IsLoggedIn"
			 Public Const ClearingBranchCode As String = "ClearingBranchCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUserMetadata)
			
				If ICUserMetadata.mapDelegates Is Nothing Then
					ICUserMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUserMetadata._meta Is Nothing Then
					ICUserMetadata._meta = New ICUserMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisplayName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Password", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("OfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PasswordExpiryDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CellNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PhoneNo1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PhoneNo2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsICAdmin", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("LastActivityDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("LastLogInDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("LastIPAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Location", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UserType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsBlocked", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("LastPasswordChangedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Department", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Is2FARequiredOnLogin", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Is2FARequiredOnApproval", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsTwoFAVIASMSAllowForLogin", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsTwoFAVIAEmailAllowForLogin", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("EmployeeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsTwoFAVIASMSAllowForApproval", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsTwoFAVIAEmailAllowForApproval", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("AuthenticatedVia", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsNotificationSent", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsLoggedIn", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ClearingBranchCode", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_User"
				meta.Destination = "IC_User"
				
				meta.spInsert = "proc_IC_UserInsert"
				meta.spUpdate = "proc_IC_UserUpdate"
				meta.spDelete = "proc_IC_UserDelete"
				meta.spLoadAll = "proc_IC_UserLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UserLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUserMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
