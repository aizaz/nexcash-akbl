
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 3/4/2014 6:02:17 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_SweepActionInstructions' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICSweepActionInstructions))> _
	<XmlType("ICSweepActionInstructions")> _	
	Partial Public Class ICSweepActionInstructions 
		Inherits esICSweepActionInstructions
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICSweepActionInstructions()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal sweepActionInstructionID As System.Int32)
			Dim obj As New ICSweepActionInstructions()
			obj.SweepActionInstructionID = sweepActionInstructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal sweepActionInstructionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICSweepActionInstructions()
			obj.SweepActionInstructionID = sweepActionInstructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICSweepActionInstructionsCollection")> _
	Partial Public Class ICSweepActionInstructionsCollection
		Inherits esICSweepActionInstructionsCollection
		Implements IEnumerable(Of ICSweepActionInstructions)
	
		Public Function FindByPrimaryKey(ByVal sweepActionInstructionID As System.Int32) As ICSweepActionInstructions
			Return MyBase.SingleOrDefault(Function(e) e.SweepActionInstructionID.HasValue AndAlso e.SweepActionInstructionID.Value = sweepActionInstructionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICSweepActionInstructions))> _
		Public Class ICSweepActionInstructionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICSweepActionInstructionsCollection)
			
			Public Shared Widening Operator CType(packet As ICSweepActionInstructionsCollectionWCFPacket) As ICSweepActionInstructionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICSweepActionInstructionsCollection) As ICSweepActionInstructionsCollectionWCFPacket
				Return New ICSweepActionInstructionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICSweepActionInstructionsQuery 
		Inherits esICSweepActionInstructionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICSweepActionInstructionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICSweepActionInstructionsQuery) As String
			Return ICSweepActionInstructionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICSweepActionInstructionsQuery
			Return DirectCast(ICSweepActionInstructionsQuery.SerializeHelper.FromXml(query, GetType(ICSweepActionInstructionsQuery)), ICSweepActionInstructionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICSweepActionInstructions
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal sweepActionInstructionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(sweepActionInstructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(sweepActionInstructionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal sweepActionInstructionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(sweepActionInstructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(sweepActionInstructionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal sweepActionInstructionID As System.Int32) As Boolean
		
			Dim query As New ICSweepActionInstructionsQuery()
			query.Where(query.SweepActionInstructionID = sweepActionInstructionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal sweepActionInstructionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SweepActionInstructionID", sweepActionInstructionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionInstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionInstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionID, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionID, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.FromAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountNo, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.FromAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.FromAccountBrCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountBrCode As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountBrCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountBrCode, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.FromAccountBrCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.FromAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountCurrency, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.FromAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.ToAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountNo, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ToAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.ToAccountBrCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountBrCode As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountBrCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountBrCode, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ToAccountBrCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.ToAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountCurrency, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ToAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionCreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionCreationDate As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreationDate)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreationDate, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionCreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionCreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionCreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreatedBy, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionCreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.InstructionAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICSweepActionInstructionsMetadata.ColumnNames.InstructionAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICSweepActionInstructionsMetadata.ColumnNames.InstructionAmount, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.InstructionAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.FromAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountType, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.FromAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.ToAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountType, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ToAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.IsProcessed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsProcessed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsProcessed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsProcessed, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsProcessed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.LastStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastStatus As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.LastStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.LastStatus, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.LastStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionFromDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionFromDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFromDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFromDate, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionFromDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionToDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionToDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionToDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionToDate, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionToDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionFrequency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionFrequency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFrequency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFrequency, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionFrequency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionTitle As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTitle, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTime, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionDayType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionDayType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionDayType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionDayType, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionDayType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.TotalInstructions
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TotalInstructions As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.TotalInstructions)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.TotalInstructions, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.TotalInstructions)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.CurrentStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CurrentStatus As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.CurrentStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionInstructionsMetadata.ColumnNames.CurrentStatus, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.CurrentStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.IsPaid
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPaid As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsPaid)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionInstructionsMetadata.ColumnNames.IsPaid, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsPaid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepInstructionActivatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepInstructionActivatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionActivatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionActivatedBy, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionActivatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepInstructionApproveddBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepInstructionApproveddBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionApproveddBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionApproveddBy, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionApproveddBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.GroupCode, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionInstructionsMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.ValueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ValueDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.ValueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.ValueDate, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ValueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionInstructionActivationDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionInstructionActivationDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionActivationDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionActivationDateTime, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionActivationDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionInstructionApprovalDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionInstructionApprovalDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionApprovalDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionApprovalDateTime, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionApprovalDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepActionInstructions.SweepActionInstructionPaidDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionInstructionPaidDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionPaidDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionPaidDateTime, value) Then
					OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionPaidDateTime)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SweepActionInstructionID"
							Me.str().SweepActionInstructionID = CType(value, string)
												
						Case "SweepActionID"
							Me.str().SweepActionID = CType(value, string)
												
						Case "FromAccountNo"
							Me.str().FromAccountNo = CType(value, string)
												
						Case "FromAccountBrCode"
							Me.str().FromAccountBrCode = CType(value, string)
												
						Case "FromAccountCurrency"
							Me.str().FromAccountCurrency = CType(value, string)
												
						Case "ToAccountNo"
							Me.str().ToAccountNo = CType(value, string)
												
						Case "ToAccountBrCode"
							Me.str().ToAccountBrCode = CType(value, string)
												
						Case "ToAccountCurrency"
							Me.str().ToAccountCurrency = CType(value, string)
												
						Case "SweepActionCreationDate"
							Me.str().SweepActionCreationDate = CType(value, string)
												
						Case "SweepActionCreatedBy"
							Me.str().SweepActionCreatedBy = CType(value, string)
												
						Case "InstructionAmount"
							Me.str().InstructionAmount = CType(value, string)
												
						Case "FromAccountType"
							Me.str().FromAccountType = CType(value, string)
												
						Case "ToAccountType"
							Me.str().ToAccountType = CType(value, string)
												
						Case "IsProcessed"
							Me.str().IsProcessed = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "LastStatus"
							Me.str().LastStatus = CType(value, string)
												
						Case "SweepActionFromDate"
							Me.str().SweepActionFromDate = CType(value, string)
												
						Case "SweepActionToDate"
							Me.str().SweepActionToDate = CType(value, string)
												
						Case "SweepActionFrequency"
							Me.str().SweepActionFrequency = CType(value, string)
												
						Case "SweepActionTitle"
							Me.str().SweepActionTitle = CType(value, string)
												
						Case "SweepActionTime"
							Me.str().SweepActionTime = CType(value, string)
												
						Case "SweepActionDayType"
							Me.str().SweepActionDayType = CType(value, string)
												
						Case "TotalInstructions"
							Me.str().TotalInstructions = CType(value, string)
												
						Case "CurrentStatus"
							Me.str().CurrentStatus = CType(value, string)
												
						Case "IsPaid"
							Me.str().IsPaid = CType(value, string)
												
						Case "SweepInstructionActivatedBy"
							Me.str().SweepInstructionActivatedBy = CType(value, string)
												
						Case "SweepInstructionApproveddBy"
							Me.str().SweepInstructionApproveddBy = CType(value, string)
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ValueDate"
							Me.str().ValueDate = CType(value, string)
												
						Case "SweepActionInstructionActivationDateTime"
							Me.str().SweepActionInstructionActivationDateTime = CType(value, string)
												
						Case "SweepActionInstructionApprovalDateTime"
							Me.str().SweepActionInstructionApprovalDateTime = CType(value, string)
												
						Case "SweepActionInstructionPaidDateTime"
							Me.str().SweepActionInstructionPaidDateTime = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SweepActionInstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepActionInstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionID)
							End If
						
						Case "SweepActionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepActionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionID)
							End If
						
						Case "SweepActionCreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepActionCreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionCreatedBy)
							End If
						
						Case "InstructionAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.InstructionAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.InstructionAmount)
							End If
						
						Case "IsProcessed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsProcessed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsProcessed)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsApproved)
							End If
						
						Case "SweepActionFromDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionFromDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionFromDate)
							End If
						
						Case "SweepActionToDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionToDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionToDate)
							End If
						
						Case "SweepActionTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionTime)
							End If
						
						Case "TotalInstructions"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TotalInstructions = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.TotalInstructions)
							End If
						
						Case "IsPaid"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPaid = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.IsPaid)
							End If
						
						Case "SweepInstructionActivatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepInstructionActivatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionActivatedBy)
							End If
						
						Case "SweepInstructionApproveddBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepInstructionApproveddBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionApproveddBy)
							End If
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.GroupCode)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "ValueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ValueDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.ValueDate)
							End If
						
						Case "SweepActionInstructionActivationDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionInstructionActivationDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionActivationDateTime)
							End If
						
						Case "SweepActionInstructionApprovalDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionInstructionApprovalDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionApprovalDateTime)
							End If
						
						Case "SweepActionInstructionPaidDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionInstructionPaidDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionPaidDateTime)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICSweepActionInstructions)
				Me.entity = entity
			End Sub				
		
	
			Public Property SweepActionInstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepActionInstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionInstructionID = Nothing
					Else
						entity.SweepActionInstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepActionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionID = Nothing
					Else
						entity.SweepActionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNo = Nothing
					Else
						entity.FromAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountBrCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountBrCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountBrCode = Nothing
					Else
						entity.FromAccountBrCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountCurrency = Nothing
					Else
						entity.FromAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNo = Nothing
					Else
						entity.ToAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountBrCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountBrCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountBrCode = Nothing
					Else
						entity.ToAccountBrCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountCurrency = Nothing
					Else
						entity.ToAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionCreationDate As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionCreationDate
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionCreationDate = Nothing
					Else
						entity.SweepActionCreationDate = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionCreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepActionCreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionCreatedBy = Nothing
					Else
						entity.SweepActionCreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.InstructionAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionAmount = Nothing
					Else
						entity.InstructionAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountType As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountType = Nothing
					Else
						entity.FromAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountType = Nothing
					Else
						entity.ToAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsProcessed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsProcessed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsProcessed = Nothing
					Else
						entity.IsProcessed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastStatus As System.String 
				Get
					Dim data_ As System.String = entity.LastStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastStatus = Nothing
					Else
						entity.LastStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionFromDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionFromDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionFromDate = Nothing
					Else
						entity.SweepActionFromDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionToDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionToDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionToDate = Nothing
					Else
						entity.SweepActionToDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionFrequency As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionFrequency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionFrequency = Nothing
					Else
						entity.SweepActionFrequency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionTitle As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionTitle = Nothing
					Else
						entity.SweepActionTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionTime = Nothing
					Else
						entity.SweepActionTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionDayType As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionDayType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionDayType = Nothing
					Else
						entity.SweepActionDayType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TotalInstructions As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TotalInstructions
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TotalInstructions = Nothing
					Else
						entity.TotalInstructions = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CurrentStatus As System.String 
				Get
					Dim data_ As System.String = entity.CurrentStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CurrentStatus = Nothing
					Else
						entity.CurrentStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPaid As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPaid
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPaid = Nothing
					Else
						entity.IsPaid = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepInstructionActivatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepInstructionActivatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepInstructionActivatedBy = Nothing
					Else
						entity.SweepInstructionActivatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepInstructionApproveddBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepInstructionApproveddBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepInstructionApproveddBy = Nothing
					Else
						entity.SweepInstructionApproveddBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ValueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ValueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ValueDate = Nothing
					Else
						entity.ValueDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionInstructionActivationDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionInstructionActivationDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionInstructionActivationDateTime = Nothing
					Else
						entity.SweepActionInstructionActivationDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionInstructionApprovalDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionInstructionApprovalDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionInstructionApprovalDateTime = Nothing
					Else
						entity.SweepActionInstructionApprovalDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionInstructionPaidDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionInstructionPaidDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionInstructionPaidDateTime = Nothing
					Else
						entity.SweepActionInstructionPaidDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICSweepActionInstructions
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSweepActionInstructionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICSweepActionInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSweepActionInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICSweepActionInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICSweepActionInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICSweepActionInstructionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICSweepActionInstructionsCollection
		Inherits esEntityCollection(Of ICSweepActionInstructions)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSweepActionInstructionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICSweepActionInstructionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICSweepActionInstructionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSweepActionInstructionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICSweepActionInstructionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICSweepActionInstructionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICSweepActionInstructionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICSweepActionInstructionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICSweepActionInstructionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICSweepActionInstructionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICSweepActionInstructionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SweepActionInstructionID" 
					Return Me.SweepActionInstructionID
				Case "SweepActionID" 
					Return Me.SweepActionID
				Case "FromAccountNo" 
					Return Me.FromAccountNo
				Case "FromAccountBrCode" 
					Return Me.FromAccountBrCode
				Case "FromAccountCurrency" 
					Return Me.FromAccountCurrency
				Case "ToAccountNo" 
					Return Me.ToAccountNo
				Case "ToAccountBrCode" 
					Return Me.ToAccountBrCode
				Case "ToAccountCurrency" 
					Return Me.ToAccountCurrency
				Case "SweepActionCreationDate" 
					Return Me.SweepActionCreationDate
				Case "SweepActionCreatedBy" 
					Return Me.SweepActionCreatedBy
				Case "InstructionAmount" 
					Return Me.InstructionAmount
				Case "FromAccountType" 
					Return Me.FromAccountType
				Case "ToAccountType" 
					Return Me.ToAccountType
				Case "IsProcessed" 
					Return Me.IsProcessed
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApproved" 
					Return Me.IsApproved
				Case "LastStatus" 
					Return Me.LastStatus
				Case "SweepActionFromDate" 
					Return Me.SweepActionFromDate
				Case "SweepActionToDate" 
					Return Me.SweepActionToDate
				Case "SweepActionFrequency" 
					Return Me.SweepActionFrequency
				Case "SweepActionTitle" 
					Return Me.SweepActionTitle
				Case "SweepActionTime" 
					Return Me.SweepActionTime
				Case "SweepActionDayType" 
					Return Me.SweepActionDayType
				Case "TotalInstructions" 
					Return Me.TotalInstructions
				Case "CurrentStatus" 
					Return Me.CurrentStatus
				Case "IsPaid" 
					Return Me.IsPaid
				Case "SweepInstructionActivatedBy" 
					Return Me.SweepInstructionActivatedBy
				Case "SweepInstructionApproveddBy" 
					Return Me.SweepInstructionApproveddBy
				Case "GroupCode" 
					Return Me.GroupCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ValueDate" 
					Return Me.ValueDate
				Case "SweepActionInstructionActivationDateTime" 
					Return Me.SweepActionInstructionActivationDateTime
				Case "SweepActionInstructionApprovalDateTime" 
					Return Me.SweepActionInstructionApprovalDateTime
				Case "SweepActionInstructionPaidDateTime" 
					Return Me.SweepActionInstructionPaidDateTime
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SweepActionInstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.FromAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountBrCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.FromAccountBrCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.FromAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.ToAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountBrCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.ToAccountBrCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.ToAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionCreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreationDate, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionCreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.InstructionAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.FromAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.ToAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsProcessed As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.IsProcessed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property LastStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.LastStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionFromDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFromDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionToDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionToDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionFrequency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFrequency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionDayType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionDayType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TotalInstructions As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.TotalInstructions, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CurrentStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.CurrentStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsPaid As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.IsPaid, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property SweepInstructionActivatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionActivatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SweepInstructionApproveddBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionApproveddBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ValueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.ValueDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionInstructionActivationDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionActivationDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionInstructionApprovalDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionApprovalDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionInstructionPaidDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionPaidDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICSweepActionInstructions 
		Inherits esICSweepActionInstructions
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICSweepActionInstructionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountNo, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.FromAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountBrCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.FromAccountBrCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountCurrency, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.FromAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountNo, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.ToAccountNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountBrCode, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.ToAccountBrCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountCurrency, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.ToAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreationDate, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionCreationDate
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionCreatedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionCreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.InstructionAmount, 10, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.InstructionAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.FromAccountType, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.FromAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.ToAccountType, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.ToAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.IsProcessed, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.IsProcessed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.IsActive, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.IsApproved, 15, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.LastStatus, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.LastStatus
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFromDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionFromDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionToDate, 18, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionToDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionFrequency, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionFrequency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTitle, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionTitle
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionTime, 21, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionDayType, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionDayType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.TotalInstructions, 23, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.TotalInstructions
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.CurrentStatus, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.CurrentStatus
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.IsPaid, 25, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.IsPaid
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionActivatedBy, 26, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionActivatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepInstructionApproveddBy, 27, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepInstructionApproveddBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.GroupCode, 28, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.GroupCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.CompanyCode, 29, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.ValueDate, 30, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.ValueDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionActivationDateTime, 31, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionActivationDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionApprovalDateTime, 32, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionApprovalDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionInstructionsMetadata.ColumnNames.SweepActionInstructionPaidDateTime, 33, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionInstructionsMetadata.PropertyNames.SweepActionInstructionPaidDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICSweepActionInstructionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SweepActionInstructionID As String = "SweepActionInstructionID"
			 Public Const SweepActionID As String = "SweepActionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBrCode As String = "FromAccountBrCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBrCode As String = "ToAccountBrCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const SweepActionCreationDate As String = "SweepActionCreationDate"
			 Public Const SweepActionCreatedBy As String = "SweepActionCreatedBy"
			 Public Const InstructionAmount As String = "InstructionAmount"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const SweepActionFromDate As String = "SweepActionFromDate"
			 Public Const SweepActionToDate As String = "SweepActionToDate"
			 Public Const SweepActionFrequency As String = "SweepActionFrequency"
			 Public Const SweepActionTitle As String = "SweepActionTitle"
			 Public Const SweepActionTime As String = "SweepActionTime"
			 Public Const SweepActionDayType As String = "SweepActionDayType"
			 Public Const TotalInstructions As String = "TotalInstructions"
			 Public Const CurrentStatus As String = "CurrentStatus"
			 Public Const IsPaid As String = "IsPaid"
			 Public Const SweepInstructionActivatedBy As String = "SweepInstructionActivatedBy"
			 Public Const SweepInstructionApproveddBy As String = "SweepInstructionApproveddBy"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ValueDate As String = "ValueDate"
			 Public Const SweepActionInstructionActivationDateTime As String = "SweepActionInstructionActivationDateTime"
			 Public Const SweepActionInstructionApprovalDateTime As String = "SweepActionInstructionApprovalDateTime"
			 Public Const SweepActionInstructionPaidDateTime As String = "SweepActionInstructionPaidDateTime"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SweepActionInstructionID As String = "SweepActionInstructionID"
			 Public Const SweepActionID As String = "SweepActionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBrCode As String = "FromAccountBrCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBrCode As String = "ToAccountBrCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const SweepActionCreationDate As String = "SweepActionCreationDate"
			 Public Const SweepActionCreatedBy As String = "SweepActionCreatedBy"
			 Public Const InstructionAmount As String = "InstructionAmount"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const SweepActionFromDate As String = "SweepActionFromDate"
			 Public Const SweepActionToDate As String = "SweepActionToDate"
			 Public Const SweepActionFrequency As String = "SweepActionFrequency"
			 Public Const SweepActionTitle As String = "SweepActionTitle"
			 Public Const SweepActionTime As String = "SweepActionTime"
			 Public Const SweepActionDayType As String = "SweepActionDayType"
			 Public Const TotalInstructions As String = "TotalInstructions"
			 Public Const CurrentStatus As String = "CurrentStatus"
			 Public Const IsPaid As String = "IsPaid"
			 Public Const SweepInstructionActivatedBy As String = "SweepInstructionActivatedBy"
			 Public Const SweepInstructionApproveddBy As String = "SweepInstructionApproveddBy"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ValueDate As String = "ValueDate"
			 Public Const SweepActionInstructionActivationDateTime As String = "SweepActionInstructionActivationDateTime"
			 Public Const SweepActionInstructionApprovalDateTime As String = "SweepActionInstructionApprovalDateTime"
			 Public Const SweepActionInstructionPaidDateTime As String = "SweepActionInstructionPaidDateTime"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICSweepActionInstructionsMetadata)
			
				If ICSweepActionInstructionsMetadata.mapDelegates Is Nothing Then
					ICSweepActionInstructionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICSweepActionInstructionsMetadata._meta Is Nothing Then
					ICSweepActionInstructionsMetadata._meta = New ICSweepActionInstructionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SweepActionInstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SweepActionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FromAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountBrCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountBrCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionCreationDate", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionCreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("FromAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsProcessed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("LastStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionFromDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionToDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionFrequency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionDayType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TotalInstructions", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CurrentStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsPaid", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("SweepInstructionActivatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SweepInstructionApproveddBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ValueDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionInstructionActivationDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionInstructionApprovalDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionInstructionPaidDateTime", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_SweepActionInstructions"
				meta.Destination = "IC_SweepActionInstructions"
				
				meta.spInsert = "proc_IC_SweepActionInstructionsInsert"
				meta.spUpdate = "proc_IC_SweepActionInstructionsUpdate"
				meta.spDelete = "proc_IC_SweepActionInstructionsDelete"
				meta.spLoadAll = "proc_IC_SweepActionInstructionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_SweepActionInstructionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICSweepActionInstructionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
