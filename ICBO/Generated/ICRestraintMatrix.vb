
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 07-Apr-2014 12:08:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_RestraintMatrix' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICRestraintMatrix))> _
	<XmlType("ICRestraintMatrix")> _	
	Partial Public Class ICRestraintMatrix 
		Inherits esICRestraintMatrix
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICRestraintMatrix()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal restraintCode As System.String)
			Dim obj As New ICRestraintMatrix()
			obj.RestraintCode = restraintCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal restraintCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICRestraintMatrix()
			obj.RestraintCode = restraintCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICRestraintMatrixCollection")> _
	Partial Public Class ICRestraintMatrixCollection
		Inherits esICRestraintMatrixCollection
		Implements IEnumerable(Of ICRestraintMatrix)
	
		Public Function FindByPrimaryKey(ByVal restraintCode As System.String) As ICRestraintMatrix
			Return MyBase.SingleOrDefault(Function(e) e.RestraintCode = restraintCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICRestraintMatrix))> _
		Public Class ICRestraintMatrixCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICRestraintMatrixCollection)
			
			Public Shared Widening Operator CType(packet As ICRestraintMatrixCollectionWCFPacket) As ICRestraintMatrixCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICRestraintMatrixCollection) As ICRestraintMatrixCollectionWCFPacket
				Return New ICRestraintMatrixCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICRestraintMatrixQuery 
		Inherits esICRestraintMatrixQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICRestraintMatrixQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICRestraintMatrixQuery) As String
			Return ICRestraintMatrixQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICRestraintMatrixQuery
			Return DirectCast(ICRestraintMatrixQuery.SerializeHelper.FromXml(query, GetType(ICRestraintMatrixQuery)), ICRestraintMatrixQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICRestraintMatrix
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal restraintCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(restraintCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(restraintCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal restraintCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(restraintCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(restraintCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal restraintCode As System.String) As Boolean
		
			Dim query As New ICRestraintMatrixQuery()
			query.Where(query.RestraintCode = restraintCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal restraintCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("RestraintCode", restraintCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_RestraintMatrix.RestraintCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RestraintCode As System.String
			Get
				Return MyBase.GetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintCode, value) Then
					OnPropertyChanged(ICRestraintMatrixMetadata.PropertyNames.RestraintCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RestraintMatrix.RestraintType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RestraintType As System.String
			Get
				Return MyBase.GetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintType, value) Then
					OnPropertyChanged(ICRestraintMatrixMetadata.PropertyNames.RestraintType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RestraintMatrix.RestraintName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RestraintName As System.String
			Get
				Return MyBase.GetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintName, value) Then
					OnPropertyChanged(ICRestraintMatrixMetadata.PropertyNames.RestraintName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RestraintMatrix.RestraintMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RestraintMessage As System.String
			Get
				Return MyBase.GetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRestraintMatrixMetadata.ColumnNames.RestraintMessage, value) Then
					OnPropertyChanged(ICRestraintMatrixMetadata.PropertyNames.RestraintMessage)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RestraintCode"
							Me.str().RestraintCode = CType(value, string)
												
						Case "RestraintType"
							Me.str().RestraintType = CType(value, string)
												
						Case "RestraintName"
							Me.str().RestraintName = CType(value, string)
												
						Case "RestraintMessage"
							Me.str().RestraintMessage = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICRestraintMatrix)
				Me.entity = entity
			End Sub				
		
	
			Public Property RestraintCode As System.String 
				Get
					Dim data_ As System.String = entity.RestraintCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RestraintCode = Nothing
					Else
						entity.RestraintCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RestraintType As System.String 
				Get
					Dim data_ As System.String = entity.RestraintType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RestraintType = Nothing
					Else
						entity.RestraintType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RestraintName As System.String 
				Get
					Dim data_ As System.String = entity.RestraintName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RestraintName = Nothing
					Else
						entity.RestraintName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RestraintMessage As System.String 
				Get
					Dim data_ As System.String = entity.RestraintMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RestraintMessage = Nothing
					Else
						entity.RestraintMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICRestraintMatrix
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRestraintMatrixMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICRestraintMatrixQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRestraintMatrixQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICRestraintMatrixQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICRestraintMatrixQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICRestraintMatrixQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICRestraintMatrixCollection
		Inherits esEntityCollection(Of ICRestraintMatrix)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRestraintMatrixMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICRestraintMatrixCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICRestraintMatrixQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRestraintMatrixQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICRestraintMatrixQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICRestraintMatrixQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICRestraintMatrixQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICRestraintMatrixQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICRestraintMatrixQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICRestraintMatrixQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICRestraintMatrixMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RestraintCode" 
					Return Me.RestraintCode
				Case "RestraintType" 
					Return Me.RestraintType
				Case "RestraintName" 
					Return Me.RestraintName
				Case "RestraintMessage" 
					Return Me.RestraintMessage
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RestraintCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICRestraintMatrixMetadata.ColumnNames.RestraintCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RestraintType As esQueryItem
			Get
				Return New esQueryItem(Me, ICRestraintMatrixMetadata.ColumnNames.RestraintType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RestraintName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRestraintMatrixMetadata.ColumnNames.RestraintName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RestraintMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICRestraintMatrixMetadata.ColumnNames.RestraintMessage, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICRestraintMatrix 
		Inherits esICRestraintMatrix
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICRestraintMatrixMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICRestraintMatrixMetadata.ColumnNames.RestraintCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRestraintMatrixMetadata.PropertyNames.RestraintCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRestraintMatrixMetadata.ColumnNames.RestraintType, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRestraintMatrixMetadata.PropertyNames.RestraintType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRestraintMatrixMetadata.ColumnNames.RestraintName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRestraintMatrixMetadata.PropertyNames.RestraintName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRestraintMatrixMetadata.ColumnNames.RestraintMessage, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRestraintMatrixMetadata.PropertyNames.RestraintMessage
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICRestraintMatrixMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RestraintCode As String = "RestraintCode"
			 Public Const RestraintType As String = "RestraintType"
			 Public Const RestraintName As String = "RestraintName"
			 Public Const RestraintMessage As String = "RestraintMessage"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RestraintCode As String = "RestraintCode"
			 Public Const RestraintType As String = "RestraintType"
			 Public Const RestraintName As String = "RestraintName"
			 Public Const RestraintMessage As String = "RestraintMessage"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICRestraintMatrixMetadata)
			
				If ICRestraintMatrixMetadata.mapDelegates Is Nothing Then
					ICRestraintMatrixMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICRestraintMatrixMetadata._meta Is Nothing Then
					ICRestraintMatrixMetadata._meta = New ICRestraintMatrixMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RestraintCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RestraintType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RestraintName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RestraintMessage", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_RestraintMatrix"
				meta.Destination = "IC_RestraintMatrix"
				
				meta.spInsert = "proc_IC_RestraintMatrixInsert"
				meta.spUpdate = "proc_IC_RestraintMatrixUpdate"
				meta.spDelete = "proc_IC_RestraintMatrixDelete"
				meta.spLoadAll = "proc_IC_RestraintMatrixLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_RestraintMatrixLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICRestraintMatrixMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
