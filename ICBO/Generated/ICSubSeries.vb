
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_SubSeries' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICSubSeries))> _
	<XmlType("ICSubSeries")> _	
	Partial Public Class ICSubSeries 
		Inherits esICSubSeries
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICSubSeries()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal subSeriesID As System.Int32)
			Dim obj As New ICSubSeries()
			obj.SubSeriesID = subSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal subSeriesID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICSubSeries()
			obj.SubSeriesID = subSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICSubSeriesCollection")> _
	Partial Public Class ICSubSeriesCollection
		Inherits esICSubSeriesCollection
		Implements IEnumerable(Of ICSubSeries)
	
		Public Function FindByPrimaryKey(ByVal subSeriesID As System.Int32) As ICSubSeries
			Return MyBase.SingleOrDefault(Function(e) e.SubSeriesID.HasValue AndAlso e.SubSeriesID.Value = subSeriesID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICSubSeries))> _
		Public Class ICSubSeriesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICSubSeriesCollection)
			
			Public Shared Widening Operator CType(packet As ICSubSeriesCollectionWCFPacket) As ICSubSeriesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICSubSeriesCollection) As ICSubSeriesCollectionWCFPacket
				Return New ICSubSeriesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICSubSeriesQuery 
		Inherits esICSubSeriesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICSubSeriesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICSubSeriesQuery) As String
			Return ICSubSeriesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICSubSeriesQuery
			Return DirectCast(ICSubSeriesQuery.SerializeHelper.FromXml(query, GetType(ICSubSeriesQuery)), ICSubSeriesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICSubSeries
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal subSeriesID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(subSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(subSeriesID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal subSeriesID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(subSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(subSeriesID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal subSeriesID As System.Int32) As Boolean
		
			Dim query As New ICSubSeriesQuery()
			query.Where(query.SubSeriesID = subSeriesID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal subSeriesID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SubSeriesID", subSeriesID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_SubSeries.SubSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.SubSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.SubSeriesID, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.SubSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.MasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.MasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.MasterSeriesID, value) Then
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
					Me.OnPropertyChanged("UpToICMasterSeriesByMasterSeriesID")
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.MasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.Series
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Series As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICSubSeriesMetadata.ColumnNames.Series)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICSubSeriesMetadata.ColumnNames.Series, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Series)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.OfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.OfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.OfficeCode, value) Then
					Me._UpToICOfficeByOfficeCode = Nothing
					Me.OnPropertyChanged("UpToICOfficeByOfficeCode")
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.OfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSubSeriesMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSubSeriesMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.IsApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSubSeriesMetadata.ColumnNames.IsApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSubSeriesMetadata.ColumnNames.IsApprove, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.IsApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSubSeriesMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSubSeriesMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.Approvedby
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Approvedby As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.Approvedby)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.Approvedby, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Approvedby)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSubSeriesMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSubSeriesMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.PrintSuccessFully
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintSuccessFully As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSubSeriesMetadata.ColumnNames.PrintSuccessFully)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSubSeriesMetadata.ColumnNames.PrintSuccessFully, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.PrintSuccessFully)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSeriesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSeriesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSeries.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSubSeriesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSubSeriesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByOfficeCode As ICOffice
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SubSeriesID"
							Me.str().SubSeriesID = CType(value, string)
												
						Case "MasterSeriesID"
							Me.str().MasterSeriesID = CType(value, string)
												
						Case "Series"
							Me.str().Series = CType(value, string)
												
						Case "OfficeCode"
							Me.str().OfficeCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApprove"
							Me.str().IsApprove = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Approvedby"
							Me.str().Approvedby = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "PrintSuccessFully"
							Me.str().PrintSuccessFully = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SubSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SubSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.SubSeriesID)
							End If
						
						Case "MasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.MasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.MasterSeriesID)
							End If
						
						Case "Series"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Series = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Series)
							End If
						
						Case "OfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.OfficeCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.IsApprove)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Approvedby"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Approvedby = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Approvedby)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "PrintSuccessFully"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.PrintSuccessFully = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.PrintSuccessFully)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSubSeriesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICSubSeries)
				Me.entity = entity
			End Sub				
		
	
			Public Property SubSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SubSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubSeriesID = Nothing
					Else
						entity.SubSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property MasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.MasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MasterSeriesID = Nothing
					Else
						entity.MasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Series As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Series
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Series = Nothing
					Else
						entity.Series = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeCode = Nothing
					Else
						entity.OfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApprove = Nothing
					Else
						entity.IsApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Approvedby As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Approvedby
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Approvedby = Nothing
					Else
						entity.Approvedby = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintSuccessFully As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.PrintSuccessFully
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintSuccessFully = Nothing
					Else
						entity.PrintSuccessFully = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICSubSeries
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSubSeriesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICSubSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSubSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICSubSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICSubSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICSubSeriesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICSubSeriesCollection
		Inherits esEntityCollection(Of ICSubSeries)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSubSeriesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICSubSeriesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICSubSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSubSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICSubSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICSubSeriesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICSubSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICSubSeriesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICSubSeriesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICSubSeriesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICSubSeriesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SubSeriesID" 
					Return Me.SubSeriesID
				Case "MasterSeriesID" 
					Return Me.MasterSeriesID
				Case "Series" 
					Return Me.Series
				Case "OfficeCode" 
					Return Me.OfficeCode
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApprove" 
					Return Me.IsApprove
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Approvedby" 
					Return Me.Approvedby
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "PrintSuccessFully" 
					Return Me.PrintSuccessFully
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SubSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.SubSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property MasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.MasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Series As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.Series, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.OfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.IsApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Approvedby As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.Approvedby, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintSuccessFully As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.PrintSuccessFully, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSeriesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICSubSeries 
		Inherits esICSubSeries
		
	
		#Region "UpToICMasterSeriesByMasterSeriesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSeries_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICMasterSeriesByMasterSeriesID Is Nothing _
						 AndAlso Not MasterSeriesID.Equals(Nothing)  Then
						
					Me._UpToICMasterSeriesByMasterSeriesID = New ICMasterSeries()
					Me._UpToICMasterSeriesByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Where(Me._UpToICMasterSeriesByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Load()
				End If

				Return Me._UpToICMasterSeriesByMasterSeriesID
			End Get
			
            Set(ByVal value As ICMasterSeries)
				Me.RemovePreSave("UpToICMasterSeriesByMasterSeriesID")
				

				If value Is Nothing Then
				
					Me.MasterSeriesID = Nothing
				
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
				Else
				
					Me.MasterSeriesID = value.MasterSeriesID
					
					Me._UpToICMasterSeriesByMasterSeriesID = value
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICOfficeByOfficeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSeries_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByOfficeCode As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByOfficeCode Is Nothing _
						 AndAlso Not OfficeCode.Equals(Nothing)  Then
						
					Me._UpToICOfficeByOfficeCode = New ICOffice()
					Me._UpToICOfficeByOfficeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByOfficeCode", Me._UpToICOfficeByOfficeCode)
					Me._UpToICOfficeByOfficeCode.Query.Where(Me._UpToICOfficeByOfficeCode.Query.OfficeID = Me.OfficeCode)
					Me._UpToICOfficeByOfficeCode.Query.Load()
				End If

				Return Me._UpToICOfficeByOfficeCode
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByOfficeCode")
				

				If value Is Nothing Then
				
					Me.OfficeCode = Nothing
				
					Me._UpToICOfficeByOfficeCode = Nothing
				Else
				
					Me.OfficeCode = value.OfficeID
					
					Me._UpToICOfficeByOfficeCode = value
					Me.SetPreSave("UpToICOfficeByOfficeCode", Me._UpToICOfficeByOfficeCode)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICMasterSeriesByMasterSeriesID Is Nothing Then
				Me.MasterSeriesID = Me._UpToICMasterSeriesByMasterSeriesID.MasterSeriesID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByOfficeCode Is Nothing Then
				Me.OfficeCode = Me._UpToICOfficeByOfficeCode.OfficeID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICSubSeriesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.SubSeriesID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.SubSeriesID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.MasterSeriesID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.MasterSeriesID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.Series, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.Series
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.OfficeCode, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.OfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.IsApprove, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.IsApprove
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.Approvedby, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.Approvedby
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.CreatedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.PrintSuccessFully, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.PrintSuccessFully
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.Creater, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSeriesMetadata.ColumnNames.CreationDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSubSeriesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICSubSeriesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SubSeriesID As String = "SubSeriesID"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const Series As String = "Series"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const PrintSuccessFully As String = "PrintSuccessFully"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SubSeriesID As String = "SubSeriesID"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const Series As String = "Series"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const PrintSuccessFully As String = "PrintSuccessFully"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICSubSeriesMetadata)
			
				If ICSubSeriesMetadata.mapDelegates Is Nothing Then
					ICSubSeriesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICSubSeriesMetadata._meta Is Nothing Then
					ICSubSeriesMetadata._meta = New ICSubSeriesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SubSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("MasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Series", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("OfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Approvedby", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintSuccessFully", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_SubSeries"
				meta.Destination = "IC_SubSeries"
				
				meta.spInsert = "proc_IC_SubSeriesInsert"
				meta.spUpdate = "proc_IC_SubSeriesUpdate"
				meta.spDelete = "proc_IC_SubSeriesDelete"
				meta.spLoadAll = "proc_IC_SubSeriesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_SubSeriesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICSubSeriesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
