
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionAccountsCollectionNatureProductType' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionAccountsCollectionNatureProductType))> _
	<XmlType("ICCollectionAccountsCollectionNatureProductType")> _	
	Partial Public Class ICCollectionAccountsCollectionNatureProductType 
		Inherits esICCollectionAccountsCollectionNatureProductType
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionAccountsCollectionNatureProductType()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String)
			Dim obj As New ICCollectionAccountsCollectionNatureProductType()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.CollectionNatureCode = collectionNatureCode
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionAccountsCollectionNatureProductType()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.CollectionNatureCode = collectionNatureCode
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionAccountsCollectionNatureProductTypeCollection")> _
	Partial Public Class ICCollectionAccountsCollectionNatureProductTypeCollection
		Inherits esICCollectionAccountsCollectionNatureProductTypeCollection
		Implements IEnumerable(Of ICCollectionAccountsCollectionNatureProductType)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String) As ICCollectionAccountsCollectionNatureProductType
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.CollectionNatureCode = collectionNatureCode And e.ProductTypeCode = productTypeCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionAccountsCollectionNatureProductType))> _
		Public Class ICCollectionAccountsCollectionNatureProductTypeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionAccountsCollectionNatureProductTypeCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionAccountsCollectionNatureProductTypeCollectionWCFPacket) As ICCollectionAccountsCollectionNatureProductTypeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionAccountsCollectionNatureProductTypeCollection) As ICCollectionAccountsCollectionNatureProductTypeCollectionWCFPacket
				Return New ICCollectionAccountsCollectionNatureProductTypeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionAccountsCollectionNatureProductTypeQuery 
		Inherits esICCollectionAccountsCollectionNatureProductTypeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionAccountsCollectionNatureProductTypeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionAccountsCollectionNatureProductTypeQuery) As String
			Return ICCollectionAccountsCollectionNatureProductTypeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionAccountsCollectionNatureProductTypeQuery
			Return DirectCast(ICCollectionAccountsCollectionNatureProductTypeQuery.SerializeHelper.FromXml(query, GetType(ICCollectionAccountsCollectionNatureProductTypeQuery)), ICCollectionAccountsCollectionNatureProductTypeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNatureProductType
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, collectionNatureCode, productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, collectionNatureCode, productTypeCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, collectionNatureCode, productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, collectionNatureCode, productTypeCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			Dim query As New ICCollectionAccountsCollectionNatureProductTypeQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.CollectionNatureCode = collectionNatureCode And query.ProductTypeCode = productTypeCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal collectionNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("CollectionNatureCode", collectionNatureCode)
						parms.Add("ProductTypeCode", productTypeCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.Currency, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CollectionNatureCode, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICCollectionProductTypeByProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICCollectionProductTypeByProductTypeCode")
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccountsCollectionNatureProductType.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionAccountsCollectionNatureByAccountNumber As ICCollectionAccountsCollectionNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionProductTypeByProductTypeCode As ICCollectionProductType
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionAccountsCollectionNatureProductType)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionAccountsCollectionNatureProductType
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureProductTypeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionAccountsCollectionNatureProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsCollectionNatureProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionAccountsCollectionNatureProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionAccountsCollectionNatureProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionAccountsCollectionNatureProductTypeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNatureProductTypeCollection
		Inherits esEntityCollection(Of ICCollectionAccountsCollectionNatureProductType)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureProductTypeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionAccountsCollectionNatureProductTypeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionAccountsCollectionNatureProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsCollectionNatureProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionAccountsCollectionNatureProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionAccountsCollectionNatureProductTypeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionAccountsCollectionNatureProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionAccountsCollectionNatureProductTypeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionAccountsCollectionNatureProductTypeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionAccountsCollectionNatureProductTypeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsCollectionNatureProductTypeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionAccountsCollectionNatureProductType 
		Inherits esICCollectionAccountsCollectionNatureProductType
		
	
		#Region "UpToICCollectionAccountsCollectionNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionAccountsCollectionNature1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionAccountsCollectionNatureByAccountNumber As ICCollectionAccountsCollectionNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionAccountsCollectionNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not CollectionNatureCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = New ICCollectionAccountsCollectionNature()
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber", Me._UpToICCollectionAccountsCollectionNatureByAccountNumber)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.CollectionNatureCode = Me.CollectionNatureCode)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICCollectionAccountsCollectionNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICCollectionAccountsCollectionNature)
				Me.RemovePreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.CollectionNatureCode = Nothing
				
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.CollectionNatureCode = value.CollectionNatureCode
					
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = value
					Me.SetPreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber", Me._UpToICCollectionAccountsCollectionNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionProductTypeByProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionProductTypeByProductTypeCode As ICCollectionProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionProductTypeByProductTypeCode Is Nothing _
						 AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionProductTypeByProductTypeCode = New ICCollectionProductType()
					Me._UpToICCollectionProductTypeByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionProductTypeByProductTypeCode", Me._UpToICCollectionProductTypeByProductTypeCode)
					Me._UpToICCollectionProductTypeByProductTypeCode.Query.Where(Me._UpToICCollectionProductTypeByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICCollectionProductTypeByProductTypeCode.Query.Load()
				End If

				Return Me._UpToICCollectionProductTypeByProductTypeCode
			End Get
			
            Set(ByVal value As ICCollectionProductType)
				Me.RemovePreSave("UpToICCollectionProductTypeByProductTypeCode")
				

				If value Is Nothing Then
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICCollectionProductTypeByProductTypeCode = Nothing
				Else
				
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICCollectionProductTypeByProductTypeCode = value
					Me.SetPreSave("UpToICCollectionProductTypeByProductTypeCode", Me._UpToICCollectionProductTypeByProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionAccountsCollectionNatureProductTypeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CollectionNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CollectionNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CompanyCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.ProductTypeCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.ProductTypeCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.CreateDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionAccountsCollectionNatureProductTypeMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionAccountsCollectionNatureProductTypeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionAccountsCollectionNatureProductTypeMetadata)
			
				If ICCollectionAccountsCollectionNatureProductTypeMetadata.mapDelegates Is Nothing Then
					ICCollectionAccountsCollectionNatureProductTypeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionAccountsCollectionNatureProductTypeMetadata._meta Is Nothing Then
					ICCollectionAccountsCollectionNatureProductTypeMetadata._meta = New ICCollectionAccountsCollectionNatureProductTypeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_CollectionAccountsCollectionNatureProductType"
				meta.Destination = "IC_CollectionAccountsCollectionNatureProductType"
				
				meta.spInsert = "proc_IC_CollectionAccountsCollectionNatureProductTypeInsert"
				meta.spUpdate = "proc_IC_CollectionAccountsCollectionNatureProductTypeUpdate"
				meta.spDelete = "proc_IC_CollectionAccountsCollectionNatureProductTypeDelete"
				meta.spLoadAll = "proc_IC_CollectionAccountsCollectionNatureProductTypeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionAccountsCollectionNatureProductTypeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionAccountsCollectionNatureProductTypeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
