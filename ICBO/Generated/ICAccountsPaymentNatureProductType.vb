
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 4/13/2015 12:23:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountsPaymentNatureProductType' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountsPaymentNatureProductType))> _
	<XmlType("ICAccountsPaymentNatureProductType")> _	
	Partial Public Class ICAccountsPaymentNatureProductType 
		Inherits esICAccountsPaymentNatureProductType
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountsPaymentNatureProductType()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String)
			Dim obj As New ICAccountsPaymentNatureProductType()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountsPaymentNatureProductType()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountsPaymentNatureProductTypeCollection")> _
	Partial Public Class ICAccountsPaymentNatureProductTypeCollection
		Inherits esICAccountsPaymentNatureProductTypeCollection
		Implements IEnumerable(Of ICAccountsPaymentNatureProductType)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String) As ICAccountsPaymentNatureProductType
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.PaymentNatureCode = paymentNatureCode And e.ProductTypeCode = productTypeCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountsPaymentNatureProductType))> _
		Public Class ICAccountsPaymentNatureProductTypeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountsPaymentNatureProductTypeCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountsPaymentNatureProductTypeCollectionWCFPacket) As ICAccountsPaymentNatureProductTypeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountsPaymentNatureProductTypeCollection) As ICAccountsPaymentNatureProductTypeCollectionWCFPacket
				Return New ICAccountsPaymentNatureProductTypeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountsPaymentNatureProductTypeQuery 
		Inherits esICAccountsPaymentNatureProductTypeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountsPaymentNatureProductTypeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountsPaymentNatureProductTypeQuery) As String
			Return ICAccountsPaymentNatureProductTypeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountsPaymentNatureProductTypeQuery
			Return DirectCast(ICAccountsPaymentNatureProductTypeQuery.SerializeHelper.FromXml(query, GetType(ICAccountsPaymentNatureProductTypeQuery)), ICAccountsPaymentNatureProductTypeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductType
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, paymentNatureCode, productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, paymentNatureCode, productTypeCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, paymentNatureCode, productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, paymentNatureCode, productTypeCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			Dim query As New ICAccountsPaymentNatureProductTypeQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.PaymentNatureCode = paymentNatureCode And query.ProductTypeCode = productTypeCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal productTypeCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("PaymentNatureCode", paymentNatureCode)
						parms.Add("ProductTypeCode", productTypeCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.AccountsPaymentNatureProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountsPaymentNatureProductTypeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICProductTypeByProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICProductTypeByProductTypeCode")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.PrintTemplateFileIDForBank
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintTemplateFileIDForBank As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateFileIDForBank)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateFileIDForBank, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateFileIDForBank)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.ReIssuanceTemplateFileIDForBank
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReIssuanceTemplateFileIDForBank As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuanceTemplateFileIDForBank)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuanceTemplateFileIDForBank, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuanceTemplateFileIDForBank)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.PrintTemplateIDForClient
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintTemplateIDForClient As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateIDForClient)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateIDForClient, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateIDForClient)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.ReIssuancePrintTemplateIDForClient
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReIssuancePrintTemplateIDForClient As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuancePrintTemplateIDForClient)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuancePrintTemplateIDForClient, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuancePrintTemplateIDForClient)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNatureProductType.PackageId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PackageId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PackageId, value) Then
					Me._UpToICPackageByPackageId = Nothing
					Me.OnPropertyChanged("UpToICPackageByPackageId")
					OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PackageId)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPackageByPackageId As ICPackage
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICProductTypeByProductTypeCode As ICProductType
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "AccountsPaymentNatureProductTypeCode"
							Me.str().AccountsPaymentNatureProductTypeCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "PrintTemplateFileIDForBank"
							Me.str().PrintTemplateFileIDForBank = CType(value, string)
												
						Case "ReIssuanceTemplateFileIDForBank"
							Me.str().ReIssuanceTemplateFileIDForBank = CType(value, string)
												
						Case "PrintTemplateIDForClient"
							Me.str().PrintTemplateIDForClient = CType(value, string)
												
						Case "ReIssuancePrintTemplateIDForClient"
							Me.str().ReIssuancePrintTemplateIDForClient = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "PackageId"
							Me.str().PackageId = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "AccountsPaymentNatureProductTypeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AccountsPaymentNatureProductTypeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "PrintTemplateFileIDForBank"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintTemplateFileIDForBank = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateFileIDForBank)
							End If
						
						Case "ReIssuanceTemplateFileIDForBank"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReIssuanceTemplateFileIDForBank = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuanceTemplateFileIDForBank)
							End If
						
						Case "PrintTemplateIDForClient"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintTemplateIDForClient = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateIDForClient)
							End If
						
						Case "ReIssuancePrintTemplateIDForClient"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReIssuancePrintTemplateIDForClient = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuancePrintTemplateIDForClient)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreationDate)
							End If
						
						Case "PackageId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PackageId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PackageId)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountsPaymentNatureProductType)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountsPaymentNatureProductTypeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AccountsPaymentNatureProductTypeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountsPaymentNatureProductTypeCode = Nothing
					Else
						entity.AccountsPaymentNatureProductTypeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintTemplateFileIDForBank As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintTemplateFileIDForBank
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintTemplateFileIDForBank = Nothing
					Else
						entity.PrintTemplateFileIDForBank = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReIssuanceTemplateFileIDForBank As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReIssuanceTemplateFileIDForBank
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReIssuanceTemplateFileIDForBank = Nothing
					Else
						entity.ReIssuanceTemplateFileIDForBank = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintTemplateIDForClient As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintTemplateIDForClient
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintTemplateIDForClient = Nothing
					Else
						entity.PrintTemplateIDForClient = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReIssuancePrintTemplateIDForClient As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReIssuancePrintTemplateIDForClient
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReIssuancePrintTemplateIDForClient = Nothing
					Else
						entity.ReIssuancePrintTemplateIDForClient = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackageId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PackageId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageId = Nothing
					Else
						entity.PackageId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountsPaymentNatureProductType
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountsPaymentNatureProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountsPaymentNatureProductTypeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductTypeCollection
		Inherits esEntityCollection(Of ICAccountsPaymentNatureProductType)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountsPaymentNatureProductTypeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountsPaymentNatureProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountsPaymentNatureProductTypeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountsPaymentNatureProductTypeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountsPaymentNatureProductTypeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountsPaymentNatureProductTypeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureProductTypeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "AccountsPaymentNatureProductTypeCode" 
					Return Me.AccountsPaymentNatureProductTypeCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "PrintTemplateFileIDForBank" 
					Return Me.PrintTemplateFileIDForBank
				Case "ReIssuanceTemplateFileIDForBank" 
					Return Me.ReIssuanceTemplateFileIDForBank
				Case "PrintTemplateIDForClient" 
					Return Me.PrintTemplateIDForClient
				Case "ReIssuancePrintTemplateIDForClient" 
					Return Me.ReIssuancePrintTemplateIDForClient
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "PackageId" 
					Return Me.PackageId
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountsPaymentNatureProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PrintTemplateFileIDForBank As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateFileIDForBank, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReIssuanceTemplateFileIDForBank As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuanceTemplateFileIDForBank, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintTemplateIDForClient As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateIDForClient, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReIssuancePrintTemplateIDForClient As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuancePrintTemplateIDForClient, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PackageId As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PackageId, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountsPaymentNatureProductType 
		Inherits esICAccountsPaymentNatureProductType
		
	
		#Region "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNatureProductType.ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductTypeCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductTypeCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureProductTypeAndPrintLocationsQuery), New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode And parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureProductTypeAndPrintLocations_IC_AccountsPaymentNatureProductType2
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection 
		
			Get
				If Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber Is Nothing Then
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber = New ICAccountPaymentNatureProductTypeAndPrintLocationsCollection()
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber", Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) AndAlso Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber")
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
		#End Region

		#Region "ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNatureProductType.ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductTypeCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureProductTypeTemplatesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureProductTypeTemplatesQuery), New ICAccountsPaymentNatureProductTypeTemplatesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode And parent.ProductTypeCode = mee.ProductCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductTypeTemplates_IC_AccountsPaymentNatureProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber As ICAccountsPaymentNatureProductTypeTemplatesCollection 
		
			Get
				If Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber Is Nothing Then
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber = New ICAccountsPaymentNatureProductTypeTemplatesCollection()
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber", Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) AndAlso Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.ProductCode = Me.ProductTypeCode)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.ProductCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureProductTypeTemplatesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber")
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber As ICAccountsPaymentNatureProductTypeTemplatesCollection
		#End Region

		#Region "ICPackageChargesCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageChargesCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNatureProductType.ICPackageChargesCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICPackageChargesCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductTypeCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode,ProductTypeCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageChargesCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias())
			
			Dim mee As ICPackageChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageChargesQuery), New ICPackageChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode And parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageChargesCollectionByAccountNumber As ICPackageChargesCollection 
		
			Get
				If Me._ICPackageChargesCollectionByAccountNumber Is Nothing Then
					Me._ICPackageChargesCollectionByAccountNumber = New ICPackageChargesCollection()
					Me._ICPackageChargesCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageChargesCollectionByAccountNumber", Me._ICPackageChargesCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) AndAlso Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageChargesCollectionByAccountNumber.Query.Where(Me._ICPackageChargesCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICPackageChargesCollectionByAccountNumber.Query.Where(Me._ICPackageChargesCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICPackageChargesCollectionByAccountNumber.Query.Where(Me._ICPackageChargesCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICPackageChargesCollectionByAccountNumber.Query.Where(Me._ICPackageChargesCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICPackageChargesCollectionByAccountNumber.Query.Where(Me._ICPackageChargesCollectionByAccountNumber.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICPackageChargesCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageChargesCollectionByAccountNumber.fks.Add(ICPackageChargesMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICPackageChargesCollectionByAccountNumber.fks.Add(ICPackageChargesMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICPackageChargesCollectionByAccountNumber.fks.Add(ICPackageChargesMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICPackageChargesCollectionByAccountNumber.fks.Add(ICPackageChargesMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
						Me._ICPackageChargesCollectionByAccountNumber.fks.Add(ICPackageChargesMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICPackageChargesCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICPackageChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageChargesCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICPackageChargesCollectionByAccountNumber")
					Me._ICPackageChargesCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageChargesCollectionByAccountNumber As ICPackageChargesCollection
		#End Region

		#Region "UpToICAccountsPaymentNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_AccountsPaymentNature1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureByAccountNumber = New ICAccountsPaymentNature()
					Me._UpToICAccountsPaymentNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNature)
				Me.RemovePreSave("UpToICAccountsPaymentNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICAccountsPaymentNatureByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPackageByPackageId - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_Package
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPackageByPackageId As ICPackage
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPackageByPackageId Is Nothing _
						 AndAlso Not PackageId.Equals(Nothing)  Then
						
					Me._UpToICPackageByPackageId = New ICPackage()
					Me._UpToICPackageByPackageId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPackageByPackageId", Me._UpToICPackageByPackageId)
					Me._UpToICPackageByPackageId.Query.Where(Me._UpToICPackageByPackageId.Query.PackageID = Me.PackageId)
					Me._UpToICPackageByPackageId.Query.Load()
				End If

				Return Me._UpToICPackageByPackageId
			End Get
			
            Set(ByVal value As ICPackage)
				Me.RemovePreSave("UpToICPackageByPackageId")
				

				If value Is Nothing Then
				
					Me.PackageId = Nothing
				
					Me._UpToICPackageByPackageId = Nothing
				Else
				
					Me.PackageId = value.PackageID
					
					Me._UpToICPackageByPackageId = value
					Me.SetPreSave("UpToICPackageByPackageId", Me._UpToICPackageByPackageId)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICProductTypeByProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_ProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICProductTypeByProductTypeCode As ICProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICProductTypeByProductTypeCode Is Nothing _
						 AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICProductTypeByProductTypeCode = New ICProductType()
					Me._UpToICProductTypeByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Where(Me._UpToICProductTypeByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Load()
				End If

				Return Me._UpToICProductTypeByProductTypeCode
			End Get
			
            Set(ByVal value As ICProductType)
				Me.RemovePreSave("UpToICProductTypeByProductTypeCode")
				

				If value Is Nothing Then
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICProductTypeByProductTypeCode = Nothing
				Else
				
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICProductTypeByProductTypeCode = value
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber"
					coll = Me.ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber
					Exit Select
				Case "ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber"
					coll = Me.ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber
					Exit Select
				Case "ICPackageChargesCollectionByAccountNumber"
					coll = Me.ICPackageChargesCollectionByAccountNumber
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByAccountNumber", GetType(ICAccountPaymentNatureProductTypeAndPrintLocationsCollection), New ICAccountPaymentNatureProductTypeAndPrintLocations()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureProductTypeTemplatesCollectionByAccountNumber", GetType(ICAccountsPaymentNatureProductTypeTemplatesCollection), New ICAccountsPaymentNatureProductTypeTemplates()))
			props.Add(new esPropertyDescriptor(Me, "ICPackageChargesCollectionByAccountNumber", GetType(ICPackageChargesCollection), New ICPackageCharges()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICPackageByPackageId Is Nothing Then
				Me.PackageId = Me._UpToICPackageByPackageId.PackageID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountsPaymentNatureProductTypeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CompanyCode, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ProductTypeCode, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ProductTypeCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreateDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.IsApproved, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ApprovedOn, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateFileIDForBank, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateFileIDForBank
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuanceTemplateFileIDForBank, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuanceTemplateFileIDForBank
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PrintTemplateIDForClient, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PrintTemplateIDForClient
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.ReIssuancePrintTemplateIDForClient, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.ReIssuancePrintTemplateIDForClient
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Creater, 17, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.CreationDate, 18, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PackageId, 19, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureProductTypeMetadata.PropertyNames.PackageId
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountsPaymentNatureProductTypeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const AccountsPaymentNatureProductTypeCode As String = "AccountsPaymentNatureProductTypeCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const PrintTemplateFileIDForBank As String = "PrintTemplateFileIDForBank"
			 Public Const ReIssuanceTemplateFileIDForBank As String = "ReIssuanceTemplateFileIDForBank"
			 Public Const PrintTemplateIDForClient As String = "PrintTemplateIDForClient"
			 Public Const ReIssuancePrintTemplateIDForClient As String = "ReIssuancePrintTemplateIDForClient"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const PackageId As String = "PackageId"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const AccountsPaymentNatureProductTypeCode As String = "AccountsPaymentNatureProductTypeCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const PrintTemplateFileIDForBank As String = "PrintTemplateFileIDForBank"
			 Public Const ReIssuanceTemplateFileIDForBank As String = "ReIssuanceTemplateFileIDForBank"
			 Public Const PrintTemplateIDForClient As String = "PrintTemplateIDForClient"
			 Public Const ReIssuancePrintTemplateIDForClient As String = "ReIssuancePrintTemplateIDForClient"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const PackageId As String = "PackageId"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountsPaymentNatureProductTypeMetadata)
			
				If ICAccountsPaymentNatureProductTypeMetadata.mapDelegates Is Nothing Then
					ICAccountsPaymentNatureProductTypeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountsPaymentNatureProductTypeMetadata._meta Is Nothing Then
					ICAccountsPaymentNatureProductTypeMetadata._meta = New ICAccountsPaymentNatureProductTypeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountsPaymentNatureProductTypeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PrintTemplateFileIDForBank", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReIssuanceTemplateFileIDForBank", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintTemplateIDForClient", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReIssuancePrintTemplateIDForClient", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PackageId", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_AccountsPaymentNatureProductType"
				meta.Destination = "IC_AccountsPaymentNatureProductType"
				
				meta.spInsert = "proc_IC_AccountsPaymentNatureProductTypeInsert"
				meta.spUpdate = "proc_IC_AccountsPaymentNatureProductTypeUpdate"
				meta.spDelete = "proc_IC_AccountsPaymentNatureProductTypeDelete"
				meta.spLoadAll = "proc_IC_AccountsPaymentNatureProductTypeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountsPaymentNatureProductTypeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountsPaymentNatureProductTypeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
