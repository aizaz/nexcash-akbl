
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Template' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICTemplate))> _
	<XmlType("ICTemplate")> _	
	Partial Public Class ICTemplate 
		Inherits esICTemplate
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICTemplate()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal templateID As System.Int32)
			Dim obj As New ICTemplate()
			obj.TemplateID = templateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal templateID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICTemplate()
			obj.TemplateID = templateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICTemplateCollection")> _
	Partial Public Class ICTemplateCollection
		Inherits esICTemplateCollection
		Implements IEnumerable(Of ICTemplate)
	
		Public Function FindByPrimaryKey(ByVal templateID As System.Int32) As ICTemplate
			Return MyBase.SingleOrDefault(Function(e) e.TemplateID.HasValue AndAlso e.TemplateID.Value = templateID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICTemplate))> _
		Public Class ICTemplateCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICTemplateCollection)
			
			Public Shared Widening Operator CType(packet As ICTemplateCollectionWCFPacket) As ICTemplateCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICTemplateCollection) As ICTemplateCollectionWCFPacket
				Return New ICTemplateCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICTemplateQuery 
		Inherits esICTemplateQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICTemplateQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICTemplateQuery) As String
			Return ICTemplateQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICTemplateQuery
			Return DirectCast(ICTemplateQuery.SerializeHelper.FromXml(query, GetType(ICTemplateQuery)), ICTemplateQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICTemplate
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal templateID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(templateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(templateID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal templateID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(templateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(templateID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal templateID As System.Int32) As Boolean
		
			Dim query As New ICTemplateQuery()
			query.Where(query.TemplateID = templateID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal templateID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("TemplateID", templateID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Template.TemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateMetadata.ColumnNames.TemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateMetadata.ColumnNames.TemplateID, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.TemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.TemplateName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateName As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.TemplateName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.TemplateName, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.TemplateName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.TemplateFormat
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateFormat As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.TemplateFormat)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.TemplateFormat, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.TemplateFormat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.SheetName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SheetName As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.SheetName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.SheetName, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.SheetName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.FieldDelimiter
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDelimiter As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.FieldDelimiter)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.FieldDelimiter, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.FieldDelimiter)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.isFixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsFixLength As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateMetadata.ColumnNames.IsFixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateMetadata.ColumnNames.IsFixLength, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsFixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.FileUploadTemplateCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileUploadTemplateCode As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.FileUploadTemplateCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.FileUploadTemplateCode, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.FileUploadTemplateCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.HeaderIdentifier
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HeaderIdentifier As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.HeaderIdentifier)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.HeaderIdentifier, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.HeaderIdentifier)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.DetailIdentifier
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailIdentifier As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.DetailIdentifier)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.DetailIdentifier, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.DetailIdentifier)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.TemplateType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateType As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateMetadata.ColumnNames.TemplateType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateMetadata.ColumnNames.TemplateType, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.TemplateType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Template.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "TemplateID"
							Me.str().TemplateID = CType(value, string)
												
						Case "TemplateName"
							Me.str().TemplateName = CType(value, string)
												
						Case "TemplateFormat"
							Me.str().TemplateFormat = CType(value, string)
												
						Case "SheetName"
							Me.str().SheetName = CType(value, string)
												
						Case "FieldDelimiter"
							Me.str().FieldDelimiter = CType(value, string)
												
						Case "IsFixLength"
							Me.str().IsFixLength = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "FileUploadTemplateCode"
							Me.str().FileUploadTemplateCode = CType(value, string)
												
						Case "HeaderIdentifier"
							Me.str().HeaderIdentifier = CType(value, string)
												
						Case "DetailIdentifier"
							Me.str().DetailIdentifier = CType(value, string)
												
						Case "TemplateType"
							Me.str().TemplateType = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "TemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.TemplateID)
							End If
						
						Case "IsFixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsFixLength = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsFixLength)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.IsApproved)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICTemplate)
				Me.entity = entity
			End Sub				
		
	
			Public Property TemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateID = Nothing
					Else
						entity.TemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateName As System.String 
				Get
					Dim data_ As System.String = entity.TemplateName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateName = Nothing
					Else
						entity.TemplateName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateFormat As System.String 
				Get
					Dim data_ As System.String = entity.TemplateFormat
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateFormat = Nothing
					Else
						entity.TemplateFormat = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SheetName As System.String 
				Get
					Dim data_ As System.String = entity.SheetName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SheetName = Nothing
					Else
						entity.SheetName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldDelimiter As System.String 
				Get
					Dim data_ As System.String = entity.FieldDelimiter
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDelimiter = Nothing
					Else
						entity.FieldDelimiter = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsFixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsFixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsFixLength = Nothing
					Else
						entity.IsFixLength = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileUploadTemplateCode As System.String 
				Get
					Dim data_ As System.String = entity.FileUploadTemplateCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileUploadTemplateCode = Nothing
					Else
						entity.FileUploadTemplateCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property HeaderIdentifier As System.String 
				Get
					Dim data_ As System.String = entity.HeaderIdentifier
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HeaderIdentifier = Nothing
					Else
						entity.HeaderIdentifier = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailIdentifier As System.String 
				Get
					Dim data_ As System.String = entity.DetailIdentifier
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailIdentifier = Nothing
					Else
						entity.DetailIdentifier = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateType As System.String 
				Get
					Dim data_ As System.String = entity.TemplateType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateType = Nothing
					Else
						entity.TemplateType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICTemplate
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICTemplateMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICTemplateQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICTemplateCollection
		Inherits esEntityCollection(Of ICTemplate)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICTemplateMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICTemplateCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICTemplateQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICTemplateQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICTemplateQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICTemplateQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICTemplateMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "TemplateID" 
					Return Me.TemplateID
				Case "TemplateName" 
					Return Me.TemplateName
				Case "TemplateFormat" 
					Return Me.TemplateFormat
				Case "SheetName" 
					Return Me.SheetName
				Case "FieldDelimiter" 
					Return Me.FieldDelimiter
				Case "IsFixLength" 
					Return Me.IsFixLength
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "FileUploadTemplateCode" 
					Return Me.FileUploadTemplateCode
				Case "HeaderIdentifier" 
					Return Me.HeaderIdentifier
				Case "DetailIdentifier" 
					Return Me.DetailIdentifier
				Case "TemplateType" 
					Return Me.TemplateType
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property TemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.TemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateName As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.TemplateName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateFormat As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.TemplateFormat, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SheetName As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.SheetName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldDelimiter As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.FieldDelimiter, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsFixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.IsFixLength, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FileUploadTemplateCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.FileUploadTemplateCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property HeaderIdentifier As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.HeaderIdentifier, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailIdentifier As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.DetailIdentifier, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateType As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.TemplateType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICTemplate 
		Inherits esICTemplate
		
	
		#Region "ICAccountPaymentNatureTemplateCollectionByTemplateID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureTemplateCollectionByTemplateID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICTemplate.ICAccountPaymentNatureTemplateCollectionByTemplateID_Delegate)
				map.PropertyName = "ICAccountPaymentNatureTemplateCollectionByTemplateID"
				map.MyColumnName = "TemplateID"
				map.ParentColumnName = "TemplateID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureTemplateCollectionByTemplateID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICTemplateQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureTemplateQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureTemplateQuery), New ICAccountPaymentNatureTemplateQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.TemplateID = mee.TemplateID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureTemplate_IC_Template
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureTemplateCollectionByTemplateID As ICAccountPaymentNatureTemplateCollection 
		
			Get
				If Me._ICAccountPaymentNatureTemplateCollectionByTemplateID Is Nothing Then
					Me._ICAccountPaymentNatureTemplateCollectionByTemplateID = New ICAccountPaymentNatureTemplateCollection()
					Me._ICAccountPaymentNatureTemplateCollectionByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureTemplateCollectionByTemplateID", Me._ICAccountPaymentNatureTemplateCollectionByTemplateID)
				
					If Not Me.TemplateID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureTemplateCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureTemplateCollectionByTemplateID.Query.TemplateID = Me.TemplateID)
							Me._ICAccountPaymentNatureTemplateCollectionByTemplateID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureTemplateCollectionByTemplateID.fks.Add(ICAccountPaymentNatureTemplateMetadata.ColumnNames.TemplateID, Me.TemplateID)
					End If
				End If

				Return Me._ICAccountPaymentNatureTemplateCollectionByTemplateID
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureTemplateCollectionByTemplateID Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureTemplateCollectionByTemplateID")
					Me._ICAccountPaymentNatureTemplateCollectionByTemplateID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureTemplateCollectionByTemplateID As ICAccountPaymentNatureTemplateCollection
		#End Region

		#Region "ICAccountPayNatureFileUploadTemplateCollectionByTemplateID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPayNatureFileUploadTemplateCollectionByTemplateID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICTemplate.ICAccountPayNatureFileUploadTemplateCollectionByTemplateID_Delegate)
				map.PropertyName = "ICAccountPayNatureFileUploadTemplateCollectionByTemplateID"
				map.MyColumnName = "TemplateID"
				map.ParentColumnName = "TemplateID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPayNatureFileUploadTemplateCollectionByTemplateID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICTemplateQuery(data.NextAlias())
			
			Dim mee As ICAccountPayNatureFileUploadTemplateQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPayNatureFileUploadTemplateQuery), New ICAccountPayNatureFileUploadTemplateQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.TemplateID = mee.TemplateID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPayNatureFileUploadTemplate_IC_Template
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPayNatureFileUploadTemplateCollectionByTemplateID As ICAccountPayNatureFileUploadTemplateCollection 
		
			Get
				If Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID Is Nothing Then
					Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID = New ICAccountPayNatureFileUploadTemplateCollection()
					Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPayNatureFileUploadTemplateCollectionByTemplateID", Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID)
				
					If Not Me.TemplateID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID.Query.Where(Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID.Query.TemplateID = Me.TemplateID)
							Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID.fks.Add(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.TemplateID, Me.TemplateID)
					End If
				End If

				Return Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID
			End Get
			
			Set(ByVal value As ICAccountPayNatureFileUploadTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID Is Nothing Then

					Me.RemovePostSave("ICAccountPayNatureFileUploadTemplateCollectionByTemplateID")
					Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPayNatureFileUploadTemplateCollectionByTemplateID As ICAccountPayNatureFileUploadTemplateCollection
		#End Region

		#Region "ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICTemplate.ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID"
				map.MyColumnName = "TemplateID"
				map.ParentColumnName = "TemplateID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICTemplateQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureProductTypeTemplatesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureProductTypeTemplatesQuery), New ICAccountsPaymentNatureProductTypeTemplatesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.TemplateID = mee.TemplateID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductTypeTemplates_IC_Template
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID As ICAccountsPaymentNatureProductTypeTemplatesCollection 
		
			Get
				If Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID Is Nothing Then
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID = New ICAccountsPaymentNatureProductTypeTemplatesCollection()
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID", Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID)
				
					If Not Me.TemplateID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID.Query.Where(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID.Query.TemplateID = Me.TemplateID)
							Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID.fks.Add(ICAccountsPaymentNatureProductTypeTemplatesMetadata.ColumnNames.TemplateID, Me.TemplateID)
					End If
				End If

				Return Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureProductTypeTemplatesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID")
					Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID As ICAccountsPaymentNatureProductTypeTemplatesCollection
		#End Region

		#Region "UpToICFieldsListCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_TemplateFields_IC_Template
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICFieldsListCollection As ICFieldsListCollection
		
			Get
				If Me._UpToICFieldsListCollection Is Nothing Then
					Me._UpToICFieldsListCollection = New ICFieldsListCollection()
					Me._UpToICFieldsListCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICFieldsListCollection", Me._UpToICFieldsListCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.TemplateID.Equals(Nothing) Then 
				
						Dim m As New ICFieldsListQuery("m")
						Dim j As New ICTemplateFieldsQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.FieldID = j.FieldID)
                        m.Where(j.TemplateID = Me.TemplateID)

						Me._UpToICFieldsListCollection.Load(m)

					End If
				End If

				Return Me._UpToICFieldsListCollection
			End Get
			
			Set(ByVal value As ICFieldsListCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICFieldsListCollection Is Nothing Then

					Me.RemovePostSave("UpToICFieldsListCollection")
					Me._UpToICFieldsListCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_TemplateFields_IC_Template
		''' </summary>
		Public Sub AssociateICFieldsListCollection(entity As ICFieldsList)
			If Me._ICTemplateFieldsCollection Is Nothing Then
				Me._ICTemplateFieldsCollection = New ICTemplateFieldsCollection()
				Me._ICTemplateFieldsCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICTemplateFieldsCollection", Me._ICTemplateFieldsCollection)
			End If
			
			Dim obj As ICTemplateFields = Me._ICTemplateFieldsCollection.AddNew()
			obj.TemplateID = Me.TemplateID
			obj.FieldID = entity.FieldID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_TemplateFields_IC_Template
		''' </summary>
		Public Sub DissociateICFieldsListCollection(entity As ICFieldsList)
			If Me._ICTemplateFieldsCollection Is Nothing Then
				Me._ICTemplateFieldsCollection = new ICTemplateFieldsCollection()
				Me._ICTemplateFieldsCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICTemplateFieldsCollection", Me._ICTemplateFieldsCollection)
			End If

			Dim obj As ICTemplateFields = Me._ICTemplateFieldsCollection.AddNew()
			obj.TemplateID = Me.TemplateID
            obj.FieldID = entity.FieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICFieldsListCollection As ICFieldsListCollection
		Private _ICTemplateFieldsCollection As ICTemplateFieldsCollection
		#End Region

		#Region "ICTemplateFieldsCollectionByTemplateID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICTemplateFieldsCollectionByTemplateID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICTemplate.ICTemplateFieldsCollectionByTemplateID_Delegate)
				map.PropertyName = "ICTemplateFieldsCollectionByTemplateID"
				map.MyColumnName = "TemplateID"
				map.ParentColumnName = "TemplateID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICTemplateFieldsCollectionByTemplateID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICTemplateQuery(data.NextAlias())
			
			Dim mee As ICTemplateFieldsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICTemplateFieldsQuery), New ICTemplateFieldsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.TemplateID = mee.TemplateID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_TemplateFields_IC_Template
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICTemplateFieldsCollectionByTemplateID As ICTemplateFieldsCollection 
		
			Get
				If Me._ICTemplateFieldsCollectionByTemplateID Is Nothing Then
					Me._ICTemplateFieldsCollectionByTemplateID = New ICTemplateFieldsCollection()
					Me._ICTemplateFieldsCollectionByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICTemplateFieldsCollectionByTemplateID", Me._ICTemplateFieldsCollectionByTemplateID)
				
					If Not Me.TemplateID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICTemplateFieldsCollectionByTemplateID.Query.Where(Me._ICTemplateFieldsCollectionByTemplateID.Query.TemplateID = Me.TemplateID)
							Me._ICTemplateFieldsCollectionByTemplateID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICTemplateFieldsCollectionByTemplateID.fks.Add(ICTemplateFieldsMetadata.ColumnNames.TemplateID, Me.TemplateID)
					End If
				End If

				Return Me._ICTemplateFieldsCollectionByTemplateID
			End Get
			
			Set(ByVal value As ICTemplateFieldsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICTemplateFieldsCollectionByTemplateID Is Nothing Then

					Me.RemovePostSave("ICTemplateFieldsCollectionByTemplateID")
					Me._ICTemplateFieldsCollectionByTemplateID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICTemplateFieldsCollectionByTemplateID As ICTemplateFieldsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureTemplateCollectionByTemplateID"
					coll = Me.ICAccountPaymentNatureTemplateCollectionByTemplateID
					Exit Select
				Case "ICAccountPayNatureFileUploadTemplateCollectionByTemplateID"
					coll = Me.ICAccountPayNatureFileUploadTemplateCollectionByTemplateID
					Exit Select
				Case "ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID"
					coll = Me.ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID
					Exit Select
				Case "ICTemplateFieldsCollectionByTemplateID"
					coll = Me.ICTemplateFieldsCollectionByTemplateID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureTemplateCollectionByTemplateID", GetType(ICAccountPaymentNatureTemplateCollection), New ICAccountPaymentNatureTemplate()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountPayNatureFileUploadTemplateCollectionByTemplateID", GetType(ICAccountPayNatureFileUploadTemplateCollection), New ICAccountPayNatureFileUploadTemplate()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID", GetType(ICAccountsPaymentNatureProductTypeTemplatesCollection), New ICAccountsPaymentNatureProductTypeTemplates()))
			props.Add(new esPropertyDescriptor(Me, "ICTemplateFieldsCollectionByTemplateID", GetType(ICTemplateFieldsCollection), New ICTemplateFields()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAccountPaymentNatureTemplateCollectionByTemplateID Is Nothing Then
				Apply(Me._ICAccountPaymentNatureTemplateCollectionByTemplateID, "TemplateID", Me.TemplateID)
			End If
			
			If Not Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID Is Nothing Then
				Apply(Me._ICAccountPayNatureFileUploadTemplateCollectionByTemplateID, "TemplateID", Me.TemplateID)
			End If
			
			If Not Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID Is Nothing Then
				Apply(Me._ICAccountsPaymentNatureProductTypeTemplatesCollectionByTemplateID, "TemplateID", Me.TemplateID)
			End If
			
			If Not Me._ICTemplateFieldsCollection Is Nothing Then
				Apply(Me._ICTemplateFieldsCollection, "TemplateID", Me.TemplateID)
			End If
			
			If Not Me._ICTemplateFieldsCollectionByTemplateID Is Nothing Then
				Apply(Me._ICTemplateFieldsCollectionByTemplateID, "TemplateID", Me.TemplateID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICTemplateMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.TemplateID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.TemplateID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.TemplateName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.TemplateName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.TemplateFormat, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.TemplateFormat
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.SheetName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.SheetName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.FieldDelimiter, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.FieldDelimiter
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.IsFixLength, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.IsFixLength
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.CreateBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.CreateDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.ApprovedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.ApprovedOn, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.IsApproved, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.FileUploadTemplateCode, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.FileUploadTemplateCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.HeaderIdentifier, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.HeaderIdentifier
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.DetailIdentifier, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.DetailIdentifier
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.TemplateType, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.TemplateType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.Creater, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateMetadata.ColumnNames.CreationDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICTemplateMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const TemplateName As String = "TemplateName"
			 Public Const TemplateFormat As String = "TemplateFormat"
			 Public Const SheetName As String = "SheetName"
			 Public Const FieldDelimiter As String = "FieldDelimiter"
			 Public Const IsFixLength As String = "isFixLength"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "isApproved"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const HeaderIdentifier As String = "HeaderIdentifier"
			 Public Const DetailIdentifier As String = "DetailIdentifier"
			 Public Const TemplateType As String = "TemplateType"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const TemplateName As String = "TemplateName"
			 Public Const TemplateFormat As String = "TemplateFormat"
			 Public Const SheetName As String = "SheetName"
			 Public Const FieldDelimiter As String = "FieldDelimiter"
			 Public Const IsFixLength As String = "IsFixLength"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const HeaderIdentifier As String = "HeaderIdentifier"
			 Public Const DetailIdentifier As String = "DetailIdentifier"
			 Public Const TemplateType As String = "TemplateType"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICTemplateMetadata)
			
				If ICTemplateMetadata.mapDelegates Is Nothing Then
					ICTemplateMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICTemplateMetadata._meta Is Nothing Then
					ICTemplateMetadata._meta = New ICTemplateMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("TemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TemplateName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateFormat", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SheetName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldDelimiter", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsFixLength", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FileUploadTemplateCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("HeaderIdentifier", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailIdentifier", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Template"
				meta.Destination = "IC_Template"
				
				meta.spInsert = "proc_IC_TemplateInsert"
				meta.spUpdate = "proc_IC_TemplateUpdate"
				meta.spDelete = "proc_IC_TemplateDelete"
				meta.spLoadAll = "proc_IC_TemplateLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_TemplateLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICTemplateMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
