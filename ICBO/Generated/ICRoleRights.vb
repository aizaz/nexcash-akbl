
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_RoleRights' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICRoleRights))> _
	<XmlType("ICRoleRights")> _	
	Partial Public Class ICRoleRights 
		Inherits esICRoleRights
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICRoleRights()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal roleRightID As System.Int32)
			Dim obj As New ICRoleRights()
			obj.RoleRightID = roleRightID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal roleRightID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICRoleRights()
			obj.RoleRightID = roleRightID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICRoleRightsCollection")> _
	Partial Public Class ICRoleRightsCollection
		Inherits esICRoleRightsCollection
		Implements IEnumerable(Of ICRoleRights)
	
		Public Function FindByPrimaryKey(ByVal roleRightID As System.Int32) As ICRoleRights
			Return MyBase.SingleOrDefault(Function(e) e.RoleRightID.HasValue AndAlso e.RoleRightID.Value = roleRightID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICRoleRights))> _
		Public Class ICRoleRightsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICRoleRightsCollection)
			
			Public Shared Widening Operator CType(packet As ICRoleRightsCollectionWCFPacket) As ICRoleRightsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICRoleRightsCollection) As ICRoleRightsCollectionWCFPacket
				Return New ICRoleRightsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICRoleRightsQuery 
		Inherits esICRoleRightsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICRoleRightsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICRoleRightsQuery) As String
			Return ICRoleRightsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICRoleRightsQuery
			Return DirectCast(ICRoleRightsQuery.SerializeHelper.FromXml(query, GetType(ICRoleRightsQuery)), ICRoleRightsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICRoleRights
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal roleRightID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(roleRightID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(roleRightID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal roleRightID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(roleRightID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(roleRightID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal roleRightID As System.Int32) As Boolean
		
			Dim query As New ICRoleRightsQuery()
			query.Where(query.RoleRightID = roleRightID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal roleRightID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("RoleRightID", roleRightID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_RoleRights.RoleRightID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleRightID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleRightsMetadata.ColumnNames.RoleRightID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleRightsMetadata.ColumnNames.RoleRightID, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RoleRightID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.RightName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightName As System.String
			Get
				Return MyBase.GetSystemString(ICRoleRightsMetadata.ColumnNames.RightName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRoleRightsMetadata.ColumnNames.RightName, value) Then
					Me._UpToICRightsByRightName = Nothing
					Me.OnPropertyChanged("UpToICRightsByRightName")
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RightName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.RightDetailName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightDetailName As System.String
			Get
				Return MyBase.GetSystemString(ICRoleRightsMetadata.ColumnNames.RightDetailName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRoleRightsMetadata.ColumnNames.RightDetailName, value) Then
					Me._UpToICRightsByRightName = Nothing
					Me.OnPropertyChanged("UpToICRightsByRightName")
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RightDetailName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.RightValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightValue As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICRoleRightsMetadata.ColumnNames.RightValue)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICRoleRightsMetadata.ColumnNames.RightValue, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RightValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleRightsMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleRightsMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICRoleByRoleID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRoleID")
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleRightsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleRightsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleRightsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleRightsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleRightsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleRightsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleRightsMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleRightsMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICRoleRightsMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICRoleRightsMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleRightsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleRightsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_RoleRights.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleRightsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleRightsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRightsByRightName As ICRights
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRoleID As ICRole
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RoleRightID"
							Me.str().RoleRightID = CType(value, string)
												
						Case "RightName"
							Me.str().RightName = CType(value, string)
												
						Case "RightDetailName"
							Me.str().RightDetailName = CType(value, string)
												
						Case "RightValue"
							Me.str().RightValue = CType(value, string)
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RoleRightID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleRightID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RoleRightID)
							End If
						
						Case "RightValue"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.RightValue = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RightValue)
							End If
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.RoleID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.IsApproved)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleRightsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICRoleRights)
				Me.entity = entity
			End Sub				
		
	
			Public Property RoleRightID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleRightID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleRightID = Nothing
					Else
						entity.RoleRightID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightName As System.String 
				Get
					Dim data_ As System.String = entity.RightName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightName = Nothing
					Else
						entity.RightName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightDetailName As System.String 
				Get
					Dim data_ As System.String = entity.RightDetailName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightDetailName = Nothing
					Else
						entity.RightDetailName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightValue As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.RightValue
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightValue = Nothing
					Else
						entity.RightValue = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICRoleRights
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRoleRightsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICRoleRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRoleRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICRoleRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICRoleRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICRoleRightsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICRoleRightsCollection
		Inherits esEntityCollection(Of ICRoleRights)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRoleRightsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICRoleRightsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICRoleRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRoleRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICRoleRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICRoleRightsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICRoleRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICRoleRightsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICRoleRightsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICRoleRightsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICRoleRightsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RoleRightID" 
					Return Me.RoleRightID
				Case "RightName" 
					Return Me.RightName
				Case "RightDetailName" 
					Return Me.RightDetailName
				Case "RightValue" 
					Return Me.RightValue
				Case "RoleID" 
					Return Me.RoleID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RoleRightID As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.RoleRightID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RightName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.RightName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RightDetailName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.RightDetailName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RightValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.RightValue, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleRightsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICRoleRights 
		Inherits esICRoleRights
		
	
		#Region "UpToICRightsByRightName - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_RoleRights_IC_Rights
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRightsByRightName As ICRights
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRightsByRightName Is Nothing _
						 AndAlso Not RightName.Equals(Nothing)  AndAlso Not RightDetailName.Equals(Nothing)  Then
						
					Me._UpToICRightsByRightName = New ICRights()
					Me._UpToICRightsByRightName.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRightsByRightName", Me._UpToICRightsByRightName)
					Me._UpToICRightsByRightName.Query.Where(Me._UpToICRightsByRightName.Query.RightName = Me.RightName)
					Me._UpToICRightsByRightName.Query.Where(Me._UpToICRightsByRightName.Query.RightDetailName = Me.RightDetailName)
					Me._UpToICRightsByRightName.Query.Load()
				End If

				Return Me._UpToICRightsByRightName
			End Get
			
            Set(ByVal value As ICRights)
				Me.RemovePreSave("UpToICRightsByRightName")
				

				If value Is Nothing Then
				
					Me.RightName = Nothing
				
					Me.RightDetailName = Nothing
				
					Me._UpToICRightsByRightName = Nothing
				Else
				
					Me.RightName = value.RightName
					
					Me.RightDetailName = value.RightDetailName
					
					Me._UpToICRightsByRightName = value
					Me.SetPreSave("UpToICRightsByRightName", Me._UpToICRightsByRightName)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICRoleByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_RoleRights_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRoleID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRoleID = New ICRole()
					Me._UpToICRoleByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
					Me._UpToICRoleByRoleID.Query.Where(Me._UpToICRoleByRoleID.Query.RoleID = Me.RoleID)
					Me._UpToICRoleByRoleID.Query.Load()
				End If

				Return Me._UpToICRoleByRoleID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me._UpToICRoleByRoleID = Nothing
				Else
				
					Me.RoleID = value.RoleID
					
					Me._UpToICRoleByRoleID = value
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICRoleRightsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.RoleRightID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.RoleRightID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.RightName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.RightName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.RightDetailName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.RightDetailName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.RightValue, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.RightValue
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.RoleID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.RoleID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.ApprovedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.IsApproved, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.Creater, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleRightsMetadata.ColumnNames.CreationDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleRightsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICRoleRightsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RoleRightID As String = "RoleRightID"
			 Public Const RightName As String = "RightName"
			 Public Const RightDetailName As String = "RightDetailName"
			 Public Const RightValue As String = "RightValue"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RoleRightID As String = "RoleRightID"
			 Public Const RightName As String = "RightName"
			 Public Const RightDetailName As String = "RightDetailName"
			 Public Const RightValue As String = "RightValue"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICRoleRightsMetadata)
			
				If ICRoleRightsMetadata.mapDelegates Is Nothing Then
					ICRoleRightsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICRoleRightsMetadata._meta Is Nothing Then
					ICRoleRightsMetadata._meta = New ICRoleRightsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RoleRightID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RightName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RightDetailName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RightValue", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_RoleRights"
				meta.Destination = "IC_RoleRights"
				
				meta.spInsert = "proc_IC_RoleRightsInsert"
				meta.spUpdate = "proc_IC_RoleRightsUpdate"
				meta.spDelete = "proc_IC_RoleRightsDelete"
				meta.spLoadAll = "proc_IC_RoleRightsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_RoleRightsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICRoleRightsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
