
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:58 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_LimitForApproval' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICLimitForApproval))> _
	<XmlType("ICLimitForApproval")> _	
	Partial Public Class ICLimitForApproval 
		Inherits esICLimitForApproval
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICLimitForApproval()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal limitsForApprovalID As System.Int32)
			Dim obj As New ICLimitForApproval()
			obj.LimitsForApprovalID = limitsForApprovalID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal limitsForApprovalID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICLimitForApproval()
			obj.LimitsForApprovalID = limitsForApprovalID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICLimitForApprovalCollection")> _
	Partial Public Class ICLimitForApprovalCollection
		Inherits esICLimitForApprovalCollection
		Implements IEnumerable(Of ICLimitForApproval)
	
		Public Function FindByPrimaryKey(ByVal limitsForApprovalID As System.Int32) As ICLimitForApproval
			Return MyBase.SingleOrDefault(Function(e) e.LimitsForApprovalID.HasValue AndAlso e.LimitsForApprovalID.Value = limitsForApprovalID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICLimitForApproval))> _
		Public Class ICLimitForApprovalCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICLimitForApprovalCollection)
			
			Public Shared Widening Operator CType(packet As ICLimitForApprovalCollectionWCFPacket) As ICLimitForApprovalCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICLimitForApprovalCollection) As ICLimitForApprovalCollectionWCFPacket
				Return New ICLimitForApprovalCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICLimitForApprovalQuery 
		Inherits esICLimitForApprovalQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICLimitForApprovalQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICLimitForApprovalQuery) As String
			Return ICLimitForApprovalQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICLimitForApprovalQuery
			Return DirectCast(ICLimitForApprovalQuery.SerializeHelper.FromXml(query, GetType(ICLimitForApprovalQuery)), ICLimitForApprovalQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICLimitForApproval
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal limitsForApprovalID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(limitsForApprovalID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(limitsForApprovalID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal limitsForApprovalID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(limitsForApprovalID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(limitsForApprovalID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal limitsForApprovalID As System.Int32) As Boolean
		
			Dim query As New ICLimitForApprovalQuery()
			query.Where(query.LimitsForApprovalID = limitsForApprovalID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal limitsForApprovalID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("LimitsForApprovalID", limitsForApprovalID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_LimitForApproval.LimitsForApprovalID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LimitsForApprovalID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.LimitsForApprovalID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.LimitsForApprovalID, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForApprovalID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICLimitForApprovalMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICLimitForApprovalMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICLimitForApprovalMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICLimitForApprovalMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICLimitForApprovalMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICLimitForApprovalMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.LimitsForPrimaryApproval
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LimitsForPrimaryApproval As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.LimitsForPrimaryApproval)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.LimitsForPrimaryApproval, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForPrimaryApproval)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.LimitsForSecondaryApproval
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LimitsForSecondaryApproval As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.LimitsForSecondaryApproval)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.LimitsForSecondaryApproval, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForSecondaryApproval)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.IsPrimaryApprover
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrimaryApprover As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICLimitForApprovalMetadata.ColumnNames.IsPrimaryApprover)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICLimitForApprovalMetadata.ColumnNames.IsPrimaryApprover, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.IsPrimaryApprover)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.IsSecondaryApprover
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsSecondaryApprover As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICLimitForApprovalMetadata.ColumnNames.IsSecondaryApprover)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICLimitForApprovalMetadata.ColumnNames.IsSecondaryApprover, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.IsSecondaryApprover)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.UserSignatureImage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserSignatureImage As System.Byte()
			Get
				Return MyBase.GetSystemByteArray(ICLimitForApprovalMetadata.ColumnNames.UserSignatureImage)
			End Get
			
			Set(ByVal value As System.Byte())
				If MyBase.SetSystemByteArray(ICLimitForApprovalMetadata.ColumnNames.UserSignatureImage, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.UserSignatureImage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICLimitForApprovalMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICLimitForApprovalMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.FileType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileType As System.String
			Get
				Return MyBase.GetSystemString(ICLimitForApprovalMetadata.ColumnNames.FileType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICLimitForApprovalMetadata.ColumnNames.FileType, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.FileType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.FileSize
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileSize As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.FileSize)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.FileSize, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.FileSize)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICLimitForApprovalMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICLimitForApprovalMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICLimitForApprovalMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.BankPrimaryApprovalLimit
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankPrimaryApprovalLimit As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.BankPrimaryApprovalLimit)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.BankPrimaryApprovalLimit, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.BankPrimaryApprovalLimit)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_LimitForApproval.BankSecondaryApprovalLimit
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankSecondaryApprovalLimit As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.BankSecondaryApprovalLimit)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICLimitForApprovalMetadata.ColumnNames.BankSecondaryApprovalLimit, value) Then
					OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.BankSecondaryApprovalLimit)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "LimitsForApprovalID"
							Me.str().LimitsForApprovalID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "LimitsForPrimaryApproval"
							Me.str().LimitsForPrimaryApproval = CType(value, string)
												
						Case "LimitsForSecondaryApproval"
							Me.str().LimitsForSecondaryApproval = CType(value, string)
												
						Case "IsPrimaryApprover"
							Me.str().IsPrimaryApprover = CType(value, string)
												
						Case "IsSecondaryApprover"
							Me.str().IsSecondaryApprover = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "FileType"
							Me.str().FileType = CType(value, string)
												
						Case "FileSize"
							Me.str().FileSize = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "BankPrimaryApprovalLimit"
							Me.str().BankPrimaryApprovalLimit = CType(value, string)
												
						Case "BankSecondaryApprovalLimit"
							Me.str().BankSecondaryApprovalLimit = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "LimitsForApprovalID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.LimitsForApprovalID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForApprovalID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.UserID)
							End If
						
						Case "LimitsForPrimaryApproval"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.LimitsForPrimaryApproval = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForPrimaryApproval)
							End If
						
						Case "LimitsForSecondaryApproval"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.LimitsForSecondaryApproval = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.LimitsForSecondaryApproval)
							End If
						
						Case "IsPrimaryApprover"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrimaryApprover = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.IsPrimaryApprover)
							End If
						
						Case "IsSecondaryApprover"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsSecondaryApprover = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.IsSecondaryApprover)
							End If
						
						Case "UserSignatureImage"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Byte()" Then
								Me.UserSignatureImage = CType(value, System.Byte())
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.UserSignatureImage)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "FileSize"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileSize = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.FileSize)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.CreationDate)
							End If
						
						Case "BankPrimaryApprovalLimit"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.BankPrimaryApprovalLimit = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.BankPrimaryApprovalLimit)
							End If
						
						Case "BankSecondaryApprovalLimit"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.BankSecondaryApprovalLimit = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICLimitForApprovalMetadata.PropertyNames.BankSecondaryApprovalLimit)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICLimitForApproval)
				Me.entity = entity
			End Sub				
		
	
			Public Property LimitsForApprovalID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.LimitsForApprovalID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LimitsForApprovalID = Nothing
					Else
						entity.LimitsForApprovalID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property LimitsForPrimaryApproval As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.LimitsForPrimaryApproval
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LimitsForPrimaryApproval = Nothing
					Else
						entity.LimitsForPrimaryApproval = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property LimitsForSecondaryApproval As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.LimitsForSecondaryApproval
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LimitsForSecondaryApproval = Nothing
					Else
						entity.LimitsForSecondaryApproval = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrimaryApprover As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrimaryApprover
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrimaryApprover = Nothing
					Else
						entity.IsPrimaryApprover = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsSecondaryApprover As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsSecondaryApprover
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsSecondaryApprover = Nothing
					Else
						entity.IsSecondaryApprover = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileType As System.String 
				Get
					Dim data_ As System.String = entity.FileType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileType = Nothing
					Else
						entity.FileType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileSize As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileSize
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileSize = Nothing
					Else
						entity.FileSize = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankPrimaryApprovalLimit As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.BankPrimaryApprovalLimit
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankPrimaryApprovalLimit = Nothing
					Else
						entity.BankPrimaryApprovalLimit = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankSecondaryApprovalLimit As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.BankSecondaryApprovalLimit
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankSecondaryApprovalLimit = Nothing
					Else
						entity.BankSecondaryApprovalLimit = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICLimitForApproval
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICLimitForApprovalMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICLimitForApprovalQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICLimitForApprovalQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICLimitForApprovalQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICLimitForApprovalQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICLimitForApprovalQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICLimitForApprovalCollection
		Inherits esEntityCollection(Of ICLimitForApproval)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICLimitForApprovalMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICLimitForApprovalCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICLimitForApprovalQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICLimitForApprovalQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICLimitForApprovalQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICLimitForApprovalQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICLimitForApprovalQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICLimitForApprovalQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICLimitForApprovalQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICLimitForApprovalQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICLimitForApprovalMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "LimitsForApprovalID" 
					Return Me.LimitsForApprovalID
				Case "UserID" 
					Return Me.UserID
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "Currency" 
					Return Me.Currency
				Case "BranchCode" 
					Return Me.BranchCode
				Case "LimitsForPrimaryApproval" 
					Return Me.LimitsForPrimaryApproval
				Case "LimitsForSecondaryApproval" 
					Return Me.LimitsForSecondaryApproval
				Case "IsPrimaryApprover" 
					Return Me.IsPrimaryApprover
				Case "IsSecondaryApprover" 
					Return Me.IsSecondaryApprover
				Case "UserSignatureImage" 
					Return Me.UserSignatureImage
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "FileType" 
					Return Me.FileType
				Case "FileSize" 
					Return Me.FileSize
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "BankPrimaryApprovalLimit" 
					Return Me.BankPrimaryApprovalLimit
				Case "BankSecondaryApprovalLimit" 
					Return Me.BankSecondaryApprovalLimit
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property LimitsForApprovalID As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.LimitsForApprovalID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property LimitsForPrimaryApproval As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.LimitsForPrimaryApproval, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property LimitsForSecondaryApproval As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.LimitsForSecondaryApproval, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrimaryApprover As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.IsPrimaryApprover, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsSecondaryApprover As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.IsSecondaryApprover, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property UserSignatureImage As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.UserSignatureImage, esSystemType.ByteArray)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property FileType As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.FileType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileSize As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.FileSize, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property BankPrimaryApprovalLimit As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.BankPrimaryApprovalLimit, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property BankSecondaryApprovalLimit As esQueryItem
			Get
				Return New esQueryItem(Me, ICLimitForApprovalMetadata.ColumnNames.BankSecondaryApprovalLimit, esSystemType.Decimal)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICLimitForApproval 
		Inherits esICLimitForApproval
		
	
		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_LimitForApproval_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_LimitForApproval_IC_User2
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICLimitForApprovalMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.LimitsForApprovalID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.LimitsForApprovalID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.UserID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.AccountNumber, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.BranchCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.LimitsForPrimaryApproval, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.LimitsForPrimaryApproval
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.LimitsForSecondaryApproval, 6, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.LimitsForSecondaryApproval
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.IsPrimaryApprover, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.IsPrimaryApprover
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.IsSecondaryApprover, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.IsSecondaryApprover
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.UserSignatureImage, 9, GetType(System.Byte()), esSystemType.ByteArray)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.UserSignatureImage
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.CreatedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.CreatedDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.FileType, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.FileType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.FileSize, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.FileSize
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.Creater, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.CreationDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.BankPrimaryApprovalLimit, 16, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.BankPrimaryApprovalLimit
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICLimitForApprovalMetadata.ColumnNames.BankSecondaryApprovalLimit, 17, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICLimitForApprovalMetadata.PropertyNames.BankSecondaryApprovalLimit
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICLimitForApprovalMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const LimitsForApprovalID As String = "LimitsForApprovalID"
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const Currency As String = "Currency"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const LimitsForPrimaryApproval As String = "LimitsForPrimaryApproval"
			 Public Const LimitsForSecondaryApproval As String = "LimitsForSecondaryApproval"
			 Public Const IsPrimaryApprover As String = "IsPrimaryApprover"
			 Public Const IsSecondaryApprover As String = "IsSecondaryApprover"
			 Public Const UserSignatureImage As String = "UserSignatureImage"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const FileType As String = "FileType"
			 Public Const FileSize As String = "FileSize"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const BankPrimaryApprovalLimit As String = "BankPrimaryApprovalLimit"
			 Public Const BankSecondaryApprovalLimit As String = "BankSecondaryApprovalLimit"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const LimitsForApprovalID As String = "LimitsForApprovalID"
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const Currency As String = "Currency"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const LimitsForPrimaryApproval As String = "LimitsForPrimaryApproval"
			 Public Const LimitsForSecondaryApproval As String = "LimitsForSecondaryApproval"
			 Public Const IsPrimaryApprover As String = "IsPrimaryApprover"
			 Public Const IsSecondaryApprover As String = "IsSecondaryApprover"
			 Public Const UserSignatureImage As String = "UserSignatureImage"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const FileType As String = "FileType"
			 Public Const FileSize As String = "FileSize"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const BankPrimaryApprovalLimit As String = "BankPrimaryApprovalLimit"
			 Public Const BankSecondaryApprovalLimit As String = "BankSecondaryApprovalLimit"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICLimitForApprovalMetadata)
			
				If ICLimitForApprovalMetadata.mapDelegates Is Nothing Then
					ICLimitForApprovalMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICLimitForApprovalMetadata._meta Is Nothing Then
					ICLimitForApprovalMetadata._meta = New ICLimitForApprovalMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("LimitsForApprovalID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("LimitsForPrimaryApproval", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("LimitsForSecondaryApproval", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("IsPrimaryApprover", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsSecondaryApprover", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("UserSignatureImage", new esTypeMap("varbinary", "System.Byte()"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("FileType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileSize", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("BankPrimaryApprovalLimit", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("BankSecondaryApprovalLimit", new esTypeMap("numeric", "System.Decimal"))			
				
				
				 
				meta.Source = "IC_LimitForApproval"
				meta.Destination = "IC_LimitForApproval"
				
				meta.spInsert = "proc_IC_LimitForApprovalInsert"
				meta.spUpdate = "proc_IC_LimitForApprovalUpdate"
				meta.spDelete = "proc_IC_LimitForApprovalDelete"
				meta.spLoadAll = "proc_IC_LimitForApprovalLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_LimitForApprovalLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICLimitForApprovalMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
