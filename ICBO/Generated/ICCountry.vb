
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 1:15:04 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Country' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCountry))> _
	<XmlType("ICCountry")> _	
	Partial Public Class ICCountry 
		Inherits esICCountry
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCountry()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal countryCode As System.Int32)
			Dim obj As New ICCountry()
			obj.CountryCode = countryCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal countryCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCountry()
			obj.CountryCode = countryCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCountryCollection")> _
	Partial Public Class ICCountryCollection
		Inherits esICCountryCollection
		Implements IEnumerable(Of ICCountry)
	
		Public Function FindByPrimaryKey(ByVal countryCode As System.Int32) As ICCountry
			Return MyBase.SingleOrDefault(Function(e) e.CountryCode.HasValue AndAlso e.CountryCode.Value = countryCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCountry))> _
		Public Class ICCountryCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCountryCollection)
			
			Public Shared Widening Operator CType(packet As ICCountryCollectionWCFPacket) As ICCountryCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCountryCollection) As ICCountryCollectionWCFPacket
				Return New ICCountryCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCountryQuery 
		Inherits esICCountryQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCountryQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCountryQuery) As String
			Return ICCountryQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCountryQuery
			Return DirectCast(ICCountryQuery.SerializeHelper.FromXml(query, GetType(ICCountryQuery)), ICCountryQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCountry
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal countryCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(countryCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(countryCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal countryCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(countryCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(countryCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal countryCode As System.Int32) As Boolean
		
			Dim query As New ICCountryQuery()
			query.Where(query.CountryCode = countryCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal countryCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CountryCode", countryCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Country.CountryCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CountryCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCountryMetadata.ColumnNames.CountryCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCountryMetadata.ColumnNames.CountryCode, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.CountryCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.CountryName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CountryName As System.String
			Get
				Return MyBase.GetSystemString(ICCountryMetadata.ColumnNames.CountryName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCountryMetadata.ColumnNames.CountryName, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.CountryName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCountryMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCountryMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCountryMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCountryMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCountryMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCountryMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCountryMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCountryMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCountryMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCountryMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCountryMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCountryMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCountryMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCountryMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCountryMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCountryMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Country.DisplayCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisplayCode As System.String
			Get
				Return MyBase.GetSystemString(ICCountryMetadata.ColumnNames.DisplayCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCountryMetadata.ColumnNames.DisplayCode, value) Then
					OnPropertyChanged(ICCountryMetadata.PropertyNames.DisplayCode)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CountryCode"
							Me.str().CountryCode = CType(value, string)
												
						Case "CountryName"
							Me.str().CountryName = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "DisplayCode"
							Me.str().DisplayCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CountryCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CountryCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.CountryCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCountryMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCountry)
				Me.entity = entity
			End Sub				
		
	
			Public Property CountryCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CountryCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CountryCode = Nothing
					Else
						entity.CountryCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CountryName As System.String 
				Get
					Dim data_ As System.String = entity.CountryName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CountryName = Nothing
					Else
						entity.CountryName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisplayCode As System.String 
				Get
					Dim data_ As System.String = entity.DisplayCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisplayCode = Nothing
					Else
						entity.DisplayCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCountry
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCountryMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCountryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCountryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCountryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCountryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCountryQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCountryCollection
		Inherits esEntityCollection(Of ICCountry)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCountryMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCountryCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCountryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCountryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCountryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCountryQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCountryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCountryQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCountryQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCountryQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCountryMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CountryCode" 
					Return Me.CountryCode
				Case "CountryName" 
					Return Me.CountryName
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "DisplayCode" 
					Return Me.DisplayCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CountryCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.CountryCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CountryName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.CountryName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DisplayCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCountryMetadata.ColumnNames.DisplayCode, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCountry 
		Inherits esICCountry
		
	
		#Region "ICProvinceCollectionByCountryCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICProvinceCollectionByCountryCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCountry.ICProvinceCollectionByCountryCode_Delegate)
				map.PropertyName = "ICProvinceCollectionByCountryCode"
				map.MyColumnName = "CountryCode"
				map.ParentColumnName = "CountryCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICProvinceCollectionByCountryCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCountryQuery(data.NextAlias())
			
			Dim mee As ICProvinceQuery = If(data.You IsNot Nothing, TryCast(data.You, ICProvinceQuery), New ICProvinceQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CountryCode = mee.CountryCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Province_IC_Country
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICProvinceCollectionByCountryCode As ICProvinceCollection 
		
			Get
				If Me._ICProvinceCollectionByCountryCode Is Nothing Then
					Me._ICProvinceCollectionByCountryCode = New ICProvinceCollection()
					Me._ICProvinceCollectionByCountryCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICProvinceCollectionByCountryCode", Me._ICProvinceCollectionByCountryCode)
				
					If Not Me.CountryCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICProvinceCollectionByCountryCode.Query.Where(Me._ICProvinceCollectionByCountryCode.Query.CountryCode = Me.CountryCode)
							Me._ICProvinceCollectionByCountryCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICProvinceCollectionByCountryCode.fks.Add(ICProvinceMetadata.ColumnNames.CountryCode, Me.CountryCode)
					End If
				End If

				Return Me._ICProvinceCollectionByCountryCode
			End Get
			
			Set(ByVal value As ICProvinceCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICProvinceCollectionByCountryCode Is Nothing Then

					Me.RemovePostSave("ICProvinceCollectionByCountryCode")
					Me._ICProvinceCollectionByCountryCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICProvinceCollectionByCountryCode As ICProvinceCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICProvinceCollectionByCountryCode"
					coll = Me.ICProvinceCollectionByCountryCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICProvinceCollectionByCountryCode", GetType(ICProvinceCollection), New ICProvince()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICProvinceCollectionByCountryCode Is Nothing Then
				Apply(Me._ICProvinceCollectionByCountryCode, "CountryCode", Me.CountryCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCountryMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.CountryCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCountryMetadata.PropertyNames.CountryCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.CountryName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCountryMetadata.PropertyNames.CountryName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.IsActive, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCountryMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.CreateBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCountryMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.CreateDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCountryMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.ApprovedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCountryMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.IsApproved, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCountryMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.ApprovedOn, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCountryMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.Creater, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCountryMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.CreationDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCountryMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCountryMetadata.ColumnNames.DisplayCode, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCountryMetadata.PropertyNames.DisplayCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCountryMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CountryCode As String = "CountryCode"
			 Public Const CountryName As String = "CountryName"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CountryCode As String = "CountryCode"
			 Public Const CountryName As String = "CountryName"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCountryMetadata)
			
				If ICCountryMetadata.mapDelegates Is Nothing Then
					ICCountryMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCountryMetadata._meta Is Nothing Then
					ICCountryMetadata._meta = New ICCountryMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CountryCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CountryName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DisplayCode", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_Country"
				meta.Destination = "IC_Country"
				
				meta.spInsert = "proc_IC_CountryInsert"
				meta.spUpdate = "proc_IC_CountryUpdate"
				meta.spDelete = "proc_IC_CountryDelete"
				meta.spLoadAll = "proc_IC_CountryLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CountryLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCountryMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
