
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionProductType' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionProductType))> _
	<XmlType("ICCollectionProductType")> _	
	Partial Public Class ICCollectionProductType 
		Inherits esICCollectionProductType
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionProductType()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal productTypeCode As System.String)
			Dim obj As New ICCollectionProductType()
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal productTypeCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionProductType()
			obj.ProductTypeCode = productTypeCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionProductTypeCollection")> _
	Partial Public Class ICCollectionProductTypeCollection
		Inherits esICCollectionProductTypeCollection
		Implements IEnumerable(Of ICCollectionProductType)
	
		Public Function FindByPrimaryKey(ByVal productTypeCode As System.String) As ICCollectionProductType
			Return MyBase.SingleOrDefault(Function(e) e.ProductTypeCode = productTypeCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionProductType))> _
		Public Class ICCollectionProductTypeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionProductTypeCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionProductTypeCollectionWCFPacket) As ICCollectionProductTypeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionProductTypeCollection) As ICCollectionProductTypeCollectionWCFPacket
				Return New ICCollectionProductTypeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionProductTypeQuery 
		Inherits esICCollectionProductTypeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionProductTypeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionProductTypeQuery) As String
			Return ICCollectionProductTypeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionProductTypeQuery
			Return DirectCast(ICCollectionProductTypeQuery.SerializeHelper.FromXml(query, GetType(ICCollectionProductTypeQuery)), ICCollectionProductTypeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionProductType
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal productTypeCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(productTypeCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal productTypeCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(productTypeCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(productTypeCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal productTypeCode As System.String) As Boolean
		
			Dim query As New ICCollectionProductTypeQuery()
			query.Where(query.ProductTypeCode = productTypeCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal productTypeCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ProductTypeCode", productTypeCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionProductType.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.ProductTypeName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeName, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ProductTypeName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.DisbursementMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursementMode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionProductTypeMetadata.ColumnNames.DisbursementMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionProductTypeMetadata.ColumnNames.DisbursementMode, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.DisbursementMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.FloatDays
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FloatDays As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.FloatDays)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.FloatDays, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.FloatDays)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionProductTypeMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionProductTypeMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionProductTypeMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionProductTypeMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionProductTypeMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionProductTypeMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionProductTypeMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionProductType.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionProductTypeMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionProductTypeMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "ProductTypeName"
							Me.str().ProductTypeName = CType(value, string)
												
						Case "DisbursementMode"
							Me.str().DisbursementMode = CType(value, string)
												
						Case "FloatDays"
							Me.str().FloatDays = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FloatDays"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FloatDays = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.FloatDays)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionProductTypeMetadata.PropertyNames.ApprovedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionProductType)
				Me.entity = entity
			End Sub				
		
	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeName As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeName = Nothing
					Else
						entity.ProductTypeName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursementMode As System.String 
				Get
					Dim data_ As System.String = entity.DisbursementMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursementMode = Nothing
					Else
						entity.DisbursementMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FloatDays As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FloatDays
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FloatDays = Nothing
					Else
						entity.FloatDays = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionProductType
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionProductTypeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionProductTypeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionProductTypeCollection
		Inherits esEntityCollection(Of ICCollectionProductType)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionProductTypeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionProductTypeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionProductTypeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionProductTypeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionProductTypeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionProductTypeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionProductTypeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionProductTypeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionProductTypeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionProductTypeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionProductTypeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "ProductTypeName" 
					Return Me.ProductTypeName
				Case "DisbursementMode" 
					Return Me.DisbursementMode
				Case "FloatDays" 
					Return Me.FloatDays
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.ProductTypeName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursementMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.DisbursementMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FloatDays As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.FloatDays, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionProductTypeMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionProductType 
		Inherits esICCollectionProductType
		
	
		#Region "UpToICCollectionAccountsCollectionNatureCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionProductType
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICCollectionAccountsCollectionNatureCollection As ICCollectionAccountsCollectionNatureCollection
		
			Get
				If Me._UpToICCollectionAccountsCollectionNatureCollection Is Nothing Then
					Me._UpToICCollectionAccountsCollectionNatureCollection = New ICCollectionAccountsCollectionNatureCollection()
					Me._UpToICCollectionAccountsCollectionNatureCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICCollectionAccountsCollectionNatureCollection", Me._UpToICCollectionAccountsCollectionNatureCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.ProductTypeCode.Equals(Nothing) Then 
				
						Dim m As New ICCollectionAccountsCollectionNatureQuery("m")
						Dim j As New ICCollectionAccountsCollectionNatureProductTypeQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.AccountNumber = j.AccountNumber And m.BranchCode = j.BranchCode And m.Currency = j.Currency And m.CollectionNatureCode = j.CollectionNatureCode)
                        m.Where(j.ProductTypeCode = Me.ProductTypeCode)

						Me._UpToICCollectionAccountsCollectionNatureCollection.Load(m)

					End If
				End If

				Return Me._UpToICCollectionAccountsCollectionNatureCollection
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollectionNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICCollectionAccountsCollectionNatureCollection Is Nothing Then

					Me.RemovePostSave("UpToICCollectionAccountsCollectionNatureCollection")
					Me._UpToICCollectionAccountsCollectionNatureCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionProductType
		''' </summary>
		Public Sub AssociateICCollectionAccountsCollectionNatureCollection(entity As ICCollectionAccountsCollectionNature)
			If Me._ICCollectionAccountsCollectionNatureProductTypeCollection Is Nothing Then
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection = New ICCollectionAccountsCollectionNatureProductTypeCollection()
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollection", Me._ICCollectionAccountsCollectionNatureProductTypeCollection)
			End If
			
			Dim obj As ICCollectionAccountsCollectionNatureProductType = Me._ICCollectionAccountsCollectionNatureProductTypeCollection.AddNew()
			obj.ProductTypeCode = Me.ProductTypeCode
			obj.AccountNumber = entity.AccountNumber
			obj.BranchCode = entity.BranchCode
			obj.Currency = entity.Currency
			obj.CollectionNatureCode = entity.CollectionNatureCode			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionProductType
		''' </summary>
		Public Sub DissociateICCollectionAccountsCollectionNatureCollection(entity As ICCollectionAccountsCollectionNature)
			If Me._ICCollectionAccountsCollectionNatureProductTypeCollection Is Nothing Then
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection = new ICCollectionAccountsCollectionNatureProductTypeCollection()
				Me._ICCollectionAccountsCollectionNatureProductTypeCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollection", Me._ICCollectionAccountsCollectionNatureProductTypeCollection)
			End If

			Dim obj As ICCollectionAccountsCollectionNatureProductType = Me._ICCollectionAccountsCollectionNatureProductTypeCollection.AddNew()
			obj.ProductTypeCode = Me.ProductTypeCode
            obj.AccountNumber = entity.AccountNumber
            obj.BranchCode = entity.BranchCode
            obj.Currency = entity.Currency
            obj.CollectionNatureCode = entity.CollectionNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICCollectionAccountsCollectionNatureCollection As ICCollectionAccountsCollectionNatureCollection
		Private _ICCollectionAccountsCollectionNatureProductTypeCollection As ICCollectionAccountsCollectionNatureProductTypeCollection
		#End Region

		#Region "ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionProductType.ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode"
				map.MyColumnName = "ProductTypeCode"
				map.ParentColumnName = "ProductTypeCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionProductTypeQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsCollectionNatureProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsCollectionNatureProductTypeQuery), New ICCollectionAccountsCollectionNatureProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ProductTypeCode = mee.ProductTypeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNatureProductType_IC_CollectionProductType
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode As ICCollectionAccountsCollectionNatureProductTypeCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode Is Nothing Then
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode = New ICCollectionAccountsCollectionNatureProductTypeCollection()
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode", Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode)
				
					If Not Me.ProductTypeCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode.Query.Where(Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
							Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode.fks.Add(ICCollectionAccountsCollectionNatureProductTypeMetadata.ColumnNames.ProductTypeCode, Me.ProductTypeCode)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollectionNatureProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode")
					Me._ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode As ICCollectionAccountsCollectionNatureProductTypeCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode"
					coll = Me.ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionNatureProductTypeCollectionByProductTypeCode", GetType(ICCollectionAccountsCollectionNatureProductTypeCollection), New ICCollectionAccountsCollectionNatureProductType()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionProductTypeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.ProductTypeCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.ProductTypeName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.ProductTypeName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.DisbursementMode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.DisbursementMode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.FloatDays, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.FloatDays
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.ApprovedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.IsApproved, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionProductTypeMetadata.ColumnNames.ApprovedOn, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionProductTypeMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionProductTypeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ProductTypeName As String = "ProductTypeName"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const FloatDays As String = "FloatDays"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const ProductTypeName As String = "ProductTypeName"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const FloatDays As String = "FloatDays"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionProductTypeMetadata)
			
				If ICCollectionProductTypeMetadata.mapDelegates Is Nothing Then
					ICCollectionProductTypeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionProductTypeMetadata._meta Is Nothing Then
					ICCollectionProductTypeMetadata._meta = New ICCollectionProductTypeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbursementMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FloatDays", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_CollectionProductType"
				meta.Destination = "IC_CollectionProductType"
				
				meta.spInsert = "proc_IC_CollectionProductTypeInsert"
				meta.spUpdate = "proc_IC_CollectionProductTypeUpdate"
				meta.spDelete = "proc_IC_CollectionProductTypeDelete"
				meta.spLoadAll = "proc_IC_CollectionProductTypeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionProductTypeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionProductTypeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
