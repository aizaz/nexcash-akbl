
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:40 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionsStatus' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionsStatus))> _
	<XmlType("ICCollectionsStatus")> _	
	Partial Public Class ICCollectionsStatus 
		Inherits esICCollectionsStatus
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionsStatus()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionStatusID As System.Int32)
			Dim obj As New ICCollectionsStatus()
			obj.CollectionStatusID = collectionStatusID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionStatusID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionsStatus()
			obj.CollectionStatusID = collectionStatusID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionsStatusCollection")> _
	Partial Public Class ICCollectionsStatusCollection
		Inherits esICCollectionsStatusCollection
		Implements IEnumerable(Of ICCollectionsStatus)
	
		Public Function FindByPrimaryKey(ByVal collectionStatusID As System.Int32) As ICCollectionsStatus
			Return MyBase.SingleOrDefault(Function(e) e.CollectionStatusID.HasValue AndAlso e.CollectionStatusID.Value = collectionStatusID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionsStatus))> _
		Public Class ICCollectionsStatusCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionsStatusCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionsStatusCollectionWCFPacket) As ICCollectionsStatusCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionsStatusCollection) As ICCollectionsStatusCollectionWCFPacket
				Return New ICCollectionsStatusCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionsStatusQuery 
		Inherits esICCollectionsStatusQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionsStatusQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionsStatusQuery) As String
			Return ICCollectionsStatusQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionsStatusQuery
			Return DirectCast(ICCollectionsStatusQuery.SerializeHelper.FromXml(query, GetType(ICCollectionsStatusQuery)), ICCollectionsStatusQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionsStatus
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionStatusID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionStatusID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionStatusID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionStatusID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionStatusID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionStatusID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionStatusID As System.Int32) As Boolean
		
			Dim query As New ICCollectionsStatusQuery()
			query.Where(query.CollectionStatusID = collectionStatusID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionStatusID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionStatusID", collectionStatusID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionsStatus.CollectionStatusID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionStatusID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusID, value) Then
					OnPropertyChanged(ICCollectionsStatusMetadata.PropertyNames.CollectionStatusID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionsStatus.CollectionStatusName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionStatusName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusName, value) Then
					OnPropertyChanged(ICCollectionsStatusMetadata.PropertyNames.CollectionStatusName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionsStatus.CollectionStatusHeader
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionStatusHeader As System.Byte()
			Get
				Return MyBase.GetSystemByteArray(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusHeader)
			End Get
			
			Set(ByVal value As System.Byte())
				If MyBase.SetSystemByteArray(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusHeader, value) Then
					OnPropertyChanged(ICCollectionsStatusMetadata.PropertyNames.CollectionStatusHeader)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionStatusID"
							Me.str().CollectionStatusID = CType(value, string)
												
						Case "CollectionStatusName"
							Me.str().CollectionStatusName = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CollectionStatusID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionStatusID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsStatusMetadata.PropertyNames.CollectionStatusID)
							End If
						
						Case "CollectionStatusHeader"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Byte()" Then
								Me.CollectionStatusHeader = CType(value, System.Byte())
								OnPropertyChanged(ICCollectionsStatusMetadata.PropertyNames.CollectionStatusHeader)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionsStatus)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionStatusID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionStatusID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionStatusID = Nothing
					Else
						entity.CollectionStatusID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionStatusName As System.String 
				Get
					Dim data_ As System.String = entity.CollectionStatusName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionStatusName = Nothing
					Else
						entity.CollectionStatusName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionsStatus
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionsStatusMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionsStatusQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionsStatusQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionsStatusQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionsStatusQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionsStatusQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionsStatusCollection
		Inherits esEntityCollection(Of ICCollectionsStatus)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionsStatusMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionsStatusCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionsStatusQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionsStatusQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionsStatusQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionsStatusQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionsStatusQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionsStatusQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionsStatusQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionsStatusQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionsStatusMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionStatusID" 
					Return Me.CollectionStatusID
				Case "CollectionStatusName" 
					Return Me.CollectionStatusName
				Case "CollectionStatusHeader" 
					Return Me.CollectionStatusHeader
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionStatusID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsStatusMetadata.ColumnNames.CollectionStatusID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionStatusName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsStatusMetadata.ColumnNames.CollectionStatusName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionStatusHeader As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsStatusMetadata.ColumnNames.CollectionStatusHeader, esSystemType.ByteArray)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionsStatus 
		Inherits esICCollectionsStatus
		
	
		#Region "ICCollectionsCollectionByStatus - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionsCollectionByStatus() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionsStatus.ICCollectionsCollectionByStatus_Delegate)
				map.PropertyName = "ICCollectionsCollectionByStatus"
				map.MyColumnName = "Status"
				map.ParentColumnName = "CollectionStatusID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionsCollectionByStatus_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionsStatusQuery(data.NextAlias())
			
			Dim mee As ICCollectionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionsQuery), New ICCollectionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CollectionStatusID = mee.Status)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionsStatus
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionsCollectionByStatus As ICCollectionsCollection 
		
			Get
				If Me._ICCollectionsCollectionByStatus Is Nothing Then
					Me._ICCollectionsCollectionByStatus = New ICCollectionsCollection()
					Me._ICCollectionsCollectionByStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionsCollectionByStatus", Me._ICCollectionsCollectionByStatus)
				
					If Not Me.CollectionStatusID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionsCollectionByStatus.Query.Where(Me._ICCollectionsCollectionByStatus.Query.Status = Me.CollectionStatusID)
							Me._ICCollectionsCollectionByStatus.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionsCollectionByStatus.fks.Add(ICCollectionsMetadata.ColumnNames.Status, Me.CollectionStatusID)
					End If
				End If

				Return Me._ICCollectionsCollectionByStatus
			End Get
			
			Set(ByVal value As ICCollectionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionsCollectionByStatus Is Nothing Then

					Me.RemovePostSave("ICCollectionsCollectionByStatus")
					Me._ICCollectionsCollectionByStatus = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionsCollectionByStatus As ICCollectionsCollection
		#End Region

		#Region "ICCollectionsCollectionByLastStatus - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionsCollectionByLastStatus() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionsStatus.ICCollectionsCollectionByLastStatus_Delegate)
				map.PropertyName = "ICCollectionsCollectionByLastStatus"
				map.MyColumnName = "LastStatus"
				map.ParentColumnName = "CollectionStatusID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionsCollectionByLastStatus_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionsStatusQuery(data.NextAlias())
			
			Dim mee As ICCollectionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionsQuery), New ICCollectionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CollectionStatusID = mee.LastStatus)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionsStatus1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionsCollectionByLastStatus As ICCollectionsCollection 
		
			Get
				If Me._ICCollectionsCollectionByLastStatus Is Nothing Then
					Me._ICCollectionsCollectionByLastStatus = New ICCollectionsCollection()
					Me._ICCollectionsCollectionByLastStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionsCollectionByLastStatus", Me._ICCollectionsCollectionByLastStatus)
				
					If Not Me.CollectionStatusID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionsCollectionByLastStatus.Query.Where(Me._ICCollectionsCollectionByLastStatus.Query.LastStatus = Me.CollectionStatusID)
							Me._ICCollectionsCollectionByLastStatus.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionsCollectionByLastStatus.fks.Add(ICCollectionsMetadata.ColumnNames.LastStatus, Me.CollectionStatusID)
					End If
				End If

				Return Me._ICCollectionsCollectionByLastStatus
			End Get
			
			Set(ByVal value As ICCollectionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionsCollectionByLastStatus Is Nothing Then

					Me.RemovePostSave("ICCollectionsCollectionByLastStatus")
					Me._ICCollectionsCollectionByLastStatus = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionsCollectionByLastStatus As ICCollectionsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionsCollectionByStatus"
					coll = Me.ICCollectionsCollectionByStatus
					Exit Select
				Case "ICCollectionsCollectionByLastStatus"
					coll = Me.ICCollectionsCollectionByLastStatus
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionsCollectionByStatus", GetType(ICCollectionsCollection), New ICCollections()))
			props.Add(new esPropertyDescriptor(Me, "ICCollectionsCollectionByLastStatus", GetType(ICCollectionsCollection), New ICCollections()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCollectionsCollectionByStatus Is Nothing Then
				Apply(Me._ICCollectionsCollectionByStatus, "Status", Me.CollectionStatusID)
			End If
			
			If Not Me._ICCollectionsCollectionByLastStatus Is Nothing Then
				Apply(Me._ICCollectionsCollectionByLastStatus, "LastStatus", Me.CollectionStatusID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionsStatusMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsStatusMetadata.PropertyNames.CollectionStatusID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsStatusMetadata.PropertyNames.CollectionStatusName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsStatusMetadata.ColumnNames.CollectionStatusHeader, 2, GetType(System.Byte()), esSystemType.ByteArray)	
			c.PropertyName = ICCollectionsStatusMetadata.PropertyNames.CollectionStatusHeader
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionsStatusMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionStatusID As String = "CollectionStatusID"
			 Public Const CollectionStatusName As String = "CollectionStatusName"
			 Public Const CollectionStatusHeader As String = "CollectionStatusHeader"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionStatusID As String = "CollectionStatusID"
			 Public Const CollectionStatusName As String = "CollectionStatusName"
			 Public Const CollectionStatusHeader As String = "CollectionStatusHeader"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionsStatusMetadata)
			
				If ICCollectionsStatusMetadata.mapDelegates Is Nothing Then
					ICCollectionsStatusMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionsStatusMetadata._meta Is Nothing Then
					ICCollectionsStatusMetadata._meta = New ICCollectionsStatusMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionStatusID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionStatusName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionStatusHeader", new esTypeMap("varbinary", "System.Byte()"))			
				
				
				 
				meta.Source = "IC_CollectionsStatus"
				meta.Destination = "IC_CollectionsStatus"
				
				meta.spInsert = "proc_IC_CollectionsStatusInsert"
				meta.spUpdate = "proc_IC_CollectionsStatusUpdate"
				meta.spDelete = "proc_IC_CollectionsStatusDelete"
				meta.spLoadAll = "proc_IC_CollectionsStatusLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionsStatusLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionsStatusMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
