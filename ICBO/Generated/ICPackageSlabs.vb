
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PackageSlabs' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPackageSlabs))> _
	<XmlType("ICPackageSlabs")> _	
	Partial Public Class ICPackageSlabs 
		Inherits esICPackageSlabs
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPackageSlabs()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal slabId As System.Int32)
			Dim obj As New ICPackageSlabs()
			obj.SlabId = slabId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal slabId As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPackageSlabs()
			obj.SlabId = slabId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPackageSlabsCollection")> _
	Partial Public Class ICPackageSlabsCollection
		Inherits esICPackageSlabsCollection
		Implements IEnumerable(Of ICPackageSlabs)
	
		Public Function FindByPrimaryKey(ByVal slabId As System.Int32) As ICPackageSlabs
			Return MyBase.SingleOrDefault(Function(e) e.SlabId.HasValue AndAlso e.SlabId.Value = slabId)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPackageSlabs))> _
		Public Class ICPackageSlabsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPackageSlabsCollection)
			
			Public Shared Widening Operator CType(packet As ICPackageSlabsCollectionWCFPacket) As ICPackageSlabsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPackageSlabsCollection) As ICPackageSlabsCollectionWCFPacket
				Return New ICPackageSlabsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPackageSlabsQuery 
		Inherits esICPackageSlabsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPackageSlabsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPackageSlabsQuery) As String
			Return ICPackageSlabsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPackageSlabsQuery
			Return DirectCast(ICPackageSlabsQuery.SerializeHelper.FromXml(query, GetType(ICPackageSlabsQuery)), ICPackageSlabsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPackageSlabs
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal slabId As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(slabId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(slabId)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal slabId As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(slabId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(slabId)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal slabId As System.Int32) As Boolean
		
			Dim query As New ICPackageSlabsQuery()
			query.Where(query.SlabId = slabId)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal slabId As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SlabId", slabId)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PackageSlabs.SlabId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SlabId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageSlabsMetadata.ColumnNames.SlabId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageSlabsMetadata.ColumnNames.SlabId, value) Then
					OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.SlabId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageSlabs.Min
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Min As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICPackageSlabsMetadata.ColumnNames.Min)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICPackageSlabsMetadata.ColumnNames.Min, value) Then
					OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Min)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageSlabs.Max
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Max As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICPackageSlabsMetadata.ColumnNames.Max)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICPackageSlabsMetadata.ColumnNames.Max, value) Then
					OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Max)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageSlabs.Percentage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Percentage As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageSlabsMetadata.ColumnNames.Percentage)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageSlabsMetadata.ColumnNames.Percentage, value) Then
					OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Percentage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageSlabs.PackageID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageSlabsMetadata.ColumnNames.PackageID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageSlabsMetadata.ColumnNames.PackageID, value) Then
					Me._UpToICPackageByPackageID = Nothing
					Me.OnPropertyChanged("UpToICPackageByPackageID")
					OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.PackageID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPackageByPackageID As ICPackage
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SlabId"
							Me.str().SlabId = CType(value, string)
												
						Case "Min"
							Me.str().Min = CType(value, string)
												
						Case "Max"
							Me.str().Max = CType(value, string)
												
						Case "Percentage"
							Me.str().Percentage = CType(value, string)
												
						Case "PackageID"
							Me.str().PackageID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SlabId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SlabId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.SlabId)
							End If
						
						Case "Min"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.Min = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Min)
							End If
						
						Case "Max"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.Max = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Max)
							End If
						
						Case "Percentage"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Percentage = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.Percentage)
							End If
						
						Case "PackageID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PackageID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageSlabsMetadata.PropertyNames.PackageID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPackageSlabs)
				Me.entity = entity
			End Sub				
		
	
			Public Property SlabId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SlabId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SlabId = Nothing
					Else
						entity.SlabId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Min As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.Min
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Min = Nothing
					Else
						entity.Min = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  	
			Public Property Max As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.Max
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Max = Nothing
					Else
						entity.Max = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  	
			Public Property Percentage As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Percentage
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Percentage = Nothing
					Else
						entity.Percentage = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackageID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PackageID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageID = Nothing
					Else
						entity.PackageID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPackageSlabs
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageSlabsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPackageSlabsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageSlabsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPackageSlabsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPackageSlabsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPackageSlabsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPackageSlabsCollection
		Inherits esEntityCollection(Of ICPackageSlabs)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageSlabsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPackageSlabsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPackageSlabsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageSlabsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPackageSlabsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPackageSlabsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPackageSlabsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPackageSlabsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPackageSlabsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPackageSlabsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPackageSlabsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SlabId" 
					Return Me.SlabId
				Case "Min" 
					Return Me.Min
				Case "Max" 
					Return Me.Max
				Case "Percentage" 
					Return Me.Percentage
				Case "PackageID" 
					Return Me.PackageID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SlabId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageSlabsMetadata.ColumnNames.SlabId, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Min As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageSlabsMetadata.ColumnNames.Min, esSystemType.Int64)
			End Get
		End Property 
		
		Public ReadOnly Property Max As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageSlabsMetadata.ColumnNames.Max, esSystemType.Int64)
			End Get
		End Property 
		
		Public ReadOnly Property Percentage As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageSlabsMetadata.ColumnNames.Percentage, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property PackageID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageSlabsMetadata.ColumnNames.PackageID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPackageSlabs 
		Inherits esICPackageSlabs
		
	
		#Region "UpToICPackageByPackageID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageSlabs_IC_Package
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPackageByPackageID As ICPackage
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPackageByPackageID Is Nothing _
						 AndAlso Not PackageID.Equals(Nothing)  Then
						
					Me._UpToICPackageByPackageID = New ICPackage()
					Me._UpToICPackageByPackageID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPackageByPackageID", Me._UpToICPackageByPackageID)
					Me._UpToICPackageByPackageID.Query.Where(Me._UpToICPackageByPackageID.Query.PackageID = Me.PackageID)
					Me._UpToICPackageByPackageID.Query.Load()
				End If

				Return Me._UpToICPackageByPackageID
			End Get
			
            Set(ByVal value As ICPackage)
				Me.RemovePreSave("UpToICPackageByPackageID")
				

				If value Is Nothing Then
				
					Me.PackageID = Nothing
				
					Me._UpToICPackageByPackageID = Nothing
				Else
				
					Me.PackageID = value.PackageID
					
					Me._UpToICPackageByPackageID = value
					Me.SetPreSave("UpToICPackageByPackageID", Me._UpToICPackageByPackageID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICPackageByPackageID Is Nothing Then
				Me.PackageID = Me._UpToICPackageByPackageID.PackageID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPackageSlabsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPackageSlabsMetadata.ColumnNames.SlabId, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageSlabsMetadata.PropertyNames.SlabId
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageSlabsMetadata.ColumnNames.Min, 1, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICPackageSlabsMetadata.PropertyNames.Min
			c.NumericPrecision = 19
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageSlabsMetadata.ColumnNames.Max, 2, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICPackageSlabsMetadata.PropertyNames.Max
			c.NumericPrecision = 19
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageSlabsMetadata.ColumnNames.Percentage, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageSlabsMetadata.PropertyNames.Percentage
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageSlabsMetadata.ColumnNames.PackageID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageSlabsMetadata.PropertyNames.PackageID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPackageSlabsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SlabId As String = "SlabId"
			 Public Const Min As String = "Min"
			 Public Const Max As String = "Max"
			 Public Const Percentage As String = "Percentage"
			 Public Const PackageID As String = "PackageID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SlabId As String = "SlabId"
			 Public Const Min As String = "Min"
			 Public Const Max As String = "Max"
			 Public Const Percentage As String = "Percentage"
			 Public Const PackageID As String = "PackageID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPackageSlabsMetadata)
			
				If ICPackageSlabsMetadata.mapDelegates Is Nothing Then
					ICPackageSlabsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPackageSlabsMetadata._meta Is Nothing Then
					ICPackageSlabsMetadata._meta = New ICPackageSlabsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SlabId", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Min", new esTypeMap("bigint", "System.Int64"))
				meta.AddTypeMap("Max", new esTypeMap("bigint", "System.Int64"))
				meta.AddTypeMap("Percentage", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("PackageID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_PackageSlabs"
				meta.Destination = "IC_PackageSlabs"
				
				meta.spInsert = "proc_IC_PackageSlabsInsert"
				meta.spUpdate = "proc_IC_PackageSlabsUpdate"
				meta.spDelete = "proc_IC_PackageSlabsDelete"
				meta.spLoadAll = "proc_IC_PackageSlabsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PackageSlabsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPackageSlabsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
