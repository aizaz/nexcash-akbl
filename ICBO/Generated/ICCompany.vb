
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/30/2015 12:26:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Company' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCompany))> _
	<XmlType("ICCompany")> _	
	Partial Public Class ICCompany 
		Inherits esICCompany
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCompany()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal companyCode As System.Int32)
			Dim obj As New ICCompany()
			obj.CompanyCode = companyCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal companyCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCompany()
			obj.CompanyCode = companyCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCompanyCollection")> _
	Partial Public Class ICCompanyCollection
		Inherits esICCompanyCollection
		Implements IEnumerable(Of ICCompany)
	
		Public Function FindByPrimaryKey(ByVal companyCode As System.Int32) As ICCompany
			Return MyBase.SingleOrDefault(Function(e) e.CompanyCode.HasValue AndAlso e.CompanyCode.Value = companyCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCompany))> _
		Public Class ICCompanyCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCompanyCollection)
			
			Public Shared Widening Operator CType(packet As ICCompanyCollectionWCFPacket) As ICCompanyCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCompanyCollection) As ICCompanyCollectionWCFPacket
				Return New ICCompanyCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCompanyQuery 
		Inherits esICCompanyQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCompanyQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCompanyQuery) As String
			Return ICCompanyQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCompanyQuery
			Return DirectCast(ICCompanyQuery.SerializeHelper.FromXml(query, GetType(ICCompanyQuery)), ICCompanyQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCompany
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal companyCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(companyCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(companyCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal companyCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(companyCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(companyCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal companyCode As System.Int32) As Boolean
		
			Dim query As New ICCompanyQuery()
			query.Where(query.CompanyCode = companyCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal companyCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CompanyCode", companyCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Company.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.GroupCode, value) Then
					Me._UpToICGroupByGroupCode = Nothing
					Me.OnPropertyChanged("UpToICGroupByGroupCode")
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CompanyName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyName As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.CompanyName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.CompanyName, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CompanyName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.City
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property City As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.City)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.City, value) Then
					Me._UpToICCityByCity = Nothing
					Me.OnPropertyChanged("UpToICCityByCity")
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.City)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.Address
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Address As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.Address)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.Address, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.Address)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.PhoneNumber1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhoneNumber1 As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.PhoneNumber1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.PhoneNumber1, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.PhoneNumber1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.PhoneNumber2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhoneNumber2 As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.PhoneNumber2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.PhoneNumber2, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.PhoneNumber2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.EmailAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EmailAddress As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.EmailAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.EmailAddress, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.EmailAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsApprove, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.ApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.ApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.ApprovedDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.ApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.SkipPaymentQueue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SkipPaymentQueue As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.SkipPaymentQueue)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.SkipPaymentQueue, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.SkipPaymentQueue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CuttOffTimeStart
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CuttOffTimeStart As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.CuttOffTimeStart)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.CuttOffTimeStart, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CuttOffTimeStart)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CuttOffTimeEnd
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CuttOffTimeEnd As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.CuttOffTimeEnd)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.CuttOffTimeEnd, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CuttOffTimeEnd)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsPaymentsAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPaymentsAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsPaymentsAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsPaymentsAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsPaymentsAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsCollectionAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsCollectionAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsCollectionAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsCollectionAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsCollectionAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsTellerScreenAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsTellerScreenAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsTellerScreenAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsTellerScreenAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsTellerScreenAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsAutoFetchAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAutoFetchAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsAutoFetchAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsAutoFetchAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsAutoFetchAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsScrollUploadAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsScrollUploadAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsScrollUploadAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsScrollUploadAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsScrollUploadAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsCollectionApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsCollectionApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsCollectionApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsCollectionApproved, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsCollectionApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CollectionApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.CollectionApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.CollectionApprovedBy, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CollectionApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CollectionApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.CollectionApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.CollectionApprovedDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CollectionApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsSingleSignatory
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsSingleSignatory As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsSingleSignatory)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsSingleSignatory, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsSingleSignatory)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.PackageInvoiceBillingDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageInvoiceBillingDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.PackageInvoiceBillingDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.PackageInvoiceBillingDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.PackageInvoiceBillingDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsMT940StatementAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMT940StatementAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsMT940StatementAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsMT940StatementAllowed, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsMT940StatementAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.MT940StatementLocation
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940StatementLocation As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.MT940StatementLocation)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.MT940StatementLocation, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.MT940StatementLocation)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.HostName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HostName As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.HostName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.HostName, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.HostName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.UserName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserName As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.UserName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.UserName, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.UserName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.LocationPassword
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LocationPassword As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.LocationPassword)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.LocationPassword, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.LocationPassword)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.FolderName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FolderName As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.FolderName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.FolderName, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.FolderName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.MT940FetchingFrequency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940FetchingFrequency As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyMetadata.ColumnNames.MT940FetchingFrequency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyMetadata.ColumnNames.MT940FetchingFrequency, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.MT940FetchingFrequency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.MT940FetchedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940FetchedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCompanyMetadata.ColumnNames.MT940FetchedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCompanyMetadata.ColumnNames.MT940FetchedDate, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.MT940FetchedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsBeneRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsBeneRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsBeneRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsBeneRequired, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsBeneRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Company.IsAllowOpenPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowOpenPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyMetadata.ColumnNames.IsAllowOpenPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyMetadata.ColumnNames.IsAllowOpenPayment, value) Then
					OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsAllowOpenPayment)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCityByCity As ICCity
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICGroupByGroupCode As ICGroup
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "CompanyName"
							Me.str().CompanyName = CType(value, string)
												
						Case "City"
							Me.str().City = CType(value, string)
												
						Case "Address"
							Me.str().Address = CType(value, string)
												
						Case "PhoneNumber1"
							Me.str().PhoneNumber1 = CType(value, string)
												
						Case "PhoneNumber2"
							Me.str().PhoneNumber2 = CType(value, string)
												
						Case "EmailAddress"
							Me.str().EmailAddress = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApprove"
							Me.str().IsApprove = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedDate"
							Me.str().ApprovedDate = CType(value, string)
												
						Case "SkipPaymentQueue"
							Me.str().SkipPaymentQueue = CType(value, string)
												
						Case "CuttOffTimeStart"
							Me.str().CuttOffTimeStart = CType(value, string)
												
						Case "CuttOffTimeEnd"
							Me.str().CuttOffTimeEnd = CType(value, string)
												
						Case "IsPaymentsAllowed"
							Me.str().IsPaymentsAllowed = CType(value, string)
												
						Case "IsCollectionAllowed"
							Me.str().IsCollectionAllowed = CType(value, string)
												
						Case "IsTellerScreenAllowed"
							Me.str().IsTellerScreenAllowed = CType(value, string)
												
						Case "IsAutoFetchAllowed"
							Me.str().IsAutoFetchAllowed = CType(value, string)
												
						Case "IsScrollUploadAllowed"
							Me.str().IsScrollUploadAllowed = CType(value, string)
												
						Case "IsCollectionApproved"
							Me.str().IsCollectionApproved = CType(value, string)
												
						Case "CollectionApprovedBy"
							Me.str().CollectionApprovedBy = CType(value, string)
												
						Case "CollectionApprovedDate"
							Me.str().CollectionApprovedDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsSingleSignatory"
							Me.str().IsSingleSignatory = CType(value, string)
												
						Case "PackageInvoiceBillingDate"
							Me.str().PackageInvoiceBillingDate = CType(value, string)
												
						Case "IsMT940StatementAllowed"
							Me.str().IsMT940StatementAllowed = CType(value, string)
												
						Case "MT940StatementLocation"
							Me.str().MT940StatementLocation = CType(value, string)
												
						Case "HostName"
							Me.str().HostName = CType(value, string)
												
						Case "UserName"
							Me.str().UserName = CType(value, string)
												
						Case "LocationPassword"
							Me.str().LocationPassword = CType(value, string)
												
						Case "FolderName"
							Me.str().FolderName = CType(value, string)
												
						Case "MT940FetchingFrequency"
							Me.str().MT940FetchingFrequency = CType(value, string)
												
						Case "MT940FetchedDate"
							Me.str().MT940FetchedDate = CType(value, string)
												
						Case "IsBeneRequired"
							Me.str().IsBeneRequired = CType(value, string)
												
						Case "IsAllowOpenPayment"
							Me.str().IsAllowOpenPayment = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.GroupCode)
							End If
						
						Case "City"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.City = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.City)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsApprove)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.ApprovedDate)
							End If
						
						Case "SkipPaymentQueue"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.SkipPaymentQueue = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.SkipPaymentQueue)
							End If
						
						Case "IsPaymentsAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPaymentsAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsPaymentsAllowed)
							End If
						
						Case "IsCollectionAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsCollectionAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsCollectionAllowed)
							End If
						
						Case "IsTellerScreenAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsTellerScreenAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsTellerScreenAllowed)
							End If
						
						Case "IsAutoFetchAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAutoFetchAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsAutoFetchAllowed)
							End If
						
						Case "IsScrollUploadAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsScrollUploadAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsScrollUploadAllowed)
							End If
						
						Case "IsCollectionApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsCollectionApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsCollectionApproved)
							End If
						
						Case "CollectionApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CollectionApprovedBy)
							End If
						
						Case "CollectionApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CollectionApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CollectionApprovedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsSingleSignatory"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsSingleSignatory = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsSingleSignatory)
							End If
						
						Case "PackageInvoiceBillingDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PackageInvoiceBillingDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.PackageInvoiceBillingDate)
							End If
						
						Case "IsMT940StatementAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMT940StatementAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsMT940StatementAllowed)
							End If
						
						Case "MT940FetchedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.MT940FetchedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.MT940FetchedDate)
							End If
						
						Case "IsBeneRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsBeneRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsBeneRequired)
							End If
						
						Case "IsAllowOpenPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowOpenPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyMetadata.PropertyNames.IsAllowOpenPayment)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCompany)
				Me.entity = entity
			End Sub				
		
	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyName As System.String 
				Get
					Dim data_ As System.String = entity.CompanyName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyName = Nothing
					Else
						entity.CompanyName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property City As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.City
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.City = Nothing
					Else
						entity.City = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Address As System.String 
				Get
					Dim data_ As System.String = entity.Address
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Address = Nothing
					Else
						entity.Address = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhoneNumber1 As System.String 
				Get
					Dim data_ As System.String = entity.PhoneNumber1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhoneNumber1 = Nothing
					Else
						entity.PhoneNumber1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhoneNumber2 As System.String 
				Get
					Dim data_ As System.String = entity.PhoneNumber2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhoneNumber2 = Nothing
					Else
						entity.PhoneNumber2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property EmailAddress As System.String 
				Get
					Dim data_ As System.String = entity.EmailAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EmailAddress = Nothing
					Else
						entity.EmailAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApprove = Nothing
					Else
						entity.IsApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedDate = Nothing
					Else
						entity.ApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SkipPaymentQueue As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.SkipPaymentQueue
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SkipPaymentQueue = Nothing
					Else
						entity.SkipPaymentQueue = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CuttOffTimeStart As System.String 
				Get
					Dim data_ As System.String = entity.CuttOffTimeStart
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CuttOffTimeStart = Nothing
					Else
						entity.CuttOffTimeStart = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CuttOffTimeEnd As System.String 
				Get
					Dim data_ As System.String = entity.CuttOffTimeEnd
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CuttOffTimeEnd = Nothing
					Else
						entity.CuttOffTimeEnd = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPaymentsAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPaymentsAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPaymentsAllowed = Nothing
					Else
						entity.IsPaymentsAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsCollectionAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsCollectionAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsCollectionAllowed = Nothing
					Else
						entity.IsCollectionAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsTellerScreenAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsTellerScreenAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsTellerScreenAllowed = Nothing
					Else
						entity.IsTellerScreenAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAutoFetchAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAutoFetchAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAutoFetchAllowed = Nothing
					Else
						entity.IsAutoFetchAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsScrollUploadAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsScrollUploadAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsScrollUploadAllowed = Nothing
					Else
						entity.IsScrollUploadAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsCollectionApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsCollectionApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsCollectionApproved = Nothing
					Else
						entity.IsCollectionApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionApprovedBy = Nothing
					Else
						entity.CollectionApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CollectionApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionApprovedDate = Nothing
					Else
						entity.CollectionApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsSingleSignatory As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsSingleSignatory
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsSingleSignatory = Nothing
					Else
						entity.IsSingleSignatory = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackageInvoiceBillingDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PackageInvoiceBillingDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageInvoiceBillingDate = Nothing
					Else
						entity.PackageInvoiceBillingDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMT940StatementAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMT940StatementAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMT940StatementAllowed = Nothing
					Else
						entity.IsMT940StatementAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property MT940StatementLocation As System.String 
				Get
					Dim data_ As System.String = entity.MT940StatementLocation
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MT940StatementLocation = Nothing
					Else
						entity.MT940StatementLocation = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property HostName As System.String 
				Get
					Dim data_ As System.String = entity.HostName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HostName = Nothing
					Else
						entity.HostName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserName As System.String 
				Get
					Dim data_ As System.String = entity.UserName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserName = Nothing
					Else
						entity.UserName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property LocationPassword As System.String 
				Get
					Dim data_ As System.String = entity.LocationPassword
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LocationPassword = Nothing
					Else
						entity.LocationPassword = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FolderName As System.String 
				Get
					Dim data_ As System.String = entity.FolderName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FolderName = Nothing
					Else
						entity.FolderName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MT940FetchingFrequency As System.String 
				Get
					Dim data_ As System.String = entity.MT940FetchingFrequency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MT940FetchingFrequency = Nothing
					Else
						entity.MT940FetchingFrequency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MT940FetchedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.MT940FetchedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MT940FetchedDate = Nothing
					Else
						entity.MT940FetchedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsBeneRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsBeneRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsBeneRequired = Nothing
					Else
						entity.IsBeneRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowOpenPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowOpenPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowOpenPayment = Nothing
					Else
						entity.IsAllowOpenPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCompany
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCompanyMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCompanyQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCompanyQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCompanyQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCompanyQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCompanyQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCompanyCollection
		Inherits esEntityCollection(Of ICCompany)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCompanyMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCompanyCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCompanyQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCompanyQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCompanyQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCompanyQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCompanyQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCompanyQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCompanyQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCompanyQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCompanyMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "GroupCode" 
					Return Me.GroupCode
				Case "CompanyName" 
					Return Me.CompanyName
				Case "City" 
					Return Me.City
				Case "Address" 
					Return Me.Address
				Case "PhoneNumber1" 
					Return Me.PhoneNumber1
				Case "PhoneNumber2" 
					Return Me.PhoneNumber2
				Case "EmailAddress" 
					Return Me.EmailAddress
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApprove" 
					Return Me.IsApprove
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedDate" 
					Return Me.ApprovedDate
				Case "SkipPaymentQueue" 
					Return Me.SkipPaymentQueue
				Case "CuttOffTimeStart" 
					Return Me.CuttOffTimeStart
				Case "CuttOffTimeEnd" 
					Return Me.CuttOffTimeEnd
				Case "IsPaymentsAllowed" 
					Return Me.IsPaymentsAllowed
				Case "IsCollectionAllowed" 
					Return Me.IsCollectionAllowed
				Case "IsTellerScreenAllowed" 
					Return Me.IsTellerScreenAllowed
				Case "IsAutoFetchAllowed" 
					Return Me.IsAutoFetchAllowed
				Case "IsScrollUploadAllowed" 
					Return Me.IsScrollUploadAllowed
				Case "IsCollectionApproved" 
					Return Me.IsCollectionApproved
				Case "CollectionApprovedBy" 
					Return Me.CollectionApprovedBy
				Case "CollectionApprovedDate" 
					Return Me.CollectionApprovedDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsSingleSignatory" 
					Return Me.IsSingleSignatory
				Case "PackageInvoiceBillingDate" 
					Return Me.PackageInvoiceBillingDate
				Case "IsMT940StatementAllowed" 
					Return Me.IsMT940StatementAllowed
				Case "MT940StatementLocation" 
					Return Me.MT940StatementLocation
				Case "HostName" 
					Return Me.HostName
				Case "UserName" 
					Return Me.UserName
				Case "LocationPassword" 
					Return Me.LocationPassword
				Case "FolderName" 
					Return Me.FolderName
				Case "MT940FetchingFrequency" 
					Return Me.MT940FetchingFrequency
				Case "MT940FetchedDate" 
					Return Me.MT940FetchedDate
				Case "IsBeneRequired" 
					Return Me.IsBeneRequired
				Case "IsAllowOpenPayment" 
					Return Me.IsAllowOpenPayment
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CompanyName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property City As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.City, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Address As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.Address, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PhoneNumber1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.PhoneNumber1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PhoneNumber2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.PhoneNumber2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property EmailAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.EmailAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.ApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SkipPaymentQueue As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.SkipPaymentQueue, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CuttOffTimeStart As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CuttOffTimeStart, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CuttOffTimeEnd As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CuttOffTimeEnd, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsPaymentsAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsPaymentsAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsCollectionAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsCollectionAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsTellerScreenAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsTellerScreenAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsAutoFetchAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsAutoFetchAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsScrollUploadAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsScrollUploadAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsCollectionApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsCollectionApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CollectionApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CollectionApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsSingleSignatory As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsSingleSignatory, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property PackageInvoiceBillingDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.PackageInvoiceBillingDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsMT940StatementAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsMT940StatementAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property MT940StatementLocation As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.MT940StatementLocation, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property HostName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.HostName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UserName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.UserName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property LocationPassword As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.LocationPassword, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FolderName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.FolderName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MT940FetchingFrequency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.MT940FetchingFrequency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MT940FetchedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.MT940FetchedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsBeneRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsBeneRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowOpenPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyMetadata.ColumnNames.IsAllowOpenPayment, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCompany 
		Inherits esICCompany
		
	
		#Region "ICAccountsCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICAccountsCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICAccountsCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICAccountsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsQuery), New ICAccountsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Accounts_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsCollectionByCompanyCode As ICAccountsCollection 
		
			Get
				If Me._ICAccountsCollectionByCompanyCode Is Nothing Then
					Me._ICAccountsCollectionByCompanyCode = New ICAccountsCollection()
					Me._ICAccountsCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsCollectionByCompanyCode", Me._ICAccountsCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsCollectionByCompanyCode.Query.Where(Me._ICAccountsCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICAccountsCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsCollectionByCompanyCode.fks.Add(ICAccountsMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICAccountsCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICAccountsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICAccountsCollectionByCompanyCode")
					Me._ICAccountsCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsCollectionByCompanyCode As ICAccountsCollection
		#End Region

		#Region "ICAmendmentsRightsManagementCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAmendmentsRightsManagementCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICAmendmentsRightsManagementCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICAmendmentsRightsManagementCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAmendmentsRightsManagementCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICAmendmentsRightsManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAmendmentsRightsManagementQuery), New ICAmendmentsRightsManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAmendmentsRightsManagementCollectionByCompanyCode As ICAmendmentsRightsManagementCollection 
		
			Get
				If Me._ICAmendmentsRightsManagementCollectionByCompanyCode Is Nothing Then
					Me._ICAmendmentsRightsManagementCollectionByCompanyCode = New ICAmendmentsRightsManagementCollection()
					Me._ICAmendmentsRightsManagementCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAmendmentsRightsManagementCollectionByCompanyCode", Me._ICAmendmentsRightsManagementCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAmendmentsRightsManagementCollectionByCompanyCode.Query.Where(Me._ICAmendmentsRightsManagementCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICAmendmentsRightsManagementCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAmendmentsRightsManagementCollectionByCompanyCode.fks.Add(ICAmendmentsRightsManagementMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICAmendmentsRightsManagementCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICAmendmentsRightsManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAmendmentsRightsManagementCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICAmendmentsRightsManagementCollectionByCompanyCode")
					Me._ICAmendmentsRightsManagementCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAmendmentsRightsManagementCollectionByCompanyCode As ICAmendmentsRightsManagementCollection
		#End Region

		#Region "ICApprovalGroupManagementCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupManagementCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICApprovalGroupManagementCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICApprovalGroupManagementCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupManagementCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupManagementQuery), New ICApprovalGroupManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupManagement_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupManagementCollectionByCompanyCode As ICApprovalGroupManagementCollection 
		
			Get
				If Me._ICApprovalGroupManagementCollectionByCompanyCode Is Nothing Then
					Me._ICApprovalGroupManagementCollectionByCompanyCode = New ICApprovalGroupManagementCollection()
					Me._ICApprovalGroupManagementCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupManagementCollectionByCompanyCode", Me._ICApprovalGroupManagementCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupManagementCollectionByCompanyCode.Query.Where(Me._ICApprovalGroupManagementCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICApprovalGroupManagementCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupManagementCollectionByCompanyCode.fks.Add(ICApprovalGroupManagementMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICApprovalGroupManagementCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICApprovalGroupManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupManagementCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupManagementCollectionByCompanyCode")
					Me._ICApprovalGroupManagementCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupManagementCollectionByCompanyCode As ICApprovalGroupManagementCollection
		#End Region

		#Region "ICApprovalRuleCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICApprovalRuleCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICApprovalRuleCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleQuery), New ICApprovalRuleQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleCollectionByCompanyCode As ICApprovalRuleCollection 
		
			Get
				If Me._ICApprovalRuleCollectionByCompanyCode Is Nothing Then
					Me._ICApprovalRuleCollectionByCompanyCode = New ICApprovalRuleCollection()
					Me._ICApprovalRuleCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleCollectionByCompanyCode", Me._ICApprovalRuleCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleCollectionByCompanyCode.Query.Where(Me._ICApprovalRuleCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICApprovalRuleCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleCollectionByCompanyCode.fks.Add(ICApprovalRuleMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICApprovalRuleCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICApprovalRuleCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleCollectionByCompanyCode")
					Me._ICApprovalRuleCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleCollectionByCompanyCode As ICApprovalRuleCollection
		#End Region

		#Region "ICBeneCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICBeneCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICBeneCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICBeneQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneQuery), New ICBeneQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Bene_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneCollectionByCompanyCode As ICBeneCollection 
		
			Get
				If Me._ICBeneCollectionByCompanyCode Is Nothing Then
					Me._ICBeneCollectionByCompanyCode = New ICBeneCollection()
					Me._ICBeneCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneCollectionByCompanyCode", Me._ICBeneCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneCollectionByCompanyCode.Query.Where(Me._ICBeneCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICBeneCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneCollectionByCompanyCode.fks.Add(ICBeneMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICBeneCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICBeneCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICBeneCollectionByCompanyCode")
					Me._ICBeneCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneCollectionByCompanyCode As ICBeneCollection
		#End Region

		#Region "ICBeneGroupCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneGroupCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICBeneGroupCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICBeneGroupCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneGroupCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICBeneGroupQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneGroupQuery), New ICBeneGroupQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_BeneGroup_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneGroupCollectionByCompanyCode As ICBeneGroupCollection 
		
			Get
				If Me._ICBeneGroupCollectionByCompanyCode Is Nothing Then
					Me._ICBeneGroupCollectionByCompanyCode = New ICBeneGroupCollection()
					Me._ICBeneGroupCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneGroupCollectionByCompanyCode", Me._ICBeneGroupCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneGroupCollectionByCompanyCode.Query.Where(Me._ICBeneGroupCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICBeneGroupCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneGroupCollectionByCompanyCode.fks.Add(ICBeneGroupMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICBeneGroupCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICBeneGroupCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneGroupCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICBeneGroupCollectionByCompanyCode")
					Me._ICBeneGroupCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneGroupCollectionByCompanyCode As ICBeneGroupCollection
		#End Region

		#Region "ICCollectionAccountsCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICCollectionAccountsCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsQuery), New ICCollectionAccountsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccounts_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionByCompanyCode As ICCollectionAccountsCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionByCompanyCode Is Nothing Then
					Me._ICCollectionAccountsCollectionByCompanyCode = New ICCollectionAccountsCollection()
					Me._ICCollectionAccountsCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionByCompanyCode", Me._ICCollectionAccountsCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionByCompanyCode.Query.Where(Me._ICCollectionAccountsCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICCollectionAccountsCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionByCompanyCode.fks.Add(ICCollectionAccountsMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionByCompanyCode")
					Me._ICCollectionAccountsCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionByCompanyCode As ICCollectionAccountsCollection
		#End Region

		#Region "ICFilesCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFilesCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICFilesCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICFilesCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICFilesCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICFilesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFilesQuery), New ICFilesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Files_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFilesCollectionByCompanyCode As ICFilesCollection 
		
			Get
				If Me._ICFilesCollectionByCompanyCode Is Nothing Then
					Me._ICFilesCollectionByCompanyCode = New ICFilesCollection()
					Me._ICFilesCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFilesCollectionByCompanyCode", Me._ICFilesCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFilesCollectionByCompanyCode.Query.Where(Me._ICFilesCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICFilesCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFilesCollectionByCompanyCode.fks.Add(ICFilesMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICFilesCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICFilesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFilesCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICFilesCollectionByCompanyCode")
					Me._ICFilesCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFilesCollectionByCompanyCode As ICFilesCollection
		#End Region

		#Region "ICFlexiFieldsCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFlexiFieldsCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICFlexiFieldsCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICFlexiFieldsCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICFlexiFieldsCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICFlexiFieldsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFlexiFieldsQuery), New ICFlexiFieldsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FlexiFields_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFlexiFieldsCollectionByCompanyCode As ICFlexiFieldsCollection 
		
			Get
				If Me._ICFlexiFieldsCollectionByCompanyCode Is Nothing Then
					Me._ICFlexiFieldsCollectionByCompanyCode = New ICFlexiFieldsCollection()
					Me._ICFlexiFieldsCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFlexiFieldsCollectionByCompanyCode", Me._ICFlexiFieldsCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFlexiFieldsCollectionByCompanyCode.Query.Where(Me._ICFlexiFieldsCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICFlexiFieldsCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFlexiFieldsCollectionByCompanyCode.fks.Add(ICFlexiFieldsMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICFlexiFieldsCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICFlexiFieldsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFlexiFieldsCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICFlexiFieldsCollectionByCompanyCode")
					Me._ICFlexiFieldsCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFlexiFieldsCollectionByCompanyCode As ICFlexiFieldsCollection
		#End Region

		#Region "ICInstructionCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICInstructionCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICInstructionCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByCompanyCode As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByCompanyCode Is Nothing Then
					Me._ICInstructionCollectionByCompanyCode = New ICInstructionCollection()
					Me._ICInstructionCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByCompanyCode", Me._ICInstructionCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByCompanyCode.Query.Where(Me._ICInstructionCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICInstructionCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByCompanyCode.fks.Add(ICInstructionMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICInstructionCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByCompanyCode")
					Me._ICInstructionCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByCompanyCode As ICInstructionCollection
		#End Region

		#Region "ICOfficeCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOfficeCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICOfficeCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICOfficeCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICOfficeCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICOfficeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOfficeQuery), New ICOfficeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Office_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOfficeCollectionByCompanyCode As ICOfficeCollection 
		
			Get
				If Me._ICOfficeCollectionByCompanyCode Is Nothing Then
					Me._ICOfficeCollectionByCompanyCode = New ICOfficeCollection()
					Me._ICOfficeCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOfficeCollectionByCompanyCode", Me._ICOfficeCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOfficeCollectionByCompanyCode.Query.Where(Me._ICOfficeCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICOfficeCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOfficeCollectionByCompanyCode.fks.Add(ICOfficeMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICOfficeCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICOfficeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOfficeCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICOfficeCollectionByCompanyCode")
					Me._ICOfficeCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOfficeCollectionByCompanyCode As ICOfficeCollection
		#End Region

		#Region "ICOnlineFormSettingsCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOnlineFormSettingsCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICOnlineFormSettingsCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICOnlineFormSettingsCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICOnlineFormSettingsCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICOnlineFormSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOnlineFormSettingsQuery), New ICOnlineFormSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_OnlineFormSettings_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOnlineFormSettingsCollectionByCompanyCode As ICOnlineFormSettingsCollection 
		
			Get
				If Me._ICOnlineFormSettingsCollectionByCompanyCode Is Nothing Then
					Me._ICOnlineFormSettingsCollectionByCompanyCode = New ICOnlineFormSettingsCollection()
					Me._ICOnlineFormSettingsCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOnlineFormSettingsCollectionByCompanyCode", Me._ICOnlineFormSettingsCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOnlineFormSettingsCollectionByCompanyCode.Query.Where(Me._ICOnlineFormSettingsCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICOnlineFormSettingsCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOnlineFormSettingsCollectionByCompanyCode.fks.Add(ICOnlineFormSettingsMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICOnlineFormSettingsCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICOnlineFormSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOnlineFormSettingsCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICOnlineFormSettingsCollectionByCompanyCode")
					Me._ICOnlineFormSettingsCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOnlineFormSettingsCollectionByCompanyCode As ICOnlineFormSettingsCollection
		#End Region

		#Region "ICPackageInvoiceCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageInvoiceCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICPackageInvoiceCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICPackageInvoiceCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageInvoiceCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICPackageInvoiceQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageInvoiceQuery), New ICPackageInvoiceQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageInvoice_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageInvoiceCollectionByCompanyCode As ICPackageInvoiceCollection 
		
			Get
				If Me._ICPackageInvoiceCollectionByCompanyCode Is Nothing Then
					Me._ICPackageInvoiceCollectionByCompanyCode = New ICPackageInvoiceCollection()
					Me._ICPackageInvoiceCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageInvoiceCollectionByCompanyCode", Me._ICPackageInvoiceCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageInvoiceCollectionByCompanyCode.Query.Where(Me._ICPackageInvoiceCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICPackageInvoiceCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageInvoiceCollectionByCompanyCode.fks.Add(ICPackageInvoiceMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICPackageInvoiceCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICPackageInvoiceCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageInvoiceCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICPackageInvoiceCollectionByCompanyCode")
					Me._ICPackageInvoiceCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageInvoiceCollectionByCompanyCode As ICPackageInvoiceCollection
		#End Region

		#Region "ICSubSetCollectionByCompanyCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSetCollectionByCompanyCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCompany.ICSubSetCollectionByCompanyCode_Delegate)
				map.PropertyName = "ICSubSetCollectionByCompanyCode"
				map.MyColumnName = "CompanyCode"
				map.ParentColumnName = "CompanyCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSetCollectionByCompanyCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCompanyQuery(data.NextAlias())
			
			Dim mee As ICSubSetQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSetQuery), New ICSubSetQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CompanyCode = mee.CompanyCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSet_IC_Company
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSetCollectionByCompanyCode As ICSubSetCollection 
		
			Get
				If Me._ICSubSetCollectionByCompanyCode Is Nothing Then
					Me._ICSubSetCollectionByCompanyCode = New ICSubSetCollection()
					Me._ICSubSetCollectionByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSetCollectionByCompanyCode", Me._ICSubSetCollectionByCompanyCode)
				
					If Not Me.CompanyCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSetCollectionByCompanyCode.Query.Where(Me._ICSubSetCollectionByCompanyCode.Query.CompanyCode = Me.CompanyCode)
							Me._ICSubSetCollectionByCompanyCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSetCollectionByCompanyCode.fks.Add(ICSubSetMetadata.ColumnNames.CompanyCode, Me.CompanyCode)
					End If
				End If

				Return Me._ICSubSetCollectionByCompanyCode
			End Get
			
			Set(ByVal value As ICSubSetCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSetCollectionByCompanyCode Is Nothing Then

					Me.RemovePostSave("ICSubSetCollectionByCompanyCode")
					Me._ICSubSetCollectionByCompanyCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSetCollectionByCompanyCode As ICSubSetCollection
		#End Region

		#Region "UpToICCityByCity - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Company_IC_City
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCityByCity As ICCity
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCityByCity Is Nothing _
						 AndAlso Not City.Equals(Nothing)  Then
						
					Me._UpToICCityByCity = New ICCity()
					Me._UpToICCityByCity.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCityByCity", Me._UpToICCityByCity)
					Me._UpToICCityByCity.Query.Where(Me._UpToICCityByCity.Query.CityCode = Me.City)
					Me._UpToICCityByCity.Query.Load()
				End If

				Return Me._UpToICCityByCity
			End Get
			
            Set(ByVal value As ICCity)
				Me.RemovePreSave("UpToICCityByCity")
				

				If value Is Nothing Then
				
					Me.City = Nothing
				
					Me._UpToICCityByCity = Nothing
				Else
				
					Me.City = value.CityCode
					
					Me._UpToICCityByCity = value
					Me.SetPreSave("UpToICCityByCity", Me._UpToICCityByCity)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICGroupByGroupCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Company_IC_Group
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICGroupByGroupCode As ICGroup
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICGroupByGroupCode Is Nothing _
						 AndAlso Not GroupCode.Equals(Nothing)  Then
						
					Me._UpToICGroupByGroupCode = New ICGroup()
					Me._UpToICGroupByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
					Me._UpToICGroupByGroupCode.Query.Where(Me._UpToICGroupByGroupCode.Query.GroupCode = Me.GroupCode)
					Me._UpToICGroupByGroupCode.Query.Load()
				End If

				Return Me._UpToICGroupByGroupCode
			End Get
			
            Set(ByVal value As ICGroup)
				Me.RemovePreSave("UpToICGroupByGroupCode")
				

				If value Is Nothing Then
				
					Me.GroupCode = Nothing
				
					Me._UpToICGroupByGroupCode = Nothing
				Else
				
					Me.GroupCode = value.GroupCode
					
					Me._UpToICGroupByGroupCode = value
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountsCollectionByCompanyCode"
					coll = Me.ICAccountsCollectionByCompanyCode
					Exit Select
				Case "ICAmendmentsRightsManagementCollectionByCompanyCode"
					coll = Me.ICAmendmentsRightsManagementCollectionByCompanyCode
					Exit Select
				Case "ICApprovalGroupManagementCollectionByCompanyCode"
					coll = Me.ICApprovalGroupManagementCollectionByCompanyCode
					Exit Select
				Case "ICApprovalRuleCollectionByCompanyCode"
					coll = Me.ICApprovalRuleCollectionByCompanyCode
					Exit Select
				Case "ICBeneCollectionByCompanyCode"
					coll = Me.ICBeneCollectionByCompanyCode
					Exit Select
				Case "ICBeneGroupCollectionByCompanyCode"
					coll = Me.ICBeneGroupCollectionByCompanyCode
					Exit Select
				Case "ICCollectionAccountsCollectionByCompanyCode"
					coll = Me.ICCollectionAccountsCollectionByCompanyCode
					Exit Select
				Case "ICFilesCollectionByCompanyCode"
					coll = Me.ICFilesCollectionByCompanyCode
					Exit Select
				Case "ICFlexiFieldsCollectionByCompanyCode"
					coll = Me.ICFlexiFieldsCollectionByCompanyCode
					Exit Select
				Case "ICInstructionCollectionByCompanyCode"
					coll = Me.ICInstructionCollectionByCompanyCode
					Exit Select
				Case "ICOfficeCollectionByCompanyCode"
					coll = Me.ICOfficeCollectionByCompanyCode
					Exit Select
				Case "ICOnlineFormSettingsCollectionByCompanyCode"
					coll = Me.ICOnlineFormSettingsCollectionByCompanyCode
					Exit Select
				Case "ICPackageInvoiceCollectionByCompanyCode"
					coll = Me.ICPackageInvoiceCollectionByCompanyCode
					Exit Select
				Case "ICSubSetCollectionByCompanyCode"
					coll = Me.ICSubSetCollectionByCompanyCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountsCollectionByCompanyCode", GetType(ICAccountsCollection), New ICAccounts()))
			props.Add(new esPropertyDescriptor(Me, "ICAmendmentsRightsManagementCollectionByCompanyCode", GetType(ICAmendmentsRightsManagementCollection), New ICAmendmentsRightsManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupManagementCollectionByCompanyCode", GetType(ICApprovalGroupManagementCollection), New ICApprovalGroupManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleCollectionByCompanyCode", GetType(ICApprovalRuleCollection), New ICApprovalRule()))
			props.Add(new esPropertyDescriptor(Me, "ICBeneCollectionByCompanyCode", GetType(ICBeneCollection), New ICBene()))
			props.Add(new esPropertyDescriptor(Me, "ICBeneGroupCollectionByCompanyCode", GetType(ICBeneGroupCollection), New ICBeneGroup()))
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionByCompanyCode", GetType(ICCollectionAccountsCollection), New ICCollectionAccounts()))
			props.Add(new esPropertyDescriptor(Me, "ICFilesCollectionByCompanyCode", GetType(ICFilesCollection), New ICFiles()))
			props.Add(new esPropertyDescriptor(Me, "ICFlexiFieldsCollectionByCompanyCode", GetType(ICFlexiFieldsCollection), New ICFlexiFields()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByCompanyCode", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICOfficeCollectionByCompanyCode", GetType(ICOfficeCollection), New ICOffice()))
			props.Add(new esPropertyDescriptor(Me, "ICOnlineFormSettingsCollectionByCompanyCode", GetType(ICOnlineFormSettingsCollection), New ICOnlineFormSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICPackageInvoiceCollectionByCompanyCode", GetType(ICPackageInvoiceCollection), New ICPackageInvoice()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSetCollectionByCompanyCode", GetType(ICSubSetCollection), New ICSubSet()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCityByCity Is Nothing Then
				Me.City = Me._UpToICCityByCity.CityCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICGroupByGroupCode Is Nothing Then
				Me.GroupCode = Me._UpToICGroupByGroupCode.GroupCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAccountsCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICAccountsCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICAmendmentsRightsManagementCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICAmendmentsRightsManagementCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICApprovalGroupManagementCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICApprovalGroupManagementCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICApprovalRuleCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICApprovalRuleCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICBeneCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICBeneCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICBeneGroupCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICBeneGroupCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICCollectionAccountsCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICCollectionAccountsCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICFilesCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICFilesCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICFlexiFieldsCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICFlexiFieldsCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICInstructionCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICInstructionCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICOfficeCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICOfficeCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICOnlineFormSettingsCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICOnlineFormSettingsCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICPackageInvoiceCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICPackageInvoiceCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
			If Not Me._ICSubSetCollectionByCompanyCode Is Nothing Then
				Apply(Me._ICSubSetCollectionByCompanyCode, "CompanyCode", Me.CompanyCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCompanyMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CompanyCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CompanyCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.GroupCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.GroupCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CompanyName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CompanyName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.City, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.City
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.Address, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.Address
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.PhoneNumber1, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.PhoneNumber1
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.PhoneNumber2, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.PhoneNumber2
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.EmailAddress, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.EmailAddress
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CreatedBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CreatedDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsActive, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsApprove, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsApprove
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.ApprovedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.ApprovedDate, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.ApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.SkipPaymentQueue, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.SkipPaymentQueue
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CuttOffTimeStart, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CuttOffTimeStart
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CuttOffTimeEnd, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CuttOffTimeEnd
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsPaymentsAllowed, 17, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsPaymentsAllowed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsCollectionAllowed, 18, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsCollectionAllowed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsTellerScreenAllowed, 19, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsTellerScreenAllowed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsAutoFetchAllowed, 20, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsAutoFetchAllowed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsScrollUploadAllowed, 21, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsScrollUploadAllowed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsCollectionApproved, 22, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsCollectionApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CollectionApprovedBy, 23, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CollectionApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CollectionApprovedDate, 24, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CollectionApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.Creater, 25, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.CreationDate, 26, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsSingleSignatory, 27, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsSingleSignatory
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.PackageInvoiceBillingDate, 28, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.PackageInvoiceBillingDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsMT940StatementAllowed, 29, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsMT940StatementAllowed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.MT940StatementLocation, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.MT940StatementLocation
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.HostName, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.HostName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.UserName, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.UserName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.LocationPassword, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.LocationPassword
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.FolderName, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.FolderName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.MT940FetchingFrequency, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.MT940FetchingFrequency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.MT940FetchedDate, 36, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.MT940FetchedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsBeneRequired, 37, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsBeneRequired
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyMetadata.ColumnNames.IsAllowOpenPayment, 38, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyMetadata.PropertyNames.IsAllowOpenPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCompanyMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyName As String = "CompanyName"
			 Public Const City As String = "City"
			 Public Const Address As String = "Address"
			 Public Const PhoneNumber1 As String = "PhoneNumber1"
			 Public Const PhoneNumber2 As String = "PhoneNumber2"
			 Public Const EmailAddress As String = "EmailAddress"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const SkipPaymentQueue As String = "SkipPaymentQueue"
			 Public Const CuttOffTimeStart As String = "CuttOffTimeStart"
			 Public Const CuttOffTimeEnd As String = "CuttOffTimeEnd"
			 Public Const IsPaymentsAllowed As String = "IsPaymentsAllowed"
			 Public Const IsCollectionAllowed As String = "IsCollectionAllowed"
			 Public Const IsTellerScreenAllowed As String = "IsTellerScreenAllowed"
			 Public Const IsAutoFetchAllowed As String = "IsAutoFetchAllowed"
			 Public Const IsScrollUploadAllowed As String = "IsScrollUploadAllowed"
			 Public Const IsCollectionApproved As String = "IsCollectionApproved"
			 Public Const CollectionApprovedBy As String = "CollectionApprovedBy"
			 Public Const CollectionApprovedDate As String = "CollectionApprovedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsSingleSignatory As String = "IsSingleSignatory"
			 Public Const PackageInvoiceBillingDate As String = "PackageInvoiceBillingDate"
			 Public Const IsMT940StatementAllowed As String = "IsMT940StatementAllowed"
			 Public Const MT940StatementLocation As String = "MT940StatementLocation"
			 Public Const HostName As String = "HostName"
			 Public Const UserName As String = "UserName"
			 Public Const LocationPassword As String = "LocationPassword"
			 Public Const FolderName As String = "FolderName"
			 Public Const MT940FetchingFrequency As String = "MT940FetchingFrequency"
			 Public Const MT940FetchedDate As String = "MT940FetchedDate"
			 Public Const IsBeneRequired As String = "IsBeneRequired"
			 Public Const IsAllowOpenPayment As String = "IsAllowOpenPayment"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyName As String = "CompanyName"
			 Public Const City As String = "City"
			 Public Const Address As String = "Address"
			 Public Const PhoneNumber1 As String = "PhoneNumber1"
			 Public Const PhoneNumber2 As String = "PhoneNumber2"
			 Public Const EmailAddress As String = "EmailAddress"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const SkipPaymentQueue As String = "SkipPaymentQueue"
			 Public Const CuttOffTimeStart As String = "CuttOffTimeStart"
			 Public Const CuttOffTimeEnd As String = "CuttOffTimeEnd"
			 Public Const IsPaymentsAllowed As String = "IsPaymentsAllowed"
			 Public Const IsCollectionAllowed As String = "IsCollectionAllowed"
			 Public Const IsTellerScreenAllowed As String = "IsTellerScreenAllowed"
			 Public Const IsAutoFetchAllowed As String = "IsAutoFetchAllowed"
			 Public Const IsScrollUploadAllowed As String = "IsScrollUploadAllowed"
			 Public Const IsCollectionApproved As String = "IsCollectionApproved"
			 Public Const CollectionApprovedBy As String = "CollectionApprovedBy"
			 Public Const CollectionApprovedDate As String = "CollectionApprovedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsSingleSignatory As String = "IsSingleSignatory"
			 Public Const PackageInvoiceBillingDate As String = "PackageInvoiceBillingDate"
			 Public Const IsMT940StatementAllowed As String = "IsMT940StatementAllowed"
			 Public Const MT940StatementLocation As String = "MT940StatementLocation"
			 Public Const HostName As String = "HostName"
			 Public Const UserName As String = "UserName"
			 Public Const LocationPassword As String = "LocationPassword"
			 Public Const FolderName As String = "FolderName"
			 Public Const MT940FetchingFrequency As String = "MT940FetchingFrequency"
			 Public Const MT940FetchedDate As String = "MT940FetchedDate"
			 Public Const IsBeneRequired As String = "IsBeneRequired"
			 Public Const IsAllowOpenPayment As String = "IsAllowOpenPayment"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCompanyMetadata)
			
				If ICCompanyMetadata.mapDelegates Is Nothing Then
					ICCompanyMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCompanyMetadata._meta Is Nothing Then
					ICCompanyMetadata._meta = New ICCompanyMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("City", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Address", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PhoneNumber1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PhoneNumber2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("EmailAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SkipPaymentQueue", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CuttOffTimeStart", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CuttOffTimeEnd", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsPaymentsAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsCollectionAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsTellerScreenAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsAutoFetchAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsScrollUploadAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsCollectionApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CollectionApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsSingleSignatory", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("PackageInvoiceBillingDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsMT940StatementAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("MT940StatementLocation", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("HostName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UserName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("LocationPassword", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FolderName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MT940FetchingFrequency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MT940FetchedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsBeneRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsAllowOpenPayment", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_Company"
				meta.Destination = "IC_Company"
				
				meta.spInsert = "proc_IC_CompanyInsert"
				meta.spUpdate = "proc_IC_CompanyUpdate"
				meta.spDelete = "proc_IC_CompanyDelete"
				meta.spLoadAll = "proc_IC_CompanyLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CompanyLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCompanyMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
