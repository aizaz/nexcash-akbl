
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:52 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountPayNatureFileUploadTemplate' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountPayNatureFileUploadTemplate))> _
	<XmlType("ICAccountPayNatureFileUploadTemplate")> _	
	Partial Public Class ICAccountPayNatureFileUploadTemplate 
		Inherits esICAccountPayNatureFileUploadTemplate
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountPayNatureFileUploadTemplate()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String)
			Dim obj As New ICAccountPayNatureFileUploadTemplate()
			obj.TemplateID = templateID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountPayNatureFileUploadTemplate()
			obj.TemplateID = templateID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountPayNatureFileUploadTemplateCollection")> _
	Partial Public Class ICAccountPayNatureFileUploadTemplateCollection
		Inherits esICAccountPayNatureFileUploadTemplateCollection
		Implements IEnumerable(Of ICAccountPayNatureFileUploadTemplate)
	
		Public Function FindByPrimaryKey(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As ICAccountPayNatureFileUploadTemplate
			Return MyBase.SingleOrDefault(Function(e) e.TemplateID.HasValue AndAlso e.TemplateID.Value = templateID And e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.PaymentNatureCode = paymentNatureCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountPayNatureFileUploadTemplate))> _
		Public Class ICAccountPayNatureFileUploadTemplateCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountPayNatureFileUploadTemplateCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountPayNatureFileUploadTemplateCollectionWCFPacket) As ICAccountPayNatureFileUploadTemplateCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountPayNatureFileUploadTemplateCollection) As ICAccountPayNatureFileUploadTemplateCollectionWCFPacket
				Return New ICAccountPayNatureFileUploadTemplateCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountPayNatureFileUploadTemplateQuery 
		Inherits esICAccountPayNatureFileUploadTemplateQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountPayNatureFileUploadTemplateQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountPayNatureFileUploadTemplateQuery) As String
			Return ICAccountPayNatureFileUploadTemplateQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountPayNatureFileUploadTemplateQuery
			Return DirectCast(ICAccountPayNatureFileUploadTemplateQuery.SerializeHelper.FromXml(query, GetType(ICAccountPayNatureFileUploadTemplateQuery)), ICAccountPayNatureFileUploadTemplateQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountPayNatureFileUploadTemplate
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(templateID, accountNumber, branchCode, currency, paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(templateID, accountNumber, branchCode, currency, paymentNatureCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(templateID, accountNumber, branchCode, currency, paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(templateID, accountNumber, branchCode, currency, paymentNatureCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			Dim query As New ICAccountPayNatureFileUploadTemplateQuery()
			query.Where(query.TemplateID = templateID And query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.PaymentNatureCode = paymentNatureCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal templateID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("TemplateID", templateID)
						parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("PaymentNatureCode", paymentNatureCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.TemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.TemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.TemplateID, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.TemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPayNatureFileUploadTemplate.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICTemplateByTemplateID As ICTemplate
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "TemplateID"
							Me.str().TemplateID = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "TemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.TemplateID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountPayNatureFileUploadTemplate)
				Me.entity = entity
			End Sub				
		
	
			Public Property TemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateID = Nothing
					Else
						entity.TemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountPayNatureFileUploadTemplate
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPayNatureFileUploadTemplateMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountPayNatureFileUploadTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPayNatureFileUploadTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountPayNatureFileUploadTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountPayNatureFileUploadTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountPayNatureFileUploadTemplateQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountPayNatureFileUploadTemplateCollection
		Inherits esEntityCollection(Of ICAccountPayNatureFileUploadTemplate)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPayNatureFileUploadTemplateMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountPayNatureFileUploadTemplateCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountPayNatureFileUploadTemplateQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPayNatureFileUploadTemplateQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountPayNatureFileUploadTemplateQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountPayNatureFileUploadTemplateQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountPayNatureFileUploadTemplateQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountPayNatureFileUploadTemplateQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountPayNatureFileUploadTemplateQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountPayNatureFileUploadTemplateQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountPayNatureFileUploadTemplateMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "TemplateID" 
					Return Me.TemplateID
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property TemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.TemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountPayNatureFileUploadTemplate 
		Inherits esICAccountPayNatureFileUploadTemplate
		
	
		#Region "ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountPayNatureFileUploadTemplate.ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID_Delegate)
				map.PropertyName = "ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID"
				map.MyColumnName = "TemplateID,AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "TemplateID,AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountPayNatureFileUploadTemplateQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureFileUploadTemplatefieldsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureFileUploadTemplatefieldsQuery), New ICAccountPaymentNatureFileUploadTemplatefieldsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.TemplateID = mee.TemplateID And parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureFileUploadTemplatefields_IC_AccountPayNatureFileUploadTemplate
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID As ICAccountPaymentNatureFileUploadTemplatefieldsCollection 
		
			Get
				If Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID Is Nothing Then
					Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID = New ICAccountPaymentNatureFileUploadTemplatefieldsCollection()
					Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID", Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID)
				
					If Not Me.TemplateID.Equals(Nothing) AndAlso Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.TemplateID = Me.TemplateID)
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.BranchCode = Me.BranchCode)
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Currency = Me.Currency)
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Where(Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.fks.Add(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.TemplateID, Me.TemplateID)
						Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.fks.Add(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.fks.Add(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.fks.Add(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID.fks.Add(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureFileUploadTemplatefieldsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID")
					Me._ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID As ICAccountPaymentNatureFileUploadTemplatefieldsCollection
		#End Region

		#Region "UpToICAccountsPaymentNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountPayNatureFileUploadTemplate_IC_AccountsPaymentNature1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureByAccountNumber = New ICAccountsPaymentNature()
					Me._UpToICAccountsPaymentNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNature)
				Me.RemovePreSave("UpToICAccountsPaymentNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICAccountsPaymentNatureByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICTemplateByTemplateID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountPayNatureFileUploadTemplate_IC_Template
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICTemplateByTemplateID As ICTemplate
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICTemplateByTemplateID Is Nothing _
						 AndAlso Not TemplateID.Equals(Nothing)  Then
						
					Me._UpToICTemplateByTemplateID = New ICTemplate()
					Me._UpToICTemplateByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
					Me._UpToICTemplateByTemplateID.Query.Where(Me._UpToICTemplateByTemplateID.Query.TemplateID = Me.TemplateID)
					Me._UpToICTemplateByTemplateID.Query.Load()
				End If

				Return Me._UpToICTemplateByTemplateID
			End Get
			
            Set(ByVal value As ICTemplate)
				Me.RemovePreSave("UpToICTemplateByTemplateID")
				

				If value Is Nothing Then
				
					Me.TemplateID = Nothing
				
					Me._UpToICTemplateByTemplateID = Nothing
				Else
				
					Me.TemplateID = value.TemplateID
					
					Me._UpToICTemplateByTemplateID = value
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID"
					coll = Me.ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureFileUploadTemplatefieldsCollectionByTemplateID", GetType(ICAccountPaymentNatureFileUploadTemplatefieldsCollection), New ICAccountPaymentNatureFileUploadTemplatefields()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICTemplateByTemplateID Is Nothing Then
				Me.TemplateID = Me._UpToICTemplateByTemplateID.TemplateID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountPayNatureFileUploadTemplateMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.TemplateID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.TemplateID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.AccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.BranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreatedDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.PaymentNatureCode, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Creater, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.CreationDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPayNatureFileUploadTemplateMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountPayNatureFileUploadTemplateMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountPayNatureFileUploadTemplateMetadata)
			
				If ICAccountPayNatureFileUploadTemplateMetadata.mapDelegates Is Nothing Then
					ICAccountPayNatureFileUploadTemplateMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountPayNatureFileUploadTemplateMetadata._meta Is Nothing Then
					ICAccountPayNatureFileUploadTemplateMetadata._meta = New ICAccountPayNatureFileUploadTemplateMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("TemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_AccountPayNatureFileUploadTemplate"
				meta.Destination = "IC_AccountPayNatureFileUploadTemplate"
				
				meta.spInsert = "proc_IC_AccountPayNatureFileUploadTemplateInsert"
				meta.spUpdate = "proc_IC_AccountPayNatureFileUploadTemplateUpdate"
				meta.spDelete = "proc_IC_AccountPayNatureFileUploadTemplateDelete"
				meta.spLoadAll = "proc_IC_AccountPayNatureFileUploadTemplateLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountPayNatureFileUploadTemplateLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountPayNatureFileUploadTemplateMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
