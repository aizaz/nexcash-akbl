
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PaymentNatureAndRoles' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPaymentNatureAndRoles))> _
	<XmlType("ICPaymentNatureAndRoles")> _	
	Partial Public Class ICPaymentNatureAndRoles 
		Inherits esICPaymentNatureAndRoles
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPaymentNatureAndRoles()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal paymentNatureAndRolesID As System.Int32)
			Dim obj As New ICPaymentNatureAndRoles()
			obj.PaymentNatureAndRolesID = paymentNatureAndRolesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal paymentNatureAndRolesID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPaymentNatureAndRoles()
			obj.PaymentNatureAndRolesID = paymentNatureAndRolesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPaymentNatureAndRolesCollection")> _
	Partial Public Class ICPaymentNatureAndRolesCollection
		Inherits esICPaymentNatureAndRolesCollection
		Implements IEnumerable(Of ICPaymentNatureAndRoles)
	
		Public Function FindByPrimaryKey(ByVal paymentNatureAndRolesID As System.Int32) As ICPaymentNatureAndRoles
			Return MyBase.SingleOrDefault(Function(e) e.PaymentNatureAndRolesID.HasValue AndAlso e.PaymentNatureAndRolesID.Value = paymentNatureAndRolesID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPaymentNatureAndRoles))> _
		Public Class ICPaymentNatureAndRolesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPaymentNatureAndRolesCollection)
			
			Public Shared Widening Operator CType(packet As ICPaymentNatureAndRolesCollectionWCFPacket) As ICPaymentNatureAndRolesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPaymentNatureAndRolesCollection) As ICPaymentNatureAndRolesCollectionWCFPacket
				Return New ICPaymentNatureAndRolesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPaymentNatureAndRolesQuery 
		Inherits esICPaymentNatureAndRolesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPaymentNatureAndRolesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPaymentNatureAndRolesQuery) As String
			Return ICPaymentNatureAndRolesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPaymentNatureAndRolesQuery
			Return DirectCast(ICPaymentNatureAndRolesQuery.SerializeHelper.FromXml(query, GetType(ICPaymentNatureAndRolesQuery)), ICPaymentNatureAndRolesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPaymentNatureAndRoles
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal paymentNatureAndRolesID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(paymentNatureAndRolesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(paymentNatureAndRolesID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal paymentNatureAndRolesID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(paymentNatureAndRolesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(paymentNatureAndRolesID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal paymentNatureAndRolesID As System.Int32) As Boolean
		
			Dim query As New ICPaymentNatureAndRolesQuery()
			query.Where(query.PaymentNatureAndRolesID = paymentNatureAndRolesID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal paymentNatureAndRolesID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("PaymentNatureAndRolesID", paymentNatureAndRolesID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICRoleByRoleID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRoleID")
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.PaymentNatureAndRolesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureAndRolesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureAndRolesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureAndRolesID, value) Then
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.PaymentNatureAndRolesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureAndRolesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNatureAndRoles.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPaymentNatureAndRolesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPaymentNatureAndRolesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRoleID As ICRole
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "PaymentNatureAndRolesID"
							Me.str().PaymentNatureAndRolesID = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.RoleID)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreateDate)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreateBy)
							End If
						
						Case "PaymentNatureAndRolesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PaymentNatureAndRolesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.PaymentNatureAndRolesID)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPaymentNatureAndRolesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPaymentNatureAndRoles)
				Me.entity = entity
			End Sub				
		
	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureAndRolesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PaymentNatureAndRolesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureAndRolesID = Nothing
					Else
						entity.PaymentNatureAndRolesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPaymentNatureAndRoles
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPaymentNatureAndRolesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPaymentNatureAndRolesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPaymentNatureAndRolesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPaymentNatureAndRolesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPaymentNatureAndRolesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPaymentNatureAndRolesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPaymentNatureAndRolesCollection
		Inherits esEntityCollection(Of ICPaymentNatureAndRoles)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPaymentNatureAndRolesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPaymentNatureAndRolesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPaymentNatureAndRolesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPaymentNatureAndRolesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPaymentNatureAndRolesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPaymentNatureAndRolesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPaymentNatureAndRolesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPaymentNatureAndRolesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPaymentNatureAndRolesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPaymentNatureAndRolesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPaymentNatureAndRolesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "RoleID" 
					Return Me.RoleID
				Case "CreateDate" 
					Return Me.CreateDate
				Case "CreateBy" 
					Return Me.CreateBy
				Case "PaymentNatureAndRolesID" 
					Return Me.PaymentNatureAndRolesID
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureAndRolesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureAndRolesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureAndRolesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPaymentNatureAndRoles 
		Inherits esICPaymentNatureAndRoles
		
	
		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PaymentNatureAndRoles_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICRoleByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PaymentNatureAndRoles_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRoleID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRoleID = New ICRole()
					Me._UpToICRoleByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
					Me._UpToICRoleByRoleID.Query.Where(Me._UpToICRoleByRoleID.Query.RoleID = Me.RoleID)
					Me._UpToICRoleByRoleID.Query.Load()
				End If

				Return Me._UpToICRoleByRoleID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me._UpToICRoleByRoleID = Nothing
				Else
				
					Me.RoleID = value.RoleID
					
					Me._UpToICRoleByRoleID = value
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICPaymentNatureAndRolesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.RoleID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.RoleID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.CreateBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureAndRolesID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.PaymentNatureAndRolesID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.Creater, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureAndRolesMetadata.ColumnNames.CreationDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPaymentNatureAndRolesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPaymentNatureAndRolesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const PaymentNatureAndRolesID As String = "PaymentNatureAndRolesID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const PaymentNatureAndRolesID As String = "PaymentNatureAndRolesID"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPaymentNatureAndRolesMetadata)
			
				If ICPaymentNatureAndRolesMetadata.mapDelegates Is Nothing Then
					ICPaymentNatureAndRolesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPaymentNatureAndRolesMetadata._meta Is Nothing Then
					ICPaymentNatureAndRolesMetadata._meta = New ICPaymentNatureAndRolesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PaymentNatureAndRolesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_PaymentNatureAndRoles"
				meta.Destination = "IC_PaymentNatureAndRoles"
				
				meta.spInsert = "proc_IC_PaymentNatureAndRolesInsert"
				meta.spUpdate = "proc_IC_PaymentNatureAndRolesUpdate"
				meta.spDelete = "proc_IC_PaymentNatureAndRolesDelete"
				meta.spLoadAll = "proc_IC_PaymentNatureAndRolesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PaymentNatureAndRolesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPaymentNatureAndRolesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
