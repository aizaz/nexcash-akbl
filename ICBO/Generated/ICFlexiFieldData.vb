
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_FlexiFieldData' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFlexiFieldData))> _
	<XmlType("ICFlexiFieldData")> _	
	Partial Public Class ICFlexiFieldData 
		Inherits esICFlexiFieldData
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFlexiFieldData()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fieldDataID As System.Int32)
			Dim obj As New ICFlexiFieldData()
			obj.FieldDataID = fieldDataID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fieldDataID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFlexiFieldData()
			obj.FieldDataID = fieldDataID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFlexiFieldDataCollection")> _
	Partial Public Class ICFlexiFieldDataCollection
		Inherits esICFlexiFieldDataCollection
		Implements IEnumerable(Of ICFlexiFieldData)
	
		Public Function FindByPrimaryKey(ByVal fieldDataID As System.Int32) As ICFlexiFieldData
			Return MyBase.SingleOrDefault(Function(e) e.FieldDataID.HasValue AndAlso e.FieldDataID.Value = fieldDataID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFlexiFieldData))> _
		Public Class ICFlexiFieldDataCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFlexiFieldDataCollection)
			
			Public Shared Widening Operator CType(packet As ICFlexiFieldDataCollectionWCFPacket) As ICFlexiFieldDataCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFlexiFieldDataCollection) As ICFlexiFieldDataCollectionWCFPacket
				Return New ICFlexiFieldDataCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFlexiFieldDataQuery 
		Inherits esICFlexiFieldDataQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFlexiFieldDataQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFlexiFieldDataQuery) As String
			Return ICFlexiFieldDataQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFlexiFieldDataQuery
			Return DirectCast(ICFlexiFieldDataQuery.SerializeHelper.FromXml(query, GetType(ICFlexiFieldDataQuery)), ICFlexiFieldDataQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFlexiFieldData
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fieldDataID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldDataID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldDataID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fieldDataID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldDataID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldDataID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fieldDataID As System.Int32) As Boolean
		
			Dim query As New ICFlexiFieldDataQuery()
			query.Where(query.FieldDataID = fieldDataID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fieldDataID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FieldDataID", fieldDataID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_FlexiFieldData.FieldDataID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDataID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.FieldDataID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.FieldDataID, value) Then
					OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.FieldDataID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFieldData.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFieldData.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFlexiFieldDataMetadata.ColumnNames.InstructionID, value) Then
					OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FlexiFieldData.FieldValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldValue As System.String
			Get
				Return MyBase.GetSystemString(ICFlexiFieldDataMetadata.ColumnNames.FieldValue)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFlexiFieldDataMetadata.ColumnNames.FieldValue, value) Then
					OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.FieldValue)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FieldDataID"
							Me.str().FieldDataID = CType(value, string)
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "FieldValue"
							Me.str().FieldValue = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FieldDataID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldDataID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.FieldDataID)
							End If
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.FieldID)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFlexiFieldDataMetadata.PropertyNames.InstructionID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFlexiFieldData)
				Me.entity = entity
			End Sub				
		
	
			Public Property FieldDataID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldDataID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDataID = Nothing
					Else
						entity.FieldDataID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldValue As System.String 
				Get
					Dim data_ As System.String = entity.FieldValue
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldValue = Nothing
					Else
						entity.FieldValue = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFlexiFieldData
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFlexiFieldDataMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFlexiFieldDataQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFlexiFieldDataQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFlexiFieldDataQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFlexiFieldDataQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFlexiFieldDataQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFlexiFieldDataCollection
		Inherits esEntityCollection(Of ICFlexiFieldData)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFlexiFieldDataMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFlexiFieldDataCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFlexiFieldDataQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFlexiFieldDataQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFlexiFieldDataQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFlexiFieldDataQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFlexiFieldDataQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFlexiFieldDataQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFlexiFieldDataQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFlexiFieldDataQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFlexiFieldDataMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FieldDataID" 
					Return Me.FieldDataID
				Case "FieldID" 
					Return Me.FieldID
				Case "InstructionID" 
					Return Me.InstructionID
				Case "FieldValue" 
					Return Me.FieldValue
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FieldDataID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldDataMetadata.ColumnNames.FieldDataID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldDataMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldDataMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICFlexiFieldDataMetadata.ColumnNames.FieldValue, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFlexiFieldData 
		Inherits esICFlexiFieldData
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICFlexiFieldDataMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFlexiFieldDataMetadata.ColumnNames.FieldDataID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldDataMetadata.PropertyNames.FieldDataID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldDataMetadata.ColumnNames.FieldID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldDataMetadata.PropertyNames.FieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldDataMetadata.ColumnNames.InstructionID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFlexiFieldDataMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFlexiFieldDataMetadata.ColumnNames.FieldValue, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFlexiFieldDataMetadata.PropertyNames.FieldValue
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFlexiFieldDataMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FieldDataID As String = "FieldDataID"
			 Public Const FieldID As String = "FieldID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FieldValue As String = "FieldValue"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FieldDataID As String = "FieldDataID"
			 Public Const FieldID As String = "FieldID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const FieldValue As String = "FieldValue"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFlexiFieldDataMetadata)
			
				If ICFlexiFieldDataMetadata.mapDelegates Is Nothing Then
					ICFlexiFieldDataMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFlexiFieldDataMetadata._meta Is Nothing Then
					ICFlexiFieldDataMetadata._meta = New ICFlexiFieldDataMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FieldDataID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldValue", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_FlexiFieldData"
				meta.Destination = "IC_FlexiFieldData"
				
				meta.spInsert = "proc_IC_FlexiFieldDataInsert"
				meta.spUpdate = "proc_IC_FlexiFieldDataUpdate"
				meta.spDelete = "proc_IC_FlexiFieldDataDelete"
				meta.spLoadAll = "proc_IC_FlexiFieldDataLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FlexiFieldDataLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFlexiFieldDataMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
