
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/18/2014 7:20:31 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Collections' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollections))> _
	<XmlType("ICCollections")> _	
	Partial Public Class ICCollections 
		Inherits esICCollections
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollections()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionID As System.Int32)
			Dim obj As New ICCollections()
			obj.CollectionID = collectionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollections()
			obj.CollectionID = collectionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionsCollection")> _
	Partial Public Class ICCollectionsCollection
		Inherits esICCollectionsCollection
		Implements IEnumerable(Of ICCollections)
	
		Public Function FindByPrimaryKey(ByVal collectionID As System.Int32) As ICCollections
			Return MyBase.SingleOrDefault(Function(e) e.CollectionID.HasValue AndAlso e.CollectionID.Value = collectionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollections))> _
		Public Class ICCollectionsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionsCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionsCollectionWCFPacket) As ICCollectionsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionsCollection) As ICCollectionsCollectionWCFPacket
				Return New ICCollectionsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionsQuery 
		Inherits esICCollectionsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionsQuery) As String
			Return ICCollectionsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionsQuery
			Return DirectCast(ICCollectionsQuery.SerializeHelper.FromXml(query, GetType(ICCollectionsQuery)), ICCollectionsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollections
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionID As System.Int32) As Boolean
		
			Dim query As New ICCollectionsQuery()
			query.Where(query.CollectionID = collectionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionID", collectionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Collections.CollectionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionID, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PayersName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayersName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PayersName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PayersName, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PayersName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PayersAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayersAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountNo, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PayersAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PayersAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayersAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountBranchCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PayersAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PayersAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayersAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountCurrency, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PayersAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PayersAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayersAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PayersAccountType, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PayersAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountNo, value) Then
					Me._UpToICCollectionAccountsByCollectionAccountNo = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByCollectionAccountNo")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountBranchCode, value) Then
					Me._UpToICCollectionAccountsByCollectionAccountNo = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByCollectionAccountNo")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountCurrency, value) Then
					Me._UpToICCollectionAccountsByCollectionAccountNo = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsByCollectionAccountNo")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionAccountType, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.Status, value) Then
					Me._UpToICCollectionsStatusByStatus = Nothing
					Me.OnPropertyChanged("UpToICCollectionsStatusByStatus")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.LastStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.LastStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.LastStatus, value) Then
					Me._UpToICCollectionsStatusByLastStatus = Nothing
					Me.OnPropertyChanged("UpToICCollectionsStatusByLastStatus")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.LastStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICCollectionsMetadata.ColumnNames.CollectionAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICCollectionsMetadata.ColumnNames.CollectionAmount, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionRemarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionRemarks As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionRemarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionRemarks, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionRemarks)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionLocationCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionLocationCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionLocationCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionLocationCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionLocationCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionCreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionCreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionCreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionCreatedBy, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionCreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionDirectPayApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionDirectPayApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApprovedBy, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionDirectPayApproveDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionDirectPayApproveDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApproveDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApproveDateTime, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayApproveDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionMode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionMode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionProductTypeCode, value) Then
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICCollectionProductTypeByCollectionProductTypeCode")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ErrorMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ErrorMessage As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ErrorMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ErrorMessage, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ErrorMessage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.SkipReason
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SkipReason As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.SkipReason)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.SkipReason, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.SkipReason)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionClearingType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionClearingType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionClearingType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionClearingType, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionClearingType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingValueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingValueDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.ClearingValueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.ClearingValueDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingValueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountNo, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountBranchCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ClearingAccountCurrency, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingCBAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingCBAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ClearingCBAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ClearingCBAccountType, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingCBAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingProcess
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingProcess As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ClearingProcess)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ClearingProcess, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingProcess)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.InstrumentNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.InstrumentNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.InstrumentNo, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.InstrumentNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ClearingBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingBranchCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.ClearingBranchCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.ClearingBranchCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.BankCode, value) Then
					Me._UpToICBankByBankCode = Nothing
					Me.OnPropertyChanged("UpToICBankByBankCode")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionTransferedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionTransferedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionTransferedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionTransferedDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionTransferedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReturnReason
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReturnReason As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReturnReason)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReturnReason, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReturnReason)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionClientCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionClientCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionClientCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionClientCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionClientCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.CollectionNatureCode, value) Then
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
					Me.OnPropertyChanged("UpToICCollectionNatureByCollectionNatureCode")
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.FileBatchNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileBatchNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.FileBatchNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.FileBatchNo, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.FileBatchNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.FileID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.FileID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.FileID, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.FileID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReferenceField1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField1 As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField1, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReferenceField1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReferenceField2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField2 As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField2, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReferenceField2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReferenceField3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField3 As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField3)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField3, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReferenceField3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReferenceField4
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField4 As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField4)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField4, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReferenceField4)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.ReferenceField5
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField5 As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField5)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.ReferenceField5, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ReferenceField5)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CreatedOfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CreatedOfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CreatedOfficeCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CreatedOfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.AmountAfterDueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountAfterDueDate As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICCollectionsMetadata.ColumnNames.AmountAfterDueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICCollectionsMetadata.ColumnNames.AmountAfterDueDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.AmountAfterDueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.DueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DueDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.DueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.DueDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionDirectPayCreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionDirectPayCreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedBy, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.CollectionDirectPayCreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionDirectPayCreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.DirectPayRejectedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DirectPayRejectedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.DirectPayRejectedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.DirectPayRejectedBy, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DirectPayRejectedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.DirectPayRejectedDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DirectPayRejectedDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.DirectPayRejectedDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.DirectPayRejectedDateTime, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DirectPayRejectedDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.DrawnOnBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DrawnOnBranchCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionsMetadata.ColumnNames.DrawnOnBranchCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionsMetadata.ColumnNames.DrawnOnBranchCode, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DrawnOnBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.RoutingNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoutingNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionsMetadata.ColumnNames.RoutingNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionsMetadata.ColumnNames.RoutingNo, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.RoutingNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.IsPrintedNextCash
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrintedNextCash As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionsMetadata.ColumnNames.IsPrintedNextCash)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionsMetadata.ColumnNames.IsPrintedNextCash, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.IsPrintedNextCash)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Collections.InstrumentDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionsMetadata.ColumnNames.InstrumentDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionsMetadata.ColumnNames.InstrumentDate, value) Then
					OnPropertyChanged(ICCollectionsMetadata.PropertyNames.InstrumentDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBankByBankCode As ICBank
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionAccountsByCollectionAccountNo As ICCollectionAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionProductTypeByCollectionProductTypeCode As ICCollectionProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionsStatusByStatus As ICCollectionsStatus
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionsStatusByLastStatus As ICCollectionsStatus
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionID"
							Me.str().CollectionID = CType(value, string)
												
						Case "PayersName"
							Me.str().PayersName = CType(value, string)
												
						Case "CollectionDate"
							Me.str().CollectionDate = CType(value, string)
												
						Case "PayersAccountNo"
							Me.str().PayersAccountNo = CType(value, string)
												
						Case "PayersAccountBranchCode"
							Me.str().PayersAccountBranchCode = CType(value, string)
												
						Case "PayersAccountCurrency"
							Me.str().PayersAccountCurrency = CType(value, string)
												
						Case "PayersAccountType"
							Me.str().PayersAccountType = CType(value, string)
												
						Case "CollectionAccountNo"
							Me.str().CollectionAccountNo = CType(value, string)
												
						Case "CollectionAccountBranchCode"
							Me.str().CollectionAccountBranchCode = CType(value, string)
												
						Case "CollectionAccountCurrency"
							Me.str().CollectionAccountCurrency = CType(value, string)
												
						Case "CollectionAccountType"
							Me.str().CollectionAccountType = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "LastStatus"
							Me.str().LastStatus = CType(value, string)
												
						Case "CollectionAmount"
							Me.str().CollectionAmount = CType(value, string)
												
						Case "CollectionRemarks"
							Me.str().CollectionRemarks = CType(value, string)
												
						Case "CollectionLocationCode"
							Me.str().CollectionLocationCode = CType(value, string)
												
						Case "CollectionCreatedBy"
							Me.str().CollectionCreatedBy = CType(value, string)
												
						Case "CollectionDirectPayApprovedBy"
							Me.str().CollectionDirectPayApprovedBy = CType(value, string)
												
						Case "CollectionDirectPayApproveDateTime"
							Me.str().CollectionDirectPayApproveDateTime = CType(value, string)
												
						Case "CollectionMode"
							Me.str().CollectionMode = CType(value, string)
												
						Case "CollectionProductTypeCode"
							Me.str().CollectionProductTypeCode = CType(value, string)
												
						Case "ErrorMessage"
							Me.str().ErrorMessage = CType(value, string)
												
						Case "SkipReason"
							Me.str().SkipReason = CType(value, string)
												
						Case "CollectionClearingType"
							Me.str().CollectionClearingType = CType(value, string)
												
						Case "ClearingValueDate"
							Me.str().ClearingValueDate = CType(value, string)
												
						Case "ClearingAccountNo"
							Me.str().ClearingAccountNo = CType(value, string)
												
						Case "ClearingAccountBranchCode"
							Me.str().ClearingAccountBranchCode = CType(value, string)
												
						Case "ClearingAccountCurrency"
							Me.str().ClearingAccountCurrency = CType(value, string)
												
						Case "ClearingCBAccountType"
							Me.str().ClearingCBAccountType = CType(value, string)
												
						Case "ClearingProcess"
							Me.str().ClearingProcess = CType(value, string)
												
						Case "InstrumentNo"
							Me.str().InstrumentNo = CType(value, string)
												
						Case "ClearingBranchCode"
							Me.str().ClearingBranchCode = CType(value, string)
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "CollectionTransferedDate"
							Me.str().CollectionTransferedDate = CType(value, string)
												
						Case "ReturnReason"
							Me.str().ReturnReason = CType(value, string)
												
						Case "CollectionClientCode"
							Me.str().CollectionClientCode = CType(value, string)
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "FileBatchNo"
							Me.str().FileBatchNo = CType(value, string)
												
						Case "FileID"
							Me.str().FileID = CType(value, string)
												
						Case "ReferenceField1"
							Me.str().ReferenceField1 = CType(value, string)
												
						Case "ReferenceField2"
							Me.str().ReferenceField2 = CType(value, string)
												
						Case "ReferenceField3"
							Me.str().ReferenceField3 = CType(value, string)
												
						Case "ReferenceField4"
							Me.str().ReferenceField4 = CType(value, string)
												
						Case "ReferenceField5"
							Me.str().ReferenceField5 = CType(value, string)
												
						Case "CreatedOfficeCode"
							Me.str().CreatedOfficeCode = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "AmountAfterDueDate"
							Me.str().AmountAfterDueDate = CType(value, string)
												
						Case "DueDate"
							Me.str().DueDate = CType(value, string)
												
						Case "CollectionDirectPayCreatedBy"
							Me.str().CollectionDirectPayCreatedBy = CType(value, string)
												
						Case "CollectionDirectPayCreatedDate"
							Me.str().CollectionDirectPayCreatedDate = CType(value, string)
												
						Case "DirectPayRejectedBy"
							Me.str().DirectPayRejectedBy = CType(value, string)
												
						Case "DirectPayRejectedDateTime"
							Me.str().DirectPayRejectedDateTime = CType(value, string)
												
						Case "DrawnOnBranchCode"
							Me.str().DrawnOnBranchCode = CType(value, string)
												
						Case "RoutingNo"
							Me.str().RoutingNo = CType(value, string)
												
						Case "IsPrintedNextCash"
							Me.str().IsPrintedNextCash = CType(value, string)
												
						Case "InstrumentDate"
							Me.str().InstrumentDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CollectionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionID)
							End If
						
						Case "CollectionDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CollectionDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDate)
							End If
						
						Case "Status"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Status = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.Status)
							End If
						
						Case "LastStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.LastStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.LastStatus)
							End If
						
						Case "CollectionAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.CollectionAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionAmount)
							End If
						
						Case "CollectionLocationCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionLocationCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionLocationCode)
							End If
						
						Case "CollectionCreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionCreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionCreatedBy)
							End If
						
						Case "CollectionDirectPayApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionDirectPayApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayApprovedBy)
							End If
						
						Case "CollectionDirectPayApproveDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CollectionDirectPayApproveDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayApproveDateTime)
							End If
						
						Case "ClearingValueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ClearingValueDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingValueDate)
							End If
						
						Case "ClearingBranchCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingBranchCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.ClearingBranchCode)
							End If
						
						Case "BankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.BankCode)
							End If
						
						Case "CollectionTransferedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CollectionTransferedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionTransferedDate)
							End If
						
						Case "CollectionClientCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionClientCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionClientCode)
							End If
						
						Case "FileID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.FileID)
							End If
						
						Case "CreatedOfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedOfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CreatedOfficeCode)
							End If
						
						Case "AmountAfterDueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.AmountAfterDueDate = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.AmountAfterDueDate)
							End If
						
						Case "DueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.DueDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DueDate)
							End If
						
						Case "CollectionDirectPayCreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionDirectPayCreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedBy)
							End If
						
						Case "CollectionDirectPayCreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CollectionDirectPayCreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedDate)
							End If
						
						Case "DirectPayRejectedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DirectPayRejectedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DirectPayRejectedBy)
							End If
						
						Case "DirectPayRejectedDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.DirectPayRejectedDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DirectPayRejectedDateTime)
							End If
						
						Case "DrawnOnBranchCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DrawnOnBranchCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.DrawnOnBranchCode)
							End If
						
						Case "IsPrintedNextCash"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrintedNextCash = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.IsPrintedNextCash)
							End If
						
						Case "InstrumentDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.InstrumentDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionsMetadata.PropertyNames.InstrumentDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollections)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionID = Nothing
					Else
						entity.CollectionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayersName As System.String 
				Get
					Dim data_ As System.String = entity.PayersName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayersName = Nothing
					Else
						entity.PayersName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CollectionDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionDate = Nothing
					Else
						entity.CollectionDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayersAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.PayersAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayersAccountNo = Nothing
					Else
						entity.PayersAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayersAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.PayersAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayersAccountBranchCode = Nothing
					Else
						entity.PayersAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayersAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.PayersAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayersAccountCurrency = Nothing
					Else
						entity.PayersAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayersAccountType As System.String 
				Get
					Dim data_ As System.String = entity.PayersAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayersAccountType = Nothing
					Else
						entity.PayersAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.CollectionAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionAccountNo = Nothing
					Else
						entity.CollectionAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionAccountBranchCode = Nothing
					Else
						entity.CollectionAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.CollectionAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionAccountCurrency = Nothing
					Else
						entity.CollectionAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionAccountType As System.String 
				Get
					Dim data_ As System.String = entity.CollectionAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionAccountType = Nothing
					Else
						entity.CollectionAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Status
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.LastStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastStatus = Nothing
					Else
						entity.LastStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.CollectionAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionAmount = Nothing
					Else
						entity.CollectionAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionRemarks As System.String 
				Get
					Dim data_ As System.String = entity.CollectionRemarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionRemarks = Nothing
					Else
						entity.CollectionRemarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionLocationCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionLocationCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionLocationCode = Nothing
					Else
						entity.CollectionLocationCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionCreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionCreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionCreatedBy = Nothing
					Else
						entity.CollectionCreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionDirectPayApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionDirectPayApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionDirectPayApprovedBy = Nothing
					Else
						entity.CollectionDirectPayApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionDirectPayApproveDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CollectionDirectPayApproveDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionDirectPayApproveDateTime = Nothing
					Else
						entity.CollectionDirectPayApproveDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionMode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionMode = Nothing
					Else
						entity.CollectionMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionProductTypeCode = Nothing
					Else
						entity.CollectionProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ErrorMessage As System.String 
				Get
					Dim data_ As System.String = entity.ErrorMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ErrorMessage = Nothing
					Else
						entity.ErrorMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SkipReason As System.String 
				Get
					Dim data_ As System.String = entity.SkipReason
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SkipReason = Nothing
					Else
						entity.SkipReason = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionClearingType As System.String 
				Get
					Dim data_ As System.String = entity.CollectionClearingType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionClearingType = Nothing
					Else
						entity.CollectionClearingType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingValueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ClearingValueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingValueDate = Nothing
					Else
						entity.ClearingValueDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountNo = Nothing
					Else
						entity.ClearingAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountBranchCode = Nothing
					Else
						entity.ClearingAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountCurrency = Nothing
					Else
						entity.ClearingAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingCBAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingCBAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingCBAccountType = Nothing
					Else
						entity.ClearingCBAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingProcess As System.String 
				Get
					Dim data_ As System.String = entity.ClearingProcess
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingProcess = Nothing
					Else
						entity.ClearingProcess = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentNo As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentNo = Nothing
					Else
						entity.InstrumentNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingBranchCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingBranchCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingBranchCode = Nothing
					Else
						entity.ClearingBranchCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionTransferedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CollectionTransferedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionTransferedDate = Nothing
					Else
						entity.CollectionTransferedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReturnReason As System.String 
				Get
					Dim data_ As System.String = entity.ReturnReason
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReturnReason = Nothing
					Else
						entity.ReturnReason = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionClientCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionClientCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionClientCode = Nothing
					Else
						entity.CollectionClientCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileBatchNo As System.String 
				Get
					Dim data_ As System.String = entity.FileBatchNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileBatchNo = Nothing
					Else
						entity.FileBatchNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileID = Nothing
					Else
						entity.FileID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField1 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField1 = Nothing
					Else
						entity.ReferenceField1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField2 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField2 = Nothing
					Else
						entity.ReferenceField2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField3 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField3
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField3 = Nothing
					Else
						entity.ReferenceField3 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField4 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField4
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField4 = Nothing
					Else
						entity.ReferenceField4 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField5 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField5
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField5 = Nothing
					Else
						entity.ReferenceField5 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedOfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOfficeCode = Nothing
					Else
						entity.CreatedOfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountAfterDueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.AmountAfterDueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountAfterDueDate = Nothing
					Else
						entity.AmountAfterDueDate = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.DueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DueDate = Nothing
					Else
						entity.DueDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionDirectPayCreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionDirectPayCreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionDirectPayCreatedBy = Nothing
					Else
						entity.CollectionDirectPayCreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionDirectPayCreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CollectionDirectPayCreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionDirectPayCreatedDate = Nothing
					Else
						entity.CollectionDirectPayCreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DirectPayRejectedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DirectPayRejectedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DirectPayRejectedBy = Nothing
					Else
						entity.DirectPayRejectedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DirectPayRejectedDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.DirectPayRejectedDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DirectPayRejectedDateTime = Nothing
					Else
						entity.DirectPayRejectedDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DrawnOnBranchCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DrawnOnBranchCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DrawnOnBranchCode = Nothing
					Else
						entity.DrawnOnBranchCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoutingNo As System.String 
				Get
					Dim data_ As System.String = entity.RoutingNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoutingNo = Nothing
					Else
						entity.RoutingNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrintedNextCash As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrintedNextCash
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrintedNextCash = Nothing
					Else
						entity.IsPrintedNextCash = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.InstrumentDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentDate = Nothing
					Else
						entity.InstrumentDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollections
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionsCollection
		Inherits esEntityCollection(Of ICCollections)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionID" 
					Return Me.CollectionID
				Case "PayersName" 
					Return Me.PayersName
				Case "CollectionDate" 
					Return Me.CollectionDate
				Case "PayersAccountNo" 
					Return Me.PayersAccountNo
				Case "PayersAccountBranchCode" 
					Return Me.PayersAccountBranchCode
				Case "PayersAccountCurrency" 
					Return Me.PayersAccountCurrency
				Case "PayersAccountType" 
					Return Me.PayersAccountType
				Case "CollectionAccountNo" 
					Return Me.CollectionAccountNo
				Case "CollectionAccountBranchCode" 
					Return Me.CollectionAccountBranchCode
				Case "CollectionAccountCurrency" 
					Return Me.CollectionAccountCurrency
				Case "CollectionAccountType" 
					Return Me.CollectionAccountType
				Case "Status" 
					Return Me.Status
				Case "LastStatus" 
					Return Me.LastStatus
				Case "CollectionAmount" 
					Return Me.CollectionAmount
				Case "CollectionRemarks" 
					Return Me.CollectionRemarks
				Case "CollectionLocationCode" 
					Return Me.CollectionLocationCode
				Case "CollectionCreatedBy" 
					Return Me.CollectionCreatedBy
				Case "CollectionDirectPayApprovedBy" 
					Return Me.CollectionDirectPayApprovedBy
				Case "CollectionDirectPayApproveDateTime" 
					Return Me.CollectionDirectPayApproveDateTime
				Case "CollectionMode" 
					Return Me.CollectionMode
				Case "CollectionProductTypeCode" 
					Return Me.CollectionProductTypeCode
				Case "ErrorMessage" 
					Return Me.ErrorMessage
				Case "SkipReason" 
					Return Me.SkipReason
				Case "CollectionClearingType" 
					Return Me.CollectionClearingType
				Case "ClearingValueDate" 
					Return Me.ClearingValueDate
				Case "ClearingAccountNo" 
					Return Me.ClearingAccountNo
				Case "ClearingAccountBranchCode" 
					Return Me.ClearingAccountBranchCode
				Case "ClearingAccountCurrency" 
					Return Me.ClearingAccountCurrency
				Case "ClearingCBAccountType" 
					Return Me.ClearingCBAccountType
				Case "ClearingProcess" 
					Return Me.ClearingProcess
				Case "InstrumentNo" 
					Return Me.InstrumentNo
				Case "ClearingBranchCode" 
					Return Me.ClearingBranchCode
				Case "BankCode" 
					Return Me.BankCode
				Case "CollectionTransferedDate" 
					Return Me.CollectionTransferedDate
				Case "ReturnReason" 
					Return Me.ReturnReason
				Case "CollectionClientCode" 
					Return Me.CollectionClientCode
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "FileBatchNo" 
					Return Me.FileBatchNo
				Case "FileID" 
					Return Me.FileID
				Case "ReferenceField1" 
					Return Me.ReferenceField1
				Case "ReferenceField2" 
					Return Me.ReferenceField2
				Case "ReferenceField3" 
					Return Me.ReferenceField3
				Case "ReferenceField4" 
					Return Me.ReferenceField4
				Case "ReferenceField5" 
					Return Me.ReferenceField5
				Case "CreatedOfficeCode" 
					Return Me.CreatedOfficeCode
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "AmountAfterDueDate" 
					Return Me.AmountAfterDueDate
				Case "DueDate" 
					Return Me.DueDate
				Case "CollectionDirectPayCreatedBy" 
					Return Me.CollectionDirectPayCreatedBy
				Case "CollectionDirectPayCreatedDate" 
					Return Me.CollectionDirectPayCreatedDate
				Case "DirectPayRejectedBy" 
					Return Me.DirectPayRejectedBy
				Case "DirectPayRejectedDateTime" 
					Return Me.DirectPayRejectedDateTime
				Case "DrawnOnBranchCode" 
					Return Me.DrawnOnBranchCode
				Case "RoutingNo" 
					Return Me.RoutingNo
				Case "IsPrintedNextCash" 
					Return Me.IsPrintedNextCash
				Case "InstrumentDate" 
					Return Me.InstrumentDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PayersName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PayersName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PayersAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PayersAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayersAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PayersAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayersAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PayersAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayersAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PayersAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.Status, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property LastStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.LastStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionRemarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionRemarks, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionLocationCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionLocationCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionCreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionCreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionDirectPayApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionDirectPayApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionDirectPayApproveDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionDirectPayApproveDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ErrorMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ErrorMessage, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SkipReason As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.SkipReason, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionClearingType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionClearingType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingValueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingValueDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingCBAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingCBAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingProcess As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingProcess, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.InstrumentNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ClearingBranchCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.BankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionTransferedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionTransferedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReturnReason As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReturnReason, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionClientCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionClientCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileBatchNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.FileBatchNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.FileID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReferenceField1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReferenceField2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReferenceField3, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField4 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReferenceField4, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField5 As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.ReferenceField5, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CreatedOfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AmountAfterDueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.AmountAfterDueDate, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.DueDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionDirectPayCreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionDirectPayCreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DirectPayRejectedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.DirectPayRejectedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DirectPayRejectedDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.DirectPayRejectedDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DrawnOnBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.DrawnOnBranchCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RoutingNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.RoutingNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrintedNextCash As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.IsPrintedNextCash, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionsMetadata.ColumnNames.InstrumentDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollections 
		Inherits esICCollections
		
	
		#Region "ICCollectionValueDateClearingInstrumentsCollectionByCollectionID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionValueDateClearingInstrumentsCollectionByCollectionID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollections.ICCollectionValueDateClearingInstrumentsCollectionByCollectionID_Delegate)
				map.PropertyName = "ICCollectionValueDateClearingInstrumentsCollectionByCollectionID"
				map.MyColumnName = "CollectionID"
				map.ParentColumnName = "CollectionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionValueDateClearingInstrumentsCollectionByCollectionID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionsQuery(data.NextAlias())
			
			Dim mee As ICCollectionValueDateClearingInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionValueDateClearingInstrumentsQuery), New ICCollectionValueDateClearingInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CollectionID = mee.CollectionID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionValueDateClearingInstruments_IC_Collections
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionValueDateClearingInstrumentsCollectionByCollectionID As ICCollectionValueDateClearingInstrumentsCollection 
		
			Get
				If Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID Is Nothing Then
					Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID = New ICCollectionValueDateClearingInstrumentsCollection()
					Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionValueDateClearingInstrumentsCollectionByCollectionID", Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID)
				
					If Not Me.CollectionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID.Query.Where(Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID.Query.CollectionID = Me.CollectionID)
							Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID.fks.Add(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.CollectionID, Me.CollectionID)
					End If
				End If

				Return Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID
			End Get
			
			Set(ByVal value As ICCollectionValueDateClearingInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID Is Nothing Then

					Me.RemovePostSave("ICCollectionValueDateClearingInstrumentsCollectionByCollectionID")
					Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionValueDateClearingInstrumentsCollectionByCollectionID As ICCollectionValueDateClearingInstrumentsCollection
		#End Region

		#Region "UpToICBankByBankCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_Bank
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBankByBankCode As ICBank
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBankByBankCode Is Nothing _
						 AndAlso Not BankCode.Equals(Nothing)  Then
						
					Me._UpToICBankByBankCode = New ICBank()
					Me._UpToICBankByBankCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
					Me._UpToICBankByBankCode.Query.Where(Me._UpToICBankByBankCode.Query.BankCode = Me.BankCode)
					Me._UpToICBankByBankCode.Query.Load()
				End If

				Return Me._UpToICBankByBankCode
			End Get
			
            Set(ByVal value As ICBank)
				Me.RemovePreSave("UpToICBankByBankCode")
				

				If value Is Nothing Then
				
					Me.BankCode = Nothing
				
					Me._UpToICBankByBankCode = Nothing
				Else
				
					Me.BankCode = value.BankCode
					
					Me._UpToICBankByBankCode = value
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionAccountsByCollectionAccountNo - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionAccounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionAccountsByCollectionAccountNo As ICCollectionAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionAccountsByCollectionAccountNo Is Nothing _
						 AndAlso Not CollectionAccountNo.Equals(Nothing)  AndAlso Not CollectionAccountBranchCode.Equals(Nothing)  AndAlso Not CollectionAccountCurrency.Equals(Nothing)  Then
						
					Me._UpToICCollectionAccountsByCollectionAccountNo = New ICCollectionAccounts()
					Me._UpToICCollectionAccountsByCollectionAccountNo.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionAccountsByCollectionAccountNo", Me._UpToICCollectionAccountsByCollectionAccountNo)
					Me._UpToICCollectionAccountsByCollectionAccountNo.Query.Where(Me._UpToICCollectionAccountsByCollectionAccountNo.Query.AccountNumber = Me.CollectionAccountNo)
					Me._UpToICCollectionAccountsByCollectionAccountNo.Query.Where(Me._UpToICCollectionAccountsByCollectionAccountNo.Query.BranchCode = Me.CollectionAccountBranchCode)
					Me._UpToICCollectionAccountsByCollectionAccountNo.Query.Where(Me._UpToICCollectionAccountsByCollectionAccountNo.Query.Currency = Me.CollectionAccountCurrency)
					Me._UpToICCollectionAccountsByCollectionAccountNo.Query.Load()
				End If

				Return Me._UpToICCollectionAccountsByCollectionAccountNo
			End Get
			
            Set(ByVal value As ICCollectionAccounts)
				Me.RemovePreSave("UpToICCollectionAccountsByCollectionAccountNo")
				

				If value Is Nothing Then
				
					Me.CollectionAccountNo = Nothing
				
					Me.CollectionAccountBranchCode = Nothing
				
					Me.CollectionAccountCurrency = Nothing
				
					Me._UpToICCollectionAccountsByCollectionAccountNo = Nothing
				Else
				
					Me.CollectionAccountNo = value.AccountNumber
					
					Me.CollectionAccountBranchCode = value.BranchCode
					
					Me.CollectionAccountCurrency = value.Currency
					
					Me._UpToICCollectionAccountsByCollectionAccountNo = value
					Me.SetPreSave("UpToICCollectionAccountsByCollectionAccountNo", Me._UpToICCollectionAccountsByCollectionAccountNo)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionNatureByCollectionNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionNatureByCollectionNatureCode Is Nothing _
						 AndAlso Not CollectionNatureCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionNatureByCollectionNatureCode = New ICCollectionNature()
					Me._UpToICCollectionNatureByCollectionNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Where(Me._UpToICCollectionNatureByCollectionNatureCode.Query.CollectionNatureCode = Me.CollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Load()
				End If

				Return Me._UpToICCollectionNatureByCollectionNatureCode
			End Get
			
            Set(ByVal value As ICCollectionNature)
				Me.RemovePreSave("UpToICCollectionNatureByCollectionNatureCode")
				

				If value Is Nothing Then
				
					Me.CollectionNatureCode = Nothing
				
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
				Else
				
					Me.CollectionNatureCode = value.CollectionNatureCode
					
					Me._UpToICCollectionNatureByCollectionNatureCode = value
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionProductTypeByCollectionProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionProductTypeByCollectionProductTypeCode As ICCollectionProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionProductTypeByCollectionProductTypeCode Is Nothing _
						 AndAlso Not CollectionProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode = New ICCollectionProductType()
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionProductTypeByCollectionProductTypeCode", Me._UpToICCollectionProductTypeByCollectionProductTypeCode)
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode.Query.Where(Me._UpToICCollectionProductTypeByCollectionProductTypeCode.Query.ProductTypeCode = Me.CollectionProductTypeCode)
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode.Query.Load()
				End If

				Return Me._UpToICCollectionProductTypeByCollectionProductTypeCode
			End Get
			
            Set(ByVal value As ICCollectionProductType)
				Me.RemovePreSave("UpToICCollectionProductTypeByCollectionProductTypeCode")
				

				If value Is Nothing Then
				
					Me.CollectionProductTypeCode = Nothing
				
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode = Nothing
				Else
				
					Me.CollectionProductTypeCode = value.ProductTypeCode
					
					Me._UpToICCollectionProductTypeByCollectionProductTypeCode = value
					Me.SetPreSave("UpToICCollectionProductTypeByCollectionProductTypeCode", Me._UpToICCollectionProductTypeByCollectionProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionsStatusByStatus - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionsStatus
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionsStatusByStatus As ICCollectionsStatus
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionsStatusByStatus Is Nothing _
						 AndAlso Not Status.Equals(Nothing)  Then
						
					Me._UpToICCollectionsStatusByStatus = New ICCollectionsStatus()
					Me._UpToICCollectionsStatusByStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionsStatusByStatus", Me._UpToICCollectionsStatusByStatus)
					Me._UpToICCollectionsStatusByStatus.Query.Where(Me._UpToICCollectionsStatusByStatus.Query.CollectionStatusID = Me.Status)
					Me._UpToICCollectionsStatusByStatus.Query.Load()
				End If

				Return Me._UpToICCollectionsStatusByStatus
			End Get
			
            Set(ByVal value As ICCollectionsStatus)
				Me.RemovePreSave("UpToICCollectionsStatusByStatus")
				

				If value Is Nothing Then
				
					Me.Status = Nothing
				
					Me._UpToICCollectionsStatusByStatus = Nothing
				Else
				
					Me.Status = value.CollectionStatusID
					
					Me._UpToICCollectionsStatusByStatus = value
					Me.SetPreSave("UpToICCollectionsStatusByStatus", Me._UpToICCollectionsStatusByStatus)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionsStatusByLastStatus - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Collections_IC_CollectionsStatus1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionsStatusByLastStatus As ICCollectionsStatus
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionsStatusByLastStatus Is Nothing _
						 AndAlso Not LastStatus.Equals(Nothing)  Then
						
					Me._UpToICCollectionsStatusByLastStatus = New ICCollectionsStatus()
					Me._UpToICCollectionsStatusByLastStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionsStatusByLastStatus", Me._UpToICCollectionsStatusByLastStatus)
					Me._UpToICCollectionsStatusByLastStatus.Query.Where(Me._UpToICCollectionsStatusByLastStatus.Query.CollectionStatusID = Me.LastStatus)
					Me._UpToICCollectionsStatusByLastStatus.Query.Load()
				End If

				Return Me._UpToICCollectionsStatusByLastStatus
			End Get
			
            Set(ByVal value As ICCollectionsStatus)
				Me.RemovePreSave("UpToICCollectionsStatusByLastStatus")
				

				If value Is Nothing Then
				
					Me.LastStatus = Nothing
				
					Me._UpToICCollectionsStatusByLastStatus = Nothing
				Else
				
					Me.LastStatus = value.CollectionStatusID
					
					Me._UpToICCollectionsStatusByLastStatus = value
					Me.SetPreSave("UpToICCollectionsStatusByLastStatus", Me._UpToICCollectionsStatusByLastStatus)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionValueDateClearingInstrumentsCollectionByCollectionID"
					coll = Me.ICCollectionValueDateClearingInstrumentsCollectionByCollectionID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionValueDateClearingInstrumentsCollectionByCollectionID", GetType(ICCollectionValueDateClearingInstrumentsCollection), New ICCollectionValueDateClearingInstruments()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICBankByBankCode Is Nothing Then
				Me.BankCode = Me._UpToICBankByBankCode.BankCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCollectionsStatusByStatus Is Nothing Then
				Me.Status = Me._UpToICCollectionsStatusByStatus.CollectionStatusID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCollectionsStatusByLastStatus Is Nothing Then
				Me.LastStatus = Me._UpToICCollectionsStatusByLastStatus.CollectionStatusID
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID Is Nothing Then
				Apply(Me._ICCollectionValueDateClearingInstrumentsCollectionByCollectionID, "CollectionID", Me.CollectionID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PayersName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PayersName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PayersAccountNo, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PayersAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PayersAccountBranchCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PayersAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PayersAccountCurrency, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PayersAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PayersAccountType, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PayersAccountType
			c.CharacterMaxLength = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionAccountNo, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionAccountBranchCode, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionAccountCurrency, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionAccountType, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionAccountType
			c.CharacterMaxLength = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.Status, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.Status
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.LastStatus, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.LastStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionAmount, 13, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionRemarks, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionRemarks
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionLocationCode, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionLocationCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionCreatedBy, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionCreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApprovedBy, 17, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionDirectPayApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionDirectPayApproveDateTime, 18, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionDirectPayApproveDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionMode, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionMode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionProductTypeCode, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ErrorMessage, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ErrorMessage
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.SkipReason, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.SkipReason
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionClearingType, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionClearingType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingValueDate, 24, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingValueDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingAccountNo, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingAccountBranchCode, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingAccountCurrency, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingCBAccountType, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingCBAccountType
			c.CharacterMaxLength = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingProcess, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingProcess
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.InstrumentNo, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.InstrumentNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ClearingBranchCode, 31, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ClearingBranchCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.BankCode, 32, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.BankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionTransferedDate, 33, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionTransferedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReturnReason, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReturnReason
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionClientCode, 35, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionClientCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionNatureCode, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.FileBatchNo, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.FileBatchNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.FileID, 38, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.FileID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReferenceField1, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReferenceField1
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReferenceField2, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReferenceField2
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReferenceField3, 41, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReferenceField3
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReferenceField4, 42, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReferenceField4
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.ReferenceField5, 43, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.ReferenceField5
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CreatedOfficeCode, 44, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CreatedOfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.PaymentNatureCode, 45, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.AmountAfterDueDate, 46, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.AmountAfterDueDate
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.DueDate, 47, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.DueDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedBy, 48, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.CollectionDirectPayCreatedDate, 49, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.CollectionDirectPayCreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.DirectPayRejectedBy, 50, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.DirectPayRejectedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.DirectPayRejectedDateTime, 51, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.DirectPayRejectedDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.DrawnOnBranchCode, 52, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.DrawnOnBranchCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.RoutingNo, 53, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.RoutingNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.IsPrintedNextCash, 54, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.IsPrintedNextCash
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionsMetadata.ColumnNames.InstrumentDate, 55, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionsMetadata.PropertyNames.InstrumentDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionID As String = "CollectionID"
			 Public Const PayersName As String = "PayersName"
			 Public Const CollectionDate As String = "CollectionDate"
			 Public Const PayersAccountNo As String = "PayersAccountNo"
			 Public Const PayersAccountBranchCode As String = "PayersAccountBranchCode"
			 Public Const PayersAccountCurrency As String = "PayersAccountCurrency"
			 Public Const PayersAccountType As String = "PayersAccountType"
			 Public Const CollectionAccountNo As String = "CollectionAccountNo"
			 Public Const CollectionAccountBranchCode As String = "CollectionAccountBranchCode"
			 Public Const CollectionAccountCurrency As String = "CollectionAccountCurrency"
			 Public Const CollectionAccountType As String = "CollectionAccountType"
			 Public Const Status As String = "Status"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const CollectionAmount As String = "CollectionAmount"
			 Public Const CollectionRemarks As String = "CollectionRemarks"
			 Public Const CollectionLocationCode As String = "CollectionLocationCode"
			 Public Const CollectionCreatedBy As String = "CollectionCreatedBy"
			 Public Const CollectionDirectPayApprovedBy As String = "CollectionDirectPayApprovedBy"
			 Public Const CollectionDirectPayApproveDateTime As String = "CollectionDirectPayApproveDateTime"
			 Public Const CollectionMode As String = "CollectionMode"
			 Public Const CollectionProductTypeCode As String = "CollectionProductTypeCode"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const SkipReason As String = "SkipReason"
			 Public Const CollectionClearingType As String = "CollectionClearingType"
			 Public Const ClearingValueDate As String = "ClearingValueDate"
			 Public Const ClearingAccountNo As String = "ClearingAccountNo"
			 Public Const ClearingAccountBranchCode As String = "ClearingAccountBranchCode"
			 Public Const ClearingAccountCurrency As String = "ClearingAccountCurrency"
			 Public Const ClearingCBAccountType As String = "ClearingCBAccountType"
			 Public Const ClearingProcess As String = "ClearingProcess"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const ClearingBranchCode As String = "ClearingBranchCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const CollectionTransferedDate As String = "CollectionTransferedDate"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const CollectionClientCode As String = "CollectionClientCode"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const FileID As String = "FileID"
			 Public Const ReferenceField1 As String = "ReferenceField1"
			 Public Const ReferenceField2 As String = "ReferenceField2"
			 Public Const ReferenceField3 As String = "ReferenceField3"
			 Public Const ReferenceField4 As String = "ReferenceField4"
			 Public Const ReferenceField5 As String = "ReferenceField5"
			 Public Const CreatedOfficeCode As String = "CreatedOfficeCode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const AmountAfterDueDate As String = "AmountAfterDueDate"
			 Public Const DueDate As String = "DueDate"
			 Public Const CollectionDirectPayCreatedBy As String = "CollectionDirectPayCreatedBy"
			 Public Const CollectionDirectPayCreatedDate As String = "CollectionDirectPayCreatedDate"
			 Public Const DirectPayRejectedBy As String = "DirectPayRejectedBy"
			 Public Const DirectPayRejectedDateTime As String = "DirectPayRejectedDateTime"
			 Public Const DrawnOnBranchCode As String = "DrawnOnBranchCode"
			 Public Const RoutingNo As String = "RoutingNo"
			 Public Const IsPrintedNextCash As String = "IsPrintedNextCash"
			 Public Const InstrumentDate As String = "InstrumentDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionID As String = "CollectionID"
			 Public Const PayersName As String = "PayersName"
			 Public Const CollectionDate As String = "CollectionDate"
			 Public Const PayersAccountNo As String = "PayersAccountNo"
			 Public Const PayersAccountBranchCode As String = "PayersAccountBranchCode"
			 Public Const PayersAccountCurrency As String = "PayersAccountCurrency"
			 Public Const PayersAccountType As String = "PayersAccountType"
			 Public Const CollectionAccountNo As String = "CollectionAccountNo"
			 Public Const CollectionAccountBranchCode As String = "CollectionAccountBranchCode"
			 Public Const CollectionAccountCurrency As String = "CollectionAccountCurrency"
			 Public Const CollectionAccountType As String = "CollectionAccountType"
			 Public Const Status As String = "Status"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const CollectionAmount As String = "CollectionAmount"
			 Public Const CollectionRemarks As String = "CollectionRemarks"
			 Public Const CollectionLocationCode As String = "CollectionLocationCode"
			 Public Const CollectionCreatedBy As String = "CollectionCreatedBy"
			 Public Const CollectionDirectPayApprovedBy As String = "CollectionDirectPayApprovedBy"
			 Public Const CollectionDirectPayApproveDateTime As String = "CollectionDirectPayApproveDateTime"
			 Public Const CollectionMode As String = "CollectionMode"
			 Public Const CollectionProductTypeCode As String = "CollectionProductTypeCode"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const SkipReason As String = "SkipReason"
			 Public Const CollectionClearingType As String = "CollectionClearingType"
			 Public Const ClearingValueDate As String = "ClearingValueDate"
			 Public Const ClearingAccountNo As String = "ClearingAccountNo"
			 Public Const ClearingAccountBranchCode As String = "ClearingAccountBranchCode"
			 Public Const ClearingAccountCurrency As String = "ClearingAccountCurrency"
			 Public Const ClearingCBAccountType As String = "ClearingCBAccountType"
			 Public Const ClearingProcess As String = "ClearingProcess"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const ClearingBranchCode As String = "ClearingBranchCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const CollectionTransferedDate As String = "CollectionTransferedDate"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const CollectionClientCode As String = "CollectionClientCode"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const FileID As String = "FileID"
			 Public Const ReferenceField1 As String = "ReferenceField1"
			 Public Const ReferenceField2 As String = "ReferenceField2"
			 Public Const ReferenceField3 As String = "ReferenceField3"
			 Public Const ReferenceField4 As String = "ReferenceField4"
			 Public Const ReferenceField5 As String = "ReferenceField5"
			 Public Const CreatedOfficeCode As String = "CreatedOfficeCode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const AmountAfterDueDate As String = "AmountAfterDueDate"
			 Public Const DueDate As String = "DueDate"
			 Public Const CollectionDirectPayCreatedBy As String = "CollectionDirectPayCreatedBy"
			 Public Const CollectionDirectPayCreatedDate As String = "CollectionDirectPayCreatedDate"
			 Public Const DirectPayRejectedBy As String = "DirectPayRejectedBy"
			 Public Const DirectPayRejectedDateTime As String = "DirectPayRejectedDateTime"
			 Public Const DrawnOnBranchCode As String = "DrawnOnBranchCode"
			 Public Const RoutingNo As String = "RoutingNo"
			 Public Const IsPrintedNextCash As String = "IsPrintedNextCash"
			 Public Const InstrumentDate As String = "InstrumentDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionsMetadata)
			
				If ICCollectionsMetadata.mapDelegates Is Nothing Then
					ICCollectionsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionsMetadata._meta Is Nothing Then
					ICCollectionsMetadata._meta = New ICCollectionsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PayersName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PayersAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayersAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayersAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayersAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Status", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("LastStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("CollectionRemarks", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionLocationCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionCreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionDirectPayApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionDirectPayApproveDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CollectionMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ErrorMessage", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SkipReason", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionClearingType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingValueDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ClearingAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingCBAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingProcess", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InstrumentNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingBranchCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionTransferedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReturnReason", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionClientCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileBatchNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReferenceField1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField3", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField4", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField5", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedOfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AmountAfterDueDate", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("DueDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CollectionDirectPayCreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionDirectPayCreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DirectPayRejectedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DirectPayRejectedDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DrawnOnBranchCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RoutingNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsPrintedNextCash", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("InstrumentDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Collections"
				meta.Destination = "IC_Collections"
				
				meta.spInsert = "proc_IC_CollectionsInsert"
				meta.spUpdate = "proc_IC_CollectionsUpdate"
				meta.spDelete = "proc_IC_CollectionsDelete"
				meta.spLoadAll = "proc_IC_CollectionsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
