
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/22/2014 7:26:25 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_UBPSCompany' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUBPSCompany))> _
	<XmlType("ICUBPSCompany")> _	
	Partial Public Class ICUBPSCompany 
		Inherits esICUBPSCompany
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUBPSCompany()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal uBPSCompanyID As System.Int32)
			Dim obj As New ICUBPSCompany()
			obj.UBPSCompanyID = uBPSCompanyID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal uBPSCompanyID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUBPSCompany()
			obj.UBPSCompanyID = uBPSCompanyID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUBPSCompanyCollection")> _
	Partial Public Class ICUBPSCompanyCollection
		Inherits esICUBPSCompanyCollection
		Implements IEnumerable(Of ICUBPSCompany)
	
		Public Function FindByPrimaryKey(ByVal uBPSCompanyID As System.Int32) As ICUBPSCompany
			Return MyBase.SingleOrDefault(Function(e) e.UBPSCompanyID.HasValue AndAlso e.UBPSCompanyID.Value = uBPSCompanyID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUBPSCompany))> _
		Public Class ICUBPSCompanyCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUBPSCompanyCollection)
			
			Public Shared Widening Operator CType(packet As ICUBPSCompanyCollectionWCFPacket) As ICUBPSCompanyCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUBPSCompanyCollection) As ICUBPSCompanyCollectionWCFPacket
				Return New ICUBPSCompanyCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUBPSCompanyQuery 
		Inherits esICUBPSCompanyQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUBPSCompanyQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUBPSCompanyQuery) As String
			Return ICUBPSCompanyQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUBPSCompanyQuery
			Return DirectCast(ICUBPSCompanyQuery.SerializeHelper.FromXml(query, GetType(ICUBPSCompanyQuery)), ICUBPSCompanyQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUBPSCompany
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal uBPSCompanyID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(uBPSCompanyID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(uBPSCompanyID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal uBPSCompanyID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(uBPSCompanyID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(uBPSCompanyID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal uBPSCompanyID As System.Int32) As Boolean
		
			Dim query As New ICUBPSCompanyQuery()
			query.Where(query.UBPSCompanyID = uBPSCompanyID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal uBPSCompanyID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UBPSCompanyID", uBPSCompanyID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_UBPSCompany.UBPSCompanyID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UBPSCompanyID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.UBPSCompanyID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.UBPSCompanyID, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.UBPSCompanyID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CompanyName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyName As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyName, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CompanyName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CompanyType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyType As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.CompanyType, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CompanyType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.MinValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MinValue As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.MinValue)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.MinValue, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.MinValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.MaxValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MaxValue As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.MaxValue)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.MaxValue, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.MaxValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.MultiplesOfAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MultiplesOfAmount As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.MultiplesOfAmount)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.MultiplesOfAmount, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.MultiplesOfAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ReferenceFieldCaption
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceFieldCaption As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldCaption)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldCaption, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldCaption)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.IsBillingInquiry
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsBillingInquiry As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsBillingInquiry)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsBillingInquiry, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsBillingInquiry)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ReferenceFieldMinLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceFieldMinLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMinLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMinLength, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMinLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ReferenceFieldMaxLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceFieldMaxLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMaxLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMaxLength, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMaxLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.RegularExpressionForReferenceField
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RegularExpressionForReferenceField As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.RegularExpressionForReferenceField)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.RegularExpressionForReferenceField, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.RegularExpressionForReferenceField)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.XAmountValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property XAmountValue As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.XAmountValue)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICUBPSCompanyMetadata.ColumnNames.XAmountValue, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.XAmountValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.Creator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creator As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.Creator)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.Creator, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.Creator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ReferenceFieldFormat
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceFieldFormat As System.String
			Get
				Return MyBase.GetSystemString(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldFormat)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldFormat, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldFormat)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUBPSCompanyMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.ApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUBPSCompanyMetadata.ColumnNames.ApprovedDate, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UBPSCompany.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUBPSCompanyMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UBPSCompanyID"
							Me.str().UBPSCompanyID = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CompanyName"
							Me.str().CompanyName = CType(value, string)
												
						Case "CompanyType"
							Me.str().CompanyType = CType(value, string)
												
						Case "MinValue"
							Me.str().MinValue = CType(value, string)
												
						Case "MaxValue"
							Me.str().MaxValue = CType(value, string)
												
						Case "MultiplesOfAmount"
							Me.str().MultiplesOfAmount = CType(value, string)
												
						Case "ReferenceFieldCaption"
							Me.str().ReferenceFieldCaption = CType(value, string)
												
						Case "IsBillingInquiry"
							Me.str().IsBillingInquiry = CType(value, string)
												
						Case "ReferenceFieldMinLength"
							Me.str().ReferenceFieldMinLength = CType(value, string)
												
						Case "ReferenceFieldMaxLength"
							Me.str().ReferenceFieldMaxLength = CType(value, string)
												
						Case "RegularExpressionForReferenceField"
							Me.str().RegularExpressionForReferenceField = CType(value, string)
												
						Case "XAmountValue"
							Me.str().XAmountValue = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creator"
							Me.str().Creator = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "ReferenceFieldFormat"
							Me.str().ReferenceFieldFormat = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedDate"
							Me.str().ApprovedDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UBPSCompanyID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UBPSCompanyID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.UBPSCompanyID)
							End If
						
						Case "MinValue"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.MinValue = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.MinValue)
							End If
						
						Case "MaxValue"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.MaxValue = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.MaxValue)
							End If
						
						Case "IsBillingInquiry"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsBillingInquiry = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsBillingInquiry)
							End If
						
						Case "ReferenceFieldMinLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReferenceFieldMinLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMinLength)
							End If
						
						Case "ReferenceFieldMaxLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReferenceFieldMaxLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMaxLength)
							End If
						
						Case "XAmountValue"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.XAmountValue = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.XAmountValue)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creator"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creator = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.Creator)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ApprovedDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUBPSCompanyMetadata.PropertyNames.ApprovedBy)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUBPSCompany)
				Me.entity = entity
			End Sub				
		
	
			Public Property UBPSCompanyID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UBPSCompanyID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UBPSCompanyID = Nothing
					Else
						entity.UBPSCompanyID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As System.String = entity.CompanyCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyName As System.String 
				Get
					Dim data_ As System.String = entity.CompanyName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyName = Nothing
					Else
						entity.CompanyName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyType As System.String 
				Get
					Dim data_ As System.String = entity.CompanyType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyType = Nothing
					Else
						entity.CompanyType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MinValue As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.MinValue
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MinValue = Nothing
					Else
						entity.MinValue = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property MaxValue As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.MaxValue
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MaxValue = Nothing
					Else
						entity.MaxValue = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property MultiplesOfAmount As System.String 
				Get
					Dim data_ As System.String = entity.MultiplesOfAmount
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MultiplesOfAmount = Nothing
					Else
						entity.MultiplesOfAmount = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceFieldCaption As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceFieldCaption
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceFieldCaption = Nothing
					Else
						entity.ReferenceFieldCaption = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsBillingInquiry As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsBillingInquiry
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsBillingInquiry = Nothing
					Else
						entity.IsBillingInquiry = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceFieldMinLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReferenceFieldMinLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceFieldMinLength = Nothing
					Else
						entity.ReferenceFieldMinLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceFieldMaxLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReferenceFieldMaxLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceFieldMaxLength = Nothing
					Else
						entity.ReferenceFieldMaxLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RegularExpressionForReferenceField As System.String 
				Get
					Dim data_ As System.String = entity.RegularExpressionForReferenceField
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RegularExpressionForReferenceField = Nothing
					Else
						entity.RegularExpressionForReferenceField = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property XAmountValue As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.XAmountValue
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.XAmountValue = Nothing
					Else
						entity.XAmountValue = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creator As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creator
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creator = Nothing
					Else
						entity.Creator = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceFieldFormat As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceFieldFormat
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceFieldFormat = Nothing
					Else
						entity.ReferenceFieldFormat = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedDate = Nothing
					Else
						entity.ApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUBPSCompany
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUBPSCompanyMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUBPSCompanyQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUBPSCompanyQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUBPSCompanyQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUBPSCompanyQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUBPSCompanyQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUBPSCompanyCollection
		Inherits esEntityCollection(Of ICUBPSCompany)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUBPSCompanyMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUBPSCompanyCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUBPSCompanyQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUBPSCompanyQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUBPSCompanyQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUBPSCompanyQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUBPSCompanyQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUBPSCompanyQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUBPSCompanyQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUBPSCompanyQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUBPSCompanyMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UBPSCompanyID" 
					Return Me.UBPSCompanyID
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CompanyName" 
					Return Me.CompanyName
				Case "CompanyType" 
					Return Me.CompanyType
				Case "MinValue" 
					Return Me.MinValue
				Case "MaxValue" 
					Return Me.MaxValue
				Case "MultiplesOfAmount" 
					Return Me.MultiplesOfAmount
				Case "ReferenceFieldCaption" 
					Return Me.ReferenceFieldCaption
				Case "IsBillingInquiry" 
					Return Me.IsBillingInquiry
				Case "ReferenceFieldMinLength" 
					Return Me.ReferenceFieldMinLength
				Case "ReferenceFieldMaxLength" 
					Return Me.ReferenceFieldMaxLength
				Case "RegularExpressionForReferenceField" 
					Return Me.RegularExpressionForReferenceField
				Case "XAmountValue" 
					Return Me.XAmountValue
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creator" 
					Return Me.Creator
				Case "CreationDate" 
					Return Me.CreationDate
				Case "ReferenceFieldFormat" 
					Return Me.ReferenceFieldFormat
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedDate" 
					Return Me.ApprovedDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UBPSCompanyID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.UBPSCompanyID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CompanyCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyName As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CompanyName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyType As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CompanyType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MinValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.MinValue, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property MaxValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.MaxValue, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property MultiplesOfAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.MultiplesOfAmount, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceFieldCaption As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldCaption, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsBillingInquiry As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.IsBillingInquiry, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceFieldMinLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMinLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceFieldMaxLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMaxLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RegularExpressionForReferenceField As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.RegularExpressionForReferenceField, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property XAmountValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.XAmountValue, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creator As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.Creator, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceFieldFormat As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldFormat, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUBPSCompanyMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUBPSCompany 
		Inherits esICUBPSCompany
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICUBPSCompanyMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.UBPSCompanyID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.UBPSCompanyID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CompanyCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CompanyCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CompanyName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CompanyName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CompanyType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CompanyType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.MinValue, 4, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.MinValue
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.MaxValue, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.MaxValue
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.MultiplesOfAmount, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.MultiplesOfAmount
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldCaption, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldCaption
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.IsBillingInquiry, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.IsBillingInquiry
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMinLength, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMinLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldMaxLength, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldMaxLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.RegularExpressionForReferenceField, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.RegularExpressionForReferenceField
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.XAmountValue, 12, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.XAmountValue
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CreatedBy, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CreatedDate, 14, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.Creator, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.Creator
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.CreationDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ReferenceFieldFormat, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ReferenceFieldFormat
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.IsActive, 18, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.IsApproved, 19, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ApprovedDate, 20, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUBPSCompanyMetadata.ColumnNames.ApprovedBy, 21, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUBPSCompanyMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUBPSCompanyMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UBPSCompanyID As String = "UBPSCompanyID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CompanyName As String = "CompanyName"
			 Public Const CompanyType As String = "CompanyType"
			 Public Const MinValue As String = "MinValue"
			 Public Const MaxValue As String = "MaxValue"
			 Public Const MultiplesOfAmount As String = "MultiplesOfAmount"
			 Public Const ReferenceFieldCaption As String = "ReferenceFieldCaption"
			 Public Const IsBillingInquiry As String = "IsBillingInquiry"
			 Public Const ReferenceFieldMinLength As String = "ReferenceFieldMinLength"
			 Public Const ReferenceFieldMaxLength As String = "ReferenceFieldMaxLength"
			 Public Const RegularExpressionForReferenceField As String = "RegularExpressionForReferenceField"
			 Public Const XAmountValue As String = "XAmountValue"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ReferenceFieldFormat As String = "ReferenceFieldFormat"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UBPSCompanyID As String = "UBPSCompanyID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CompanyName As String = "CompanyName"
			 Public Const CompanyType As String = "CompanyType"
			 Public Const MinValue As String = "MinValue"
			 Public Const MaxValue As String = "MaxValue"
			 Public Const MultiplesOfAmount As String = "MultiplesOfAmount"
			 Public Const ReferenceFieldCaption As String = "ReferenceFieldCaption"
			 Public Const IsBillingInquiry As String = "IsBillingInquiry"
			 Public Const ReferenceFieldMinLength As String = "ReferenceFieldMinLength"
			 Public Const ReferenceFieldMaxLength As String = "ReferenceFieldMaxLength"
			 Public Const RegularExpressionForReferenceField As String = "RegularExpressionForReferenceField"
			 Public Const XAmountValue As String = "XAmountValue"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ReferenceFieldFormat As String = "ReferenceFieldFormat"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUBPSCompanyMetadata)
			
				If ICUBPSCompanyMetadata.mapDelegates Is Nothing Then
					ICUBPSCompanyMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUBPSCompanyMetadata._meta Is Nothing Then
					ICUBPSCompanyMetadata._meta = New ICUBPSCompanyMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UBPSCompanyID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MinValue", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("MaxValue", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("MultiplesOfAmount", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceFieldCaption", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsBillingInquiry", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReferenceFieldMinLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReferenceFieldMaxLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RegularExpressionForReferenceField", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("XAmountValue", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creator", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReferenceFieldFormat", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_UBPSCompany"
				meta.Destination = "IC_UBPSCompany"
				
				meta.spInsert = "proc_IC_UBPSCompanyInsert"
				meta.spUpdate = "proc_IC_UBPSCompanyUpdate"
				meta.spDelete = "proc_IC_UBPSCompanyDelete"
				meta.spLoadAll = "proc_IC_UBPSCompanyLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UBPSCompanyLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUBPSCompanyMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
