
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CompanyRoleRights' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCompanyRoleRights))> _
	<XmlType("ICCompanyRoleRights")> _	
	Partial Public Class ICCompanyRoleRights 
		Inherits esICCompanyRoleRights
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCompanyRoleRights()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal companyRoleRightID As System.Int32)
			Dim obj As New ICCompanyRoleRights()
			obj.CompanyRoleRightID = companyRoleRightID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal companyRoleRightID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCompanyRoleRights()
			obj.CompanyRoleRightID = companyRoleRightID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCompanyRoleRightsCollection")> _
	Partial Public Class ICCompanyRoleRightsCollection
		Inherits esICCompanyRoleRightsCollection
		Implements IEnumerable(Of ICCompanyRoleRights)
	
		Public Function FindByPrimaryKey(ByVal companyRoleRightID As System.Int32) As ICCompanyRoleRights
			Return MyBase.SingleOrDefault(Function(e) e.CompanyRoleRightID.HasValue AndAlso e.CompanyRoleRightID.Value = companyRoleRightID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCompanyRoleRights))> _
		Public Class ICCompanyRoleRightsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCompanyRoleRightsCollection)
			
			Public Shared Widening Operator CType(packet As ICCompanyRoleRightsCollectionWCFPacket) As ICCompanyRoleRightsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCompanyRoleRightsCollection) As ICCompanyRoleRightsCollectionWCFPacket
				Return New ICCompanyRoleRightsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCompanyRoleRightsQuery 
		Inherits esICCompanyRoleRightsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCompanyRoleRightsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCompanyRoleRightsQuery) As String
			Return ICCompanyRoleRightsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCompanyRoleRightsQuery
			Return DirectCast(ICCompanyRoleRightsQuery.SerializeHelper.FromXml(query, GetType(ICCompanyRoleRightsQuery)), ICCompanyRoleRightsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCompanyRoleRights
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal companyRoleRightID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(companyRoleRightID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(companyRoleRightID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal companyRoleRightID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(companyRoleRightID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(companyRoleRightID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal companyRoleRightID As System.Int32) As Boolean
		
			Dim query As New ICCompanyRoleRightsQuery()
			query.Where(query.CompanyRoleRightID = companyRoleRightID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal companyRoleRightID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CompanyRoleRightID", companyRoleRightID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CompanyRoleRights.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyRoleRightsMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyRoleRightsMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICRoleByRoleID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRoleID")
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.RightName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightName As System.String
			Get
				Return MyBase.GetSystemString(ICCompanyRoleRightsMetadata.ColumnNames.RightName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCompanyRoleRightsMetadata.ColumnNames.RightName, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.RightName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CanView
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanView As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanView)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanView, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanView)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CanAdd
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanAdd As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanAdd)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanAdd, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanAdd)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CanEdit
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanEdit As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanEdit)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanEdit, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanEdit)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CanDelete
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanDelete As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanDelete)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanDelete, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanDelete)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CanApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCompanyRoleRightsMetadata.ColumnNames.CanApprove, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CompanyRoleRights.CompanyRoleRightID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyRoleRightID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCompanyRoleRightsMetadata.ColumnNames.CompanyRoleRightID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCompanyRoleRightsMetadata.ColumnNames.CompanyRoleRightID, value) Then
					OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CompanyRoleRightID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRoleID As ICRole
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "RightName"
							Me.str().RightName = CType(value, string)
												
						Case "CanView"
							Me.str().CanView = CType(value, string)
												
						Case "CanAdd"
							Me.str().CanAdd = CType(value, string)
												
						Case "CanEdit"
							Me.str().CanEdit = CType(value, string)
												
						Case "CanDelete"
							Me.str().CanDelete = CType(value, string)
												
						Case "CanApprove"
							Me.str().CanApprove = CType(value, string)
												
						Case "CompanyRoleRightID"
							Me.str().CompanyRoleRightID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.RoleID)
							End If
						
						Case "CanView"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanView = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanView)
							End If
						
						Case "CanAdd"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanAdd = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanAdd)
							End If
						
						Case "CanEdit"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanEdit = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanEdit)
							End If
						
						Case "CanDelete"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanDelete = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanDelete)
							End If
						
						Case "CanApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CanApprove)
							End If
						
						Case "CompanyRoleRightID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyRoleRightID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCompanyRoleRightsMetadata.PropertyNames.CompanyRoleRightID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCompanyRoleRights)
				Me.entity = entity
			End Sub				
		
	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightName As System.String 
				Get
					Dim data_ As System.String = entity.RightName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightName = Nothing
					Else
						entity.RightName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanView As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanView
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanView = Nothing
					Else
						entity.CanView = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanAdd As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanAdd
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanAdd = Nothing
					Else
						entity.CanAdd = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanEdit As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanEdit
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanEdit = Nothing
					Else
						entity.CanEdit = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanDelete As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanDelete
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanDelete = Nothing
					Else
						entity.CanDelete = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanApprove = Nothing
					Else
						entity.CanApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyRoleRightID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyRoleRightID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyRoleRightID = Nothing
					Else
						entity.CompanyRoleRightID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCompanyRoleRights
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCompanyRoleRightsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCompanyRoleRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCompanyRoleRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCompanyRoleRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCompanyRoleRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCompanyRoleRightsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCompanyRoleRightsCollection
		Inherits esEntityCollection(Of ICCompanyRoleRights)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCompanyRoleRightsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCompanyRoleRightsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCompanyRoleRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCompanyRoleRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCompanyRoleRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCompanyRoleRightsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCompanyRoleRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCompanyRoleRightsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCompanyRoleRightsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCompanyRoleRightsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCompanyRoleRightsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RoleID" 
					Return Me.RoleID
				Case "RightName" 
					Return Me.RightName
				Case "CanView" 
					Return Me.CanView
				Case "CanAdd" 
					Return Me.CanAdd
				Case "CanEdit" 
					Return Me.CanEdit
				Case "CanDelete" 
					Return Me.CanDelete
				Case "CanApprove" 
					Return Me.CanApprove
				Case "CompanyRoleRightID" 
					Return Me.CompanyRoleRightID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RightName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.RightName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CanView As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CanView, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CanAdd As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CanAdd, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CanEdit As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CanEdit, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CanDelete As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CanDelete, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CanApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CanApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyRoleRightID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCompanyRoleRightsMetadata.ColumnNames.CompanyRoleRightID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCompanyRoleRights 
		Inherits esICCompanyRoleRights
		
	
		#Region "UpToICRoleByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CompanyRoleRights_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRoleID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRoleID = New ICRole()
					Me._UpToICRoleByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
					Me._UpToICRoleByRoleID.Query.Where(Me._UpToICRoleByRoleID.Query.RoleID = Me.RoleID)
					Me._UpToICRoleByRoleID.Query.Load()
				End If

				Return Me._UpToICRoleByRoleID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me._UpToICRoleByRoleID = Nothing
				Else
				
					Me.RoleID = value.RoleID
					
					Me._UpToICRoleByRoleID = value
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCompanyRoleRightsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.RoleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.RoleID
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.RightName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.RightName
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CanView, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CanView
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CanAdd, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CanAdd
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CanEdit, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CanEdit
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CanDelete, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CanDelete
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CanApprove, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CanApprove
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCompanyRoleRightsMetadata.ColumnNames.CompanyRoleRightID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCompanyRoleRightsMetadata.PropertyNames.CompanyRoleRightID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCompanyRoleRightsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RoleID As String = "RoleID"
			 Public Const RightName As String = "RightName"
			 Public Const CanView As String = "CanView"
			 Public Const CanAdd As String = "CanAdd"
			 Public Const CanEdit As String = "CanEdit"
			 Public Const CanDelete As String = "CanDelete"
			 Public Const CanApprove As String = "CanApprove"
			 Public Const CompanyRoleRightID As String = "CompanyRoleRightID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RoleID As String = "RoleID"
			 Public Const RightName As String = "RightName"
			 Public Const CanView As String = "CanView"
			 Public Const CanAdd As String = "CanAdd"
			 Public Const CanEdit As String = "CanEdit"
			 Public Const CanDelete As String = "CanDelete"
			 Public Const CanApprove As String = "CanApprove"
			 Public Const CompanyRoleRightID As String = "CompanyRoleRightID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCompanyRoleRightsMetadata)
			
				If ICCompanyRoleRightsMetadata.mapDelegates Is Nothing Then
					ICCompanyRoleRightsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCompanyRoleRightsMetadata._meta Is Nothing Then
					ICCompanyRoleRightsMetadata._meta = New ICCompanyRoleRightsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RightName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CanView", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CanAdd", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CanEdit", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CanDelete", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CanApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CompanyRoleRightID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_CompanyRoleRights"
				meta.Destination = "IC_CompanyRoleRights"
				
				meta.spInsert = "proc_IC_CompanyRoleRightsInsert"
				meta.spUpdate = "proc_IC_CompanyRoleRightsUpdate"
				meta.spDelete = "proc_IC_CompanyRoleRightsDelete"
				meta.spLoadAll = "proc_IC_CompanyRoleRightsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CompanyRoleRightsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCompanyRoleRightsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
