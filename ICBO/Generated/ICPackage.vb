
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Package' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPackage))> _
	<XmlType("ICPackage")> _	
	Partial Public Class ICPackage 
		Inherits esICPackage
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPackage()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal packageID As System.Int32)
			Dim obj As New ICPackage()
			obj.PackageID = packageID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal packageID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPackage()
			obj.PackageID = packageID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPackageCollection")> _
	Partial Public Class ICPackageCollection
		Inherits esICPackageCollection
		Implements IEnumerable(Of ICPackage)
	
		Public Function FindByPrimaryKey(ByVal packageID As System.Int32) As ICPackage
			Return MyBase.SingleOrDefault(Function(e) e.PackageID.HasValue AndAlso e.PackageID.Value = packageID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPackage))> _
		Public Class ICPackageCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPackageCollection)
			
			Public Shared Widening Operator CType(packet As ICPackageCollectionWCFPacket) As ICPackageCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPackageCollection) As ICPackageCollectionWCFPacket
				Return New ICPackageCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPackageQuery 
		Inherits esICPackageQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPackageQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPackageQuery) As String
			Return ICPackageQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPackageQuery
			Return DirectCast(ICPackageQuery.SerializeHelper.FromXml(query, GetType(ICPackageQuery)), ICPackageQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPackage
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal packageID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(packageID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(packageID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal packageID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(packageID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(packageID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal packageID As System.Int32) As Boolean
		
			Dim query As New ICPackageQuery()
			query.Where(query.PackageID = packageID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal packageID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("PackageID", packageID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Package.PackageID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageMetadata.ColumnNames.PackageID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageMetadata.ColumnNames.PackageID, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.PackageID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.Title
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Title As System.String
			Get
				Return MyBase.GetSystemString(ICPackageMetadata.ColumnNames.Title)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageMetadata.ColumnNames.Title, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.Title)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.DisbursementMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursementMode As System.String
			Get
				Return MyBase.GetSystemString(ICPackageMetadata.ColumnNames.DisbursementMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageMetadata.ColumnNames.DisbursementMode, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.DisbursementMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.PackageType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageType As System.String
			Get
				Return MyBase.GetSystemString(ICPackageMetadata.ColumnNames.PackageType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageMetadata.ColumnNames.PackageType, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.PackageType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.PackageAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageMetadata.ColumnNames.PackageAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageMetadata.ColumnNames.PackageAmount, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.PackageAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.PackagePercentage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackagePercentage As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageMetadata.ColumnNames.PackagePercentage)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageMetadata.ColumnNames.PackagePercentage, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.PackagePercentage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.ChargeFrequency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeFrequency As System.String
			Get
				Return MyBase.GetSystemString(ICPackageMetadata.ColumnNames.ChargeFrequency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageMetadata.ColumnNames.ChargeFrequency, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.ChargeFrequency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPackageMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPackageMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.CreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageMetadata.ColumnNames.CreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageMetadata.ColumnNames.CreatedOn, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.CreatedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Package.LastScheduleRunDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastScheduleRunDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageMetadata.ColumnNames.LastScheduleRunDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageMetadata.ColumnNames.LastScheduleRunDate, value) Then
					OnPropertyChanged(ICPackageMetadata.PropertyNames.LastScheduleRunDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "PackageID"
							Me.str().PackageID = CType(value, string)
												
						Case "Title"
							Me.str().Title = CType(value, string)
												
						Case "DisbursementMode"
							Me.str().DisbursementMode = CType(value, string)
												
						Case "PackageType"
							Me.str().PackageType = CType(value, string)
												
						Case "PackageAmount"
							Me.str().PackageAmount = CType(value, string)
												
						Case "PackagePercentage"
							Me.str().PackagePercentage = CType(value, string)
												
						Case "ChargeFrequency"
							Me.str().ChargeFrequency = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedOn"
							Me.str().CreatedOn = CType(value, string)
												
						Case "LastScheduleRunDate"
							Me.str().LastScheduleRunDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "PackageID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PackageID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.PackageID)
							End If
						
						Case "PackageAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.PackageAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.PackageAmount)
							End If
						
						Case "PackagePercentage"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.PackagePercentage = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.PackagePercentage)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.CreatedOn)
							End If
						
						Case "LastScheduleRunDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastScheduleRunDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageMetadata.PropertyNames.LastScheduleRunDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPackage)
				Me.entity = entity
			End Sub				
		
	
			Public Property PackageID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PackageID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageID = Nothing
					Else
						entity.PackageID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Title As System.String 
				Get
					Dim data_ As System.String = entity.Title
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Title = Nothing
					Else
						entity.Title = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursementMode As System.String 
				Get
					Dim data_ As System.String = entity.DisbursementMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursementMode = Nothing
					Else
						entity.DisbursementMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackageType As System.String 
				Get
					Dim data_ As System.String = entity.PackageType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageType = Nothing
					Else
						entity.PackageType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackageAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.PackageAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageAmount = Nothing
					Else
						entity.PackageAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property PackagePercentage As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.PackagePercentage
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackagePercentage = Nothing
					Else
						entity.PackagePercentage = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeFrequency As System.String 
				Get
					Dim data_ As System.String = entity.ChargeFrequency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeFrequency = Nothing
					Else
						entity.ChargeFrequency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOn = Nothing
					Else
						entity.CreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastScheduleRunDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastScheduleRunDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastScheduleRunDate = Nothing
					Else
						entity.LastScheduleRunDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPackage
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPackageQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPackageQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPackageQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPackageQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPackageCollection
		Inherits esEntityCollection(Of ICPackage)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPackageCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPackageQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPackageQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPackageQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPackageQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPackageQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPackageQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPackageQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPackageMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "PackageID" 
					Return Me.PackageID
				Case "Title" 
					Return Me.Title
				Case "DisbursementMode" 
					Return Me.DisbursementMode
				Case "PackageType" 
					Return Me.PackageType
				Case "PackageAmount" 
					Return Me.PackageAmount
				Case "PackagePercentage" 
					Return Me.PackagePercentage
				Case "ChargeFrequency" 
					Return Me.ChargeFrequency
				Case "IsActive" 
					Return Me.IsActive
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedOn" 
					Return Me.CreatedOn
				Case "LastScheduleRunDate" 
					Return Me.LastScheduleRunDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property PackageID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.PackageID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Title As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.Title, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursementMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.DisbursementMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PackageType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.PackageType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PackageAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.PackageAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property PackagePercentage As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.PackagePercentage, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeFrequency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.ChargeFrequency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.CreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property LastScheduleRunDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageMetadata.ColumnNames.LastScheduleRunDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPackage 
		Inherits esICPackage
		
	
		#Region "ICAccountsPaymentNatureProductTypeCollectionByPackageId - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureProductTypeCollectionByPackageId() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPackage.ICAccountsPaymentNatureProductTypeCollectionByPackageId_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureProductTypeCollectionByPackageId"
				map.MyColumnName = "PackageId"
				map.ParentColumnName = "PackageID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureProductTypeCollectionByPackageId_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPackageQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureProductTypeQuery), New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PackageID = mee.PackageId)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_Package
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureProductTypeCollectionByPackageId As ICAccountsPaymentNatureProductTypeCollection 
		
			Get
				If Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId Is Nothing Then
					Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId = New ICAccountsPaymentNatureProductTypeCollection()
					Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureProductTypeCollectionByPackageId", Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId)
				
					If Not Me.PackageID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId.Query.PackageId = Me.PackageID)
							Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PackageId, Me.PackageID)
					End If
				End If

				Return Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureProductTypeCollectionByPackageId")
					Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureProductTypeCollectionByPackageId As ICAccountsPaymentNatureProductTypeCollection
		#End Region

		#Region "ICPackageChargesCollectionByPacakgeId - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageChargesCollectionByPacakgeId() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPackage.ICPackageChargesCollectionByPacakgeId_Delegate)
				map.PropertyName = "ICPackageChargesCollectionByPacakgeId"
				map.MyColumnName = "PacakgeId"
				map.ParentColumnName = "PackageID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageChargesCollectionByPacakgeId_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPackageQuery(data.NextAlias())
			
			Dim mee As ICPackageChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageChargesQuery), New ICPackageChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PackageID = mee.PacakgeId)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageChargesCollectionByPacakgeId As ICPackageChargesCollection 
		
			Get
				If Me._ICPackageChargesCollectionByPacakgeId Is Nothing Then
					Me._ICPackageChargesCollectionByPacakgeId = New ICPackageChargesCollection()
					Me._ICPackageChargesCollectionByPacakgeId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageChargesCollectionByPacakgeId", Me._ICPackageChargesCollectionByPacakgeId)
				
					If Not Me.PackageID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageChargesCollectionByPacakgeId.Query.Where(Me._ICPackageChargesCollectionByPacakgeId.Query.PacakgeId = Me.PackageID)
							Me._ICPackageChargesCollectionByPacakgeId.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageChargesCollectionByPacakgeId.fks.Add(ICPackageChargesMetadata.ColumnNames.PacakgeId, Me.PackageID)
					End If
				End If

				Return Me._ICPackageChargesCollectionByPacakgeId
			End Get
			
			Set(ByVal value As ICPackageChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageChargesCollectionByPacakgeId Is Nothing Then

					Me.RemovePostSave("ICPackageChargesCollectionByPacakgeId")
					Me._ICPackageChargesCollectionByPacakgeId = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageChargesCollectionByPacakgeId As ICPackageChargesCollection
		#End Region

		#Region "UpToICGlobalChargesCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_MasterPackage_IC_PackageManagement
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICGlobalChargesCollection As ICGlobalChargesCollection
		
			Get
				If Me._UpToICGlobalChargesCollection Is Nothing Then
					Me._UpToICGlobalChargesCollection = New ICGlobalChargesCollection()
					Me._UpToICGlobalChargesCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICGlobalChargesCollection", Me._UpToICGlobalChargesCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.PackageID.Equals(Nothing) Then 
				
						Dim m As New ICGlobalChargesQuery("m")
						Dim j As New ICPackagesGlobalChargesQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.GlobalChargeID = j.GlobalChargeID)
                        m.Where(j.PackageID = Me.PackageID)

						Me._UpToICGlobalChargesCollection.Load(m)

					End If
				End If

				Return Me._UpToICGlobalChargesCollection
			End Get
			
			Set(ByVal value As ICGlobalChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICGlobalChargesCollection Is Nothing Then

					Me.RemovePostSave("UpToICGlobalChargesCollection")
					Me._UpToICGlobalChargesCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_MasterPackage_IC_PackageManagement
		''' </summary>
		Public Sub AssociateICGlobalChargesCollection(entity As ICGlobalCharges)
			If Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Me._ICPackagesGlobalChargesCollection = New ICPackagesGlobalChargesCollection()
				Me._ICPackagesGlobalChargesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICPackagesGlobalChargesCollection", Me._ICPackagesGlobalChargesCollection)
			End If
			
			Dim obj As ICPackagesGlobalCharges = Me._ICPackagesGlobalChargesCollection.AddNew()
			obj.PackageID = Me.PackageID
			obj.GlobalChargeID = entity.GlobalChargeID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_MasterPackage_IC_PackageManagement
		''' </summary>
		Public Sub DissociateICGlobalChargesCollection(entity As ICGlobalCharges)
			If Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Me._ICPackagesGlobalChargesCollection = new ICPackagesGlobalChargesCollection()
				Me._ICPackagesGlobalChargesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICPackagesGlobalChargesCollection", Me._ICPackagesGlobalChargesCollection)
			End If

			Dim obj As ICPackagesGlobalCharges = Me._ICPackagesGlobalChargesCollection.AddNew()
			obj.PackageID = Me.PackageID
            obj.GlobalChargeID = entity.GlobalChargeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICGlobalChargesCollection As ICGlobalChargesCollection
		Private _ICPackagesGlobalChargesCollection As ICPackagesGlobalChargesCollection
		#End Region

		#Region "ICPackagesGlobalChargesCollectionByPackageID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackagesGlobalChargesCollectionByPackageID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPackage.ICPackagesGlobalChargesCollectionByPackageID_Delegate)
				map.PropertyName = "ICPackagesGlobalChargesCollectionByPackageID"
				map.MyColumnName = "PackageID"
				map.ParentColumnName = "PackageID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackagesGlobalChargesCollectionByPackageID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPackageQuery(data.NextAlias())
			
			Dim mee As ICPackagesGlobalChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackagesGlobalChargesQuery), New ICPackagesGlobalChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PackageID = mee.PackageID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MasterPackage_IC_PackageManagement
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackagesGlobalChargesCollectionByPackageID As ICPackagesGlobalChargesCollection 
		
			Get
				If Me._ICPackagesGlobalChargesCollectionByPackageID Is Nothing Then
					Me._ICPackagesGlobalChargesCollectionByPackageID = New ICPackagesGlobalChargesCollection()
					Me._ICPackagesGlobalChargesCollectionByPackageID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackagesGlobalChargesCollectionByPackageID", Me._ICPackagesGlobalChargesCollectionByPackageID)
				
					If Not Me.PackageID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackagesGlobalChargesCollectionByPackageID.Query.Where(Me._ICPackagesGlobalChargesCollectionByPackageID.Query.PackageID = Me.PackageID)
							Me._ICPackagesGlobalChargesCollectionByPackageID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackagesGlobalChargesCollectionByPackageID.fks.Add(ICPackagesGlobalChargesMetadata.ColumnNames.PackageID, Me.PackageID)
					End If
				End If

				Return Me._ICPackagesGlobalChargesCollectionByPackageID
			End Get
			
			Set(ByVal value As ICPackagesGlobalChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackagesGlobalChargesCollectionByPackageID Is Nothing Then

					Me.RemovePostSave("ICPackagesGlobalChargesCollectionByPackageID")
					Me._ICPackagesGlobalChargesCollectionByPackageID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackagesGlobalChargesCollectionByPackageID As ICPackagesGlobalChargesCollection
		#End Region

		#Region "ICPackageSlabsCollectionByPackageID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageSlabsCollectionByPackageID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPackage.ICPackageSlabsCollectionByPackageID_Delegate)
				map.PropertyName = "ICPackageSlabsCollectionByPackageID"
				map.MyColumnName = "PackageID"
				map.ParentColumnName = "PackageID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageSlabsCollectionByPackageID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPackageQuery(data.NextAlias())
			
			Dim mee As ICPackageSlabsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageSlabsQuery), New ICPackageSlabsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PackageID = mee.PackageID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageSlabs_IC_Package
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageSlabsCollectionByPackageID As ICPackageSlabsCollection 
		
			Get
				If Me._ICPackageSlabsCollectionByPackageID Is Nothing Then
					Me._ICPackageSlabsCollectionByPackageID = New ICPackageSlabsCollection()
					Me._ICPackageSlabsCollectionByPackageID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageSlabsCollectionByPackageID", Me._ICPackageSlabsCollectionByPackageID)
				
					If Not Me.PackageID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageSlabsCollectionByPackageID.Query.Where(Me._ICPackageSlabsCollectionByPackageID.Query.PackageID = Me.PackageID)
							Me._ICPackageSlabsCollectionByPackageID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageSlabsCollectionByPackageID.fks.Add(ICPackageSlabsMetadata.ColumnNames.PackageID, Me.PackageID)
					End If
				End If

				Return Me._ICPackageSlabsCollectionByPackageID
			End Get
			
			Set(ByVal value As ICPackageSlabsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageSlabsCollectionByPackageID Is Nothing Then

					Me.RemovePostSave("ICPackageSlabsCollectionByPackageID")
					Me._ICPackageSlabsCollectionByPackageID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageSlabsCollectionByPackageID As ICPackageSlabsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountsPaymentNatureProductTypeCollectionByPackageId"
					coll = Me.ICAccountsPaymentNatureProductTypeCollectionByPackageId
					Exit Select
				Case "ICPackageChargesCollectionByPacakgeId"
					coll = Me.ICPackageChargesCollectionByPacakgeId
					Exit Select
				Case "ICPackagesGlobalChargesCollectionByPackageID"
					coll = Me.ICPackagesGlobalChargesCollectionByPackageID
					Exit Select
				Case "ICPackageSlabsCollectionByPackageID"
					coll = Me.ICPackageSlabsCollectionByPackageID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureProductTypeCollectionByPackageId", GetType(ICAccountsPaymentNatureProductTypeCollection), New ICAccountsPaymentNatureProductType()))
			props.Add(new esPropertyDescriptor(Me, "ICPackageChargesCollectionByPacakgeId", GetType(ICPackageChargesCollection), New ICPackageCharges()))
			props.Add(new esPropertyDescriptor(Me, "ICPackagesGlobalChargesCollectionByPackageID", GetType(ICPackagesGlobalChargesCollection), New ICPackagesGlobalCharges()))
			props.Add(new esPropertyDescriptor(Me, "ICPackageSlabsCollectionByPackageID", GetType(ICPackageSlabsCollection), New ICPackageSlabs()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId Is Nothing Then
				Apply(Me._ICAccountsPaymentNatureProductTypeCollectionByPackageId, "PackageId", Me.PackageID)
			End If
			
			If Not Me._ICPackageChargesCollectionByPacakgeId Is Nothing Then
				Apply(Me._ICPackageChargesCollectionByPacakgeId, "PacakgeId", Me.PackageID)
			End If
			
			If Not Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Apply(Me._ICPackagesGlobalChargesCollection, "PackageID", Me.PackageID)
			End If
			
			If Not Me._ICPackagesGlobalChargesCollectionByPackageID Is Nothing Then
				Apply(Me._ICPackagesGlobalChargesCollectionByPackageID, "PackageID", Me.PackageID)
			End If
			
			If Not Me._ICPackageSlabsCollectionByPackageID Is Nothing Then
				Apply(Me._ICPackageSlabsCollectionByPackageID, "PackageID", Me.PackageID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPackageMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.PackageID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageMetadata.PropertyNames.PackageID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.Title, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageMetadata.PropertyNames.Title
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.DisbursementMode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageMetadata.PropertyNames.DisbursementMode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.PackageType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageMetadata.PropertyNames.PackageType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.PackageAmount, 4, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageMetadata.PropertyNames.PackageAmount
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.PackagePercentage, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageMetadata.PropertyNames.PackagePercentage
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.ChargeFrequency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageMetadata.PropertyNames.ChargeFrequency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPackageMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.CreatedBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.CreatedOn, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageMetadata.PropertyNames.CreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageMetadata.ColumnNames.LastScheduleRunDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageMetadata.PropertyNames.LastScheduleRunDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPackageMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const PackageID As String = "PackageID"
			 Public Const Title As String = "Title"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const PackageType As String = "PackageType"
			 Public Const PackageAmount As String = "PackageAmount"
			 Public Const PackagePercentage As String = "PackagePercentage"
			 Public Const ChargeFrequency As String = "ChargeFrequency"
			 Public Const IsActive As String = "isActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const LastScheduleRunDate As String = "LastScheduleRunDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const PackageID As String = "PackageID"
			 Public Const Title As String = "Title"
			 Public Const DisbursementMode As String = "DisbursementMode"
			 Public Const PackageType As String = "PackageType"
			 Public Const PackageAmount As String = "PackageAmount"
			 Public Const PackagePercentage As String = "PackagePercentage"
			 Public Const ChargeFrequency As String = "ChargeFrequency"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const LastScheduleRunDate As String = "LastScheduleRunDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPackageMetadata)
			
				If ICPackageMetadata.mapDelegates Is Nothing Then
					ICPackageMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPackageMetadata._meta Is Nothing Then
					ICPackageMetadata._meta = New ICPackageMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("PackageID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Title", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbursementMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PackageType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PackageAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("PackagePercentage", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("ChargeFrequency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("LastScheduleRunDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Package"
				meta.Destination = "IC_Package"
				
				meta.spInsert = "proc_IC_PackageInsert"
				meta.spUpdate = "proc_IC_PackageUpdate"
				meta.spDelete = "proc_IC_PackageDelete"
				meta.spLoadAll = "proc_IC_PackageLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PackageLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPackageMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
