
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_OnlineFormSettings' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICOnlineFormSettings))> _
	<XmlType("ICOnlineFormSettings")> _	
	Partial Public Class ICOnlineFormSettings 
		Inherits esICOnlineFormSettings
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICOnlineFormSettings()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal onlineFormSettingsID As System.Int32)
			Dim obj As New ICOnlineFormSettings()
			obj.OnlineFormSettingsID = onlineFormSettingsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal onlineFormSettingsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICOnlineFormSettings()
			obj.OnlineFormSettingsID = onlineFormSettingsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICOnlineFormSettingsCollection")> _
	Partial Public Class ICOnlineFormSettingsCollection
		Inherits esICOnlineFormSettingsCollection
		Implements IEnumerable(Of ICOnlineFormSettings)
	
		Public Function FindByPrimaryKey(ByVal onlineFormSettingsID As System.Int32) As ICOnlineFormSettings
			Return MyBase.SingleOrDefault(Function(e) e.OnlineFormSettingsID.HasValue AndAlso e.OnlineFormSettingsID.Value = onlineFormSettingsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICOnlineFormSettings))> _
		Public Class ICOnlineFormSettingsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICOnlineFormSettingsCollection)
			
			Public Shared Widening Operator CType(packet As ICOnlineFormSettingsCollectionWCFPacket) As ICOnlineFormSettingsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICOnlineFormSettingsCollection) As ICOnlineFormSettingsCollectionWCFPacket
				Return New ICOnlineFormSettingsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICOnlineFormSettingsQuery 
		Inherits esICOnlineFormSettingsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICOnlineFormSettingsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICOnlineFormSettingsQuery) As String
			Return ICOnlineFormSettingsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICOnlineFormSettingsQuery
			Return DirectCast(ICOnlineFormSettingsQuery.SerializeHelper.FromXml(query, GetType(ICOnlineFormSettingsQuery)), ICOnlineFormSettingsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICOnlineFormSettings
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal onlineFormSettingsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(onlineFormSettingsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(onlineFormSettingsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal onlineFormSettingsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(onlineFormSettingsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(onlineFormSettingsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal onlineFormSettingsID As System.Int32) As Boolean
		
			Dim query As New ICOnlineFormSettingsQuery()
			query.Where(query.OnlineFormSettingsID = onlineFormSettingsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal onlineFormSettingsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("OnlineFormSettingsID", onlineFormSettingsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_OnlineFormSettings.OnlineFormSettingsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OnlineFormSettingsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.OnlineFormSettingsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.OnlineFormSettingsID, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.OnlineFormSettingsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.IsRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsRequired, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.IsVisible
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsVisible As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsVisible)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsVisible, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsVisible)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOnlineFormSettingsMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.ApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedDate, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.ApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormSettingsMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormSettingsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineFormSettings.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOnlineFormSettingsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "OnlineFormSettingsID"
							Me.str().OnlineFormSettingsID = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "IsRequired"
							Me.str().IsRequired = CType(value, string)
												
						Case "IsVisible"
							Me.str().IsVisible = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedDate"
							Me.str().ApprovedDate = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "OnlineFormSettingsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OnlineFormSettingsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.OnlineFormSettingsID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.FieldID)
							End If
						
						Case "IsRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsRequired)
							End If
						
						Case "IsVisible"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsVisible = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsVisible)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.ApprovedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOnlineFormSettingsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICOnlineFormSettings)
				Me.entity = entity
			End Sub				
		
	
			Public Property OnlineFormSettingsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OnlineFormSettingsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OnlineFormSettingsID = Nothing
					Else
						entity.OnlineFormSettingsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequired = Nothing
					Else
						entity.IsRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsVisible As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsVisible
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsVisible = Nothing
					Else
						entity.IsVisible = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedDate = Nothing
					Else
						entity.ApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICOnlineFormSettings
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOnlineFormSettingsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICOnlineFormSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOnlineFormSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICOnlineFormSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICOnlineFormSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICOnlineFormSettingsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICOnlineFormSettingsCollection
		Inherits esEntityCollection(Of ICOnlineFormSettings)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOnlineFormSettingsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICOnlineFormSettingsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICOnlineFormSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOnlineFormSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICOnlineFormSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICOnlineFormSettingsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICOnlineFormSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICOnlineFormSettingsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICOnlineFormSettingsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICOnlineFormSettingsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICOnlineFormSettingsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "OnlineFormSettingsID" 
					Return Me.OnlineFormSettingsID
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "FieldID" 
					Return Me.FieldID
				Case "IsRequired" 
					Return Me.IsRequired
				Case "IsVisible" 
					Return Me.IsVisible
				Case "IsActive" 
					Return Me.IsActive
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedDate" 
					Return Me.ApprovedDate
				Case "Currency" 
					Return Me.Currency
				Case "BranchCode" 
					Return Me.BranchCode
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property OnlineFormSettingsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.OnlineFormSettingsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.IsRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsVisible As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.IsVisible, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.ApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormSettingsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICOnlineFormSettings 
		Inherits esICOnlineFormSettings
		
	
		#Region "UpToICAccountsPaymentNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_OnlineFormSettings_IC_AccountsPaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureByAccountNumber = New ICAccountsPaymentNature()
					Me._UpToICAccountsPaymentNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNature)
				Me.RemovePreSave("UpToICAccountsPaymentNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICAccountsPaymentNatureByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_OnlineFormSettings_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICOnlineFormSettingsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.OnlineFormSettingsID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.OnlineFormSettingsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.CompanyCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.PaymentNatureCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.ProductTypeCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.FieldID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.FieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.IsRequired, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.IsRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.IsVisible, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.IsVisible
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.CreatedBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.CreatedDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.IsApproved, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedBy, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.ApprovedDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.ApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.Currency, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.BranchCode, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.AccountNumber, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.Creater, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormSettingsMetadata.ColumnNames.CreationDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOnlineFormSettingsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICOnlineFormSettingsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const OnlineFormSettingsID As String = "OnlineFormSettingsID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsVisible As String = "IsVisible"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const Currency As String = "Currency"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const OnlineFormSettingsID As String = "OnlineFormSettingsID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsVisible As String = "IsVisible"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const Currency As String = "Currency"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICOnlineFormSettingsMetadata)
			
				If ICOnlineFormSettingsMetadata.mapDelegates Is Nothing Then
					ICOnlineFormSettingsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICOnlineFormSettingsMetadata._meta Is Nothing Then
					ICOnlineFormSettingsMetadata._meta = New ICOnlineFormSettingsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("OnlineFormSettingsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsVisible", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_OnlineFormSettings"
				meta.Destination = "IC_OnlineFormSettings"
				
				meta.spInsert = "proc_IC_OnlineFormSettingsInsert"
				meta.spUpdate = "proc_IC_OnlineFormSettingsUpdate"
				meta.spDelete = "proc_IC_OnlineFormSettingsDelete"
				meta.spLoadAll = "proc_IC_OnlineFormSettingsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_OnlineFormSettingsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICOnlineFormSettingsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
