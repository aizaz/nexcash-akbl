
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/22/2014 7:26:23 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountPaymentNatureFileUploadTemplatefields' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountPaymentNatureFileUploadTemplatefields))> _
	<XmlType("ICAccountPaymentNatureFileUploadTemplatefields")> _	
	Partial Public Class ICAccountPaymentNatureFileUploadTemplatefields 
		Inherits esICAccountPaymentNatureFileUploadTemplatefields
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountPaymentNatureFileUploadTemplatefields()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal aPNFUTemplateID As System.Int32)
			Dim obj As New ICAccountPaymentNatureFileUploadTemplatefields()
			obj.APNFUTemplateID = aPNFUTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal aPNFUTemplateID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountPaymentNatureFileUploadTemplatefields()
			obj.APNFUTemplateID = aPNFUTemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountPaymentNatureFileUploadTemplatefieldsCollection")> _
	Partial Public Class ICAccountPaymentNatureFileUploadTemplatefieldsCollection
		Inherits esICAccountPaymentNatureFileUploadTemplatefieldsCollection
		Implements IEnumerable(Of ICAccountPaymentNatureFileUploadTemplatefields)
	
		Public Function FindByPrimaryKey(ByVal aPNFUTemplateID As System.Int32) As ICAccountPaymentNatureFileUploadTemplatefields
			Return MyBase.SingleOrDefault(Function(e) e.APNFUTemplateID.HasValue AndAlso e.APNFUTemplateID.Value = aPNFUTemplateID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountPaymentNatureFileUploadTemplatefields))> _
		Public Class ICAccountPaymentNatureFileUploadTemplatefieldsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountPaymentNatureFileUploadTemplatefieldsCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountPaymentNatureFileUploadTemplatefieldsCollectionWCFPacket) As ICAccountPaymentNatureFileUploadTemplatefieldsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountPaymentNatureFileUploadTemplatefieldsCollection) As ICAccountPaymentNatureFileUploadTemplatefieldsCollectionWCFPacket
				Return New ICAccountPaymentNatureFileUploadTemplatefieldsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountPaymentNatureFileUploadTemplatefieldsQuery 
		Inherits esICAccountPaymentNatureFileUploadTemplatefieldsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountPaymentNatureFileUploadTemplatefieldsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery) As String
			Return ICAccountPaymentNatureFileUploadTemplatefieldsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountPaymentNatureFileUploadTemplatefieldsQuery
			Return DirectCast(ICAccountPaymentNatureFileUploadTemplatefieldsQuery.SerializeHelper.FromXml(query, GetType(ICAccountPaymentNatureFileUploadTemplatefieldsQuery)), ICAccountPaymentNatureFileUploadTemplatefieldsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureFileUploadTemplatefields
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal aPNFUTemplateID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(aPNFUTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(aPNFUTemplateID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal aPNFUTemplateID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(aPNFUTemplateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(aPNFUTemplateID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal aPNFUTemplateID As System.Int32) As Boolean
		
			Dim query As New ICAccountPaymentNatureFileUploadTemplatefieldsQuery()
			query.Where(query.APNFUTemplateID = aPNFUTemplateID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal aPNFUTemplateID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("APNFUTemplateID", aPNFUTemplateID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.TemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.TemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.TemplateID, value) Then
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.TemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FixLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FixLength, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.isRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequired, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.MustRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MustRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.MustRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.MustRequired, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.MustRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.APNFUTemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property APNFUTemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.APNFUTemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.APNFUTemplateID, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.APNFUTemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FlexiFieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FlexiFieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FlexiFieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FlexiFieldID, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FlexiFieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.FieldOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldOrder, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredIBFT, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredFT, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredCQ
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCQ As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCQ)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCQ, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCQ)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredDD, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredPO, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredIBFT, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredFT, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredCQ
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredCQ As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCQ)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCQ, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCQ)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredDD, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredPO, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredCOTC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCOTC As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCOTC)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCOTC, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCOTC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredCOTC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredCOTC As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCOTC)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCOTC, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCOTC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsRequiredBillPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredBillPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredBillPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredBillPayment, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredBillPayment)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureFileUploadTemplatefields.IsMustRequiredBillPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredBillPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredBillPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredBillPayment, value) Then
					OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredBillPayment)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountPayNatureFileUploadTemplateByTemplateID As ICAccountPayNatureFileUploadTemplate
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "TemplateID"
							Me.str().TemplateID = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FixLength"
							Me.str().FixLength = CType(value, string)
												
						Case "IsRequired"
							Me.str().IsRequired = CType(value, string)
												
						Case "MustRequired"
							Me.str().MustRequired = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "APNFUTemplateID"
							Me.str().APNFUTemplateID = CType(value, string)
												
						Case "FlexiFieldID"
							Me.str().FlexiFieldID = CType(value, string)
												
						Case "FieldOrder"
							Me.str().FieldOrder = CType(value, string)
												
						Case "IsRequiredIBFT"
							Me.str().IsRequiredIBFT = CType(value, string)
												
						Case "IsRequiredFT"
							Me.str().IsRequiredFT = CType(value, string)
												
						Case "IsRequiredCQ"
							Me.str().IsRequiredCQ = CType(value, string)
												
						Case "IsRequiredDD"
							Me.str().IsRequiredDD = CType(value, string)
												
						Case "IsRequiredPO"
							Me.str().IsRequiredPO = CType(value, string)
												
						Case "IsMustRequiredIBFT"
							Me.str().IsMustRequiredIBFT = CType(value, string)
												
						Case "IsMustRequiredFT"
							Me.str().IsMustRequiredFT = CType(value, string)
												
						Case "IsMustRequiredCQ"
							Me.str().IsMustRequiredCQ = CType(value, string)
												
						Case "IsMustRequiredDD"
							Me.str().IsMustRequiredDD = CType(value, string)
												
						Case "IsMustRequiredPO"
							Me.str().IsMustRequiredPO = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsRequiredCOTC"
							Me.str().IsRequiredCOTC = CType(value, string)
												
						Case "IsMustRequiredCOTC"
							Me.str().IsMustRequiredCOTC = CType(value, string)
												
						Case "IsRequiredBillPayment"
							Me.str().IsRequiredBillPayment = CType(value, string)
												
						Case "IsMustRequiredBillPayment"
							Me.str().IsMustRequiredBillPayment = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "TemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.TemplateID)
							End If
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldID)
							End If
						
						Case "FixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FixLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FixLength)
							End If
						
						Case "IsRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequired)
							End If
						
						Case "MustRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.MustRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.MustRequired)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "APNFUTemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.APNFUTemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.APNFUTemplateID)
							End If
						
						Case "FlexiFieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FlexiFieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FlexiFieldID)
							End If
						
						Case "FieldOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldOrder)
							End If
						
						Case "IsRequiredIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredIBFT)
							End If
						
						Case "IsRequiredFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredFT)
							End If
						
						Case "IsRequiredCQ"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCQ = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCQ)
							End If
						
						Case "IsRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredDD)
							End If
						
						Case "IsRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredPO)
							End If
						
						Case "IsMustRequiredIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredIBFT)
							End If
						
						Case "IsMustRequiredFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredFT)
							End If
						
						Case "IsMustRequiredCQ"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredCQ = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCQ)
							End If
						
						Case "IsMustRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredDD)
							End If
						
						Case "IsMustRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredPO)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsRequiredCOTC"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCOTC = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCOTC)
							End If
						
						Case "IsMustRequiredCOTC"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredCOTC = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCOTC)
							End If
						
						Case "IsRequiredBillPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredBillPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredBillPayment)
							End If
						
						Case "IsMustRequiredBillPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredBillPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredBillPayment)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountPaymentNatureFileUploadTemplatefields)
				Me.entity = entity
			End Sub				
		
	
			Public Property TemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateID = Nothing
					Else
						entity.TemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FixLength = Nothing
					Else
						entity.FixLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequired = Nothing
					Else
						entity.IsRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property MustRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.MustRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MustRequired = Nothing
					Else
						entity.MustRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property APNFUTemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.APNFUTemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.APNFUTemplateID = Nothing
					Else
						entity.APNFUTemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FlexiFieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FlexiFieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FlexiFieldID = Nothing
					Else
						entity.FlexiFieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldOrder = Nothing
					Else
						entity.FieldOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredIBFT = Nothing
					Else
						entity.IsRequiredIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFT = Nothing
					Else
						entity.IsRequiredFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCQ As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCQ
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCQ = Nothing
					Else
						entity.IsRequiredCQ = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredDD = Nothing
					Else
						entity.IsRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredPO = Nothing
					Else
						entity.IsRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredIBFT = Nothing
					Else
						entity.IsMustRequiredIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredFT = Nothing
					Else
						entity.IsMustRequiredFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredCQ As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredCQ
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredCQ = Nothing
					Else
						entity.IsMustRequiredCQ = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredDD = Nothing
					Else
						entity.IsMustRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredPO = Nothing
					Else
						entity.IsMustRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCOTC As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCOTC
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCOTC = Nothing
					Else
						entity.IsRequiredCOTC = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredCOTC As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredCOTC
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredCOTC = Nothing
					Else
						entity.IsMustRequiredCOTC = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredBillPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredBillPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredBillPayment = Nothing
					Else
						entity.IsRequiredBillPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredBillPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredBillPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredBillPayment = Nothing
					Else
						entity.IsMustRequiredBillPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountPaymentNatureFileUploadTemplatefields
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountPaymentNatureFileUploadTemplatefieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureFileUploadTemplatefieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureFileUploadTemplatefieldsCollection
		Inherits esEntityCollection(Of ICAccountPaymentNatureFileUploadTemplatefields)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountPaymentNatureFileUploadTemplatefieldsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountPaymentNatureFileUploadTemplatefieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureFileUploadTemplatefieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountPaymentNatureFileUploadTemplatefieldsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountPaymentNatureFileUploadTemplatefieldsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountPaymentNatureFileUploadTemplatefieldsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountPaymentNatureFileUploadTemplatefieldsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "TemplateID" 
					Return Me.TemplateID
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "FieldID" 
					Return Me.FieldID
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldName" 
					Return Me.FieldName
				Case "FixLength" 
					Return Me.FixLength
				Case "IsRequired" 
					Return Me.IsRequired
				Case "MustRequired" 
					Return Me.MustRequired
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "APNFUTemplateID" 
					Return Me.APNFUTemplateID
				Case "FlexiFieldID" 
					Return Me.FlexiFieldID
				Case "FieldOrder" 
					Return Me.FieldOrder
				Case "IsRequiredIBFT" 
					Return Me.IsRequiredIBFT
				Case "IsRequiredFT" 
					Return Me.IsRequiredFT
				Case "IsRequiredCQ" 
					Return Me.IsRequiredCQ
				Case "IsRequiredDD" 
					Return Me.IsRequiredDD
				Case "IsRequiredPO" 
					Return Me.IsRequiredPO
				Case "IsMustRequiredIBFT" 
					Return Me.IsMustRequiredIBFT
				Case "IsMustRequiredFT" 
					Return Me.IsMustRequiredFT
				Case "IsMustRequiredCQ" 
					Return Me.IsMustRequiredCQ
				Case "IsMustRequiredDD" 
					Return Me.IsMustRequiredDD
				Case "IsMustRequiredPO" 
					Return Me.IsMustRequiredPO
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsRequiredCOTC" 
					Return Me.IsRequiredCOTC
				Case "IsMustRequiredCOTC" 
					Return Me.IsMustRequiredCOTC
				Case "IsRequiredBillPayment" 
					Return Me.IsRequiredBillPayment
				Case "IsMustRequiredBillPayment" 
					Return Me.IsMustRequiredBillPayment
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property TemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.TemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FixLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property MustRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.MustRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property APNFUTemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.APNFUTemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FlexiFieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FlexiFieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCQ As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCQ, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredCQ As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCQ, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCOTC As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCOTC, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredCOTC As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCOTC, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredBillPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredBillPayment, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredBillPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredBillPayment, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountPaymentNatureFileUploadTemplatefields 
		Inherits esICAccountPaymentNatureFileUploadTemplatefields
		
	
		#Region "UpToICAccountPayNatureFileUploadTemplateByTemplateID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountPaymentNatureFileUploadTemplatefields_IC_AccountPayNatureFileUploadTemplate
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountPayNatureFileUploadTemplateByTemplateID As ICAccountPayNatureFileUploadTemplate
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID Is Nothing _
						 AndAlso Not TemplateID.Equals(Nothing)  AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = New ICAccountPayNatureFileUploadTemplate()
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountPayNatureFileUploadTemplateByTemplateID", Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Where(Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.TemplateID = Me.TemplateID)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Where(Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Where(Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Where(Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Currency = Me.Currency)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Where(Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID.Query.Load()
				End If

				Return Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID
			End Get
			
            Set(ByVal value As ICAccountPayNatureFileUploadTemplate)
				Me.RemovePreSave("UpToICAccountPayNatureFileUploadTemplateByTemplateID")
				

				If value Is Nothing Then
				
					Me.TemplateID = Nothing
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = Nothing
				Else
				
					Me.TemplateID = value.TemplateID
					
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID = value
					Me.SetPreSave("UpToICAccountPayNatureFileUploadTemplateByTemplateID", Me._UpToICAccountPayNatureFileUploadTemplateByTemplateID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountPaymentNatureFileUploadTemplatefieldsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.TemplateID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.TemplateID
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.AccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.BranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldType, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldName, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FixLength, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FixLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequired, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.MustRequired, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.MustRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreatedDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.PaymentNatureCode, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.APNFUTemplateID, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.APNFUTemplateID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FlexiFieldID, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FlexiFieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.FieldOrder, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.FieldOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredIBFT, 16, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredFT, 17, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCQ, 18, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCQ
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredDD, 19, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredPO, 20, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredIBFT, 21, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredFT, 22, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCQ, 23, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCQ
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredDD, 24, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredPO, 25, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.Creater, 26, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.CreationDate, 27, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredCOTC, 28, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredCOTC
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredCOTC, 29, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredCOTC
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsRequiredBillPayment, 30, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsRequiredBillPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.ColumnNames.IsMustRequiredBillPayment, 31, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.PropertyNames.IsMustRequiredBillPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountPaymentNatureFileUploadTemplatefieldsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequired As String = "isRequired"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const APNFUTemplateID As String = "APNFUTemplateID"
			 Public Const FlexiFieldID As String = "FlexiFieldID"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsRequiredCQ As String = "IsRequiredCQ"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsMustRequiredIBFT As String = "IsMustRequiredIBFT"
			 Public Const IsMustRequiredFT As String = "IsMustRequiredFT"
			 Public Const IsMustRequiredCQ As String = "IsMustRequiredCQ"
			 Public Const IsMustRequiredDD As String = "IsMustRequiredDD"
			 Public Const IsMustRequiredPO As String = "IsMustRequiredPO"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsMustRequiredCOTC As String = "IsMustRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
			 Public Const IsMustRequiredBillPayment As String = "IsMustRequiredBillPayment"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const TemplateID As String = "TemplateID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const APNFUTemplateID As String = "APNFUTemplateID"
			 Public Const FlexiFieldID As String = "FlexiFieldID"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsRequiredCQ As String = "IsRequiredCQ"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsMustRequiredIBFT As String = "IsMustRequiredIBFT"
			 Public Const IsMustRequiredFT As String = "IsMustRequiredFT"
			 Public Const IsMustRequiredCQ As String = "IsMustRequiredCQ"
			 Public Const IsMustRequiredDD As String = "IsMustRequiredDD"
			 Public Const IsMustRequiredPO As String = "IsMustRequiredPO"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsMustRequiredCOTC As String = "IsMustRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
			 Public Const IsMustRequiredBillPayment As String = "IsMustRequiredBillPayment"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountPaymentNatureFileUploadTemplatefieldsMetadata)
			
				If ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.mapDelegates Is Nothing Then
					ICAccountPaymentNatureFileUploadTemplatefieldsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountPaymentNatureFileUploadTemplatefieldsMetadata._meta Is Nothing Then
					ICAccountPaymentNatureFileUploadTemplatefieldsMetadata._meta = New ICAccountPaymentNatureFileUploadTemplatefieldsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("TemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FixLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("MustRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("APNFUTemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FlexiFieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequiredIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredCQ", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredCQ", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsRequiredCOTC", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredCOTC", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredBillPayment", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredBillPayment", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_AccountPaymentNatureFileUploadTemplatefields"
				meta.Destination = "IC_AccountPaymentNatureFileUploadTemplatefields"
				
				meta.spInsert = "proc_IC_AccountPaymentNatureFileUploadTemplatefieldsInsert"
				meta.spUpdate = "proc_IC_AccountPaymentNatureFileUploadTemplatefieldsUpdate"
				meta.spDelete = "proc_IC_AccountPaymentNatureFileUploadTemplatefieldsDelete"
				meta.spLoadAll = "proc_IC_AccountPaymentNatureFileUploadTemplatefieldsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountPaymentNatureFileUploadTemplatefieldsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountPaymentNatureFileUploadTemplatefieldsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
