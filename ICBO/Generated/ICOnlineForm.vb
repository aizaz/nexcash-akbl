
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_OnlineForm' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICOnlineForm))> _
	<XmlType("ICOnlineForm")> _	
	Partial Public Class ICOnlineForm 
		Inherits esICOnlineForm
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICOnlineForm()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal onlineFormID As System.Int32)
			Dim obj As New ICOnlineForm()
			obj.OnlineFormID = onlineFormID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal onlineFormID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICOnlineForm()
			obj.OnlineFormID = onlineFormID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICOnlineFormCollection")> _
	Partial Public Class ICOnlineFormCollection
		Inherits esICOnlineFormCollection
		Implements IEnumerable(Of ICOnlineForm)
	
		Public Function FindByPrimaryKey(ByVal onlineFormID As System.Int32) As ICOnlineForm
			Return MyBase.SingleOrDefault(Function(e) e.OnlineFormID.HasValue AndAlso e.OnlineFormID.Value = onlineFormID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICOnlineForm))> _
		Public Class ICOnlineFormCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICOnlineFormCollection)
			
			Public Shared Widening Operator CType(packet As ICOnlineFormCollectionWCFPacket) As ICOnlineFormCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICOnlineFormCollection) As ICOnlineFormCollectionWCFPacket
				Return New ICOnlineFormCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICOnlineFormQuery 
		Inherits esICOnlineFormQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICOnlineFormQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICOnlineFormQuery) As String
			Return ICOnlineFormQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICOnlineFormQuery
			Return DirectCast(ICOnlineFormQuery.SerializeHelper.FromXml(query, GetType(ICOnlineFormQuery)), ICOnlineFormQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICOnlineForm
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal onlineFormID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(onlineFormID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(onlineFormID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal onlineFormID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(onlineFormID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(onlineFormID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal onlineFormID As System.Int32) As Boolean
		
			Dim query As New ICOnlineFormQuery()
			query.Where(query.OnlineFormID = onlineFormID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal onlineFormID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("OnlineFormID", onlineFormID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_OnlineForm.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineForm.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineForm.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineForm.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICOnlineFormMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOnlineFormMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_OnlineForm.OnlineFormID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OnlineFormID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOnlineFormMetadata.ColumnNames.OnlineFormID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOnlineFormMetadata.ColumnNames.OnlineFormID, value) Then
					OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.OnlineFormID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "OnlineFormID"
							Me.str().OnlineFormID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "OnlineFormID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OnlineFormID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOnlineFormMetadata.PropertyNames.OnlineFormID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICOnlineForm)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OnlineFormID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OnlineFormID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OnlineFormID = Nothing
					Else
						entity.OnlineFormID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICOnlineForm
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOnlineFormMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICOnlineFormQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOnlineFormQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICOnlineFormQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICOnlineFormQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICOnlineFormQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICOnlineFormCollection
		Inherits esEntityCollection(Of ICOnlineForm)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOnlineFormMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICOnlineFormCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICOnlineFormQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOnlineFormQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICOnlineFormQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICOnlineFormQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICOnlineFormQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICOnlineFormQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICOnlineFormQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICOnlineFormQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICOnlineFormMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "OnlineFormID" 
					Return Me.OnlineFormID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OnlineFormID As esQueryItem
			Get
				Return New esQueryItem(Me, ICOnlineFormMetadata.ColumnNames.OnlineFormID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICOnlineForm 
		Inherits esICOnlineForm
		
	
		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_OnlineForm_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICOnlineFormMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICOnlineFormMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOnlineFormMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOnlineFormMetadata.ColumnNames.OnlineFormID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOnlineFormMetadata.PropertyNames.OnlineFormID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICOnlineFormMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const OnlineFormID As String = "OnlineFormID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const OnlineFormID As String = "OnlineFormID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICOnlineFormMetadata)
			
				If ICOnlineFormMetadata.mapDelegates Is Nothing Then
					ICOnlineFormMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICOnlineFormMetadata._meta Is Nothing Then
					ICOnlineFormMetadata._meta = New ICOnlineFormMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OnlineFormID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_OnlineForm"
				meta.Destination = "IC_OnlineForm"
				
				meta.spInsert = "proc_IC_OnlineFormInsert"
				meta.spUpdate = "proc_IC_OnlineFormUpdate"
				meta.spDelete = "proc_IC_OnlineFormDelete"
				meta.spLoadAll = "proc_IC_OnlineFormLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_OnlineFormLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICOnlineFormMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
