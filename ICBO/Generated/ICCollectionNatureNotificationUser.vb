
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:40 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionNatureNotificationUser' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionNatureNotificationUser))> _
	<XmlType("ICCollectionNatureNotificationUser")> _	
	Partial Public Class ICCollectionNatureNotificationUser 
		Inherits esICCollectionNatureNotificationUser
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionNatureNotificationUser()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collNatureNotifiUserID As System.Int32)
			Dim obj As New ICCollectionNatureNotificationUser()
			obj.CollNatureNotifiUserID = collNatureNotifiUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collNatureNotifiUserID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionNatureNotificationUser()
			obj.CollNatureNotifiUserID = collNatureNotifiUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionNatureNotificationUserCollection")> _
	Partial Public Class ICCollectionNatureNotificationUserCollection
		Inherits esICCollectionNatureNotificationUserCollection
		Implements IEnumerable(Of ICCollectionNatureNotificationUser)
	
		Public Function FindByPrimaryKey(ByVal collNatureNotifiUserID As System.Int32) As ICCollectionNatureNotificationUser
			Return MyBase.SingleOrDefault(Function(e) e.CollNatureNotifiUserID.HasValue AndAlso e.CollNatureNotifiUserID.Value = collNatureNotifiUserID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionNatureNotificationUser))> _
		Public Class ICCollectionNatureNotificationUserCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionNatureNotificationUserCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionNatureNotificationUserCollectionWCFPacket) As ICCollectionNatureNotificationUserCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionNatureNotificationUserCollection) As ICCollectionNatureNotificationUserCollectionWCFPacket
				Return New ICCollectionNatureNotificationUserCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionNatureNotificationUserQuery 
		Inherits esICCollectionNatureNotificationUserQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionNatureNotificationUserQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionNatureNotificationUserQuery) As String
			Return ICCollectionNatureNotificationUserQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionNatureNotificationUserQuery
			Return DirectCast(ICCollectionNatureNotificationUserQuery.SerializeHelper.FromXml(query, GetType(ICCollectionNatureNotificationUserQuery)), ICCollectionNatureNotificationUserQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionNatureNotificationUser
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collNatureNotifiUserID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collNatureNotifiUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collNatureNotifiUserID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collNatureNotifiUserID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collNatureNotifiUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collNatureNotifiUserID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collNatureNotifiUserID As System.Int32) As Boolean
		
			Dim query As New ICCollectionNatureNotificationUserQuery()
			query.Where(query.CollNatureNotifiUserID = collNatureNotifiUserID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collNatureNotifiUserID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollNatureNotifiUserID", collNatureNotifiUserID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionNatureNotificationUser.CollNatureNotifiUserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollNatureNotifiUserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollNatureNotifiUserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollNatureNotifiUserID, value) Then
					OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.CollNatureNotifiUserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNatureNotificationUser.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollectionNatureCode, value) Then
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
					Me.OnPropertyChanged("UpToICCollectionNatureByCollectionNatureCode")
					OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNatureNotificationUser.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionNatureNotificationUserMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionNatureNotificationUserMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNatureNotificationUser.EventType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EventType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionNatureNotificationUserMetadata.ColumnNames.EventType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionNatureNotificationUserMetadata.ColumnNames.EventType, value) Then
					OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.EventType)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollNatureNotifiUserID"
							Me.str().CollNatureNotifiUserID = CType(value, string)
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "EventType"
							Me.str().EventType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CollNatureNotifiUserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollNatureNotifiUserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.CollNatureNotifiUserID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionNatureNotificationUserMetadata.PropertyNames.UserID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionNatureNotificationUser)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollNatureNotifiUserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollNatureNotifiUserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollNatureNotifiUserID = Nothing
					Else
						entity.CollNatureNotifiUserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property EventType As System.String 
				Get
					Dim data_ As System.String = entity.EventType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EventType = Nothing
					Else
						entity.EventType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionNatureNotificationUser
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionNatureNotificationUserMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionNatureNotificationUserQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionNatureNotificationUserQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionNatureNotificationUserQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionNatureNotificationUserQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionNatureNotificationUserQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionNatureNotificationUserCollection
		Inherits esEntityCollection(Of ICCollectionNatureNotificationUser)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionNatureNotificationUserMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionNatureNotificationUserCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionNatureNotificationUserQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionNatureNotificationUserQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionNatureNotificationUserQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionNatureNotificationUserQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionNatureNotificationUserQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionNatureNotificationUserQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionNatureNotificationUserQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionNatureNotificationUserQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionNatureNotificationUserMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollNatureNotifiUserID" 
					Return Me.CollNatureNotifiUserID
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "UserID" 
					Return Me.UserID
				Case "EventType" 
					Return Me.EventType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollNatureNotifiUserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureNotificationUserMetadata.ColumnNames.CollNatureNotifiUserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureNotificationUserMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureNotificationUserMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property EventType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureNotificationUserMetadata.ColumnNames.EventType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionNatureNotificationUser 
		Inherits esICCollectionNatureNotificationUser
		
	
		#Region "UpToICCollectionNatureByCollectionNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionNatureNotificationUser_IC_CollectionNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionNatureByCollectionNatureCode As ICCollectionNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionNatureByCollectionNatureCode Is Nothing _
						 AndAlso Not CollectionNatureCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionNatureByCollectionNatureCode = New ICCollectionNature()
					Me._UpToICCollectionNatureByCollectionNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Where(Me._UpToICCollectionNatureByCollectionNatureCode.Query.CollectionNatureCode = Me.CollectionNatureCode)
					Me._UpToICCollectionNatureByCollectionNatureCode.Query.Load()
				End If

				Return Me._UpToICCollectionNatureByCollectionNatureCode
			End Get
			
            Set(ByVal value As ICCollectionNature)
				Me.RemovePreSave("UpToICCollectionNatureByCollectionNatureCode")
				

				If value Is Nothing Then
				
					Me.CollectionNatureCode = Nothing
				
					Me._UpToICCollectionNatureByCollectionNatureCode = Nothing
				Else
				
					Me.CollectionNatureCode = value.CollectionNatureCode
					
					Me._UpToICCollectionNatureByCollectionNatureCode = value
					Me.SetPreSave("UpToICCollectionNatureByCollectionNatureCode", Me._UpToICCollectionNatureByCollectionNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionNatureNotificationUser_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionNatureNotificationUserMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollNatureNotifiUserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionNatureNotificationUserMetadata.PropertyNames.CollNatureNotifiUserID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureNotificationUserMetadata.ColumnNames.CollectionNatureCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionNatureNotificationUserMetadata.PropertyNames.CollectionNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureNotificationUserMetadata.ColumnNames.UserID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionNatureNotificationUserMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureNotificationUserMetadata.ColumnNames.EventType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionNatureNotificationUserMetadata.PropertyNames.EventType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionNatureNotificationUserMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollNatureNotifiUserID As String = "CollNatureNotifiUserID"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const UserID As String = "UserID"
			 Public Const EventType As String = "EventType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollNatureNotifiUserID As String = "CollNatureNotifiUserID"
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const UserID As String = "UserID"
			 Public Const EventType As String = "EventType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionNatureNotificationUserMetadata)
			
				If ICCollectionNatureNotificationUserMetadata.mapDelegates Is Nothing Then
					ICCollectionNatureNotificationUserMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionNatureNotificationUserMetadata._meta Is Nothing Then
					ICCollectionNatureNotificationUserMetadata._meta = New ICCollectionNatureNotificationUserMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollNatureNotifiUserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("EventType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CollectionNatureNotificationUser"
				meta.Destination = "IC_CollectionNatureNotificationUser"
				
				meta.spInsert = "proc_IC_CollectionNatureNotificationUserInsert"
				meta.spUpdate = "proc_IC_CollectionNatureNotificationUserUpdate"
				meta.spDelete = "proc_IC_CollectionNatureNotificationUserDelete"
				meta.spLoadAll = "proc_IC_CollectionNatureNotificationUserLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionNatureNotificationUserLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionNatureNotificationUserMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
