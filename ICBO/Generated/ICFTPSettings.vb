
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_FTPSettings' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFTPSettings))> _
	<XmlType("ICFTPSettings")> _	
	Partial Public Class ICFTPSettings 
		Inherits esICFTPSettings
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFTPSettings()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fTPSettingsID As System.Int32)
			Dim obj As New ICFTPSettings()
			obj.FTPSettingsID = fTPSettingsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fTPSettingsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFTPSettings()
			obj.FTPSettingsID = fTPSettingsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFTPSettingsCollection")> _
	Partial Public Class ICFTPSettingsCollection
		Inherits esICFTPSettingsCollection
		Implements IEnumerable(Of ICFTPSettings)
	
		Public Function FindByPrimaryKey(ByVal fTPSettingsID As System.Int32) As ICFTPSettings
			Return MyBase.SingleOrDefault(Function(e) e.FTPSettingsID.HasValue AndAlso e.FTPSettingsID.Value = fTPSettingsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFTPSettings))> _
		Public Class ICFTPSettingsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFTPSettingsCollection)
			
			Public Shared Widening Operator CType(packet As ICFTPSettingsCollectionWCFPacket) As ICFTPSettingsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFTPSettingsCollection) As ICFTPSettingsCollectionWCFPacket
				Return New ICFTPSettingsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFTPSettingsQuery 
		Inherits esICFTPSettingsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFTPSettingsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFTPSettingsQuery) As String
			Return ICFTPSettingsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFTPSettingsQuery
			Return DirectCast(ICFTPSettingsQuery.SerializeHelper.FromXml(query, GetType(ICFTPSettingsQuery)), ICFTPSettingsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFTPSettings
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fTPSettingsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fTPSettingsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fTPSettingsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fTPSettingsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fTPSettingsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fTPSettingsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fTPSettingsID As System.Int32) As Boolean
		
			Dim query As New ICFTPSettingsQuery()
			query.Where(query.FTPSettingsID = fTPSettingsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fTPSettingsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FTPSettingsID", fTPSettingsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_FTPSettings.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureByAccountNumber")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FTPUserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTPUserID As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPUserID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPUserID, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPUserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FTPPassword
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTPPassword As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPPassword)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPPassword, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPPassword)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FTPIPAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTPIPAddress As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPIPAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPIPAddress, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPIPAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FTPFolderName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTPFolderName As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPFolderName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.FTPFolderName, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPFolderName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FTPSettingsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTPSettingsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFTPSettingsMetadata.ColumnNames.FTPSettingsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFTPSettingsMetadata.ColumnNames.FTPSettingsID, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPSettingsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFTPSettingsMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFTPSettingsMetadata.ColumnNames.GroupCode, value) Then
					Me._UpToICGroupByGroupCode = Nothing
					Me.OnPropertyChanged("UpToICGroupByGroupCode")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFTPSettingsMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFTPSettingsMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFTPSettingsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFTPSettingsMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFTPSettingsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFTPSettingsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.FileUploadTemplateCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileUploadTemplateCode As System.String
			Get
				Return MyBase.GetSystemString(ICFTPSettingsMetadata.ColumnNames.FileUploadTemplateCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFTPSettingsMetadata.ColumnNames.FileUploadTemplateCode, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FileUploadTemplateCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFTPSettingsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFTPSettingsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FTPSettings.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFTPSettingsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFTPSettingsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICGroupByGroupCode As ICGroup
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "FTPUserID"
							Me.str().FTPUserID = CType(value, string)
												
						Case "FTPPassword"
							Me.str().FTPPassword = CType(value, string)
												
						Case "FTPIPAddress"
							Me.str().FTPIPAddress = CType(value, string)
												
						Case "FTPFolderName"
							Me.str().FTPFolderName = CType(value, string)
												
						Case "FTPSettingsID"
							Me.str().FTPSettingsID = CType(value, string)
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "FileUploadTemplateCode"
							Me.str().FileUploadTemplateCode = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FTPSettingsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FTPSettingsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.FTPSettingsID)
							End If
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.GroupCode)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.UserID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFTPSettingsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFTPSettings)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FTPUserID As System.String 
				Get
					Dim data_ As System.String = entity.FTPUserID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTPUserID = Nothing
					Else
						entity.FTPUserID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FTPPassword As System.String 
				Get
					Dim data_ As System.String = entity.FTPPassword
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTPPassword = Nothing
					Else
						entity.FTPPassword = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FTPIPAddress As System.String 
				Get
					Dim data_ As System.String = entity.FTPIPAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTPIPAddress = Nothing
					Else
						entity.FTPIPAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FTPFolderName As System.String 
				Get
					Dim data_ As System.String = entity.FTPFolderName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTPFolderName = Nothing
					Else
						entity.FTPFolderName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FTPSettingsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FTPSettingsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTPSettingsID = Nothing
					Else
						entity.FTPSettingsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileUploadTemplateCode As System.String 
				Get
					Dim data_ As System.String = entity.FileUploadTemplateCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileUploadTemplateCode = Nothing
					Else
						entity.FileUploadTemplateCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFTPSettings
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFTPSettingsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFTPSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFTPSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFTPSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFTPSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFTPSettingsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFTPSettingsCollection
		Inherits esEntityCollection(Of ICFTPSettings)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFTPSettingsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFTPSettingsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFTPSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFTPSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFTPSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFTPSettingsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFTPSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFTPSettingsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFTPSettingsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFTPSettingsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFTPSettingsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "FTPUserID" 
					Return Me.FTPUserID
				Case "FTPPassword" 
					Return Me.FTPPassword
				Case "FTPIPAddress" 
					Return Me.FTPIPAddress
				Case "FTPFolderName" 
					Return Me.FTPFolderName
				Case "FTPSettingsID" 
					Return Me.FTPSettingsID
				Case "GroupCode" 
					Return Me.GroupCode
				Case "UserID" 
					Return Me.UserID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "FileUploadTemplateCode" 
					Return Me.FileUploadTemplateCode
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FTPUserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FTPUserID, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FTPPassword As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FTPPassword, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FTPIPAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FTPIPAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FTPFolderName As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FTPFolderName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FTPSettingsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FTPSettingsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property FileUploadTemplateCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.FileUploadTemplateCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFTPSettingsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFTPSettings 
		Inherits esICFTPSettings
		
	
		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FTP_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICAccountsPaymentNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FTPSettings_IC_AccountsPaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureByAccountNumber As ICAccountsPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureByAccountNumber = New ICAccountsPaymentNature()
					Me._UpToICAccountsPaymentNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNature)
				Me.RemovePreSave("UpToICAccountsPaymentNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICAccountsPaymentNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICAccountsPaymentNatureByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureByAccountNumber", Me._UpToICAccountsPaymentNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICGroupByGroupCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FTPSettings_IC_Group
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICGroupByGroupCode As ICGroup
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICGroupByGroupCode Is Nothing _
						 AndAlso Not GroupCode.Equals(Nothing)  Then
						
					Me._UpToICGroupByGroupCode = New ICGroup()
					Me._UpToICGroupByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
					Me._UpToICGroupByGroupCode.Query.Where(Me._UpToICGroupByGroupCode.Query.GroupCode = Me.GroupCode)
					Me._UpToICGroupByGroupCode.Query.Load()
				End If

				Return Me._UpToICGroupByGroupCode
			End Get
			
            Set(ByVal value As ICGroup)
				Me.RemovePreSave("UpToICGroupByGroupCode")
				

				If value Is Nothing Then
				
					Me.GroupCode = Nothing
				
					Me._UpToICGroupByGroupCode = Nothing
				Else
				
					Me.GroupCode = value.GroupCode
					
					Me._UpToICGroupByGroupCode = value
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FTPSettings_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_FTPSettings_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICGroupByGroupCode Is Nothing Then
				Me.GroupCode = Me._UpToICGroupByGroupCode.GroupCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICFTPSettingsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FTPUserID, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FTPUserID
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FTPPassword, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FTPPassword
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FTPIPAddress, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FTPIPAddress
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FTPFolderName, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FTPFolderName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FTPSettingsID, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FTPSettingsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.GroupCode, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.GroupCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.UserID, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.CreatedBy, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.CreatedDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.FileUploadTemplateCode, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.FileUploadTemplateCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.Creater, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFTPSettingsMetadata.ColumnNames.CreationDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFTPSettingsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFTPSettingsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FTPUserID As String = "FTPUserID"
			 Public Const FTPPassword As String = "FTPPassword"
			 Public Const FTPIPAddress As String = "FTPIPAddress"
			 Public Const FTPFolderName As String = "FTPFolderName"
			 Public Const FTPSettingsID As String = "FTPSettingsID"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FTPUserID As String = "FTPUserID"
			 Public Const FTPPassword As String = "FTPPassword"
			 Public Const FTPIPAddress As String = "FTPIPAddress"
			 Public Const FTPFolderName As String = "FTPFolderName"
			 Public Const FTPSettingsID As String = "FTPSettingsID"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const FileUploadTemplateCode As String = "FileUploadTemplateCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFTPSettingsMetadata)
			
				If ICFTPSettingsMetadata.mapDelegates Is Nothing Then
					ICFTPSettingsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFTPSettingsMetadata._meta Is Nothing Then
					ICFTPSettingsMetadata._meta = New ICFTPSettingsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FTPUserID", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FTPPassword", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FTPIPAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FTPFolderName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FTPSettingsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("FileUploadTemplateCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_FTPSettings"
				meta.Destination = "IC_FTPSettings"
				
				meta.spInsert = "proc_IC_FTPSettingsInsert"
				meta.spUpdate = "proc_IC_FTPSettingsUpdate"
				meta.spDelete = "proc_IC_FTPSettingsDelete"
				meta.spLoadAll = "proc_IC_FTPSettingsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FTPSettingsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFTPSettingsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
