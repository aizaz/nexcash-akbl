
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/14/2014 10:21:28 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionMISReport' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionMISReport))> _
	<XmlType("ICCollectionMISReport")> _	
	Partial Public Class ICCollectionMISReport 
		Inherits esICCollectionMISReport
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionMISReport()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal reportID As System.Int32)
			Dim obj As New ICCollectionMISReport()
			obj.ReportID = reportID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal reportID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionMISReport()
			obj.ReportID = reportID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionMISReportCollection")> _
	Partial Public Class ICCollectionMISReportCollection
		Inherits esICCollectionMISReportCollection
		Implements IEnumerable(Of ICCollectionMISReport)
	
		Public Function FindByPrimaryKey(ByVal reportID As System.Int32) As ICCollectionMISReport
			Return MyBase.SingleOrDefault(Function(e) e.ReportID.HasValue AndAlso e.ReportID.Value = reportID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionMISReport))> _
		Public Class ICCollectionMISReportCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionMISReportCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionMISReportCollectionWCFPacket) As ICCollectionMISReportCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionMISReportCollection) As ICCollectionMISReportCollectionWCFPacket
				Return New ICCollectionMISReportCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionMISReportQuery 
		Inherits esICCollectionMISReportQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionMISReportQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionMISReportQuery) As String
			Return ICCollectionMISReportQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionMISReportQuery
			Return DirectCast(ICCollectionMISReportQuery.SerializeHelper.FromXml(query, GetType(ICCollectionMISReportQuery)), ICCollectionMISReportQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionMISReport
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal reportID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(reportID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(reportID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal reportID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(reportID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(reportID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal reportID As System.Int32) As Boolean
		
			Dim query As New ICCollectionMISReportQuery()
			query.Where(query.ReportID = reportID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal reportID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ReportID", reportID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.ReportID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.ReportID, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportName, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportFileData
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportFileData As System.Byte()
			Get
				Return MyBase.GetSystemByteArray(ICCollectionMISReportMetadata.ColumnNames.ReportFileData)
			End Get
			
			Set(ByVal value As System.Byte())
				If MyBase.SetSystemByteArray(ICCollectionMISReportMetadata.ColumnNames.ReportFileData, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportFileData)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportFileSize
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportFileSize As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.ReportFileSize)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.ReportFileSize, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportFileSize)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportFileType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportFileType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportFileType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportFileType, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportFileType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.ReportType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionMISReportMetadata.ColumnNames.ReportType, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionMISReportMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionMISReportMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionMISReportMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionMISReport.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionMISReportMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionMISReportMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ReportID"
							Me.str().ReportID = CType(value, string)
												
						Case "ReportName"
							Me.str().ReportName = CType(value, string)
												
						Case "ReportFileSize"
							Me.str().ReportFileSize = CType(value, string)
												
						Case "ReportFileType"
							Me.str().ReportFileType = CType(value, string)
												
						Case "ReportType"
							Me.str().ReportType = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ReportID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReportID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportID)
							End If
						
						Case "ReportFileData"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Byte()" Then
								Me.ReportFileData = CType(value, System.Byte())
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportFileData)
							End If
						
						Case "ReportFileSize"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReportFileSize = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.ReportFileSize)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionMISReportMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionMISReport)
				Me.entity = entity
			End Sub				
		
	
			Public Property ReportID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReportID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportID = Nothing
					Else
						entity.ReportID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReportName As System.String 
				Get
					Dim data_ As System.String = entity.ReportName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportName = Nothing
					Else
						entity.ReportName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReportFileSize As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReportFileSize
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportFileSize = Nothing
					Else
						entity.ReportFileSize = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReportFileType As System.String 
				Get
					Dim data_ As System.String = entity.ReportFileType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportFileType = Nothing
					Else
						entity.ReportFileType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReportType As System.String 
				Get
					Dim data_ As System.String = entity.ReportType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportType = Nothing
					Else
						entity.ReportType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionMISReport
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionMISReportMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionMISReportQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionMISReportQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionMISReportQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionMISReportQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionMISReportQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionMISReportCollection
		Inherits esEntityCollection(Of ICCollectionMISReport)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionMISReportMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionMISReportCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionMISReportQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionMISReportQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionMISReportQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionMISReportQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionMISReportQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionMISReportQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionMISReportQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionMISReportQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionMISReportMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ReportID" 
					Return Me.ReportID
				Case "ReportName" 
					Return Me.ReportName
				Case "ReportFileData" 
					Return Me.ReportFileData
				Case "ReportFileSize" 
					Return Me.ReportFileSize
				Case "ReportFileType" 
					Return Me.ReportFileType
				Case "ReportType" 
					Return Me.ReportType
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ReportID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReportName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReportFileData As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportFileData, esSystemType.ByteArray)
			End Get
		End Property 
		
		Public ReadOnly Property ReportFileSize As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportFileSize, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReportFileType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportFileType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReportType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.ReportType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionMISReportMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionMISReport 
		Inherits esICCollectionMISReport
		
	
		#Region "ICCollectionMISReportUsersCollectionByReportID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionMISReportUsersCollectionByReportID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionMISReport.ICCollectionMISReportUsersCollectionByReportID_Delegate)
				map.PropertyName = "ICCollectionMISReportUsersCollectionByReportID"
				map.MyColumnName = "ReportID"
				map.ParentColumnName = "ReportID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionMISReportUsersCollectionByReportID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionMISReportQuery(data.NextAlias())
			
			Dim mee As ICCollectionMISReportUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionMISReportUsersQuery), New ICCollectionMISReportUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ReportID = mee.ReportID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionMISReportUsers_IC_CollectionMISReport
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionMISReportUsersCollectionByReportID As ICCollectionMISReportUsersCollection 
		
			Get
				If Me._ICCollectionMISReportUsersCollectionByReportID Is Nothing Then
					Me._ICCollectionMISReportUsersCollectionByReportID = New ICCollectionMISReportUsersCollection()
					Me._ICCollectionMISReportUsersCollectionByReportID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionMISReportUsersCollectionByReportID", Me._ICCollectionMISReportUsersCollectionByReportID)
				
					If Not Me.ReportID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionMISReportUsersCollectionByReportID.Query.Where(Me._ICCollectionMISReportUsersCollectionByReportID.Query.ReportID = Me.ReportID)
							Me._ICCollectionMISReportUsersCollectionByReportID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionMISReportUsersCollectionByReportID.fks.Add(ICCollectionMISReportUsersMetadata.ColumnNames.ReportID, Me.ReportID)
					End If
				End If

				Return Me._ICCollectionMISReportUsersCollectionByReportID
			End Get
			
			Set(ByVal value As ICCollectionMISReportUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionMISReportUsersCollectionByReportID Is Nothing Then

					Me.RemovePostSave("ICCollectionMISReportUsersCollectionByReportID")
					Me._ICCollectionMISReportUsersCollectionByReportID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionMISReportUsersCollectionByReportID As ICCollectionMISReportUsersCollection
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionMISReport_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionMISReportUsersCollectionByReportID"
					coll = Me.ICCollectionMISReportUsersCollectionByReportID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionMISReportUsersCollectionByReportID", GetType(ICCollectionMISReportUsersCollection), New ICCollectionMISReportUsers()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICCollectionMISReportUsersCollectionByReportID Is Nothing Then
				Apply(Me._ICCollectionMISReportUsersCollectionByReportID, "ReportID", Me.ReportID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionMISReportMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportName
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportFileData, 2, GetType(System.Byte()), esSystemType.ByteArray)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportFileData
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportFileSize, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportFileSize
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportFileType, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportFileType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.ReportType, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.ReportType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.CreatedDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.Creater, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionMISReportMetadata.ColumnNames.CreationDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionMISReportMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionMISReportMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ReportID As String = "ReportID"
			 Public Const ReportName As String = "ReportName"
			 Public Const ReportFileData As String = "ReportFileData"
			 Public Const ReportFileSize As String = "ReportFileSize"
			 Public Const ReportFileType As String = "ReportFileType"
			 Public Const ReportType As String = "ReportType"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ReportID As String = "ReportID"
			 Public Const ReportName As String = "ReportName"
			 Public Const ReportFileData As String = "ReportFileData"
			 Public Const ReportFileSize As String = "ReportFileSize"
			 Public Const ReportFileType As String = "ReportFileType"
			 Public Const ReportType As String = "ReportType"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionMISReportMetadata)
			
				If ICCollectionMISReportMetadata.mapDelegates Is Nothing Then
					ICCollectionMISReportMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionMISReportMetadata._meta Is Nothing Then
					ICCollectionMISReportMetadata._meta = New ICCollectionMISReportMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ReportID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReportName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReportFileData", new esTypeMap("varbinary", "System.Byte()"))
				meta.AddTypeMap("ReportFileSize", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReportFileType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReportType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_CollectionMISReport"
				meta.Destination = "IC_CollectionMISReport"
				
				meta.spInsert = "proc_IC_CollectionMISReportInsert"
				meta.spUpdate = "proc_IC_CollectionMISReportUpdate"
				meta.spDelete = "proc_IC_CollectionMISReportDelete"
				meta.spLoadAll = "proc_IC_CollectionMISReportLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionMISReportLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionMISReportMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
