
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:58 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_InstructionStatus' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstructionStatus))> _
	<XmlType("ICInstructionStatus")> _	
	Partial Public Class ICInstructionStatus 
		Inherits esICInstructionStatus
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstructionStatus()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal statusID As System.Int32)
			Dim obj As New ICInstructionStatus()
			obj.StatusID = statusID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal statusID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstructionStatus()
			obj.StatusID = statusID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstructionStatusCollection")> _
	Partial Public Class ICInstructionStatusCollection
		Inherits esICInstructionStatusCollection
		Implements IEnumerable(Of ICInstructionStatus)
	
		Public Function FindByPrimaryKey(ByVal statusID As System.Int32) As ICInstructionStatus
			Return MyBase.SingleOrDefault(Function(e) e.StatusID.HasValue AndAlso e.StatusID.Value = statusID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstructionStatus))> _
		Public Class ICInstructionStatusCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstructionStatusCollection)
			
			Public Shared Widening Operator CType(packet As ICInstructionStatusCollectionWCFPacket) As ICInstructionStatusCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstructionStatusCollection) As ICInstructionStatusCollectionWCFPacket
				Return New ICInstructionStatusCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstructionStatusQuery 
		Inherits esICInstructionStatusQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstructionStatusQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstructionStatusQuery) As String
			Return ICInstructionStatusQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstructionStatusQuery
			Return DirectCast(ICInstructionStatusQuery.SerializeHelper.FromXml(query, GetType(ICInstructionStatusQuery)), ICInstructionStatusQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstructionStatus
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal statusID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(statusID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(statusID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal statusID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(statusID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(statusID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal statusID As System.Int32) As Boolean
		
			Dim query As New ICInstructionStatusQuery()
			query.Where(query.StatusID = statusID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal statusID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("StatusID", statusID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_InstructionStatus.StatusID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionStatusMetadata.ColumnNames.StatusID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionStatusMetadata.ColumnNames.StatusID, value) Then
					OnPropertyChanged(ICInstructionStatusMetadata.PropertyNames.StatusID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionStatus.StatusName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionStatusMetadata.ColumnNames.StatusName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionStatusMetadata.ColumnNames.StatusName, value) Then
					OnPropertyChanged(ICInstructionStatusMetadata.PropertyNames.StatusName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionStatus.GroupHeader
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupHeader As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionStatusMetadata.ColumnNames.GroupHeader)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionStatusMetadata.ColumnNames.GroupHeader, value) Then
					OnPropertyChanged(ICInstructionStatusMetadata.PropertyNames.GroupHeader)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "StatusID"
							Me.str().StatusID = CType(value, string)
												
						Case "StatusName"
							Me.str().StatusName = CType(value, string)
												
						Case "GroupHeader"
							Me.str().GroupHeader = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "StatusID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionStatusMetadata.PropertyNames.StatusID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstructionStatus)
				Me.entity = entity
			End Sub				
		
	
			Public Property StatusID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusID = Nothing
					Else
						entity.StatusID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusName As System.String 
				Get
					Dim data_ As System.String = entity.StatusName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusName = Nothing
					Else
						entity.StatusName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupHeader As System.String 
				Get
					Dim data_ As System.String = entity.GroupHeader
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupHeader = Nothing
					Else
						entity.GroupHeader = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstructionStatus
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionStatusMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstructionStatusQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionStatusQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstructionStatusQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstructionStatusQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstructionStatusQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstructionStatusCollection
		Inherits esEntityCollection(Of ICInstructionStatus)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionStatusMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstructionStatusCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstructionStatusQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionStatusQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstructionStatusQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstructionStatusQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstructionStatusQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstructionStatusQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstructionStatusQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstructionStatusQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstructionStatusMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "StatusID" 
					Return Me.StatusID
				Case "StatusName" 
					Return Me.StatusName
				Case "GroupHeader" 
					Return Me.GroupHeader
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property StatusID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionStatusMetadata.ColumnNames.StatusID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionStatusMetadata.ColumnNames.StatusName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property GroupHeader As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionStatusMetadata.ColumnNames.GroupHeader, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstructionStatus 
		Inherits esICInstructionStatus
		
	
		#Region "ICInstructionCollectionByStatus - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByStatus() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstructionStatus.ICInstructionCollectionByStatus_Delegate)
				map.PropertyName = "ICInstructionCollectionByStatus"
				map.MyColumnName = "Status"
				map.ParentColumnName = "StatusID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByStatus_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionStatusQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.StatusID = mee.Status)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_InstructionStatus1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByStatus As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByStatus Is Nothing Then
					Me._ICInstructionCollectionByStatus = New ICInstructionCollection()
					Me._ICInstructionCollectionByStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByStatus", Me._ICInstructionCollectionByStatus)
				
					If Not Me.StatusID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByStatus.Query.Where(Me._ICInstructionCollectionByStatus.Query.Status = Me.StatusID)
							Me._ICInstructionCollectionByStatus.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByStatus.fks.Add(ICInstructionMetadata.ColumnNames.Status, Me.StatusID)
					End If
				End If

				Return Me._ICInstructionCollectionByStatus
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByStatus Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByStatus")
					Me._ICInstructionCollectionByStatus = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByStatus As ICInstructionCollection
		#End Region

		#Region "ICInstructionCollectionByLastStatus - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByLastStatus() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstructionStatus.ICInstructionCollectionByLastStatus_Delegate)
				map.PropertyName = "ICInstructionCollectionByLastStatus"
				map.MyColumnName = "LastStatus"
				map.ParentColumnName = "StatusID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByLastStatus_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionStatusQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.StatusID = mee.LastStatus)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_InstructionStatus2
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByLastStatus As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByLastStatus Is Nothing Then
					Me._ICInstructionCollectionByLastStatus = New ICInstructionCollection()
					Me._ICInstructionCollectionByLastStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByLastStatus", Me._ICInstructionCollectionByLastStatus)
				
					If Not Me.StatusID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByLastStatus.Query.Where(Me._ICInstructionCollectionByLastStatus.Query.LastStatus = Me.StatusID)
							Me._ICInstructionCollectionByLastStatus.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByLastStatus.fks.Add(ICInstructionMetadata.ColumnNames.LastStatus, Me.StatusID)
					End If
				End If

				Return Me._ICInstructionCollectionByLastStatus
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByLastStatus Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByLastStatus")
					Me._ICInstructionCollectionByLastStatus = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByLastStatus As ICInstructionCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICInstructionCollectionByStatus"
					coll = Me.ICInstructionCollectionByStatus
					Exit Select
				Case "ICInstructionCollectionByLastStatus"
					coll = Me.ICInstructionCollectionByLastStatus
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByStatus", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByLastStatus", GetType(ICInstructionCollection), New ICInstruction()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICInstructionCollectionByStatus Is Nothing Then
				Apply(Me._ICInstructionCollectionByStatus, "Status", Me.StatusID)
			End If
			
			If Not Me._ICInstructionCollectionByLastStatus Is Nothing Then
				Apply(Me._ICInstructionCollectionByLastStatus, "LastStatus", Me.StatusID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstructionStatusMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstructionStatusMetadata.ColumnNames.StatusID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionStatusMetadata.PropertyNames.StatusID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionStatusMetadata.ColumnNames.StatusName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionStatusMetadata.PropertyNames.StatusName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionStatusMetadata.ColumnNames.GroupHeader, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionStatusMetadata.PropertyNames.GroupHeader
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstructionStatusMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const StatusID As String = "StatusID"
			 Public Const StatusName As String = "StatusName"
			 Public Const GroupHeader As String = "GroupHeader"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const StatusID As String = "StatusID"
			 Public Const StatusName As String = "StatusName"
			 Public Const GroupHeader As String = "GroupHeader"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstructionStatusMetadata)
			
				If ICInstructionStatusMetadata.mapDelegates Is Nothing Then
					ICInstructionStatusMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstructionStatusMetadata._meta Is Nothing Then
					ICInstructionStatusMetadata._meta = New ICInstructionStatusMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("StatusID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("GroupHeader", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_InstructionStatus"
				meta.Destination = "IC_InstructionStatus"
				
				meta.spInsert = "proc_IC_InstructionStatusInsert"
				meta.spUpdate = "proc_IC_InstructionStatusUpdate"
				meta.spDelete = "proc_IC_InstructionStatusDelete"
				meta.spLoadAll = "proc_IC_InstructionStatusLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstructionStatusLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstructionStatusMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
