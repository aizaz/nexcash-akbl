
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 3/24/2014 3:23:49 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Complains' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICComplains))> _
	<XmlType("ICComplains")> _	
	Partial Public Class ICComplains 
		Inherits esICComplains
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICComplains()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal complainID As System.Int32)
			Dim obj As New ICComplains()
			obj.ComplainID = complainID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal complainID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICComplains()
			obj.ComplainID = complainID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICComplainsCollection")> _
	Partial Public Class ICComplainsCollection
		Inherits esICComplainsCollection
		Implements IEnumerable(Of ICComplains)
	
		Public Function FindByPrimaryKey(ByVal complainID As System.Int32) As ICComplains
			Return MyBase.SingleOrDefault(Function(e) e.ComplainID.HasValue AndAlso e.ComplainID.Value = complainID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICComplains))> _
		Public Class ICComplainsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICComplainsCollection)
			
			Public Shared Widening Operator CType(packet As ICComplainsCollectionWCFPacket) As ICComplainsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICComplainsCollection) As ICComplainsCollectionWCFPacket
				Return New ICComplainsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICComplainsQuery 
		Inherits esICComplainsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICComplainsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICComplainsQuery) As String
			Return ICComplainsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICComplainsQuery
			Return DirectCast(ICComplainsQuery.SerializeHelper.FromXml(query, GetType(ICComplainsQuery)), ICComplainsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICComplains
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal complainID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(complainID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(complainID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal complainID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(complainID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(complainID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal complainID As System.Int32) As Boolean
		
			Dim query As New ICComplainsQuery()
			query.Where(query.ComplainID = complainID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal complainID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ComplainID", complainID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Complains.ComplainID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ComplainID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICComplainsMetadata.ColumnNames.ComplainID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICComplainsMetadata.ColumnNames.ComplainID, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.ComplainID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Subject
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Subject As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.Subject)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.Subject, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Subject)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.ComplainMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ComplainMessage As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.ComplainMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.ComplainMessage, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.ComplainMessage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Sender
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Sender As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICComplainsMetadata.ColumnNames.Sender)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICComplainsMetadata.ColumnNames.Sender, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Sender)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Receiver
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Receiver As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICComplainsMetadata.ColumnNames.Receiver)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICComplainsMetadata.ColumnNames.Receiver, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Receiver)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICComplainsMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICComplainsMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.ParentID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ParentID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICComplainsMetadata.ColumnNames.ParentID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICComplainsMetadata.ColumnNames.ParentID, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.ParentID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Name
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Name As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.Name)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.Name, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Name)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.PhoneNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhoneNo As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.PhoneNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.PhoneNo, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.PhoneNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Complains.Email
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Email As System.String
			Get
				Return MyBase.GetSystemString(ICComplainsMetadata.ColumnNames.Email)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICComplainsMetadata.ColumnNames.Email, value) Then
					OnPropertyChanged(ICComplainsMetadata.PropertyNames.Email)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ComplainID"
							Me.str().ComplainID = CType(value, string)
												
						Case "Subject"
							Me.str().Subject = CType(value, string)
												
						Case "ComplainMessage"
							Me.str().ComplainMessage = CType(value, string)
												
						Case "Sender"
							Me.str().Sender = CType(value, string)
												
						Case "Receiver"
							Me.str().Receiver = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ParentID"
							Me.str().ParentID = CType(value, string)
												
						Case "Name"
							Me.str().Name = CType(value, string)
												
						Case "PhoneNo"
							Me.str().PhoneNo = CType(value, string)
												
						Case "Email"
							Me.str().Email = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ComplainID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ComplainID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICComplainsMetadata.PropertyNames.ComplainID)
							End If
						
						Case "Sender"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Sender = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICComplainsMetadata.PropertyNames.Sender)
							End If
						
						Case "Receiver"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Receiver = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICComplainsMetadata.PropertyNames.Receiver)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICComplainsMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ParentID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ParentID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICComplainsMetadata.PropertyNames.ParentID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICComplains)
				Me.entity = entity
			End Sub				
		
	
			Public Property ComplainID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ComplainID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ComplainID = Nothing
					Else
						entity.ComplainID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Subject As System.String 
				Get
					Dim data_ As System.String = entity.Subject
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Subject = Nothing
					Else
						entity.Subject = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ComplainMessage As System.String 
				Get
					Dim data_ As System.String = entity.ComplainMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ComplainMessage = Nothing
					Else
						entity.ComplainMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Sender As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Sender
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Sender = Nothing
					Else
						entity.Sender = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Receiver As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Receiver
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Receiver = Nothing
					Else
						entity.Receiver = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As System.String = entity.Status
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ParentID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ParentID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ParentID = Nothing
					Else
						entity.ParentID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Name As System.String 
				Get
					Dim data_ As System.String = entity.Name
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Name = Nothing
					Else
						entity.Name = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhoneNo As System.String 
				Get
					Dim data_ As System.String = entity.PhoneNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhoneNo = Nothing
					Else
						entity.PhoneNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Email As System.String 
				Get
					Dim data_ As System.String = entity.Email
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Email = Nothing
					Else
						entity.Email = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICComplains
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICComplainsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICComplainsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICComplainsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICComplainsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICComplainsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICComplainsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICComplainsCollection
		Inherits esEntityCollection(Of ICComplains)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICComplainsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICComplainsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICComplainsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICComplainsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICComplainsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICComplainsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICComplainsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICComplainsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICComplainsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICComplainsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICComplainsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ComplainID" 
					Return Me.ComplainID
				Case "Subject" 
					Return Me.Subject
				Case "ComplainMessage" 
					Return Me.ComplainMessage
				Case "Sender" 
					Return Me.Sender
				Case "Receiver" 
					Return Me.Receiver
				Case "Status" 
					Return Me.Status
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ParentID" 
					Return Me.ParentID
				Case "Name" 
					Return Me.Name
				Case "PhoneNo" 
					Return Me.PhoneNo
				Case "Email" 
					Return Me.Email
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ComplainID As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.ComplainID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Subject As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Subject, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ComplainMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.ComplainMessage, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Sender As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Sender, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Receiver As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Receiver, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Status, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ParentID As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.ParentID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Name As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Name, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PhoneNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.PhoneNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Email As esQueryItem
			Get
				Return New esQueryItem(Me, ICComplainsMetadata.ColumnNames.Email, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICComplains 
		Inherits esICComplains
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICComplainsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.ComplainID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.ComplainID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Subject, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Subject
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.ComplainMessage, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.ComplainMessage
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Sender, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Sender
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Receiver, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Receiver
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Status, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Status
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.ParentID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.ParentID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Name, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Name
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.PhoneNo, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.PhoneNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICComplainsMetadata.ColumnNames.Email, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICComplainsMetadata.PropertyNames.Email
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICComplainsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ComplainID As String = "ComplainID"
			 Public Const Subject As String = "Subject"
			 Public Const ComplainMessage As String = "ComplainMessage"
			 Public Const Sender As String = "Sender"
			 Public Const Receiver As String = "Receiver"
			 Public Const Status As String = "Status"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ParentID As String = "ParentID"
			 Public Const Name As String = "Name"
			 Public Const PhoneNo As String = "PhoneNo"
			 Public Const Email As String = "Email"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ComplainID As String = "ComplainID"
			 Public Const Subject As String = "Subject"
			 Public Const ComplainMessage As String = "ComplainMessage"
			 Public Const Sender As String = "Sender"
			 Public Const Receiver As String = "Receiver"
			 Public Const Status As String = "Status"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ParentID As String = "ParentID"
			 Public Const Name As String = "Name"
			 Public Const PhoneNo As String = "PhoneNo"
			 Public Const Email As String = "Email"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICComplainsMetadata)
			
				If ICComplainsMetadata.mapDelegates Is Nothing Then
					ICComplainsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICComplainsMetadata._meta Is Nothing Then
					ICComplainsMetadata._meta = New ICComplainsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ComplainID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Subject", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ComplainMessage", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Sender", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Receiver", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ParentID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Name", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PhoneNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_Complains"
				meta.Destination = "IC_Complains"
				
				meta.spInsert = "proc_IC_ComplainsInsert"
				meta.spUpdate = "proc_IC_ComplainsUpdate"
				meta.spDelete = "proc_IC_ComplainsDelete"
				meta.spLoadAll = "proc_IC_ComplainsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ComplainsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICComplainsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
