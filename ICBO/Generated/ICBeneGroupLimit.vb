
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/30/2015 3:59:47 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_BeneGroupLimit' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBeneGroupLimit))> _
	<XmlType("ICBeneGroupLimit")> _	
	Partial Public Class ICBeneGroupLimit 
		Inherits esICBeneGroupLimit
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBeneGroupLimit()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal beneGroupLimitID As System.Int32)
			Dim obj As New ICBeneGroupLimit()
			obj.BeneGroupLimitID = beneGroupLimitID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal beneGroupLimitID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBeneGroupLimit()
			obj.BeneGroupLimitID = beneGroupLimitID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBeneGroupLimitCollection")> _
	Partial Public Class ICBeneGroupLimitCollection
		Inherits esICBeneGroupLimitCollection
		Implements IEnumerable(Of ICBeneGroupLimit)
	
		Public Function FindByPrimaryKey(ByVal beneGroupLimitID As System.Int32) As ICBeneGroupLimit
			Return MyBase.SingleOrDefault(Function(e) e.BeneGroupLimitID.HasValue AndAlso e.BeneGroupLimitID.Value = beneGroupLimitID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBeneGroupLimit))> _
		Public Class ICBeneGroupLimitCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBeneGroupLimitCollection)
			
			Public Shared Widening Operator CType(packet As ICBeneGroupLimitCollectionWCFPacket) As ICBeneGroupLimitCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBeneGroupLimitCollection) As ICBeneGroupLimitCollectionWCFPacket
				Return New ICBeneGroupLimitCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBeneGroupLimitQuery 
		Inherits esICBeneGroupLimitQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBeneGroupLimitQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBeneGroupLimitQuery) As String
			Return ICBeneGroupLimitQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBeneGroupLimitQuery
			Return DirectCast(ICBeneGroupLimitQuery.SerializeHelper.FromXml(query, GetType(ICBeneGroupLimitQuery)), ICBeneGroupLimitQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBeneGroupLimit
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal beneGroupLimitID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneGroupLimitID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneGroupLimitID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal beneGroupLimitID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneGroupLimitID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneGroupLimitID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal beneGroupLimitID As System.Int32) As Boolean
		
			Dim query As New ICBeneGroupLimitQuery()
			query.Where(query.BeneGroupLimitID = beneGroupLimitID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal beneGroupLimitID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BeneGroupLimitID", beneGroupLimitID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_BeneGroupLimit.BeneGroupLimitID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneGroupLimitID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupLimitID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupLimitID, value) Then
					OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.BeneGroupLimitID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroupLimit.BeneGroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneGroupCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupCode, value) Then
					OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.BeneGroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroupLimit.ReleaseDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReleaseDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneGroupLimitMetadata.ColumnNames.ReleaseDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneGroupLimitMetadata.ColumnNames.ReleaseDate, value) Then
					OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.ReleaseDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroupLimit.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICBeneGroupLimitMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICBeneGroupLimitMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BeneGroupLimitID"
							Me.str().BeneGroupLimitID = CType(value, string)
												
						Case "BeneGroupCode"
							Me.str().BeneGroupCode = CType(value, string)
												
						Case "ReleaseDate"
							Me.str().ReleaseDate = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BeneGroupLimitID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BeneGroupLimitID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.BeneGroupLimitID)
							End If
						
						Case "ReleaseDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReleaseDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.ReleaseDate)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICBeneGroupLimitMetadata.PropertyNames.Amount)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBeneGroupLimit)
				Me.entity = entity
			End Sub				
		
	
			Public Property BeneGroupLimitID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BeneGroupLimitID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneGroupLimitID = Nothing
					Else
						entity.BeneGroupLimitID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneGroupCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneGroupCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneGroupCode = Nothing
					Else
						entity.BeneGroupCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReleaseDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReleaseDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReleaseDate = Nothing
					Else
						entity.ReleaseDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBeneGroupLimit
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneGroupLimitMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBeneGroupLimitQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneGroupLimitQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBeneGroupLimitQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBeneGroupLimitQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBeneGroupLimitQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBeneGroupLimitCollection
		Inherits esEntityCollection(Of ICBeneGroupLimit)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneGroupLimitMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBeneGroupLimitCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBeneGroupLimitQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneGroupLimitQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBeneGroupLimitQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBeneGroupLimitQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBeneGroupLimitQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBeneGroupLimitQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBeneGroupLimitQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBeneGroupLimitQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBeneGroupLimitMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BeneGroupLimitID" 
					Return Me.BeneGroupLimitID
				Case "BeneGroupCode" 
					Return Me.BeneGroupCode
				Case "ReleaseDate" 
					Return Me.ReleaseDate
				Case "Amount" 
					Return Me.Amount
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BeneGroupLimitID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupLimitMetadata.ColumnNames.BeneGroupLimitID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneGroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupLimitMetadata.ColumnNames.BeneGroupCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReleaseDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupLimitMetadata.ColumnNames.ReleaseDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupLimitMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBeneGroupLimit 
		Inherits esICBeneGroupLimit
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICBeneGroupLimitMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupLimitID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneGroupLimitMetadata.PropertyNames.BeneGroupLimitID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupLimitMetadata.ColumnNames.BeneGroupCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneGroupLimitMetadata.PropertyNames.BeneGroupCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupLimitMetadata.ColumnNames.ReleaseDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneGroupLimitMetadata.PropertyNames.ReleaseDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupLimitMetadata.ColumnNames.Amount, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICBeneGroupLimitMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBeneGroupLimitMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BeneGroupLimitID As String = "BeneGroupLimitID"
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const ReleaseDate As String = "ReleaseDate"
			 Public Const Amount As String = "Amount"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BeneGroupLimitID As String = "BeneGroupLimitID"
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const ReleaseDate As String = "ReleaseDate"
			 Public Const Amount As String = "Amount"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBeneGroupLimitMetadata)
			
				If ICBeneGroupLimitMetadata.mapDelegates Is Nothing Then
					ICBeneGroupLimitMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBeneGroupLimitMetadata._meta Is Nothing Then
					ICBeneGroupLimitMetadata._meta = New ICBeneGroupLimitMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BeneGroupLimitID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneGroupCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReleaseDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Amount", new esTypeMap("numeric", "System.Decimal"))			
				
				
				 
				meta.Source = "IC_BeneGroupLimit"
				meta.Destination = "IC_BeneGroupLimit"
				
				meta.spInsert = "proc_IC_BeneGroupLimitInsert"
				meta.spUpdate = "proc_IC_BeneGroupLimitUpdate"
				meta.spDelete = "proc_IC_BeneGroupLimitDelete"
				meta.spLoadAll = "proc_IC_BeneGroupLimitLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BeneGroupLimitLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBeneGroupLimitMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
