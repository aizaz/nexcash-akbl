
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:52 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountPaymentNatureProductTypeAndPrintLocations' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountPaymentNatureProductTypeAndPrintLocations))> _
	<XmlType("ICAccountPaymentNatureProductTypeAndPrintLocations")> _	
	Partial Public Class ICAccountPaymentNatureProductTypeAndPrintLocations 
		Inherits esICAccountPaymentNatureProductTypeAndPrintLocations
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountPaymentNatureProductTypeAndPrintLocations()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal apnptid As System.Int32)
			Dim obj As New ICAccountPaymentNatureProductTypeAndPrintLocations()
			obj.Apnptid = apnptid
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal apnptid As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountPaymentNatureProductTypeAndPrintLocations()
			obj.Apnptid = apnptid
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountPaymentNatureProductTypeAndPrintLocationsCollection")> _
	Partial Public Class ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
		Inherits esICAccountPaymentNatureProductTypeAndPrintLocationsCollection
		Implements IEnumerable(Of ICAccountPaymentNatureProductTypeAndPrintLocations)
	
		Public Function FindByPrimaryKey(ByVal apnptid As System.Int32) As ICAccountPaymentNatureProductTypeAndPrintLocations
			Return MyBase.SingleOrDefault(Function(e) e.Apnptid.HasValue AndAlso e.Apnptid.Value = apnptid)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountPaymentNatureProductTypeAndPrintLocations))> _
		Public Class ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountPaymentNatureProductTypeAndPrintLocationsCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionWCFPacket) As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection) As ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionWCFPacket
				Return New ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountPaymentNatureProductTypeAndPrintLocationsQuery 
		Inherits esICAccountPaymentNatureProductTypeAndPrintLocationsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountPaymentNatureProductTypeAndPrintLocationsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery) As String
			Return ICAccountPaymentNatureProductTypeAndPrintLocationsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery
			Return DirectCast(ICAccountPaymentNatureProductTypeAndPrintLocationsQuery.SerializeHelper.FromXml(query, GetType(ICAccountPaymentNatureProductTypeAndPrintLocationsQuery)), ICAccountPaymentNatureProductTypeAndPrintLocationsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeAndPrintLocations
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal apnptid As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(apnptid)
			Else
				Return LoadByPrimaryKeyStoredProcedure(apnptid)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal apnptid As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(apnptid)
			Else
				Return LoadByPrimaryKeyStoredProcedure(apnptid)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal apnptid As System.Int32) As Boolean
		
			Dim query As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery()
			query.Where(query.Apnptid = apnptid)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal apnptid As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("Apnptid", apnptid)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.APNPTID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Apnptid As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Apnptid)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Apnptid, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Apnptid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.OfficeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.OfficeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.OfficeID, value) Then
					Me._UpToICOfficeByOfficeID = Nothing
					Me.OnPropertyChanged("UpToICOfficeByOfficeID")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.OfficeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.CreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedOn, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeAndPrintLocations.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByOfficeID As ICOffice
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "Apnptid"
							Me.str().Apnptid = CType(value, string)
												
						Case "OfficeID"
							Me.str().OfficeID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedOn"
							Me.str().CreatedOn = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "Apnptid"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Apnptid = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Apnptid)
							End If
						
						Case "OfficeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.OfficeID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedOn)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountPaymentNatureProductTypeAndPrintLocations)
				Me.entity = entity
			End Sub				
		
	
			Public Property Apnptid As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Apnptid
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Apnptid = Nothing
					Else
						entity.Apnptid = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeID = Nothing
					Else
						entity.OfficeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOn = Nothing
					Else
						entity.CreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountPaymentNatureProductTypeAndPrintLocations
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeAndPrintLocationsCollection
		Inherits esEntityCollection(Of ICAccountPaymentNatureProductTypeAndPrintLocations)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountPaymentNatureProductTypeAndPrintLocationsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountPaymentNatureProductTypeAndPrintLocationsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeAndPrintLocationsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "Apnptid" 
					Return Me.Apnptid
				Case "OfficeID" 
					Return Me.OfficeID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedOn" 
					Return Me.CreatedOn
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property Apnptid As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Apnptid, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.OfficeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountPaymentNatureProductTypeAndPrintLocations 
		Inherits esICAccountPaymentNatureProductTypeAndPrintLocations
		
	
		#Region "UpToICAccountsPaymentNatureProductTypeByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountPaymentNatureProductTypeAndPrintLocations_IC_AccountsPaymentNatureProductType2
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = New ICAccountsPaymentNatureProductType()
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNatureProductType)
				Me.RemovePreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICOfficeByOfficeID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountPaymentNatureProductTypeAndPrintLocations_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByOfficeID As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByOfficeID Is Nothing _
						 AndAlso Not OfficeID.Equals(Nothing)  Then
						
					Me._UpToICOfficeByOfficeID = New ICOffice()
					Me._UpToICOfficeByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
					Me._UpToICOfficeByOfficeID.Query.Where(Me._UpToICOfficeByOfficeID.Query.OfficeID = Me.OfficeID)
					Me._UpToICOfficeByOfficeID.Query.Load()
				End If

				Return Me._UpToICOfficeByOfficeID
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByOfficeID")
				

				If value Is Nothing Then
				
					Me.OfficeID = Nothing
				
					Me._UpToICOfficeByOfficeID = Nothing
				Else
				
					Me.OfficeID = value.OfficeID
					
					Me._UpToICOfficeByOfficeID = value
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByOfficeID Is Nothing Then
				Me.OfficeID = Me._UpToICOfficeByOfficeID.OfficeID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Apnptid, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Apnptid
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.OfficeID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.OfficeID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedBy, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreatedOn, 3, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.AccountNumber, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.BranchCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Currency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.PaymentNatureCode, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.ProductTypeCode, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const Apnptid As String = "APNPTID"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const Apnptid As String = "Apnptid"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata)
			
				If ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.mapDelegates Is Nothing Then
					ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata._meta Is Nothing Then
					ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata._meta = New ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("Apnptid", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("OfficeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_AccountPaymentNatureProductTypeAndPrintLocations"
				meta.Destination = "IC_AccountPaymentNatureProductTypeAndPrintLocations"
				
				meta.spInsert = "proc_IC_AccountPaymentNatureProductTypeAndPrintLocationsInsert"
				meta.spUpdate = "proc_IC_AccountPaymentNatureProductTypeAndPrintLocationsUpdate"
				meta.spDelete = "proc_IC_AccountPaymentNatureProductTypeAndPrintLocationsDelete"
				meta.spLoadAll = "proc_IC_AccountPaymentNatureProductTypeAndPrintLocationsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountPaymentNatureProductTypeAndPrintLocationsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
