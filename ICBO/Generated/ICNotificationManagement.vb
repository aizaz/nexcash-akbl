
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_NotificationManagement' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICNotificationManagement))> _
	<XmlType("ICNotificationManagement")> _	
	Partial Public Class ICNotificationManagement 
		Inherits esICNotificationManagement
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICNotificationManagement()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal notificationID As System.Int32)
			Dim obj As New ICNotificationManagement()
			obj.NotificationID = notificationID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal notificationID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICNotificationManagement()
			obj.NotificationID = notificationID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICNotificationManagementCollection")> _
	Partial Public Class ICNotificationManagementCollection
		Inherits esICNotificationManagementCollection
		Implements IEnumerable(Of ICNotificationManagement)
	
		Public Function FindByPrimaryKey(ByVal notificationID As System.Int32) As ICNotificationManagement
			Return MyBase.SingleOrDefault(Function(e) e.NotificationID.HasValue AndAlso e.NotificationID.Value = notificationID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICNotificationManagement))> _
		Public Class ICNotificationManagementCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICNotificationManagementCollection)
			
			Public Shared Widening Operator CType(packet As ICNotificationManagementCollectionWCFPacket) As ICNotificationManagementCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICNotificationManagementCollection) As ICNotificationManagementCollectionWCFPacket
				Return New ICNotificationManagementCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICNotificationManagementQuery 
		Inherits esICNotificationManagementQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICNotificationManagementQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICNotificationManagementQuery) As String
			Return ICNotificationManagementQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICNotificationManagementQuery
			Return DirectCast(ICNotificationManagementQuery.SerializeHelper.FromXml(query, GetType(ICNotificationManagementQuery)), ICNotificationManagementQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICNotificationManagement
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal notificationID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(notificationID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(notificationID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal notificationID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(notificationID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(notificationID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal notificationID As System.Int32) As Boolean
		
			Dim query As New ICNotificationManagementQuery()
			query.Where(query.NotificationID = notificationID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal notificationID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("NotificationID", notificationID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_NotificationManagement.NotificationID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NotificationID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.NotificationID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.NotificationID, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.NotificationID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.EventID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EventID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.EventID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.EventID, value) Then
					Me._UpToICEventsByEventID = Nothing
					Me.OnPropertyChanged("UpToICEventsByEventID")
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.EventID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.IsSMSAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsSMSAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsSMSAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsSMSAllow, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsSMSAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.IsEmailAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsEmailAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsEmailAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsEmailAllow, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsEmailAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICNotificationManagementMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICNotificationManagementMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICNotificationManagementMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_NotificationManagement.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICNotificationManagementMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICEventsByEventID As ICEvents
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "NotificationID"
							Me.str().NotificationID = CType(value, string)
												
						Case "EventID"
							Me.str().EventID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "IsSMSAllow"
							Me.str().IsSMSAllow = CType(value, string)
												
						Case "IsEmailAllow"
							Me.str().IsEmailAllow = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "NotificationID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.NotificationID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.NotificationID)
							End If
						
						Case "EventID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.EventID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.EventID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.UserID)
							End If
						
						Case "IsSMSAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsSMSAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsSMSAllow)
							End If
						
						Case "IsEmailAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsEmailAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsEmailAllow)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICNotificationManagementMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICNotificationManagement)
				Me.entity = entity
			End Sub				
		
	
			Public Property NotificationID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.NotificationID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NotificationID = Nothing
					Else
						entity.NotificationID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property EventID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.EventID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EventID = Nothing
					Else
						entity.EventID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsSMSAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsSMSAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsSMSAllow = Nothing
					Else
						entity.IsSMSAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsEmailAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsEmailAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsEmailAllow = Nothing
					Else
						entity.IsEmailAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICNotificationManagement
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICNotificationManagementMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICNotificationManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICNotificationManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICNotificationManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICNotificationManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICNotificationManagementQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICNotificationManagementCollection
		Inherits esEntityCollection(Of ICNotificationManagement)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICNotificationManagementMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICNotificationManagementCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICNotificationManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICNotificationManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICNotificationManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICNotificationManagementQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICNotificationManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICNotificationManagementQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICNotificationManagementQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICNotificationManagementQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICNotificationManagementMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "NotificationID" 
					Return Me.NotificationID
				Case "EventID" 
					Return Me.EventID
				Case "UserID" 
					Return Me.UserID
				Case "IsSMSAllow" 
					Return Me.IsSMSAllow
				Case "IsEmailAllow" 
					Return Me.IsEmailAllow
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property NotificationID As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.NotificationID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property EventID As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.EventID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsSMSAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.IsSMSAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsEmailAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.IsEmailAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICNotificationManagementMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICNotificationManagement 
		Inherits esICNotificationManagement
		
	
		#Region "UpToICEventsByEventID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_Events
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICEventsByEventID As ICEvents
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICEventsByEventID Is Nothing _
						 AndAlso Not EventID.Equals(Nothing)  Then
						
					Me._UpToICEventsByEventID = New ICEvents()
					Me._UpToICEventsByEventID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICEventsByEventID", Me._UpToICEventsByEventID)
					Me._UpToICEventsByEventID.Query.Where(Me._UpToICEventsByEventID.Query.EventID = Me.EventID)
					Me._UpToICEventsByEventID.Query.Load()
				End If

				Return Me._UpToICEventsByEventID
			End Get
			
            Set(ByVal value As ICEvents)
				Me.RemovePreSave("UpToICEventsByEventID")
				

				If value Is Nothing Then
				
					Me.EventID = Nothing
				
					Me._UpToICEventsByEventID = Nothing
				Else
				
					Me.EventID = value.EventID
					
					Me._UpToICEventsByEventID = value
					Me.SetPreSave("UpToICEventsByEventID", Me._UpToICEventsByEventID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICEventsByEventID Is Nothing Then
				Me.EventID = Me._UpToICEventsByEventID.EventID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICNotificationManagementMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.NotificationID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.NotificationID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.EventID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.EventID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.UserID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.IsSMSAllow, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.IsSMSAllow
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.IsEmailAllow, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.IsEmailAllow
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.IsApproved, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.ApprovedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.Creater, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICNotificationManagementMetadata.ColumnNames.CreationDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICNotificationManagementMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICNotificationManagementMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const NotificationID As String = "NotificationID"
			 Public Const EventID As String = "EventID"
			 Public Const UserID As String = "UserID"
			 Public Const IsSMSAllow As String = "IsSMSAllow"
			 Public Const IsEmailAllow As String = "IsEmailAllow"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const NotificationID As String = "NotificationID"
			 Public Const EventID As String = "EventID"
			 Public Const UserID As String = "UserID"
			 Public Const IsSMSAllow As String = "IsSMSAllow"
			 Public Const IsEmailAllow As String = "IsEmailAllow"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICNotificationManagementMetadata)
			
				If ICNotificationManagementMetadata.mapDelegates Is Nothing Then
					ICNotificationManagementMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICNotificationManagementMetadata._meta Is Nothing Then
					ICNotificationManagementMetadata._meta = New ICNotificationManagementMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("NotificationID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("EventID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsSMSAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsEmailAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_NotificationManagement"
				meta.Destination = "IC_NotificationManagement"
				
				meta.spInsert = "proc_IC_NotificationManagementInsert"
				meta.spUpdate = "proc_IC_NotificationManagementUpdate"
				meta.spDelete = "proc_IC_NotificationManagementDelete"
				meta.spLoadAll = "proc_IC_NotificationManagementLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_NotificationManagementLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICNotificationManagementMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
