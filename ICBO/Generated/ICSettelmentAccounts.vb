
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_SettelmentAccounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICSettelmentAccounts))> _
	<XmlType("ICSettelmentAccounts")> _	
	Partial Public Class ICSettelmentAccounts 
		Inherits esICSettelmentAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICSettelmentAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal settelmentAccountID As System.Int32)
			Dim obj As New ICSettelmentAccounts()
			obj.SettelmentAccountID = settelmentAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal settelmentAccountID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICSettelmentAccounts()
			obj.SettelmentAccountID = settelmentAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICSettelmentAccountsCollection")> _
	Partial Public Class ICSettelmentAccountsCollection
		Inherits esICSettelmentAccountsCollection
		Implements IEnumerable(Of ICSettelmentAccounts)
	
		Public Function FindByPrimaryKey(ByVal settelmentAccountID As System.Int32) As ICSettelmentAccounts
			Return MyBase.SingleOrDefault(Function(e) e.SettelmentAccountID.HasValue AndAlso e.SettelmentAccountID.Value = settelmentAccountID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICSettelmentAccounts))> _
		Public Class ICSettelmentAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICSettelmentAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICSettelmentAccountsCollectionWCFPacket) As ICSettelmentAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICSettelmentAccountsCollection) As ICSettelmentAccountsCollectionWCFPacket
				Return New ICSettelmentAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICSettelmentAccountsQuery 
		Inherits esICSettelmentAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICSettelmentAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICSettelmentAccountsQuery) As String
			Return ICSettelmentAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICSettelmentAccountsQuery
			Return DirectCast(ICSettelmentAccountsQuery.SerializeHelper.FromXml(query, GetType(ICSettelmentAccountsQuery)), ICSettelmentAccountsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICSettelmentAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal settelmentAccountID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(settelmentAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(settelmentAccountID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal settelmentAccountID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(settelmentAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(settelmentAccountID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal settelmentAccountID As System.Int32) As Boolean
		
			Dim query As New ICSettelmentAccountsQuery()
			query.Where(query.SettelmentAccountID = settelmentAccountID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal settelmentAccountID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SettelmentAccountID", settelmentAccountID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_SettelmentAccounts.SettelmentAccountID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SettelmentAccountID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSettelmentAccountsMetadata.ColumnNames.SettelmentAccountID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSettelmentAccountsMetadata.ColumnNames.SettelmentAccountID, value) Then
					OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.SettelmentAccountID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SettelmentAccounts.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICSettelmentAccountsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSettelmentAccountsMetadata.ColumnNames.ProductTypeCode, value) Then
					OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SettelmentAccounts.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As System.String
			Get
				Return MyBase.GetSystemString(ICSettelmentAccountsMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSettelmentAccountsMetadata.ColumnNames.BankCode, value) Then
					OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SettelmentAccounts.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICSettelmentAccountsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSettelmentAccountsMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SettelmentAccounts.AccountTittle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountTittle As System.String
			Get
				Return MyBase.GetSystemString(ICSettelmentAccountsMetadata.ColumnNames.AccountTittle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSettelmentAccountsMetadata.ColumnNames.AccountTittle, value) Then
					OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.AccountTittle)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SettelmentAccountID"
							Me.str().SettelmentAccountID = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "AccountTittle"
							Me.str().AccountTittle = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SettelmentAccountID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SettelmentAccountID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSettelmentAccountsMetadata.PropertyNames.SettelmentAccountID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICSettelmentAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property SettelmentAccountID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SettelmentAccountID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SettelmentAccountID = Nothing
					Else
						entity.SettelmentAccountID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankCode As System.String 
				Get
					Dim data_ As System.String = entity.BankCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountTittle As System.String 
				Get
					Dim data_ As System.String = entity.AccountTittle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountTittle = Nothing
					Else
						entity.AccountTittle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICSettelmentAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSettelmentAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICSettelmentAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSettelmentAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICSettelmentAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICSettelmentAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICSettelmentAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICSettelmentAccountsCollection
		Inherits esEntityCollection(Of ICSettelmentAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSettelmentAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICSettelmentAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICSettelmentAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSettelmentAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICSettelmentAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICSettelmentAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICSettelmentAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICSettelmentAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICSettelmentAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICSettelmentAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICSettelmentAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SettelmentAccountID" 
					Return Me.SettelmentAccountID
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "BankCode" 
					Return Me.BankCode
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "AccountTittle" 
					Return Me.AccountTittle
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SettelmentAccountID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSettelmentAccountsMetadata.ColumnNames.SettelmentAccountID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSettelmentAccountsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSettelmentAccountsMetadata.ColumnNames.BankCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICSettelmentAccountsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountTittle As esQueryItem
			Get
				Return New esQueryItem(Me, ICSettelmentAccountsMetadata.ColumnNames.AccountTittle, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICSettelmentAccounts 
		Inherits esICSettelmentAccounts
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICSettelmentAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICSettelmentAccountsMetadata.ColumnNames.SettelmentAccountID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSettelmentAccountsMetadata.PropertyNames.SettelmentAccountID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSettelmentAccountsMetadata.ColumnNames.ProductTypeCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSettelmentAccountsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSettelmentAccountsMetadata.ColumnNames.BankCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSettelmentAccountsMetadata.PropertyNames.BankCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSettelmentAccountsMetadata.ColumnNames.AccountNumber, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSettelmentAccountsMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSettelmentAccountsMetadata.ColumnNames.AccountTittle, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSettelmentAccountsMetadata.PropertyNames.AccountTittle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICSettelmentAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SettelmentAccountID As String = "SettelmentAccountID"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const AccountTittle As String = "AccountTittle"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SettelmentAccountID As String = "SettelmentAccountID"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const AccountTittle As String = "AccountTittle"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICSettelmentAccountsMetadata)
			
				If ICSettelmentAccountsMetadata.mapDelegates Is Nothing Then
					ICSettelmentAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICSettelmentAccountsMetadata._meta Is Nothing Then
					ICSettelmentAccountsMetadata._meta = New ICSettelmentAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SettelmentAccountID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountTittle", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_SettelmentAccounts"
				meta.Destination = "IC_SettelmentAccounts"
				
				meta.spInsert = "proc_IC_SettelmentAccountsInsert"
				meta.spUpdate = "proc_IC_SettelmentAccountsUpdate"
				meta.spDelete = "proc_IC_SettelmentAccountsDelete"
				meta.spLoadAll = "proc_IC_SettelmentAccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_SettelmentAccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICSettelmentAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
