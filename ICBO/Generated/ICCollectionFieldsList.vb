
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionFieldsList' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionFieldsList))> _
	<XmlType("ICCollectionFieldsList")> _	
	Partial Public Class ICCollectionFieldsList 
		Inherits esICCollectionFieldsList
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionFieldsList()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionFieldID As System.Int32)
			Dim obj As New ICCollectionFieldsList()
			obj.CollectionFieldID = collectionFieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionFieldID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionFieldsList()
			obj.CollectionFieldID = collectionFieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionFieldsListCollection")> _
	Partial Public Class ICCollectionFieldsListCollection
		Inherits esICCollectionFieldsListCollection
		Implements IEnumerable(Of ICCollectionFieldsList)
	
		Public Function FindByPrimaryKey(ByVal collectionFieldID As System.Int32) As ICCollectionFieldsList
			Return MyBase.SingleOrDefault(Function(e) e.CollectionFieldID.HasValue AndAlso e.CollectionFieldID.Value = collectionFieldID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionFieldsList))> _
		Public Class ICCollectionFieldsListCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionFieldsListCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionFieldsListCollectionWCFPacket) As ICCollectionFieldsListCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionFieldsListCollection) As ICCollectionFieldsListCollectionWCFPacket
				Return New ICCollectionFieldsListCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionFieldsListQuery 
		Inherits esICCollectionFieldsListQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionFieldsListQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionFieldsListQuery) As String
			Return ICCollectionFieldsListQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionFieldsListQuery
			Return DirectCast(ICCollectionFieldsListQuery.SerializeHelper.FromXml(query, GetType(ICCollectionFieldsListQuery)), ICCollectionFieldsListQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionFieldsList
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionFieldID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionFieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionFieldID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionFieldID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionFieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionFieldID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionFieldID As System.Int32) As Boolean
		
			Dim query As New ICCollectionFieldsListQuery()
			query.Where(query.CollectionFieldID = collectionFieldID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionFieldID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionFieldID", collectionFieldID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionFieldsList.CollectionFieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionFieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.CollectionFieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.CollectionFieldID, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.CollectionFieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionFieldsListMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionFieldsListMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.FieldOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FieldOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FieldOrder, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionFieldsListMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionFieldsListMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.FixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FixLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FixLength, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredCash
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCash As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCash)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCash, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCash)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCheque, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredDD, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredPO, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredFTWithCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFTWithCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithCheque, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredFTWithOutCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFTWithOutCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithOutCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithOutCheque, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithOutCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.FieldDBLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDBLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FieldDBLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionFieldsListMetadata.ColumnNames.FieldDBLength, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldDBLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.MustRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MustRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.MustRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.MustRequired, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.MustRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredOnlineForm
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredOnlineForm As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredOnlineForm)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredOnlineForm)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionFieldsList.IsRequiredFileUploadTemplate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFileUploadTemplate As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, value) Then
					OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionFieldID"
							Me.str().CollectionFieldID = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldOrder"
							Me.str().FieldOrder = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FixLength"
							Me.str().FixLength = CType(value, string)
												
						Case "IsRequiredCash"
							Me.str().IsRequiredCash = CType(value, string)
												
						Case "IsRequiredCheque"
							Me.str().IsRequiredCheque = CType(value, string)
												
						Case "IsRequiredDD"
							Me.str().IsRequiredDD = CType(value, string)
												
						Case "IsRequiredPO"
							Me.str().IsRequiredPO = CType(value, string)
												
						Case "IsRequiredFTWithCheque"
							Me.str().IsRequiredFTWithCheque = CType(value, string)
												
						Case "IsRequiredFTWithOutCheque"
							Me.str().IsRequiredFTWithOutCheque = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "FieldDBLength"
							Me.str().FieldDBLength = CType(value, string)
												
						Case "MustRequired"
							Me.str().MustRequired = CType(value, string)
												
						Case "IsRequiredOnlineForm"
							Me.str().IsRequiredOnlineForm = CType(value, string)
												
						Case "IsRequiredFileUploadTemplate"
							Me.str().IsRequiredFileUploadTemplate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CollectionFieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionFieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.CollectionFieldID)
							End If
						
						Case "FieldOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldOrder)
							End If
						
						Case "FixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FixLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FixLength)
							End If
						
						Case "IsRequiredCash"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCash = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCash)
							End If
						
						Case "IsRequiredCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCheque)
							End If
						
						Case "IsRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredDD)
							End If
						
						Case "IsRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredPO)
							End If
						
						Case "IsRequiredFTWithCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFTWithCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithCheque)
							End If
						
						Case "IsRequiredFTWithOutCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFTWithOutCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithOutCheque)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsActive)
							End If
						
						Case "FieldDBLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldDBLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.FieldDBLength)
							End If
						
						Case "MustRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.MustRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.MustRequired)
							End If
						
						Case "IsRequiredOnlineForm"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredOnlineForm = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredOnlineForm)
							End If
						
						Case "IsRequiredFileUploadTemplate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFileUploadTemplate = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionFieldsList)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionFieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionFieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionFieldID = Nothing
					Else
						entity.CollectionFieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldOrder = Nothing
					Else
						entity.FieldOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FixLength = Nothing
					Else
						entity.FixLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCash As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCash
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCash = Nothing
					Else
						entity.IsRequiredCash = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCheque = Nothing
					Else
						entity.IsRequiredCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredDD = Nothing
					Else
						entity.IsRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredPO = Nothing
					Else
						entity.IsRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFTWithCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFTWithCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFTWithCheque = Nothing
					Else
						entity.IsRequiredFTWithCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFTWithOutCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFTWithOutCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFTWithOutCheque = Nothing
					Else
						entity.IsRequiredFTWithOutCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldDBLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldDBLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDBLength = Nothing
					Else
						entity.FieldDBLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property MustRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.MustRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MustRequired = Nothing
					Else
						entity.MustRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredOnlineForm As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredOnlineForm
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredOnlineForm = Nothing
					Else
						entity.IsRequiredOnlineForm = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFileUploadTemplate As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFileUploadTemplate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFileUploadTemplate = Nothing
					Else
						entity.IsRequiredFileUploadTemplate = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionFieldsList
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionFieldsListMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionFieldsListQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionFieldsListQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionFieldsListQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionFieldsListQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionFieldsListQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionFieldsListCollection
		Inherits esEntityCollection(Of ICCollectionFieldsList)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionFieldsListMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionFieldsListCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionFieldsListQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionFieldsListQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionFieldsListQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionFieldsListQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionFieldsListQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionFieldsListQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionFieldsListQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionFieldsListQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionFieldsListMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionFieldID" 
					Return Me.CollectionFieldID
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldOrder" 
					Return Me.FieldOrder
				Case "FieldName" 
					Return Me.FieldName
				Case "FixLength" 
					Return Me.FixLength
				Case "IsRequiredCash" 
					Return Me.IsRequiredCash
				Case "IsRequiredCheque" 
					Return Me.IsRequiredCheque
				Case "IsRequiredDD" 
					Return Me.IsRequiredDD
				Case "IsRequiredPO" 
					Return Me.IsRequiredPO
				Case "IsRequiredFTWithCheque" 
					Return Me.IsRequiredFTWithCheque
				Case "IsRequiredFTWithOutCheque" 
					Return Me.IsRequiredFTWithOutCheque
				Case "IsActive" 
					Return Me.IsActive
				Case "FieldDBLength" 
					Return Me.FieldDBLength
				Case "MustRequired" 
					Return Me.MustRequired
				Case "IsRequiredOnlineForm" 
					Return Me.IsRequiredOnlineForm
				Case "IsRequiredFileUploadTemplate" 
					Return Me.IsRequiredFileUploadTemplate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionFieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.CollectionFieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.FieldOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.FixLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCash As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCash, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFTWithCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFTWithOutCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithOutCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FieldDBLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.FieldDBLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property MustRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.MustRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredOnlineForm As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFileUploadTemplate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionFieldsList 
		Inherits esICCollectionFieldsList
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionFieldsListMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.CollectionFieldID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.CollectionFieldID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.FieldType, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.FieldOrder, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.FieldOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.FieldName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.FixLength, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.FixLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCash, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCash
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredCheque, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredDD, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredPO, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithCheque, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFTWithOutCheque, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFTWithOutCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsActive, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.FieldDBLength, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.FieldDBLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.MustRequired, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.MustRequired
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredOnlineForm
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, 15, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionFieldsListMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionFieldID As String = "CollectionFieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequiredCash As String = "IsRequiredCash"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsRequiredFTWithCheque As String = "IsRequiredFTWithCheque"
			 Public Const IsRequiredFTWithOutCheque As String = "IsRequiredFTWithOutCheque"
			 Public Const IsActive As String = "isActive"
			 Public Const FieldDBLength As String = "FieldDBLength"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsRequiredOnlineForm As String = "IsRequiredOnlineForm"
			 Public Const IsRequiredFileUploadTemplate As String = "IsRequiredFileUploadTemplate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionFieldID As String = "CollectionFieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequiredCash As String = "IsRequiredCash"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsRequiredFTWithCheque As String = "IsRequiredFTWithCheque"
			 Public Const IsRequiredFTWithOutCheque As String = "IsRequiredFTWithOutCheque"
			 Public Const IsActive As String = "IsActive"
			 Public Const FieldDBLength As String = "FieldDBLength"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsRequiredOnlineForm As String = "IsRequiredOnlineForm"
			 Public Const IsRequiredFileUploadTemplate As String = "IsRequiredFileUploadTemplate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionFieldsListMetadata)
			
				If ICCollectionFieldsListMetadata.mapDelegates Is Nothing Then
					ICCollectionFieldsListMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionFieldsListMetadata._meta Is Nothing Then
					ICCollectionFieldsListMetadata._meta = New ICCollectionFieldsListMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionFieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FixLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequiredCash", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredFTWithCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredFTWithOutCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FieldDBLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("MustRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredOnlineForm", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredFileUploadTemplate", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_CollectionFieldsList"
				meta.Destination = "IC_CollectionFieldsList"
				
				meta.spInsert = "proc_IC_CollectionFieldsListInsert"
				meta.spUpdate = "proc_IC_CollectionFieldsListUpdate"
				meta.spDelete = "proc_IC_CollectionFieldsListDelete"
				meta.spLoadAll = "proc_IC_CollectionFieldsListLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionFieldsListLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionFieldsListMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
