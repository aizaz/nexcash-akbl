
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:57 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_InstructionActivity' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstructionActivity))> _
	<XmlType("ICInstructionActivity")> _	
	Partial Public Class ICInstructionActivity 
		Inherits esICInstructionActivity
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstructionActivity()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal instructionActivityID As System.Int32)
			Dim obj As New ICInstructionActivity()
			obj.InstructionActivityID = instructionActivityID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal instructionActivityID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstructionActivity()
			obj.InstructionActivityID = instructionActivityID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstructionActivityCollection")> _
	Partial Public Class ICInstructionActivityCollection
		Inherits esICInstructionActivityCollection
		Implements IEnumerable(Of ICInstructionActivity)
	
		Public Function FindByPrimaryKey(ByVal instructionActivityID As System.Int32) As ICInstructionActivity
			Return MyBase.SingleOrDefault(Function(e) e.InstructionActivityID.HasValue AndAlso e.InstructionActivityID.Value = instructionActivityID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstructionActivity))> _
		Public Class ICInstructionActivityCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstructionActivityCollection)
			
			Public Shared Widening Operator CType(packet As ICInstructionActivityCollectionWCFPacket) As ICInstructionActivityCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstructionActivityCollection) As ICInstructionActivityCollectionWCFPacket
				Return New ICInstructionActivityCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstructionActivityQuery 
		Inherits esICInstructionActivityQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstructionActivityQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstructionActivityQuery) As String
			Return ICInstructionActivityQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstructionActivityQuery
			Return DirectCast(ICInstructionActivityQuery.SerializeHelper.FromXml(query, GetType(ICInstructionActivityQuery)), ICInstructionActivityQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstructionActivity
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal instructionActivityID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionActivityID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionActivityID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal instructionActivityID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionActivityID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionActivityID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal instructionActivityID As System.Int32) As Boolean
		
			Dim query As New ICInstructionActivityQuery()
			query.Where(query.InstructionActivityID = instructionActivityID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal instructionActivityID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("InstructionActivityID", instructionActivityID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_InstructionActivity.InstructionActivityID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionActivityID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.InstructionActivityID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.InstructionActivityID, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.InstructionActivityID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.InstructionID, value) Then
					Me._UpToICInstructionByInstructionID = Nothing
					Me.OnPropertyChanged("UpToICInstructionByInstructionID")
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.Action
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Action As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionActivityMetadata.ColumnNames.Action)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionActivityMetadata.ColumnNames.Action, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.Action)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.ActionBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ActionBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.ActionBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.ActionBy, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ActionBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.ActionDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ActionDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionActivityMetadata.ColumnNames.ActionDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionActivityMetadata.ColumnNames.ActionDate, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ActionDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.FromStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.FromStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.FromStatus, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.FromStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.ToStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.ToStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.ToStatus, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ToStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.BatchNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BatchNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionActivityMetadata.ColumnNames.BatchNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionActivityMetadata.ColumnNames.BatchNo, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.BatchNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionActivityMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionActivityMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionActivity.UserIP
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserIP As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionActivityMetadata.ColumnNames.UserIP)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionActivityMetadata.ColumnNames.UserIP, value) Then
					OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.UserIP)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionByInstructionID As ICInstruction
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "InstructionActivityID"
							Me.str().InstructionActivityID = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "Action"
							Me.str().Action = CType(value, string)
												
						Case "ActionBy"
							Me.str().ActionBy = CType(value, string)
												
						Case "ActionDate"
							Me.str().ActionDate = CType(value, string)
												
						Case "FromStatus"
							Me.str().FromStatus = CType(value, string)
												
						Case "ToStatus"
							Me.str().ToStatus = CType(value, string)
												
						Case "BatchNo"
							Me.str().BatchNo = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "UserIP"
							Me.str().UserIP = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "InstructionActivityID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionActivityID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.InstructionActivityID)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.InstructionID)
							End If
						
						Case "ActionBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ActionBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ActionBy)
							End If
						
						Case "ActionDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ActionDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ActionDate)
							End If
						
						Case "FromStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FromStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.FromStatus)
							End If
						
						Case "ToStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ToStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.ToStatus)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionActivityMetadata.PropertyNames.CompanyCode)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstructionActivity)
				Me.entity = entity
			End Sub				
		
	
			Public Property InstructionActivityID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionActivityID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionActivityID = Nothing
					Else
						entity.InstructionActivityID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Action As System.String 
				Get
					Dim data_ As System.String = entity.Action
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Action = Nothing
					Else
						entity.Action = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ActionBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ActionBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ActionBy = Nothing
					Else
						entity.ActionBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ActionDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ActionDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ActionDate = Nothing
					Else
						entity.ActionDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FromStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromStatus = Nothing
					Else
						entity.FromStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ToStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToStatus = Nothing
					Else
						entity.ToStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BatchNo As System.String 
				Get
					Dim data_ As System.String = entity.BatchNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BatchNo = Nothing
					Else
						entity.BatchNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserIP As System.String 
				Get
					Dim data_ As System.String = entity.UserIP
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserIP = Nothing
					Else
						entity.UserIP = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstructionActivity
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionActivityMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstructionActivityQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionActivityQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstructionActivityQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstructionActivityQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstructionActivityQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstructionActivityCollection
		Inherits esEntityCollection(Of ICInstructionActivity)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionActivityMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstructionActivityCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstructionActivityQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionActivityQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstructionActivityQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstructionActivityQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstructionActivityQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstructionActivityQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstructionActivityQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstructionActivityQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstructionActivityMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "InstructionActivityID" 
					Return Me.InstructionActivityID
				Case "InstructionID" 
					Return Me.InstructionID
				Case "Action" 
					Return Me.Action
				Case "ActionBy" 
					Return Me.ActionBy
				Case "ActionDate" 
					Return Me.ActionDate
				Case "FromStatus" 
					Return Me.FromStatus
				Case "ToStatus" 
					Return Me.ToStatus
				Case "BatchNo" 
					Return Me.BatchNo
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "UserIP" 
					Return Me.UserIP
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property InstructionActivityID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.InstructionActivityID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Action As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.Action, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ActionBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.ActionBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ActionDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.ActionDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property FromStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.FromStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ToStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.ToStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BatchNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.BatchNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserIP As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionActivityMetadata.ColumnNames.UserIP, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstructionActivity 
		Inherits esICInstructionActivity
		
	
		#Region "UpToICInstructionByInstructionID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_InstructionActivity_IC_Instruction
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionByInstructionID As ICInstruction
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionByInstructionID Is Nothing _
						 AndAlso Not InstructionID.Equals(Nothing)  Then
						
					Me._UpToICInstructionByInstructionID = New ICInstruction()
					Me._UpToICInstructionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
					Me._UpToICInstructionByInstructionID.Query.Where(Me._UpToICInstructionByInstructionID.Query.InstructionID = Me.InstructionID)
					Me._UpToICInstructionByInstructionID.Query.Load()
				End If

				Return Me._UpToICInstructionByInstructionID
			End Get
			
            Set(ByVal value As ICInstruction)
				Me.RemovePreSave("UpToICInstructionByInstructionID")
				

				If value Is Nothing Then
				
					Me.InstructionID = Nothing
				
					Me._UpToICInstructionByInstructionID = Nothing
				Else
				
					Me.InstructionID = value.InstructionID
					
					Me._UpToICInstructionByInstructionID = value
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionByInstructionID Is Nothing Then
				Me.InstructionID = Me._UpToICInstructionByInstructionID.InstructionID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstructionActivityMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.InstructionActivityID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.InstructionActivityID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.InstructionID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.Action, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.Action
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.ActionBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.ActionBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.ActionDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.ActionDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.FromStatus, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.FromStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.ToStatus, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.ToStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.BatchNo, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.BatchNo
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.CompanyCode, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionActivityMetadata.ColumnNames.UserIP, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionActivityMetadata.PropertyNames.UserIP
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstructionActivityMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const InstructionActivityID As String = "InstructionActivityID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const Action As String = "Action"
			 Public Const ActionBy As String = "ActionBy"
			 Public Const ActionDate As String = "ActionDate"
			 Public Const FromStatus As String = "FromStatus"
			 Public Const ToStatus As String = "ToStatus"
			 Public Const BatchNo As String = "BatchNo"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const UserIP As String = "UserIP"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const InstructionActivityID As String = "InstructionActivityID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const Action As String = "Action"
			 Public Const ActionBy As String = "ActionBy"
			 Public Const ActionDate As String = "ActionDate"
			 Public Const FromStatus As String = "FromStatus"
			 Public Const ToStatus As String = "ToStatus"
			 Public Const BatchNo As String = "BatchNo"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const UserIP As String = "UserIP"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstructionActivityMetadata)
			
				If ICInstructionActivityMetadata.mapDelegates Is Nothing Then
					ICInstructionActivityMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstructionActivityMetadata._meta Is Nothing Then
					ICInstructionActivityMetadata._meta = New ICInstructionActivityMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("InstructionActivityID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Action", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ActionBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ActionDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("FromStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ToStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BatchNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserIP", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_InstructionActivity"
				meta.Destination = "IC_InstructionActivity"
				
				meta.spInsert = "proc_IC_InstructionActivityInsert"
				meta.spUpdate = "proc_IC_InstructionActivityUpdate"
				meta.spDelete = "proc_IC_InstructionActivityDelete"
				meta.spLoadAll = "proc_IC_InstructionActivityLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstructionActivityLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstructionActivityMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
