
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:58 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_MasterSeries' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICMasterSeries))> _
	<XmlType("ICMasterSeries")> _	
	Partial Public Class ICMasterSeries 
		Inherits esICMasterSeries
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICMasterSeries()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal masterSeriesID As System.Int32)
			Dim obj As New ICMasterSeries()
			obj.MasterSeriesID = masterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal masterSeriesID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICMasterSeries()
			obj.MasterSeriesID = masterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICMasterSeriesCollection")> _
	Partial Public Class ICMasterSeriesCollection
		Inherits esICMasterSeriesCollection
		Implements IEnumerable(Of ICMasterSeries)
	
		Public Function FindByPrimaryKey(ByVal masterSeriesID As System.Int32) As ICMasterSeries
			Return MyBase.SingleOrDefault(Function(e) e.MasterSeriesID.HasValue AndAlso e.MasterSeriesID.Value = masterSeriesID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICMasterSeries))> _
		Public Class ICMasterSeriesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICMasterSeriesCollection)
			
			Public Shared Widening Operator CType(packet As ICMasterSeriesCollectionWCFPacket) As ICMasterSeriesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICMasterSeriesCollection) As ICMasterSeriesCollectionWCFPacket
				Return New ICMasterSeriesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICMasterSeriesQuery 
		Inherits esICMasterSeriesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICMasterSeriesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICMasterSeriesQuery) As String
			Return ICMasterSeriesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICMasterSeriesQuery
			Return DirectCast(ICMasterSeriesQuery.SerializeHelper.FromXml(query, GetType(ICMasterSeriesQuery)), ICMasterSeriesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICMasterSeries
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal masterSeriesID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(masterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(masterSeriesID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal masterSeriesID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(masterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(masterSeriesID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal masterSeriesID As System.Int32) As Boolean
		
			Dim query As New ICMasterSeriesQuery()
			query.Where(query.MasterSeriesID = masterSeriesID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal masterSeriesID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("MasterSeriesID", masterSeriesID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_MasterSeries.MasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMasterSeriesMetadata.ColumnNames.MasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMasterSeriesMetadata.ColumnNames.MasterSeriesID, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.MasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.StartFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StartFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICMasterSeriesMetadata.ColumnNames.StartFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICMasterSeriesMetadata.ColumnNames.StartFrom, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.StartFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.EndsAt
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EndsAt As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICMasterSeriesMetadata.ColumnNames.EndsAt)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICMasterSeriesMetadata.ColumnNames.EndsAt, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.EndsAt)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.AccountsPaymentNatureProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountsPaymentNatureProductTypeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMasterSeriesMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMasterSeriesMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICMasterSeriesMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMasterSeriesMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICMasterSeriesMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMasterSeriesMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICMasterSeriesMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMasterSeriesMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.DisbTYpe
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbTYpe As System.String
			Get
				Return MyBase.GetSystemString(ICMasterSeriesMetadata.ColumnNames.DisbTYpe)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMasterSeriesMetadata.ColumnNames.DisbTYpe, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.DisbTYpe)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.IsPrePrinted
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrePrinted As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsPrePrinted)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsPrePrinted, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsPrePrinted)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.IsApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICMasterSeriesMetadata.ColumnNames.IsApprove, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.Approvedby
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Approvedby As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMasterSeriesMetadata.ColumnNames.Approvedby)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMasterSeriesMetadata.ColumnNames.Approvedby, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.Approvedby)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMasterSeriesMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMasterSeriesMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMasterSeriesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMasterSeriesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MasterSeries.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMasterSeriesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "MasterSeriesID"
							Me.str().MasterSeriesID = CType(value, string)
												
						Case "StartFrom"
							Me.str().StartFrom = CType(value, string)
												
						Case "EndsAt"
							Me.str().EndsAt = CType(value, string)
												
						Case "AccountsPaymentNatureProductTypeCode"
							Me.str().AccountsPaymentNatureProductTypeCode = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "DisbTYpe"
							Me.str().DisbTYpe = CType(value, string)
												
						Case "IsPrePrinted"
							Me.str().IsPrePrinted = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApprove"
							Me.str().IsApprove = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Approvedby"
							Me.str().Approvedby = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "MasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.MasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.MasterSeriesID)
							End If
						
						Case "StartFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.StartFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.StartFrom)
							End If
						
						Case "EndsAt"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.EndsAt = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.EndsAt)
							End If
						
						Case "AccountsPaymentNatureProductTypeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AccountsPaymentNatureProductTypeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode)
							End If
						
						Case "IsPrePrinted"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrePrinted = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsPrePrinted)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.IsApprove)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Approvedby"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Approvedby = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.Approvedby)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreateBy)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMasterSeriesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICMasterSeries)
				Me.entity = entity
			End Sub				
		
	
			Public Property MasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.MasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MasterSeriesID = Nothing
					Else
						entity.MasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StartFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.StartFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StartFrom = Nothing
					Else
						entity.StartFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property EndsAt As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.EndsAt
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EndsAt = Nothing
					Else
						entity.EndsAt = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountsPaymentNatureProductTypeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AccountsPaymentNatureProductTypeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountsPaymentNatureProductTypeCode = Nothing
					Else
						entity.AccountsPaymentNatureProductTypeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbTYpe As System.String 
				Get
					Dim data_ As System.String = entity.DisbTYpe
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbTYpe = Nothing
					Else
						entity.DisbTYpe = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrePrinted As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrePrinted
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrePrinted = Nothing
					Else
						entity.IsPrePrinted = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApprove = Nothing
					Else
						entity.IsApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Approvedby As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Approvedby
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Approvedby = Nothing
					Else
						entity.Approvedby = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICMasterSeries
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMasterSeriesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICMasterSeriesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICMasterSeriesCollection
		Inherits esEntityCollection(Of ICMasterSeries)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMasterSeriesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICMasterSeriesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICMasterSeriesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICMasterSeriesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICMasterSeriesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICMasterSeriesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICMasterSeriesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "MasterSeriesID" 
					Return Me.MasterSeriesID
				Case "StartFrom" 
					Return Me.StartFrom
				Case "EndsAt" 
					Return Me.EndsAt
				Case "AccountsPaymentNatureProductTypeCode" 
					Return Me.AccountsPaymentNatureProductTypeCode
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "DisbTYpe" 
					Return Me.DisbTYpe
				Case "IsPrePrinted" 
					Return Me.IsPrePrinted
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApprove" 
					Return Me.IsApprove
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Approvedby" 
					Return Me.Approvedby
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "CreateBy" 
					Return Me.CreateBy
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property MasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.MasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StartFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.StartFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property EndsAt As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.EndsAt, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property AccountsPaymentNatureProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbTYpe As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.DisbTYpe, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrePrinted As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.IsPrePrinted, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.IsApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Approvedby As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.Approvedby, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMasterSeriesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICMasterSeries 
		Inherits esICMasterSeries
		
	
		#Region "ICInstrumentsCollectionByMasterSeriesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstrumentsCollectionByMasterSeriesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICMasterSeries.ICInstrumentsCollectionByMasterSeriesID_Delegate)
				map.PropertyName = "ICInstrumentsCollectionByMasterSeriesID"
				map.MyColumnName = "MasterSeriesID"
				map.ParentColumnName = "MasterSeriesID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstrumentsCollectionByMasterSeriesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICMasterSeriesQuery(data.NextAlias())
			
			Dim mee As ICInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstrumentsQuery), New ICInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.MasterSeriesID = mee.MasterSeriesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruments_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstrumentsCollectionByMasterSeriesID As ICInstrumentsCollection 
		
			Get
				If Me._ICInstrumentsCollectionByMasterSeriesID Is Nothing Then
					Me._ICInstrumentsCollectionByMasterSeriesID = New ICInstrumentsCollection()
					Me._ICInstrumentsCollectionByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstrumentsCollectionByMasterSeriesID", Me._ICInstrumentsCollectionByMasterSeriesID)
				
					If Not Me.MasterSeriesID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstrumentsCollectionByMasterSeriesID.Query.Where(Me._ICInstrumentsCollectionByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
							Me._ICInstrumentsCollectionByMasterSeriesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstrumentsCollectionByMasterSeriesID.fks.Add(ICInstrumentsMetadata.ColumnNames.MasterSeriesID, Me.MasterSeriesID)
					End If
				End If

				Return Me._ICInstrumentsCollectionByMasterSeriesID
			End Get
			
			Set(ByVal value As ICInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstrumentsCollectionByMasterSeriesID Is Nothing Then

					Me.RemovePostSave("ICInstrumentsCollectionByMasterSeriesID")
					Me._ICInstrumentsCollectionByMasterSeriesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstrumentsCollectionByMasterSeriesID As ICInstrumentsCollection
		#End Region

		#Region "ICSubSeriesCollectionByMasterSeriesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSeriesCollectionByMasterSeriesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICMasterSeries.ICSubSeriesCollectionByMasterSeriesID_Delegate)
				map.PropertyName = "ICSubSeriesCollectionByMasterSeriesID"
				map.MyColumnName = "MasterSeriesID"
				map.ParentColumnName = "MasterSeriesID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSeriesCollectionByMasterSeriesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICMasterSeriesQuery(data.NextAlias())
			
			Dim mee As ICSubSeriesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSeriesQuery), New ICSubSeriesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.MasterSeriesID = mee.MasterSeriesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSeries_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSeriesCollectionByMasterSeriesID As ICSubSeriesCollection 
		
			Get
				If Me._ICSubSeriesCollectionByMasterSeriesID Is Nothing Then
					Me._ICSubSeriesCollectionByMasterSeriesID = New ICSubSeriesCollection()
					Me._ICSubSeriesCollectionByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSeriesCollectionByMasterSeriesID", Me._ICSubSeriesCollectionByMasterSeriesID)
				
					If Not Me.MasterSeriesID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSeriesCollectionByMasterSeriesID.Query.Where(Me._ICSubSeriesCollectionByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
							Me._ICSubSeriesCollectionByMasterSeriesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSeriesCollectionByMasterSeriesID.fks.Add(ICSubSeriesMetadata.ColumnNames.MasterSeriesID, Me.MasterSeriesID)
					End If
				End If

				Return Me._ICSubSeriesCollectionByMasterSeriesID
			End Get
			
			Set(ByVal value As ICSubSeriesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSeriesCollectionByMasterSeriesID Is Nothing Then

					Me.RemovePostSave("ICSubSeriesCollectionByMasterSeriesID")
					Me._ICSubSeriesCollectionByMasterSeriesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSeriesCollectionByMasterSeriesID As ICSubSeriesCollection
		#End Region

		#Region "ICSubSetCollectionByMasterSeriesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSetCollectionByMasterSeriesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICMasterSeries.ICSubSetCollectionByMasterSeriesID_Delegate)
				map.PropertyName = "ICSubSetCollectionByMasterSeriesID"
				map.MyColumnName = "MasterSeriesID"
				map.ParentColumnName = "MasterSeriesID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSetCollectionByMasterSeriesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICMasterSeriesQuery(data.NextAlias())
			
			Dim mee As ICSubSetQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSetQuery), New ICSubSetQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.MasterSeriesID = mee.MasterSeriesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSet_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSetCollectionByMasterSeriesID As ICSubSetCollection 
		
			Get
				If Me._ICSubSetCollectionByMasterSeriesID Is Nothing Then
					Me._ICSubSetCollectionByMasterSeriesID = New ICSubSetCollection()
					Me._ICSubSetCollectionByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSetCollectionByMasterSeriesID", Me._ICSubSetCollectionByMasterSeriesID)
				
					If Not Me.MasterSeriesID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSetCollectionByMasterSeriesID.Query.Where(Me._ICSubSetCollectionByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
							Me._ICSubSetCollectionByMasterSeriesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSetCollectionByMasterSeriesID.fks.Add(ICSubSetMetadata.ColumnNames.MasterSeriesID, Me.MasterSeriesID)
					End If
				End If

				Return Me._ICSubSetCollectionByMasterSeriesID
			End Get
			
			Set(ByVal value As ICSubSetCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSetCollectionByMasterSeriesID Is Nothing Then

					Me.RemovePostSave("ICSubSetCollectionByMasterSeriesID")
					Me._ICSubSetCollectionByMasterSeriesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSetCollectionByMasterSeriesID As ICSubSetCollection
		#End Region

		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MasterSeries_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICInstrumentsCollectionByMasterSeriesID"
					coll = Me.ICInstrumentsCollectionByMasterSeriesID
					Exit Select
				Case "ICSubSeriesCollectionByMasterSeriesID"
					coll = Me.ICSubSeriesCollectionByMasterSeriesID
					Exit Select
				Case "ICSubSetCollectionByMasterSeriesID"
					coll = Me.ICSubSetCollectionByMasterSeriesID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICInstrumentsCollectionByMasterSeriesID", GetType(ICInstrumentsCollection), New ICInstruments()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSeriesCollectionByMasterSeriesID", GetType(ICSubSeriesCollection), New ICSubSeries()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSetCollectionByMasterSeriesID", GetType(ICSubSetCollection), New ICSubSet()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICInstrumentsCollectionByMasterSeriesID Is Nothing Then
				Apply(Me._ICInstrumentsCollectionByMasterSeriesID, "MasterSeriesID", Me.MasterSeriesID)
			End If
			
			If Not Me._ICSubSeriesCollectionByMasterSeriesID Is Nothing Then
				Apply(Me._ICSubSeriesCollectionByMasterSeriesID, "MasterSeriesID", Me.MasterSeriesID)
			End If
			
			If Not Me._ICSubSetCollectionByMasterSeriesID Is Nothing Then
				Apply(Me._ICSubSetCollectionByMasterSeriesID, "MasterSeriesID", Me.MasterSeriesID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICMasterSeriesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.MasterSeriesID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.MasterSeriesID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.StartFrom, 1, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.StartFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.EndsAt, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.EndsAt
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.AccountsPaymentNatureProductTypeCode, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.AccountsPaymentNatureProductTypeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.AccountNumber, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.BranchCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.Currency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.DisbTYpe, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.DisbTYpe
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.IsPrePrinted, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.IsPrePrinted
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.IsActive, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.IsApprove, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.IsApprove
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.CreateDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.Approvedby, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.Approvedby
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.ApprovedOn, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.CreateBy, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.Creater, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMasterSeriesMetadata.ColumnNames.CreationDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMasterSeriesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICMasterSeriesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const AccountsPaymentNatureProductTypeCode As String = "AccountsPaymentNatureProductTypeCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const DisbTYpe As String = "DisbTYpe"
			 Public Const IsPrePrinted As String = "IsPrePrinted"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const AccountsPaymentNatureProductTypeCode As String = "AccountsPaymentNatureProductTypeCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const DisbTYpe As String = "DisbTYpe"
			 Public Const IsPrePrinted As String = "IsPrePrinted"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICMasterSeriesMetadata)
			
				If ICMasterSeriesMetadata.mapDelegates Is Nothing Then
					ICMasterSeriesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICMasterSeriesMetadata._meta Is Nothing Then
					ICMasterSeriesMetadata._meta = New ICMasterSeriesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("MasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StartFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("EndsAt", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("AccountsPaymentNatureProductTypeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbTYpe", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsPrePrinted", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Approvedby", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_MasterSeries"
				meta.Destination = "IC_MasterSeries"
				
				meta.spInsert = "proc_IC_MasterSeriesInsert"
				meta.spUpdate = "proc_IC_MasterSeriesUpdate"
				meta.spDelete = "proc_IC_MasterSeriesDelete"
				meta.spLoadAll = "proc_IC_MasterSeriesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_MasterSeriesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICMasterSeriesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
