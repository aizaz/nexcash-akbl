
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 7/30/2015 10:46:36 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ApprovalRule' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICApprovalRule))> _
	<XmlType("ICApprovalRule")> _	
	Partial Public Class ICApprovalRule 
		Inherits esICApprovalRule
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICApprovalRule()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal approvalRuleID As System.Int32)
			Dim obj As New ICApprovalRule()
			obj.ApprovalRuleID = approvalRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal approvalRuleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICApprovalRule()
			obj.ApprovalRuleID = approvalRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICApprovalRuleCollection")> _
	Partial Public Class ICApprovalRuleCollection
		Inherits esICApprovalRuleCollection
		Implements IEnumerable(Of ICApprovalRule)
	
		Public Function FindByPrimaryKey(ByVal approvalRuleID As System.Int32) As ICApprovalRule
			Return MyBase.SingleOrDefault(Function(e) e.ApprovalRuleID.HasValue AndAlso e.ApprovalRuleID.Value = approvalRuleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICApprovalRule))> _
		Public Class ICApprovalRuleCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICApprovalRuleCollection)
			
			Public Shared Widening Operator CType(packet As ICApprovalRuleCollectionWCFPacket) As ICApprovalRuleCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICApprovalRuleCollection) As ICApprovalRuleCollectionWCFPacket
				Return New ICApprovalRuleCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICApprovalRuleQuery 
		Inherits esICApprovalRuleQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICApprovalRuleQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICApprovalRuleQuery) As String
			Return ICApprovalRuleQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICApprovalRuleQuery
			Return DirectCast(ICApprovalRuleQuery.SerializeHelper.FromXml(query, GetType(ICApprovalRuleQuery)), ICApprovalRuleQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRule
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal approvalRuleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal approvalRuleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal approvalRuleID As System.Int32) As Boolean
		
			Dim query As New ICApprovalRuleQuery()
			query.Where(query.ApprovalRuleID = approvalRuleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal approvalRuleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ApprovalRuleID", approvalRuleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ApprovalRule.ApprovalRuleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleID, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovalRuleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.ApprovalRuleName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleName As System.String
			Get
				Return MyBase.GetSystemString(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleName, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovalRuleName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICApprovalRuleMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICApprovalRuleMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.FromAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICApprovalRuleMetadata.ColumnNames.FromAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICApprovalRuleMetadata.ColumnNames.FromAmount, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.FromAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.ToAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICApprovalRuleMetadata.ColumnNames.ToAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICApprovalRuleMetadata.ColumnNames.ToAmount, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ToAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.Creator
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creator As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.Creator)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.Creator, value) Then
					Me._UpToICUserByCreator = Nothing
					Me.OnPropertyChanged("UpToICUserByCreator")
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.Creator)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICApprovalRuleMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICApprovalRuleMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.CopyCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CopyCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CopyCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.CopyCount, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CopyCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRule.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreator As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ApprovalRuleID"
							Me.str().ApprovalRuleID = CType(value, string)
												
						Case "ApprovalRuleName"
							Me.str().ApprovalRuleName = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "FromAmount"
							Me.str().FromAmount = CType(value, string)
												
						Case "ToAmount"
							Me.str().ToAmount = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creator"
							Me.str().Creator = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CopyCount"
							Me.str().CopyCount = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ApprovalRuleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalRuleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovalRuleID)
							End If
						
						Case "FromAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.FromAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.FromAmount)
							End If
						
						Case "ToAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.ToAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ToAmount)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creator"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creator = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.Creator)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.IsActive)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CopyCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CopyCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.CopyCount)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleMetadata.PropertyNames.ApprovedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICApprovalRule)
				Me.entity = entity
			End Sub				
		
	
			Public Property ApprovalRuleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalRuleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleID = Nothing
					Else
						entity.ApprovalRuleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalRuleName As System.String 
				Get
					Dim data_ As System.String = entity.ApprovalRuleName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleName = Nothing
					Else
						entity.ApprovalRuleName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.FromAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAmount = Nothing
					Else
						entity.FromAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.ToAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAmount = Nothing
					Else
						entity.ToAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creator As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creator
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creator = Nothing
					Else
						entity.Creator = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CopyCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CopyCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CopyCount = Nothing
					Else
						entity.CopyCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICApprovalRule
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICApprovalRuleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICApprovalRuleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICApprovalRuleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICApprovalRuleQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRuleCollection
		Inherits esEntityCollection(Of ICApprovalRule)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICApprovalRuleCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICApprovalRuleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICApprovalRuleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICApprovalRuleQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICApprovalRuleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICApprovalRuleQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICApprovalRuleQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICApprovalRuleQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICApprovalRuleMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ApprovalRuleID" 
					Return Me.ApprovalRuleID
				Case "ApprovalRuleName" 
					Return Me.ApprovalRuleName
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "FromAmount" 
					Return Me.FromAmount
				Case "ToAmount" 
					Return Me.ToAmount
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creator" 
					Return Me.Creator
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsActive" 
					Return Me.IsActive
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CopyCount" 
					Return Me.CopyCount
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ApprovalRuleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.ApprovalRuleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalRuleName As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.ApprovalRuleName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.FromAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ToAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.ToAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creator As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.Creator, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CopyCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.CopyCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICApprovalRule 
		Inherits esICApprovalRule
		
	
		#Region "ICApprovalRuleConditionsCollectionByApprovalRuleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleConditionsCollectionByApprovalRuleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICApprovalRule.ICApprovalRuleConditionsCollectionByApprovalRuleID_Delegate)
				map.PropertyName = "ICApprovalRuleConditionsCollectionByApprovalRuleID"
				map.MyColumnName = "ApprovalRuleID"
				map.ParentColumnName = "ApprovalRuleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleConditionsCollectionByApprovalRuleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICApprovalRuleQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleConditionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleConditionsQuery), New ICApprovalRuleConditionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ApprovalRuleID = mee.ApprovalRuleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_ApprovalRule
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleConditionsCollectionByApprovalRuleID As ICApprovalRuleConditionsCollection 
		
			Get
				If Me._ICApprovalRuleConditionsCollectionByApprovalRuleID Is Nothing Then
					Me._ICApprovalRuleConditionsCollectionByApprovalRuleID = New ICApprovalRuleConditionsCollection()
					Me._ICApprovalRuleConditionsCollectionByApprovalRuleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleConditionsCollectionByApprovalRuleID", Me._ICApprovalRuleConditionsCollectionByApprovalRuleID)
				
					If Not Me.ApprovalRuleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleConditionsCollectionByApprovalRuleID.Query.Where(Me._ICApprovalRuleConditionsCollectionByApprovalRuleID.Query.ApprovalRuleID = Me.ApprovalRuleID)
							Me._ICApprovalRuleConditionsCollectionByApprovalRuleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleConditionsCollectionByApprovalRuleID.fks.Add(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalRuleID, Me.ApprovalRuleID)
					End If
				End If

				Return Me._ICApprovalRuleConditionsCollectionByApprovalRuleID
			End Get
			
			Set(ByVal value As ICApprovalRuleConditionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleConditionsCollectionByApprovalRuleID Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleConditionsCollectionByApprovalRuleID")
					Me._ICApprovalRuleConditionsCollectionByApprovalRuleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleConditionsCollectionByApprovalRuleID As ICApprovalRuleConditionsCollection
		#End Region

		#Region "ICInstructionApprovalLogCollectionByApprovalRuleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionApprovalLogCollectionByApprovalRuleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICApprovalRule.ICInstructionApprovalLogCollectionByApprovalRuleID_Delegate)
				map.PropertyName = "ICInstructionApprovalLogCollectionByApprovalRuleID"
				map.MyColumnName = "ApprovalRuleID"
				map.ParentColumnName = "ApprovalRuleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionApprovalLogCollectionByApprovalRuleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICApprovalRuleQuery(data.NextAlias())
			
			Dim mee As ICInstructionApprovalLogQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionApprovalLogQuery), New ICInstructionApprovalLogQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ApprovalRuleID = mee.ApprovalRuleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_ApprovalRule
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionApprovalLogCollectionByApprovalRuleID As ICInstructionApprovalLogCollection 
		
			Get
				If Me._ICInstructionApprovalLogCollectionByApprovalRuleID Is Nothing Then
					Me._ICInstructionApprovalLogCollectionByApprovalRuleID = New ICInstructionApprovalLogCollection()
					Me._ICInstructionApprovalLogCollectionByApprovalRuleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionApprovalLogCollectionByApprovalRuleID", Me._ICInstructionApprovalLogCollectionByApprovalRuleID)
				
					If Not Me.ApprovalRuleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionApprovalLogCollectionByApprovalRuleID.Query.Where(Me._ICInstructionApprovalLogCollectionByApprovalRuleID.Query.ApprovalRuleID = Me.ApprovalRuleID)
							Me._ICInstructionApprovalLogCollectionByApprovalRuleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionApprovalLogCollectionByApprovalRuleID.fks.Add(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalRuleID, Me.ApprovalRuleID)
					End If
				End If

				Return Me._ICInstructionApprovalLogCollectionByApprovalRuleID
			End Get
			
			Set(ByVal value As ICInstructionApprovalLogCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionApprovalLogCollectionByApprovalRuleID Is Nothing Then

					Me.RemovePostSave("ICInstructionApprovalLogCollectionByApprovalRuleID")
					Me._ICInstructionApprovalLogCollectionByApprovalRuleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionApprovalLogCollectionByApprovalRuleID As ICInstructionApprovalLogCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreator - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreator As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreator Is Nothing _
						 AndAlso Not Creator.Equals(Nothing)  Then
						
					Me._UpToICUserByCreator = New ICUser()
					Me._UpToICUserByCreator.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
					Me._UpToICUserByCreator.Query.Where(Me._UpToICUserByCreator.Query.UserID = Me.Creator)
					Me._UpToICUserByCreator.Query.Load()
				End If

				Return Me._UpToICUserByCreator
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreator")
				

				If value Is Nothing Then
				
					Me.Creator = Nothing
				
					Me._UpToICUserByCreator = Nothing
				Else
				
					Me.Creator = value.UserID
					
					Me._UpToICUserByCreator = value
					Me.SetPreSave("UpToICUserByCreator", Me._UpToICUserByCreator)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICApprovalRuleConditionsCollectionByApprovalRuleID"
					coll = Me.ICApprovalRuleConditionsCollectionByApprovalRuleID
					Exit Select
				Case "ICInstructionApprovalLogCollectionByApprovalRuleID"
					coll = Me.ICInstructionApprovalLogCollectionByApprovalRuleID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleConditionsCollectionByApprovalRuleID", GetType(ICApprovalRuleConditionsCollection), New ICApprovalRuleConditions()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionApprovalLogCollectionByApprovalRuleID", GetType(ICInstructionApprovalLogCollection), New ICInstructionApprovalLog()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICApprovalRuleConditionsCollectionByApprovalRuleID Is Nothing Then
				Apply(Me._ICApprovalRuleConditionsCollectionByApprovalRuleID, "ApprovalRuleID", Me.ApprovalRuleID)
			End If
			
			If Not Me._ICInstructionApprovalLogCollectionByApprovalRuleID Is Nothing Then
				Apply(Me._ICInstructionApprovalLogCollectionByApprovalRuleID, "ApprovalRuleID", Me.ApprovalRuleID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICApprovalRuleMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.ApprovalRuleID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.ApprovalRuleName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.ApprovalRuleName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.PaymentNatureCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.FromAmount, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.FromAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.ToAmount, 4, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.ToAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.Creator, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.Creator
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.CreationDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.IsActive, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.CompanyCode, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.CopyCount, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.CopyCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.ApprovedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleMetadata.ColumnNames.ApprovedOn, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICApprovalRuleMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const ApprovalRuleName As String = "ApprovalRuleName"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FromAmount As String = "FromAmount"
			 Public Const ToAmount As String = "ToAmount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CopyCount As String = "CopyCount"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const ApprovalRuleName As String = "ApprovalRuleName"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FromAmount As String = "FromAmount"
			 Public Const ToAmount As String = "ToAmount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creator As String = "Creator"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CopyCount As String = "CopyCount"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICApprovalRuleMetadata)
			
				If ICApprovalRuleMetadata.mapDelegates Is Nothing Then
					ICApprovalRuleMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICApprovalRuleMetadata._meta Is Nothing Then
					ICApprovalRuleMetadata._meta = New ICApprovalRuleMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ApprovalRuleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovalRuleName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("ToAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creator", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CopyCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_ApprovalRule"
				meta.Destination = "IC_ApprovalRule"
				
				meta.spInsert = "proc_IC_ApprovalRuleInsert"
				meta.spUpdate = "proc_IC_ApprovalRuleUpdate"
				meta.spDelete = "proc_IC_ApprovalRuleDelete"
				meta.spLoadAll = "proc_IC_ApprovalRuleLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ApprovalRuleLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICApprovalRuleMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
