
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/22/2014 7:26:25 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_TemplateFields' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICTemplateFields))> _
	<XmlType("ICTemplateFields")> _	
	Partial Public Class ICTemplateFields 
		Inherits esICTemplateFields
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICTemplateFields()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fieldID As System.Int32, ByVal templateID As System.Int32)
			Dim obj As New ICTemplateFields()
			obj.FieldID = fieldID
			obj.TemplateID = templateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fieldID As System.Int32, ByVal templateID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICTemplateFields()
			obj.FieldID = fieldID
			obj.TemplateID = templateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICTemplateFieldsCollection")> _
	Partial Public Class ICTemplateFieldsCollection
		Inherits esICTemplateFieldsCollection
		Implements IEnumerable(Of ICTemplateFields)
	
		Public Function FindByPrimaryKey(ByVal fieldID As System.Int32, ByVal templateID As System.Int32) As ICTemplateFields
			Return MyBase.SingleOrDefault(Function(e) e.FieldID.HasValue AndAlso e.FieldID.Value = fieldID And e.TemplateID.HasValue AndAlso e.TemplateID.Value = templateID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICTemplateFields))> _
		Public Class ICTemplateFieldsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICTemplateFieldsCollection)
			
			Public Shared Widening Operator CType(packet As ICTemplateFieldsCollectionWCFPacket) As ICTemplateFieldsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICTemplateFieldsCollection) As ICTemplateFieldsCollectionWCFPacket
				Return New ICTemplateFieldsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICTemplateFieldsQuery 
		Inherits esICTemplateFieldsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICTemplateFieldsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICTemplateFieldsQuery) As String
			Return ICTemplateFieldsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICTemplateFieldsQuery
			Return DirectCast(ICTemplateFieldsQuery.SerializeHelper.FromXml(query, GetType(ICTemplateFieldsQuery)), ICTemplateFieldsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICTemplateFields
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fieldID As System.Int32, ByVal templateID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID, templateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID, templateID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fieldID As System.Int32, ByVal templateID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID, templateID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID, templateID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fieldID As System.Int32, ByVal templateID As System.Int32) As Boolean
		
			Dim query As New ICTemplateFieldsQuery()
			query.Where(query.FieldID = fieldID And query.TemplateID = templateID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fieldID As System.Int32, ByVal templateID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FieldID", fieldID)
						parms.Add("TemplateID", templateID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_TemplateFields.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FieldID, value) Then
					Me._UpToICFieldsListByFieldID = Nothing
					Me.OnPropertyChanged("UpToICFieldsListByFieldID")
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateFieldsMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateFieldsMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.FieldOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FieldOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FieldOrder, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICTemplateFieldsMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICTemplateFieldsMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.TemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.TemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.TemplateID, value) Then
					Me._UpToICTemplateByTemplateID = Nothing
					Me.OnPropertyChanged("UpToICTemplateByTemplateID")
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.TemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.FixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FixLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.FixLength, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredIBFT, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredIBFT, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.TemplateFieldsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TemplateFieldsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.TemplateFieldsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.TemplateFieldsID, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.TemplateFieldsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredFT, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredFT, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredDD, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredDD, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredPO, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredPO, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCheque, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCheque, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequired, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequired, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICTemplateFieldsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICTemplateFieldsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredCOTC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCOTC As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCOTC)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCOTC, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredCOTC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredCOTC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredCOTC As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCOTC)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCOTC, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCOTC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsRequiredBillPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredBillPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredBillPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsRequiredBillPayment, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredBillPayment)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_TemplateFields.IsMustRequiredBillPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsMustRequiredBillPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredBillPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredBillPayment, value) Then
					OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredBillPayment)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICFieldsListByFieldID As ICFieldsList
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICTemplateByTemplateID As ICTemplate
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldOrder"
							Me.str().FieldOrder = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "TemplateID"
							Me.str().TemplateID = CType(value, string)
												
						Case "FixLength"
							Me.str().FixLength = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsRequiredIBFT"
							Me.str().IsRequiredIBFT = CType(value, string)
												
						Case "IsMustRequiredIBFT"
							Me.str().IsMustRequiredIBFT = CType(value, string)
												
						Case "TemplateFieldsID"
							Me.str().TemplateFieldsID = CType(value, string)
												
						Case "IsRequiredFT"
							Me.str().IsRequiredFT = CType(value, string)
												
						Case "IsMustRequiredFT"
							Me.str().IsMustRequiredFT = CType(value, string)
												
						Case "IsRequiredDD"
							Me.str().IsRequiredDD = CType(value, string)
												
						Case "IsMustRequiredDD"
							Me.str().IsMustRequiredDD = CType(value, string)
												
						Case "IsRequiredPO"
							Me.str().IsRequiredPO = CType(value, string)
												
						Case "IsMustRequiredPO"
							Me.str().IsMustRequiredPO = CType(value, string)
												
						Case "IsRequiredCheque"
							Me.str().IsRequiredCheque = CType(value, string)
												
						Case "IsMustRequiredCheque"
							Me.str().IsMustRequiredCheque = CType(value, string)
												
						Case "IsRequired"
							Me.str().IsRequired = CType(value, string)
												
						Case "IsMustRequired"
							Me.str().IsMustRequired = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsRequiredCOTC"
							Me.str().IsRequiredCOTC = CType(value, string)
												
						Case "IsMustRequiredCOTC"
							Me.str().IsMustRequiredCOTC = CType(value, string)
												
						Case "IsRequiredBillPayment"
							Me.str().IsRequiredBillPayment = CType(value, string)
												
						Case "IsMustRequiredBillPayment"
							Me.str().IsMustRequiredBillPayment = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldID)
							End If
						
						Case "FieldOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FieldOrder)
							End If
						
						Case "TemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.TemplateID)
							End If
						
						Case "FixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FixLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.FixLength)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsRequiredIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredIBFT)
							End If
						
						Case "IsMustRequiredIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredIBFT)
							End If
						
						Case "TemplateFieldsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TemplateFieldsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.TemplateFieldsID)
							End If
						
						Case "IsRequiredFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredFT)
							End If
						
						Case "IsMustRequiredFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredFT)
							End If
						
						Case "IsRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredDD)
							End If
						
						Case "IsMustRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredDD)
							End If
						
						Case "IsRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredPO)
							End If
						
						Case "IsMustRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredPO)
							End If
						
						Case "IsRequiredCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredCheque)
							End If
						
						Case "IsMustRequiredCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCheque)
							End If
						
						Case "IsRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequired)
							End If
						
						Case "IsMustRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequired)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsRequiredCOTC"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCOTC = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredCOTC)
							End If
						
						Case "IsMustRequiredCOTC"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredCOTC = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCOTC)
							End If
						
						Case "IsRequiredBillPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredBillPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsRequiredBillPayment)
							End If
						
						Case "IsMustRequiredBillPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsMustRequiredBillPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredBillPayment)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICTemplateFields)
				Me.entity = entity
			End Sub				
		
	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldOrder = Nothing
					Else
						entity.FieldOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateID = Nothing
					Else
						entity.TemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FixLength = Nothing
					Else
						entity.FixLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredIBFT = Nothing
					Else
						entity.IsRequiredIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredIBFT = Nothing
					Else
						entity.IsMustRequiredIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property TemplateFieldsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TemplateFieldsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TemplateFieldsID = Nothing
					Else
						entity.TemplateFieldsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFT = Nothing
					Else
						entity.IsRequiredFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredFT = Nothing
					Else
						entity.IsMustRequiredFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredDD = Nothing
					Else
						entity.IsRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredDD = Nothing
					Else
						entity.IsMustRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredPO = Nothing
					Else
						entity.IsRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredPO = Nothing
					Else
						entity.IsMustRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCheque = Nothing
					Else
						entity.IsRequiredCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredCheque = Nothing
					Else
						entity.IsMustRequiredCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequired = Nothing
					Else
						entity.IsRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequired = Nothing
					Else
						entity.IsMustRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCOTC As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCOTC
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCOTC = Nothing
					Else
						entity.IsRequiredCOTC = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredCOTC As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredCOTC
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredCOTC = Nothing
					Else
						entity.IsMustRequiredCOTC = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredBillPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredBillPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredBillPayment = Nothing
					Else
						entity.IsRequiredBillPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsMustRequiredBillPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsMustRequiredBillPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsMustRequiredBillPayment = Nothing
					Else
						entity.IsMustRequiredBillPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICTemplateFields
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICTemplateFieldsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICTemplateFieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICTemplateFieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICTemplateFieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICTemplateFieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICTemplateFieldsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICTemplateFieldsCollection
		Inherits esEntityCollection(Of ICTemplateFields)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICTemplateFieldsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICTemplateFieldsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICTemplateFieldsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICTemplateFieldsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICTemplateFieldsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICTemplateFieldsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICTemplateFieldsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICTemplateFieldsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICTemplateFieldsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICTemplateFieldsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICTemplateFieldsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FieldID" 
					Return Me.FieldID
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldOrder" 
					Return Me.FieldOrder
				Case "FieldName" 
					Return Me.FieldName
				Case "TemplateID" 
					Return Me.TemplateID
				Case "FixLength" 
					Return Me.FixLength
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsRequiredIBFT" 
					Return Me.IsRequiredIBFT
				Case "IsMustRequiredIBFT" 
					Return Me.IsMustRequiredIBFT
				Case "TemplateFieldsID" 
					Return Me.TemplateFieldsID
				Case "IsRequiredFT" 
					Return Me.IsRequiredFT
				Case "IsMustRequiredFT" 
					Return Me.IsMustRequiredFT
				Case "IsRequiredDD" 
					Return Me.IsRequiredDD
				Case "IsMustRequiredDD" 
					Return Me.IsMustRequiredDD
				Case "IsRequiredPO" 
					Return Me.IsRequiredPO
				Case "IsMustRequiredPO" 
					Return Me.IsMustRequiredPO
				Case "IsRequiredCheque" 
					Return Me.IsRequiredCheque
				Case "IsMustRequiredCheque" 
					Return Me.IsMustRequiredCheque
				Case "IsRequired" 
					Return Me.IsRequired
				Case "IsMustRequired" 
					Return Me.IsMustRequired
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsRequiredCOTC" 
					Return Me.IsRequiredCOTC
				Case "IsMustRequiredCOTC" 
					Return Me.IsMustRequiredCOTC
				Case "IsRequiredBillPayment" 
					Return Me.IsRequiredBillPayment
				Case "IsMustRequiredBillPayment" 
					Return Me.IsMustRequiredBillPayment
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.FieldOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.TemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.FixLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property TemplateFieldsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.TemplateFieldsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCOTC As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredCOTC, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredCOTC As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCOTC, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredBillPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsRequiredBillPayment, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsMustRequiredBillPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredBillPayment, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICTemplateFields 
		Inherits esICTemplateFields
		
	
		#Region "UpToICFieldsListByFieldID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_TemplateFields_IC_FieldsList
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICFieldsListByFieldID As ICFieldsList
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICFieldsListByFieldID Is Nothing _
						 AndAlso Not FieldID.Equals(Nothing)  Then
						
					Me._UpToICFieldsListByFieldID = New ICFieldsList()
					Me._UpToICFieldsListByFieldID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICFieldsListByFieldID", Me._UpToICFieldsListByFieldID)
					Me._UpToICFieldsListByFieldID.Query.Where(Me._UpToICFieldsListByFieldID.Query.FieldID = Me.FieldID)
					Me._UpToICFieldsListByFieldID.Query.Load()
				End If

				Return Me._UpToICFieldsListByFieldID
			End Get
			
            Set(ByVal value As ICFieldsList)
				Me.RemovePreSave("UpToICFieldsListByFieldID")
				

				If value Is Nothing Then
				
					Me.FieldID = Nothing
				
					Me._UpToICFieldsListByFieldID = Nothing
				Else
				
					Me.FieldID = value.FieldID
					
					Me._UpToICFieldsListByFieldID = value
					Me.SetPreSave("UpToICFieldsListByFieldID", Me._UpToICFieldsListByFieldID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICTemplateByTemplateID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_TemplateFields_IC_Template
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICTemplateByTemplateID As ICTemplate
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICTemplateByTemplateID Is Nothing _
						 AndAlso Not TemplateID.Equals(Nothing)  Then
						
					Me._UpToICTemplateByTemplateID = New ICTemplate()
					Me._UpToICTemplateByTemplateID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
					Me._UpToICTemplateByTemplateID.Query.Where(Me._UpToICTemplateByTemplateID.Query.TemplateID = Me.TemplateID)
					Me._UpToICTemplateByTemplateID.Query.Load()
				End If

				Return Me._UpToICTemplateByTemplateID
			End Get
			
            Set(ByVal value As ICTemplate)
				Me.RemovePreSave("UpToICTemplateByTemplateID")
				

				If value Is Nothing Then
				
					Me.TemplateID = Nothing
				
					Me._UpToICTemplateByTemplateID = Nothing
				Else
				
					Me.TemplateID = value.TemplateID
					
					Me._UpToICTemplateByTemplateID = value
					Me.SetPreSave("UpToICTemplateByTemplateID", Me._UpToICTemplateByTemplateID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICFieldsListByFieldID Is Nothing Then
				Me.FieldID = Me._UpToICFieldsListByFieldID.FieldID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICTemplateByTemplateID Is Nothing Then
				Me.TemplateID = Me._UpToICTemplateByTemplateID.TemplateID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICTemplateFieldsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.FieldID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.FieldID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.FieldType, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.FieldOrder, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.FieldOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.FieldName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.TemplateID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.TemplateID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.FixLength, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.FixLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.CreateBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.CreateDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.ApprovedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.ApprovedOn, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredIBFT, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredIBFT, 12, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.TemplateFieldsID, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.TemplateFieldsID
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredFT, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredFT, 15, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredDD, 16, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredDD, 17, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredPO, 18, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredPO, 19, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCheque, 20, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCheque, 21, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequired, 22, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequired, 23, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.Creater, 24, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.CreationDate, 25, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredCOTC, 26, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredCOTC
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredCOTC, 27, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredCOTC
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsRequiredBillPayment, 28, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsRequiredBillPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICTemplateFieldsMetadata.ColumnNames.IsMustRequiredBillPayment, 29, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICTemplateFieldsMetadata.PropertyNames.IsMustRequiredBillPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICTemplateFieldsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const TemplateID As String = "TemplateID"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsMustRequiredIBFT As String = "IsMustRequiredIBFT"
			 Public Const TemplateFieldsID As String = "TemplateFieldsID"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsMustRequiredFT As String = "IsMustRequiredFT"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsMustRequiredDD As String = "IsMustRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsMustRequiredPO As String = "IsMustRequiredPO"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsMustRequiredCheque As String = "IsMustRequiredCheque"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsMustRequired As String = "IsMustRequired"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsMustRequiredCOTC As String = "IsMustRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
			 Public Const IsMustRequiredBillPayment As String = "IsMustRequiredBillPayment"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const TemplateID As String = "TemplateID"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsMustRequiredIBFT As String = "IsMustRequiredIBFT"
			 Public Const TemplateFieldsID As String = "TemplateFieldsID"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsMustRequiredFT As String = "IsMustRequiredFT"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsMustRequiredDD As String = "IsMustRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const IsMustRequiredPO As String = "IsMustRequiredPO"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsMustRequiredCheque As String = "IsMustRequiredCheque"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const IsMustRequired As String = "IsMustRequired"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsMustRequiredCOTC As String = "IsMustRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
			 Public Const IsMustRequiredBillPayment As String = "IsMustRequiredBillPayment"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICTemplateFieldsMetadata)
			
				If ICTemplateFieldsMetadata.mapDelegates Is Nothing Then
					ICTemplateFieldsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICTemplateFieldsMetadata._meta Is Nothing Then
					ICTemplateFieldsMetadata._meta = New ICTemplateFieldsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FixLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsRequiredIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("TemplateFieldsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequiredFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsRequiredCOTC", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredCOTC", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredBillPayment", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsMustRequiredBillPayment", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_TemplateFields"
				meta.Destination = "IC_TemplateFields"
				
				meta.spInsert = "proc_IC_TemplateFieldsInsert"
				meta.spUpdate = "proc_IC_TemplateFieldsUpdate"
				meta.spDelete = "proc_IC_TemplateFieldsDelete"
				meta.spLoadAll = "proc_IC_TemplateFieldsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_TemplateFieldsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICTemplateFieldsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
