
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CBIBFTDetails' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCBIBFTDetails))> _
	<XmlType("ICCBIBFTDetails")> _	
	Partial Public Class ICCBIBFTDetails 
		Inherits esICCBIBFTDetails
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCBIBFTDetails()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal iBFTDetailID As System.Int32)
			Dim obj As New ICCBIBFTDetails()
			obj.IBFTDetailID = iBFTDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal iBFTDetailID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCBIBFTDetails()
			obj.IBFTDetailID = iBFTDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCBIBFTDetailsCollection")> _
	Partial Public Class ICCBIBFTDetailsCollection
		Inherits esICCBIBFTDetailsCollection
		Implements IEnumerable(Of ICCBIBFTDetails)
	
		Public Function FindByPrimaryKey(ByVal iBFTDetailID As System.Int32) As ICCBIBFTDetails
			Return MyBase.SingleOrDefault(Function(e) e.IBFTDetailID.HasValue AndAlso e.IBFTDetailID.Value = iBFTDetailID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCBIBFTDetails))> _
		Public Class ICCBIBFTDetailsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCBIBFTDetailsCollection)
			
			Public Shared Widening Operator CType(packet As ICCBIBFTDetailsCollectionWCFPacket) As ICCBIBFTDetailsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCBIBFTDetailsCollection) As ICCBIBFTDetailsCollectionWCFPacket
				Return New ICCBIBFTDetailsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCBIBFTDetailsQuery 
		Inherits esICCBIBFTDetailsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCBIBFTDetailsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCBIBFTDetailsQuery) As String
			Return ICCBIBFTDetailsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCBIBFTDetailsQuery
			Return DirectCast(ICCBIBFTDetailsQuery.SerializeHelper.FromXml(query, GetType(ICCBIBFTDetailsQuery)), ICCBIBFTDetailsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCBIBFTDetails
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal iBFTDetailID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(iBFTDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(iBFTDetailID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal iBFTDetailID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(iBFTDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(iBFTDetailID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal iBFTDetailID As System.Int32) As Boolean
		
			Dim query As New ICCBIBFTDetailsQuery()
			query.Where(query.IBFTDetailID = iBFTDetailID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal iBFTDetailID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("IBFTDetailID", iBFTDetailID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CBIBFTDetails.IBFTDetailID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IBFTDetailID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCBIBFTDetailsMetadata.ColumnNames.IBFTDetailID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCBIBFTDetailsMetadata.ColumnNames.IBFTDetailID, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.IBFTDetailID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.FromBankIMD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromBankIMD As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.FromBankIMD)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.FromBankIMD, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.FromBankIMD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.FromAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.FromAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.FromAccountNumber, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.FromAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.ToBankIMD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToBankIMD As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.ToBankIMD)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.ToBankIMD, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.ToBankIMD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.ToAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.ToAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.ToAccountNumber, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.ToAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICCBIBFTDetailsMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICCBIBFTDetailsMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.EntityType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EntityType As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.EntityType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.EntityType, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.EntityType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBIBFTDetails.RelatedID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedID As System.String
			Get
				Return MyBase.GetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.RelatedID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBIBFTDetailsMetadata.ColumnNames.RelatedID, value) Then
					OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.RelatedID)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "IBFTDetailID"
							Me.str().IBFTDetailID = CType(value, string)
												
						Case "FromBankIMD"
							Me.str().FromBankIMD = CType(value, string)
												
						Case "FromAccountNumber"
							Me.str().FromAccountNumber = CType(value, string)
												
						Case "ToBankIMD"
							Me.str().ToBankIMD = CType(value, string)
												
						Case "ToAccountNumber"
							Me.str().ToAccountNumber = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
												
						Case "EntityType"
							Me.str().EntityType = CType(value, string)
												
						Case "RelatedID"
							Me.str().RelatedID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "IBFTDetailID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.IBFTDetailID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.IBFTDetailID)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICCBIBFTDetailsMetadata.PropertyNames.Amount)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCBIBFTDetails)
				Me.entity = entity
			End Sub				
		
	
			Public Property IBFTDetailID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.IBFTDetailID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IBFTDetailID = Nothing
					Else
						entity.IBFTDetailID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromBankIMD As System.String 
				Get
					Dim data_ As System.String = entity.FromBankIMD
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromBankIMD = Nothing
					Else
						entity.FromBankIMD = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNumber = Nothing
					Else
						entity.FromAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToBankIMD As System.String 
				Get
					Dim data_ As System.String = entity.ToBankIMD
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToBankIMD = Nothing
					Else
						entity.ToBankIMD = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNumber = Nothing
					Else
						entity.ToAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property EntityType As System.String 
				Get
					Dim data_ As System.String = entity.EntityType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EntityType = Nothing
					Else
						entity.EntityType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedID As System.String 
				Get
					Dim data_ As System.String = entity.RelatedID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedID = Nothing
					Else
						entity.RelatedID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCBIBFTDetails
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBIBFTDetailsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCBIBFTDetailsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBIBFTDetailsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCBIBFTDetailsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCBIBFTDetailsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCBIBFTDetailsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCBIBFTDetailsCollection
		Inherits esEntityCollection(Of ICCBIBFTDetails)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBIBFTDetailsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCBIBFTDetailsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCBIBFTDetailsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBIBFTDetailsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCBIBFTDetailsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCBIBFTDetailsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCBIBFTDetailsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCBIBFTDetailsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCBIBFTDetailsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCBIBFTDetailsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCBIBFTDetailsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "IBFTDetailID" 
					Return Me.IBFTDetailID
				Case "FromBankIMD" 
					Return Me.FromBankIMD
				Case "FromAccountNumber" 
					Return Me.FromAccountNumber
				Case "ToBankIMD" 
					Return Me.ToBankIMD
				Case "ToAccountNumber" 
					Return Me.ToAccountNumber
				Case "Amount" 
					Return Me.Amount
				Case "EntityType" 
					Return Me.EntityType
				Case "RelatedID" 
					Return Me.RelatedID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property IBFTDetailID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.IBFTDetailID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FromBankIMD As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.FromBankIMD, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.FromAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToBankIMD As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.ToBankIMD, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.ToAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property EntityType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.EntityType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBIBFTDetailsMetadata.ColumnNames.RelatedID, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCBIBFTDetails 
		Inherits esICCBIBFTDetails
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCBIBFTDetailsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.IBFTDetailID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.IBFTDetailID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.FromBankIMD, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.FromBankIMD
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.FromAccountNumber, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.FromAccountNumber
			c.CharacterMaxLength = 300
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.ToBankIMD, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.ToBankIMD
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.ToAccountNumber, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.ToAccountNumber
			c.CharacterMaxLength = 300
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.Amount, 5, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.EntityType, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.EntityType
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBIBFTDetailsMetadata.ColumnNames.RelatedID, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBIBFTDetailsMetadata.PropertyNames.RelatedID
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCBIBFTDetailsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const IBFTDetailID As String = "IBFTDetailID"
			 Public Const FromBankIMD As String = "FromBankIMD"
			 Public Const FromAccountNumber As String = "FromAccountNumber"
			 Public Const ToBankIMD As String = "ToBankIMD"
			 Public Const ToAccountNumber As String = "ToAccountNumber"
			 Public Const Amount As String = "Amount"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedID As String = "RelatedID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const IBFTDetailID As String = "IBFTDetailID"
			 Public Const FromBankIMD As String = "FromBankIMD"
			 Public Const FromAccountNumber As String = "FromAccountNumber"
			 Public Const ToBankIMD As String = "ToBankIMD"
			 Public Const ToAccountNumber As String = "ToAccountNumber"
			 Public Const Amount As String = "Amount"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedID As String = "RelatedID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCBIBFTDetailsMetadata)
			
				If ICCBIBFTDetailsMetadata.mapDelegates Is Nothing Then
					ICCBIBFTDetailsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCBIBFTDetailsMetadata._meta Is Nothing Then
					ICCBIBFTDetailsMetadata._meta = New ICCBIBFTDetailsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("IBFTDetailID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FromBankIMD", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToBankIMD", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Amount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("EntityType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RelatedID", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CBIBFTDetails"
				meta.Destination = "IC_CBIBFTDetails"
				
				meta.spInsert = "proc_IC_CBIBFTDetailsInsert"
				meta.spUpdate = "proc_IC_CBIBFTDetailsUpdate"
				meta.spDelete = "proc_IC_CBIBFTDetailsDelete"
				meta.spLoadAll = "proc_IC_CBIBFTDetailsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CBIBFTDetailsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCBIBFTDetailsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
