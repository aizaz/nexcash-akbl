
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:55 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionNature))> _
	<XmlType("ICCollectionNature")> _	
	Partial Public Class ICCollectionNature 
		Inherits esICCollectionNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionNatureCode As System.String)
			Dim obj As New ICCollectionNature()
			obj.CollectionNatureCode = collectionNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionNatureCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionNature()
			obj.CollectionNatureCode = collectionNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionNatureCollection")> _
	Partial Public Class ICCollectionNatureCollection
		Inherits esICCollectionNatureCollection
		Implements IEnumerable(Of ICCollectionNature)
	
		Public Function FindByPrimaryKey(ByVal collectionNatureCode As System.String) As ICCollectionNature
			Return MyBase.SingleOrDefault(Function(e) e.CollectionNatureCode = collectionNatureCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionNature))> _
		Public Class ICCollectionNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionNatureCollectionWCFPacket) As ICCollectionNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionNatureCollection) As ICCollectionNatureCollectionWCFPacket
				Return New ICCollectionNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionNatureQuery 
		Inherits esICCollectionNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionNatureQuery) As String
			Return ICCollectionNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionNatureQuery
			Return DirectCast(ICCollectionNatureQuery.SerializeHelper.FromXml(query, GetType(ICCollectionNatureQuery)), ICCollectionNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionNatureCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionNatureCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionNatureCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionNatureCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionNatureCode As System.String) As Boolean
		
			Dim query As New ICCollectionNatureQuery()
			query.Where(query.CollectionNatureCode = collectionNatureCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionNatureCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionNatureCode", collectionNatureCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionNature.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionNatureMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionNatureMetadata.ColumnNames.CollectionNatureCode, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNature.CollectionNatureName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureName As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionNatureMetadata.ColumnNames.CollectionNatureName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionNatureMetadata.ColumnNames.CollectionNatureName, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CollectionNatureName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNature.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionNatureMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionNatureMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNature.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionNatureMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionNatureMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNature.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionNatureMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionNatureMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionNature.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionNatureMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionNatureMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "CollectionNatureName"
							Me.str().CollectionNatureName = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionNatureMetadata.PropertyNames.IsActive)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionNatureName As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureName = Nothing
					Else
						entity.CollectionNatureName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionNatureCollection
		Inherits esEntityCollection(Of ICCollectionNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "CollectionNatureName" 
					Return Me.CollectionNatureName
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsActive" 
					Return Me.IsActive
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionNatureName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.CollectionNatureName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionNatureMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionNature 
		Inherits esICCollectionNature
		
	
		#Region "ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionNature.ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode"
				map.MyColumnName = "CollectionNatureCode"
				map.ParentColumnName = "CollectionNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionNatureQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsCollectionNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsCollectionNatureQuery), New ICCollectionAccountsCollectionNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CollectionNatureCode = mee.CollectionNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNature_IC_CollectionNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode As ICCollectionAccountsCollectionNatureCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode Is Nothing Then
					Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode = New ICCollectionAccountsCollectionNatureCollection()
					Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode", Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode)
				
					If Not Me.CollectionNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode.Query.Where(Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode.Query.CollectionNatureCode = Me.CollectionNatureCode)
							Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode.fks.Add(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.CollectionNatureCode, Me.CollectionNatureCode)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollectionNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode")
					Me._ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode As ICCollectionAccountsCollectionNatureCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode"
					coll = Me.ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionNatureCollectionByCollectionNatureCode", GetType(ICCollectionAccountsCollectionNatureCollection), New ICCollectionAccountsCollectionNature()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.CollectionNatureCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.CollectionNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.CollectionNatureName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.CollectionNatureName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.CompanyCode, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.CreatedBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.CreatedDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionNatureMetadata.ColumnNames.IsActive, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionNatureMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CollectionNatureName As String = "CollectionNatureName"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const CollectionNatureName As String = "CollectionNatureName"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionNatureMetadata)
			
				If ICCollectionNatureMetadata.mapDelegates Is Nothing Then
					ICCollectionNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionNatureMetadata._meta Is Nothing Then
					ICCollectionNatureMetadata._meta = New ICCollectionNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CollectionNatureName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_CollectionNature"
				meta.Destination = "IC_CollectionNature"
				
				meta.spInsert = "proc_IC_CollectionNatureInsert"
				meta.spUpdate = "proc_IC_CollectionNatureUpdate"
				meta.spDelete = "proc_IC_CollectionNatureDelete"
				meta.spLoadAll = "proc_IC_CollectionNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
