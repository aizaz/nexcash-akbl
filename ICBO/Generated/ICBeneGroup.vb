
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/30/2015 12:26:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_BeneGroup' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBeneGroup))> _
	<XmlType("ICBeneGroup")> _	
	Partial Public Class ICBeneGroup 
		Inherits esICBeneGroup
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBeneGroup()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal beneGroupCode As System.String)
			Dim obj As New ICBeneGroup()
			obj.BeneGroupCode = beneGroupCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal beneGroupCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBeneGroup()
			obj.BeneGroupCode = beneGroupCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBeneGroupCollection")> _
	Partial Public Class ICBeneGroupCollection
		Inherits esICBeneGroupCollection
		Implements IEnumerable(Of ICBeneGroup)
	
		Public Function FindByPrimaryKey(ByVal beneGroupCode As System.String) As ICBeneGroup
			Return MyBase.SingleOrDefault(Function(e) e.BeneGroupCode = beneGroupCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBeneGroup))> _
		Public Class ICBeneGroupCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBeneGroupCollection)
			
			Public Shared Widening Operator CType(packet As ICBeneGroupCollectionWCFPacket) As ICBeneGroupCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBeneGroupCollection) As ICBeneGroupCollectionWCFPacket
				Return New ICBeneGroupCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBeneGroupQuery 
		Inherits esICBeneGroupQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBeneGroupQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBeneGroupQuery) As String
			Return ICBeneGroupQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBeneGroupQuery
			Return DirectCast(ICBeneGroupQuery.SerializeHelper.FromXml(query, GetType(ICBeneGroupQuery)), ICBeneGroupQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBeneGroup
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal beneGroupCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneGroupCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneGroupCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal beneGroupCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneGroupCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneGroupCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal beneGroupCode As System.String) As Boolean
		
			Dim query As New ICBeneGroupQuery()
			query.Where(query.BeneGroupCode = beneGroupCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal beneGroupCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BeneGroupCode", beneGroupCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_BeneGroup.BeneGroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneGroupCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneGroupMetadata.ColumnNames.BeneGroupCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneGroupMetadata.ColumnNames.BeneGroupCode, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.BeneGroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.BeneGroupName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneGroupName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneGroupMetadata.ColumnNames.BeneGroupName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneGroupMetadata.ColumnNames.BeneGroupName, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.BeneGroupName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.BeneTransactionLimit
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneTransactionLimit As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICBeneGroupMetadata.ColumnNames.BeneTransactionLimit)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICBeneGroupMetadata.ColumnNames.BeneTransactionLimit, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.BeneTransactionLimit)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneGroupMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneGroupMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneGroupMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneGroupMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneGroupMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneGroupMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneGroupMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneGroupMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBeneGroupMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBeneGroupMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneGroupMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneGroupMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_BeneGroup.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneGroupMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneGroupMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BeneGroupCode"
							Me.str().BeneGroupCode = CType(value, string)
												
						Case "BeneGroupName"
							Me.str().BeneGroupName = CType(value, string)
												
						Case "BeneTransactionLimit"
							Me.str().BeneTransactionLimit = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BeneTransactionLimit"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.BeneTransactionLimit = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.BeneTransactionLimit)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreateDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneGroupMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBeneGroup)
				Me.entity = entity
			End Sub				
		
	
			Public Property BeneGroupCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneGroupCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneGroupCode = Nothing
					Else
						entity.BeneGroupCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneGroupName As System.String 
				Get
					Dim data_ As System.String = entity.BeneGroupName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneGroupName = Nothing
					Else
						entity.BeneGroupName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneTransactionLimit As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.BeneTransactionLimit
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneTransactionLimit = Nothing
					Else
						entity.BeneTransactionLimit = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBeneGroup
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneGroupMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBeneGroupQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneGroupQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBeneGroupQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBeneGroupQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBeneGroupQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBeneGroupCollection
		Inherits esEntityCollection(Of ICBeneGroup)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneGroupMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBeneGroupCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBeneGroupQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneGroupQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBeneGroupQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBeneGroupQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBeneGroupQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBeneGroupQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBeneGroupQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBeneGroupQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBeneGroupMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BeneGroupCode" 
					Return Me.BeneGroupCode
				Case "BeneGroupName" 
					Return Me.BeneGroupName
				Case "BeneTransactionLimit" 
					Return Me.BeneTransactionLimit
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BeneGroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.BeneGroupCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneGroupName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.BeneGroupName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneTransactionLimit As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.BeneTransactionLimit, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneGroupMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBeneGroup 
		Inherits esICBeneGroup
		
	
		#Region "ICBeneCollectionByBeneGroupCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneCollectionByBeneGroupCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICBeneGroup.ICBeneCollectionByBeneGroupCode_Delegate)
				map.PropertyName = "ICBeneCollectionByBeneGroupCode"
				map.MyColumnName = "BeneGroupCode"
				map.ParentColumnName = "BeneGroupCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneCollectionByBeneGroupCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICBeneGroupQuery(data.NextAlias())
			
			Dim mee As ICBeneQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneQuery), New ICBeneQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.BeneGroupCode = mee.BeneGroupCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Bene_IC_BeneGroup
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneCollectionByBeneGroupCode As ICBeneCollection 
		
			Get
				If Me._ICBeneCollectionByBeneGroupCode Is Nothing Then
					Me._ICBeneCollectionByBeneGroupCode = New ICBeneCollection()
					Me._ICBeneCollectionByBeneGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneCollectionByBeneGroupCode", Me._ICBeneCollectionByBeneGroupCode)
				
					If Not Me.BeneGroupCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneCollectionByBeneGroupCode.Query.Where(Me._ICBeneCollectionByBeneGroupCode.Query.BeneGroupCode = Me.BeneGroupCode)
							Me._ICBeneCollectionByBeneGroupCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneCollectionByBeneGroupCode.fks.Add(ICBeneMetadata.ColumnNames.BeneGroupCode, Me.BeneGroupCode)
					End If
				End If

				Return Me._ICBeneCollectionByBeneGroupCode
			End Get
			
			Set(ByVal value As ICBeneCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneCollectionByBeneGroupCode Is Nothing Then

					Me.RemovePostSave("ICBeneCollectionByBeneGroupCode")
					Me._ICBeneCollectionByBeneGroupCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneCollectionByBeneGroupCode As ICBeneCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_BeneGroup_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_BeneGroup_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICBeneCollectionByBeneGroupCode"
					coll = Me.ICBeneCollectionByBeneGroupCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICBeneCollectionByBeneGroupCode", GetType(ICBeneCollection), New ICBene()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICBeneGroupMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.BeneGroupCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.BeneGroupCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.BeneGroupName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.BeneGroupName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.BeneTransactionLimit, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.BeneTransactionLimit
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.CompanyCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.Creater, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneGroupMetadata.ColumnNames.CreationDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneGroupMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBeneGroupMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const BeneGroupName As String = "BeneGroupName"
			 Public Const BeneTransactionLimit As String = "BeneTransactionLimit"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "isActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const BeneGroupName As String = "BeneGroupName"
			 Public Const BeneTransactionLimit As String = "BeneTransactionLimit"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBeneGroupMetadata)
			
				If ICBeneGroupMetadata.mapDelegates Is Nothing Then
					ICBeneGroupMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBeneGroupMetadata._meta Is Nothing Then
					ICBeneGroupMetadata._meta = New ICBeneGroupMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BeneGroupCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneGroupName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneTransactionLimit", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_BeneGroup"
				meta.Destination = "IC_BeneGroup"
				
				meta.spInsert = "proc_IC_BeneGroupInsert"
				meta.spUpdate = "proc_IC_BeneGroupUpdate"
				meta.spDelete = "proc_IC_BeneGroupDelete"
				meta.spLoadAll = "proc_IC_BeneGroupLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BeneGroupLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBeneGroupMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
