
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 7/11/2015 3:27:04 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Bene' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBene))> _
	<XmlType("ICBene")> _	
	Partial Public Class ICBene 
		Inherits esICBene
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBene()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal beneID As System.Int32)
			Dim obj As New ICBene()
			obj.BeneID = beneID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal beneID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBene()
			obj.BeneID = beneID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBeneCollection")> _
	Partial Public Class ICBeneCollection
		Inherits esICBeneCollection
		Implements IEnumerable(Of ICBene)
	
		Public Function FindByPrimaryKey(ByVal beneID As System.Int32) As ICBene
			Return MyBase.SingleOrDefault(Function(e) e.BeneID.HasValue AndAlso e.BeneID.Value = beneID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBene))> _
		Public Class ICBeneCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBeneCollection)
			
			Public Shared Widening Operator CType(packet As ICBeneCollectionWCFPacket) As ICBeneCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBeneCollection) As ICBeneCollectionWCFPacket
				Return New ICBeneCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBeneQuery 
		Inherits esICBeneQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBeneQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBeneQuery) As String
			Return ICBeneQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBeneQuery
			Return DirectCast(ICBeneQuery.SerializeHelper.FromXml(query, GetType(ICBeneQuery)), ICBeneQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBene
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal beneID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal beneID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal beneID As System.Int32) As Boolean
		
			Dim query As New ICBeneQuery()
			query.Where(query.BeneID = beneID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal beneID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BeneID", beneID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Bene.BeneID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.BeneID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.BeneID, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCode, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryName, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAccountNo, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryAccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAccountTitle, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryAccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAddress As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAddress, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankCode, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankName, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBankAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankAddress As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBankAddress, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBankAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchCode, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBranchName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBranchName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchName, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBranchName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryCity
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCity As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCity)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCity, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryCity)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryCNIC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCNIC As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCNIC)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCNIC, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryCNIC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryCountry
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCountry As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCountry)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryCountry, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryCountry)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryEmail
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryEmail As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryEmail)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryEmail, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryEmail)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryMobile
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryMobile As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryMobile)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryMobile, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryMobile)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryPhone
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryPhone As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryPhone)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryPhone, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryPhone)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryProvince
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryProvince As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryProvince)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryProvince, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryProvince)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneAccountBranchCode, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneAccountCurrency, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICBeneMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICBeneMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneGroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneGroupCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneGroupCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneGroupCode, value) Then
					Me._UpToICBeneGroupByBeneGroupCode = Nothing
					Me.OnPropertyChanged("UpToICBeneGroupByBeneGroupCode")
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneGroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBeneMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBeneMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBeneMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBeneMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.ApprovalByforDeletion
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalByforDeletion As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.ApprovalByforDeletion)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.ApprovalByforDeletion, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovalByforDeletion)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.ApprovalforDeletionStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalforDeletionStatus As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBeneMetadata.ColumnNames.ApprovalforDeletionStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBeneMetadata.ColumnNames.ApprovalforDeletionStatus, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovalforDeletionStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryBranchAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBranchAddress As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryBranchAddress, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryBranchAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryAddedVia
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAddedVia As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAddedVia)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryAddedVia, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryAddedVia)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryIBFTAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIBFTAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountNo, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryIBFTAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryStatus As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryStatus, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryIBFTAccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIBFTAccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountTitle, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryIBFTAccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryIDType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIDType As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIDType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIDType, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryIDType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.BeneficiaryIDNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIDNo As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIDNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.BeneficiaryIDNo, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneficiaryIDNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.UBPReferenceNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UBPReferenceNo As System.String
			Get
				Return MyBase.GetSystemString(ICBeneMetadata.ColumnNames.UBPReferenceNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneMetadata.ColumnNames.UBPReferenceNo, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.UBPReferenceNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bene.UBPCompanyID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UBPCompanyID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneMetadata.ColumnNames.UBPCompanyID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneMetadata.ColumnNames.UBPCompanyID, value) Then
					OnPropertyChanged(ICBeneMetadata.PropertyNames.UBPCompanyID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBeneGroupByBeneGroupCode As ICBeneGroup
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BeneID"
							Me.str().BeneID = CType(value, string)
												
						Case "BeneficiaryCode"
							Me.str().BeneficiaryCode = CType(value, string)
												
						Case "BeneficiaryName"
							Me.str().BeneficiaryName = CType(value, string)
												
						Case "BeneficiaryAccountNo"
							Me.str().BeneficiaryAccountNo = CType(value, string)
												
						Case "BeneficiaryAccountTitle"
							Me.str().BeneficiaryAccountTitle = CType(value, string)
												
						Case "BeneficiaryAddress"
							Me.str().BeneficiaryAddress = CType(value, string)
												
						Case "BeneficiaryBankCode"
							Me.str().BeneficiaryBankCode = CType(value, string)
												
						Case "BeneficiaryBankName"
							Me.str().BeneficiaryBankName = CType(value, string)
												
						Case "BeneficiaryBankAddress"
							Me.str().BeneficiaryBankAddress = CType(value, string)
												
						Case "BeneficiaryBranchCode"
							Me.str().BeneficiaryBranchCode = CType(value, string)
												
						Case "BeneficiaryBranchName"
							Me.str().BeneficiaryBranchName = CType(value, string)
												
						Case "BeneficiaryCity"
							Me.str().BeneficiaryCity = CType(value, string)
												
						Case "BeneficiaryCNIC"
							Me.str().BeneficiaryCNIC = CType(value, string)
												
						Case "BeneficiaryCountry"
							Me.str().BeneficiaryCountry = CType(value, string)
												
						Case "BeneficiaryEmail"
							Me.str().BeneficiaryEmail = CType(value, string)
												
						Case "BeneficiaryMobile"
							Me.str().BeneficiaryMobile = CType(value, string)
												
						Case "BeneficiaryPhone"
							Me.str().BeneficiaryPhone = CType(value, string)
												
						Case "BeneficiaryProvince"
							Me.str().BeneficiaryProvince = CType(value, string)
												
						Case "BeneAccountBranchCode"
							Me.str().BeneAccountBranchCode = CType(value, string)
												
						Case "BeneAccountCurrency"
							Me.str().BeneAccountCurrency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "BeneGroupCode"
							Me.str().BeneGroupCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "ApprovalByforDeletion"
							Me.str().ApprovalByforDeletion = CType(value, string)
												
						Case "ApprovalforDeletionStatus"
							Me.str().ApprovalforDeletionStatus = CType(value, string)
												
						Case "BeneficiaryBranchAddress"
							Me.str().BeneficiaryBranchAddress = CType(value, string)
												
						Case "BeneficiaryAddedVia"
							Me.str().BeneficiaryAddedVia = CType(value, string)
												
						Case "BeneficiaryIBFTAccountNo"
							Me.str().BeneficiaryIBFTAccountNo = CType(value, string)
												
						Case "BeneficiaryStatus"
							Me.str().BeneficiaryStatus = CType(value, string)
												
						Case "BeneficiaryIBFTAccountTitle"
							Me.str().BeneficiaryIBFTAccountTitle = CType(value, string)
												
						Case "BeneficiaryIDType"
							Me.str().BeneficiaryIDType = CType(value, string)
												
						Case "BeneficiaryIDNo"
							Me.str().BeneficiaryIDNo = CType(value, string)
												
						Case "UBPReferenceNo"
							Me.str().UBPReferenceNo = CType(value, string)
												
						Case "UBPCompanyID"
							Me.str().UBPCompanyID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BeneID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BeneID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.BeneID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.CreateDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.CreationDate)
							End If
						
						Case "ApprovalByforDeletion"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalByforDeletion = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovalByforDeletion)
							End If
						
						Case "ApprovalforDeletionStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.ApprovalforDeletionStatus = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.ApprovalforDeletionStatus)
							End If
						
						Case "UBPCompanyID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UBPCompanyID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneMetadata.PropertyNames.UBPCompanyID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBene)
				Me.entity = entity
			End Sub				
		
	
			Public Property BeneID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BeneID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneID = Nothing
					Else
						entity.BeneID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCode = Nothing
					Else
						entity.BeneficiaryCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryName = Nothing
					Else
						entity.BeneficiaryName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountNo = Nothing
					Else
						entity.BeneficiaryAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountTitle = Nothing
					Else
						entity.BeneficiaryAccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAddress = Nothing
					Else
						entity.BeneficiaryAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankCode = Nothing
					Else
						entity.BeneficiaryBankCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankName = Nothing
					Else
						entity.BeneficiaryBankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankAddress = Nothing
					Else
						entity.BeneficiaryBankAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBranchCode = Nothing
					Else
						entity.BeneficiaryBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBranchName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBranchName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBranchName = Nothing
					Else
						entity.BeneficiaryBranchName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCity As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCity
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCity = Nothing
					Else
						entity.BeneficiaryCity = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCNIC As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCNIC
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCNIC = Nothing
					Else
						entity.BeneficiaryCNIC = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCountry As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCountry
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCountry = Nothing
					Else
						entity.BeneficiaryCountry = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryEmail As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryEmail
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryEmail = Nothing
					Else
						entity.BeneficiaryEmail = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryMobile As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryMobile
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryMobile = Nothing
					Else
						entity.BeneficiaryMobile = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryPhone As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryPhone
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryPhone = Nothing
					Else
						entity.BeneficiaryPhone = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryProvince As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryProvince
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryProvince = Nothing
					Else
						entity.BeneficiaryProvince = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAccountBranchCode = Nothing
					Else
						entity.BeneAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.BeneAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAccountCurrency = Nothing
					Else
						entity.BeneAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneGroupCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneGroupCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneGroupCode = Nothing
					Else
						entity.BeneGroupCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalByforDeletion As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalByforDeletion
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalByforDeletion = Nothing
					Else
						entity.ApprovalByforDeletion = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalforDeletionStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.ApprovalforDeletionStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalforDeletionStatus = Nothing
					Else
						entity.ApprovalforDeletionStatus = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBranchAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBranchAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBranchAddress = Nothing
					Else
						entity.BeneficiaryBranchAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAddedVia As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAddedVia
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAddedVia = Nothing
					Else
						entity.BeneficiaryAddedVia = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIBFTAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIBFTAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIBFTAccountNo = Nothing
					Else
						entity.BeneficiaryIBFTAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryStatus As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryStatus = Nothing
					Else
						entity.BeneficiaryStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIBFTAccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIBFTAccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIBFTAccountTitle = Nothing
					Else
						entity.BeneficiaryIBFTAccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIDType As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIDType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIDType = Nothing
					Else
						entity.BeneficiaryIDType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIDNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIDNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIDNo = Nothing
					Else
						entity.BeneficiaryIDNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UBPReferenceNo As System.String 
				Get
					Dim data_ As System.String = entity.UBPReferenceNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UBPReferenceNo = Nothing
					Else
						entity.UBPReferenceNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UBPCompanyID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UBPCompanyID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UBPCompanyID = Nothing
					Else
						entity.UBPCompanyID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBene
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBeneQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBeneQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBeneQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBeneQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBeneCollection
		Inherits esEntityCollection(Of ICBene)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBeneCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBeneQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBeneQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBeneQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBeneQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBeneQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBeneQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBeneQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBeneMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BeneID" 
					Return Me.BeneID
				Case "BeneficiaryCode" 
					Return Me.BeneficiaryCode
				Case "BeneficiaryName" 
					Return Me.BeneficiaryName
				Case "BeneficiaryAccountNo" 
					Return Me.BeneficiaryAccountNo
				Case "BeneficiaryAccountTitle" 
					Return Me.BeneficiaryAccountTitle
				Case "BeneficiaryAddress" 
					Return Me.BeneficiaryAddress
				Case "BeneficiaryBankCode" 
					Return Me.BeneficiaryBankCode
				Case "BeneficiaryBankName" 
					Return Me.BeneficiaryBankName
				Case "BeneficiaryBankAddress" 
					Return Me.BeneficiaryBankAddress
				Case "BeneficiaryBranchCode" 
					Return Me.BeneficiaryBranchCode
				Case "BeneficiaryBranchName" 
					Return Me.BeneficiaryBranchName
				Case "BeneficiaryCity" 
					Return Me.BeneficiaryCity
				Case "BeneficiaryCNIC" 
					Return Me.BeneficiaryCNIC
				Case "BeneficiaryCountry" 
					Return Me.BeneficiaryCountry
				Case "BeneficiaryEmail" 
					Return Me.BeneficiaryEmail
				Case "BeneficiaryMobile" 
					Return Me.BeneficiaryMobile
				Case "BeneficiaryPhone" 
					Return Me.BeneficiaryPhone
				Case "BeneficiaryProvince" 
					Return Me.BeneficiaryProvince
				Case "BeneAccountBranchCode" 
					Return Me.BeneAccountBranchCode
				Case "BeneAccountCurrency" 
					Return Me.BeneAccountCurrency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "BeneGroupCode" 
					Return Me.BeneGroupCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "ApprovalByforDeletion" 
					Return Me.ApprovalByforDeletion
				Case "ApprovalforDeletionStatus" 
					Return Me.ApprovalforDeletionStatus
				Case "BeneficiaryBranchAddress" 
					Return Me.BeneficiaryBranchAddress
				Case "BeneficiaryAddedVia" 
					Return Me.BeneficiaryAddedVia
				Case "BeneficiaryIBFTAccountNo" 
					Return Me.BeneficiaryIBFTAccountNo
				Case "BeneficiaryStatus" 
					Return Me.BeneficiaryStatus
				Case "BeneficiaryIBFTAccountTitle" 
					Return Me.BeneficiaryIBFTAccountTitle
				Case "BeneficiaryIDType" 
					Return Me.BeneficiaryIDType
				Case "BeneficiaryIDNo" 
					Return Me.BeneficiaryIDNo
				Case "UBPReferenceNo" 
					Return Me.UBPReferenceNo
				Case "UBPCompanyID" 
					Return Me.UBPCompanyID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BeneID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryAccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBankCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBankAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBranchName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBranchName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCity As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryCity, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCNIC As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryCNIC, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCountry As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryCountry, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryEmail As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryEmail, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryMobile As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryMobile, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryPhone As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryPhone, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryProvince As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryProvince, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneGroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneGroupCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalByforDeletion As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.ApprovalByforDeletion, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalforDeletionStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.ApprovalforDeletionStatus, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBranchAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryBranchAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAddedVia As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryAddedVia, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIBFTAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIBFTAccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIDType As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryIDType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIDNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.BeneficiaryIDNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UBPReferenceNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.UBPReferenceNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UBPCompanyID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneMetadata.ColumnNames.UBPCompanyID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBene 
		Inherits esICBene
		
	
		#Region "ICBeneProductTypeCollectionByBeneID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneProductTypeCollectionByBeneID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICBene.ICBeneProductTypeCollectionByBeneID_Delegate)
				map.PropertyName = "ICBeneProductTypeCollectionByBeneID"
				map.MyColumnName = "BeneID"
				map.ParentColumnName = "BeneID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneProductTypeCollectionByBeneID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICBeneQuery(data.NextAlias())
			
			Dim mee As ICBeneProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneProductTypeQuery), New ICBeneProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.BeneID = mee.BeneID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_BeneProductType_IC_Bene
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneProductTypeCollectionByBeneID As ICBeneProductTypeCollection 
		
			Get
				If Me._ICBeneProductTypeCollectionByBeneID Is Nothing Then
					Me._ICBeneProductTypeCollectionByBeneID = New ICBeneProductTypeCollection()
					Me._ICBeneProductTypeCollectionByBeneID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneProductTypeCollectionByBeneID", Me._ICBeneProductTypeCollectionByBeneID)
				
					If Not Me.BeneID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneProductTypeCollectionByBeneID.Query.Where(Me._ICBeneProductTypeCollectionByBeneID.Query.BeneID = Me.BeneID)
							Me._ICBeneProductTypeCollectionByBeneID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneProductTypeCollectionByBeneID.fks.Add(ICBeneProductTypeMetadata.ColumnNames.BeneID, Me.BeneID)
					End If
				End If

				Return Me._ICBeneProductTypeCollectionByBeneID
			End Get
			
			Set(ByVal value As ICBeneProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneProductTypeCollectionByBeneID Is Nothing Then

					Me.RemovePostSave("ICBeneProductTypeCollectionByBeneID")
					Me._ICBeneProductTypeCollectionByBeneID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneProductTypeCollectionByBeneID As ICBeneProductTypeCollection
		#End Region

		#Region "ICInstructionCollectionByBeneID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByBeneID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICBene.ICInstructionCollectionByBeneID_Delegate)
				map.PropertyName = "ICInstructionCollectionByBeneID"
				map.MyColumnName = "BeneID"
				map.ParentColumnName = "BeneID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByBeneID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICBeneQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.BeneID = mee.BeneID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_Bene
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByBeneID As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByBeneID Is Nothing Then
					Me._ICInstructionCollectionByBeneID = New ICInstructionCollection()
					Me._ICInstructionCollectionByBeneID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByBeneID", Me._ICInstructionCollectionByBeneID)
				
					If Not Me.BeneID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByBeneID.Query.Where(Me._ICInstructionCollectionByBeneID.Query.BeneID = Me.BeneID)
							Me._ICInstructionCollectionByBeneID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByBeneID.fks.Add(ICInstructionMetadata.ColumnNames.BeneID, Me.BeneID)
					End If
				End If

				Return Me._ICInstructionCollectionByBeneID
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByBeneID Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByBeneID")
					Me._ICInstructionCollectionByBeneID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByBeneID As ICInstructionCollection
		#End Region

		#Region "UpToICBeneGroupByBeneGroupCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Bene_IC_BeneGroup
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBeneGroupByBeneGroupCode As ICBeneGroup
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBeneGroupByBeneGroupCode Is Nothing _
						 AndAlso Not BeneGroupCode.Equals(Nothing)  Then
						
					Me._UpToICBeneGroupByBeneGroupCode = New ICBeneGroup()
					Me._UpToICBeneGroupByBeneGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBeneGroupByBeneGroupCode", Me._UpToICBeneGroupByBeneGroupCode)
					Me._UpToICBeneGroupByBeneGroupCode.Query.Where(Me._UpToICBeneGroupByBeneGroupCode.Query.BeneGroupCode = Me.BeneGroupCode)
					Me._UpToICBeneGroupByBeneGroupCode.Query.Load()
				End If

				Return Me._UpToICBeneGroupByBeneGroupCode
			End Get
			
            Set(ByVal value As ICBeneGroup)
				Me.RemovePreSave("UpToICBeneGroupByBeneGroupCode")
				

				If value Is Nothing Then
				
					Me.BeneGroupCode = Nothing
				
					Me._UpToICBeneGroupByBeneGroupCode = Nothing
				Else
				
					Me.BeneGroupCode = value.BeneGroupCode
					
					Me._UpToICBeneGroupByBeneGroupCode = value
					Me.SetPreSave("UpToICBeneGroupByBeneGroupCode", Me._UpToICBeneGroupByBeneGroupCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Bene_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Bene_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICBeneProductTypeCollectionByBeneID"
					coll = Me.ICBeneProductTypeCollectionByBeneID
					Exit Select
				Case "ICInstructionCollectionByBeneID"
					coll = Me.ICInstructionCollectionByBeneID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICBeneProductTypeCollectionByBeneID", GetType(ICBeneProductTypeCollection), New ICBeneProductType()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByBeneID", GetType(ICInstructionCollection), New ICInstruction()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICBeneProductTypeCollectionByBeneID Is Nothing Then
				Apply(Me._ICBeneProductTypeCollectionByBeneID, "BeneID", Me.BeneID)
			End If
			
			If Not Me._ICInstructionCollectionByBeneID Is Nothing Then
				Apply(Me._ICInstructionCollectionByBeneID, "BeneID", Me.BeneID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICBeneMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryAccountNo, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryAccountTitle, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryAccountTitle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryAddress, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBankCode, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBankCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBankName, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBankAddress, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBankAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBranchCode, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBranchCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBranchName, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBranchName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryCity, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryCity
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryCNIC, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryCNIC
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryCountry, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryCountry
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryEmail, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryEmail
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryMobile, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryMobile
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryPhone, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryPhone
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryProvince, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryProvince
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneAccountBranchCode, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneAccountCurrency, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.PaymentNatureCode, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.CompanyCode, 21, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneGroupCode, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneGroupCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.CreateBy, 23, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.CreateDate, 24, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.IsActive, 25, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBeneMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.IsApproved, 26, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBeneMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.ApprovedOn, 27, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.ApprovedBy, 28, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.Creater, 29, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.CreationDate, 30, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.ApprovalByforDeletion, 31, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.ApprovalByforDeletion
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.ApprovalforDeletionStatus, 32, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBeneMetadata.PropertyNames.ApprovalforDeletionStatus
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryBranchAddress, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryBranchAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryAddedVia, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryAddedVia
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountNo, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryIBFTAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryStatus, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryStatus
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryIBFTAccountTitle, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryIBFTAccountTitle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryIDType, 38, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryIDType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.BeneficiaryIDNo, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.BeneficiaryIDNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.UBPReferenceNo, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneMetadata.PropertyNames.UBPReferenceNo
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneMetadata.ColumnNames.UBPCompanyID, 41, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneMetadata.PropertyNames.UBPCompanyID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBeneMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const BeneficiaryName As String = "BeneficiaryName"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountTitle As String = "BeneficiaryAccountTitle"
			 Public Const BeneficiaryAddress As String = "BeneficiaryAddress"
			 Public Const BeneficiaryBankCode As String = "BeneficiaryBankCode"
			 Public Const BeneficiaryBankName As String = "BeneficiaryBankName"
			 Public Const BeneficiaryBankAddress As String = "BeneficiaryBankAddress"
			 Public Const BeneficiaryBranchCode As String = "BeneficiaryBranchCode"
			 Public Const BeneficiaryBranchName As String = "BeneficiaryBranchName"
			 Public Const BeneficiaryCity As String = "BeneficiaryCity"
			 Public Const BeneficiaryCNIC As String = "BeneficiaryCNIC"
			 Public Const BeneficiaryCountry As String = "BeneficiaryCountry"
			 Public Const BeneficiaryEmail As String = "BeneficiaryEmail"
			 Public Const BeneficiaryMobile As String = "BeneficiaryMobile"
			 Public Const BeneficiaryPhone As String = "BeneficiaryPhone"
			 Public Const BeneficiaryProvince As String = "BeneficiaryProvince"
			 Public Const BeneAccountBranchCode As String = "BeneAccountBranchCode"
			 Public Const BeneAccountCurrency As String = "BeneAccountCurrency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "isActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ApprovalByforDeletion As String = "ApprovalByforDeletion"
			 Public Const ApprovalforDeletionStatus As String = "ApprovalforDeletionStatus"
			 Public Const BeneficiaryBranchAddress As String = "BeneficiaryBranchAddress"
			 Public Const BeneficiaryAddedVia As String = "BeneficiaryAddedVia"
			 Public Const BeneficiaryIBFTAccountNo As String = "BeneficiaryIBFTAccountNo"
			 Public Const BeneficiaryStatus As String = "BeneficiaryStatus"
			 Public Const BeneficiaryIBFTAccountTitle As String = "BeneficiaryIBFTAccountTitle"
			 Public Const BeneficiaryIDType As String = "BeneficiaryIDType"
			 Public Const BeneficiaryIDNo As String = "BeneficiaryIDNo"
			 Public Const UBPReferenceNo As String = "UBPReferenceNo"
			 Public Const UBPCompanyID As String = "UBPCompanyID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const BeneficiaryName As String = "BeneficiaryName"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountTitle As String = "BeneficiaryAccountTitle"
			 Public Const BeneficiaryAddress As String = "BeneficiaryAddress"
			 Public Const BeneficiaryBankCode As String = "BeneficiaryBankCode"
			 Public Const BeneficiaryBankName As String = "BeneficiaryBankName"
			 Public Const BeneficiaryBankAddress As String = "BeneficiaryBankAddress"
			 Public Const BeneficiaryBranchCode As String = "BeneficiaryBranchCode"
			 Public Const BeneficiaryBranchName As String = "BeneficiaryBranchName"
			 Public Const BeneficiaryCity As String = "BeneficiaryCity"
			 Public Const BeneficiaryCNIC As String = "BeneficiaryCNIC"
			 Public Const BeneficiaryCountry As String = "BeneficiaryCountry"
			 Public Const BeneficiaryEmail As String = "BeneficiaryEmail"
			 Public Const BeneficiaryMobile As String = "BeneficiaryMobile"
			 Public Const BeneficiaryPhone As String = "BeneficiaryPhone"
			 Public Const BeneficiaryProvince As String = "BeneficiaryProvince"
			 Public Const BeneAccountBranchCode As String = "BeneAccountBranchCode"
			 Public Const BeneAccountCurrency As String = "BeneAccountCurrency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const BeneGroupCode As String = "BeneGroupCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const ApprovalByforDeletion As String = "ApprovalByforDeletion"
			 Public Const ApprovalforDeletionStatus As String = "ApprovalforDeletionStatus"
			 Public Const BeneficiaryBranchAddress As String = "BeneficiaryBranchAddress"
			 Public Const BeneficiaryAddedVia As String = "BeneficiaryAddedVia"
			 Public Const BeneficiaryIBFTAccountNo As String = "BeneficiaryIBFTAccountNo"
			 Public Const BeneficiaryStatus As String = "BeneficiaryStatus"
			 Public Const BeneficiaryIBFTAccountTitle As String = "BeneficiaryIBFTAccountTitle"
			 Public Const BeneficiaryIDType As String = "BeneficiaryIDType"
			 Public Const BeneficiaryIDNo As String = "BeneficiaryIDNo"
			 Public Const UBPReferenceNo As String = "UBPReferenceNo"
			 Public Const UBPCompanyID As String = "UBPCompanyID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBeneMetadata)
			
				If ICBeneMetadata.mapDelegates Is Nothing Then
					ICBeneMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBeneMetadata._meta Is Nothing Then
					ICBeneMetadata._meta = New ICBeneMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BeneID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneficiaryCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBranchName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCity", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCNIC", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCountry", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryEmail", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryMobile", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryPhone", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryProvince", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneGroupCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovalByforDeletion", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovalforDeletionStatus", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("BeneficiaryBranchAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAddedVia", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryIBFTAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryIBFTAccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryIDType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryIDNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UBPReferenceNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UBPCompanyID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_Bene"
				meta.Destination = "IC_Bene"
				
				meta.spInsert = "proc_IC_BeneInsert"
				meta.spUpdate = "proc_IC_BeneUpdate"
				meta.spDelete = "proc_IC_BeneDelete"
				meta.spLoadAll = "proc_IC_BeneLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BeneLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBeneMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
