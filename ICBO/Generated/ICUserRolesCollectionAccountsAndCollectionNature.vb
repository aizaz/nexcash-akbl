
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:41 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_UserRolesCollectionAccountsAndCollectionNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUserRolesCollectionAccountsAndCollectionNature))> _
	<XmlType("ICUserRolesCollectionAccountsAndCollectionNature")> _	
	Partial Public Class ICUserRolesCollectionAccountsAndCollectionNature 
		Inherits esICUserRolesCollectionAccountsAndCollectionNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUserRolesCollectionAccountsAndCollectionNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32)
			Dim obj As New ICUserRolesCollectionAccountsAndCollectionNature()
			obj.CollectionNatureCode = collectionNatureCode
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.UserID = userID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUserRolesCollectionAccountsAndCollectionNature()
			obj.CollectionNatureCode = collectionNatureCode
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.UserID = userID
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUserRolesCollectionAccountsAndCollectionNatureCollection")> _
	Partial Public Class ICUserRolesCollectionAccountsAndCollectionNatureCollection
		Inherits esICUserRolesCollectionAccountsAndCollectionNatureCollection
		Implements IEnumerable(Of ICUserRolesCollectionAccountsAndCollectionNature)
	
		Public Function FindByPrimaryKey(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32) As ICUserRolesCollectionAccountsAndCollectionNature
			Return MyBase.SingleOrDefault(Function(e) e.CollectionNatureCode = collectionNatureCode And e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.UserID.HasValue AndAlso e.UserID.Value = userID And e.RoleID.HasValue AndAlso e.RoleID.Value = roleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUserRolesCollectionAccountsAndCollectionNature))> _
		Public Class ICUserRolesCollectionAccountsAndCollectionNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUserRolesCollectionAccountsAndCollectionNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICUserRolesCollectionAccountsAndCollectionNatureCollectionWCFPacket) As ICUserRolesCollectionAccountsAndCollectionNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUserRolesCollectionAccountsAndCollectionNatureCollection) As ICUserRolesCollectionAccountsAndCollectionNatureCollectionWCFPacket
				Return New ICUserRolesCollectionAccountsAndCollectionNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUserRolesCollectionAccountsAndCollectionNatureQuery 
		Inherits esICUserRolesCollectionAccountsAndCollectionNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUserRolesCollectionAccountsAndCollectionNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUserRolesCollectionAccountsAndCollectionNatureQuery) As String
			Return ICUserRolesCollectionAccountsAndCollectionNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUserRolesCollectionAccountsAndCollectionNatureQuery
			Return DirectCast(ICUserRolesCollectionAccountsAndCollectionNatureQuery.SerializeHelper.FromXml(query, GetType(ICUserRolesCollectionAccountsAndCollectionNatureQuery)), ICUserRolesCollectionAccountsAndCollectionNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesCollectionAccountsAndCollectionNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionNatureCode, accountNumber, branchCode, currency, userID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionNatureCode, accountNumber, branchCode, currency, userID, roleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(collectionNatureCode, accountNumber, branchCode, currency, userID, roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(collectionNatureCode, accountNumber, branchCode, currency, userID, roleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim query As New ICUserRolesCollectionAccountsAndCollectionNatureQuery()
			query.Where(query.CollectionNatureCode = collectionNatureCode And query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.UserID = userID And query.RoleID = roleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal collectionNatureCode As System.String, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal userID As System.Int32, ByVal roleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CollectionNatureCode", collectionNatureCode)
						parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("UserID", userID)
						parms.Add("RoleID", roleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.CollectionNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CollectionNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CollectionNatureCode, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CollectionNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.Currency, value) Then
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICCollectionAccountsCollectionNatureByAccountNumber")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.RoleID, value) Then
					Me._UpToICRoleByRoleID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRoleID")
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesCollectionAccountsAndCollectionNature.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionAccountsCollectionNatureByAccountNumber As ICCollectionAccountsCollectionNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRoleID As ICRole
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CollectionNatureCode"
							Me.str().CollectionNatureCode = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.UserID)
							End If
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.RoleID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.ApprovedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUserRolesCollectionAccountsAndCollectionNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property CollectionNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.CollectionNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionNatureCode = Nothing
					Else
						entity.CollectionNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUserRolesCollectionAccountsAndCollectionNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesCollectionAccountsAndCollectionNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUserRolesCollectionAccountsAndCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesCollectionAccountsAndCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUserRolesCollectionAccountsAndCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUserRolesCollectionAccountsAndCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUserRolesCollectionAccountsAndCollectionNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesCollectionAccountsAndCollectionNatureCollection
		Inherits esEntityCollection(Of ICUserRolesCollectionAccountsAndCollectionNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesCollectionAccountsAndCollectionNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUserRolesCollectionAccountsAndCollectionNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUserRolesCollectionAccountsAndCollectionNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesCollectionAccountsAndCollectionNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUserRolesCollectionAccountsAndCollectionNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUserRolesCollectionAccountsAndCollectionNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUserRolesCollectionAccountsAndCollectionNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUserRolesCollectionAccountsAndCollectionNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUserRolesCollectionAccountsAndCollectionNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUserRolesCollectionAccountsAndCollectionNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUserRolesCollectionAccountsAndCollectionNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CollectionNatureCode" 
					Return Me.CollectionNatureCode
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "UserID" 
					Return Me.UserID
				Case "RoleID" 
					Return Me.RoleID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CollectionNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CollectionNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUserRolesCollectionAccountsAndCollectionNature 
		Inherits esICUserRolesCollectionAccountsAndCollectionNature
		
	
		#Region "UpToICCollectionAccountsCollectionNatureByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesCollectionAccountsAndCollectionNature_IC_CollectionAccountsCollectionNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionAccountsCollectionNatureByAccountNumber As ICCollectionAccountsCollectionNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionAccountsCollectionNatureByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not CollectionNatureCode.Equals(Nothing)  Then
						
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = New ICCollectionAccountsCollectionNature()
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber", Me._UpToICCollectionAccountsCollectionNatureByAccountNumber)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Where(Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.CollectionNatureCode = Me.CollectionNatureCode)
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber.Query.Load()
				End If

				Return Me._UpToICCollectionAccountsCollectionNatureByAccountNumber
			End Get
			
            Set(ByVal value As ICCollectionAccountsCollectionNature)
				Me.RemovePreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.CollectionNatureCode = Nothing
				
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.CollectionNatureCode = value.CollectionNatureCode
					
					Me._UpToICCollectionAccountsCollectionNatureByAccountNumber = value
					Me.SetPreSave("UpToICCollectionAccountsCollectionNatureByAccountNumber", Me._UpToICCollectionAccountsCollectionNatureByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICRoleByRoleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesCollectionAccountsAndCollectionNature_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRoleID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRoleID Is Nothing _
						 AndAlso Not RoleID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRoleID = New ICRole()
					Me._UpToICRoleByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
					Me._UpToICRoleByRoleID.Query.Where(Me._UpToICRoleByRoleID.Query.RoleID = Me.RoleID)
					Me._UpToICRoleByRoleID.Query.Load()
				End If

				Return Me._UpToICRoleByRoleID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRoleID")
				

				If value Is Nothing Then
				
					Me.RoleID = Nothing
				
					Me._UpToICRoleByRoleID = Nothing
				Else
				
					Me.RoleID = value.RoleID
					
					Me._UpToICRoleByRoleID = value
					Me.SetPreSave("UpToICRoleByRoleID", Me._UpToICRoleByRoleID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesCollectionAccountsAndCollectionNature_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICUserRolesCollectionAccountsAndCollectionNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CollectionNatureCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CollectionNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.AccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.BranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.UserID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.UserID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.RoleID, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.RoleID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.CreatedDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.IsApproved, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesCollectionAccountsAndCollectionNatureMetadata.ColumnNames.ApprovedOn, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesCollectionAccountsAndCollectionNatureMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUserRolesCollectionAccountsAndCollectionNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const UserID As String = "UserID"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CollectionNatureCode As String = "CollectionNatureCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const UserID As String = "UserID"
			 Public Const RoleID As String = "RoleID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUserRolesCollectionAccountsAndCollectionNatureMetadata)
			
				If ICUserRolesCollectionAccountsAndCollectionNatureMetadata.mapDelegates Is Nothing Then
					ICUserRolesCollectionAccountsAndCollectionNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUserRolesCollectionAccountsAndCollectionNatureMetadata._meta Is Nothing Then
					ICUserRolesCollectionAccountsAndCollectionNatureMetadata._meta = New ICUserRolesCollectionAccountsAndCollectionNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CollectionNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_UserRolesCollectionAccountsAndCollectionNature"
				meta.Destination = "IC_UserRolesCollectionAccountsAndCollectionNature"
				
				meta.spInsert = "proc_IC_UserRolesCollectionAccountsAndCollectionNatureInsert"
				meta.spUpdate = "proc_IC_UserRolesCollectionAccountsAndCollectionNatureUpdate"
				meta.spDelete = "proc_IC_UserRolesCollectionAccountsAndCollectionNatureDelete"
				meta.spLoadAll = "proc_IC_UserRolesCollectionAccountsAndCollectionNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UserRolesCollectionAccountsAndCollectionNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUserRolesCollectionAccountsAndCollectionNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
