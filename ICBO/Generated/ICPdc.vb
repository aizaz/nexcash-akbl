
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 4/4/2014 10:44:57 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Pdc' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPdc))> _
	<XmlType("ICPdc")> _	
	Partial Public Class ICPdc 
		Inherits esICPdc
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPdc()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal chequeId As System.Int32)
			Dim obj As New ICPdc()
			obj.ChequeId = chequeId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal chequeId As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPdc()
			obj.ChequeId = chequeId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPdcCollection")> _
	Partial Public Class ICPdcCollection
		Inherits esICPdcCollection
		Implements IEnumerable(Of ICPdc)
	
		Public Function FindByPrimaryKey(ByVal chequeId As System.Int32) As ICPdc
			Return MyBase.SingleOrDefault(Function(e) e.ChequeId.HasValue AndAlso e.ChequeId.Value = chequeId)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPdc))> _
		Public Class ICPdcCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPdcCollection)
			
			Public Shared Widening Operator CType(packet As ICPdcCollectionWCFPacket) As ICPdcCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPdcCollection) As ICPdcCollectionWCFPacket
				Return New ICPdcCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPdcQuery 
		Inherits esICPdcQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPdcQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPdcQuery) As String
			Return ICPdcQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPdcQuery
			Return DirectCast(ICPdcQuery.SerializeHelper.FromXml(query, GetType(ICPdcQuery)), ICPdcQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPdc
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal chequeId As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(chequeId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(chequeId)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal chequeId As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(chequeId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(chequeId)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal chequeId As System.Int32) As Boolean
		
			Dim query As New ICPdcQuery()
			query.Where(query.ChequeId = chequeId)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal chequeId As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ChequeId", chequeId)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Pdc.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICPdcMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICPdcMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICPdcMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.ChequeId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChequeId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPdcMetadata.ColumnNames.ChequeId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPdcMetadata.ColumnNames.ChequeId, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.ChequeId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.ChequeNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChequeNo As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.ChequeNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.ChequeNo, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.ChequeNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPdcMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPdcMetadata.ColumnNames.BankCode, value) Then
					Me._UpToICBankByBankCode = Nothing
					Me.OnPropertyChanged("UpToICBankByBankCode")
					OnPropertyChanged(ICPdcMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.Presentment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Presentment As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPdcMetadata.ColumnNames.Presentment)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPdcMetadata.ColumnNames.Presentment, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.Presentment)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.PostingDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PostingDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPdcMetadata.ColumnNames.PostingDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPdcMetadata.ColumnNames.PostingDate, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.PostingDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.CityCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CityCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPdcMetadata.ColumnNames.CityCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPdcMetadata.ColumnNames.CityCode, value) Then
					Me._UpToICCityByCityCode = Nothing
					Me.OnPropertyChanged("UpToICCityByCityCode")
					OnPropertyChanged(ICPdcMetadata.PropertyNames.CityCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.TransactionType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionType As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.TransactionType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.TransactionType, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.TransactionType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.ClearingType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingType As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.ClearingType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.ClearingType, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.ClearingType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.TransactionDetails
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionDetails As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.TransactionDetails)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.TransactionDetails, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.TransactionDetails)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.AddtionalDetails
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AddtionalDetails As System.String
			Get
				Return MyBase.GetSystemString(ICPdcMetadata.ColumnNames.AddtionalDetails)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPdcMetadata.ColumnNames.AddtionalDetails, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.AddtionalDetails)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPdcMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPdcMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPdcMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPdcMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPdcMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPdcMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPdcMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPdcMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Pdc.NotificationSendDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NotificationSendDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPdcMetadata.ColumnNames.NotificationSendDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPdcMetadata.ColumnNames.NotificationSendDate, value) Then
					OnPropertyChanged(ICPdcMetadata.PropertyNames.NotificationSendDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBankByBankCode As ICBank
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCityByCityCode As ICCity
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "ChequeId"
							Me.str().ChequeId = CType(value, string)
												
						Case "ChequeNo"
							Me.str().ChequeNo = CType(value, string)
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "Presentment"
							Me.str().Presentment = CType(value, string)
												
						Case "PostingDate"
							Me.str().PostingDate = CType(value, string)
												
						Case "CityCode"
							Me.str().CityCode = CType(value, string)
												
						Case "TransactionType"
							Me.str().TransactionType = CType(value, string)
												
						Case "ClearingType"
							Me.str().ClearingType = CType(value, string)
												
						Case "TransactionDetails"
							Me.str().TransactionDetails = CType(value, string)
												
						Case "AddtionalDetails"
							Me.str().AddtionalDetails = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "NotificationSendDate"
							Me.str().NotificationSendDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ChequeId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ChequeId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.ChequeId)
							End If
						
						Case "BankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.BankCode)
							End If
						
						Case "Presentment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.Presentment = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.Presentment)
							End If
						
						Case "PostingDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PostingDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.PostingDate)
							End If
						
						Case "CityCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CityCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.CityCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.CreationDate)
							End If
						
						Case "NotificationSendDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.NotificationSendDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPdcMetadata.PropertyNames.NotificationSendDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPdc)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChequeId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ChequeId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChequeId = Nothing
					Else
						entity.ChequeId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChequeNo As System.String 
				Get
					Dim data_ As System.String = entity.ChequeNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChequeNo = Nothing
					Else
						entity.ChequeNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Presentment As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.Presentment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Presentment = Nothing
					Else
						entity.Presentment = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PostingDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PostingDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PostingDate = Nothing
					Else
						entity.PostingDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CityCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CityCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CityCode = Nothing
					Else
						entity.CityCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionType As System.String 
				Get
					Dim data_ As System.String = entity.TransactionType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionType = Nothing
					Else
						entity.TransactionType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingType = Nothing
					Else
						entity.ClearingType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionDetails As System.String 
				Get
					Dim data_ As System.String = entity.TransactionDetails
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionDetails = Nothing
					Else
						entity.TransactionDetails = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AddtionalDetails As System.String 
				Get
					Dim data_ As System.String = entity.AddtionalDetails
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AddtionalDetails = Nothing
					Else
						entity.AddtionalDetails = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property NotificationSendDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.NotificationSendDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NotificationSendDate = Nothing
					Else
						entity.NotificationSendDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPdc
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPdcMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPdcQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPdcQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPdcQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPdcQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPdcQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPdcCollection
		Inherits esEntityCollection(Of ICPdc)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPdcMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPdcCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPdcQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPdcQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPdcQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPdcQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPdcQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPdcQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPdcQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPdcQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPdcMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "ChequeId" 
					Return Me.ChequeId
				Case "ChequeNo" 
					Return Me.ChequeNo
				Case "BankCode" 
					Return Me.BankCode
				Case "Presentment" 
					Return Me.Presentment
				Case "PostingDate" 
					Return Me.PostingDate
				Case "CityCode" 
					Return Me.CityCode
				Case "TransactionType" 
					Return Me.TransactionType
				Case "ClearingType" 
					Return Me.ClearingType
				Case "TransactionDetails" 
					Return Me.TransactionDetails
				Case "AddtionalDetails" 
					Return Me.AddtionalDetails
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "NotificationSendDate" 
					Return Me.NotificationSendDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ChequeId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.ChequeId, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChequeNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.ChequeNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.BankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Presentment As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.Presentment, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PostingDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.PostingDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CityCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.CityCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.TransactionType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.ClearingType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionDetails As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.TransactionDetails, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AddtionalDetails As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.AddtionalDetails, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property NotificationSendDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPdcMetadata.ColumnNames.NotificationSendDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPdc 
		Inherits esICPdc
		
	
		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Pdc_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICBankByBankCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Pdc_IC_Bank
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBankByBankCode As ICBank
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBankByBankCode Is Nothing _
						 AndAlso Not BankCode.Equals(Nothing)  Then
						
					Me._UpToICBankByBankCode = New ICBank()
					Me._UpToICBankByBankCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
					Me._UpToICBankByBankCode.Query.Where(Me._UpToICBankByBankCode.Query.BankCode = Me.BankCode)
					Me._UpToICBankByBankCode.Query.Load()
				End If

				Return Me._UpToICBankByBankCode
			End Get
			
            Set(ByVal value As ICBank)
				Me.RemovePreSave("UpToICBankByBankCode")
				

				If value Is Nothing Then
				
					Me.BankCode = Nothing
				
					Me._UpToICBankByBankCode = Nothing
				Else
				
					Me.BankCode = value.BankCode
					
					Me._UpToICBankByBankCode = value
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCityByCityCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Pdc_IC_City
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCityByCityCode As ICCity
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCityByCityCode Is Nothing _
						 AndAlso Not CityCode.Equals(Nothing)  Then
						
					Me._UpToICCityByCityCode = New ICCity()
					Me._UpToICCityByCityCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCityByCityCode", Me._UpToICCityByCityCode)
					Me._UpToICCityByCityCode.Query.Where(Me._UpToICCityByCityCode.Query.CityCode = Me.CityCode)
					Me._UpToICCityByCityCode.Query.Load()
				End If

				Return Me._UpToICCityByCityCode
			End Get
			
            Set(ByVal value As ICCity)
				Me.RemovePreSave("UpToICCityByCityCode")
				

				If value Is Nothing Then
				
					Me.CityCode = Nothing
				
					Me._UpToICCityByCityCode = Nothing
				Else
				
					Me.CityCode = value.CityCode
					
					Me._UpToICCityByCityCode = value
					Me.SetPreSave("UpToICCityByCityCode", Me._UpToICCityByCityCode)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICBankByBankCode Is Nothing Then
				Me.BankCode = Me._UpToICBankByBankCode.BankCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCityByCityCode Is Nothing Then
				Me.CityCode = Me._UpToICCityByCityCode.CityCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPdcMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.ChequeId, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPdcMetadata.PropertyNames.ChequeId
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.ChequeNo, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.ChequeNo
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.BankCode, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPdcMetadata.PropertyNames.BankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.Presentment, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPdcMetadata.PropertyNames.Presentment
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.PostingDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPdcMetadata.PropertyNames.PostingDate
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.CityCode, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPdcMetadata.PropertyNames.CityCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.TransactionType, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.TransactionType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.ClearingType, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.ClearingType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.TransactionDetails, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.TransactionDetails
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.AddtionalDetails, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPdcMetadata.PropertyNames.AddtionalDetails
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.CreateBy, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPdcMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.CreateDate, 14, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPdcMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.Creater, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPdcMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.CreationDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPdcMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPdcMetadata.ColumnNames.NotificationSendDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPdcMetadata.PropertyNames.NotificationSendDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPdcMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const ChequeId As String = "ChequeId"
			 Public Const ChequeNo As String = "ChequeNo"
			 Public Const BankCode As String = "BankCode"
			 Public Const Presentment As String = "Presentment"
			 Public Const PostingDate As String = "PostingDate"
			 Public Const CityCode As String = "CityCode"
			 Public Const TransactionType As String = "TransactionType"
			 Public Const ClearingType As String = "ClearingType"
			 Public Const TransactionDetails As String = "TransactionDetails"
			 Public Const AddtionalDetails As String = "AddtionalDetails"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const NotificationSendDate As String = "NotificationSendDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const ChequeId As String = "ChequeId"
			 Public Const ChequeNo As String = "ChequeNo"
			 Public Const BankCode As String = "BankCode"
			 Public Const Presentment As String = "Presentment"
			 Public Const PostingDate As String = "PostingDate"
			 Public Const CityCode As String = "CityCode"
			 Public Const TransactionType As String = "TransactionType"
			 Public Const ClearingType As String = "ClearingType"
			 Public Const TransactionDetails As String = "TransactionDetails"
			 Public Const AddtionalDetails As String = "AddtionalDetails"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const NotificationSendDate As String = "NotificationSendDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPdcMetadata)
			
				If ICPdcMetadata.mapDelegates Is Nothing Then
					ICPdcMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPdcMetadata._meta Is Nothing Then
					ICPdcMetadata._meta = New ICPdcMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ChequeId", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChequeNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Presentment", new esTypeMap("date", "System.DateTime"))
				meta.AddTypeMap("PostingDate", new esTypeMap("date", "System.DateTime"))
				meta.AddTypeMap("CityCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TransactionType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TransactionDetails", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AddtionalDetails", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("NotificationSendDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Pdc"
				meta.Destination = "IC_Pdc"
				
				meta.spInsert = "proc_IC_PdcInsert"
				meta.spUpdate = "proc_IC_PdcUpdate"
				meta.spDelete = "proc_IC_PdcDelete"
				meta.spLoadAll = "proc_IC_PdcLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PdcLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPdcMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
