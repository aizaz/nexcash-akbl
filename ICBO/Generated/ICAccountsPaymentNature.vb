
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:53 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountsPaymentNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountsPaymentNature))> _
	<XmlType("ICAccountsPaymentNature")> _	
	Partial Public Class ICAccountsPaymentNature 
		Inherits esICAccountsPaymentNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountsPaymentNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String)
			Dim obj As New ICAccountsPaymentNature()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountsPaymentNature()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountsPaymentNatureCollection")> _
	Partial Public Class ICAccountsPaymentNatureCollection
		Inherits esICAccountsPaymentNatureCollection
		Implements IEnumerable(Of ICAccountsPaymentNature)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As ICAccountsPaymentNature
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency And e.PaymentNatureCode = paymentNatureCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountsPaymentNature))> _
		Public Class ICAccountsPaymentNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountsPaymentNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountsPaymentNatureCollectionWCFPacket) As ICAccountsPaymentNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountsPaymentNatureCollection) As ICAccountsPaymentNatureCollectionWCFPacket
				Return New ICAccountsPaymentNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountsPaymentNatureQuery 
		Inherits esICAccountsPaymentNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountsPaymentNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountsPaymentNatureQuery) As String
			Return ICAccountsPaymentNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountsPaymentNatureQuery
			Return DirectCast(ICAccountsPaymentNatureQuery.SerializeHelper.FromXml(query, GetType(ICAccountsPaymentNatureQuery)), ICAccountsPaymentNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, paymentNatureCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency, paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency, paymentNatureCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			Dim query As New ICAccountsPaymentNatureQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency And query.PaymentNatureCode = paymentNatureCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal paymentNatureCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
						parms.Add("PaymentNatureCode", paymentNatureCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsPaymentNatureMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsPaymentNatureMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsPaymentNatureMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsPaymentNatureMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsPaymentNatureMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsPaymentNatureMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountsPaymentNature.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsPaymentNatureMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsPaymentNatureMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountsPaymentNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountsPaymentNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountsPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountsPaymentNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountsPaymentNatureCollection
		Inherits esEntityCollection(Of ICAccountsPaymentNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountsPaymentNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountsPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountsPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountsPaymentNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountsPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountsPaymentNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountsPaymentNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountsPaymentNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountsPaymentNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsPaymentNatureMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountsPaymentNature 
		Inherits esICAccountsPaymentNature
		
	
		#Region "ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNature.ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICAccountPayNatureFileUploadTemplateQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPayNatureFileUploadTemplateQuery), New ICAccountPayNatureFileUploadTemplateQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPayNatureFileUploadTemplate_IC_AccountsPaymentNature1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber As ICAccountPayNatureFileUploadTemplateCollection 
		
			Get
				If Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber Is Nothing Then
					Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber = New ICAccountPayNatureFileUploadTemplateCollection()
					Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber", Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.fks.Add(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.fks.Add(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.fks.Add(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber.fks.Add(ICAccountPayNatureFileUploadTemplateMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountPayNatureFileUploadTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber")
					Me._ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber As ICAccountPayNatureFileUploadTemplateCollection
		#End Region

		#Region "ICAccountsPaymentNatureProductTypeCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureProductTypeCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNature.ICAccountsPaymentNatureProductTypeCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureProductTypeCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureProductTypeCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureProductTypeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureProductTypeQuery), New ICAccountsPaymentNatureProductTypeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNatureProductType_IC_AccountsPaymentNature1
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureProductTypeCollectionByAccountNumber As ICAccountsPaymentNatureProductTypeCollection 
		
			Get
				If Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber Is Nothing Then
					Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber = New ICAccountsPaymentNatureProductTypeCollection()
					Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureProductTypeCollectionByAccountNumber", Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureProductTypeMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureProductTypeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureProductTypeCollectionByAccountNumber")
					Me._ICAccountsPaymentNatureProductTypeCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureProductTypeCollectionByAccountNumber As ICAccountsPaymentNatureProductTypeCollection
		#End Region

		#Region "ICEmailSettingsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICEmailSettingsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNature.ICEmailSettingsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICEmailSettingsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICEmailSettingsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICEmailSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICEmailSettingsQuery), New ICEmailSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_EmailSettings_IC_AccountsPaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICEmailSettingsCollectionByAccountNumber As ICEmailSettingsCollection 
		
			Get
				If Me._ICEmailSettingsCollectionByAccountNumber Is Nothing Then
					Me._ICEmailSettingsCollectionByAccountNumber = New ICEmailSettingsCollection()
					Me._ICEmailSettingsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICEmailSettingsCollectionByAccountNumber", Me._ICEmailSettingsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICEmailSettingsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICEmailSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICEmailSettingsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICEmailSettingsCollectionByAccountNumber")
					Me._ICEmailSettingsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICEmailSettingsCollectionByAccountNumber As ICEmailSettingsCollection
		#End Region

		#Region "ICFTPSettingsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFTPSettingsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNature.ICFTPSettingsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICFTPSettingsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICFTPSettingsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICFTPSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFTPSettingsQuery), New ICFTPSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FTPSettings_IC_AccountsPaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFTPSettingsCollectionByAccountNumber As ICFTPSettingsCollection 
		
			Get
				If Me._ICFTPSettingsCollectionByAccountNumber Is Nothing Then
					Me._ICFTPSettingsCollectionByAccountNumber = New ICFTPSettingsCollection()
					Me._ICFTPSettingsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFTPSettingsCollectionByAccountNumber", Me._ICFTPSettingsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICFTPSettingsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICFTPSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFTPSettingsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICFTPSettingsCollectionByAccountNumber")
					Me._ICFTPSettingsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFTPSettingsCollectionByAccountNumber As ICFTPSettingsCollection
		#End Region

		#Region "ICOnlineFormSettingsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOnlineFormSettingsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccountsPaymentNature.ICOnlineFormSettingsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICOnlineFormSettingsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency,PaymentNatureCode"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICOnlineFormSettingsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICOnlineFormSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOnlineFormSettingsQuery), New ICOnlineFormSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency And parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_OnlineFormSettings_IC_AccountsPaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOnlineFormSettingsCollectionByAccountNumber As ICOnlineFormSettingsCollection 
		
			Get
				If Me._ICOnlineFormSettingsCollectionByAccountNumber Is Nothing Then
					Me._ICOnlineFormSettingsCollectionByAccountNumber = New ICOnlineFormSettingsCollection()
					Me._ICOnlineFormSettingsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOnlineFormSettingsCollectionByAccountNumber", Me._ICOnlineFormSettingsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Where(Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Where(Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Where(Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Where(Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICOnlineFormSettingsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOnlineFormSettingsCollectionByAccountNumber.fks.Add(ICOnlineFormSettingsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICOnlineFormSettingsCollectionByAccountNumber.fks.Add(ICOnlineFormSettingsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICOnlineFormSettingsCollectionByAccountNumber.fks.Add(ICOnlineFormSettingsMetadata.ColumnNames.Currency, Me.Currency)
						Me._ICOnlineFormSettingsCollectionByAccountNumber.fks.Add(ICOnlineFormSettingsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICOnlineFormSettingsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICOnlineFormSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOnlineFormSettingsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICOnlineFormSettingsCollectionByAccountNumber")
					Me._ICOnlineFormSettingsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOnlineFormSettingsCollectionByAccountNumber As ICOnlineFormSettingsCollection
		#End Region

		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNature_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AccountsPaymentNature_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber"
					coll = Me.ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber
					Exit Select
				Case "ICAccountsPaymentNatureProductTypeCollectionByAccountNumber"
					coll = Me.ICAccountsPaymentNatureProductTypeCollectionByAccountNumber
					Exit Select
				Case "ICEmailSettingsCollectionByAccountNumber"
					coll = Me.ICEmailSettingsCollectionByAccountNumber
					Exit Select
				Case "ICFTPSettingsCollectionByAccountNumber"
					coll = Me.ICFTPSettingsCollectionByAccountNumber
					Exit Select
				Case "ICOnlineFormSettingsCollectionByAccountNumber"
					coll = Me.ICOnlineFormSettingsCollectionByAccountNumber
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPayNatureFileUploadTemplateCollectionByAccountNumber", GetType(ICAccountPayNatureFileUploadTemplateCollection), New ICAccountPayNatureFileUploadTemplate()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureProductTypeCollectionByAccountNumber", GetType(ICAccountsPaymentNatureProductTypeCollection), New ICAccountsPaymentNatureProductType()))
			props.Add(new esPropertyDescriptor(Me, "ICEmailSettingsCollectionByAccountNumber", GetType(ICEmailSettingsCollection), New ICEmailSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICFTPSettingsCollectionByAccountNumber", GetType(ICFTPSettingsCollection), New ICFTPSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICOnlineFormSettingsCollectionByAccountNumber", GetType(ICOnlineFormSettingsCollection), New ICOnlineFormSettings()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountsPaymentNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.PaymentNatureCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.CreateBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.IsApproved, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.IsActive, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.Creater, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsPaymentNatureMetadata.ColumnNames.CreationDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsPaymentNatureMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountsPaymentNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "isApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountsPaymentNatureMetadata)
			
				If ICAccountsPaymentNatureMetadata.mapDelegates Is Nothing Then
					ICAccountsPaymentNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountsPaymentNatureMetadata._meta Is Nothing Then
					ICAccountsPaymentNatureMetadata._meta = New ICAccountsPaymentNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_AccountsPaymentNature"
				meta.Destination = "IC_AccountsPaymentNature"
				
				meta.spInsert = "proc_IC_AccountsPaymentNatureInsert"
				meta.spUpdate = "proc_IC_AccountsPaymentNatureUpdate"
				meta.spDelete = "proc_IC_AccountsPaymentNatureDelete"
				meta.spLoadAll = "proc_IC_AccountsPaymentNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountsPaymentNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountsPaymentNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
