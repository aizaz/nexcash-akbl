
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Bank' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBank))> _
	<XmlType("ICBank")> _	
	Partial Public Class ICBank 
		Inherits esICBank
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBank()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal bankCode As System.Int32)
			Dim obj As New ICBank()
			obj.BankCode = bankCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal bankCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBank()
			obj.BankCode = bankCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBankCollection")> _
	Partial Public Class ICBankCollection
		Inherits esICBankCollection
		Implements IEnumerable(Of ICBank)
	
		Public Function FindByPrimaryKey(ByVal bankCode As System.Int32) As ICBank
			Return MyBase.SingleOrDefault(Function(e) e.BankCode.HasValue AndAlso e.BankCode.Value = bankCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBank))> _
		Public Class ICBankCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBankCollection)
			
			Public Shared Widening Operator CType(packet As ICBankCollectionWCFPacket) As ICBankCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBankCollection) As ICBankCollectionWCFPacket
				Return New ICBankCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBankQuery 
		Inherits esICBankQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBankQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBankQuery) As String
			Return ICBankQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBankQuery
			Return DirectCast(ICBankQuery.SerializeHelper.FromXml(query, GetType(ICBankQuery)), ICBankQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBank
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal bankCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(bankCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(bankCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal bankCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(bankCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(bankCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal bankCode As System.Int32) As Boolean
		
			Dim query As New ICBankQuery()
			query.Where(query.BankCode = bankCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal bankCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BankCode", bankCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Bank.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankMetadata.ColumnNames.BankCode, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.BankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankName As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.BankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.BankName, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.BankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsRTGS
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRTGS As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsRTGS)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsRTGS, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsRTGS)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsIBFT, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsPrincipal
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrincipal As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsPrincipal)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsPrincipal, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsPrincipal)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.WebSite
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property WebSite As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.WebSite)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.WebSite, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.WebSite)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.Approvedby
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Approvedby As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankMetadata.ColumnNames.Approvedby)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankMetadata.ColumnNames.Approvedby, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.Approvedby)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.phone
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Phone As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.Phone)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.Phone, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.Phone)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.BankIMD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankIMD As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.BankIMD)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.BankIMD, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.BankIMD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.Decription
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Decription As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.Decription)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.Decription, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.Decription)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.BankID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankID As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.BankID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.BankID, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.BankID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsDDEnabled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsDDEnabled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsDDEnabled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsDDEnabled, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsDDEnabled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.IsDDPreferred
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsDDPreferred As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBankMetadata.ColumnNames.IsDDPreferred)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBankMetadata.ColumnNames.IsDDPreferred, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.IsDDPreferred)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.DDPreferredAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDPreferredAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountNumber, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.DDPreferredAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.DDPreferredAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDPreferredAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountBranchCode, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.DDPreferredAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.DDPreferredAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDPreferredAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBankMetadata.ColumnNames.DDPreferredAccountCurrency, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.DDPreferredAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBankMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBankMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Bank.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBankMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBankMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICBankMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "BankName"
							Me.str().BankName = CType(value, string)
												
						Case "IsRTGS"
							Me.str().IsRTGS = CType(value, string)
												
						Case "IsIBFT"
							Me.str().IsIBFT = CType(value, string)
												
						Case "IsPrincipal"
							Me.str().IsPrincipal = CType(value, string)
												
						Case "WebSite"
							Me.str().WebSite = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Approvedby"
							Me.str().Approvedby = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "Phone"
							Me.str().Phone = CType(value, string)
												
						Case "BankIMD"
							Me.str().BankIMD = CType(value, string)
												
						Case "Decription"
							Me.str().Decription = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "BankID"
							Me.str().BankID = CType(value, string)
												
						Case "IsDDEnabled"
							Me.str().IsDDEnabled = CType(value, string)
												
						Case "IsDDPreferred"
							Me.str().IsDDPreferred = CType(value, string)
												
						Case "DDPreferredAccountNumber"
							Me.str().DDPreferredAccountNumber = CType(value, string)
												
						Case "DDPreferredAccountBranchCode"
							Me.str().DDPreferredAccountBranchCode = CType(value, string)
												
						Case "DDPreferredAccountCurrency"
							Me.str().DDPreferredAccountCurrency = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankMetadata.PropertyNames.BankCode)
							End If
						
						Case "IsRTGS"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRTGS = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsRTGS)
							End If
						
						Case "IsIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsIBFT)
							End If
						
						Case "IsPrincipal"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrincipal = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsPrincipal)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsApproved)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Approvedby"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Approvedby = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankMetadata.PropertyNames.Approvedby)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsDDEnabled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsDDEnabled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsDDEnabled)
							End If
						
						Case "IsDDPreferred"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsDDPreferred = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBankMetadata.PropertyNames.IsDDPreferred)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBankMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBankMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBank)
				Me.entity = entity
			End Sub				
		
	
			Public Property BankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankName As System.String 
				Get
					Dim data_ As System.String = entity.BankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankName = Nothing
					Else
						entity.BankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRTGS As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRTGS
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRTGS = Nothing
					Else
						entity.IsRTGS = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsIBFT = Nothing
					Else
						entity.IsIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrincipal As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrincipal
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrincipal = Nothing
					Else
						entity.IsPrincipal = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property WebSite As System.String 
				Get
					Dim data_ As System.String = entity.WebSite
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.WebSite = Nothing
					Else
						entity.WebSite = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Approvedby As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Approvedby
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Approvedby = Nothing
					Else
						entity.Approvedby = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Phone As System.String 
				Get
					Dim data_ As System.String = entity.Phone
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Phone = Nothing
					Else
						entity.Phone = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankIMD As System.String 
				Get
					Dim data_ As System.String = entity.BankIMD
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankIMD = Nothing
					Else
						entity.BankIMD = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Decription As System.String 
				Get
					Dim data_ As System.String = entity.Decription
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Decription = Nothing
					Else
						entity.Decription = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankID As System.String 
				Get
					Dim data_ As System.String = entity.BankID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankID = Nothing
					Else
						entity.BankID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsDDEnabled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsDDEnabled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsDDEnabled = Nothing
					Else
						entity.IsDDEnabled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsDDPreferred As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsDDPreferred
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsDDPreferred = Nothing
					Else
						entity.IsDDPreferred = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDPreferredAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.DDPreferredAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDPreferredAccountNumber = Nothing
					Else
						entity.DDPreferredAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDPreferredAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.DDPreferredAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDPreferredAccountBranchCode = Nothing
					Else
						entity.DDPreferredAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDPreferredAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.DDPreferredAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDPreferredAccountCurrency = Nothing
					Else
						entity.DDPreferredAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBank
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBankMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBankQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBankQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBankQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBankQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBankQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBankCollection
		Inherits esEntityCollection(Of ICBank)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBankMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBankCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBankQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBankQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBankQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBankQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBankQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBankQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBankQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBankQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBankMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BankCode" 
					Return Me.BankCode
				Case "BankName" 
					Return Me.BankName
				Case "IsRTGS" 
					Return Me.IsRTGS
				Case "IsIBFT" 
					Return Me.IsIBFT
				Case "IsPrincipal" 
					Return Me.IsPrincipal
				Case "WebSite" 
					Return Me.WebSite
				Case "IsApproved" 
					Return Me.IsApproved
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Approvedby" 
					Return Me.Approvedby
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "Phone" 
					Return Me.Phone
				Case "BankIMD" 
					Return Me.BankIMD
				Case "Decription" 
					Return Me.Decription
				Case "IsActive" 
					Return Me.IsActive
				Case "BankID" 
					Return Me.BankID
				Case "IsDDEnabled" 
					Return Me.IsDDEnabled
				Case "IsDDPreferred" 
					Return Me.IsDDPreferred
				Case "DDPreferredAccountNumber" 
					Return Me.DDPreferredAccountNumber
				Case "DDPreferredAccountBranchCode" 
					Return Me.DDPreferredAccountBranchCode
				Case "DDPreferredAccountCurrency" 
					Return Me.DDPreferredAccountCurrency
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.BankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.BankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsRTGS As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsRTGS, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrincipal As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsPrincipal, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property WebSite As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.WebSite, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Approvedby As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.Approvedby, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Phone As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.Phone, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankIMD As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.BankIMD, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Decription As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.Decription, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property BankID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.BankID, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsDDEnabled As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsDDEnabled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsDDPreferred As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.IsDDPreferred, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property DDPreferredAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.DDPreferredAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DDPreferredAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.DDPreferredAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DDPreferredAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.DDPreferredAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBankMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBank 
		Inherits esICBank
		
	
		#Region "ICOfficeCollectionByBankCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOfficeCollectionByBankCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICBank.ICOfficeCollectionByBankCode_Delegate)
				map.PropertyName = "ICOfficeCollectionByBankCode"
				map.MyColumnName = "BankCode"
				map.ParentColumnName = "BankCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICOfficeCollectionByBankCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICBankQuery(data.NextAlias())
			
			Dim mee As ICOfficeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOfficeQuery), New ICOfficeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.BankCode = mee.BankCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Office_IC_Bank
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOfficeCollectionByBankCode As ICOfficeCollection 
		
			Get
				If Me._ICOfficeCollectionByBankCode Is Nothing Then
					Me._ICOfficeCollectionByBankCode = New ICOfficeCollection()
					Me._ICOfficeCollectionByBankCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOfficeCollectionByBankCode", Me._ICOfficeCollectionByBankCode)
				
					If Not Me.BankCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOfficeCollectionByBankCode.Query.Where(Me._ICOfficeCollectionByBankCode.Query.BankCode = Me.BankCode)
							Me._ICOfficeCollectionByBankCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOfficeCollectionByBankCode.fks.Add(ICOfficeMetadata.ColumnNames.BankCode, Me.BankCode)
					End If
				End If

				Return Me._ICOfficeCollectionByBankCode
			End Get
			
			Set(ByVal value As ICOfficeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOfficeCollectionByBankCode Is Nothing Then

					Me.RemovePostSave("ICOfficeCollectionByBankCode")
					Me._ICOfficeCollectionByBankCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOfficeCollectionByBankCode As ICOfficeCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICOfficeCollectionByBankCode"
					coll = Me.ICOfficeCollectionByBankCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICOfficeCollectionByBankCode", GetType(ICOfficeCollection), New ICOffice()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICOfficeCollectionByBankCode Is Nothing Then
				Apply(Me._ICOfficeCollectionByBankCode, "BankCode", Me.BankCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICBankMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBankMetadata.ColumnNames.BankCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankMetadata.PropertyNames.BankCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.BankName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.BankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsRTGS, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsRTGS
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsIBFT, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsPrincipal, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsPrincipal
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.WebSite, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.WebSite
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsApproved, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.CreateDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.Approvedby, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankMetadata.PropertyNames.Approvedby
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.ApprovedOn, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.CreatedBy, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.Phone, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.Phone
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.BankIMD, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.BankIMD
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.Decription, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.Decription
			c.CharacterMaxLength = 400
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsActive, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.BankID, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.BankID
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsDDEnabled, 16, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsDDEnabled
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.IsDDPreferred, 17, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBankMetadata.PropertyNames.IsDDPreferred
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.DDPreferredAccountNumber, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.DDPreferredAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.DDPreferredAccountBranchCode, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.DDPreferredAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.DDPreferredAccountCurrency, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBankMetadata.PropertyNames.DDPreferredAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.Creater, 21, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBankMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBankMetadata.ColumnNames.CreationDate, 22, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBankMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBankMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BankCode As String = "BankCode"
			 Public Const BankName As String = "BankName"
			 Public Const IsRTGS As String = "IsRTGS"
			 Public Const IsIBFT As String = "IsIBFT"
			 Public Const IsPrincipal As String = "IsPrincipal"
			 Public Const WebSite As String = "WebSite"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const Phone As String = "phone"
			 Public Const BankIMD As String = "BankIMD"
			 Public Const Decription As String = "Decription"
			 Public Const IsActive As String = "isActive"
			 Public Const BankID As String = "BankID"
			 Public Const IsDDEnabled As String = "IsDDEnabled"
			 Public Const IsDDPreferred As String = "IsDDPreferred"
			 Public Const DDPreferredAccountNumber As String = "DDPreferredAccountNumber"
			 Public Const DDPreferredAccountBranchCode As String = "DDPreferredAccountBranchCode"
			 Public Const DDPreferredAccountCurrency As String = "DDPreferredAccountCurrency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BankCode As String = "BankCode"
			 Public Const BankName As String = "BankName"
			 Public Const IsRTGS As String = "IsRTGS"
			 Public Const IsIBFT As String = "IsIBFT"
			 Public Const IsPrincipal As String = "IsPrincipal"
			 Public Const WebSite As String = "WebSite"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const Phone As String = "Phone"
			 Public Const BankIMD As String = "BankIMD"
			 Public Const Decription As String = "Decription"
			 Public Const IsActive As String = "IsActive"
			 Public Const BankID As String = "BankID"
			 Public Const IsDDEnabled As String = "IsDDEnabled"
			 Public Const IsDDPreferred As String = "IsDDPreferred"
			 Public Const DDPreferredAccountNumber As String = "DDPreferredAccountNumber"
			 Public Const DDPreferredAccountBranchCode As String = "DDPreferredAccountBranchCode"
			 Public Const DDPreferredAccountCurrency As String = "DDPreferredAccountCurrency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBankMetadata)
			
				If ICBankMetadata.mapDelegates Is Nothing Then
					ICBankMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBankMetadata._meta Is Nothing Then
					ICBankMetadata._meta = New ICBankMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsRTGS", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsPrincipal", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("WebSite", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Approvedby", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Phone", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankIMD", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Decription", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("BankID", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsDDEnabled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsDDPreferred", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("DDPreferredAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DDPreferredAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DDPreferredAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Bank"
				meta.Destination = "IC_Bank"
				
				meta.spInsert = "proc_IC_BankInsert"
				meta.spUpdate = "proc_IC_BankUpdate"
				meta.spDelete = "proc_IC_BankDelete"
				meta.spLoadAll = "proc_IC_BankLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BankLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBankMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
