
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 4/7/2015 4:45:32 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Accounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccounts))> _
	<XmlType("ICAccounts")> _	
	Partial Public Class ICAccounts 
		Inherits esICAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String)
			Dim obj As New ICAccounts()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccounts()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
		Protected Overrides Function GetConnectionName() As String
			Return "ConnectionToICAccount"
		End Function
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountsCollection")> _
	Partial Public Class ICAccountsCollection
		Inherits esICAccountsCollection
		Implements IEnumerable(Of ICAccounts)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As ICAccounts
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccounts))> _
		Public Class ICAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountsCollectionWCFPacket) As ICAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountsCollection) As ICAccountsCollectionWCFPacket
				Return New ICAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
		
		Protected Overrides Function GetConnectionName() As String
			Return "ConnectionToICAccount"
		End Function
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountsQuery 
		Inherits esICAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountsQuery) As String
			Return ICAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountsQuery
			Return DirectCast(ICAccountsQuery.SerializeHelper.FromXml(query, GetType(ICAccountsQuery)), ICAccountsQuery)
		End Operator

		#End Region
		
		Protected Overrides Function GetConnectionName() As String
			Return "ConnectionToICAccount"
		End Function	
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim query As New ICAccountsQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Accounts.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.AccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsMetadata.ColumnNames.AccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsMetadata.ColumnNames.AccountTitle, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.AccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsMetadata.ColumnNames.CreateBy, value) Then
					Me._UpToICUserByCreateBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreateBy")
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.NotificationSendDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NotificationSendDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsMetadata.ColumnNames.NotificationSendDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsMetadata.ColumnNames.NotificationSendDate, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.NotificationSendDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.DailyBalanceAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DailyBalanceAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsMetadata.ColumnNames.DailyBalanceAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsMetadata.ColumnNames.DailyBalanceAllow, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.DailyBalanceAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.CustomerSwiftCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CustomerSwiftCode As System.String
			Get
				Return MyBase.GetSystemString(ICAccountsMetadata.ColumnNames.CustomerSwiftCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountsMetadata.ColumnNames.CustomerSwiftCode, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.CustomerSwiftCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountsMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountsMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountsMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountsMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Accounts.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAccountsMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAccountsMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICAccountsMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreateBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "AccountTitle"
							Me.str().AccountTitle = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "NotificationSendDate"
							Me.str().NotificationSendDate = CType(value, string)
												
						Case "DailyBalanceAllow"
							Me.str().DailyBalanceAllow = CType(value, string)
												
						Case "CustomerSwiftCode"
							Me.str().CustomerSwiftCode = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreateDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "NotificationSendDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.NotificationSendDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.NotificationSendDate)
							End If
						
						Case "DailyBalanceAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.DailyBalanceAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.DailyBalanceAllow)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAccountsMetadata.PropertyNames.ApprovedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.AccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountTitle = Nothing
					Else
						entity.AccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property NotificationSendDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.NotificationSendDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NotificationSendDate = Nothing
					Else
						entity.NotificationSendDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DailyBalanceAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.DailyBalanceAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DailyBalanceAllow = Nothing
					Else
						entity.DailyBalanceAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CustomerSwiftCode As System.String 
				Get
					Dim data_ As System.String = entity.CustomerSwiftCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CustomerSwiftCode = Nothing
					Else
						entity.CustomerSwiftCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountsCollection
		Inherits esEntityCollection(Of ICAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "AccountTitle" 
					Return Me.AccountTitle
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "NotificationSendDate" 
					Return Me.NotificationSendDate
				Case "DailyBalanceAllow" 
					Return Me.DailyBalanceAllow
				Case "CustomerSwiftCode" 
					Return Me.CustomerSwiftCode
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.AccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property NotificationSendDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.NotificationSendDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DailyBalanceAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.DailyBalanceAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CustomerSwiftCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.CustomerSwiftCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountsMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccounts 
		Inherits esICAccounts
		
	
		#Region "ICAccountPaymentNatureTemplateCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureTemplateCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICAccountPaymentNatureTemplateCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountPaymentNatureTemplateCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureTemplateCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureTemplateQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureTemplateQuery), New ICAccountPaymentNatureTemplateQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureTemplate_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureTemplateCollectionByAccountNumber As ICAccountPaymentNatureTemplateCollection 
		
			Get
				If Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber Is Nothing Then
					Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber = New ICAccountPaymentNatureTemplateCollection()
					Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureTemplateCollectionByAccountNumber", Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.Where(Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureTemplateMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureTemplateMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber.fks.Add(ICAccountPaymentNatureTemplateMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureTemplateCollectionByAccountNumber")
					Me._ICAccountPaymentNatureTemplateCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureTemplateCollectionByAccountNumber As ICAccountPaymentNatureTemplateCollection
		#End Region

		#Region "ICAccountsPaymentNatureCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICAccountsPaymentNatureCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureQuery), New ICAccountsPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNature_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureCollectionByAccountNumber As ICAccountsPaymentNatureCollection 
		
			Get
				If Me._ICAccountsPaymentNatureCollectionByAccountNumber Is Nothing Then
					Me._ICAccountsPaymentNatureCollectionByAccountNumber = New ICAccountsPaymentNatureCollection()
					Me._ICAccountsPaymentNatureCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureCollectionByAccountNumber", Me._ICAccountsPaymentNatureCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICAccountsPaymentNatureCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICAccountsPaymentNatureCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICAccountsPaymentNatureCollectionByAccountNumber.fks.Add(ICAccountsPaymentNatureMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICAccountsPaymentNatureCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureCollectionByAccountNumber")
					Me._ICAccountsPaymentNatureCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureCollectionByAccountNumber As ICAccountsPaymentNatureCollection
		#End Region

		#Region "ICEmailSettingsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICEmailSettingsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICEmailSettingsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICEmailSettingsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICEmailSettingsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICEmailSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICEmailSettingsQuery), New ICEmailSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Email_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICEmailSettingsCollectionByAccountNumber As ICEmailSettingsCollection 
		
			Get
				If Me._ICEmailSettingsCollectionByAccountNumber Is Nothing Then
					Me._ICEmailSettingsCollectionByAccountNumber = New ICEmailSettingsCollection()
					Me._ICEmailSettingsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICEmailSettingsCollectionByAccountNumber", Me._ICEmailSettingsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Where(Me._ICEmailSettingsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICEmailSettingsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICEmailSettingsCollectionByAccountNumber.fks.Add(ICEmailSettingsMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICEmailSettingsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICEmailSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICEmailSettingsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICEmailSettingsCollectionByAccountNumber")
					Me._ICEmailSettingsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICEmailSettingsCollectionByAccountNumber As ICEmailSettingsCollection
		#End Region

		#Region "ICFTPSettingsCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICFTPSettingsCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICFTPSettingsCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICFTPSettingsCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICFTPSettingsCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICFTPSettingsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICFTPSettingsQuery), New ICFTPSettingsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_FTP_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICFTPSettingsCollectionByAccountNumber As ICFTPSettingsCollection 
		
			Get
				If Me._ICFTPSettingsCollectionByAccountNumber Is Nothing Then
					Me._ICFTPSettingsCollectionByAccountNumber = New ICFTPSettingsCollection()
					Me._ICFTPSettingsCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICFTPSettingsCollectionByAccountNumber", Me._ICFTPSettingsCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Where(Me._ICFTPSettingsCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICFTPSettingsCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICFTPSettingsCollectionByAccountNumber.fks.Add(ICFTPSettingsMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICFTPSettingsCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICFTPSettingsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICFTPSettingsCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICFTPSettingsCollectionByAccountNumber")
					Me._ICFTPSettingsCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICFTPSettingsCollectionByAccountNumber As ICFTPSettingsCollection
		#End Region

		#Region "ICInstructionCollectionByClientAccountNo - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByClientAccountNo() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICInstructionCollectionByClientAccountNo_Delegate)
				map.PropertyName = "ICInstructionCollectionByClientAccountNo"
				map.MyColumnName = "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByClientAccountNo_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.ClientAccountNo And parent.BranchCode = mee.ClientAccountBranchCode And parent.Currency = mee.ClientAccountCurrency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByClientAccountNo As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByClientAccountNo Is Nothing Then
					Me._ICInstructionCollectionByClientAccountNo = New ICInstructionCollection()
					Me._ICInstructionCollectionByClientAccountNo.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByClientAccountNo", Me._ICInstructionCollectionByClientAccountNo)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByClientAccountNo.Query.Where(Me._ICInstructionCollectionByClientAccountNo.Query.ClientAccountNo = Me.AccountNumber)
							Me._ICInstructionCollectionByClientAccountNo.Query.Where(Me._ICInstructionCollectionByClientAccountNo.Query.ClientAccountBranchCode = Me.BranchCode)
							Me._ICInstructionCollectionByClientAccountNo.Query.Where(Me._ICInstructionCollectionByClientAccountNo.Query.ClientAccountCurrency = Me.Currency)
							Me._ICInstructionCollectionByClientAccountNo.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByClientAccountNo.fks.Add(ICInstructionMetadata.ColumnNames.ClientAccountNo, Me.AccountNumber)
						Me._ICInstructionCollectionByClientAccountNo.fks.Add(ICInstructionMetadata.ColumnNames.ClientAccountBranchCode, Me.BranchCode)
						Me._ICInstructionCollectionByClientAccountNo.fks.Add(ICInstructionMetadata.ColumnNames.ClientAccountCurrency, Me.Currency)
					End If
				End If

				Return Me._ICInstructionCollectionByClientAccountNo
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByClientAccountNo Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByClientAccountNo")
					Me._ICInstructionCollectionByClientAccountNo = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByClientAccountNo As ICInstructionCollection
		#End Region

		#Region "ICLimitForApprovalCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICLimitForApprovalCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICLimitForApprovalCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICLimitForApprovalCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICLimitForApprovalCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICLimitForApprovalQuery = If(data.You IsNot Nothing, TryCast(data.You, ICLimitForApprovalQuery), New ICLimitForApprovalQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_LimitForApproval_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICLimitForApprovalCollectionByAccountNumber As ICLimitForApprovalCollection 
		
			Get
				If Me._ICLimitForApprovalCollectionByAccountNumber Is Nothing Then
					Me._ICLimitForApprovalCollectionByAccountNumber = New ICLimitForApprovalCollection()
					Me._ICLimitForApprovalCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICLimitForApprovalCollectionByAccountNumber", Me._ICLimitForApprovalCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICLimitForApprovalCollectionByAccountNumber.Query.Where(Me._ICLimitForApprovalCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICLimitForApprovalCollectionByAccountNumber.Query.Where(Me._ICLimitForApprovalCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICLimitForApprovalCollectionByAccountNumber.Query.Where(Me._ICLimitForApprovalCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICLimitForApprovalCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICLimitForApprovalCollectionByAccountNumber.fks.Add(ICLimitForApprovalMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICLimitForApprovalCollectionByAccountNumber.fks.Add(ICLimitForApprovalMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICLimitForApprovalCollectionByAccountNumber.fks.Add(ICLimitForApprovalMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICLimitForApprovalCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICLimitForApprovalCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICLimitForApprovalCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICLimitForApprovalCollectionByAccountNumber")
					Me._ICLimitForApprovalCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICLimitForApprovalCollectionByAccountNumber As ICLimitForApprovalCollection
		#End Region

		#Region "ICMasterSeriesCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICMasterSeriesCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICMasterSeriesCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICMasterSeriesCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICMasterSeriesCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICMasterSeriesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICMasterSeriesQuery), New ICMasterSeriesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MasterSeries_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICMasterSeriesCollectionByAccountNumber As ICMasterSeriesCollection 
		
			Get
				If Me._ICMasterSeriesCollectionByAccountNumber Is Nothing Then
					Me._ICMasterSeriesCollectionByAccountNumber = New ICMasterSeriesCollection()
					Me._ICMasterSeriesCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICMasterSeriesCollectionByAccountNumber", Me._ICMasterSeriesCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICMasterSeriesCollectionByAccountNumber.Query.Where(Me._ICMasterSeriesCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICMasterSeriesCollectionByAccountNumber.Query.Where(Me._ICMasterSeriesCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICMasterSeriesCollectionByAccountNumber.Query.Where(Me._ICMasterSeriesCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICMasterSeriesCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICMasterSeriesCollectionByAccountNumber.fks.Add(ICMasterSeriesMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICMasterSeriesCollectionByAccountNumber.fks.Add(ICMasterSeriesMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICMasterSeriesCollectionByAccountNumber.fks.Add(ICMasterSeriesMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICMasterSeriesCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICMasterSeriesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICMasterSeriesCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICMasterSeriesCollectionByAccountNumber")
					Me._ICMasterSeriesCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICMasterSeriesCollectionByAccountNumber As ICMasterSeriesCollection
		#End Region

		#Region "ICOnlineFormCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOnlineFormCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICOnlineFormCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICOnlineFormCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICOnlineFormCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICOnlineFormQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOnlineFormQuery), New ICOnlineFormQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_OnlineForm_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOnlineFormCollectionByAccountNumber As ICOnlineFormCollection 
		
			Get
				If Me._ICOnlineFormCollectionByAccountNumber Is Nothing Then
					Me._ICOnlineFormCollectionByAccountNumber = New ICOnlineFormCollection()
					Me._ICOnlineFormCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOnlineFormCollectionByAccountNumber", Me._ICOnlineFormCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOnlineFormCollectionByAccountNumber.Query.Where(Me._ICOnlineFormCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICOnlineFormCollectionByAccountNumber.Query.Where(Me._ICOnlineFormCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICOnlineFormCollectionByAccountNumber.Query.Where(Me._ICOnlineFormCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICOnlineFormCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOnlineFormCollectionByAccountNumber.fks.Add(ICOnlineFormMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICOnlineFormCollectionByAccountNumber.fks.Add(ICOnlineFormMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICOnlineFormCollectionByAccountNumber.fks.Add(ICOnlineFormMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICOnlineFormCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICOnlineFormCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOnlineFormCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICOnlineFormCollectionByAccountNumber")
					Me._ICOnlineFormCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOnlineFormCollectionByAccountNumber As ICOnlineFormCollection
		#End Region

		#Region "ICPdcCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPdcCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICPdcCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICPdcCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICPdcCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICPdcQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPdcQuery), New ICPdcQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Pdc_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPdcCollectionByAccountNumber As ICPdcCollection 
		
			Get
				If Me._ICPdcCollectionByAccountNumber Is Nothing Then
					Me._ICPdcCollectionByAccountNumber = New ICPdcCollection()
					Me._ICPdcCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPdcCollectionByAccountNumber", Me._ICPdcCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPdcCollectionByAccountNumber.Query.Where(Me._ICPdcCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICPdcCollectionByAccountNumber.Query.Where(Me._ICPdcCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICPdcCollectionByAccountNumber.Query.Where(Me._ICPdcCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICPdcCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPdcCollectionByAccountNumber.fks.Add(ICPdcMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICPdcCollectionByAccountNumber.fks.Add(ICPdcMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICPdcCollectionByAccountNumber.fks.Add(ICPdcMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICPdcCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICPdcCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPdcCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICPdcCollectionByAccountNumber")
					Me._ICPdcCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPdcCollectionByAccountNumber As ICPdcCollection
		#End Region

		#Region "ICSubSetCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSetCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICSubSetCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICSubSetCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSetCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICSubSetQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSetQuery), New ICSubSetQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSet_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSetCollectionByAccountNumber As ICSubSetCollection 
		
			Get
				If Me._ICSubSetCollectionByAccountNumber Is Nothing Then
					Me._ICSubSetCollectionByAccountNumber = New ICSubSetCollection()
					Me._ICSubSetCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSetCollectionByAccountNumber", Me._ICSubSetCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSetCollectionByAccountNumber.Query.Where(Me._ICSubSetCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICSubSetCollectionByAccountNumber.Query.Where(Me._ICSubSetCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICSubSetCollectionByAccountNumber.Query.Where(Me._ICSubSetCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICSubSetCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSetCollectionByAccountNumber.fks.Add(ICSubSetMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICSubSetCollectionByAccountNumber.fks.Add(ICSubSetMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICSubSetCollectionByAccountNumber.fks.Add(ICSubSetMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICSubSetCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICSubSetCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSetCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICSubSetCollectionByAccountNumber")
					Me._ICSubSetCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSetCollectionByAccountNumber As ICSubSetCollection
		#End Region

		#Region "ICTaggedClientUserForAccountCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICTaggedClientUserForAccountCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICTaggedClientUserForAccountCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICTaggedClientUserForAccountCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICTaggedClientUserForAccountCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICTaggedClientUserForAccountQuery = If(data.You IsNot Nothing, TryCast(data.You, ICTaggedClientUserForAccountQuery), New ICTaggedClientUserForAccountQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_TaggedUser_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICTaggedClientUserForAccountCollectionByAccountNumber As ICTaggedClientUserForAccountCollection 
		
			Get
				If Me._ICTaggedClientUserForAccountCollectionByAccountNumber Is Nothing Then
					Me._ICTaggedClientUserForAccountCollectionByAccountNumber = New ICTaggedClientUserForAccountCollection()
					Me._ICTaggedClientUserForAccountCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICTaggedClientUserForAccountCollectionByAccountNumber", Me._ICTaggedClientUserForAccountCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.Where(Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.Where(Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.Where(Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICTaggedClientUserForAccountCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICTaggedClientUserForAccountCollectionByAccountNumber.fks.Add(ICTaggedClientUserForAccountMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICTaggedClientUserForAccountCollectionByAccountNumber.fks.Add(ICTaggedClientUserForAccountMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICTaggedClientUserForAccountCollectionByAccountNumber.fks.Add(ICTaggedClientUserForAccountMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICTaggedClientUserForAccountCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICTaggedClientUserForAccountCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICTaggedClientUserForAccountCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICTaggedClientUserForAccountCollectionByAccountNumber")
					Me._ICTaggedClientUserForAccountCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICTaggedClientUserForAccountCollectionByAccountNumber As ICTaggedClientUserForAccountCollection
		#End Region

		#Region "ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICUserRolesAccountAndPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesAccountAndPaymentNatureQuery), New ICUserRolesAccountAndPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber As ICUserRolesAccountAndPaymentNatureCollection 
		
			Get
				If Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber Is Nothing Then
					Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber = New ICUserRolesAccountAndPaymentNatureCollection()
					Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber", Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICUserRolesAccountAndPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber")
					Me._ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber As ICUserRolesAccountAndPaymentNatureCollection
		#End Region

		#Region "ICWebServiceCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICWebServiceCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICAccounts.ICWebServiceCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICWebServiceCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICWebServiceCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICAccountsQuery(data.NextAlias())
			
			Dim mee As ICWebServiceQuery = If(data.You IsNot Nothing, TryCast(data.You, ICWebServiceQuery), New ICWebServiceQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_WebService_IC_Accounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICWebServiceCollectionByAccountNumber As ICWebServiceCollection 
		
			Get
				If Me._ICWebServiceCollectionByAccountNumber Is Nothing Then
					Me._ICWebServiceCollectionByAccountNumber = New ICWebServiceCollection()
					Me._ICWebServiceCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICWebServiceCollectionByAccountNumber", Me._ICWebServiceCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICWebServiceCollectionByAccountNumber.Query.Where(Me._ICWebServiceCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICWebServiceCollectionByAccountNumber.Query.Where(Me._ICWebServiceCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICWebServiceCollectionByAccountNumber.Query.Where(Me._ICWebServiceCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICWebServiceCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICWebServiceCollectionByAccountNumber.fks.Add(ICWebServiceMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICWebServiceCollectionByAccountNumber.fks.Add(ICWebServiceMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICWebServiceCollectionByAccountNumber.fks.Add(ICWebServiceMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICWebServiceCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICWebServiceCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICWebServiceCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICWebServiceCollectionByAccountNumber")
					Me._ICWebServiceCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICWebServiceCollectionByAccountNumber As ICWebServiceCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Accounts_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreateBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Accounts_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreateBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreateBy Is Nothing _
						 AndAlso Not CreateBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreateBy = New ICUser()
					Me._UpToICUserByCreateBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreateBy", Me._UpToICUserByCreateBy)
					Me._UpToICUserByCreateBy.Query.Where(Me._UpToICUserByCreateBy.Query.UserID = Me.CreateBy)
					Me._UpToICUserByCreateBy.Query.Load()
				End If

				Return Me._UpToICUserByCreateBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreateBy")
				

				If value Is Nothing Then
				
					Me.CreateBy = Nothing
				
					Me._UpToICUserByCreateBy = Nothing
				Else
				
					Me.CreateBy = value.UserID
					
					Me._UpToICUserByCreateBy = value
					Me.SetPreSave("UpToICUserByCreateBy", Me._UpToICUserByCreateBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureTemplateCollectionByAccountNumber"
					coll = Me.ICAccountPaymentNatureTemplateCollectionByAccountNumber
					Exit Select
				Case "ICAccountsPaymentNatureCollectionByAccountNumber"
					coll = Me.ICAccountsPaymentNatureCollectionByAccountNumber
					Exit Select
				Case "ICEmailSettingsCollectionByAccountNumber"
					coll = Me.ICEmailSettingsCollectionByAccountNumber
					Exit Select
				Case "ICFTPSettingsCollectionByAccountNumber"
					coll = Me.ICFTPSettingsCollectionByAccountNumber
					Exit Select
				Case "ICInstructionCollectionByClientAccountNo"
					coll = Me.ICInstructionCollectionByClientAccountNo
					Exit Select
				Case "ICLimitForApprovalCollectionByAccountNumber"
					coll = Me.ICLimitForApprovalCollectionByAccountNumber
					Exit Select
				Case "ICMasterSeriesCollectionByAccountNumber"
					coll = Me.ICMasterSeriesCollectionByAccountNumber
					Exit Select
				Case "ICOnlineFormCollectionByAccountNumber"
					coll = Me.ICOnlineFormCollectionByAccountNumber
					Exit Select
				Case "ICPdcCollectionByAccountNumber"
					coll = Me.ICPdcCollectionByAccountNumber
					Exit Select
				Case "ICSubSetCollectionByAccountNumber"
					coll = Me.ICSubSetCollectionByAccountNumber
					Exit Select
				Case "ICTaggedClientUserForAccountCollectionByAccountNumber"
					coll = Me.ICTaggedClientUserForAccountCollectionByAccountNumber
					Exit Select
				Case "ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber"
					coll = Me.ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber
					Exit Select
				Case "ICWebServiceCollectionByAccountNumber"
					coll = Me.ICWebServiceCollectionByAccountNumber
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureTemplateCollectionByAccountNumber", GetType(ICAccountPaymentNatureTemplateCollection), New ICAccountPaymentNatureTemplate()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureCollectionByAccountNumber", GetType(ICAccountsPaymentNatureCollection), New ICAccountsPaymentNature()))
			props.Add(new esPropertyDescriptor(Me, "ICEmailSettingsCollectionByAccountNumber", GetType(ICEmailSettingsCollection), New ICEmailSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICFTPSettingsCollectionByAccountNumber", GetType(ICFTPSettingsCollection), New ICFTPSettings()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByClientAccountNo", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICLimitForApprovalCollectionByAccountNumber", GetType(ICLimitForApprovalCollection), New ICLimitForApproval()))
			props.Add(new esPropertyDescriptor(Me, "ICMasterSeriesCollectionByAccountNumber", GetType(ICMasterSeriesCollection), New ICMasterSeries()))
			props.Add(new esPropertyDescriptor(Me, "ICOnlineFormCollectionByAccountNumber", GetType(ICOnlineFormCollection), New ICOnlineForm()))
			props.Add(new esPropertyDescriptor(Me, "ICPdcCollectionByAccountNumber", GetType(ICPdcCollection), New ICPdc()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSetCollectionByAccountNumber", GetType(ICSubSetCollection), New ICSubSet()))
			props.Add(new esPropertyDescriptor(Me, "ICTaggedClientUserForAccountCollectionByAccountNumber", GetType(ICTaggedClientUserForAccountCollection), New ICTaggedClientUserForAccount()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesAccountAndPaymentNatureCollectionByAccountNumber", GetType(ICUserRolesAccountAndPaymentNatureCollection), New ICUserRolesAccountAndPaymentNature()))
			props.Add(new esPropertyDescriptor(Me, "ICWebServiceCollectionByAccountNumber", GetType(ICWebServiceCollection), New ICWebService()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.AccountTitle, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.AccountTitle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.CompanyCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.Creater, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.CreationDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.NotificationSendDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.NotificationSendDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.DailyBalanceAllow, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.DailyBalanceAllow
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.CustomerSwiftCode, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.CustomerSwiftCode
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.ApprovedBy, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.IsApproved, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountsMetadata.ColumnNames.ApprovedOn, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAccountsMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "isActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const NotificationSendDate As String = "NotificationSendDate"
			 Public Const DailyBalanceAllow As String = "DailyBalanceAllow"
			 Public Const CustomerSwiftCode As String = "CustomerSwiftCode"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const NotificationSendDate As String = "NotificationSendDate"
			 Public Const DailyBalanceAllow As String = "DailyBalanceAllow"
			 Public Const CustomerSwiftCode As String = "CustomerSwiftCode"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountsMetadata)
			
				If ICAccountsMetadata.mapDelegates Is Nothing Then
					ICAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountsMetadata._meta Is Nothing Then
					ICAccountsMetadata._meta = New ICAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("NotificationSendDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DailyBalanceAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CustomerSwiftCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Accounts"
				meta.Destination = "IC_Accounts"
				
				meta.spInsert = "proc_IC_AccountsInsert"
				meta.spUpdate = "proc_IC_AccountsUpdate"
				meta.spDelete = "proc_IC_AccountsDelete"
				meta.spLoadAll = "proc_IC_AccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
