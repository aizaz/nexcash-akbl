
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 12-May-2014 11:12:31 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Office' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICOffice))> _
	<XmlType("ICOffice")> _	
	Partial Public Class ICOffice 
		Inherits esICOffice
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICOffice()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal officeID As System.Int32)
			Dim obj As New ICOffice()
			obj.OfficeID = officeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal officeID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICOffice()
			obj.OfficeID = officeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICOfficeCollection")> _
	Partial Public Class ICOfficeCollection
		Inherits esICOfficeCollection
		Implements IEnumerable(Of ICOffice)
	
		Public Function FindByPrimaryKey(ByVal officeID As System.Int32) As ICOffice
			Return MyBase.SingleOrDefault(Function(e) e.OfficeID.HasValue AndAlso e.OfficeID.Value = officeID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICOffice))> _
		Public Class ICOfficeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICOfficeCollection)
			
			Public Shared Widening Operator CType(packet As ICOfficeCollectionWCFPacket) As ICOfficeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICOfficeCollection) As ICOfficeCollectionWCFPacket
				Return New ICOfficeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICOfficeQuery 
		Inherits esICOfficeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICOfficeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICOfficeQuery) As String
			Return ICOfficeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICOfficeQuery
			Return DirectCast(ICOfficeQuery.SerializeHelper.FromXml(query, GetType(ICOfficeQuery)), ICOfficeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICOffice
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal officeID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(officeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(officeID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal officeID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(officeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(officeID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal officeID As System.Int32) As Boolean
		
			Dim query As New ICOfficeQuery()
			query.Where(query.OfficeID = officeID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal officeID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("OfficeID", officeID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Office.OfficeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.OfficeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.OfficeID, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.OfficeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Description
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Description As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.Description)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.Description, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Description)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.CityID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CityID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.CityID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.CityID, value) Then
					Me._UpToICCityByCityID = Nothing
					Me.OnPropertyChanged("UpToICCityByCityID")
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.CityID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.IsPrintingLocation
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrintingLocation As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOfficeMetadata.ColumnNames.IsPrintingLocation)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOfficeMetadata.ColumnNames.IsPrintingLocation, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsPrintingLocation)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Address
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Address As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.Address)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.Address, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Address)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Phone1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Phone1 As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.Phone1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.Phone1, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Phone1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Phone2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Phone2 As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.Phone2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.Phone2, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Phone2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Email
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Email As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.Email)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.Email, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Email)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.IsApprove
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApprove As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOfficeMetadata.ColumnNames.IsApprove)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOfficeMetadata.ColumnNames.IsApprove, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsApprove)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOfficeMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOfficeMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Approvedby
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Approvedby As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.Approvedby)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.Approvedby, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Approvedby)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOfficeMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOfficeMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOfficeMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOfficeMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.OffceType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OffceType As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.OffceType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.OffceType, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.OffceType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.BankCode, value) Then
					Me._UpToICBankByBankCode = Nothing
					Me.OnPropertyChanged("UpToICBankByBankCode")
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.OfficeName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeName As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.OfficeName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.OfficeName, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.OfficeName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.OfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeCode As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.OfficeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.OfficeCode, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.OfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.IsBranch
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsBranch As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOfficeMetadata.ColumnNames.IsBranch)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOfficeMetadata.ColumnNames.IsBranch, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsBranch)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountNumber, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountBranchCode, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountCurrency, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountNumber, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountCurrency, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountBranchCode, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountNumber, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountBranchCode, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountCurrency, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalAccountType, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalSeqNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.ClearingNormalSeqNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.ClearingNormalSeqNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalClientNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.ClearingNormalProfitCenter
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingNormalProfitCenter As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalProfitCenter)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.ClearingNormalProfitCenter, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalProfitCenter)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountType, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountSeqNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.InterCityAccountSeqNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.InterCityAccountSeqNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountClientNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.InterCityAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InterCityAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.InterCityAccountProfitCentre, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountType, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountSeqNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.SameDayAccountSeqNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.SameDayAccountSeqNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountClientNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.SameDayAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SameDayAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.SameDayAccountProfitCentre, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountType, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountSeqNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.BranchCashTillAccountSeqNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.BranchCashTillAccountSeqNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountClientNo, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountProfitCentre, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountNumber, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountBranchCode, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.BranchCashTillAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCashTillAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICOfficeMetadata.ColumnNames.BranchCashTillAccountCurrency, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICOfficeMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICOfficeMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICOfficeMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICOfficeMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Office.IsAllowedForTagging
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowedForTagging As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICOfficeMetadata.ColumnNames.IsAllowedForTagging)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICOfficeMetadata.ColumnNames.IsAllowedForTagging, value) Then
					OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsAllowedForTagging)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBankByBankCode As ICBank
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCityByCityID As ICCity
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "OfficeID"
							Me.str().OfficeID = CType(value, string)
												
						Case "Description"
							Me.str().Description = CType(value, string)
												
						Case "CityID"
							Me.str().CityID = CType(value, string)
												
						Case "IsPrintingLocation"
							Me.str().IsPrintingLocation = CType(value, string)
												
						Case "Address"
							Me.str().Address = CType(value, string)
												
						Case "Phone1"
							Me.str().Phone1 = CType(value, string)
												
						Case "Phone2"
							Me.str().Phone2 = CType(value, string)
												
						Case "Email"
							Me.str().Email = CType(value, string)
												
						Case "IsApprove"
							Me.str().IsApprove = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Approvedby"
							Me.str().Approvedby = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "OffceType"
							Me.str().OffceType = CType(value, string)
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "OfficeName"
							Me.str().OfficeName = CType(value, string)
												
						Case "OfficeCode"
							Me.str().OfficeCode = CType(value, string)
												
						Case "IsBranch"
							Me.str().IsBranch = CType(value, string)
												
						Case "ClearingNormalAccountNumber"
							Me.str().ClearingNormalAccountNumber = CType(value, string)
												
						Case "ClearingNormalAccountBranchCode"
							Me.str().ClearingNormalAccountBranchCode = CType(value, string)
												
						Case "ClearingNormalAccountCurrency"
							Me.str().ClearingNormalAccountCurrency = CType(value, string)
												
						Case "InterCityAccountNumber"
							Me.str().InterCityAccountNumber = CType(value, string)
												
						Case "InterCityAccountCurrency"
							Me.str().InterCityAccountCurrency = CType(value, string)
												
						Case "InterCityAccountBranchCode"
							Me.str().InterCityAccountBranchCode = CType(value, string)
												
						Case "SameDayAccountNumber"
							Me.str().SameDayAccountNumber = CType(value, string)
												
						Case "SameDayAccountBranchCode"
							Me.str().SameDayAccountBranchCode = CType(value, string)
												
						Case "SameDayAccountCurrency"
							Me.str().SameDayAccountCurrency = CType(value, string)
												
						Case "ClearingNormalAccountType"
							Me.str().ClearingNormalAccountType = CType(value, string)
												
						Case "ClearingNormalSeqNo"
							Me.str().ClearingNormalSeqNo = CType(value, string)
												
						Case "ClearingNormalClientNo"
							Me.str().ClearingNormalClientNo = CType(value, string)
												
						Case "ClearingNormalProfitCenter"
							Me.str().ClearingNormalProfitCenter = CType(value, string)
												
						Case "InterCityAccountType"
							Me.str().InterCityAccountType = CType(value, string)
												
						Case "InterCityAccountSeqNo"
							Me.str().InterCityAccountSeqNo = CType(value, string)
												
						Case "InterCityAccountClientNo"
							Me.str().InterCityAccountClientNo = CType(value, string)
												
						Case "InterCityAccountProfitCentre"
							Me.str().InterCityAccountProfitCentre = CType(value, string)
												
						Case "SameDayAccountType"
							Me.str().SameDayAccountType = CType(value, string)
												
						Case "SameDayAccountSeqNo"
							Me.str().SameDayAccountSeqNo = CType(value, string)
												
						Case "SameDayAccountClientNo"
							Me.str().SameDayAccountClientNo = CType(value, string)
												
						Case "SameDayAccountProfitCentre"
							Me.str().SameDayAccountProfitCentre = CType(value, string)
												
						Case "BranchCashTillAccountType"
							Me.str().BranchCashTillAccountType = CType(value, string)
												
						Case "BranchCashTillAccountSeqNo"
							Me.str().BranchCashTillAccountSeqNo = CType(value, string)
												
						Case "BranchCashTillAccountClientNo"
							Me.str().BranchCashTillAccountClientNo = CType(value, string)
												
						Case "BranchCashTillAccountProfitCentre"
							Me.str().BranchCashTillAccountProfitCentre = CType(value, string)
												
						Case "BranchCashTillAccountNumber"
							Me.str().BranchCashTillAccountNumber = CType(value, string)
												
						Case "BranchCashTillAccountBranchCode"
							Me.str().BranchCashTillAccountBranchCode = CType(value, string)
												
						Case "BranchCashTillAccountCurrency"
							Me.str().BranchCashTillAccountCurrency = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "IsAllowedForTagging"
							Me.str().IsAllowedForTagging = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "OfficeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.OfficeID)
							End If
						
						Case "CityID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CityID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.CityID)
							End If
						
						Case "IsPrintingLocation"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrintingLocation = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsPrintingLocation)
							End If
						
						Case "IsApprove"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApprove = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsApprove)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Approvedby"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Approvedby = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.Approvedby)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsActive)
							End If
						
						Case "BankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.BankCode)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "IsBranch"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsBranch = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsBranch)
							End If
						
						Case "ClearingNormalSeqNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingNormalSeqNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.ClearingNormalSeqNo)
							End If
						
						Case "InterCityAccountSeqNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InterCityAccountSeqNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.InterCityAccountSeqNo)
							End If
						
						Case "SameDayAccountSeqNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SameDayAccountSeqNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.SameDayAccountSeqNo)
							End If
						
						Case "BranchCashTillAccountSeqNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BranchCashTillAccountSeqNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.BranchCashTillAccountSeqNo)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.CreationDate)
							End If
						
						Case "IsAllowedForTagging"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowedForTagging = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICOfficeMetadata.PropertyNames.IsAllowedForTagging)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICOffice)
				Me.entity = entity
			End Sub				
		
	
			Public Property OfficeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeID = Nothing
					Else
						entity.OfficeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Description As System.String 
				Get
					Dim data_ As System.String = entity.Description
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Description = Nothing
					Else
						entity.Description = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CityID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CityID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CityID = Nothing
					Else
						entity.CityID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrintingLocation As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrintingLocation
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrintingLocation = Nothing
					Else
						entity.IsPrintingLocation = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Address As System.String 
				Get
					Dim data_ As System.String = entity.Address
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Address = Nothing
					Else
						entity.Address = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Phone1 As System.String 
				Get
					Dim data_ As System.String = entity.Phone1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Phone1 = Nothing
					Else
						entity.Phone1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Phone2 As System.String 
				Get
					Dim data_ As System.String = entity.Phone2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Phone2 = Nothing
					Else
						entity.Phone2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Email As System.String 
				Get
					Dim data_ As System.String = entity.Email
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Email = Nothing
					Else
						entity.Email = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApprove As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApprove
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApprove = Nothing
					Else
						entity.IsApprove = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Approvedby As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Approvedby
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Approvedby = Nothing
					Else
						entity.Approvedby = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property OffceType As System.String 
				Get
					Dim data_ As System.String = entity.OffceType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OffceType = Nothing
					Else
						entity.OffceType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeName As System.String 
				Get
					Dim data_ As System.String = entity.OfficeName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeName = Nothing
					Else
						entity.OfficeName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeCode As System.String 
				Get
					Dim data_ As System.String = entity.OfficeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeCode = Nothing
					Else
						entity.OfficeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsBranch As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsBranch
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsBranch = Nothing
					Else
						entity.IsBranch = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalAccountNumber = Nothing
					Else
						entity.ClearingNormalAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalAccountBranchCode = Nothing
					Else
						entity.ClearingNormalAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalAccountCurrency = Nothing
					Else
						entity.ClearingNormalAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountNumber = Nothing
					Else
						entity.InterCityAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountCurrency = Nothing
					Else
						entity.InterCityAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountBranchCode = Nothing
					Else
						entity.InterCityAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountNumber = Nothing
					Else
						entity.SameDayAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountBranchCode = Nothing
					Else
						entity.SameDayAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountCurrency = Nothing
					Else
						entity.SameDayAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalAccountType = Nothing
					Else
						entity.ClearingNormalAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalSeqNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingNormalSeqNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalSeqNo = Nothing
					Else
						entity.ClearingNormalSeqNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalClientNo As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalClientNo = Nothing
					Else
						entity.ClearingNormalClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingNormalProfitCenter As System.String 
				Get
					Dim data_ As System.String = entity.ClearingNormalProfitCenter
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingNormalProfitCenter = Nothing
					Else
						entity.ClearingNormalProfitCenter = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountType As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountType = Nothing
					Else
						entity.InterCityAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountSeqNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InterCityAccountSeqNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountSeqNo = Nothing
					Else
						entity.InterCityAccountSeqNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountClientNo = Nothing
					Else
						entity.InterCityAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InterCityAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.InterCityAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InterCityAccountProfitCentre = Nothing
					Else
						entity.InterCityAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountType As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountType = Nothing
					Else
						entity.SameDayAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountSeqNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SameDayAccountSeqNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountSeqNo = Nothing
					Else
						entity.SameDayAccountSeqNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountClientNo = Nothing
					Else
						entity.SameDayAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SameDayAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.SameDayAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SameDayAccountProfitCentre = Nothing
					Else
						entity.SameDayAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountType As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountType = Nothing
					Else
						entity.BranchCashTillAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountSeqNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BranchCashTillAccountSeqNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountSeqNo = Nothing
					Else
						entity.BranchCashTillAccountSeqNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountClientNo = Nothing
					Else
						entity.BranchCashTillAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountProfitCentre = Nothing
					Else
						entity.BranchCashTillAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountNumber = Nothing
					Else
						entity.BranchCashTillAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountBranchCode = Nothing
					Else
						entity.BranchCashTillAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCashTillAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.BranchCashTillAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCashTillAccountCurrency = Nothing
					Else
						entity.BranchCashTillAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowedForTagging As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowedForTagging
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowedForTagging = Nothing
					Else
						entity.IsAllowedForTagging = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICOffice
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOfficeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICOfficeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOfficeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICOfficeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICOfficeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICOfficeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICOfficeCollection
		Inherits esEntityCollection(Of ICOffice)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICOfficeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICOfficeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICOfficeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICOfficeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICOfficeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICOfficeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICOfficeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICOfficeQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICOfficeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICOfficeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICOfficeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "OfficeID" 
					Return Me.OfficeID
				Case "Description" 
					Return Me.Description
				Case "CityID" 
					Return Me.CityID
				Case "IsPrintingLocation" 
					Return Me.IsPrintingLocation
				Case "Address" 
					Return Me.Address
				Case "Phone1" 
					Return Me.Phone1
				Case "Phone2" 
					Return Me.Phone2
				Case "Email" 
					Return Me.Email
				Case "IsApprove" 
					Return Me.IsApprove
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Approvedby" 
					Return Me.Approvedby
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "IsActive" 
					Return Me.IsActive
				Case "OffceType" 
					Return Me.OffceType
				Case "BankCode" 
					Return Me.BankCode
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "OfficeName" 
					Return Me.OfficeName
				Case "OfficeCode" 
					Return Me.OfficeCode
				Case "IsBranch" 
					Return Me.IsBranch
				Case "ClearingNormalAccountNumber" 
					Return Me.ClearingNormalAccountNumber
				Case "ClearingNormalAccountBranchCode" 
					Return Me.ClearingNormalAccountBranchCode
				Case "ClearingNormalAccountCurrency" 
					Return Me.ClearingNormalAccountCurrency
				Case "InterCityAccountNumber" 
					Return Me.InterCityAccountNumber
				Case "InterCityAccountCurrency" 
					Return Me.InterCityAccountCurrency
				Case "InterCityAccountBranchCode" 
					Return Me.InterCityAccountBranchCode
				Case "SameDayAccountNumber" 
					Return Me.SameDayAccountNumber
				Case "SameDayAccountBranchCode" 
					Return Me.SameDayAccountBranchCode
				Case "SameDayAccountCurrency" 
					Return Me.SameDayAccountCurrency
				Case "ClearingNormalAccountType" 
					Return Me.ClearingNormalAccountType
				Case "ClearingNormalSeqNo" 
					Return Me.ClearingNormalSeqNo
				Case "ClearingNormalClientNo" 
					Return Me.ClearingNormalClientNo
				Case "ClearingNormalProfitCenter" 
					Return Me.ClearingNormalProfitCenter
				Case "InterCityAccountType" 
					Return Me.InterCityAccountType
				Case "InterCityAccountSeqNo" 
					Return Me.InterCityAccountSeqNo
				Case "InterCityAccountClientNo" 
					Return Me.InterCityAccountClientNo
				Case "InterCityAccountProfitCentre" 
					Return Me.InterCityAccountProfitCentre
				Case "SameDayAccountType" 
					Return Me.SameDayAccountType
				Case "SameDayAccountSeqNo" 
					Return Me.SameDayAccountSeqNo
				Case "SameDayAccountClientNo" 
					Return Me.SameDayAccountClientNo
				Case "SameDayAccountProfitCentre" 
					Return Me.SameDayAccountProfitCentre
				Case "BranchCashTillAccountType" 
					Return Me.BranchCashTillAccountType
				Case "BranchCashTillAccountSeqNo" 
					Return Me.BranchCashTillAccountSeqNo
				Case "BranchCashTillAccountClientNo" 
					Return Me.BranchCashTillAccountClientNo
				Case "BranchCashTillAccountProfitCentre" 
					Return Me.BranchCashTillAccountProfitCentre
				Case "BranchCashTillAccountNumber" 
					Return Me.BranchCashTillAccountNumber
				Case "BranchCashTillAccountBranchCode" 
					Return Me.BranchCashTillAccountBranchCode
				Case "BranchCashTillAccountCurrency" 
					Return Me.BranchCashTillAccountCurrency
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "IsAllowedForTagging" 
					Return Me.IsAllowedForTagging
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property OfficeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.OfficeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Description As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Description, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CityID As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.CityID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrintingLocation As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.IsPrintingLocation, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Address As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Address, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Phone1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Phone1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Phone2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Phone2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Email As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Email, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsApprove As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.IsApprove, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Approvedby As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Approvedby, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property OffceType As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.OffceType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeName As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.OfficeName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.OfficeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsBranch As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.IsBranch, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalSeqNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingNormalProfitCenter As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.ClearingNormalProfitCenter, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountSeqNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InterCityAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.InterCityAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountSeqNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SameDayAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.SameDayAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountSeqNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCashTillAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.BranchCashTillAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowedForTagging As esQueryItem
			Get
				Return New esQueryItem(Me, ICOfficeMetadata.ColumnNames.IsAllowedForTagging, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICOffice 
		Inherits esICOffice
		
	
		#Region "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID_Delegate)
				map.PropertyName = "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID"
				map.MyColumnName = "OfficeID"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureProductTypeAndPrintLocationsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureProductTypeAndPrintLocationsQuery), New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.OfficeID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureProductTypeAndPrintLocations_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection 
		
			Get
				If Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID Is Nothing Then
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID = New ICAccountPaymentNatureProductTypeAndPrintLocationsCollection()
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID", Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID.Query.Where(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID.Query.OfficeID = Me.OfficeID)
							Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID.fks.Add(ICAccountPaymentNatureProductTypeAndPrintLocationsMetadata.ColumnNames.OfficeID, Me.OfficeID)
					End If
				End If

				Return Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID")
					Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
		#End Region

		#Region "ICInstructionCollectionByPrintLocationCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionCollectionByPrintLocationCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICInstructionCollectionByPrintLocationCode_Delegate)
				map.PropertyName = "ICInstructionCollectionByPrintLocationCode"
				map.MyColumnName = "PrintLocationCode"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionCollectionByPrintLocationCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICInstructionQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionQuery), New ICInstructionQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.PrintLocationCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruction_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionCollectionByPrintLocationCode As ICInstructionCollection 
		
			Get
				If Me._ICInstructionCollectionByPrintLocationCode Is Nothing Then
					Me._ICInstructionCollectionByPrintLocationCode = New ICInstructionCollection()
					Me._ICInstructionCollectionByPrintLocationCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionCollectionByPrintLocationCode", Me._ICInstructionCollectionByPrintLocationCode)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionCollectionByPrintLocationCode.Query.Where(Me._ICInstructionCollectionByPrintLocationCode.Query.PrintLocationCode = Me.OfficeID)
							Me._ICInstructionCollectionByPrintLocationCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionCollectionByPrintLocationCode.fks.Add(ICInstructionMetadata.ColumnNames.PrintLocationCode, Me.OfficeID)
					End If
				End If

				Return Me._ICInstructionCollectionByPrintLocationCode
			End Get
			
			Set(ByVal value As ICInstructionCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionCollectionByPrintLocationCode Is Nothing Then

					Me.RemovePostSave("ICInstructionCollectionByPrintLocationCode")
					Me._ICInstructionCollectionByPrintLocationCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionCollectionByPrintLocationCode As ICInstructionCollection
		#End Region

		#Region "ICInstrumentsCollectionByOfficeID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstrumentsCollectionByOfficeID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICInstrumentsCollectionByOfficeID_Delegate)
				map.PropertyName = "ICInstrumentsCollectionByOfficeID"
				map.MyColumnName = "OfficeID"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstrumentsCollectionByOfficeID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstrumentsQuery), New ICInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.OfficeID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruments_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstrumentsCollectionByOfficeID As ICInstrumentsCollection 
		
			Get
				If Me._ICInstrumentsCollectionByOfficeID Is Nothing Then
					Me._ICInstrumentsCollectionByOfficeID = New ICInstrumentsCollection()
					Me._ICInstrumentsCollectionByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstrumentsCollectionByOfficeID", Me._ICInstrumentsCollectionByOfficeID)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstrumentsCollectionByOfficeID.Query.Where(Me._ICInstrumentsCollectionByOfficeID.Query.OfficeID = Me.OfficeID)
							Me._ICInstrumentsCollectionByOfficeID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstrumentsCollectionByOfficeID.fks.Add(ICInstrumentsMetadata.ColumnNames.OfficeID, Me.OfficeID)
					End If
				End If

				Return Me._ICInstrumentsCollectionByOfficeID
			End Get
			
			Set(ByVal value As ICInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstrumentsCollectionByOfficeID Is Nothing Then

					Me.RemovePostSave("ICInstrumentsCollectionByOfficeID")
					Me._ICInstrumentsCollectionByOfficeID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstrumentsCollectionByOfficeID As ICInstrumentsCollection
		#End Region

		#Region "ICSubSeriesCollectionByOfficeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSeriesCollectionByOfficeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICSubSeriesCollectionByOfficeCode_Delegate)
				map.PropertyName = "ICSubSeriesCollectionByOfficeCode"
				map.MyColumnName = "OfficeCode"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSeriesCollectionByOfficeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICSubSeriesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSeriesQuery), New ICSubSeriesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.OfficeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSeries_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSeriesCollectionByOfficeCode As ICSubSeriesCollection 
		
			Get
				If Me._ICSubSeriesCollectionByOfficeCode Is Nothing Then
					Me._ICSubSeriesCollectionByOfficeCode = New ICSubSeriesCollection()
					Me._ICSubSeriesCollectionByOfficeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSeriesCollectionByOfficeCode", Me._ICSubSeriesCollectionByOfficeCode)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSeriesCollectionByOfficeCode.Query.Where(Me._ICSubSeriesCollectionByOfficeCode.Query.OfficeCode = Me.OfficeID)
							Me._ICSubSeriesCollectionByOfficeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSeriesCollectionByOfficeCode.fks.Add(ICSubSeriesMetadata.ColumnNames.OfficeCode, Me.OfficeID)
					End If
				End If

				Return Me._ICSubSeriesCollectionByOfficeCode
			End Get
			
			Set(ByVal value As ICSubSeriesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSeriesCollectionByOfficeCode Is Nothing Then

					Me.RemovePostSave("ICSubSeriesCollectionByOfficeCode")
					Me._ICSubSeriesCollectionByOfficeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSeriesCollectionByOfficeCode As ICSubSeriesCollection
		#End Region

		#Region "ICSubSetCollectionByOfficeID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICSubSetCollectionByOfficeID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICSubSetCollectionByOfficeID_Delegate)
				map.PropertyName = "ICSubSetCollectionByOfficeID"
				map.MyColumnName = "OfficeID"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICSubSetCollectionByOfficeID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICSubSetQuery = If(data.You IsNot Nothing, TryCast(data.You, ICSubSetQuery), New ICSubSetQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.OfficeID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_SubSet_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICSubSetCollectionByOfficeID As ICSubSetCollection 
		
			Get
				If Me._ICSubSetCollectionByOfficeID Is Nothing Then
					Me._ICSubSetCollectionByOfficeID = New ICSubSetCollection()
					Me._ICSubSetCollectionByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICSubSetCollectionByOfficeID", Me._ICSubSetCollectionByOfficeID)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICSubSetCollectionByOfficeID.Query.Where(Me._ICSubSetCollectionByOfficeID.Query.OfficeID = Me.OfficeID)
							Me._ICSubSetCollectionByOfficeID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICSubSetCollectionByOfficeID.fks.Add(ICSubSetMetadata.ColumnNames.OfficeID, Me.OfficeID)
					End If
				End If

				Return Me._ICSubSetCollectionByOfficeID
			End Get
			
			Set(ByVal value As ICSubSetCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICSubSetCollectionByOfficeID Is Nothing Then

					Me.RemovePostSave("ICSubSetCollectionByOfficeID")
					Me._ICSubSetCollectionByOfficeID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICSubSetCollectionByOfficeID As ICSubSetCollection
		#End Region

		#Region "ICUserCollectionByOfficeCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserCollectionByOfficeCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICOffice.ICUserCollectionByOfficeCode_Delegate)
				map.PropertyName = "ICUserCollectionByOfficeCode"
				map.MyColumnName = "OfficeCode"
				map.ParentColumnName = "OfficeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserCollectionByOfficeCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICOfficeQuery(data.NextAlias())
			
			Dim mee As ICUserQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserQuery), New ICUserQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.OfficeID = mee.OfficeCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_User_IC_Office
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserCollectionByOfficeCode As ICUserCollection 
		
			Get
				If Me._ICUserCollectionByOfficeCode Is Nothing Then
					Me._ICUserCollectionByOfficeCode = New ICUserCollection()
					Me._ICUserCollectionByOfficeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserCollectionByOfficeCode", Me._ICUserCollectionByOfficeCode)
				
					If Not Me.OfficeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserCollectionByOfficeCode.Query.Where(Me._ICUserCollectionByOfficeCode.Query.OfficeCode = Me.OfficeID)
							Me._ICUserCollectionByOfficeCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserCollectionByOfficeCode.fks.Add(ICUserMetadata.ColumnNames.OfficeCode, Me.OfficeID)
					End If
				End If

				Return Me._ICUserCollectionByOfficeCode
			End Get
			
			Set(ByVal value As ICUserCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserCollectionByOfficeCode Is Nothing Then

					Me.RemovePostSave("ICUserCollectionByOfficeCode")
					Me._ICUserCollectionByOfficeCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserCollectionByOfficeCode As ICUserCollection
		#End Region

		#Region "UpToICBankByBankCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Office_IC_Bank
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBankByBankCode As ICBank
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBankByBankCode Is Nothing _
						 AndAlso Not BankCode.Equals(Nothing)  Then
						
					Me._UpToICBankByBankCode = New ICBank()
					Me._UpToICBankByBankCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
					Me._UpToICBankByBankCode.Query.Where(Me._UpToICBankByBankCode.Query.BankCode = Me.BankCode)
					Me._UpToICBankByBankCode.Query.Load()
				End If

				Return Me._UpToICBankByBankCode
			End Get
			
            Set(ByVal value As ICBank)
				Me.RemovePreSave("UpToICBankByBankCode")
				

				If value Is Nothing Then
				
					Me.BankCode = Nothing
				
					Me._UpToICBankByBankCode = Nothing
				Else
				
					Me.BankCode = value.BankCode
					
					Me._UpToICBankByBankCode = value
					Me.SetPreSave("UpToICBankByBankCode", Me._UpToICBankByBankCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCityByCityID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Office_IC_City
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCityByCityID As ICCity
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCityByCityID Is Nothing _
						 AndAlso Not CityID.Equals(Nothing)  Then
						
					Me._UpToICCityByCityID = New ICCity()
					Me._UpToICCityByCityID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCityByCityID", Me._UpToICCityByCityID)
					Me._UpToICCityByCityID.Query.Where(Me._UpToICCityByCityID.Query.CityCode = Me.CityID)
					Me._UpToICCityByCityID.Query.Load()
				End If

				Return Me._UpToICCityByCityID
			End Get
			
            Set(ByVal value As ICCity)
				Me.RemovePreSave("UpToICCityByCityID")
				

				If value Is Nothing Then
				
					Me.CityID = Nothing
				
					Me._UpToICCityByCityID = Nothing
				Else
				
					Me.CityID = value.CityCode
					
					Me._UpToICCityByCityID = value
					Me.SetPreSave("UpToICCityByCityID", Me._UpToICCityByCityID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Office_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID"
					coll = Me.ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID
					Exit Select
				Case "ICInstructionCollectionByPrintLocationCode"
					coll = Me.ICInstructionCollectionByPrintLocationCode
					Exit Select
				Case "ICInstrumentsCollectionByOfficeID"
					coll = Me.ICInstrumentsCollectionByOfficeID
					Exit Select
				Case "ICSubSeriesCollectionByOfficeCode"
					coll = Me.ICSubSeriesCollectionByOfficeCode
					Exit Select
				Case "ICSubSetCollectionByOfficeID"
					coll = Me.ICSubSetCollectionByOfficeID
					Exit Select
				Case "ICUserCollectionByOfficeCode"
					coll = Me.ICUserCollectionByOfficeCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID", GetType(ICAccountPaymentNatureProductTypeAndPrintLocationsCollection), New ICAccountPaymentNatureProductTypeAndPrintLocations()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionCollectionByPrintLocationCode", GetType(ICInstructionCollection), New ICInstruction()))
			props.Add(new esPropertyDescriptor(Me, "ICInstrumentsCollectionByOfficeID", GetType(ICInstrumentsCollection), New ICInstruments()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSeriesCollectionByOfficeCode", GetType(ICSubSeriesCollection), New ICSubSeries()))
			props.Add(new esPropertyDescriptor(Me, "ICSubSetCollectionByOfficeID", GetType(ICSubSetCollection), New ICSubSet()))
			props.Add(new esPropertyDescriptor(Me, "ICUserCollectionByOfficeCode", GetType(ICUserCollection), New ICUser()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICBankByBankCode Is Nothing Then
				Me.BankCode = Me._UpToICBankByBankCode.BankCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCityByCityID Is Nothing Then
				Me.CityID = Me._UpToICCityByCityID.CityCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID Is Nothing Then
				Apply(Me._ICAccountPaymentNatureProductTypeAndPrintLocationsCollectionByOfficeID, "OfficeID", Me.OfficeID)
			End If
			
			If Not Me._ICInstructionCollectionByPrintLocationCode Is Nothing Then
				Apply(Me._ICInstructionCollectionByPrintLocationCode, "PrintLocationCode", Me.OfficeID)
			End If
			
			If Not Me._ICInstrumentsCollectionByOfficeID Is Nothing Then
				Apply(Me._ICInstrumentsCollectionByOfficeID, "OfficeID", Me.OfficeID)
			End If
			
			If Not Me._ICSubSeriesCollectionByOfficeCode Is Nothing Then
				Apply(Me._ICSubSeriesCollectionByOfficeCode, "OfficeCode", Me.OfficeID)
			End If
			
			If Not Me._ICSubSetCollectionByOfficeID Is Nothing Then
				Apply(Me._ICSubSetCollectionByOfficeID, "OfficeID", Me.OfficeID)
			End If
			
			If Not Me._ICUserCollectionByOfficeCode Is Nothing Then
				Apply(Me._ICUserCollectionByOfficeCode, "OfficeCode", Me.OfficeID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICOfficeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.OfficeID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.OfficeID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Description, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Description
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.CityID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.CityID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.IsPrintingLocation, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.IsPrintingLocation
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Address, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Address
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Phone1, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Phone1
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Phone2, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Phone2
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Email, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Email
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.IsApprove, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.IsApprove
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.CreateDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Approvedby, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Approvedby
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ApprovedOn, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.CreatedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.IsActive, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.OffceType, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.OffceType
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BankCode, 15, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.CompanyCode, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.OfficeName, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.OfficeName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.OfficeCode, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.OfficeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.IsBranch, 19, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.IsBranch
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalAccountNumber, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalAccountBranchCode, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalAccountCurrency, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountNumber, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountCurrency, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountBranchCode, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountNumber, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountBranchCode, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountCurrency, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalAccountType, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalSeqNo, 30, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalSeqNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalClientNo, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.ClearingNormalProfitCenter, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.ClearingNormalProfitCenter
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountType, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountSeqNo, 34, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountSeqNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountClientNo, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.InterCityAccountProfitCentre, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.InterCityAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountType, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountSeqNo, 38, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountSeqNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountClientNo, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.SameDayAccountProfitCentre, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.SameDayAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountType, 41, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountSeqNo, 42, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountSeqNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountClientNo, 43, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountProfitCentre, 44, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountNumber, 45, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountBranchCode, 46, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.BranchCashTillAccountCurrency, 47, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.BranchCashTillAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.Creater, 48, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.CreationDate, 49, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICOfficeMetadata.ColumnNames.IsAllowedForTagging, 50, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICOfficeMetadata.PropertyNames.IsAllowedForTagging
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICOfficeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const OfficeID As String = "OfficeID"
			 Public Const Description As String = "Description"
			 Public Const CityID As String = "CityID"
			 Public Const IsPrintingLocation As String = "IsPrintingLocation"
			 Public Const Address As String = "Address"
			 Public Const Phone1 As String = "Phone1"
			 Public Const Phone2 As String = "Phone2"
			 Public Const Email As String = "Email"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const IsActive As String = "isActive"
			 Public Const OffceType As String = "OffceType"
			 Public Const BankCode As String = "BankCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const OfficeName As String = "OfficeName"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const IsBranch As String = "IsBranch"
			 Public Const ClearingNormalAccountNumber As String = "ClearingNormalAccountNumber"
			 Public Const ClearingNormalAccountBranchCode As String = "ClearingNormalAccountBranchCode"
			 Public Const ClearingNormalAccountCurrency As String = "ClearingNormalAccountCurrency"
			 Public Const InterCityAccountNumber As String = "InterCityAccountNumber"
			 Public Const InterCityAccountCurrency As String = "InterCityAccountCurrency"
			 Public Const InterCityAccountBranchCode As String = "InterCityAccountBranchCode"
			 Public Const SameDayAccountNumber As String = "SameDayAccountNumber"
			 Public Const SameDayAccountBranchCode As String = "SameDayAccountBranchCode"
			 Public Const SameDayAccountCurrency As String = "SameDayAccountCurrency"
			 Public Const ClearingNormalAccountType As String = "ClearingNormalAccountType"
			 Public Const ClearingNormalSeqNo As String = "ClearingNormalSeqNo"
			 Public Const ClearingNormalClientNo As String = "ClearingNormalClientNo"
			 Public Const ClearingNormalProfitCenter As String = "ClearingNormalProfitCenter"
			 Public Const InterCityAccountType As String = "InterCityAccountType"
			 Public Const InterCityAccountSeqNo As String = "InterCityAccountSeqNo"
			 Public Const InterCityAccountClientNo As String = "InterCityAccountClientNo"
			 Public Const InterCityAccountProfitCentre As String = "InterCityAccountProfitCentre"
			 Public Const SameDayAccountType As String = "SameDayAccountType"
			 Public Const SameDayAccountSeqNo As String = "SameDayAccountSeqNo"
			 Public Const SameDayAccountClientNo As String = "SameDayAccountClientNo"
			 Public Const SameDayAccountProfitCentre As String = "SameDayAccountProfitCentre"
			 Public Const BranchCashTillAccountType As String = "BranchCashTillAccountType"
			 Public Const BranchCashTillAccountSeqNo As String = "BranchCashTillAccountSeqNo"
			 Public Const BranchCashTillAccountClientNo As String = "BranchCashTillAccountClientNo"
			 Public Const BranchCashTillAccountProfitCentre As String = "BranchCashTillAccountProfitCentre"
			 Public Const BranchCashTillAccountNumber As String = "BranchCashTillAccountNumber"
			 Public Const BranchCashTillAccountBranchCode As String = "BranchCashTillAccountBranchCode"
			 Public Const BranchCashTillAccountCurrency As String = "BranchCashTillAccountCurrency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsAllowedForTagging As String = "IsAllowedForTagging"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const OfficeID As String = "OfficeID"
			 Public Const Description As String = "Description"
			 Public Const CityID As String = "CityID"
			 Public Const IsPrintingLocation As String = "IsPrintingLocation"
			 Public Const Address As String = "Address"
			 Public Const Phone1 As String = "Phone1"
			 Public Const Phone2 As String = "Phone2"
			 Public Const Email As String = "Email"
			 Public Const IsApprove As String = "IsApprove"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Approvedby As String = "Approvedby"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const IsActive As String = "IsActive"
			 Public Const OffceType As String = "OffceType"
			 Public Const BankCode As String = "BankCode"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const OfficeName As String = "OfficeName"
			 Public Const OfficeCode As String = "OfficeCode"
			 Public Const IsBranch As String = "IsBranch"
			 Public Const ClearingNormalAccountNumber As String = "ClearingNormalAccountNumber"
			 Public Const ClearingNormalAccountBranchCode As String = "ClearingNormalAccountBranchCode"
			 Public Const ClearingNormalAccountCurrency As String = "ClearingNormalAccountCurrency"
			 Public Const InterCityAccountNumber As String = "InterCityAccountNumber"
			 Public Const InterCityAccountCurrency As String = "InterCityAccountCurrency"
			 Public Const InterCityAccountBranchCode As String = "InterCityAccountBranchCode"
			 Public Const SameDayAccountNumber As String = "SameDayAccountNumber"
			 Public Const SameDayAccountBranchCode As String = "SameDayAccountBranchCode"
			 Public Const SameDayAccountCurrency As String = "SameDayAccountCurrency"
			 Public Const ClearingNormalAccountType As String = "ClearingNormalAccountType"
			 Public Const ClearingNormalSeqNo As String = "ClearingNormalSeqNo"
			 Public Const ClearingNormalClientNo As String = "ClearingNormalClientNo"
			 Public Const ClearingNormalProfitCenter As String = "ClearingNormalProfitCenter"
			 Public Const InterCityAccountType As String = "InterCityAccountType"
			 Public Const InterCityAccountSeqNo As String = "InterCityAccountSeqNo"
			 Public Const InterCityAccountClientNo As String = "InterCityAccountClientNo"
			 Public Const InterCityAccountProfitCentre As String = "InterCityAccountProfitCentre"
			 Public Const SameDayAccountType As String = "SameDayAccountType"
			 Public Const SameDayAccountSeqNo As String = "SameDayAccountSeqNo"
			 Public Const SameDayAccountClientNo As String = "SameDayAccountClientNo"
			 Public Const SameDayAccountProfitCentre As String = "SameDayAccountProfitCentre"
			 Public Const BranchCashTillAccountType As String = "BranchCashTillAccountType"
			 Public Const BranchCashTillAccountSeqNo As String = "BranchCashTillAccountSeqNo"
			 Public Const BranchCashTillAccountClientNo As String = "BranchCashTillAccountClientNo"
			 Public Const BranchCashTillAccountProfitCentre As String = "BranchCashTillAccountProfitCentre"
			 Public Const BranchCashTillAccountNumber As String = "BranchCashTillAccountNumber"
			 Public Const BranchCashTillAccountBranchCode As String = "BranchCashTillAccountBranchCode"
			 Public Const BranchCashTillAccountCurrency As String = "BranchCashTillAccountCurrency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const IsAllowedForTagging As String = "IsAllowedForTagging"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICOfficeMetadata)
			
				If ICOfficeMetadata.mapDelegates Is Nothing Then
					ICOfficeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICOfficeMetadata._meta Is Nothing Then
					ICOfficeMetadata._meta = New ICOfficeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("OfficeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Description", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CityID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsPrintingLocation", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Address", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Phone1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Phone2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsApprove", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Approvedby", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("OffceType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("OfficeName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OfficeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsBranch", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ClearingNormalAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalSeqNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClearingNormalClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingNormalProfitCenter", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountSeqNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InterCityAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InterCityAccountProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountSeqNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SameDayAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SameDayAccountProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountSeqNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BranchCashTillAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCashTillAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAllowedForTagging", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_Office"
				meta.Destination = "IC_Office"
				
				meta.spInsert = "proc_IC_OfficeInsert"
				meta.spUpdate = "proc_IC_OfficeUpdate"
				meta.spDelete = "proc_IC_OfficeDelete"
				meta.spLoadAll = "proc_IC_OfficeLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_OfficeLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICOfficeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
