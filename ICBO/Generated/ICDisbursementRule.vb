
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_DisbursementRule' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICDisbursementRule))> _
	<XmlType("ICDisbursementRule")> _	
	Partial Public Class ICDisbursementRule 
		Inherits esICDisbursementRule
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICDisbursementRule()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal disbRuleID As System.Int32)
			Dim obj As New ICDisbursementRule()
			obj.DisbRuleID = disbRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal disbRuleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICDisbursementRule()
			obj.DisbRuleID = disbRuleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICDisbursementRuleCollection")> _
	Partial Public Class ICDisbursementRuleCollection
		Inherits esICDisbursementRuleCollection
		Implements IEnumerable(Of ICDisbursementRule)
	
		Public Function FindByPrimaryKey(ByVal disbRuleID As System.Int32) As ICDisbursementRule
			Return MyBase.SingleOrDefault(Function(e) e.DisbRuleID.HasValue AndAlso e.DisbRuleID.Value = disbRuleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICDisbursementRule))> _
		Public Class ICDisbursementRuleCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICDisbursementRuleCollection)
			
			Public Shared Widening Operator CType(packet As ICDisbursementRuleCollectionWCFPacket) As ICDisbursementRuleCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICDisbursementRuleCollection) As ICDisbursementRuleCollectionWCFPacket
				Return New ICDisbursementRuleCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICDisbursementRuleQuery 
		Inherits esICDisbursementRuleQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICDisbursementRuleQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICDisbursementRuleQuery) As String
			Return ICDisbursementRuleQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICDisbursementRuleQuery
			Return DirectCast(ICDisbursementRuleQuery.SerializeHelper.FromXml(query, GetType(ICDisbursementRuleQuery)), ICDisbursementRuleQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICDisbursementRule
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal disbRuleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(disbRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(disbRuleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal disbRuleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(disbRuleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(disbRuleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal disbRuleID As System.Int32) As Boolean
		
			Dim query As New ICDisbursementRuleQuery()
			query.Where(query.DisbRuleID = disbRuleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal disbRuleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DisbRuleID", disbRuleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_DisbursementRule.DisbRuleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbRuleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.DisbRuleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.DisbRuleID, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.DisbRuleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.DisbRuleName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbRuleName As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.DisbRuleName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.DisbRuleName, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.DisbRuleName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.FieldValue
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldValue As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldValue)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldValue, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.FieldValue)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.FieldCondition
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldCondition As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldCondition)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.FieldCondition, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.FieldCondition)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.DisbType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbType As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.DisbType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.DisbType, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.DisbType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As System.String
			Get
				Return MyBase.GetSystemString(ICDisbursementRuleMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDisbursementRuleMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICDisbursementRuleMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICDisbursementRuleMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICDisbursementRuleMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICDisbursementRuleMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDisbursementRuleMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DisbursementRule.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDisbursementRuleMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DisbRuleID"
							Me.str().DisbRuleID = CType(value, string)
												
						Case "DisbRuleName"
							Me.str().DisbRuleName = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldValue"
							Me.str().FieldValue = CType(value, string)
												
						Case "FieldCondition"
							Me.str().FieldCondition = CType(value, string)
												
						Case "DisbType"
							Me.str().DisbType = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "DisbRuleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DisbRuleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.DisbRuleID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.IsApproved)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDisbursementRuleMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICDisbursementRule)
				Me.entity = entity
			End Sub				
		
	
			Public Property DisbRuleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DisbRuleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbRuleID = Nothing
					Else
						entity.DisbRuleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbRuleName As System.String 
				Get
					Dim data_ As System.String = entity.DisbRuleName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbRuleName = Nothing
					Else
						entity.DisbRuleName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldValue As System.String 
				Get
					Dim data_ As System.String = entity.FieldValue
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldValue = Nothing
					Else
						entity.FieldValue = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldCondition As System.String 
				Get
					Dim data_ As System.String = entity.FieldCondition
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldCondition = Nothing
					Else
						entity.FieldCondition = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbType As System.String 
				Get
					Dim data_ As System.String = entity.DisbType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbType = Nothing
					Else
						entity.DisbType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As System.String = entity.CompanyCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICDisbursementRule
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDisbursementRuleMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICDisbursementRuleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDisbursementRuleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICDisbursementRuleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICDisbursementRuleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICDisbursementRuleQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICDisbursementRuleCollection
		Inherits esEntityCollection(Of ICDisbursementRule)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDisbursementRuleMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICDisbursementRuleCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICDisbursementRuleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDisbursementRuleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICDisbursementRuleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICDisbursementRuleQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICDisbursementRuleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICDisbursementRuleQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICDisbursementRuleQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICDisbursementRuleQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICDisbursementRuleMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DisbRuleID" 
					Return Me.DisbRuleID
				Case "DisbRuleName" 
					Return Me.DisbRuleName
				Case "FieldName" 
					Return Me.FieldName
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldValue" 
					Return Me.FieldValue
				Case "FieldCondition" 
					Return Me.FieldCondition
				Case "DisbType" 
					Return Me.DisbType
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "IsApproved" 
					Return Me.IsApproved
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DisbRuleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.DisbRuleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DisbRuleName As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.DisbRuleName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldValue As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.FieldValue, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldCondition As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.FieldCondition, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbType As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.DisbType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.CompanyCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDisbursementRuleMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICDisbursementRule 
		Inherits esICDisbursementRule
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICDisbursementRuleMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.DisbRuleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.DisbRuleID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.DisbRuleName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.DisbRuleName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.FieldName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.FieldType, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.FieldValue, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.FieldValue
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.FieldCondition, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.FieldCondition
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.DisbType, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.DisbType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.CompanyCode, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.CompanyCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.IsActive, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.CreateBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.CreateDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.ApprovedBy, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.ApprovedOn, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.IsApproved, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.Creater, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDisbursementRuleMetadata.ColumnNames.CreationDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDisbursementRuleMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICDisbursementRuleMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DisbRuleID As String = "DisbRuleID"
			 Public Const DisbRuleName As String = "DisbRuleName"
			 Public Const FieldName As String = "FieldName"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldValue As String = "FieldValue"
			 Public Const FieldCondition As String = "FieldCondition"
			 Public Const DisbType As String = "DisbType"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "isApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DisbRuleID As String = "DisbRuleID"
			 Public Const DisbRuleName As String = "DisbRuleName"
			 Public Const FieldName As String = "FieldName"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldValue As String = "FieldValue"
			 Public Const FieldCondition As String = "FieldCondition"
			 Public Const DisbType As String = "DisbType"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICDisbursementRuleMetadata)
			
				If ICDisbursementRuleMetadata.mapDelegates Is Nothing Then
					ICDisbursementRuleMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICDisbursementRuleMetadata._meta Is Nothing Then
					ICDisbursementRuleMetadata._meta = New ICDisbursementRuleMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DisbRuleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DisbRuleName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldValue", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldCondition", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_DisbursementRule"
				meta.Destination = "IC_DisbursementRule"
				
				meta.spInsert = "proc_IC_DisbursementRuleInsert"
				meta.spUpdate = "proc_IC_DisbursementRuleUpdate"
				meta.spDelete = "proc_IC_DisbursementRuleDelete"
				meta.spLoadAll = "proc_IC_DisbursementRuleLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_DisbursementRuleLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICDisbursementRuleMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
