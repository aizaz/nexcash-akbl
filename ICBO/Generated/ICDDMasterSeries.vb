
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_DDMasterSeries' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICDDMasterSeries))> _
	<XmlType("ICDDMasterSeries")> _	
	Partial Public Class ICDDMasterSeries 
		Inherits esICDDMasterSeries
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICDDMasterSeries()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal dDMasterSeriesID As System.Int32)
			Dim obj As New ICDDMasterSeries()
			obj.DDMasterSeriesID = dDMasterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal dDMasterSeriesID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICDDMasterSeries()
			obj.DDMasterSeriesID = dDMasterSeriesID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICDDMasterSeriesCollection")> _
	Partial Public Class ICDDMasterSeriesCollection
		Inherits esICDDMasterSeriesCollection
		Implements IEnumerable(Of ICDDMasterSeries)
	
		Public Function FindByPrimaryKey(ByVal dDMasterSeriesID As System.Int32) As ICDDMasterSeries
			Return MyBase.SingleOrDefault(Function(e) e.DDMasterSeriesID.HasValue AndAlso e.DDMasterSeriesID.Value = dDMasterSeriesID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICDDMasterSeries))> _
		Public Class ICDDMasterSeriesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICDDMasterSeriesCollection)
			
			Public Shared Widening Operator CType(packet As ICDDMasterSeriesCollectionWCFPacket) As ICDDMasterSeriesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICDDMasterSeriesCollection) As ICDDMasterSeriesCollectionWCFPacket
				Return New ICDDMasterSeriesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICDDMasterSeriesQuery 
		Inherits esICDDMasterSeriesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICDDMasterSeriesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICDDMasterSeriesQuery) As String
			Return ICDDMasterSeriesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICDDMasterSeriesQuery
			Return DirectCast(ICDDMasterSeriesQuery.SerializeHelper.FromXml(query, GetType(ICDDMasterSeriesQuery)), ICDDMasterSeriesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICDDMasterSeries
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal dDMasterSeriesID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDMasterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDMasterSeriesID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal dDMasterSeriesID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDMasterSeriesID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDMasterSeriesID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal dDMasterSeriesID As System.Int32) As Boolean
		
			Dim query As New ICDDMasterSeriesQuery()
			query.Where(query.DDMasterSeriesID = dDMasterSeriesID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal dDMasterSeriesID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DDMasterSeriesID", dDMasterSeriesID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_DDMasterSeries.DDMasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDMasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.DDMasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.DDMasterSeriesID, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.DDMasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.PreFix
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreFix As System.String
			Get
				Return MyBase.GetSystemString(ICDDMasterSeriesMetadata.ColumnNames.PreFix)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDMasterSeriesMetadata.ColumnNames.PreFix, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.PreFix)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.StartFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StartFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICDDMasterSeriesMetadata.ColumnNames.StartFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICDDMasterSeriesMetadata.ColumnNames.StartFrom, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.StartFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.EndsAt
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EndsAt As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICDDMasterSeriesMetadata.ColumnNames.EndsAt)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICDDMasterSeriesMetadata.ColumnNames.EndsAt, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.EndsAt)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICDDMasterSeriesMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICDDMasterSeriesMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDDMasterSeriesMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDDMasterSeriesMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDMasterSeriesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDMasterSeries.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICDDMasterSeriesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICDDMasterSeriesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DDMasterSeriesID"
							Me.str().DDMasterSeriesID = CType(value, string)
												
						Case "PreFix"
							Me.str().PreFix = CType(value, string)
												
						Case "StartFrom"
							Me.str().StartFrom = CType(value, string)
												
						Case "EndsAt"
							Me.str().EndsAt = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "DDMasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DDMasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.DDMasterSeriesID)
							End If
						
						Case "StartFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.StartFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.StartFrom)
							End If
						
						Case "EndsAt"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.EndsAt = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.EndsAt)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICDDMasterSeriesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICDDMasterSeries)
				Me.entity = entity
			End Sub				
		
	
			Public Property DDMasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DDMasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDMasterSeriesID = Nothing
					Else
						entity.DDMasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreFix As System.String 
				Get
					Dim data_ As System.String = entity.PreFix
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreFix = Nothing
					Else
						entity.PreFix = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property StartFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.StartFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StartFrom = Nothing
					Else
						entity.StartFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property EndsAt As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.EndsAt
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EndsAt = Nothing
					Else
						entity.EndsAt = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICDDMasterSeries
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDMasterSeriesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICDDMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICDDMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICDDMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICDDMasterSeriesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICDDMasterSeriesCollection
		Inherits esEntityCollection(Of ICDDMasterSeries)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDMasterSeriesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICDDMasterSeriesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICDDMasterSeriesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDMasterSeriesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICDDMasterSeriesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICDDMasterSeriesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICDDMasterSeriesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICDDMasterSeriesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICDDMasterSeriesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICDDMasterSeriesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICDDMasterSeriesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DDMasterSeriesID" 
					Return Me.DDMasterSeriesID
				Case "PreFix" 
					Return Me.PreFix
				Case "StartFrom" 
					Return Me.StartFrom
				Case "EndsAt" 
					Return Me.EndsAt
				Case "IsActive" 
					Return Me.IsActive
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DDMasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.DDMasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PreFix As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.PreFix, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property StartFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.StartFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property EndsAt As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.EndsAt, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDMasterSeriesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICDDMasterSeries 
		Inherits esICDDMasterSeries
		
	
		#Region "ICDDInstrumentsCollectionByDDMasterSeriesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICDDInstrumentsCollectionByDDMasterSeriesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICDDMasterSeries.ICDDInstrumentsCollectionByDDMasterSeriesID_Delegate)
				map.PropertyName = "ICDDInstrumentsCollectionByDDMasterSeriesID"
				map.MyColumnName = "DDMasterSeriesID"
				map.ParentColumnName = "DDMasterSeriesID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICDDInstrumentsCollectionByDDMasterSeriesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICDDMasterSeriesQuery(data.NextAlias())
			
			Dim mee As ICDDInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICDDInstrumentsQuery), New ICDDInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.DDMasterSeriesID = mee.DDMasterSeriesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_DDInstruments_IC_DDMasterSeries
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICDDInstrumentsCollectionByDDMasterSeriesID As ICDDInstrumentsCollection 
		
			Get
				If Me._ICDDInstrumentsCollectionByDDMasterSeriesID Is Nothing Then
					Me._ICDDInstrumentsCollectionByDDMasterSeriesID = New ICDDInstrumentsCollection()
					Me._ICDDInstrumentsCollectionByDDMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICDDInstrumentsCollectionByDDMasterSeriesID", Me._ICDDInstrumentsCollectionByDDMasterSeriesID)
				
					If Not Me.DDMasterSeriesID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICDDInstrumentsCollectionByDDMasterSeriesID.Query.Where(Me._ICDDInstrumentsCollectionByDDMasterSeriesID.Query.DDMasterSeriesID = Me.DDMasterSeriesID)
							Me._ICDDInstrumentsCollectionByDDMasterSeriesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICDDInstrumentsCollectionByDDMasterSeriesID.fks.Add(ICDDInstrumentsMetadata.ColumnNames.DDMasterSeriesID, Me.DDMasterSeriesID)
					End If
				End If

				Return Me._ICDDInstrumentsCollectionByDDMasterSeriesID
			End Get
			
			Set(ByVal value As ICDDInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICDDInstrumentsCollectionByDDMasterSeriesID Is Nothing Then

					Me.RemovePostSave("ICDDInstrumentsCollectionByDDMasterSeriesID")
					Me._ICDDInstrumentsCollectionByDDMasterSeriesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICDDInstrumentsCollectionByDDMasterSeriesID As ICDDInstrumentsCollection
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_DDMasterSeries_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICDDInstrumentsCollectionByDDMasterSeriesID"
					coll = Me.ICDDInstrumentsCollectionByDDMasterSeriesID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICDDInstrumentsCollectionByDDMasterSeriesID", GetType(ICDDInstrumentsCollection), New ICDDInstruments()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICDDInstrumentsCollectionByDDMasterSeriesID Is Nothing Then
				Apply(Me._ICDDInstrumentsCollectionByDDMasterSeriesID, "DDMasterSeriesID", Me.DDMasterSeriesID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICDDMasterSeriesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.DDMasterSeriesID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.DDMasterSeriesID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.PreFix, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.PreFix
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.StartFrom, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.StartFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.EndsAt, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.EndsAt
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.Creater, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDMasterSeriesMetadata.ColumnNames.CreationDate, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICDDMasterSeriesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICDDMasterSeriesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DDMasterSeriesID As String = "DDMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DDMasterSeriesID As String = "DDMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const StartFrom As String = "StartFrom"
			 Public Const EndsAt As String = "EndsAt"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICDDMasterSeriesMetadata)
			
				If ICDDMasterSeriesMetadata.mapDelegates Is Nothing Then
					ICDDMasterSeriesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICDDMasterSeriesMetadata._meta Is Nothing Then
					ICDDMasterSeriesMetadata._meta = New ICDDMasterSeriesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DDMasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PreFix", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("StartFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("EndsAt", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_DDMasterSeries"
				meta.Destination = "IC_DDMasterSeries"
				
				meta.spInsert = "proc_IC_DDMasterSeriesInsert"
				meta.spUpdate = "proc_IC_DDMasterSeriesUpdate"
				meta.spDelete = "proc_IC_DDMasterSeriesDelete"
				meta.spLoadAll = "proc_IC_DDMasterSeriesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_DDMasterSeriesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICDDMasterSeriesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
