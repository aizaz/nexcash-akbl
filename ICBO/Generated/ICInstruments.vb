
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:58 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Instruments' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstruments))> _
	<XmlType("ICInstruments")> _	
	Partial Public Class ICInstruments 
		Inherits esICInstruments
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstruments()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal instrumentsID As System.Int32)
			Dim obj As New ICInstruments()
			obj.InstrumentsID = instrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal instrumentsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstruments()
			obj.InstrumentsID = instrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstrumentsCollection")> _
	Partial Public Class ICInstrumentsCollection
		Inherits esICInstrumentsCollection
		Implements IEnumerable(Of ICInstruments)
	
		Public Function FindByPrimaryKey(ByVal instrumentsID As System.Int32) As ICInstruments
			Return MyBase.SingleOrDefault(Function(e) e.InstrumentsID.HasValue AndAlso e.InstrumentsID.Value = instrumentsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstruments))> _
		Public Class ICInstrumentsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstrumentsCollection)
			
			Public Shared Widening Operator CType(packet As ICInstrumentsCollectionWCFPacket) As ICInstrumentsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstrumentsCollection) As ICInstrumentsCollectionWCFPacket
				Return New ICInstrumentsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstrumentsQuery 
		Inherits esICInstrumentsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstrumentsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstrumentsQuery) As String
			Return ICInstrumentsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstrumentsQuery
			Return DirectCast(ICInstrumentsQuery.SerializeHelper.FromXml(query, GetType(ICInstrumentsQuery)), ICInstrumentsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstruments
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal instrumentsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instrumentsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal instrumentsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instrumentsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal instrumentsID As System.Int32) As Boolean
		
			Dim query As New ICInstrumentsQuery()
			query.Where(query.InstrumentsID = instrumentsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal instrumentsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("InstrumentsID", instrumentsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Instruments.InstrumentsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.InstrumentsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.InstrumentsID, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.InstrumentsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.InstrumentNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentNumber As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstrumentsMetadata.ColumnNames.InstrumentNumber)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstrumentsMetadata.ColumnNames.InstrumentNumber, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.InstrumentNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.SubSetID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubSetID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.SubSetID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.SubSetID, value) Then
					Me._UpToICSubSetBySubSetID = Nothing
					Me.OnPropertyChanged("UpToICSubSetBySubSetID")
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.SubSetID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.MasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.MasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.MasterSeriesID, value) Then
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
					Me.OnPropertyChanged("UpToICMasterSeriesByMasterSeriesID")
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.MasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.OfficeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.OfficeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.OfficeID, value) Then
					Me._UpToICOfficeByOfficeID = Nothing
					Me.OnPropertyChanged("UpToICOfficeByOfficeID")
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.OfficeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.IsUSed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsUSed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstrumentsMetadata.ColumnNames.IsUSed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstrumentsMetadata.ColumnNames.IsUSed, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.IsUSed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.CreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstrumentsMetadata.ColumnNames.CreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstrumentsMetadata.ColumnNames.CreatedOn, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreatedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.UsedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UsedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstrumentsMetadata.ColumnNames.UsedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstrumentsMetadata.ColumnNames.UsedOn, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.UsedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.IsAssigned
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAssigned As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstrumentsMetadata.ColumnNames.IsAssigned)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstrumentsMetadata.ColumnNames.IsAssigned, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.IsAssigned)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstrumentsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstrumentsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruments.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstrumentsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstrumentsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByOfficeID As ICOffice
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICSubSetBySubSetID As ICSubSet
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "InstrumentsID"
							Me.str().InstrumentsID = CType(value, string)
												
						Case "InstrumentNumber"
							Me.str().InstrumentNumber = CType(value, string)
												
						Case "SubSetID"
							Me.str().SubSetID = CType(value, string)
												
						Case "MasterSeriesID"
							Me.str().MasterSeriesID = CType(value, string)
												
						Case "OfficeID"
							Me.str().OfficeID = CType(value, string)
												
						Case "IsUSed"
							Me.str().IsUSed = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedOn"
							Me.str().CreatedOn = CType(value, string)
												
						Case "UsedOn"
							Me.str().UsedOn = CType(value, string)
												
						Case "IsAssigned"
							Me.str().IsAssigned = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "InstrumentsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstrumentsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.InstrumentsID)
							End If
						
						Case "InstrumentNumber"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.InstrumentNumber = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.InstrumentNumber)
							End If
						
						Case "SubSetID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SubSetID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.SubSetID)
							End If
						
						Case "MasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.MasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.MasterSeriesID)
							End If
						
						Case "OfficeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.OfficeID)
							End If
						
						Case "IsUSed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsUSed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.IsUSed)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreatedOn)
							End If
						
						Case "UsedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.UsedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.UsedOn)
							End If
						
						Case "IsAssigned"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAssigned = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.IsAssigned)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstrumentsMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstruments)
				Me.entity = entity
			End Sub				
		
	
			Public Property InstrumentsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstrumentsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentsID = Nothing
					Else
						entity.InstrumentsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentNumber As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.InstrumentNumber
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentNumber = Nothing
					Else
						entity.InstrumentNumber = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubSetID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SubSetID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubSetID = Nothing
					Else
						entity.SubSetID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property MasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.MasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MasterSeriesID = Nothing
					Else
						entity.MasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeID = Nothing
					Else
						entity.OfficeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsUSed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsUSed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsUSed = Nothing
					Else
						entity.IsUSed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOn = Nothing
					Else
						entity.CreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property UsedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.UsedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UsedOn = Nothing
					Else
						entity.UsedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAssigned As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAssigned
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAssigned = Nothing
					Else
						entity.IsAssigned = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstruments
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstrumentsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstrumentsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstrumentsCollection
		Inherits esEntityCollection(Of ICInstruments)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstrumentsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstrumentsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstrumentsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstrumentsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstrumentsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstrumentsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstrumentsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "InstrumentsID" 
					Return Me.InstrumentsID
				Case "InstrumentNumber" 
					Return Me.InstrumentNumber
				Case "SubSetID" 
					Return Me.SubSetID
				Case "MasterSeriesID" 
					Return Me.MasterSeriesID
				Case "OfficeID" 
					Return Me.OfficeID
				Case "IsUSed" 
					Return Me.IsUSed
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedOn" 
					Return Me.CreatedOn
				Case "UsedOn" 
					Return Me.UsedOn
				Case "IsAssigned" 
					Return Me.IsAssigned
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property InstrumentsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.InstrumentsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.InstrumentNumber, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property SubSetID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.SubSetID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property MasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.MasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.OfficeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsUSed As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.IsUSed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.CreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property UsedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.UsedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAssigned As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.IsAssigned, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstrumentsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstruments 
		Inherits esICInstruments
		
	
		#Region "UpToICMasterSeriesByMasterSeriesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruments_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICMasterSeriesByMasterSeriesID Is Nothing _
						 AndAlso Not MasterSeriesID.Equals(Nothing)  Then
						
					Me._UpToICMasterSeriesByMasterSeriesID = New ICMasterSeries()
					Me._UpToICMasterSeriesByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Where(Me._UpToICMasterSeriesByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Load()
				End If

				Return Me._UpToICMasterSeriesByMasterSeriesID
			End Get
			
            Set(ByVal value As ICMasterSeries)
				Me.RemovePreSave("UpToICMasterSeriesByMasterSeriesID")
				

				If value Is Nothing Then
				
					Me.MasterSeriesID = Nothing
				
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
				Else
				
					Me.MasterSeriesID = value.MasterSeriesID
					
					Me._UpToICMasterSeriesByMasterSeriesID = value
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICOfficeByOfficeID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruments_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByOfficeID As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByOfficeID Is Nothing _
						 AndAlso Not OfficeID.Equals(Nothing)  Then
						
					Me._UpToICOfficeByOfficeID = New ICOffice()
					Me._UpToICOfficeByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
					Me._UpToICOfficeByOfficeID.Query.Where(Me._UpToICOfficeByOfficeID.Query.OfficeID = Me.OfficeID)
					Me._UpToICOfficeByOfficeID.Query.Load()
				End If

				Return Me._UpToICOfficeByOfficeID
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByOfficeID")
				

				If value Is Nothing Then
				
					Me.OfficeID = Nothing
				
					Me._UpToICOfficeByOfficeID = Nothing
				Else
				
					Me.OfficeID = value.OfficeID
					
					Me._UpToICOfficeByOfficeID = value
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICSubSetBySubSetID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruments_IC_SubSet
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICSubSetBySubSetID As ICSubSet
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICSubSetBySubSetID Is Nothing _
						 AndAlso Not SubSetID.Equals(Nothing)  Then
						
					Me._UpToICSubSetBySubSetID = New ICSubSet()
					Me._UpToICSubSetBySubSetID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICSubSetBySubSetID", Me._UpToICSubSetBySubSetID)
					Me._UpToICSubSetBySubSetID.Query.Where(Me._UpToICSubSetBySubSetID.Query.SubSetID = Me.SubSetID)
					Me._UpToICSubSetBySubSetID.Query.Load()
				End If

				Return Me._UpToICSubSetBySubSetID
			End Get
			
            Set(ByVal value As ICSubSet)
				Me.RemovePreSave("UpToICSubSetBySubSetID")
				

				If value Is Nothing Then
				
					Me.SubSetID = Nothing
				
					Me._UpToICSubSetBySubSetID = Nothing
				Else
				
					Me.SubSetID = value.SubSetID
					
					Me._UpToICSubSetBySubSetID = value
					Me.SetPreSave("UpToICSubSetBySubSetID", Me._UpToICSubSetBySubSetID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruments_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICMasterSeriesByMasterSeriesID Is Nothing Then
				Me.MasterSeriesID = Me._UpToICMasterSeriesByMasterSeriesID.MasterSeriesID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByOfficeID Is Nothing Then
				Me.OfficeID = Me._UpToICOfficeByOfficeID.OfficeID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICSubSetBySubSetID Is Nothing Then
				Me.SubSetID = Me._UpToICSubSetBySubSetID.SubSetID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstrumentsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.InstrumentsID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.InstrumentsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.InstrumentNumber, 1, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.InstrumentNumber
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.SubSetID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.SubSetID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.MasterSeriesID, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.MasterSeriesID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.OfficeID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.OfficeID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.IsUSed, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.IsUSed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.CreatedOn, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.CreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.UsedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.UsedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.IsAssigned, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.IsAssigned
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.Creater, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstrumentsMetadata.ColumnNames.CreationDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstrumentsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstrumentsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const InstrumentsID As String = "InstrumentsID"
			 Public Const InstrumentNumber As String = "InstrumentNumber"
			 Public Const SubSetID As String = "SubSetID"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const IsUSed As String = "IsUSed"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const InstrumentsID As String = "InstrumentsID"
			 Public Const InstrumentNumber As String = "InstrumentNumber"
			 Public Const SubSetID As String = "SubSetID"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const IsUSed As String = "IsUSed"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstrumentsMetadata)
			
				If ICInstrumentsMetadata.mapDelegates Is Nothing Then
					ICInstrumentsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstrumentsMetadata._meta Is Nothing Then
					ICInstrumentsMetadata._meta = New ICInstrumentsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("InstrumentsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstrumentNumber", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("SubSetID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("MasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("OfficeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsUSed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("UsedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAssigned", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Instruments"
				meta.Destination = "IC_Instruments"
				
				meta.spInsert = "proc_IC_InstrumentsInsert"
				meta.spUpdate = "proc_IC_InstrumentsUpdate"
				meta.spDelete = "proc_IC_InstrumentsDelete"
				meta.spLoadAll = "proc_IC_InstrumentsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstrumentsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstrumentsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
