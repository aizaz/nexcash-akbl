
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/2/2014 1:55:41 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_WorkingDays' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICWorkingDays))> _
	<XmlType("ICWorkingDays")> _	
	Partial Public Class ICWorkingDays 
		Inherits esICWorkingDays
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICWorkingDays()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal dayName As System.String)
			Dim obj As New ICWorkingDays()
			obj.DayName = dayName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal dayName As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICWorkingDays()
			obj.DayName = dayName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICWorkingDaysCollection")> _
	Partial Public Class ICWorkingDaysCollection
		Inherits esICWorkingDaysCollection
		Implements IEnumerable(Of ICWorkingDays)
	
		Public Function FindByPrimaryKey(ByVal dayName As System.String) As ICWorkingDays
			Return MyBase.SingleOrDefault(Function(e) e.DayName = dayName)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICWorkingDays))> _
		Public Class ICWorkingDaysCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICWorkingDaysCollection)
			
			Public Shared Widening Operator CType(packet As ICWorkingDaysCollectionWCFPacket) As ICWorkingDaysCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICWorkingDaysCollection) As ICWorkingDaysCollectionWCFPacket
				Return New ICWorkingDaysCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICWorkingDaysQuery 
		Inherits esICWorkingDaysQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICWorkingDaysQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICWorkingDaysQuery) As String
			Return ICWorkingDaysQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICWorkingDaysQuery
			Return DirectCast(ICWorkingDaysQuery.SerializeHelper.FromXml(query, GetType(ICWorkingDaysQuery)), ICWorkingDaysQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICWorkingDays
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal dayName As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dayName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dayName)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal dayName As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dayName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dayName)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal dayName As System.String) As Boolean
		
			Dim query As New ICWorkingDaysQuery()
			query.Where(query.DayName = dayName)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal dayName As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DayName", dayName)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_WorkingDays.DayName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DayName As System.String
			Get
				Return MyBase.GetSystemString(ICWorkingDaysMetadata.ColumnNames.DayName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICWorkingDaysMetadata.ColumnNames.DayName, value) Then
					OnPropertyChanged(ICWorkingDaysMetadata.PropertyNames.DayName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_WorkingDays.OrderDay
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OrderDay As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICWorkingDaysMetadata.ColumnNames.OrderDay)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICWorkingDaysMetadata.ColumnNames.OrderDay, value) Then
					OnPropertyChanged(ICWorkingDaysMetadata.PropertyNames.OrderDay)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DayName"
							Me.str().DayName = CType(value, string)
												
						Case "OrderDay"
							Me.str().OrderDay = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "OrderDay"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OrderDay = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICWorkingDaysMetadata.PropertyNames.OrderDay)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICWorkingDays)
				Me.entity = entity
			End Sub				
		
	
			Public Property DayName As System.String 
				Get
					Dim data_ As System.String = entity.DayName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DayName = Nothing
					Else
						entity.DayName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OrderDay As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OrderDay
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OrderDay = Nothing
					Else
						entity.OrderDay = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICWorkingDays
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICWorkingDaysMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICWorkingDaysQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICWorkingDaysQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICWorkingDaysQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICWorkingDaysQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICWorkingDaysQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICWorkingDaysCollection
		Inherits esEntityCollection(Of ICWorkingDays)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICWorkingDaysMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICWorkingDaysCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICWorkingDaysQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICWorkingDaysQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICWorkingDaysQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICWorkingDaysQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICWorkingDaysQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICWorkingDaysQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICWorkingDaysQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICWorkingDaysQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICWorkingDaysMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DayName" 
					Return Me.DayName
				Case "OrderDay" 
					Return Me.OrderDay
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DayName As esQueryItem
			Get
				Return New esQueryItem(Me, ICWorkingDaysMetadata.ColumnNames.DayName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OrderDay As esQueryItem
			Get
				Return New esQueryItem(Me, ICWorkingDaysMetadata.ColumnNames.OrderDay, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICWorkingDays 
		Inherits esICWorkingDays
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICWorkingDaysMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICWorkingDaysMetadata.ColumnNames.DayName, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICWorkingDaysMetadata.PropertyNames.DayName
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICWorkingDaysMetadata.ColumnNames.OrderDay, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICWorkingDaysMetadata.PropertyNames.OrderDay
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICWorkingDaysMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DayName As String = "DayName"
			 Public Const OrderDay As String = "OrderDay"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DayName As String = "DayName"
			 Public Const OrderDay As String = "OrderDay"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICWorkingDaysMetadata)
			
				If ICWorkingDaysMetadata.mapDelegates Is Nothing Then
					ICWorkingDaysMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICWorkingDaysMetadata._meta Is Nothing Then
					ICWorkingDaysMetadata._meta = New ICWorkingDaysMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DayName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OrderDay", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_WorkingDays"
				meta.Destination = "IC_WorkingDays"
				
				meta.spInsert = "proc_IC_WorkingDaysInsert"
				meta.spUpdate = "proc_IC_WorkingDaysUpdate"
				meta.spDelete = "proc_IC_WorkingDaysDelete"
				meta.spLoadAll = "proc_IC_WorkingDaysLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_WorkingDaysLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICWorkingDaysMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
