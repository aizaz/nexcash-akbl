
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/20/2015 12:16:04 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_InstructionApprovalLog' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstructionApprovalLog))> _
	<XmlType("ICInstructionApprovalLog")> _	
	Partial Public Class ICInstructionApprovalLog 
		Inherits esICInstructionApprovalLog
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstructionApprovalLog()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal userApprovalLogID As System.Int32)
			Dim obj As New ICInstructionApprovalLog()
			obj.UserApprovalLogID = userApprovalLogID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal userApprovalLogID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstructionApprovalLog()
			obj.UserApprovalLogID = userApprovalLogID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstructionApprovalLogCollection")> _
	Partial Public Class ICInstructionApprovalLogCollection
		Inherits esICInstructionApprovalLogCollection
		Implements IEnumerable(Of ICInstructionApprovalLog)
	
		Public Function FindByPrimaryKey(ByVal userApprovalLogID As System.Int32) As ICInstructionApprovalLog
			Return MyBase.SingleOrDefault(Function(e) e.UserApprovalLogID.HasValue AndAlso e.UserApprovalLogID.Value = userApprovalLogID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstructionApprovalLog))> _
		Public Class ICInstructionApprovalLogCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstructionApprovalLogCollection)
			
			Public Shared Widening Operator CType(packet As ICInstructionApprovalLogCollectionWCFPacket) As ICInstructionApprovalLogCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstructionApprovalLogCollection) As ICInstructionApprovalLogCollectionWCFPacket
				Return New ICInstructionApprovalLogCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstructionApprovalLogQuery 
		Inherits esICInstructionApprovalLogQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstructionApprovalLogQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstructionApprovalLogQuery) As String
			Return ICInstructionApprovalLogQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstructionApprovalLogQuery
			Return DirectCast(ICInstructionApprovalLogQuery.SerializeHelper.FromXml(query, GetType(ICInstructionApprovalLogQuery)), ICInstructionApprovalLogQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstructionApprovalLog
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal userApprovalLogID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userApprovalLogID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userApprovalLogID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal userApprovalLogID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(userApprovalLogID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(userApprovalLogID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal userApprovalLogID As System.Int32) As Boolean
		
			Dim query As New ICInstructionApprovalLogQuery()
			query.Where(query.UserApprovalLogID = userApprovalLogID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal userApprovalLogID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("UserApprovalLogID", userApprovalLogID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.UserApprovalLogID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserApprovalLogID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.UserApprovalLogID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.UserApprovalLogID, value) Then
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.UserApprovalLogID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.InstructionID, value) Then
					Me._UpToICInstructionByInstructionID = Nothing
					Me.OnPropertyChanged("UpToICInstructionByInstructionID")
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.ApprovalRuleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalRuleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalRuleID, value) Then
					Me._UpToICApprovalRuleByApprovalRuleID = Nothing
					Me.OnPropertyChanged("UpToICApprovalRuleByApprovalRuleID")
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.ApprovalRuleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.IsAprrovalVoid
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAprrovalVoid As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionApprovalLogMetadata.ColumnNames.IsAprrovalVoid)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionApprovalLogMetadata.ColumnNames.IsAprrovalVoid, value) Then
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.IsAprrovalVoid)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.Action
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Action As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionApprovalLogMetadata.ColumnNames.Action)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionApprovalLogMetadata.ColumnNames.Action, value) Then
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.Action)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_InstructionApprovalLog.ApprovalDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalDateTime, value) Then
					OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.ApprovalDateTime)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICApprovalRuleByApprovalRuleID As ICApprovalRule
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionByInstructionID As ICInstruction
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "UserApprovalLogID"
							Me.str().UserApprovalLogID = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "ApprovalRuleID"
							Me.str().ApprovalRuleID = CType(value, string)
												
						Case "IsAprrovalVoid"
							Me.str().IsAprrovalVoid = CType(value, string)
												
						Case "Action"
							Me.str().Action = CType(value, string)
												
						Case "ApprovalDateTime"
							Me.str().ApprovalDateTime = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "UserApprovalLogID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserApprovalLogID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.UserApprovalLogID)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.InstructionID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.UserID)
							End If
						
						Case "ApprovalRuleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalRuleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.ApprovalRuleID)
							End If
						
						Case "IsAprrovalVoid"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAprrovalVoid = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.IsAprrovalVoid)
							End If
						
						Case "ApprovalDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovalDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionApprovalLogMetadata.PropertyNames.ApprovalDateTime)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstructionApprovalLog)
				Me.entity = entity
			End Sub				
		
	
			Public Property UserApprovalLogID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserApprovalLogID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserApprovalLogID = Nothing
					Else
						entity.UserApprovalLogID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalRuleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalRuleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleID = Nothing
					Else
						entity.ApprovalRuleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAprrovalVoid As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAprrovalVoid
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAprrovalVoid = Nothing
					Else
						entity.IsAprrovalVoid = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Action As System.String 
				Get
					Dim data_ As System.String = entity.Action
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Action = Nothing
					Else
						entity.Action = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovalDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalDateTime = Nothing
					Else
						entity.ApprovalDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstructionApprovalLog
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionApprovalLogMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstructionApprovalLogQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionApprovalLogQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstructionApprovalLogQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstructionApprovalLogQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstructionApprovalLogQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstructionApprovalLogCollection
		Inherits esEntityCollection(Of ICInstructionApprovalLog)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionApprovalLogMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstructionApprovalLogCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstructionApprovalLogQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionApprovalLogQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstructionApprovalLogQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstructionApprovalLogQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstructionApprovalLogQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstructionApprovalLogQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstructionApprovalLogQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstructionApprovalLogQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstructionApprovalLogMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "UserApprovalLogID" 
					Return Me.UserApprovalLogID
				Case "InstructionID" 
					Return Me.InstructionID
				Case "UserID" 
					Return Me.UserID
				Case "ApprovalRuleID" 
					Return Me.ApprovalRuleID
				Case "IsAprrovalVoid" 
					Return Me.IsAprrovalVoid
				Case "Action" 
					Return Me.Action
				Case "ApprovalDateTime" 
					Return Me.ApprovalDateTime
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property UserApprovalLogID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.UserApprovalLogID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalRuleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.ApprovalRuleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsAprrovalVoid As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.IsAprrovalVoid, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Action As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.Action, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionApprovalLogMetadata.ColumnNames.ApprovalDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstructionApprovalLog 
		Inherits esICInstructionApprovalLog
		
	
		#Region "UpToICApprovalRuleByApprovalRuleID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_ApprovalRule
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICApprovalRuleByApprovalRuleID As ICApprovalRule
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICApprovalRuleByApprovalRuleID Is Nothing _
						 AndAlso Not ApprovalRuleID.Equals(Nothing)  Then
						
					Me._UpToICApprovalRuleByApprovalRuleID = New ICApprovalRule()
					Me._UpToICApprovalRuleByApprovalRuleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICApprovalRuleByApprovalRuleID", Me._UpToICApprovalRuleByApprovalRuleID)
					Me._UpToICApprovalRuleByApprovalRuleID.Query.Where(Me._UpToICApprovalRuleByApprovalRuleID.Query.ApprovalRuleID = Me.ApprovalRuleID)
					Me._UpToICApprovalRuleByApprovalRuleID.Query.Load()
				End If

				Return Me._UpToICApprovalRuleByApprovalRuleID
			End Get
			
            Set(ByVal value As ICApprovalRule)
				Me.RemovePreSave("UpToICApprovalRuleByApprovalRuleID")
				

				If value Is Nothing Then
				
					Me.ApprovalRuleID = Nothing
				
					Me._UpToICApprovalRuleByApprovalRuleID = Nothing
				Else
				
					Me.ApprovalRuleID = value.ApprovalRuleID
					
					Me._UpToICApprovalRuleByApprovalRuleID = value
					Me.SetPreSave("UpToICApprovalRuleByApprovalRuleID", Me._UpToICApprovalRuleByApprovalRuleID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICInstructionByInstructionID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_Instruction
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionByInstructionID As ICInstruction
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionByInstructionID Is Nothing _
						 AndAlso Not InstructionID.Equals(Nothing)  Then
						
					Me._UpToICInstructionByInstructionID = New ICInstruction()
					Me._UpToICInstructionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
					Me._UpToICInstructionByInstructionID.Query.Where(Me._UpToICInstructionByInstructionID.Query.InstructionID = Me.InstructionID)
					Me._UpToICInstructionByInstructionID.Query.Load()
				End If

				Return Me._UpToICInstructionByInstructionID
			End Get
			
            Set(ByVal value As ICInstruction)
				Me.RemovePreSave("UpToICInstructionByInstructionID")
				

				If value Is Nothing Then
				
					Me.InstructionID = Nothing
				
					Me._UpToICInstructionByInstructionID = Nothing
				Else
				
					Me.InstructionID = value.InstructionID
					
					Me._UpToICInstructionByInstructionID = value
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICApprovalRuleByApprovalRuleID Is Nothing Then
				Me.ApprovalRuleID = Me._UpToICApprovalRuleByApprovalRuleID.ApprovalRuleID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionByInstructionID Is Nothing Then
				Me.InstructionID = Me._UpToICInstructionByInstructionID.InstructionID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstructionApprovalLogMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.UserApprovalLogID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.UserApprovalLogID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.InstructionID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.UserID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalRuleID, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.ApprovalRuleID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.IsAprrovalVoid, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.IsAprrovalVoid
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.Action, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.Action
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionApprovalLogMetadata.ColumnNames.ApprovalDateTime, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionApprovalLogMetadata.PropertyNames.ApprovalDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstructionApprovalLogMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const UserApprovalLogID As String = "UserApprovalLogID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const UserID As String = "UserID"
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const IsAprrovalVoid As String = "IsAprrovalVoid"
			 Public Const Action As String = "Action"
			 Public Const ApprovalDateTime As String = "ApprovalDateTime"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const UserApprovalLogID As String = "UserApprovalLogID"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const UserID As String = "UserID"
			 Public Const ApprovalRuleID As String = "ApprovalRuleID"
			 Public Const IsAprrovalVoid As String = "IsAprrovalVoid"
			 Public Const Action As String = "Action"
			 Public Const ApprovalDateTime As String = "ApprovalDateTime"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstructionApprovalLogMetadata)
			
				If ICInstructionApprovalLogMetadata.mapDelegates Is Nothing Then
					ICInstructionApprovalLogMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstructionApprovalLogMetadata._meta Is Nothing Then
					ICInstructionApprovalLogMetadata._meta = New ICInstructionApprovalLogMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("UserApprovalLogID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovalRuleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsAprrovalVoid", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Action", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ApprovalDateTime", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_InstructionApprovalLog"
				meta.Destination = "IC_InstructionApprovalLog"
				
				meta.spInsert = "proc_IC_InstructionApprovalLogInsert"
				meta.spUpdate = "proc_IC_InstructionApprovalLogUpdate"
				meta.spDelete = "proc_IC_InstructionApprovalLogDelete"
				meta.spLoadAll = "proc_IC_InstructionApprovalLogLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstructionApprovalLogLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstructionApprovalLogMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
