
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/23/2014 12:34:49 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_FieldsList' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFieldsList))> _
	<XmlType("ICFieldsList")> _	
	Partial Public Class ICFieldsList 
		Inherits esICFieldsList
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFieldsList()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fieldID As System.Int32)
			Dim obj As New ICFieldsList()
			obj.FieldID = fieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fieldID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFieldsList()
			obj.FieldID = fieldID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFieldsListCollection")> _
	Partial Public Class ICFieldsListCollection
		Inherits esICFieldsListCollection
		Implements IEnumerable(Of ICFieldsList)
	
		Public Function FindByPrimaryKey(ByVal fieldID As System.Int32) As ICFieldsList
			Return MyBase.SingleOrDefault(Function(e) e.FieldID.HasValue AndAlso e.FieldID.Value = fieldID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFieldsList))> _
		Public Class ICFieldsListCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFieldsListCollection)
			
			Public Shared Widening Operator CType(packet As ICFieldsListCollectionWCFPacket) As ICFieldsListCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFieldsListCollection) As ICFieldsListCollectionWCFPacket
				Return New ICFieldsListCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFieldsListQuery 
		Inherits esICFieldsListQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFieldsListQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFieldsListQuery) As String
			Return ICFieldsListQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFieldsListQuery
			Return DirectCast(ICFieldsListQuery.SerializeHelper.FromXml(query, GetType(ICFieldsListQuery)), ICFieldsListQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFieldsList
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fieldID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fieldID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fieldID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fieldID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fieldID As System.Int32) As Boolean
		
			Dim query As New ICFieldsListQuery()
			query.Where(query.FieldID = fieldID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fieldID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FieldID", fieldID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_FieldsList.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FieldType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldType As System.String
			Get
				Return MyBase.GetSystemString(ICFieldsListMetadata.ColumnNames.FieldType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFieldsListMetadata.ColumnNames.FieldType, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FieldOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldOrder, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICFieldsListMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFieldsListMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FixLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFieldsListMetadata.ColumnNames.FixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFieldsListMetadata.ColumnNames.FixLength, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredIBFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredIBFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredIBFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredIBFT, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredIBFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FieldDBLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDBLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldDBLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFieldsListMetadata.ColumnNames.FieldDBLength, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldDBLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredFT
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFT As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredFT)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredFT, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredFT)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredCheque
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCheque As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredCheque)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredCheque, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredCheque)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredDD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredDD As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredDD)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredDD, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredDD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredPO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredPO As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredPO)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredPO, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredPO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.MustRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MustRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.MustRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.MustRequired, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.MustRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredOnlineForm
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredOnlineForm As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredOnlineForm)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredOnlineForm)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredFileUploadTemplate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredFileUploadTemplate As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.FieldDBType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldDBType As System.String
			Get
				Return MyBase.GetSystemString(ICFieldsListMetadata.ColumnNames.FieldDBType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFieldsListMetadata.ColumnNames.FieldDBType, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldDBType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredCOTC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredCOTC As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredCOTC)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredCOTC, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredCOTC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_FieldsList.IsRequiredBillPayment
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequiredBillPayment As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredBillPayment)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICFieldsListMetadata.ColumnNames.IsRequiredBillPayment, value) Then
					OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredBillPayment)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "FieldType"
							Me.str().FieldType = CType(value, string)
												
						Case "FieldOrder"
							Me.str().FieldOrder = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FixLength"
							Me.str().FixLength = CType(value, string)
												
						Case "IsRequiredIBFT"
							Me.str().IsRequiredIBFT = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "FieldDBLength"
							Me.str().FieldDBLength = CType(value, string)
												
						Case "IsRequiredFT"
							Me.str().IsRequiredFT = CType(value, string)
												
						Case "IsRequiredCheque"
							Me.str().IsRequiredCheque = CType(value, string)
												
						Case "IsRequiredDD"
							Me.str().IsRequiredDD = CType(value, string)
												
						Case "IsRequiredPO"
							Me.str().IsRequiredPO = CType(value, string)
												
						Case "MustRequired"
							Me.str().MustRequired = CType(value, string)
												
						Case "IsRequiredOnlineForm"
							Me.str().IsRequiredOnlineForm = CType(value, string)
												
						Case "IsRequiredFileUploadTemplate"
							Me.str().IsRequiredFileUploadTemplate = CType(value, string)
												
						Case "FieldDBType"
							Me.str().FieldDBType = CType(value, string)
												
						Case "IsRequiredCOTC"
							Me.str().IsRequiredCOTC = CType(value, string)
												
						Case "IsRequiredBillPayment"
							Me.str().IsRequiredBillPayment = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldID)
							End If
						
						Case "FieldOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldOrder)
							End If
						
						Case "FixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FixLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FixLength)
							End If
						
						Case "IsRequiredIBFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredIBFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredIBFT)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsActive)
							End If
						
						Case "FieldDBLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldDBLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.FieldDBLength)
							End If
						
						Case "IsRequiredFT"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFT = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredFT)
							End If
						
						Case "IsRequiredCheque"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCheque = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredCheque)
							End If
						
						Case "IsRequiredDD"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredDD = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredDD)
							End If
						
						Case "IsRequiredPO"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredPO = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredPO)
							End If
						
						Case "MustRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.MustRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.MustRequired)
							End If
						
						Case "IsRequiredOnlineForm"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredOnlineForm = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredOnlineForm)
							End If
						
						Case "IsRequiredFileUploadTemplate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredFileUploadTemplate = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate)
							End If
						
						Case "IsRequiredCOTC"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredCOTC = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredCOTC)
							End If
						
						Case "IsRequiredBillPayment"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequiredBillPayment = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICFieldsListMetadata.PropertyNames.IsRequiredBillPayment)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFieldsList)
				Me.entity = entity
			End Sub				
		
	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldType As System.String 
				Get
					Dim data_ As System.String = entity.FieldType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldType = Nothing
					Else
						entity.FieldType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldOrder = Nothing
					Else
						entity.FieldOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FixLength = Nothing
					Else
						entity.FixLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredIBFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredIBFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredIBFT = Nothing
					Else
						entity.IsRequiredIBFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldDBLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldDBLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDBLength = Nothing
					Else
						entity.FieldDBLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFT As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFT
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFT = Nothing
					Else
						entity.IsRequiredFT = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCheque As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCheque
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCheque = Nothing
					Else
						entity.IsRequiredCheque = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredDD As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredDD
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredDD = Nothing
					Else
						entity.IsRequiredDD = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredPO As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredPO
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredPO = Nothing
					Else
						entity.IsRequiredPO = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property MustRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.MustRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MustRequired = Nothing
					Else
						entity.MustRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredOnlineForm As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredOnlineForm
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredOnlineForm = Nothing
					Else
						entity.IsRequiredOnlineForm = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredFileUploadTemplate As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredFileUploadTemplate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredFileUploadTemplate = Nothing
					Else
						entity.IsRequiredFileUploadTemplate = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldDBType As System.String 
				Get
					Dim data_ As System.String = entity.FieldDBType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldDBType = Nothing
					Else
						entity.FieldDBType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredCOTC As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredCOTC
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredCOTC = Nothing
					Else
						entity.IsRequiredCOTC = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequiredBillPayment As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequiredBillPayment
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequiredBillPayment = Nothing
					Else
						entity.IsRequiredBillPayment = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFieldsList
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFieldsListMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFieldsListQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFieldsListQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFieldsListQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFieldsListQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFieldsListQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFieldsListCollection
		Inherits esEntityCollection(Of ICFieldsList)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFieldsListMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFieldsListCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFieldsListQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFieldsListQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFieldsListQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFieldsListQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFieldsListQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFieldsListQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFieldsListQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFieldsListQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFieldsListMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FieldID" 
					Return Me.FieldID
				Case "FieldType" 
					Return Me.FieldType
				Case "FieldOrder" 
					Return Me.FieldOrder
				Case "FieldName" 
					Return Me.FieldName
				Case "FixLength" 
					Return Me.FixLength
				Case "IsRequiredIBFT" 
					Return Me.IsRequiredIBFT
				Case "IsActive" 
					Return Me.IsActive
				Case "FieldDBLength" 
					Return Me.FieldDBLength
				Case "IsRequiredFT" 
					Return Me.IsRequiredFT
				Case "IsRequiredCheque" 
					Return Me.IsRequiredCheque
				Case "IsRequiredDD" 
					Return Me.IsRequiredDD
				Case "IsRequiredPO" 
					Return Me.IsRequiredPO
				Case "MustRequired" 
					Return Me.MustRequired
				Case "IsRequiredOnlineForm" 
					Return Me.IsRequiredOnlineForm
				Case "IsRequiredFileUploadTemplate" 
					Return Me.IsRequiredFileUploadTemplate
				Case "FieldDBType" 
					Return Me.FieldDBType
				Case "IsRequiredCOTC" 
					Return Me.IsRequiredCOTC
				Case "IsRequiredBillPayment" 
					Return Me.IsRequiredBillPayment
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FieldOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FixLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredIBFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredIBFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FieldDBLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldDBLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFT As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredFT, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCheque As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredCheque, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredDD As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredDD, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredPO As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredPO, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property MustRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.MustRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredOnlineForm As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredFileUploadTemplate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property FieldDBType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.FieldDBType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredCOTC As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredCOTC, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequiredBillPayment As esQueryItem
			Get
				Return New esQueryItem(Me, ICFieldsListMetadata.ColumnNames.IsRequiredBillPayment, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFieldsList 
		Inherits esICFieldsList
		
	
		#Region "ICAmendmentsRightsManagementCollectionByFieldID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAmendmentsRightsManagementCollectionByFieldID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICFieldsList.ICAmendmentsRightsManagementCollectionByFieldID_Delegate)
				map.PropertyName = "ICAmendmentsRightsManagementCollectionByFieldID"
				map.MyColumnName = "FieldID"
				map.ParentColumnName = "FieldID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAmendmentsRightsManagementCollectionByFieldID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICFieldsListQuery(data.NextAlias())
			
			Dim mee As ICAmendmentsRightsManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAmendmentsRightsManagementQuery), New ICAmendmentsRightsManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.FieldID = mee.FieldID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_FieldsList
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAmendmentsRightsManagementCollectionByFieldID As ICAmendmentsRightsManagementCollection 
		
			Get
				If Me._ICAmendmentsRightsManagementCollectionByFieldID Is Nothing Then
					Me._ICAmendmentsRightsManagementCollectionByFieldID = New ICAmendmentsRightsManagementCollection()
					Me._ICAmendmentsRightsManagementCollectionByFieldID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAmendmentsRightsManagementCollectionByFieldID", Me._ICAmendmentsRightsManagementCollectionByFieldID)
				
					If Not Me.FieldID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAmendmentsRightsManagementCollectionByFieldID.Query.Where(Me._ICAmendmentsRightsManagementCollectionByFieldID.Query.FieldID = Me.FieldID)
							Me._ICAmendmentsRightsManagementCollectionByFieldID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAmendmentsRightsManagementCollectionByFieldID.fks.Add(ICAmendmentsRightsManagementMetadata.ColumnNames.FieldID, Me.FieldID)
					End If
				End If

				Return Me._ICAmendmentsRightsManagementCollectionByFieldID
			End Get
			
			Set(ByVal value As ICAmendmentsRightsManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAmendmentsRightsManagementCollectionByFieldID Is Nothing Then

					Me.RemovePostSave("ICAmendmentsRightsManagementCollectionByFieldID")
					Me._ICAmendmentsRightsManagementCollectionByFieldID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAmendmentsRightsManagementCollectionByFieldID As ICAmendmentsRightsManagementCollection
		#End Region

		#Region "UpToICTemplateCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_TemplateFields_IC_FieldsList
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICTemplateCollection As ICTemplateCollection
		
			Get
				If Me._UpToICTemplateCollection Is Nothing Then
					Me._UpToICTemplateCollection = New ICTemplateCollection()
					Me._UpToICTemplateCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICTemplateCollection", Me._UpToICTemplateCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.FieldID.Equals(Nothing) Then 
				
						Dim m As New ICTemplateQuery("m")
						Dim j As New ICTemplateFieldsQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.TemplateID = j.TemplateID)
                        m.Where(j.FieldID = Me.FieldID)

						Me._UpToICTemplateCollection.Load(m)

					End If
				End If

				Return Me._UpToICTemplateCollection
			End Get
			
			Set(ByVal value As ICTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICTemplateCollection Is Nothing Then

					Me.RemovePostSave("UpToICTemplateCollection")
					Me._UpToICTemplateCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_TemplateFields_IC_FieldsList
		''' </summary>
		Public Sub AssociateICTemplateCollection(entity As ICTemplate)
			If Me._ICTemplateFieldsCollection Is Nothing Then
				Me._ICTemplateFieldsCollection = New ICTemplateFieldsCollection()
				Me._ICTemplateFieldsCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICTemplateFieldsCollection", Me._ICTemplateFieldsCollection)
			End If
			
			Dim obj As ICTemplateFields = Me._ICTemplateFieldsCollection.AddNew()
			obj.FieldID = Me.FieldID
			obj.TemplateID = entity.TemplateID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_TemplateFields_IC_FieldsList
		''' </summary>
		Public Sub DissociateICTemplateCollection(entity As ICTemplate)
			If Me._ICTemplateFieldsCollection Is Nothing Then
				Me._ICTemplateFieldsCollection = new ICTemplateFieldsCollection()
				Me._ICTemplateFieldsCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICTemplateFieldsCollection", Me._ICTemplateFieldsCollection)
			End If

			Dim obj As ICTemplateFields = Me._ICTemplateFieldsCollection.AddNew()
			obj.FieldID = Me.FieldID
            obj.TemplateID = entity.TemplateID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICTemplateCollection As ICTemplateCollection
		Private _ICTemplateFieldsCollection As ICTemplateFieldsCollection
		#End Region

		#Region "ICTemplateFieldsCollectionByFieldID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICTemplateFieldsCollectionByFieldID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICFieldsList.ICTemplateFieldsCollectionByFieldID_Delegate)
				map.PropertyName = "ICTemplateFieldsCollectionByFieldID"
				map.MyColumnName = "FieldID"
				map.ParentColumnName = "FieldID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICTemplateFieldsCollectionByFieldID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICFieldsListQuery(data.NextAlias())
			
			Dim mee As ICTemplateFieldsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICTemplateFieldsQuery), New ICTemplateFieldsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.FieldID = mee.FieldID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_TemplateFields_IC_FieldsList
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICTemplateFieldsCollectionByFieldID As ICTemplateFieldsCollection 
		
			Get
				If Me._ICTemplateFieldsCollectionByFieldID Is Nothing Then
					Me._ICTemplateFieldsCollectionByFieldID = New ICTemplateFieldsCollection()
					Me._ICTemplateFieldsCollectionByFieldID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICTemplateFieldsCollectionByFieldID", Me._ICTemplateFieldsCollectionByFieldID)
				
					If Not Me.FieldID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICTemplateFieldsCollectionByFieldID.Query.Where(Me._ICTemplateFieldsCollectionByFieldID.Query.FieldID = Me.FieldID)
							Me._ICTemplateFieldsCollectionByFieldID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICTemplateFieldsCollectionByFieldID.fks.Add(ICTemplateFieldsMetadata.ColumnNames.FieldID, Me.FieldID)
					End If
				End If

				Return Me._ICTemplateFieldsCollectionByFieldID
			End Get
			
			Set(ByVal value As ICTemplateFieldsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICTemplateFieldsCollectionByFieldID Is Nothing Then

					Me.RemovePostSave("ICTemplateFieldsCollectionByFieldID")
					Me._ICTemplateFieldsCollectionByFieldID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICTemplateFieldsCollectionByFieldID As ICTemplateFieldsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAmendmentsRightsManagementCollectionByFieldID"
					coll = Me.ICAmendmentsRightsManagementCollectionByFieldID
					Exit Select
				Case "ICTemplateFieldsCollectionByFieldID"
					coll = Me.ICTemplateFieldsCollectionByFieldID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAmendmentsRightsManagementCollectionByFieldID", GetType(ICAmendmentsRightsManagementCollection), New ICAmendmentsRightsManagement()))
			props.Add(new esPropertyDescriptor(Me, "ICTemplateFieldsCollectionByFieldID", GetType(ICTemplateFieldsCollection), New ICTemplateFields()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAmendmentsRightsManagementCollectionByFieldID Is Nothing Then
				Apply(Me._ICAmendmentsRightsManagementCollectionByFieldID, "FieldID", Me.FieldID)
			End If
			
			If Not Me._ICTemplateFieldsCollection Is Nothing Then
				Apply(Me._ICTemplateFieldsCollection, "FieldID", Me.FieldID)
			End If
			
			If Not Me._ICTemplateFieldsCollectionByFieldID Is Nothing Then
				Apply(Me._ICTemplateFieldsCollectionByFieldID, "FieldID", Me.FieldID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICFieldsListMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldType, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldOrder, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FixLength, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FixLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredIBFT, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredIBFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsActive, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldDBLength, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldDBLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredFT, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredFT
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredCheque, 9, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredCheque
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredDD, 10, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredDD
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredPO, 11, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredPO
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.MustRequired, 12, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.MustRequired
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredOnlineForm, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredOnlineForm
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredFileUploadTemplate, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredFileUploadTemplate
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.FieldDBType, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.FieldDBType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredCOTC, 16, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredCOTC
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFieldsListMetadata.ColumnNames.IsRequiredBillPayment, 17, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICFieldsListMetadata.PropertyNames.IsRequiredBillPayment
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFieldsListMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsActive As String = "isActive"
			 Public Const FieldDBLength As String = "FieldDBLength"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsRequiredOnlineForm As String = "IsRequiredOnlineForm"
			 Public Const IsRequiredFileUploadTemplate As String = "IsRequiredFileUploadTemplate"
			 Public Const FieldDBType As String = "FieldDBType"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldType As String = "FieldType"
			 Public Const FieldOrder As String = "FieldOrder"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequiredIBFT As String = "IsRequiredIBFT"
			 Public Const IsActive As String = "IsActive"
			 Public Const FieldDBLength As String = "FieldDBLength"
			 Public Const IsRequiredFT As String = "IsRequiredFT"
			 Public Const IsRequiredCheque As String = "IsRequiredCheque"
			 Public Const IsRequiredDD As String = "IsRequiredDD"
			 Public Const IsRequiredPO As String = "IsRequiredPO"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsRequiredOnlineForm As String = "IsRequiredOnlineForm"
			 Public Const IsRequiredFileUploadTemplate As String = "IsRequiredFileUploadTemplate"
			 Public Const FieldDBType As String = "FieldDBType"
			 Public Const IsRequiredCOTC As String = "IsRequiredCOTC"
			 Public Const IsRequiredBillPayment As String = "IsRequiredBillPayment"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFieldsListMetadata)
			
				If ICFieldsListMetadata.mapDelegates Is Nothing Then
					ICFieldsListMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFieldsListMetadata._meta Is Nothing Then
					ICFieldsListMetadata._meta = New ICFieldsListMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FieldOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FixLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequiredIBFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FieldDBLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequiredFT", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredCheque", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredDD", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredPO", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("MustRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredOnlineForm", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredFileUploadTemplate", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("FieldDBType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsRequiredCOTC", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRequiredBillPayment", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_FieldsList"
				meta.Destination = "IC_FieldsList"
				
				meta.spInsert = "proc_IC_FieldsListInsert"
				meta.spUpdate = "proc_IC_FieldsListUpdate"
				meta.spDelete = "proc_IC_FieldsListDelete"
				meta.spLoadAll = "proc_IC_FieldsListLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FieldsListLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFieldsListMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
