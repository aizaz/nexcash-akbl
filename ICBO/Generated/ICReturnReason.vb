
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10-Dec-15 12:47:09 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ReturnReason' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICReturnReason))> _
	<XmlType("ICReturnReason")> _	
	Partial Public Class ICReturnReason 
		Inherits esICReturnReason
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICReturnReason()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal returnReasonID As System.Int32)
			Dim obj As New ICReturnReason()
			obj.ReturnReasonID = returnReasonID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal returnReasonID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICReturnReason()
			obj.ReturnReasonID = returnReasonID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICReturnReasonCollection")> _
	Partial Public Class ICReturnReasonCollection
		Inherits esICReturnReasonCollection
		Implements IEnumerable(Of ICReturnReason)
	
		Public Function FindByPrimaryKey(ByVal returnReasonID As System.Int32) As ICReturnReason
			Return MyBase.SingleOrDefault(Function(e) e.ReturnReasonID.HasValue AndAlso e.ReturnReasonID.Value = returnReasonID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICReturnReason))> _
		Public Class ICReturnReasonCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICReturnReasonCollection)
			
			Public Shared Widening Operator CType(packet As ICReturnReasonCollectionWCFPacket) As ICReturnReasonCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICReturnReasonCollection) As ICReturnReasonCollectionWCFPacket
				Return New ICReturnReasonCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICReturnReasonQuery 
		Inherits esICReturnReasonQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICReturnReasonQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICReturnReasonQuery) As String
			Return ICReturnReasonQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICReturnReasonQuery
			Return DirectCast(ICReturnReasonQuery.SerializeHelper.FromXml(query, GetType(ICReturnReasonQuery)), ICReturnReasonQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICReturnReason
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal returnReasonID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(returnReasonID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(returnReasonID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal returnReasonID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(returnReasonID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(returnReasonID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal returnReasonID As System.Int32) As Boolean
		
			Dim query As New ICReturnReasonQuery()
			query.Where(query.ReturnReasonID = returnReasonID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal returnReasonID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ReturnReasonID", returnReasonID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ReturnReason.ReturnReasonID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReturnReasonID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICReturnReasonMetadata.ColumnNames.ReturnReasonID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICReturnReasonMetadata.ColumnNames.ReturnReasonID, value) Then
					OnPropertyChanged(ICReturnReasonMetadata.PropertyNames.ReturnReasonID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ReturnReason.ReturnReason
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReturnReason As System.String
			Get
				Return MyBase.GetSystemString(ICReturnReasonMetadata.ColumnNames.ReturnReason)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICReturnReasonMetadata.ColumnNames.ReturnReason, value) Then
					OnPropertyChanged(ICReturnReasonMetadata.PropertyNames.ReturnReason)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ReturnReason.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICReturnReasonMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICReturnReasonMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICReturnReasonMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ReturnReasonID"
							Me.str().ReturnReasonID = CType(value, string)
												
						Case "ReturnReason"
							Me.str().ReturnReason = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ReturnReasonID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReturnReasonID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICReturnReasonMetadata.PropertyNames.ReturnReasonID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICReturnReasonMetadata.PropertyNames.IsActive)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICReturnReason)
				Me.entity = entity
			End Sub				
		
	
			Public Property ReturnReasonID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReturnReasonID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReturnReasonID = Nothing
					Else
						entity.ReturnReasonID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReturnReason As System.String 
				Get
					Dim data_ As System.String = entity.ReturnReason
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReturnReason = Nothing
					Else
						entity.ReturnReason = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICReturnReason
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICReturnReasonMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICReturnReasonQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICReturnReasonQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICReturnReasonQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICReturnReasonQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICReturnReasonQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICReturnReasonCollection
		Inherits esEntityCollection(Of ICReturnReason)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICReturnReasonMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICReturnReasonCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICReturnReasonQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICReturnReasonQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICReturnReasonQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICReturnReasonQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICReturnReasonQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICReturnReasonQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICReturnReasonQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICReturnReasonQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICReturnReasonMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ReturnReasonID" 
					Return Me.ReturnReasonID
				Case "ReturnReason" 
					Return Me.ReturnReason
				Case "IsActive" 
					Return Me.IsActive
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ReturnReasonID As esQueryItem
			Get
				Return New esQueryItem(Me, ICReturnReasonMetadata.ColumnNames.ReturnReasonID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReturnReason As esQueryItem
			Get
				Return New esQueryItem(Me, ICReturnReasonMetadata.ColumnNames.ReturnReason, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICReturnReasonMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICReturnReason 
		Inherits esICReturnReason
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICReturnReasonMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICReturnReasonMetadata.ColumnNames.ReturnReasonID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICReturnReasonMetadata.PropertyNames.ReturnReasonID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICReturnReasonMetadata.ColumnNames.ReturnReason, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICReturnReasonMetadata.PropertyNames.ReturnReason
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICReturnReasonMetadata.ColumnNames.IsActive, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICReturnReasonMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICReturnReasonMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ReturnReasonID As String = "ReturnReasonID"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ReturnReasonID As String = "ReturnReasonID"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICReturnReasonMetadata)
			
				If ICReturnReasonMetadata.mapDelegates Is Nothing Then
					ICReturnReasonMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICReturnReasonMetadata._meta Is Nothing Then
					ICReturnReasonMetadata._meta = New ICReturnReasonMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ReturnReasonID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReturnReason", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_ReturnReason"
				meta.Destination = "IC_ReturnReason"
				
				meta.spInsert = "proc_IC_ReturnReasonInsert"
				meta.spUpdate = "proc_IC_ReturnReasonUpdate"
				meta.spDelete = "proc_IC_ReturnReasonDelete"
				meta.spLoadAll = "proc_IC_ReturnReasonLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ReturnReasonLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICReturnReasonMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
