
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionAccounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionAccounts))> _
	<XmlType("ICCollectionAccounts")> _	
	Partial Public Class ICCollectionAccounts 
		Inherits esICCollectionAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String)
			Dim obj As New ICCollectionAccounts()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionAccounts()
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionAccountsCollection")> _
	Partial Public Class ICCollectionAccountsCollection
		Inherits esICCollectionAccountsCollection
		Implements IEnumerable(Of ICCollectionAccounts)
	
		Public Function FindByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As ICCollectionAccounts
			Return MyBase.SingleOrDefault(Function(e) e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionAccounts))> _
		Public Class ICCollectionAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionAccountsCollectionWCFPacket) As ICCollectionAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionAccountsCollection) As ICCollectionAccountsCollectionWCFPacket
				Return New ICCollectionAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionAccountsQuery 
		Inherits esICCollectionAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionAccountsQuery) As String
			Return ICCollectionAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionAccountsQuery
			Return DirectCast(ICCollectionAccountsQuery.SerializeHelper.FromXml(query, GetType(ICCollectionAccountsQuery)), ICCollectionAccountsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim query As New ICCollectionAccountsQuery()
			query.Where(query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionAccounts.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.AccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionAccountsMetadata.ColumnNames.AccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionAccountsMetadata.ColumnNames.AccountTitle, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.AccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionAccountsMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionAccountsMetadata.ColumnNames.CreateBy, value) Then
					Me._UpToICUserByCreateBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreateBy")
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCollectionAccountsMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCollectionAccountsMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionAccounts.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionAccountsMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionAccountsMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreateBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "AccountTitle"
							Me.str().AccountTitle = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.CreateDate)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionAccountsMetadata.PropertyNames.IsActive)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.AccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountTitle = Nothing
					Else
						entity.AccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionAccountsCollection
		Inherits esEntityCollection(Of ICCollectionAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "AccountTitle" 
					Return Me.AccountTitle
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "IsActive" 
					Return Me.IsActive
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.AccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionAccountsMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionAccounts 
		Inherits esICCollectionAccounts
		
	
		#Region "ICCollectionAccountsCollectionNatureCollectionByAccountNumber - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCollectionAccountsCollectionNatureCollectionByAccountNumber() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCollectionAccounts.ICCollectionAccountsCollectionNatureCollectionByAccountNumber_Delegate)
				map.PropertyName = "ICCollectionAccountsCollectionNatureCollectionByAccountNumber"
				map.MyColumnName = "AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICCollectionAccountsCollectionNatureCollectionByAccountNumber_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCollectionAccountsQuery(data.NextAlias())
			
			Dim mee As ICCollectionAccountsCollectionNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCollectionAccountsCollectionNatureQuery), New ICCollectionAccountsCollectionNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CollectionAccountsCollectionNature_IC_CollectionAccounts
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCollectionAccountsCollectionNatureCollectionByAccountNumber As ICCollectionAccountsCollectionNatureCollection 
		
			Get
				If Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber Is Nothing Then
					Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber = New ICCollectionAccountsCollectionNatureCollection()
					Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCollectionAccountsCollectionNatureCollectionByAccountNumber", Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber)
				
					If Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.AccountNumber = Me.AccountNumber)
							Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.BranchCode = Me.BranchCode)
							Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.Where(Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.Currency = Me.Currency)
							Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber.fks.Add(ICCollectionAccountsCollectionNatureMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber
			End Get
			
			Set(ByVal value As ICCollectionAccountsCollectionNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber Is Nothing Then

					Me.RemovePostSave("ICCollectionAccountsCollectionNatureCollectionByAccountNumber")
					Me._ICCollectionAccountsCollectionNatureCollectionByAccountNumber = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCollectionAccountsCollectionNatureCollectionByAccountNumber As ICCollectionAccountsCollectionNatureCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccounts_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreateBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionAccounts_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreateBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreateBy Is Nothing _
						 AndAlso Not CreateBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreateBy = New ICUser()
					Me._UpToICUserByCreateBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreateBy", Me._UpToICUserByCreateBy)
					Me._UpToICUserByCreateBy.Query.Where(Me._UpToICUserByCreateBy.Query.UserID = Me.CreateBy)
					Me._UpToICUserByCreateBy.Query.Load()
				End If

				Return Me._UpToICUserByCreateBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreateBy")
				

				If value Is Nothing Then
				
					Me.CreateBy = Nothing
				
					Me._UpToICUserByCreateBy = Nothing
				Else
				
					Me.CreateBy = value.UserID
					
					Me._UpToICUserByCreateBy = value
					Me.SetPreSave("UpToICUserByCreateBy", Me._UpToICUserByCreateBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICCollectionAccountsCollectionNatureCollectionByAccountNumber"
					coll = Me.ICCollectionAccountsCollectionNatureCollectionByAccountNumber
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICCollectionAccountsCollectionNatureCollectionByAccountNumber", GetType(ICCollectionAccountsCollectionNatureCollection), New ICCollectionAccountsCollectionNature()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.AccountNumber, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.BranchCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.Currency, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.AccountTitle, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.AccountTitle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.CompanyCode, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionAccountsMetadata.ColumnNames.IsActive, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionAccountsMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "isActive"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const AccountTitle As String = "AccountTitle"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const IsActive As String = "IsActive"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionAccountsMetadata)
			
				If ICCollectionAccountsMetadata.mapDelegates Is Nothing Then
					ICCollectionAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionAccountsMetadata._meta Is Nothing Then
					ICCollectionAccountsMetadata._meta = New ICCollectionAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_CollectionAccounts"
				meta.Destination = "IC_CollectionAccounts"
				
				meta.spInsert = "proc_IC_CollectionAccountsInsert"
				meta.spUpdate = "proc_IC_CollectionAccountsUpdate"
				meta.spDelete = "proc_IC_CollectionAccountsDelete"
				meta.spLoadAll = "proc_IC_CollectionAccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionAccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
