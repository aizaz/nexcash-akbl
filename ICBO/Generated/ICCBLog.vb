
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CBLog' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCBLog))> _
	<XmlType("ICCBLog")> _	
	Partial Public Class ICCBLog 
		Inherits esICCBLog
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCBLog()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal logId As System.Int64)
			Dim obj As New ICCBLog()
			obj.LogId = logId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal logId As System.Int64, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCBLog()
			obj.LogId = logId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCBLogCollection")> _
	Partial Public Class ICCBLogCollection
		Inherits esICCBLogCollection
		Implements IEnumerable(Of ICCBLog)
	
		Public Function FindByPrimaryKey(ByVal logId As System.Int64) As ICCBLog
			Return MyBase.SingleOrDefault(Function(e) e.LogId.HasValue AndAlso e.LogId.Value = logId)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCBLog))> _
		Public Class ICCBLogCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCBLogCollection)
			
			Public Shared Widening Operator CType(packet As ICCBLogCollectionWCFPacket) As ICCBLogCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCBLogCollection) As ICCBLogCollectionWCFPacket
				Return New ICCBLogCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCBLogQuery 
		Inherits esICCBLogQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCBLogQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCBLogQuery) As String
			Return ICCBLogQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCBLogQuery
			Return DirectCast(ICCBLogQuery.SerializeHelper.FromXml(query, GetType(ICCBLogQuery)), ICCBLogQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCBLog
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal logId As System.Int64) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(logId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(logId)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal logId As System.Int64) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(logId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(logId)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal logId As System.Int64) As Boolean
		
			Dim query As New ICCBLogQuery()
			query.Where(query.LogId = logId)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal logId As System.Int64) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("LogId", logId)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CBLog.LogId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LogId As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICCBLogMetadata.ColumnNames.LogId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICCBLogMetadata.ColumnNames.LogId, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.LogId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.LogDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LogDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCBLogMetadata.ColumnNames.LogDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCBLogMetadata.ColumnNames.LogDateTime, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.LogDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.EntityType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EntityType As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.EntityType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.EntityType, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.EntityType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.RelatedId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedId As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.RelatedId)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.RelatedId, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.RelatedId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.ErrorDesc
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ErrorDesc As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.ErrorDesc)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.ErrorDesc, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.ErrorDesc)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.RetryCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RetryCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCBLogMetadata.ColumnNames.RetryCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCBLogMetadata.ColumnNames.RetryCount, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.RetryCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.fromaccountnumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Fromaccountnumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Fromaccountnumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Fromaccountnumber, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Fromaccountnumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.fromaccounttype
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Fromaccounttype As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Fromaccounttype)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Fromaccounttype, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Fromaccounttype)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.toaccountnumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Toaccountnumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Toaccountnumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Toaccountnumber, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Toaccountnumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.toaccounttype
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Toaccounttype As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Toaccounttype)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Toaccounttype, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Toaccounttype)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.frombankIMD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FrombankIMD As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.FrombankIMD)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.FrombankIMD, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.FrombankIMD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.tobankIMD
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TobankIMD As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.TobankIMD)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.TobankIMD, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.TobankIMD)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.transactionamount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Transactionamount As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.Transactionamount)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.Transactionamount, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.Transactionamount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBLog.MessageType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MessageType As System.String
			Get
				Return MyBase.GetSystemString(ICCBLogMetadata.ColumnNames.MessageType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBLogMetadata.ColumnNames.MessageType, value) Then
					OnPropertyChanged(ICCBLogMetadata.PropertyNames.MessageType)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "LogId"
							Me.str().LogId = CType(value, string)
												
						Case "LogDateTime"
							Me.str().LogDateTime = CType(value, string)
												
						Case "EntityType"
							Me.str().EntityType = CType(value, string)
												
						Case "RelatedId"
							Me.str().RelatedId = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "ErrorDesc"
							Me.str().ErrorDesc = CType(value, string)
												
						Case "RetryCount"
							Me.str().RetryCount = CType(value, string)
												
						Case "Fromaccountnumber"
							Me.str().Fromaccountnumber = CType(value, string)
												
						Case "Fromaccounttype"
							Me.str().Fromaccounttype = CType(value, string)
												
						Case "Toaccountnumber"
							Me.str().Toaccountnumber = CType(value, string)
												
						Case "Toaccounttype"
							Me.str().Toaccounttype = CType(value, string)
												
						Case "FrombankIMD"
							Me.str().FrombankIMD = CType(value, string)
												
						Case "TobankIMD"
							Me.str().TobankIMD = CType(value, string)
												
						Case "Transactionamount"
							Me.str().Transactionamount = CType(value, string)
												
						Case "MessageType"
							Me.str().MessageType = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "LogId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.LogId = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICCBLogMetadata.PropertyNames.LogId)
							End If
						
						Case "LogDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LogDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCBLogMetadata.PropertyNames.LogDateTime)
							End If
						
						Case "RetryCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RetryCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCBLogMetadata.PropertyNames.RetryCount)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCBLog)
				Me.entity = entity
			End Sub				
		
	
			Public Property LogId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.LogId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LogId = Nothing
					Else
						entity.LogId = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  	
			Public Property LogDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LogDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LogDateTime = Nothing
					Else
						entity.LogDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property EntityType As System.String 
				Get
					Dim data_ As System.String = entity.EntityType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EntityType = Nothing
					Else
						entity.EntityType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedId As System.String 
				Get
					Dim data_ As System.String = entity.RelatedId
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedId = Nothing
					Else
						entity.RelatedId = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As System.String = entity.Status
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ErrorDesc As System.String 
				Get
					Dim data_ As System.String = entity.ErrorDesc
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ErrorDesc = Nothing
					Else
						entity.ErrorDesc = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RetryCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RetryCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RetryCount = Nothing
					Else
						entity.RetryCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Fromaccountnumber As System.String 
				Get
					Dim data_ As System.String = entity.Fromaccountnumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Fromaccountnumber = Nothing
					Else
						entity.Fromaccountnumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Fromaccounttype As System.String 
				Get
					Dim data_ As System.String = entity.Fromaccounttype
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Fromaccounttype = Nothing
					Else
						entity.Fromaccounttype = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Toaccountnumber As System.String 
				Get
					Dim data_ As System.String = entity.Toaccountnumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Toaccountnumber = Nothing
					Else
						entity.Toaccountnumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Toaccounttype As System.String 
				Get
					Dim data_ As System.String = entity.Toaccounttype
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Toaccounttype = Nothing
					Else
						entity.Toaccounttype = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FrombankIMD As System.String 
				Get
					Dim data_ As System.String = entity.FrombankIMD
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FrombankIMD = Nothing
					Else
						entity.FrombankIMD = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TobankIMD As System.String 
				Get
					Dim data_ As System.String = entity.TobankIMD
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TobankIMD = Nothing
					Else
						entity.TobankIMD = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Transactionamount As System.String 
				Get
					Dim data_ As System.String = entity.Transactionamount
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Transactionamount = Nothing
					Else
						entity.Transactionamount = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MessageType As System.String 
				Get
					Dim data_ As System.String = entity.MessageType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MessageType = Nothing
					Else
						entity.MessageType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCBLog
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBLogMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCBLogQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBLogQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCBLogQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCBLogQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCBLogQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCBLogCollection
		Inherits esEntityCollection(Of ICCBLog)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBLogMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCBLogCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCBLogQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBLogQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCBLogQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCBLogQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCBLogQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCBLogQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCBLogQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCBLogQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCBLogMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "LogId" 
					Return Me.LogId
				Case "LogDateTime" 
					Return Me.LogDateTime
				Case "EntityType" 
					Return Me.EntityType
				Case "RelatedId" 
					Return Me.RelatedId
				Case "Status" 
					Return Me.Status
				Case "ErrorDesc" 
					Return Me.ErrorDesc
				Case "RetryCount" 
					Return Me.RetryCount
				Case "Fromaccountnumber" 
					Return Me.Fromaccountnumber
				Case "Fromaccounttype" 
					Return Me.Fromaccounttype
				Case "Toaccountnumber" 
					Return Me.Toaccountnumber
				Case "Toaccounttype" 
					Return Me.Toaccounttype
				Case "FrombankIMD" 
					Return Me.FrombankIMD
				Case "TobankIMD" 
					Return Me.TobankIMD
				Case "Transactionamount" 
					Return Me.Transactionamount
				Case "MessageType" 
					Return Me.MessageType
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property LogId As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.LogId, esSystemType.Int64)
			End Get
		End Property 
		
		Public ReadOnly Property LogDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.LogDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property EntityType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.EntityType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedId As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.RelatedId, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Status, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ErrorDesc As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.ErrorDesc, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RetryCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.RetryCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Fromaccountnumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Fromaccountnumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Fromaccounttype As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Fromaccounttype, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Toaccountnumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Toaccountnumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Toaccounttype As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Toaccounttype, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FrombankIMD As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.FrombankIMD, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TobankIMD As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.TobankIMD, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Transactionamount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.Transactionamount, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MessageType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBLogMetadata.ColumnNames.MessageType, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCBLog 
		Inherits esICCBLog
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCBLogMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.LogId, 0, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.LogId
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 19
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.LogDateTime, 1, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.LogDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.EntityType, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.EntityType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.RelatedId, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.RelatedId
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Status, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Status
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.ErrorDesc, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.ErrorDesc
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.RetryCount, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.RetryCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Fromaccountnumber, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Fromaccountnumber
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Fromaccounttype, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Fromaccounttype
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Toaccountnumber, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Toaccountnumber
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Toaccounttype, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Toaccounttype
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.FrombankIMD, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.FrombankIMD
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.TobankIMD, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.TobankIMD
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.Transactionamount, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.Transactionamount
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBLogMetadata.ColumnNames.MessageType, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBLogMetadata.PropertyNames.MessageType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCBLogMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const LogId As String = "LogId"
			 Public Const LogDateTime As String = "LogDateTime"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedId As String = "RelatedId"
			 Public Const Status As String = "Status"
			 Public Const ErrorDesc As String = "ErrorDesc"
			 Public Const RetryCount As String = "RetryCount"
			 Public Const Fromaccountnumber As String = "fromaccountnumber"
			 Public Const Fromaccounttype As String = "fromaccounttype"
			 Public Const Toaccountnumber As String = "toaccountnumber"
			 Public Const Toaccounttype As String = "toaccounttype"
			 Public Const FrombankIMD As String = "frombankIMD"
			 Public Const TobankIMD As String = "tobankIMD"
			 Public Const Transactionamount As String = "transactionamount"
			 Public Const MessageType As String = "MessageType"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const LogId As String = "LogId"
			 Public Const LogDateTime As String = "LogDateTime"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedId As String = "RelatedId"
			 Public Const Status As String = "Status"
			 Public Const ErrorDesc As String = "ErrorDesc"
			 Public Const RetryCount As String = "RetryCount"
			 Public Const Fromaccountnumber As String = "Fromaccountnumber"
			 Public Const Fromaccounttype As String = "Fromaccounttype"
			 Public Const Toaccountnumber As String = "Toaccountnumber"
			 Public Const Toaccounttype As String = "Toaccounttype"
			 Public Const FrombankIMD As String = "FrombankIMD"
			 Public Const TobankIMD As String = "TobankIMD"
			 Public Const Transactionamount As String = "Transactionamount"
			 Public Const MessageType As String = "MessageType"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCBLogMetadata)
			
				If ICCBLogMetadata.mapDelegates Is Nothing Then
					ICCBLogMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCBLogMetadata._meta Is Nothing Then
					ICCBLogMetadata._meta = New ICCBLogMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("LogId", new esTypeMap("bigint", "System.Int64"))
				meta.AddTypeMap("LogDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("EntityType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RelatedId", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ErrorDesc", new esTypeMap("text", "System.String"))
				meta.AddTypeMap("RetryCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Fromaccountnumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Fromaccounttype", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Toaccountnumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Toaccounttype", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FrombankIMD", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TobankIMD", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Transactionamount", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MessageType", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CBLog"
				meta.Destination = "IC_CBLog"
				
				meta.spInsert = "proc_IC_CBLogInsert"
				meta.spUpdate = "proc_IC_CBLogUpdate"
				meta.spDelete = "proc_IC_CBLogDelete"
				meta.spLoadAll = "proc_IC_CBLogLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CBLogLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCBLogMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
