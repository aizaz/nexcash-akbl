
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/30/2015 12:26:05 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PaymentNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPaymentNature))> _
	<XmlType("ICPaymentNature")> _	
	Partial Public Class ICPaymentNature 
		Inherits esICPaymentNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPaymentNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal paymentNatureCode As System.String)
			Dim obj As New ICPaymentNature()
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal paymentNatureCode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPaymentNature()
			obj.PaymentNatureCode = paymentNatureCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPaymentNatureCollection")> _
	Partial Public Class ICPaymentNatureCollection
		Inherits esICPaymentNatureCollection
		Implements IEnumerable(Of ICPaymentNature)
	
		Public Function FindByPrimaryKey(ByVal paymentNatureCode As System.String) As ICPaymentNature
			Return MyBase.SingleOrDefault(Function(e) e.PaymentNatureCode = paymentNatureCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPaymentNature))> _
		Public Class ICPaymentNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPaymentNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICPaymentNatureCollectionWCFPacket) As ICPaymentNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPaymentNatureCollection) As ICPaymentNatureCollectionWCFPacket
				Return New ICPaymentNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPaymentNatureQuery 
		Inherits esICPaymentNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPaymentNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPaymentNatureQuery) As String
			Return ICPaymentNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPaymentNatureQuery
			Return DirectCast(ICPaymentNatureQuery.SerializeHelper.FromXml(query, GetType(ICPaymentNatureQuery)), ICPaymentNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPaymentNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal paymentNatureCode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(paymentNatureCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal paymentNatureCode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(paymentNatureCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(paymentNatureCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal paymentNatureCode As System.String) As Boolean
		
			Dim query As New ICPaymentNatureQuery()
			query.Where(query.PaymentNatureCode = paymentNatureCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal paymentNatureCode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("PaymentNatureCode", paymentNatureCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PaymentNature.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICPaymentNatureMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPaymentNatureMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.PaymentNatureName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureName As System.String
			Get
				Return MyBase.GetSystemString(ICPaymentNatureMetadata.ColumnNames.PaymentNatureName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPaymentNatureMetadata.ColumnNames.PaymentNatureName, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.PaymentNatureName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPaymentNatureMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPaymentNatureMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPaymentNatureMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPaymentNatureMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPaymentNatureMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPaymentNatureMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PaymentNature.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPaymentNatureMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPaymentNatureMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "PaymentNatureName"
							Me.str().PaymentNatureName = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreateDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.IsActive)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPaymentNatureMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPaymentNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureName As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureName = Nothing
					Else
						entity.PaymentNatureName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPaymentNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPaymentNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPaymentNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPaymentNatureCollection
		Inherits esEntityCollection(Of ICPaymentNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPaymentNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPaymentNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPaymentNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPaymentNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPaymentNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPaymentNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPaymentNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "PaymentNatureName" 
					Return Me.PaymentNatureName
				Case "CreateDate" 
					Return Me.CreateDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "IsActive" 
					Return Me.IsActive
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureName As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.PaymentNatureName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPaymentNatureMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPaymentNature 
		Inherits esICPaymentNature
		
	
		#Region "ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICAccountPaymentNatureTemplateQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountPaymentNatureTemplateQuery), New ICAccountPaymentNatureTemplateQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountPaymentNatureTemplate_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode As ICAccountPaymentNatureTemplateCollection 
		
			Get
				If Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode Is Nothing Then
					Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode = New ICAccountPaymentNatureTemplateCollection()
					Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode", Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode.Query.Where(Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode.fks.Add(ICAccountPaymentNatureTemplateMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICAccountPaymentNatureTemplateCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode")
					Me._ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode As ICAccountPaymentNatureTemplateCollection
		#End Region

		#Region "ICAccountsPaymentNatureCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAccountsPaymentNatureCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICAccountsPaymentNatureCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICAccountsPaymentNatureCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAccountsPaymentNatureCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICAccountsPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAccountsPaymentNatureQuery), New ICAccountsPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_AccountsPaymentNature_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAccountsPaymentNatureCollectionByPaymentNatureCode As ICAccountsPaymentNatureCollection 
		
			Get
				If Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode Is Nothing Then
					Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode = New ICAccountsPaymentNatureCollection()
					Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAccountsPaymentNatureCollectionByPaymentNatureCode", Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode.Query.Where(Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode.fks.Add(ICAccountsPaymentNatureMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICAccountsPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICAccountsPaymentNatureCollectionByPaymentNatureCode")
					Me._ICAccountsPaymentNatureCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAccountsPaymentNatureCollectionByPaymentNatureCode As ICAccountsPaymentNatureCollection
		#End Region

		#Region "ICApprovalRuleCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICApprovalRuleCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICApprovalRuleCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleQuery), New ICApprovalRuleQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRule_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleCollectionByPaymentNatureCode As ICApprovalRuleCollection 
		
			Get
				If Me._ICApprovalRuleCollectionByPaymentNatureCode Is Nothing Then
					Me._ICApprovalRuleCollectionByPaymentNatureCode = New ICApprovalRuleCollection()
					Me._ICApprovalRuleCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleCollectionByPaymentNatureCode", Me._ICApprovalRuleCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleCollectionByPaymentNatureCode.Query.Where(Me._ICApprovalRuleCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICApprovalRuleCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleCollectionByPaymentNatureCode.fks.Add(ICApprovalRuleMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICApprovalRuleCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICApprovalRuleCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleCollectionByPaymentNatureCode")
					Me._ICApprovalRuleCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleCollectionByPaymentNatureCode As ICApprovalRuleCollection
		#End Region

		#Region "ICBeneCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICBeneCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICBeneCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICBeneQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneQuery), New ICBeneQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Bene_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneCollectionByPaymentNatureCode As ICBeneCollection 
		
			Get
				If Me._ICBeneCollectionByPaymentNatureCode Is Nothing Then
					Me._ICBeneCollectionByPaymentNatureCode = New ICBeneCollection()
					Me._ICBeneCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneCollectionByPaymentNatureCode", Me._ICBeneCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneCollectionByPaymentNatureCode.Query.Where(Me._ICBeneCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICBeneCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneCollectionByPaymentNatureCode.fks.Add(ICBeneMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICBeneCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICBeneCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICBeneCollectionByPaymentNatureCode")
					Me._ICBeneCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneCollectionByPaymentNatureCode As ICBeneCollection
		#End Region

		#Region "ICBeneGroupCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBeneGroupCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICBeneGroupCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICBeneGroupCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBeneGroupCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICBeneGroupQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBeneGroupQuery), New ICBeneGroupQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_BeneGroup_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBeneGroupCollectionByPaymentNatureCode As ICBeneGroupCollection 
		
			Get
				If Me._ICBeneGroupCollectionByPaymentNatureCode Is Nothing Then
					Me._ICBeneGroupCollectionByPaymentNatureCode = New ICBeneGroupCollection()
					Me._ICBeneGroupCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBeneGroupCollectionByPaymentNatureCode", Me._ICBeneGroupCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBeneGroupCollectionByPaymentNatureCode.Query.Where(Me._ICBeneGroupCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICBeneGroupCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBeneGroupCollectionByPaymentNatureCode.fks.Add(ICBeneGroupMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICBeneGroupCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICBeneGroupCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBeneGroupCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICBeneGroupCollectionByPaymentNatureCode")
					Me._ICBeneGroupCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBeneGroupCollectionByPaymentNatureCode As ICBeneGroupCollection
		#End Region

		#Region "ICPaymentNatureAndRolesCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPaymentNatureAndRolesCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICPaymentNatureAndRolesCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICPaymentNatureAndRolesCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPaymentNatureAndRolesCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICPaymentNatureAndRolesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPaymentNatureAndRolesQuery), New ICPaymentNatureAndRolesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PaymentNatureAndRoles_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPaymentNatureAndRolesCollectionByPaymentNatureCode As ICPaymentNatureAndRolesCollection 
		
			Get
				If Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode Is Nothing Then
					Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode = New ICPaymentNatureAndRolesCollection()
					Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPaymentNatureAndRolesCollectionByPaymentNatureCode", Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode.Query.Where(Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode.fks.Add(ICPaymentNatureAndRolesMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICPaymentNatureAndRolesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICPaymentNatureAndRolesCollectionByPaymentNatureCode")
					Me._ICPaymentNatureAndRolesCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPaymentNatureAndRolesCollectionByPaymentNatureCode As ICPaymentNatureAndRolesCollection
		#End Region

		#Region "ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICPaymentNature.ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode_Delegate)
				map.PropertyName = "ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode"
				map.MyColumnName = "PaymentNatureCode"
				map.ParentColumnName = "PaymentNatureCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICUserRolesAccountAndPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesAccountAndPaymentNatureQuery), New ICUserRolesAccountAndPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.PaymentNatureCode = mee.PaymentNatureCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode As ICUserRolesAccountAndPaymentNatureCollection 
		
			Get
				If Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode Is Nothing Then
					Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode = New ICUserRolesAccountAndPaymentNatureCollection()
					Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode", Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode)
				
					If Not Me.PaymentNatureCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
					End If
				End If

				Return Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode
			End Get
			
			Set(ByVal value As ICUserRolesAccountAndPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode Is Nothing Then

					Me.RemovePostSave("ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode")
					Me._ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode As ICUserRolesAccountAndPaymentNatureCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode"
					coll = Me.ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode
					Exit Select
				Case "ICAccountsPaymentNatureCollectionByPaymentNatureCode"
					coll = Me.ICAccountsPaymentNatureCollectionByPaymentNatureCode
					Exit Select
				Case "ICApprovalRuleCollectionByPaymentNatureCode"
					coll = Me.ICApprovalRuleCollectionByPaymentNatureCode
					Exit Select
				Case "ICBeneCollectionByPaymentNatureCode"
					coll = Me.ICBeneCollectionByPaymentNatureCode
					Exit Select
				Case "ICBeneGroupCollectionByPaymentNatureCode"
					coll = Me.ICBeneGroupCollectionByPaymentNatureCode
					Exit Select
				Case "ICPaymentNatureAndRolesCollectionByPaymentNatureCode"
					coll = Me.ICPaymentNatureAndRolesCollectionByPaymentNatureCode
					Exit Select
				Case "ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode"
					coll = Me.ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAccountPaymentNatureTemplateCollectionByPaymentNatureCode", GetType(ICAccountPaymentNatureTemplateCollection), New ICAccountPaymentNatureTemplate()))
			props.Add(new esPropertyDescriptor(Me, "ICAccountsPaymentNatureCollectionByPaymentNatureCode", GetType(ICAccountsPaymentNatureCollection), New ICAccountsPaymentNature()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleCollectionByPaymentNatureCode", GetType(ICApprovalRuleCollection), New ICApprovalRule()))
			props.Add(new esPropertyDescriptor(Me, "ICBeneCollectionByPaymentNatureCode", GetType(ICBeneCollection), New ICBene()))
			props.Add(new esPropertyDescriptor(Me, "ICBeneGroupCollectionByPaymentNatureCode", GetType(ICBeneGroupCollection), New ICBeneGroup()))
			props.Add(new esPropertyDescriptor(Me, "ICPaymentNatureAndRolesCollectionByPaymentNatureCode", GetType(ICPaymentNatureAndRolesCollection), New ICPaymentNatureAndRoles()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesAccountAndPaymentNatureCollectionByPaymentNatureCode", GetType(ICUserRolesAccountAndPaymentNatureCollection), New ICUserRolesAccountAndPaymentNature()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICPaymentNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.PaymentNatureCode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.PaymentNatureName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.PaymentNatureName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.CreateDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.CreatedBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.Creater, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPaymentNatureMetadata.ColumnNames.CreationDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPaymentNatureMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPaymentNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const PaymentNatureName As String = "PaymentNatureName"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const PaymentNatureName As String = "PaymentNatureName"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const IsActive As String = "IsActive"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPaymentNatureMetadata)
			
				If ICPaymentNatureMetadata.mapDelegates Is Nothing Then
					ICPaymentNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPaymentNatureMetadata._meta Is Nothing Then
					ICPaymentNatureMetadata._meta = New ICPaymentNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_PaymentNature"
				meta.Destination = "IC_PaymentNature"
				
				meta.spInsert = "proc_IC_PaymentNatureInsert"
				meta.spUpdate = "proc_IC_PaymentNatureUpdate"
				meta.spDelete = "proc_IC_PaymentNatureDelete"
				meta.spLoadAll = "proc_IC_PaymentNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PaymentNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPaymentNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
