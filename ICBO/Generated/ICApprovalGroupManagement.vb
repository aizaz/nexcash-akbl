
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/20/2015 12:16:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ApprovalGroupManagement' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICApprovalGroupManagement))> _
	<XmlType("ICApprovalGroupManagement")> _	
	Partial Public Class ICApprovalGroupManagement 
		Inherits esICApprovalGroupManagement
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICApprovalGroupManagement()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal approvalGroupID As System.Int32)
			Dim obj As New ICApprovalGroupManagement()
			obj.ApprovalGroupID = approvalGroupID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal approvalGroupID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICApprovalGroupManagement()
			obj.ApprovalGroupID = approvalGroupID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICApprovalGroupManagementCollection")> _
	Partial Public Class ICApprovalGroupManagementCollection
		Inherits esICApprovalGroupManagementCollection
		Implements IEnumerable(Of ICApprovalGroupManagement)
	
		Public Function FindByPrimaryKey(ByVal approvalGroupID As System.Int32) As ICApprovalGroupManagement
			Return MyBase.SingleOrDefault(Function(e) e.ApprovalGroupID.HasValue AndAlso e.ApprovalGroupID.Value = approvalGroupID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICApprovalGroupManagement))> _
		Public Class ICApprovalGroupManagementCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICApprovalGroupManagementCollection)
			
			Public Shared Widening Operator CType(packet As ICApprovalGroupManagementCollectionWCFPacket) As ICApprovalGroupManagementCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICApprovalGroupManagementCollection) As ICApprovalGroupManagementCollectionWCFPacket
				Return New ICApprovalGroupManagementCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICApprovalGroupManagementQuery 
		Inherits esICApprovalGroupManagementQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICApprovalGroupManagementQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICApprovalGroupManagementQuery) As String
			Return ICApprovalGroupManagementQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICApprovalGroupManagementQuery
			Return DirectCast(ICApprovalGroupManagementQuery.SerializeHelper.FromXml(query, GetType(ICApprovalGroupManagementQuery)), ICApprovalGroupManagementQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICApprovalGroupManagement
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal approvalGroupID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalGroupID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalGroupID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal approvalGroupID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalGroupID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalGroupID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal approvalGroupID As System.Int32) As Boolean
		
			Dim query As New ICApprovalGroupManagementQuery()
			query.Where(query.ApprovalGroupID = approvalGroupID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal approvalGroupID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ApprovalGroupID", approvalGroupID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.ApprovalGroupID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalGroupID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.ApprovalGroupID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.ApprovalGroupID, value) Then
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.ApprovalGroupID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.GroupName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupName As System.String
			Get
				Return MyBase.GetSystemString(ICApprovalGroupManagementMetadata.ColumnNames.GroupName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICApprovalGroupManagementMetadata.ColumnNames.GroupName, value) Then
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.GroupName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICApprovalGroupManagementMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICApprovalGroupManagementMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalGroupManagementMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalGroupManagement.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalGroupManagementMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalGroupManagementMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ApprovalGroupID"
							Me.str().ApprovalGroupID = CType(value, string)
												
						Case "GroupName"
							Me.str().GroupName = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ApprovalGroupID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalGroupID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.ApprovalGroupID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.IsActive)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalGroupManagementMetadata.PropertyNames.CreateDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICApprovalGroupManagement)
				Me.entity = entity
			End Sub				
		
	
			Public Property ApprovalGroupID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalGroupID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalGroupID = Nothing
					Else
						entity.ApprovalGroupID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupName As System.String 
				Get
					Dim data_ As System.String = entity.GroupName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupName = Nothing
					Else
						entity.GroupName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICApprovalGroupManagement
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalGroupManagementMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICApprovalGroupManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalGroupManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICApprovalGroupManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICApprovalGroupManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICApprovalGroupManagementQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICApprovalGroupManagementCollection
		Inherits esEntityCollection(Of ICApprovalGroupManagement)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalGroupManagementMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICApprovalGroupManagementCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICApprovalGroupManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalGroupManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICApprovalGroupManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICApprovalGroupManagementQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICApprovalGroupManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICApprovalGroupManagementQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICApprovalGroupManagementQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICApprovalGroupManagementQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICApprovalGroupManagementMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ApprovalGroupID" 
					Return Me.ApprovalGroupID
				Case "GroupName" 
					Return Me.GroupName
				Case "IsActive" 
					Return Me.IsActive
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreateDate" 
					Return Me.CreateDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ApprovalGroupID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.ApprovalGroupID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupName As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.GroupName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalGroupManagementMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICApprovalGroupManagement 
		Inherits esICApprovalGroupManagement
		
	
		#Region "ICApprovalGroupUsersCollectionByApprovalGroupID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalGroupUsersCollectionByApprovalGroupID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICApprovalGroupManagement.ICApprovalGroupUsersCollectionByApprovalGroupID_Delegate)
				map.PropertyName = "ICApprovalGroupUsersCollectionByApprovalGroupID"
				map.MyColumnName = "ApprovalGroupID"
				map.ParentColumnName = "ApprovalGroupID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalGroupUsersCollectionByApprovalGroupID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICApprovalGroupManagementQuery(data.NextAlias())
			
			Dim mee As ICApprovalGroupUsersQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalGroupUsersQuery), New ICApprovalGroupUsersQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ApprovalGroupID = mee.ApprovalGroupID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalGroupUsers_IC_ApprovalGroupManagement
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalGroupUsersCollectionByApprovalGroupID As ICApprovalGroupUsersCollection 
		
			Get
				If Me._ICApprovalGroupUsersCollectionByApprovalGroupID Is Nothing Then
					Me._ICApprovalGroupUsersCollectionByApprovalGroupID = New ICApprovalGroupUsersCollection()
					Me._ICApprovalGroupUsersCollectionByApprovalGroupID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalGroupUsersCollectionByApprovalGroupID", Me._ICApprovalGroupUsersCollectionByApprovalGroupID)
				
					If Not Me.ApprovalGroupID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalGroupUsersCollectionByApprovalGroupID.Query.Where(Me._ICApprovalGroupUsersCollectionByApprovalGroupID.Query.ApprovalGroupID = Me.ApprovalGroupID)
							Me._ICApprovalGroupUsersCollectionByApprovalGroupID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalGroupUsersCollectionByApprovalGroupID.fks.Add(ICApprovalGroupUsersMetadata.ColumnNames.ApprovalGroupID, Me.ApprovalGroupID)
					End If
				End If

				Return Me._ICApprovalGroupUsersCollectionByApprovalGroupID
			End Get
			
			Set(ByVal value As ICApprovalGroupUsersCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalGroupUsersCollectionByApprovalGroupID Is Nothing Then

					Me.RemovePostSave("ICApprovalGroupUsersCollectionByApprovalGroupID")
					Me._ICApprovalGroupUsersCollectionByApprovalGroupID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalGroupUsersCollectionByApprovalGroupID As ICApprovalGroupUsersCollection
		#End Region

		#Region "ICApprovalRuleConditionsCollectionByApprovalGroupID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICApprovalRuleConditionsCollectionByApprovalGroupID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICApprovalGroupManagement.ICApprovalRuleConditionsCollectionByApprovalGroupID_Delegate)
				map.PropertyName = "ICApprovalRuleConditionsCollectionByApprovalGroupID"
				map.MyColumnName = "ApprovalGroupID"
				map.ParentColumnName = "ApprovalGroupID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICApprovalRuleConditionsCollectionByApprovalGroupID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICApprovalGroupManagementQuery(data.NextAlias())
			
			Dim mee As ICApprovalRuleConditionsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICApprovalRuleConditionsQuery), New ICApprovalRuleConditionsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.ApprovalGroupID = mee.ApprovalGroupID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_ApprovalRuleConditions_IC_ApprovalGroupManagement
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICApprovalRuleConditionsCollectionByApprovalGroupID As ICApprovalRuleConditionsCollection 
		
			Get
				If Me._ICApprovalRuleConditionsCollectionByApprovalGroupID Is Nothing Then
					Me._ICApprovalRuleConditionsCollectionByApprovalGroupID = New ICApprovalRuleConditionsCollection()
					Me._ICApprovalRuleConditionsCollectionByApprovalGroupID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICApprovalRuleConditionsCollectionByApprovalGroupID", Me._ICApprovalRuleConditionsCollectionByApprovalGroupID)
				
					If Not Me.ApprovalGroupID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICApprovalRuleConditionsCollectionByApprovalGroupID.Query.Where(Me._ICApprovalRuleConditionsCollectionByApprovalGroupID.Query.ApprovalGroupID = Me.ApprovalGroupID)
							Me._ICApprovalRuleConditionsCollectionByApprovalGroupID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICApprovalRuleConditionsCollectionByApprovalGroupID.fks.Add(ICApprovalRuleConditionsMetadata.ColumnNames.ApprovalGroupID, Me.ApprovalGroupID)
					End If
				End If

				Return Me._ICApprovalRuleConditionsCollectionByApprovalGroupID
			End Get
			
			Set(ByVal value As ICApprovalRuleConditionsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICApprovalRuleConditionsCollectionByApprovalGroupID Is Nothing Then

					Me.RemovePostSave("ICApprovalRuleConditionsCollectionByApprovalGroupID")
					Me._ICApprovalRuleConditionsCollectionByApprovalGroupID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICApprovalRuleConditionsCollectionByApprovalGroupID As ICApprovalRuleConditionsCollection
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupManagement_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalGroupManagement_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICApprovalGroupUsersCollectionByApprovalGroupID"
					coll = Me.ICApprovalGroupUsersCollectionByApprovalGroupID
					Exit Select
				Case "ICApprovalRuleConditionsCollectionByApprovalGroupID"
					coll = Me.ICApprovalRuleConditionsCollectionByApprovalGroupID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICApprovalGroupUsersCollectionByApprovalGroupID", GetType(ICApprovalGroupUsersCollection), New ICApprovalGroupUsers()))
			props.Add(new esPropertyDescriptor(Me, "ICApprovalRuleConditionsCollectionByApprovalGroupID", GetType(ICApprovalRuleConditionsCollection), New ICApprovalRuleConditions()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICApprovalGroupUsersCollectionByApprovalGroupID Is Nothing Then
				Apply(Me._ICApprovalGroupUsersCollectionByApprovalGroupID, "ApprovalGroupID", Me.ApprovalGroupID)
			End If
			
			If Not Me._ICApprovalRuleConditionsCollectionByApprovalGroupID Is Nothing Then
				Apply(Me._ICApprovalRuleConditionsCollectionByApprovalGroupID, "ApprovalGroupID", Me.ApprovalGroupID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICApprovalGroupManagementMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.ApprovalGroupID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.ApprovalGroupID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.GroupName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.GroupName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.IsActive, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.CompanyCode, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.CreatedBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalGroupManagementMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalGroupManagementMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICApprovalGroupManagementMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const GroupName As String = "GroupName"
			 Public Const IsActive As String = "IsActive"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreateDate As String = "CreateDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ApprovalGroupID As String = "ApprovalGroupID"
			 Public Const GroupName As String = "GroupName"
			 Public Const IsActive As String = "IsActive"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreateDate As String = "CreateDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICApprovalGroupManagementMetadata)
			
				If ICApprovalGroupManagementMetadata.mapDelegates Is Nothing Then
					ICApprovalGroupManagementMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICApprovalGroupManagementMetadata._meta Is Nothing Then
					ICApprovalGroupManagementMetadata._meta = New ICApprovalGroupManagementMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ApprovalGroupID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_ApprovalGroupManagement"
				meta.Destination = "IC_ApprovalGroupManagement"
				
				meta.spInsert = "proc_IC_ApprovalGroupManagementInsert"
				meta.spUpdate = "proc_IC_ApprovalGroupManagementUpdate"
				meta.spDelete = "proc_IC_ApprovalGroupManagementDelete"
				meta.spLoadAll = "proc_IC_ApprovalGroupManagementLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ApprovalGroupManagementLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICApprovalGroupManagementMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
