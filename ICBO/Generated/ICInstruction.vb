
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 9/18/2015 12:45:33 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Instruction' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICInstruction))> _
	<XmlType("ICInstruction")> _	
	Partial Public Class ICInstruction 
		Inherits esICInstruction
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICInstruction()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal instructionID As System.Int32)
			Dim obj As New ICInstruction()
			obj.InstructionID = instructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal instructionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICInstruction()
			obj.InstructionID = instructionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICInstructionCollection")> _
	Partial Public Class ICInstructionCollection
		Inherits esICInstructionCollection
		Implements IEnumerable(Of ICInstruction)
	
		Public Function FindByPrimaryKey(ByVal instructionID As System.Int32) As ICInstruction
			Return MyBase.SingleOrDefault(Function(e) e.InstructionID.HasValue AndAlso e.InstructionID.Value = instructionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICInstruction))> _
		Public Class ICInstructionCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICInstructionCollection)
			
			Public Shared Widening Operator CType(packet As ICInstructionCollectionWCFPacket) As ICInstructionCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICInstructionCollection) As ICInstructionCollectionWCFPacket
				Return New ICInstructionCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICInstructionQuery 
		Inherits esICInstructionQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICInstructionQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICInstructionQuery) As String
			Return ICInstructionQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICInstructionQuery
			Return DirectCast(ICInstructionQuery.SerializeHelper.FromXml(query, GetType(ICInstructionQuery)), ICInstructionQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICInstruction
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal instructionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal instructionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(instructionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(instructionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal instructionID As System.Int32) As Boolean
		
			Dim query As New ICInstructionQuery()
			query.Where(query.InstructionID = instructionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal instructionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("InstructionID", instructionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Instruction.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.InstructionID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.Description
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Description As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.Description)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.Description, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Description)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.Remarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Remarks As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.Remarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.Remarks, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Remarks)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.Status, value) Then
					Me._UpToICInstructionStatusByStatus = Nothing
					Me.OnPropertyChanged("UpToICInstructionStatusByStatus")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.FileID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.FileID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.FileID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.FileID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AcquisitionMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AcquisitionMode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.AcquisitionMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.AcquisitionMode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AcquisitionMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.LastStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.LastStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.LastStatus, value) Then
					Me._UpToICInstructionStatusByLastStatus = Nothing
					Me.OnPropertyChanged("UpToICInstructionStatusByLastStatus")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.LastStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ErrorMessage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ErrorMessage As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ErrorMessage)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ErrorMessage, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ErrorMessage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AmountInWords
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountInWords As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.AmountInWords)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.AmountInWords, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmountInWords)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAccountNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryAccountTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAccountTitle As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAccountTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAccountTitle, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryAccountTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryAddress, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryBankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryBankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryBankAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBankAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBankAddress, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryBankAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryBranchName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryBranchName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBranchName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryBranchName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryBranchName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryCity
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCity As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCity)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCity, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryCity)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryCNIC
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCNIC As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCNIC)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCNIC, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryCNIC)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryCountry
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryCountry As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCountry)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryCountry, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryCountry)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryEmail
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryEmail As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryEmail)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryEmail, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryEmail)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryMobile
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryMobile As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryMobile)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryMobile, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryMobile)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryPhone
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryPhone As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryPhone)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryPhone, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryPhone)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryProvince
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryProvince As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryProvince)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryProvince, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryProvince)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountNo, value) Then
					Me._UpToICAccountsByClientAccountNo = Nothing
					Me.OnPropertyChanged("UpToICAccountsByClientAccountNo")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientAddress, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientBankCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientBankCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientBankCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientBankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientBankName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientBankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientBankName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientBankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientBranchAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientBranchAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchAddress, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientBranchAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientBranchName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientBranchName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientBranchName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientBranchName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientCity
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientCity As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientCity)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientCity, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientCity)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientCountry
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientCountry As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientCountry)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientCountry, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientCountry)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientEmail
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientEmail As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientEmail)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientEmail, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientEmail)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientMobile
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientMobile As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientMobile)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientMobile, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientMobile)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientProvince
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientProvince As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientProvince)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientProvince, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientProvince)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DetailAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DetailAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionMetadata.ColumnNames.DetailAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionMetadata.ColumnNames.DetailAmount, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DetailAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DDPayableLocation
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDPayableLocation As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.DDPayableLocation)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.DDPayableLocation, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DDPayableLocation)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.InstrumentNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.InstrumentNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.InstrumentNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.InstrumentNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.InvoiceNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.InvoiceNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.InvoiceNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.InvoiceNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PaymentMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentMode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PaymentMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PaymentMode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PaymentMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PrintLocationName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PrintLocationName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICProductTypeByProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICProductTypeByProductTypeCode")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.TransactionDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.TransactionDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.TransactionDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.TransactionDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.TXN_Code
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TXNCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.TXNCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.TXNCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.TXNCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ValueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ValueDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ValueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ValueDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ValueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsPrinted
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrinted As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrinted)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrinted, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrinted)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsRePrinted
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRePrinted As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsRePrinted)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsRePrinted, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsRePrinted)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.RePrintCounter
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RePrintCounter As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.RePrintCounter)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.RePrintCounter, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.RePrintCounter)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReIssued
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReIssued As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReIssued)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReIssued, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReIssued)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReValidated
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReValidated As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValidated)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValidated, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValidated)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReferenceField1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField1 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField1, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReferenceField1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReferenceField2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField2 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField2, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReferenceField2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReferenceField3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceField3 As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField3)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ReferenceField3, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReferenceField3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountBranchCode, value) Then
					Me._UpToICAccountsByClientAccountNo = Nothing
					Me.OnPropertyChanged("UpToICAccountsByClientAccountNo")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClientAccountCurrency, value) Then
					Me._UpToICAccountsByClientAccountNo = Nothing
					Me.OnPropertyChanged("UpToICAccountsByClientAccountNo")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneAccountBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneAccountCurrency, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.GroupCode, value) Then
					Me._UpToICGroupByGroupCode = Nothing
					Me.OnPropertyChanged("UpToICGroupByGroupCode")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountNumber, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountCurrency, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountCurrency, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DDDrawnCityCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDDrawnCityCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.DDDrawnCityCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.DDDrawnCityCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DDDrawnCityCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationCode, value) Then
					Me._UpToICOfficeByPrintLocationCode = Nothing
					Me.OnPropertyChanged("UpToICOfficeByPrintLocationCode")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CreatedOfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.CreatedOfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.CreatedOfficeCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreatedOfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.FileBatchNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileBatchNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.FileBatchNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.FileBatchNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.FileBatchNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReferenceNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReferenceNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ReferenceNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ReferenceNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReferenceNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PreviousPrintLocationName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreviousPrintLocationName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PreviousPrintLocationName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PreviousPrintLocationName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PreviousPrintLocationName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PreviousPrintLocationCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreviousPrintLocationCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PreviousPrintLocationCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PreviousPrintLocationCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PreviousPrintLocationCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.VerificationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property VerificationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.VerificationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.VerificationDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.VerificationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ApprovalDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ApprovalDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ApprovalDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ApprovalDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StaleDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StaleDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StaleDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StaleDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StaleDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CancellationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CancellationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.CancellationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.CancellationDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancellationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.LastPrintDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property LastPrintDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.LastPrintDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.LastPrintDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.LastPrintDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.RevalidationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RevalidationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.RevalidationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.RevalidationDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.RevalidationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DrawnOnBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DrawnOnBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.DrawnOnBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.DrawnOnBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DrawnOnBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientPrimaryApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientPrimaryApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedBy, value) Then
					Me._UpToICUserByClientPrimaryApprovedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByClientPrimaryApprovedBy")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientSecondaryApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientSecondaryApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovedBy, value) Then
					Me._UpToICUserByClientSecondaryApprovedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByClientSecondaryApprovedBy")
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientSecondaryApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientPrimaryApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientPrimaryApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClientSecondaryApprovalDateTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientSecondaryApprovalDateTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovalDateTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovalDateTime, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientSecondaryApprovalDateTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BankSecondaryApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankSecondaryApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankSecondaryApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BankPrimaryApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankPrimaryApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankPrimaryApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BankSecondaryApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankSecondaryApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankSecondaryApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BankPrimaryApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankPrimaryApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankPrimaryApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationAmendedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationAmendedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationAmendmentDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationAmendmentDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationAmendmentApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationAmendmentApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationApprovedAmendmentDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationApprovedAmendmentDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationApprovedAmendmentDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationApprovedAmendmentDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationApprovedAmendmentDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsPrintLocationAmendmentApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrintLocationAmendmentApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentApproved, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationAmendmentCancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationAmendmentCancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintLocationAmendmentCancellDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintLocationAmendmentCancellDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancellDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancellDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancellDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsPrintLocationAmendmentCancelled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrintLocationAmendmentCancelled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentCancelled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentCancelled, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentCancelled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsPrintLocationAmended
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsPrintLocationAmended As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmended)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsPrintLocationAmended, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmended)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReprintingAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReprintingAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReprintingAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReprintingAllowed, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReprintingAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReprintingAllowedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReprintingAllowedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReprintingAllowedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReprintingAllowedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReprintingAllowedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReprintingAllowedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReprintingAllowedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReprintingAllowedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsAllowReprintingApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowReprintingApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowReprintingApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowReprintingApproved, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowReprintingApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowReprintingApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowReprintingApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReprintingApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowReprintingApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowReprintingApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReprintingApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsAllowReprintingCancelled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowReprintingCancelled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowReprintingCancelled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowReprintingCancelled, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowReprintingCancelled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReprintingAllowedCancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReprintingAllowedCancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReprintingAllowedCancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReprintingAllowedCancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedCancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReprintingAllowCancelledDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReprintingAllowCancelledDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReprintingAllowCancelledDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReprintingAllowCancelledDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowCancelledDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReIssuanceAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReIssuanceAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReIssuanceAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReIssuanceAllowed, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReIssuanceAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReissuanceAlloweBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReissuanceAlloweBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReissuanceAlloweBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReissuanceAlloweBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReissuanceAlloweBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReIssuanceAllowedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReIssuanceAllowedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReIssuanceAllowedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReIssuanceAllowedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReIssuanceAllowedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsAllowedReIssuanceApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowedReIssuanceApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceApproved, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowReIssuanceApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowReIssuanceApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowReIssuanceApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowReIssuanceApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReIssuanceApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowedReIssuanceApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowedReIssuanceApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.AllowedReIssuanceApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.AllowedReIssuanceApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsAllowedReIssuanceCancelled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAllowedReIssuanceCancelled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceCancelled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceCancelled, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceCancelled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowedReIssuanceCancelledDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowedReIssuanceCancelledDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AllowedReIssuanceCancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AllowedReIssuanceCancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReIssuanceID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReIssuanceID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReIssuanceID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReIssuanceID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReIssuanceID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.NewInstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NewInstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.NewInstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.NewInstructionID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.NewInstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PhysicalInstrumentNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PhysicalInstrumentNumber As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PhysicalInstrumentNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PhysicalInstrumentNumber, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PhysicalInstrumentNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValiDatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValiDatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValiDatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValiDatedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValiDationAllowedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValiDationAllowedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReValiDationAllowedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReValiDationAllowedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationAllowedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReValiDationAllowed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReValiDationAllowed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationAllowed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationAllowed, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationAllowed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValidationApprovalDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValidationApprovalDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValidationApprovalDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValidationApprovalDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValidationApprovalDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValidationApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValidationApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReValidationApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReValidationApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValidationApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReValiDationApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReValiDationApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationApproved, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValiDationCancelDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValiDationCancelDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValiDationCancelDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ReValiDationCancelDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationCancelDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReValiDationCancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReValiDationCancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ReValiDationCancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ReValiDationCancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationCancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsReValiDationCancelled
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsReValiDationCancelled As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationCancelled)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsReValiDationCancelled, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationCancelled)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingBankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingBankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingBankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingBankCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingBankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ClearingDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ClearingDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingOfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingOfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingOfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingOfficeCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingOfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClearedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClearedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ClearingApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ClearingApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingApprovedOfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingApprovedOfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingApprovedOfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClearingApprovedOfficeCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedOfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AmendedPrintLocationCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmendedPrintLocationCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.AmendedPrintLocationCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.AmendedPrintLocationCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmendedPrintLocationCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AmendedPrintLocationName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmendedPrintLocationName As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.AmendedPrintLocationName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.AmendedPrintLocationName, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmendedPrintLocationName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableClientNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableSeqNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableProfitCentre, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PayableAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.PayableAccountType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PayableAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingClientNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingSeqNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingProfitCentre, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingProfitCentre)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingAccountType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.PrintDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrintDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.PrintDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsAmendmentComplete
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAmendmentComplete As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAmendmentComplete)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsAmendmentComplete, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAmendmentComplete)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClubID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClubID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ClubID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ClubID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClubID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.SkipReason
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SkipReason As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.SkipReason)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.SkipReason, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.SkipReason)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneBranchAddress
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneBranchAddress As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneBranchAddress)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneBranchAddress, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneBranchAddress)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusCancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusCancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusCancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusCancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusCancelledDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusCancelledDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusCancelledDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusCancelledDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusReversedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusReversedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusReversedOfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusReversedOfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedOfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedOfficeCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedOfficeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusReversedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusReversedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusReversedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusReversedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusSettledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusSettledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusSettledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusSettledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusSettledDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusSettledDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusSettledDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusSettledDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusCancelledApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusCancelledApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusSettledApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusSettledApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusSettledApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusSettledApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusReversedApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusReversedApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusReversedApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusCancelledApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusCancelledApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusSettledApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusSettledApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusSettledApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusSettledApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusReversedApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusReversedApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusReversedApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusReversedApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusChangeRemarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusChangeRemarks As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.StatusChangeRemarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.StatusChangeRemarks, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusChangeRemarks)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusApproveRemarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusApproveRemarks As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.StatusApproveRemarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.StatusApproveRemarks, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusApproveRemarks)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusChangeApprovalCancelledby
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusChangeApprovalCancelledby As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledby)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledby, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledby)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusChangeApprovalCancelledDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusChangeApprovalCancelledDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ClearingType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClearingType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ClearingType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ClearingType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ScheduleTransactionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleTransactionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.ScheduleTransactionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.ScheduleTransactionID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ScheduleTransactionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ScheduleTransactionTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ScheduleTransactionTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.ScheduleTransactionTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.ScheduleTransactionTime, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ScheduleTransactionTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IsCheduleTransactionProcessed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsCheduleTransactionProcessed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICInstructionMetadata.ColumnNames.IsCheduleTransactionProcessed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICInstructionMetadata.ColumnNames.IsCheduleTransactionProcessed, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsCheduleTransactionProcessed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryIDType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIDType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryIDType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryIDType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryIDType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficiaryIDNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficiaryIDNo As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryIDNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficiaryIDNo, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficiaryIDNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.RIN
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Rin As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.Rin)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.Rin, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.Rin)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DisbursedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.DisbursedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.DisbursedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DisbursedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.DisbursedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.DisbursedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DisbursedBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisbursedBranchCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.DisbursedBranchCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.DisbursedBranchCode, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CancelledBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CancelledBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.CancelledBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.CancelledBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CancelledApproveBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CancelledApproveBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.CancelledApproveBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.CancelledApproveBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledApproveBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.CancelledApprovalDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CancelledApprovalDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.CancelledApprovalDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.CancelledApprovalDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledApprovalDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.UBPCompanyID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UBPCompanyID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.UBPCompanyID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.UBPCompanyID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.UBPCompanyID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.UBPReferenceField
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UBPReferenceField As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.UBPReferenceField)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.UBPReferenceField, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.UBPReferenceField)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneficaryAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneficaryAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.BeneficaryAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.BeneficaryAccountType, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneficaryAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.BeneID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.BeneID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.BeneID, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.ReturnReason
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReturnReason As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.ReturnReason)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.ReturnReason, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReturnReason)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AmountBeforeDueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountBeforeDueDate As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionMetadata.ColumnNames.AmountBeforeDueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionMetadata.ColumnNames.AmountBeforeDueDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmountBeforeDueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.AmountAfterDueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountAfterDueDate As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICInstructionMetadata.ColumnNames.AmountAfterDueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICInstructionMetadata.ColumnNames.AmountAfterDueDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmountAfterDueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.DueDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DueDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.DueDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.DueDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.DueDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusHoldBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusHoldBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusHoldBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusHoldBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusHoldDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusHoldDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusHoldDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusHoldDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusHoldApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusHoldApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusHoldApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusHoldApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusHoldApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusHoldApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusHoldApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusHoldApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusUnHoldBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusUnHoldBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusUnHoldBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusUnHoldBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusUnHoldDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusUnHoldDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusUnHoldDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusUnHoldDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusUnHoldApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusUnHoldApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusUnHoldApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusUnHoldApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusRevertBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusRevertBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusRevertBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusRevertBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.statusRevertDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusRevertDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusRevertDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusRevertDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusRevertApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusRevertApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.StatusRevertApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.StatusRevertApprovedBy, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.StatusRevertApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property StatusRevertApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusRevertApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICInstructionMetadata.ColumnNames.StatusRevertApprovedDate, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.IBFTStan
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IBFTStan As System.String
			Get
				Return MyBase.GetSystemString(ICInstructionMetadata.ColumnNames.IBFTStan)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICInstructionMetadata.ColumnNames.IBFTStan, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.IBFTStan)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Instruction.FinalStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FinalStatus As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICInstructionMetadata.ColumnNames.FinalStatus)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICInstructionMetadata.ColumnNames.FinalStatus, value) Then
					OnPropertyChanged(ICInstructionMetadata.PropertyNames.FinalStatus)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByClientAccountNo As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICGroupByGroupCode As ICGroup
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionStatusByStatus As ICInstructionStatus
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionStatusByLastStatus As ICInstructionStatus
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByPrintLocationCode As ICOffice
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICProductTypeByProductTypeCode As ICProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByClientPrimaryApprovedBy As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByClientSecondaryApprovedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "Description"
							Me.str().Description = CType(value, string)
												
						Case "Remarks"
							Me.str().Remarks = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "FileID"
							Me.str().FileID = CType(value, string)
												
						Case "AcquisitionMode"
							Me.str().AcquisitionMode = CType(value, string)
												
						Case "LastStatus"
							Me.str().LastStatus = CType(value, string)
												
						Case "ErrorMessage"
							Me.str().ErrorMessage = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
												
						Case "AmountInWords"
							Me.str().AmountInWords = CType(value, string)
												
						Case "BeneficiaryAccountNo"
							Me.str().BeneficiaryAccountNo = CType(value, string)
												
						Case "BeneficiaryAccountTitle"
							Me.str().BeneficiaryAccountTitle = CType(value, string)
												
						Case "BeneficiaryAddress"
							Me.str().BeneficiaryAddress = CType(value, string)
												
						Case "BeneficiaryBankCode"
							Me.str().BeneficiaryBankCode = CType(value, string)
												
						Case "BeneficiaryBankName"
							Me.str().BeneficiaryBankName = CType(value, string)
												
						Case "BeneficiaryBankAddress"
							Me.str().BeneficiaryBankAddress = CType(value, string)
												
						Case "BeneficiaryBranchCode"
							Me.str().BeneficiaryBranchCode = CType(value, string)
												
						Case "BeneficiaryBranchName"
							Me.str().BeneficiaryBranchName = CType(value, string)
												
						Case "BeneficiaryCity"
							Me.str().BeneficiaryCity = CType(value, string)
												
						Case "BeneficiaryCNIC"
							Me.str().BeneficiaryCNIC = CType(value, string)
												
						Case "BeneficiaryCode"
							Me.str().BeneficiaryCode = CType(value, string)
												
						Case "BeneficiaryCountry"
							Me.str().BeneficiaryCountry = CType(value, string)
												
						Case "BeneficiaryEmail"
							Me.str().BeneficiaryEmail = CType(value, string)
												
						Case "BeneficiaryMobile"
							Me.str().BeneficiaryMobile = CType(value, string)
												
						Case "BeneficiaryName"
							Me.str().BeneficiaryName = CType(value, string)
												
						Case "BeneficiaryPhone"
							Me.str().BeneficiaryPhone = CType(value, string)
												
						Case "BeneficiaryProvince"
							Me.str().BeneficiaryProvince = CType(value, string)
												
						Case "BeneficiaryType"
							Me.str().BeneficiaryType = CType(value, string)
												
						Case "ClientAccountNo"
							Me.str().ClientAccountNo = CType(value, string)
												
						Case "ClientAddress"
							Me.str().ClientAddress = CType(value, string)
												
						Case "ClientBankCode"
							Me.str().ClientBankCode = CType(value, string)
												
						Case "ClientBankName"
							Me.str().ClientBankName = CType(value, string)
												
						Case "ClientBranchAddress"
							Me.str().ClientBranchAddress = CType(value, string)
												
						Case "ClientBranchCode"
							Me.str().ClientBranchCode = CType(value, string)
												
						Case "ClientBranchName"
							Me.str().ClientBranchName = CType(value, string)
												
						Case "ClientCity"
							Me.str().ClientCity = CType(value, string)
												
						Case "ClientCountry"
							Me.str().ClientCountry = CType(value, string)
												
						Case "ClientEmail"
							Me.str().ClientEmail = CType(value, string)
												
						Case "ClientMobile"
							Me.str().ClientMobile = CType(value, string)
												
						Case "ClientName"
							Me.str().ClientName = CType(value, string)
												
						Case "ClientProvince"
							Me.str().ClientProvince = CType(value, string)
												
						Case "DetailAmount"
							Me.str().DetailAmount = CType(value, string)
												
						Case "DDPayableLocation"
							Me.str().DDPayableLocation = CType(value, string)
												
						Case "InstrumentNo"
							Me.str().InstrumentNo = CType(value, string)
												
						Case "InvoiceNo"
							Me.str().InvoiceNo = CType(value, string)
												
						Case "PaymentMode"
							Me.str().PaymentMode = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "PrintLocationName"
							Me.str().PrintLocationName = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "TransactionDate"
							Me.str().TransactionDate = CType(value, string)
												
						Case "TXNCode"
							Me.str().TXNCode = CType(value, string)
												
						Case "ValueDate"
							Me.str().ValueDate = CType(value, string)
												
						Case "IsPrinted"
							Me.str().IsPrinted = CType(value, string)
												
						Case "IsRePrinted"
							Me.str().IsRePrinted = CType(value, string)
												
						Case "RePrintCounter"
							Me.str().RePrintCounter = CType(value, string)
												
						Case "IsReIssued"
							Me.str().IsReIssued = CType(value, string)
												
						Case "IsReValidated"
							Me.str().IsReValidated = CType(value, string)
												
						Case "ReferenceField1"
							Me.str().ReferenceField1 = CType(value, string)
												
						Case "ReferenceField2"
							Me.str().ReferenceField2 = CType(value, string)
												
						Case "ReferenceField3"
							Me.str().ReferenceField3 = CType(value, string)
												
						Case "ClientAccountBranchCode"
							Me.str().ClientAccountBranchCode = CType(value, string)
												
						Case "ClientAccountCurrency"
							Me.str().ClientAccountCurrency = CType(value, string)
												
						Case "BeneAccountBranchCode"
							Me.str().BeneAccountBranchCode = CType(value, string)
												
						Case "BeneAccountCurrency"
							Me.str().BeneAccountCurrency = CType(value, string)
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "PayableAccountNumber"
							Me.str().PayableAccountNumber = CType(value, string)
												
						Case "PayableAccountBranchCode"
							Me.str().PayableAccountBranchCode = CType(value, string)
												
						Case "PayableAccountCurrency"
							Me.str().PayableAccountCurrency = CType(value, string)
												
						Case "ClearingAccountNo"
							Me.str().ClearingAccountNo = CType(value, string)
												
						Case "ClearingAccountBranchCode"
							Me.str().ClearingAccountBranchCode = CType(value, string)
												
						Case "ClearingAccountCurrency"
							Me.str().ClearingAccountCurrency = CType(value, string)
												
						Case "DDDrawnCityCode"
							Me.str().DDDrawnCityCode = CType(value, string)
												
						Case "PrintLocationCode"
							Me.str().PrintLocationCode = CType(value, string)
												
						Case "CreatedOfficeCode"
							Me.str().CreatedOfficeCode = CType(value, string)
												
						Case "FileBatchNo"
							Me.str().FileBatchNo = CType(value, string)
												
						Case "ReferenceNo"
							Me.str().ReferenceNo = CType(value, string)
												
						Case "PreviousPrintLocationName"
							Me.str().PreviousPrintLocationName = CType(value, string)
												
						Case "PreviousPrintLocationCode"
							Me.str().PreviousPrintLocationCode = CType(value, string)
												
						Case "VerificationDate"
							Me.str().VerificationDate = CType(value, string)
												
						Case "ApprovalDate"
							Me.str().ApprovalDate = CType(value, string)
												
						Case "StaleDate"
							Me.str().StaleDate = CType(value, string)
												
						Case "CancellationDate"
							Me.str().CancellationDate = CType(value, string)
												
						Case "LastPrintDate"
							Me.str().LastPrintDate = CType(value, string)
												
						Case "RevalidationDate"
							Me.str().RevalidationDate = CType(value, string)
												
						Case "DrawnOnBranchCode"
							Me.str().DrawnOnBranchCode = CType(value, string)
												
						Case "ClientPrimaryApprovedBy"
							Me.str().ClientPrimaryApprovedBy = CType(value, string)
												
						Case "ClientSecondaryApprovedBy"
							Me.str().ClientSecondaryApprovedBy = CType(value, string)
												
						Case "ClientPrimaryApprovedDate"
							Me.str().ClientPrimaryApprovedDate = CType(value, string)
												
						Case "ClientSecondaryApprovalDateTime"
							Me.str().ClientSecondaryApprovalDateTime = CType(value, string)
												
						Case "BankSecondaryApprovedBy"
							Me.str().BankSecondaryApprovedBy = CType(value, string)
												
						Case "BankPrimaryApprovedDate"
							Me.str().BankPrimaryApprovedDate = CType(value, string)
												
						Case "BankSecondaryApprovedDate"
							Me.str().BankSecondaryApprovedDate = CType(value, string)
												
						Case "BankPrimaryApprovedBy"
							Me.str().BankPrimaryApprovedBy = CType(value, string)
												
						Case "PrintLocationAmendedBy"
							Me.str().PrintLocationAmendedBy = CType(value, string)
												
						Case "PrintLocationAmendmentDate"
							Me.str().PrintLocationAmendmentDate = CType(value, string)
												
						Case "PrintLocationAmendmentApprovedBy"
							Me.str().PrintLocationAmendmentApprovedBy = CType(value, string)
												
						Case "PrintLocationApprovedAmendmentDate"
							Me.str().PrintLocationApprovedAmendmentDate = CType(value, string)
												
						Case "IsPrintLocationAmendmentApproved"
							Me.str().IsPrintLocationAmendmentApproved = CType(value, string)
												
						Case "PrintLocationAmendmentCancelledBy"
							Me.str().PrintLocationAmendmentCancelledBy = CType(value, string)
												
						Case "PrintLocationAmendmentCancellDate"
							Me.str().PrintLocationAmendmentCancellDate = CType(value, string)
												
						Case "IsPrintLocationAmendmentCancelled"
							Me.str().IsPrintLocationAmendmentCancelled = CType(value, string)
												
						Case "IsPrintLocationAmended"
							Me.str().IsPrintLocationAmended = CType(value, string)
												
						Case "IsReprintingAllowed"
							Me.str().IsReprintingAllowed = CType(value, string)
												
						Case "ReprintingAllowedDate"
							Me.str().ReprintingAllowedDate = CType(value, string)
												
						Case "ReprintingAllowedBy"
							Me.str().ReprintingAllowedBy = CType(value, string)
												
						Case "IsAllowReprintingApproved"
							Me.str().IsAllowReprintingApproved = CType(value, string)
												
						Case "AllowReprintingApprovedBy"
							Me.str().AllowReprintingApprovedBy = CType(value, string)
												
						Case "AllowReprintingApprovedDate"
							Me.str().AllowReprintingApprovedDate = CType(value, string)
												
						Case "IsAllowReprintingCancelled"
							Me.str().IsAllowReprintingCancelled = CType(value, string)
												
						Case "ReprintingAllowedCancelledBy"
							Me.str().ReprintingAllowedCancelledBy = CType(value, string)
												
						Case "ReprintingAllowCancelledDate"
							Me.str().ReprintingAllowCancelledDate = CType(value, string)
												
						Case "IsReIssuanceAllowed"
							Me.str().IsReIssuanceAllowed = CType(value, string)
												
						Case "ReissuanceAlloweBy"
							Me.str().ReissuanceAlloweBy = CType(value, string)
												
						Case "ReIssuanceAllowedDate"
							Me.str().ReIssuanceAllowedDate = CType(value, string)
												
						Case "IsAllowedReIssuanceApproved"
							Me.str().IsAllowedReIssuanceApproved = CType(value, string)
												
						Case "AllowReIssuanceApprovedDate"
							Me.str().AllowReIssuanceApprovedDate = CType(value, string)
												
						Case "AllowedReIssuanceApprovedBy"
							Me.str().AllowedReIssuanceApprovedBy = CType(value, string)
												
						Case "IsAllowedReIssuanceCancelled"
							Me.str().IsAllowedReIssuanceCancelled = CType(value, string)
												
						Case "AllowedReIssuanceCancelledDate"
							Me.str().AllowedReIssuanceCancelledDate = CType(value, string)
												
						Case "AllowedReIssuanceCancelledBy"
							Me.str().AllowedReIssuanceCancelledBy = CType(value, string)
												
						Case "ReIssuanceID"
							Me.str().ReIssuanceID = CType(value, string)
												
						Case "NewInstructionID"
							Me.str().NewInstructionID = CType(value, string)
												
						Case "PhysicalInstrumentNumber"
							Me.str().PhysicalInstrumentNumber = CType(value, string)
												
						Case "ReValiDatedDate"
							Me.str().ReValiDatedDate = CType(value, string)
												
						Case "ReValiDationAllowedBy"
							Me.str().ReValiDationAllowedBy = CType(value, string)
												
						Case "IsReValiDationAllowed"
							Me.str().IsReValiDationAllowed = CType(value, string)
												
						Case "ReValidationApprovalDate"
							Me.str().ReValidationApprovalDate = CType(value, string)
												
						Case "ReValidationApprovedBy"
							Me.str().ReValidationApprovedBy = CType(value, string)
												
						Case "IsReValiDationApproved"
							Me.str().IsReValiDationApproved = CType(value, string)
												
						Case "ReValiDationCancelDate"
							Me.str().ReValiDationCancelDate = CType(value, string)
												
						Case "ReValiDationCancelledBy"
							Me.str().ReValiDationCancelledBy = CType(value, string)
												
						Case "IsReValiDationCancelled"
							Me.str().IsReValiDationCancelled = CType(value, string)
												
						Case "ClearingBankCode"
							Me.str().ClearingBankCode = CType(value, string)
												
						Case "ClearingDate"
							Me.str().ClearingDate = CType(value, string)
												
						Case "ClearingOfficeCode"
							Me.str().ClearingOfficeCode = CType(value, string)
												
						Case "ClearedBy"
							Me.str().ClearedBy = CType(value, string)
												
						Case "ClearingApprovedBy"
							Me.str().ClearingApprovedBy = CType(value, string)
												
						Case "ClearingApprovedDate"
							Me.str().ClearingApprovedDate = CType(value, string)
												
						Case "ClearingApprovedOfficeCode"
							Me.str().ClearingApprovedOfficeCode = CType(value, string)
												
						Case "AmendedPrintLocationCode"
							Me.str().AmendedPrintLocationCode = CType(value, string)
												
						Case "AmendedPrintLocationName"
							Me.str().AmendedPrintLocationName = CType(value, string)
												
						Case "PayableClientNo"
							Me.str().PayableClientNo = CType(value, string)
												
						Case "PayableSeqNo"
							Me.str().PayableSeqNo = CType(value, string)
												
						Case "PayableProfitCentre"
							Me.str().PayableProfitCentre = CType(value, string)
												
						Case "PayableAccountType"
							Me.str().PayableAccountType = CType(value, string)
												
						Case "ClearingClientNo"
							Me.str().ClearingClientNo = CType(value, string)
												
						Case "ClearingSeqNo"
							Me.str().ClearingSeqNo = CType(value, string)
												
						Case "ClearingProfitCentre"
							Me.str().ClearingProfitCentre = CType(value, string)
												
						Case "ClearingAccountType"
							Me.str().ClearingAccountType = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "PrintDate"
							Me.str().PrintDate = CType(value, string)
												
						Case "IsAmendmentComplete"
							Me.str().IsAmendmentComplete = CType(value, string)
												
						Case "ClubID"
							Me.str().ClubID = CType(value, string)
												
						Case "SkipReason"
							Me.str().SkipReason = CType(value, string)
												
						Case "BeneBranchAddress"
							Me.str().BeneBranchAddress = CType(value, string)
												
						Case "StatusCancelledBy"
							Me.str().StatusCancelledBy = CType(value, string)
												
						Case "StatusCancelledDate"
							Me.str().StatusCancelledDate = CType(value, string)
												
						Case "StatusReversedBy"
							Me.str().StatusReversedBy = CType(value, string)
												
						Case "StatusReversedOfficeCode"
							Me.str().StatusReversedOfficeCode = CType(value, string)
												
						Case "StatusReversedDate"
							Me.str().StatusReversedDate = CType(value, string)
												
						Case "StatusSettledBy"
							Me.str().StatusSettledBy = CType(value, string)
												
						Case "StatusSettledDate"
							Me.str().StatusSettledDate = CType(value, string)
												
						Case "StatusCancelledApprovedBy"
							Me.str().StatusCancelledApprovedBy = CType(value, string)
												
						Case "StatusSettledApprovedBy"
							Me.str().StatusSettledApprovedBy = CType(value, string)
												
						Case "StatusReversedApprovedBy"
							Me.str().StatusReversedApprovedBy = CType(value, string)
												
						Case "StatusCancelledApprovedDate"
							Me.str().StatusCancelledApprovedDate = CType(value, string)
												
						Case "StatusSettledApprovedDate"
							Me.str().StatusSettledApprovedDate = CType(value, string)
												
						Case "StatusReversedApprovedDate"
							Me.str().StatusReversedApprovedDate = CType(value, string)
												
						Case "StatusChangeRemarks"
							Me.str().StatusChangeRemarks = CType(value, string)
												
						Case "StatusApproveRemarks"
							Me.str().StatusApproveRemarks = CType(value, string)
												
						Case "StatusChangeApprovalCancelledby"
							Me.str().StatusChangeApprovalCancelledby = CType(value, string)
												
						Case "StatusChangeApprovalCancelledDate"
							Me.str().StatusChangeApprovalCancelledDate = CType(value, string)
												
						Case "ClearingType"
							Me.str().ClearingType = CType(value, string)
												
						Case "ScheduleTransactionID"
							Me.str().ScheduleTransactionID = CType(value, string)
												
						Case "ScheduleTransactionTime"
							Me.str().ScheduleTransactionTime = CType(value, string)
												
						Case "IsCheduleTransactionProcessed"
							Me.str().IsCheduleTransactionProcessed = CType(value, string)
												
						Case "BeneficiaryIDType"
							Me.str().BeneficiaryIDType = CType(value, string)
												
						Case "BeneficiaryIDNo"
							Me.str().BeneficiaryIDNo = CType(value, string)
												
						Case "Rin"
							Me.str().Rin = CType(value, string)
												
						Case "DisbursedBy"
							Me.str().DisbursedBy = CType(value, string)
												
						Case "DisbursedDate"
							Me.str().DisbursedDate = CType(value, string)
												
						Case "DisbursedBranchCode"
							Me.str().DisbursedBranchCode = CType(value, string)
												
						Case "CancelledBy"
							Me.str().CancelledBy = CType(value, string)
												
						Case "CancelledApproveBy"
							Me.str().CancelledApproveBy = CType(value, string)
												
						Case "CancelledApprovalDate"
							Me.str().CancelledApprovalDate = CType(value, string)
												
						Case "UBPCompanyID"
							Me.str().UBPCompanyID = CType(value, string)
												
						Case "UBPReferenceField"
							Me.str().UBPReferenceField = CType(value, string)
												
						Case "BeneficaryAccountType"
							Me.str().BeneficaryAccountType = CType(value, string)
												
						Case "BeneID"
							Me.str().BeneID = CType(value, string)
												
						Case "ReturnReason"
							Me.str().ReturnReason = CType(value, string)
												
						Case "AmountBeforeDueDate"
							Me.str().AmountBeforeDueDate = CType(value, string)
												
						Case "AmountAfterDueDate"
							Me.str().AmountAfterDueDate = CType(value, string)
												
						Case "DueDate"
							Me.str().DueDate = CType(value, string)
												
						Case "StatusHoldBy"
							Me.str().StatusHoldBy = CType(value, string)
												
						Case "StatusHoldDate"
							Me.str().StatusHoldDate = CType(value, string)
												
						Case "StatusHoldApprovedBy"
							Me.str().StatusHoldApprovedBy = CType(value, string)
												
						Case "StatusHoldApprovedDate"
							Me.str().StatusHoldApprovedDate = CType(value, string)
												
						Case "StatusUnHoldBy"
							Me.str().StatusUnHoldBy = CType(value, string)
												
						Case "StatusUnHoldDate"
							Me.str().StatusUnHoldDate = CType(value, string)
												
						Case "StatusUnHoldApprovedBy"
							Me.str().StatusUnHoldApprovedBy = CType(value, string)
												
						Case "StatusUnHoldApprovedDate"
							Me.str().StatusUnHoldApprovedDate = CType(value, string)
												
						Case "StatusRevertBy"
							Me.str().StatusRevertBy = CType(value, string)
												
						Case "StatusRevertDate"
							Me.str().StatusRevertDate = CType(value, string)
												
						Case "StatusRevertApprovedBy"
							Me.str().StatusRevertApprovedBy = CType(value, string)
												
						Case "StatusRevertApprovedDate"
							Me.str().StatusRevertApprovedDate = CType(value, string)
												
						Case "IBFTStan"
							Me.str().IBFTStan = CType(value, string)
												
						Case "FinalStatus"
							Me.str().FinalStatus = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.InstructionID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Status"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Status = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.Status)
							End If
						
						Case "FileID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.FileID)
							End If
						
						Case "LastStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.LastStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.LastStatus)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.Amount)
							End If
						
						Case "DetailAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.DetailAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DetailAmount)
							End If
						
						Case "TransactionDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.TransactionDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.TransactionDate)
							End If
						
						Case "TXNCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TXNCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.TXNCode)
							End If
						
						Case "ValueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ValueDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ValueDate)
							End If
						
						Case "IsPrinted"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrinted = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrinted)
							End If
						
						Case "IsRePrinted"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRePrinted = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsRePrinted)
							End If
						
						Case "RePrintCounter"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RePrintCounter = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.RePrintCounter)
							End If
						
						Case "IsReIssued"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReIssued = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReIssued)
							End If
						
						Case "IsReValidated"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReValidated = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValidated)
							End If
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.GroupCode)
							End If
						
						Case "DDDrawnCityCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DDDrawnCityCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DDDrawnCityCode)
							End If
						
						Case "PrintLocationCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintLocationCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationCode)
							End If
						
						Case "CreatedOfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedOfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreatedOfficeCode)
							End If
						
						Case "VerificationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.VerificationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.VerificationDate)
							End If
						
						Case "ApprovalDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovalDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ApprovalDate)
							End If
						
						Case "StaleDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StaleDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StaleDate)
							End If
						
						Case "CancellationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CancellationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancellationDate)
							End If
						
						Case "LastPrintDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.LastPrintDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.LastPrintDate)
							End If
						
						Case "RevalidationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.RevalidationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.RevalidationDate)
							End If
						
						Case "ClientPrimaryApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClientPrimaryApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedBy)
							End If
						
						Case "ClientSecondaryApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClientSecondaryApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientSecondaryApprovedBy)
							End If
						
						Case "ClientPrimaryApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ClientPrimaryApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedDate)
							End If
						
						Case "ClientSecondaryApprovalDateTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ClientSecondaryApprovalDateTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClientSecondaryApprovalDateTime)
							End If
						
						Case "BankSecondaryApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankSecondaryApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankSecondaryApprovedBy)
							End If
						
						Case "BankPrimaryApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.BankPrimaryApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankPrimaryApprovedDate)
							End If
						
						Case "BankSecondaryApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.BankSecondaryApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankSecondaryApprovedDate)
							End If
						
						Case "BankPrimaryApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankPrimaryApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.BankPrimaryApprovedBy)
							End If
						
						Case "PrintLocationAmendedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintLocationAmendedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendedBy)
							End If
						
						Case "PrintLocationAmendmentDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PrintLocationAmendmentDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentDate)
							End If
						
						Case "PrintLocationAmendmentApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintLocationAmendmentApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentApprovedBy)
							End If
						
						Case "PrintLocationApprovedAmendmentDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PrintLocationApprovedAmendmentDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationApprovedAmendmentDate)
							End If
						
						Case "IsPrintLocationAmendmentApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrintLocationAmendmentApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentApproved)
							End If
						
						Case "PrintLocationAmendmentCancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrintLocationAmendmentCancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancelledBy)
							End If
						
						Case "PrintLocationAmendmentCancellDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PrintLocationAmendmentCancellDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancellDate)
							End If
						
						Case "IsPrintLocationAmendmentCancelled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrintLocationAmendmentCancelled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentCancelled)
							End If
						
						Case "IsPrintLocationAmended"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsPrintLocationAmended = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsPrintLocationAmended)
							End If
						
						Case "IsReprintingAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReprintingAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReprintingAllowed)
							End If
						
						Case "ReprintingAllowedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReprintingAllowedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedDate)
							End If
						
						Case "ReprintingAllowedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReprintingAllowedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedBy)
							End If
						
						Case "IsAllowReprintingApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowReprintingApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowReprintingApproved)
							End If
						
						Case "AllowReprintingApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AllowReprintingApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReprintingApprovedBy)
							End If
						
						Case "AllowReprintingApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AllowReprintingApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReprintingApprovedDate)
							End If
						
						Case "IsAllowReprintingCancelled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowReprintingCancelled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowReprintingCancelled)
							End If
						
						Case "ReprintingAllowedCancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReprintingAllowedCancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowedCancelledBy)
							End If
						
						Case "ReprintingAllowCancelledDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReprintingAllowCancelledDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReprintingAllowCancelledDate)
							End If
						
						Case "IsReIssuanceAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReIssuanceAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReIssuanceAllowed)
							End If
						
						Case "ReissuanceAlloweBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReissuanceAlloweBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReissuanceAlloweBy)
							End If
						
						Case "ReIssuanceAllowedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReIssuanceAllowedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReIssuanceAllowedDate)
							End If
						
						Case "IsAllowedReIssuanceApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowedReIssuanceApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceApproved)
							End If
						
						Case "AllowReIssuanceApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AllowReIssuanceApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowReIssuanceApprovedDate)
							End If
						
						Case "AllowedReIssuanceApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AllowedReIssuanceApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceApprovedBy)
							End If
						
						Case "IsAllowedReIssuanceCancelled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAllowedReIssuanceCancelled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceCancelled)
							End If
						
						Case "AllowedReIssuanceCancelledDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.AllowedReIssuanceCancelledDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledDate)
							End If
						
						Case "AllowedReIssuanceCancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AllowedReIssuanceCancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledBy)
							End If
						
						Case "ReIssuanceID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReIssuanceID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReIssuanceID)
							End If
						
						Case "NewInstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.NewInstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.NewInstructionID)
							End If
						
						Case "ReValiDatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReValiDatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDatedDate)
							End If
						
						Case "ReValiDationAllowedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReValiDationAllowedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationAllowedBy)
							End If
						
						Case "IsReValiDationAllowed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReValiDationAllowed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationAllowed)
							End If
						
						Case "ReValidationApprovalDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReValidationApprovalDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValidationApprovalDate)
							End If
						
						Case "ReValidationApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReValidationApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValidationApprovedBy)
							End If
						
						Case "IsReValiDationApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReValiDationApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationApproved)
							End If
						
						Case "ReValiDationCancelDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ReValiDationCancelDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationCancelDate)
							End If
						
						Case "ReValiDationCancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReValiDationCancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ReValiDationCancelledBy)
							End If
						
						Case "IsReValiDationCancelled"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsReValiDationCancelled = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsReValiDationCancelled)
							End If
						
						Case "ClearingBankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingBankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingBankCode)
							End If
						
						Case "ClearingDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ClearingDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingDate)
							End If
						
						Case "ClearingOfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingOfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingOfficeCode)
							End If
						
						Case "ClearedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearedBy)
							End If
						
						Case "ClearingApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedBy)
							End If
						
						Case "ClearingApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ClearingApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedDate)
							End If
						
						Case "ClearingApprovedOfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClearingApprovedOfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClearingApprovedOfficeCode)
							End If
						
						Case "AmendedPrintLocationCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AmendedPrintLocationCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmendedPrintLocationCode)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CreationDate)
							End If
						
						Case "PrintDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PrintDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.PrintDate)
							End If
						
						Case "IsAmendmentComplete"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAmendmentComplete = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsAmendmentComplete)
							End If
						
						Case "ClubID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ClubID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ClubID)
							End If
						
						Case "StatusCancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusCancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledBy)
							End If
						
						Case "StatusCancelledDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusCancelledDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledDate)
							End If
						
						Case "StatusReversedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusReversedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedBy)
							End If
						
						Case "StatusReversedOfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusReversedOfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedOfficeCode)
							End If
						
						Case "StatusReversedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusReversedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedDate)
							End If
						
						Case "StatusSettledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusSettledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledBy)
							End If
						
						Case "StatusSettledDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusSettledDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledDate)
							End If
						
						Case "StatusCancelledApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusCancelledApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledApprovedBy)
							End If
						
						Case "StatusSettledApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusSettledApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledApprovedBy)
							End If
						
						Case "StatusReversedApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusReversedApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedApprovedBy)
							End If
						
						Case "StatusCancelledApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusCancelledApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusCancelledApprovedDate)
							End If
						
						Case "StatusSettledApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusSettledApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusSettledApprovedDate)
							End If
						
						Case "StatusReversedApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusReversedApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusReversedApprovedDate)
							End If
						
						Case "StatusChangeApprovalCancelledby"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusChangeApprovalCancelledby = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledby)
							End If
						
						Case "StatusChangeApprovalCancelledDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusChangeApprovalCancelledDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledDate)
							End If
						
						Case "ScheduleTransactionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ScheduleTransactionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ScheduleTransactionID)
							End If
						
						Case "ScheduleTransactionTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ScheduleTransactionTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.ScheduleTransactionTime)
							End If
						
						Case "IsCheduleTransactionProcessed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsCheduleTransactionProcessed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.IsCheduleTransactionProcessed)
							End If
						
						Case "DisbursedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DisbursedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedBy)
							End If
						
						Case "DisbursedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.DisbursedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedDate)
							End If
						
						Case "DisbursedBranchCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DisbursedBranchCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DisbursedBranchCode)
							End If
						
						Case "CancelledBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CancelledBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledBy)
							End If
						
						Case "CancelledApproveBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CancelledApproveBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledApproveBy)
							End If
						
						Case "CancelledApprovalDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CancelledApprovalDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.CancelledApprovalDate)
							End If
						
						Case "UBPCompanyID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UBPCompanyID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.UBPCompanyID)
							End If
						
						Case "BeneID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BeneID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.BeneID)
							End If
						
						Case "AmountBeforeDueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.AmountBeforeDueDate = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmountBeforeDueDate)
							End If
						
						Case "AmountAfterDueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.AmountAfterDueDate = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.AmountAfterDueDate)
							End If
						
						Case "DueDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.DueDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.DueDate)
							End If
						
						Case "StatusHoldBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusHoldBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldBy)
							End If
						
						Case "StatusHoldDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusHoldDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldDate)
							End If
						
						Case "StatusHoldApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusHoldApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldApprovedBy)
							End If
						
						Case "StatusHoldApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusHoldApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusHoldApprovedDate)
							End If
						
						Case "StatusUnHoldBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusUnHoldBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldBy)
							End If
						
						Case "StatusUnHoldDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusUnHoldDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldDate)
							End If
						
						Case "StatusUnHoldApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusUnHoldApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedBy)
							End If
						
						Case "StatusUnHoldApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusUnHoldApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedDate)
							End If
						
						Case "StatusRevertBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusRevertBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertBy)
							End If
						
						Case "StatusRevertDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusRevertDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertDate)
							End If
						
						Case "StatusRevertApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.StatusRevertApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertApprovedBy)
							End If
						
						Case "StatusRevertApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.StatusRevertApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.StatusRevertApprovedDate)
							End If
						
						Case "FinalStatus"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FinalStatus = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICInstructionMetadata.PropertyNames.FinalStatus)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICInstruction)
				Me.entity = entity
			End Sub				
		
	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Description As System.String 
				Get
					Dim data_ As System.String = entity.Description
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Description = Nothing
					Else
						entity.Description = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Remarks As System.String 
				Get
					Dim data_ As System.String = entity.Remarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Remarks = Nothing
					Else
						entity.Remarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Status
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileID = Nothing
					Else
						entity.FileID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AcquisitionMode As System.String 
				Get
					Dim data_ As System.String = entity.AcquisitionMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AcquisitionMode = Nothing
					Else
						entity.AcquisitionMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.LastStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastStatus = Nothing
					Else
						entity.LastStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ErrorMessage As System.String 
				Get
					Dim data_ As System.String = entity.ErrorMessage
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ErrorMessage = Nothing
					Else
						entity.ErrorMessage = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountInWords As System.String 
				Get
					Dim data_ As System.String = entity.AmountInWords
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountInWords = Nothing
					Else
						entity.AmountInWords = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountNo = Nothing
					Else
						entity.BeneficiaryAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAccountTitle As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAccountTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAccountTitle = Nothing
					Else
						entity.BeneficiaryAccountTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryAddress = Nothing
					Else
						entity.BeneficiaryAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankCode = Nothing
					Else
						entity.BeneficiaryBankCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankName = Nothing
					Else
						entity.BeneficiaryBankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBankAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBankAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBankAddress = Nothing
					Else
						entity.BeneficiaryBankAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBranchCode = Nothing
					Else
						entity.BeneficiaryBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryBranchName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryBranchName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryBranchName = Nothing
					Else
						entity.BeneficiaryBranchName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCity As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCity
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCity = Nothing
					Else
						entity.BeneficiaryCity = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCNIC As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCNIC
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCNIC = Nothing
					Else
						entity.BeneficiaryCNIC = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCode = Nothing
					Else
						entity.BeneficiaryCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryCountry As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryCountry
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryCountry = Nothing
					Else
						entity.BeneficiaryCountry = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryEmail As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryEmail
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryEmail = Nothing
					Else
						entity.BeneficiaryEmail = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryMobile As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryMobile
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryMobile = Nothing
					Else
						entity.BeneficiaryMobile = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryName As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryName = Nothing
					Else
						entity.BeneficiaryName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryPhone As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryPhone
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryPhone = Nothing
					Else
						entity.BeneficiaryPhone = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryProvince As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryProvince
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryProvince = Nothing
					Else
						entity.BeneficiaryProvince = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryType As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryType = Nothing
					Else
						entity.BeneficiaryType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountNo = Nothing
					Else
						entity.ClientAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAddress As System.String 
				Get
					Dim data_ As System.String = entity.ClientAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAddress = Nothing
					Else
						entity.ClientAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientBankCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientBankCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientBankCode = Nothing
					Else
						entity.ClientBankCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientBankName As System.String 
				Get
					Dim data_ As System.String = entity.ClientBankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientBankName = Nothing
					Else
						entity.ClientBankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientBranchAddress As System.String 
				Get
					Dim data_ As System.String = entity.ClientBranchAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientBranchAddress = Nothing
					Else
						entity.ClientBranchAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientBranchCode = Nothing
					Else
						entity.ClientBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientBranchName As System.String 
				Get
					Dim data_ As System.String = entity.ClientBranchName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientBranchName = Nothing
					Else
						entity.ClientBranchName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientCity As System.String 
				Get
					Dim data_ As System.String = entity.ClientCity
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientCity = Nothing
					Else
						entity.ClientCity = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientCountry As System.String 
				Get
					Dim data_ As System.String = entity.ClientCountry
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientCountry = Nothing
					Else
						entity.ClientCountry = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientEmail As System.String 
				Get
					Dim data_ As System.String = entity.ClientEmail
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientEmail = Nothing
					Else
						entity.ClientEmail = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientMobile As System.String 
				Get
					Dim data_ As System.String = entity.ClientMobile
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientMobile = Nothing
					Else
						entity.ClientMobile = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientName As System.String 
				Get
					Dim data_ As System.String = entity.ClientName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientName = Nothing
					Else
						entity.ClientName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientProvince As System.String 
				Get
					Dim data_ As System.String = entity.ClientProvince
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientProvince = Nothing
					Else
						entity.ClientProvince = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DetailAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.DetailAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DetailAmount = Nothing
					Else
						entity.DetailAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDPayableLocation As System.String 
				Get
					Dim data_ As System.String = entity.DDPayableLocation
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDPayableLocation = Nothing
					Else
						entity.DDPayableLocation = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentNo As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentNo = Nothing
					Else
						entity.InstrumentNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceNo As System.String 
				Get
					Dim data_ As System.String = entity.InvoiceNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceNo = Nothing
					Else
						entity.InvoiceNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentMode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentMode = Nothing
					Else
						entity.PaymentMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationName As System.String 
				Get
					Dim data_ As System.String = entity.PrintLocationName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationName = Nothing
					Else
						entity.PrintLocationName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.TransactionDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionDate = Nothing
					Else
						entity.TransactionDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property TXNCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TXNCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TXNCode = Nothing
					Else
						entity.TXNCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ValueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ValueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ValueDate = Nothing
					Else
						entity.ValueDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrinted As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrinted
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrinted = Nothing
					Else
						entity.IsPrinted = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRePrinted As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRePrinted
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRePrinted = Nothing
					Else
						entity.IsRePrinted = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property RePrintCounter As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RePrintCounter
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RePrintCounter = Nothing
					Else
						entity.RePrintCounter = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReIssued As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReIssued
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReIssued = Nothing
					Else
						entity.IsReIssued = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReValidated As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReValidated
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReValidated = Nothing
					Else
						entity.IsReValidated = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField1 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField1 = Nothing
					Else
						entity.ReferenceField1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField2 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField2 = Nothing
					Else
						entity.ReferenceField2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceField3 As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceField3
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceField3 = Nothing
					Else
						entity.ReferenceField3 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountBranchCode = Nothing
					Else
						entity.ClientAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountCurrency = Nothing
					Else
						entity.ClientAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BeneAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAccountBranchCode = Nothing
					Else
						entity.BeneAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.BeneAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAccountCurrency = Nothing
					Else
						entity.BeneAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountNumber = Nothing
					Else
						entity.PayableAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountBranchCode = Nothing
					Else
						entity.PayableAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountCurrency = Nothing
					Else
						entity.PayableAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountNo = Nothing
					Else
						entity.ClearingAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountBranchCode = Nothing
					Else
						entity.ClearingAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountCurrency = Nothing
					Else
						entity.ClearingAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DDDrawnCityCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DDDrawnCityCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDDrawnCityCode = Nothing
					Else
						entity.DDDrawnCityCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintLocationCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationCode = Nothing
					Else
						entity.PrintLocationCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedOfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOfficeCode = Nothing
					Else
						entity.CreatedOfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileBatchNo As System.String 
				Get
					Dim data_ As System.String = entity.FileBatchNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileBatchNo = Nothing
					Else
						entity.FileBatchNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReferenceNo As System.String 
				Get
					Dim data_ As System.String = entity.ReferenceNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReferenceNo = Nothing
					Else
						entity.ReferenceNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreviousPrintLocationName As System.String 
				Get
					Dim data_ As System.String = entity.PreviousPrintLocationName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreviousPrintLocationName = Nothing
					Else
						entity.PreviousPrintLocationName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreviousPrintLocationCode As System.String 
				Get
					Dim data_ As System.String = entity.PreviousPrintLocationCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreviousPrintLocationCode = Nothing
					Else
						entity.PreviousPrintLocationCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property VerificationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.VerificationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.VerificationDate = Nothing
					Else
						entity.VerificationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovalDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovalDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalDate = Nothing
					Else
						entity.ApprovalDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StaleDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StaleDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StaleDate = Nothing
					Else
						entity.StaleDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CancellationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CancellationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CancellationDate = Nothing
					Else
						entity.CancellationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property LastPrintDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.LastPrintDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.LastPrintDate = Nothing
					Else
						entity.LastPrintDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property RevalidationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.RevalidationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RevalidationDate = Nothing
					Else
						entity.RevalidationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DrawnOnBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.DrawnOnBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DrawnOnBranchCode = Nothing
					Else
						entity.DrawnOnBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientPrimaryApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClientPrimaryApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientPrimaryApprovedBy = Nothing
					Else
						entity.ClientPrimaryApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientSecondaryApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClientSecondaryApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientSecondaryApprovedBy = Nothing
					Else
						entity.ClientSecondaryApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientPrimaryApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ClientPrimaryApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientPrimaryApprovedDate = Nothing
					Else
						entity.ClientPrimaryApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientSecondaryApprovalDateTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ClientSecondaryApprovalDateTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientSecondaryApprovalDateTime = Nothing
					Else
						entity.ClientSecondaryApprovalDateTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankSecondaryApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankSecondaryApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankSecondaryApprovedBy = Nothing
					Else
						entity.BankSecondaryApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankPrimaryApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.BankPrimaryApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankPrimaryApprovedDate = Nothing
					Else
						entity.BankPrimaryApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankSecondaryApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.BankSecondaryApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankSecondaryApprovedDate = Nothing
					Else
						entity.BankSecondaryApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankPrimaryApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankPrimaryApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankPrimaryApprovedBy = Nothing
					Else
						entity.BankPrimaryApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationAmendedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintLocationAmendedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationAmendedBy = Nothing
					Else
						entity.PrintLocationAmendedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationAmendmentDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PrintLocationAmendmentDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationAmendmentDate = Nothing
					Else
						entity.PrintLocationAmendmentDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationAmendmentApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintLocationAmendmentApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationAmendmentApprovedBy = Nothing
					Else
						entity.PrintLocationAmendmentApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationApprovedAmendmentDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PrintLocationApprovedAmendmentDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationApprovedAmendmentDate = Nothing
					Else
						entity.PrintLocationApprovedAmendmentDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrintLocationAmendmentApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrintLocationAmendmentApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrintLocationAmendmentApproved = Nothing
					Else
						entity.IsPrintLocationAmendmentApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationAmendmentCancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrintLocationAmendmentCancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationAmendmentCancelledBy = Nothing
					Else
						entity.PrintLocationAmendmentCancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintLocationAmendmentCancellDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PrintLocationAmendmentCancellDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintLocationAmendmentCancellDate = Nothing
					Else
						entity.PrintLocationAmendmentCancellDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrintLocationAmendmentCancelled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrintLocationAmendmentCancelled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrintLocationAmendmentCancelled = Nothing
					Else
						entity.IsPrintLocationAmendmentCancelled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsPrintLocationAmended As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsPrintLocationAmended
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsPrintLocationAmended = Nothing
					Else
						entity.IsPrintLocationAmended = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReprintingAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReprintingAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReprintingAllowed = Nothing
					Else
						entity.IsReprintingAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReprintingAllowedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReprintingAllowedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReprintingAllowedDate = Nothing
					Else
						entity.ReprintingAllowedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReprintingAllowedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReprintingAllowedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReprintingAllowedBy = Nothing
					Else
						entity.ReprintingAllowedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowReprintingApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowReprintingApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowReprintingApproved = Nothing
					Else
						entity.IsAllowReprintingApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowReprintingApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AllowReprintingApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowReprintingApprovedBy = Nothing
					Else
						entity.AllowReprintingApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowReprintingApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AllowReprintingApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowReprintingApprovedDate = Nothing
					Else
						entity.AllowReprintingApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowReprintingCancelled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowReprintingCancelled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowReprintingCancelled = Nothing
					Else
						entity.IsAllowReprintingCancelled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReprintingAllowedCancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReprintingAllowedCancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReprintingAllowedCancelledBy = Nothing
					Else
						entity.ReprintingAllowedCancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReprintingAllowCancelledDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReprintingAllowCancelledDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReprintingAllowCancelledDate = Nothing
					Else
						entity.ReprintingAllowCancelledDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReIssuanceAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReIssuanceAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReIssuanceAllowed = Nothing
					Else
						entity.IsReIssuanceAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReissuanceAlloweBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReissuanceAlloweBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReissuanceAlloweBy = Nothing
					Else
						entity.ReissuanceAlloweBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReIssuanceAllowedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReIssuanceAllowedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReIssuanceAllowedDate = Nothing
					Else
						entity.ReIssuanceAllowedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowedReIssuanceApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowedReIssuanceApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowedReIssuanceApproved = Nothing
					Else
						entity.IsAllowedReIssuanceApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowReIssuanceApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AllowReIssuanceApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowReIssuanceApprovedDate = Nothing
					Else
						entity.AllowReIssuanceApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowedReIssuanceApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AllowedReIssuanceApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowedReIssuanceApprovedBy = Nothing
					Else
						entity.AllowedReIssuanceApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAllowedReIssuanceCancelled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAllowedReIssuanceCancelled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAllowedReIssuanceCancelled = Nothing
					Else
						entity.IsAllowedReIssuanceCancelled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowedReIssuanceCancelledDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.AllowedReIssuanceCancelledDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowedReIssuanceCancelledDate = Nothing
					Else
						entity.AllowedReIssuanceCancelledDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AllowedReIssuanceCancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AllowedReIssuanceCancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AllowedReIssuanceCancelledBy = Nothing
					Else
						entity.AllowedReIssuanceCancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReIssuanceID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReIssuanceID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReIssuanceID = Nothing
					Else
						entity.ReIssuanceID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property NewInstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.NewInstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NewInstructionID = Nothing
					Else
						entity.NewInstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PhysicalInstrumentNumber As System.String 
				Get
					Dim data_ As System.String = entity.PhysicalInstrumentNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PhysicalInstrumentNumber = Nothing
					Else
						entity.PhysicalInstrumentNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValiDatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReValiDatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValiDatedDate = Nothing
					Else
						entity.ReValiDatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValiDationAllowedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReValiDationAllowedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValiDationAllowedBy = Nothing
					Else
						entity.ReValiDationAllowedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReValiDationAllowed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReValiDationAllowed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReValiDationAllowed = Nothing
					Else
						entity.IsReValiDationAllowed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValidationApprovalDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReValidationApprovalDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValidationApprovalDate = Nothing
					Else
						entity.ReValidationApprovalDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValidationApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReValidationApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValidationApprovedBy = Nothing
					Else
						entity.ReValidationApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReValiDationApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReValiDationApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReValiDationApproved = Nothing
					Else
						entity.IsReValiDationApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValiDationCancelDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ReValiDationCancelDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValiDationCancelDate = Nothing
					Else
						entity.ReValiDationCancelDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReValiDationCancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReValiDationCancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReValiDationCancelledBy = Nothing
					Else
						entity.ReValiDationCancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsReValiDationCancelled As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsReValiDationCancelled
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsReValiDationCancelled = Nothing
					Else
						entity.IsReValiDationCancelled = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingBankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingBankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingBankCode = Nothing
					Else
						entity.ClearingBankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ClearingDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingDate = Nothing
					Else
						entity.ClearingDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingOfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingOfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingOfficeCode = Nothing
					Else
						entity.ClearingOfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearedBy = Nothing
					Else
						entity.ClearedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingApprovedBy = Nothing
					Else
						entity.ClearingApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ClearingApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingApprovedDate = Nothing
					Else
						entity.ClearingApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingApprovedOfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClearingApprovedOfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingApprovedOfficeCode = Nothing
					Else
						entity.ClearingApprovedOfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmendedPrintLocationCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AmendedPrintLocationCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmendedPrintLocationCode = Nothing
					Else
						entity.AmendedPrintLocationCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmendedPrintLocationName As System.String 
				Get
					Dim data_ As System.String = entity.AmendedPrintLocationName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmendedPrintLocationName = Nothing
					Else
						entity.AmendedPrintLocationName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableClientNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableClientNo = Nothing
					Else
						entity.PayableClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableSeqNo = Nothing
					Else
						entity.PayableSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.PayableProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableProfitCentre = Nothing
					Else
						entity.PayableProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountType As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountType = Nothing
					Else
						entity.PayableAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingClientNo As System.String 
				Get
					Dim data_ As System.String = entity.ClearingClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingClientNo = Nothing
					Else
						entity.ClearingClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.ClearingSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingSeqNo = Nothing
					Else
						entity.ClearingSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.ClearingProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingProfitCentre = Nothing
					Else
						entity.ClearingProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingAccountType = Nothing
					Else
						entity.ClearingAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrintDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PrintDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrintDate = Nothing
					Else
						entity.PrintDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAmendmentComplete As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAmendmentComplete
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAmendmentComplete = Nothing
					Else
						entity.IsAmendmentComplete = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClubID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ClubID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClubID = Nothing
					Else
						entity.ClubID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SkipReason As System.String 
				Get
					Dim data_ As System.String = entity.SkipReason
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SkipReason = Nothing
					Else
						entity.SkipReason = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneBranchAddress As System.String 
				Get
					Dim data_ As System.String = entity.BeneBranchAddress
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneBranchAddress = Nothing
					Else
						entity.BeneBranchAddress = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusCancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusCancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusCancelledBy = Nothing
					Else
						entity.StatusCancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusCancelledDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusCancelledDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusCancelledDate = Nothing
					Else
						entity.StatusCancelledDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusReversedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusReversedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusReversedBy = Nothing
					Else
						entity.StatusReversedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusReversedOfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusReversedOfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusReversedOfficeCode = Nothing
					Else
						entity.StatusReversedOfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusReversedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusReversedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusReversedDate = Nothing
					Else
						entity.StatusReversedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusSettledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusSettledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusSettledBy = Nothing
					Else
						entity.StatusSettledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusSettledDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusSettledDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusSettledDate = Nothing
					Else
						entity.StatusSettledDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusCancelledApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusCancelledApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusCancelledApprovedBy = Nothing
					Else
						entity.StatusCancelledApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusSettledApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusSettledApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusSettledApprovedBy = Nothing
					Else
						entity.StatusSettledApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusReversedApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusReversedApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusReversedApprovedBy = Nothing
					Else
						entity.StatusReversedApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusCancelledApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusCancelledApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusCancelledApprovedDate = Nothing
					Else
						entity.StatusCancelledApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusSettledApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusSettledApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusSettledApprovedDate = Nothing
					Else
						entity.StatusSettledApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusReversedApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusReversedApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusReversedApprovedDate = Nothing
					Else
						entity.StatusReversedApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusChangeRemarks As System.String 
				Get
					Dim data_ As System.String = entity.StatusChangeRemarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusChangeRemarks = Nothing
					Else
						entity.StatusChangeRemarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusApproveRemarks As System.String 
				Get
					Dim data_ As System.String = entity.StatusApproveRemarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusApproveRemarks = Nothing
					Else
						entity.StatusApproveRemarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusChangeApprovalCancelledby As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusChangeApprovalCancelledby
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusChangeApprovalCancelledby = Nothing
					Else
						entity.StatusChangeApprovalCancelledby = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusChangeApprovalCancelledDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusChangeApprovalCancelledDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusChangeApprovalCancelledDate = Nothing
					Else
						entity.StatusChangeApprovalCancelledDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClearingType As System.String 
				Get
					Dim data_ As System.String = entity.ClearingType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClearingType = Nothing
					Else
						entity.ClearingType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ScheduleTransactionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ScheduleTransactionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleTransactionID = Nothing
					Else
						entity.ScheduleTransactionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ScheduleTransactionTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ScheduleTransactionTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ScheduleTransactionTime = Nothing
					Else
						entity.ScheduleTransactionTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsCheduleTransactionProcessed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsCheduleTransactionProcessed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsCheduleTransactionProcessed = Nothing
					Else
						entity.IsCheduleTransactionProcessed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIDType As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIDType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIDType = Nothing
					Else
						entity.BeneficiaryIDType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficiaryIDNo As System.String 
				Get
					Dim data_ As System.String = entity.BeneficiaryIDNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficiaryIDNo = Nothing
					Else
						entity.BeneficiaryIDNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Rin As System.String 
				Get
					Dim data_ As System.String = entity.Rin
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Rin = Nothing
					Else
						entity.Rin = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DisbursedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursedBy = Nothing
					Else
						entity.DisbursedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.DisbursedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursedDate = Nothing
					Else
						entity.DisbursedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisbursedBranchCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DisbursedBranchCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisbursedBranchCode = Nothing
					Else
						entity.DisbursedBranchCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CancelledBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CancelledBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CancelledBy = Nothing
					Else
						entity.CancelledBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CancelledApproveBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CancelledApproveBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CancelledApproveBy = Nothing
					Else
						entity.CancelledApproveBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CancelledApprovalDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CancelledApprovalDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CancelledApprovalDate = Nothing
					Else
						entity.CancelledApprovalDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property UBPCompanyID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UBPCompanyID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UBPCompanyID = Nothing
					Else
						entity.UBPCompanyID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UBPReferenceField As System.String 
				Get
					Dim data_ As System.String = entity.UBPReferenceField
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UBPReferenceField = Nothing
					Else
						entity.UBPReferenceField = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneficaryAccountType As System.String 
				Get
					Dim data_ As System.String = entity.BeneficaryAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneficaryAccountType = Nothing
					Else
						entity.BeneficaryAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BeneID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneID = Nothing
					Else
						entity.BeneID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReturnReason As System.String 
				Get
					Dim data_ As System.String = entity.ReturnReason
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReturnReason = Nothing
					Else
						entity.ReturnReason = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountBeforeDueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.AmountBeforeDueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountBeforeDueDate = Nothing
					Else
						entity.AmountBeforeDueDate = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountAfterDueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.AmountAfterDueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountAfterDueDate = Nothing
					Else
						entity.AmountAfterDueDate = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property DueDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.DueDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DueDate = Nothing
					Else
						entity.DueDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusHoldBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusHoldBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusHoldBy = Nothing
					Else
						entity.StatusHoldBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusHoldDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusHoldDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusHoldDate = Nothing
					Else
						entity.StatusHoldDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusHoldApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusHoldApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusHoldApprovedBy = Nothing
					Else
						entity.StatusHoldApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusHoldApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusHoldApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusHoldApprovedDate = Nothing
					Else
						entity.StatusHoldApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusUnHoldBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusUnHoldBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusUnHoldBy = Nothing
					Else
						entity.StatusUnHoldBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusUnHoldDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusUnHoldDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusUnHoldDate = Nothing
					Else
						entity.StatusUnHoldDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusUnHoldApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusUnHoldApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusUnHoldApprovedBy = Nothing
					Else
						entity.StatusUnHoldApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusUnHoldApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusUnHoldApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusUnHoldApprovedDate = Nothing
					Else
						entity.StatusUnHoldApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusRevertBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusRevertBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusRevertBy = Nothing
					Else
						entity.StatusRevertBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusRevertDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusRevertDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusRevertDate = Nothing
					Else
						entity.StatusRevertDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusRevertApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.StatusRevertApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusRevertApprovedBy = Nothing
					Else
						entity.StatusRevertApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property StatusRevertApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.StatusRevertApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.StatusRevertApprovedDate = Nothing
					Else
						entity.StatusRevertApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IBFTStan As System.String 
				Get
					Dim data_ As System.String = entity.IBFTStan
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IBFTStan = Nothing
					Else
						entity.IBFTStan = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FinalStatus As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FinalStatus
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FinalStatus = Nothing
					Else
						entity.FinalStatus = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICInstruction
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICInstructionQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICInstructionQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICInstructionQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICInstructionQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICInstructionCollection
		Inherits esEntityCollection(Of ICInstruction)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICInstructionMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICInstructionCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICInstructionQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICInstructionQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICInstructionQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICInstructionQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICInstructionQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICInstructionQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICInstructionQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICInstructionQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICInstructionMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "InstructionID" 
					Return Me.InstructionID
				Case "Description" 
					Return Me.Description
				Case "Remarks" 
					Return Me.Remarks
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Status" 
					Return Me.Status
				Case "FileID" 
					Return Me.FileID
				Case "AcquisitionMode" 
					Return Me.AcquisitionMode
				Case "LastStatus" 
					Return Me.LastStatus
				Case "ErrorMessage" 
					Return Me.ErrorMessage
				Case "Amount" 
					Return Me.Amount
				Case "AmountInWords" 
					Return Me.AmountInWords
				Case "BeneficiaryAccountNo" 
					Return Me.BeneficiaryAccountNo
				Case "BeneficiaryAccountTitle" 
					Return Me.BeneficiaryAccountTitle
				Case "BeneficiaryAddress" 
					Return Me.BeneficiaryAddress
				Case "BeneficiaryBankCode" 
					Return Me.BeneficiaryBankCode
				Case "BeneficiaryBankName" 
					Return Me.BeneficiaryBankName
				Case "BeneficiaryBankAddress" 
					Return Me.BeneficiaryBankAddress
				Case "BeneficiaryBranchCode" 
					Return Me.BeneficiaryBranchCode
				Case "BeneficiaryBranchName" 
					Return Me.BeneficiaryBranchName
				Case "BeneficiaryCity" 
					Return Me.BeneficiaryCity
				Case "BeneficiaryCNIC" 
					Return Me.BeneficiaryCNIC
				Case "BeneficiaryCode" 
					Return Me.BeneficiaryCode
				Case "BeneficiaryCountry" 
					Return Me.BeneficiaryCountry
				Case "BeneficiaryEmail" 
					Return Me.BeneficiaryEmail
				Case "BeneficiaryMobile" 
					Return Me.BeneficiaryMobile
				Case "BeneficiaryName" 
					Return Me.BeneficiaryName
				Case "BeneficiaryPhone" 
					Return Me.BeneficiaryPhone
				Case "BeneficiaryProvince" 
					Return Me.BeneficiaryProvince
				Case "BeneficiaryType" 
					Return Me.BeneficiaryType
				Case "ClientAccountNo" 
					Return Me.ClientAccountNo
				Case "ClientAddress" 
					Return Me.ClientAddress
				Case "ClientBankCode" 
					Return Me.ClientBankCode
				Case "ClientBankName" 
					Return Me.ClientBankName
				Case "ClientBranchAddress" 
					Return Me.ClientBranchAddress
				Case "ClientBranchCode" 
					Return Me.ClientBranchCode
				Case "ClientBranchName" 
					Return Me.ClientBranchName
				Case "ClientCity" 
					Return Me.ClientCity
				Case "ClientCountry" 
					Return Me.ClientCountry
				Case "ClientEmail" 
					Return Me.ClientEmail
				Case "ClientMobile" 
					Return Me.ClientMobile
				Case "ClientName" 
					Return Me.ClientName
				Case "ClientProvince" 
					Return Me.ClientProvince
				Case "DetailAmount" 
					Return Me.DetailAmount
				Case "DDPayableLocation" 
					Return Me.DDPayableLocation
				Case "InstrumentNo" 
					Return Me.InstrumentNo
				Case "InvoiceNo" 
					Return Me.InvoiceNo
				Case "PaymentMode" 
					Return Me.PaymentMode
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "PrintLocationName" 
					Return Me.PrintLocationName
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "TransactionDate" 
					Return Me.TransactionDate
				Case "TXNCode" 
					Return Me.TXNCode
				Case "ValueDate" 
					Return Me.ValueDate
				Case "IsPrinted" 
					Return Me.IsPrinted
				Case "IsRePrinted" 
					Return Me.IsRePrinted
				Case "RePrintCounter" 
					Return Me.RePrintCounter
				Case "IsReIssued" 
					Return Me.IsReIssued
				Case "IsReValidated" 
					Return Me.IsReValidated
				Case "ReferenceField1" 
					Return Me.ReferenceField1
				Case "ReferenceField2" 
					Return Me.ReferenceField2
				Case "ReferenceField3" 
					Return Me.ReferenceField3
				Case "ClientAccountBranchCode" 
					Return Me.ClientAccountBranchCode
				Case "ClientAccountCurrency" 
					Return Me.ClientAccountCurrency
				Case "BeneAccountBranchCode" 
					Return Me.BeneAccountBranchCode
				Case "BeneAccountCurrency" 
					Return Me.BeneAccountCurrency
				Case "GroupCode" 
					Return Me.GroupCode
				Case "PayableAccountNumber" 
					Return Me.PayableAccountNumber
				Case "PayableAccountBranchCode" 
					Return Me.PayableAccountBranchCode
				Case "PayableAccountCurrency" 
					Return Me.PayableAccountCurrency
				Case "ClearingAccountNo" 
					Return Me.ClearingAccountNo
				Case "ClearingAccountBranchCode" 
					Return Me.ClearingAccountBranchCode
				Case "ClearingAccountCurrency" 
					Return Me.ClearingAccountCurrency
				Case "DDDrawnCityCode" 
					Return Me.DDDrawnCityCode
				Case "PrintLocationCode" 
					Return Me.PrintLocationCode
				Case "CreatedOfficeCode" 
					Return Me.CreatedOfficeCode
				Case "FileBatchNo" 
					Return Me.FileBatchNo
				Case "ReferenceNo" 
					Return Me.ReferenceNo
				Case "PreviousPrintLocationName" 
					Return Me.PreviousPrintLocationName
				Case "PreviousPrintLocationCode" 
					Return Me.PreviousPrintLocationCode
				Case "VerificationDate" 
					Return Me.VerificationDate
				Case "ApprovalDate" 
					Return Me.ApprovalDate
				Case "StaleDate" 
					Return Me.StaleDate
				Case "CancellationDate" 
					Return Me.CancellationDate
				Case "LastPrintDate" 
					Return Me.LastPrintDate
				Case "RevalidationDate" 
					Return Me.RevalidationDate
				Case "DrawnOnBranchCode" 
					Return Me.DrawnOnBranchCode
				Case "ClientPrimaryApprovedBy" 
					Return Me.ClientPrimaryApprovedBy
				Case "ClientSecondaryApprovedBy" 
					Return Me.ClientSecondaryApprovedBy
				Case "ClientPrimaryApprovedDate" 
					Return Me.ClientPrimaryApprovedDate
				Case "ClientSecondaryApprovalDateTime" 
					Return Me.ClientSecondaryApprovalDateTime
				Case "BankSecondaryApprovedBy" 
					Return Me.BankSecondaryApprovedBy
				Case "BankPrimaryApprovedDate" 
					Return Me.BankPrimaryApprovedDate
				Case "BankSecondaryApprovedDate" 
					Return Me.BankSecondaryApprovedDate
				Case "BankPrimaryApprovedBy" 
					Return Me.BankPrimaryApprovedBy
				Case "PrintLocationAmendedBy" 
					Return Me.PrintLocationAmendedBy
				Case "PrintLocationAmendmentDate" 
					Return Me.PrintLocationAmendmentDate
				Case "PrintLocationAmendmentApprovedBy" 
					Return Me.PrintLocationAmendmentApprovedBy
				Case "PrintLocationApprovedAmendmentDate" 
					Return Me.PrintLocationApprovedAmendmentDate
				Case "IsPrintLocationAmendmentApproved" 
					Return Me.IsPrintLocationAmendmentApproved
				Case "PrintLocationAmendmentCancelledBy" 
					Return Me.PrintLocationAmendmentCancelledBy
				Case "PrintLocationAmendmentCancellDate" 
					Return Me.PrintLocationAmendmentCancellDate
				Case "IsPrintLocationAmendmentCancelled" 
					Return Me.IsPrintLocationAmendmentCancelled
				Case "IsPrintLocationAmended" 
					Return Me.IsPrintLocationAmended
				Case "IsReprintingAllowed" 
					Return Me.IsReprintingAllowed
				Case "ReprintingAllowedDate" 
					Return Me.ReprintingAllowedDate
				Case "ReprintingAllowedBy" 
					Return Me.ReprintingAllowedBy
				Case "IsAllowReprintingApproved" 
					Return Me.IsAllowReprintingApproved
				Case "AllowReprintingApprovedBy" 
					Return Me.AllowReprintingApprovedBy
				Case "AllowReprintingApprovedDate" 
					Return Me.AllowReprintingApprovedDate
				Case "IsAllowReprintingCancelled" 
					Return Me.IsAllowReprintingCancelled
				Case "ReprintingAllowedCancelledBy" 
					Return Me.ReprintingAllowedCancelledBy
				Case "ReprintingAllowCancelledDate" 
					Return Me.ReprintingAllowCancelledDate
				Case "IsReIssuanceAllowed" 
					Return Me.IsReIssuanceAllowed
				Case "ReissuanceAlloweBy" 
					Return Me.ReissuanceAlloweBy
				Case "ReIssuanceAllowedDate" 
					Return Me.ReIssuanceAllowedDate
				Case "IsAllowedReIssuanceApproved" 
					Return Me.IsAllowedReIssuanceApproved
				Case "AllowReIssuanceApprovedDate" 
					Return Me.AllowReIssuanceApprovedDate
				Case "AllowedReIssuanceApprovedBy" 
					Return Me.AllowedReIssuanceApprovedBy
				Case "IsAllowedReIssuanceCancelled" 
					Return Me.IsAllowedReIssuanceCancelled
				Case "AllowedReIssuanceCancelledDate" 
					Return Me.AllowedReIssuanceCancelledDate
				Case "AllowedReIssuanceCancelledBy" 
					Return Me.AllowedReIssuanceCancelledBy
				Case "ReIssuanceID" 
					Return Me.ReIssuanceID
				Case "NewInstructionID" 
					Return Me.NewInstructionID
				Case "PhysicalInstrumentNumber" 
					Return Me.PhysicalInstrumentNumber
				Case "ReValiDatedDate" 
					Return Me.ReValiDatedDate
				Case "ReValiDationAllowedBy" 
					Return Me.ReValiDationAllowedBy
				Case "IsReValiDationAllowed" 
					Return Me.IsReValiDationAllowed
				Case "ReValidationApprovalDate" 
					Return Me.ReValidationApprovalDate
				Case "ReValidationApprovedBy" 
					Return Me.ReValidationApprovedBy
				Case "IsReValiDationApproved" 
					Return Me.IsReValiDationApproved
				Case "ReValiDationCancelDate" 
					Return Me.ReValiDationCancelDate
				Case "ReValiDationCancelledBy" 
					Return Me.ReValiDationCancelledBy
				Case "IsReValiDationCancelled" 
					Return Me.IsReValiDationCancelled
				Case "ClearingBankCode" 
					Return Me.ClearingBankCode
				Case "ClearingDate" 
					Return Me.ClearingDate
				Case "ClearingOfficeCode" 
					Return Me.ClearingOfficeCode
				Case "ClearedBy" 
					Return Me.ClearedBy
				Case "ClearingApprovedBy" 
					Return Me.ClearingApprovedBy
				Case "ClearingApprovedDate" 
					Return Me.ClearingApprovedDate
				Case "ClearingApprovedOfficeCode" 
					Return Me.ClearingApprovedOfficeCode
				Case "AmendedPrintLocationCode" 
					Return Me.AmendedPrintLocationCode
				Case "AmendedPrintLocationName" 
					Return Me.AmendedPrintLocationName
				Case "PayableClientNo" 
					Return Me.PayableClientNo
				Case "PayableSeqNo" 
					Return Me.PayableSeqNo
				Case "PayableProfitCentre" 
					Return Me.PayableProfitCentre
				Case "PayableAccountType" 
					Return Me.PayableAccountType
				Case "ClearingClientNo" 
					Return Me.ClearingClientNo
				Case "ClearingSeqNo" 
					Return Me.ClearingSeqNo
				Case "ClearingProfitCentre" 
					Return Me.ClearingProfitCentre
				Case "ClearingAccountType" 
					Return Me.ClearingAccountType
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "PrintDate" 
					Return Me.PrintDate
				Case "IsAmendmentComplete" 
					Return Me.IsAmendmentComplete
				Case "ClubID" 
					Return Me.ClubID
				Case "SkipReason" 
					Return Me.SkipReason
				Case "BeneBranchAddress" 
					Return Me.BeneBranchAddress
				Case "StatusCancelledBy" 
					Return Me.StatusCancelledBy
				Case "StatusCancelledDate" 
					Return Me.StatusCancelledDate
				Case "StatusReversedBy" 
					Return Me.StatusReversedBy
				Case "StatusReversedOfficeCode" 
					Return Me.StatusReversedOfficeCode
				Case "StatusReversedDate" 
					Return Me.StatusReversedDate
				Case "StatusSettledBy" 
					Return Me.StatusSettledBy
				Case "StatusSettledDate" 
					Return Me.StatusSettledDate
				Case "StatusCancelledApprovedBy" 
					Return Me.StatusCancelledApprovedBy
				Case "StatusSettledApprovedBy" 
					Return Me.StatusSettledApprovedBy
				Case "StatusReversedApprovedBy" 
					Return Me.StatusReversedApprovedBy
				Case "StatusCancelledApprovedDate" 
					Return Me.StatusCancelledApprovedDate
				Case "StatusSettledApprovedDate" 
					Return Me.StatusSettledApprovedDate
				Case "StatusReversedApprovedDate" 
					Return Me.StatusReversedApprovedDate
				Case "StatusChangeRemarks" 
					Return Me.StatusChangeRemarks
				Case "StatusApproveRemarks" 
					Return Me.StatusApproveRemarks
				Case "StatusChangeApprovalCancelledby" 
					Return Me.StatusChangeApprovalCancelledby
				Case "StatusChangeApprovalCancelledDate" 
					Return Me.StatusChangeApprovalCancelledDate
				Case "ClearingType" 
					Return Me.ClearingType
				Case "ScheduleTransactionID" 
					Return Me.ScheduleTransactionID
				Case "ScheduleTransactionTime" 
					Return Me.ScheduleTransactionTime
				Case "IsCheduleTransactionProcessed" 
					Return Me.IsCheduleTransactionProcessed
				Case "BeneficiaryIDType" 
					Return Me.BeneficiaryIDType
				Case "BeneficiaryIDNo" 
					Return Me.BeneficiaryIDNo
				Case "Rin" 
					Return Me.Rin
				Case "DisbursedBy" 
					Return Me.DisbursedBy
				Case "DisbursedDate" 
					Return Me.DisbursedDate
				Case "DisbursedBranchCode" 
					Return Me.DisbursedBranchCode
				Case "CancelledBy" 
					Return Me.CancelledBy
				Case "CancelledApproveBy" 
					Return Me.CancelledApproveBy
				Case "CancelledApprovalDate" 
					Return Me.CancelledApprovalDate
				Case "UBPCompanyID" 
					Return Me.UBPCompanyID
				Case "UBPReferenceField" 
					Return Me.UBPReferenceField
				Case "BeneficaryAccountType" 
					Return Me.BeneficaryAccountType
				Case "BeneID" 
					Return Me.BeneID
				Case "ReturnReason" 
					Return Me.ReturnReason
				Case "AmountBeforeDueDate" 
					Return Me.AmountBeforeDueDate
				Case "AmountAfterDueDate" 
					Return Me.AmountAfterDueDate
				Case "DueDate" 
					Return Me.DueDate
				Case "StatusHoldBy" 
					Return Me.StatusHoldBy
				Case "StatusHoldDate" 
					Return Me.StatusHoldDate
				Case "StatusHoldApprovedBy" 
					Return Me.StatusHoldApprovedBy
				Case "StatusHoldApprovedDate" 
					Return Me.StatusHoldApprovedDate
				Case "StatusUnHoldBy" 
					Return Me.StatusUnHoldBy
				Case "StatusUnHoldDate" 
					Return Me.StatusUnHoldDate
				Case "StatusUnHoldApprovedBy" 
					Return Me.StatusUnHoldApprovedBy
				Case "StatusUnHoldApprovedDate" 
					Return Me.StatusUnHoldApprovedDate
				Case "StatusRevertBy" 
					Return Me.StatusRevertBy
				Case "StatusRevertDate" 
					Return Me.StatusRevertDate
				Case "StatusRevertApprovedBy" 
					Return Me.StatusRevertApprovedBy
				Case "StatusRevertApprovedDate" 
					Return Me.StatusRevertApprovedDate
				Case "IBFTStan" 
					Return Me.IBFTStan
				Case "FinalStatus" 
					Return Me.FinalStatus
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Description As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Description, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Remarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Remarks, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Status, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FileID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.FileID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AcquisitionMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AcquisitionMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property LastStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.LastStatus, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ErrorMessage As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ErrorMessage, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property AmountInWords As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AmountInWords, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAccountTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryAccountTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryBankCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryBankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBankAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryBankAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryBranchName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryBranchName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCity As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryCity, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCNIC As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryCNIC, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryCountry As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryCountry, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryEmail As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryEmail, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryMobile As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryMobile, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryPhone As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryPhone, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryProvince As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryProvince, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientBankCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientBankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientBankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientBranchAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientBranchAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientBranchName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientBranchName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientCity As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientCity, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientCountry As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientCountry, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientEmail As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientEmail, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientMobile As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientMobile, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientProvince As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientProvince, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DetailAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DetailAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DDPayableLocation As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DDPayableLocation, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.InstrumentNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.InvoiceNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PaymentMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.TransactionDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property TXNCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.TXNCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ValueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ValueDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrinted As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsPrinted, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsRePrinted As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsRePrinted, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property RePrintCounter As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.RePrintCounter, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsReIssued As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReIssued, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsReValidated As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReValidated, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReferenceField1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReferenceField2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceField3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReferenceField3, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DDDrawnCityCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DDDrawnCityCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CreatedOfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FileBatchNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.FileBatchNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReferenceNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReferenceNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PreviousPrintLocationName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PreviousPrintLocationName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PreviousPrintLocationCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PreviousPrintLocationCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property VerificationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.VerificationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovalDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ApprovalDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StaleDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StaleDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CancellationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CancellationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property LastPrintDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.LastPrintDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property RevalidationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.RevalidationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DrawnOnBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DrawnOnBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientPrimaryApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClientSecondaryApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientSecondaryApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClientPrimaryApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ClientSecondaryApprovalDateTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClientSecondaryApprovalDateTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property BankSecondaryApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BankSecondaryApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BankPrimaryApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BankPrimaryApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property BankSecondaryApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BankSecondaryApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property BankPrimaryApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BankPrimaryApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationAmendedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationAmendedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationAmendmentDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationAmendmentDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationAmendmentApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationAmendmentApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationApprovedAmendmentDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationApprovedAmendmentDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrintLocationAmendmentApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationAmendmentCancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PrintLocationAmendmentCancellDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancellDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrintLocationAmendmentCancelled As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentCancelled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsPrintLocationAmended As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsPrintLocationAmended, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsReprintingAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReprintingAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReprintingAllowedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReprintingAllowedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReprintingAllowedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReprintingAllowedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowReprintingApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsAllowReprintingApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property AllowReprintingApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowReprintingApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AllowReprintingApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowReprintingApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowReprintingCancelled As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsAllowReprintingCancelled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReprintingAllowedCancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReprintingAllowedCancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReprintingAllowCancelledDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReprintingAllowCancelledDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsReIssuanceAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReIssuanceAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReissuanceAlloweBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReissuanceAlloweBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReIssuanceAllowedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReIssuanceAllowedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowedReIssuanceApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property AllowReIssuanceApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowReIssuanceApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AllowedReIssuanceApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowedReIssuanceApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsAllowedReIssuanceCancelled As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceCancelled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property AllowedReIssuanceCancelledDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AllowedReIssuanceCancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReIssuanceID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReIssuanceID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property NewInstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.NewInstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PhysicalInstrumentNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PhysicalInstrumentNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ReValiDatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValiDatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReValiDationAllowedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValiDationAllowedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsReValiDationAllowed As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReValiDationAllowed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReValidationApprovalDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValidationApprovalDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReValidationApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValidationApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsReValiDationApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReValiDationApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ReValiDationCancelDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValiDationCancelDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ReValiDationCancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReValiDationCancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsReValiDationCancelled As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsReValiDationCancelled, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingBankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingBankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingOfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingOfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClearedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingApprovedOfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingApprovedOfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AmendedPrintLocationCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AmendedPrintLocationCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AmendedPrintLocationName As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AmendedPrintLocationName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PayableAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingProfitCentre, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property PrintDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.PrintDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAmendmentComplete As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsAmendmentComplete, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ClubID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClubID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SkipReason As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.SkipReason, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneBranchAddress As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneBranchAddress, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property StatusCancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusCancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusCancelledDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusCancelledDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusReversedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusReversedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusReversedOfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusReversedOfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusReversedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusReversedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusSettledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusSettledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusSettledDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusSettledDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusCancelledApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusCancelledApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusSettledApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusSettledApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusReversedApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusReversedApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusCancelledApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusCancelledApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusSettledApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusSettledApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusReversedApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusReversedApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusChangeRemarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusChangeRemarks, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property StatusApproveRemarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusApproveRemarks, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property StatusChangeApprovalCancelledby As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledby, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusChangeApprovalCancelledDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ClearingType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ClearingType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ScheduleTransactionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ScheduleTransactionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ScheduleTransactionTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ScheduleTransactionTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsCheduleTransactionProcessed As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IsCheduleTransactionProcessed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIDType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryIDType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficiaryIDNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficiaryIDNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Rin As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.Rin, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DisbursedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DisbursedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DisbursedBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DisbursedBranchCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CancelledBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CancelledBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CancelledApproveBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CancelledApproveBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CancelledApprovalDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.CancelledApprovalDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property UBPCompanyID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.UBPCompanyID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UBPReferenceField As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.UBPReferenceField, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneficaryAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneficaryAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneID As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.BeneID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReturnReason As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.ReturnReason, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property AmountBeforeDueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AmountBeforeDueDate, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property AmountAfterDueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.AmountAfterDueDate, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property DueDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.DueDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusHoldBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusHoldBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusHoldDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusHoldDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusHoldApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusHoldApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusHoldApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusHoldApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusUnHoldBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusUnHoldBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusUnHoldDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusUnHoldDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusUnHoldApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusUnHoldApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusRevertBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusRevertBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusRevertDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusRevertDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property StatusRevertApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusRevertApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property StatusRevertApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.StatusRevertApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IBFTStan As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.IBFTStan, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FinalStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICInstructionMetadata.ColumnNames.FinalStatus, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICInstruction 
		Inherits esICInstruction
		
	
		#Region "ICInstructionActivityCollectionByInstructionID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionActivityCollectionByInstructionID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstruction.ICInstructionActivityCollectionByInstructionID_Delegate)
				map.PropertyName = "ICInstructionActivityCollectionByInstructionID"
				map.MyColumnName = "InstructionID"
				map.ParentColumnName = "InstructionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionActivityCollectionByInstructionID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionQuery(data.NextAlias())
			
			Dim mee As ICInstructionActivityQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionActivityQuery), New ICInstructionActivityQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.InstructionID = mee.InstructionID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_InstructionActivity_IC_Instruction
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionActivityCollectionByInstructionID As ICInstructionActivityCollection 
		
			Get
				If Me._ICInstructionActivityCollectionByInstructionID Is Nothing Then
					Me._ICInstructionActivityCollectionByInstructionID = New ICInstructionActivityCollection()
					Me._ICInstructionActivityCollectionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionActivityCollectionByInstructionID", Me._ICInstructionActivityCollectionByInstructionID)
				
					If Not Me.InstructionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionActivityCollectionByInstructionID.Query.Where(Me._ICInstructionActivityCollectionByInstructionID.Query.InstructionID = Me.InstructionID)
							Me._ICInstructionActivityCollectionByInstructionID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionActivityCollectionByInstructionID.fks.Add(ICInstructionActivityMetadata.ColumnNames.InstructionID, Me.InstructionID)
					End If
				End If

				Return Me._ICInstructionActivityCollectionByInstructionID
			End Get
			
			Set(ByVal value As ICInstructionActivityCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionActivityCollectionByInstructionID Is Nothing Then

					Me.RemovePostSave("ICInstructionActivityCollectionByInstructionID")
					Me._ICInstructionActivityCollectionByInstructionID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionActivityCollectionByInstructionID As ICInstructionActivityCollection
		#End Region

		#Region "ICInstructionApprovalLogCollectionByInstructionID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionApprovalLogCollectionByInstructionID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstruction.ICInstructionApprovalLogCollectionByInstructionID_Delegate)
				map.PropertyName = "ICInstructionApprovalLogCollectionByInstructionID"
				map.MyColumnName = "InstructionID"
				map.ParentColumnName = "InstructionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionApprovalLogCollectionByInstructionID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionQuery(data.NextAlias())
			
			Dim mee As ICInstructionApprovalLogQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionApprovalLogQuery), New ICInstructionApprovalLogQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.InstructionID = mee.InstructionID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_InstructionApprovalLog_IC_Instruction
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionApprovalLogCollectionByInstructionID As ICInstructionApprovalLogCollection 
		
			Get
				If Me._ICInstructionApprovalLogCollectionByInstructionID Is Nothing Then
					Me._ICInstructionApprovalLogCollectionByInstructionID = New ICInstructionApprovalLogCollection()
					Me._ICInstructionApprovalLogCollectionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionApprovalLogCollectionByInstructionID", Me._ICInstructionApprovalLogCollectionByInstructionID)
				
					If Not Me.InstructionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionApprovalLogCollectionByInstructionID.Query.Where(Me._ICInstructionApprovalLogCollectionByInstructionID.Query.InstructionID = Me.InstructionID)
							Me._ICInstructionApprovalLogCollectionByInstructionID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionApprovalLogCollectionByInstructionID.fks.Add(ICInstructionApprovalLogMetadata.ColumnNames.InstructionID, Me.InstructionID)
					End If
				End If

				Return Me._ICInstructionApprovalLogCollectionByInstructionID
			End Get
			
			Set(ByVal value As ICInstructionApprovalLogCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionApprovalLogCollectionByInstructionID Is Nothing Then

					Me.RemovePostSave("ICInstructionApprovalLogCollectionByInstructionID")
					Me._ICInstructionApprovalLogCollectionByInstructionID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionApprovalLogCollectionByInstructionID As ICInstructionApprovalLogCollection
		#End Region

		#Region "ICInstructionDetailCollectionByInstructionID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstructionDetailCollectionByInstructionID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstruction.ICInstructionDetailCollectionByInstructionID_Delegate)
				map.PropertyName = "ICInstructionDetailCollectionByInstructionID"
				map.MyColumnName = "InstructionID"
				map.ParentColumnName = "InstructionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstructionDetailCollectionByInstructionID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionQuery(data.NextAlias())
			
			Dim mee As ICInstructionDetailQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstructionDetailQuery), New ICInstructionDetailQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.InstructionID = mee.InstructionID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_InstructionDetail_IC_Instruction
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstructionDetailCollectionByInstructionID As ICInstructionDetailCollection 
		
			Get
				If Me._ICInstructionDetailCollectionByInstructionID Is Nothing Then
					Me._ICInstructionDetailCollectionByInstructionID = New ICInstructionDetailCollection()
					Me._ICInstructionDetailCollectionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstructionDetailCollectionByInstructionID", Me._ICInstructionDetailCollectionByInstructionID)
				
					If Not Me.InstructionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstructionDetailCollectionByInstructionID.Query.Where(Me._ICInstructionDetailCollectionByInstructionID.Query.InstructionID = Me.InstructionID)
							Me._ICInstructionDetailCollectionByInstructionID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstructionDetailCollectionByInstructionID.fks.Add(ICInstructionDetailMetadata.ColumnNames.InstructionID, Me.InstructionID)
					End If
				End If

				Return Me._ICInstructionDetailCollectionByInstructionID
			End Get
			
			Set(ByVal value As ICInstructionDetailCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstructionDetailCollectionByInstructionID Is Nothing Then

					Me.RemovePostSave("ICInstructionDetailCollectionByInstructionID")
					Me._ICInstructionDetailCollectionByInstructionID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstructionDetailCollectionByInstructionID As ICInstructionDetailCollection
		#End Region

		#Region "ICPackageChargesCollectionByInstructionID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageChargesCollectionByInstructionID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICInstruction.ICPackageChargesCollectionByInstructionID_Delegate)
				map.PropertyName = "ICPackageChargesCollectionByInstructionID"
				map.MyColumnName = "InstructionID"
				map.ParentColumnName = "InstructionID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageChargesCollectionByInstructionID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICInstructionQuery(data.NextAlias())
			
			Dim mee As ICPackageChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageChargesQuery), New ICPackageChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.InstructionID = mee.InstructionID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageCharges_IC_Instruction
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageChargesCollectionByInstructionID As ICPackageChargesCollection 
		
			Get
				If Me._ICPackageChargesCollectionByInstructionID Is Nothing Then
					Me._ICPackageChargesCollectionByInstructionID = New ICPackageChargesCollection()
					Me._ICPackageChargesCollectionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageChargesCollectionByInstructionID", Me._ICPackageChargesCollectionByInstructionID)
				
					If Not Me.InstructionID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageChargesCollectionByInstructionID.Query.Where(Me._ICPackageChargesCollectionByInstructionID.Query.InstructionID = Me.InstructionID)
							Me._ICPackageChargesCollectionByInstructionID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageChargesCollectionByInstructionID.fks.Add(ICPackageChargesMetadata.ColumnNames.InstructionID, Me.InstructionID)
					End If
				End If

				Return Me._ICPackageChargesCollectionByInstructionID
			End Get
			
			Set(ByVal value As ICPackageChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageChargesCollectionByInstructionID Is Nothing Then

					Me.RemovePostSave("ICPackageChargesCollectionByInstructionID")
					Me._ICPackageChargesCollectionByInstructionID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageChargesCollectionByInstructionID As ICPackageChargesCollection
		#End Region

		#Region "UpToICAccountsByClientAccountNo - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByClientAccountNo As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByClientAccountNo Is Nothing _
						 AndAlso Not ClientAccountNo.Equals(Nothing)  AndAlso Not ClientAccountBranchCode.Equals(Nothing)  AndAlso Not ClientAccountCurrency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByClientAccountNo = New ICAccounts()
					Me._UpToICAccountsByClientAccountNo.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByClientAccountNo", Me._UpToICAccountsByClientAccountNo)
					Me._UpToICAccountsByClientAccountNo.Query.Where(Me._UpToICAccountsByClientAccountNo.Query.AccountNumber = Me.ClientAccountNo)
					Me._UpToICAccountsByClientAccountNo.Query.Where(Me._UpToICAccountsByClientAccountNo.Query.BranchCode = Me.ClientAccountBranchCode)
					Me._UpToICAccountsByClientAccountNo.Query.Where(Me._UpToICAccountsByClientAccountNo.Query.Currency = Me.ClientAccountCurrency)
					Me._UpToICAccountsByClientAccountNo.Query.Load()
				End If

				Return Me._UpToICAccountsByClientAccountNo
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByClientAccountNo")
				

				If value Is Nothing Then
				
					Me.ClientAccountNo = Nothing
				
					Me.ClientAccountBranchCode = Nothing
				
					Me.ClientAccountCurrency = Nothing
				
					Me._UpToICAccountsByClientAccountNo = Nothing
				Else
				
					Me.ClientAccountNo = value.AccountNumber
					
					Me.ClientAccountBranchCode = value.BranchCode
					
					Me.ClientAccountCurrency = value.Currency
					
					Me._UpToICAccountsByClientAccountNo = value
					Me.SetPreSave("UpToICAccountsByClientAccountNo", Me._UpToICAccountsByClientAccountNo)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICGroupByGroupCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_Group
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICGroupByGroupCode As ICGroup
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICGroupByGroupCode Is Nothing _
						 AndAlso Not GroupCode.Equals(Nothing)  Then
						
					Me._UpToICGroupByGroupCode = New ICGroup()
					Me._UpToICGroupByGroupCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
					Me._UpToICGroupByGroupCode.Query.Where(Me._UpToICGroupByGroupCode.Query.GroupCode = Me.GroupCode)
					Me._UpToICGroupByGroupCode.Query.Load()
				End If

				Return Me._UpToICGroupByGroupCode
			End Get
			
            Set(ByVal value As ICGroup)
				Me.RemovePreSave("UpToICGroupByGroupCode")
				

				If value Is Nothing Then
				
					Me.GroupCode = Nothing
				
					Me._UpToICGroupByGroupCode = Nothing
				Else
				
					Me.GroupCode = value.GroupCode
					
					Me._UpToICGroupByGroupCode = value
					Me.SetPreSave("UpToICGroupByGroupCode", Me._UpToICGroupByGroupCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICInstructionStatusByStatus - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_InstructionStatus1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionStatusByStatus As ICInstructionStatus
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionStatusByStatus Is Nothing _
						 AndAlso Not Status.Equals(Nothing)  Then
						
					Me._UpToICInstructionStatusByStatus = New ICInstructionStatus()
					Me._UpToICInstructionStatusByStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionStatusByStatus", Me._UpToICInstructionStatusByStatus)
					Me._UpToICInstructionStatusByStatus.Query.Where(Me._UpToICInstructionStatusByStatus.Query.StatusID = Me.Status)
					Me._UpToICInstructionStatusByStatus.Query.Load()
				End If

				Return Me._UpToICInstructionStatusByStatus
			End Get
			
            Set(ByVal value As ICInstructionStatus)
				Me.RemovePreSave("UpToICInstructionStatusByStatus")
				

				If value Is Nothing Then
				
					Me.Status = Nothing
				
					Me._UpToICInstructionStatusByStatus = Nothing
				Else
				
					Me.Status = value.StatusID
					
					Me._UpToICInstructionStatusByStatus = value
					Me.SetPreSave("UpToICInstructionStatusByStatus", Me._UpToICInstructionStatusByStatus)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICInstructionStatusByLastStatus - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_InstructionStatus2
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionStatusByLastStatus As ICInstructionStatus
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionStatusByLastStatus Is Nothing _
						 AndAlso Not LastStatus.Equals(Nothing)  Then
						
					Me._UpToICInstructionStatusByLastStatus = New ICInstructionStatus()
					Me._UpToICInstructionStatusByLastStatus.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionStatusByLastStatus", Me._UpToICInstructionStatusByLastStatus)
					Me._UpToICInstructionStatusByLastStatus.Query.Where(Me._UpToICInstructionStatusByLastStatus.Query.StatusID = Me.LastStatus)
					Me._UpToICInstructionStatusByLastStatus.Query.Load()
				End If

				Return Me._UpToICInstructionStatusByLastStatus
			End Get
			
            Set(ByVal value As ICInstructionStatus)
				Me.RemovePreSave("UpToICInstructionStatusByLastStatus")
				

				If value Is Nothing Then
				
					Me.LastStatus = Nothing
				
					Me._UpToICInstructionStatusByLastStatus = Nothing
				Else
				
					Me.LastStatus = value.StatusID
					
					Me._UpToICInstructionStatusByLastStatus = value
					Me.SetPreSave("UpToICInstructionStatusByLastStatus", Me._UpToICInstructionStatusByLastStatus)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICOfficeByPrintLocationCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByPrintLocationCode As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByPrintLocationCode Is Nothing _
						 AndAlso Not PrintLocationCode.Equals(Nothing)  Then
						
					Me._UpToICOfficeByPrintLocationCode = New ICOffice()
					Me._UpToICOfficeByPrintLocationCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByPrintLocationCode", Me._UpToICOfficeByPrintLocationCode)
					Me._UpToICOfficeByPrintLocationCode.Query.Where(Me._UpToICOfficeByPrintLocationCode.Query.OfficeID = Me.PrintLocationCode)
					Me._UpToICOfficeByPrintLocationCode.Query.Load()
				End If

				Return Me._UpToICOfficeByPrintLocationCode
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByPrintLocationCode")
				

				If value Is Nothing Then
				
					Me.PrintLocationCode = Nothing
				
					Me._UpToICOfficeByPrintLocationCode = Nothing
				Else
				
					Me.PrintLocationCode = value.OfficeID
					
					Me._UpToICOfficeByPrintLocationCode = value
					Me.SetPreSave("UpToICOfficeByPrintLocationCode", Me._UpToICOfficeByPrintLocationCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICProductTypeByProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_ProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICProductTypeByProductTypeCode As ICProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICProductTypeByProductTypeCode Is Nothing _
						 AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICProductTypeByProductTypeCode = New ICProductType()
					Me._UpToICProductTypeByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Where(Me._UpToICProductTypeByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Load()
				End If

				Return Me._UpToICProductTypeByProductTypeCode
			End Get
			
            Set(ByVal value As ICProductType)
				Me.RemovePreSave("UpToICProductTypeByProductTypeCode")
				

				If value Is Nothing Then
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICProductTypeByProductTypeCode = Nothing
				Else
				
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICProductTypeByProductTypeCode = value
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByClientPrimaryApprovedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByClientPrimaryApprovedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByClientPrimaryApprovedBy Is Nothing _
						 AndAlso Not ClientPrimaryApprovedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByClientPrimaryApprovedBy = New ICUser()
					Me._UpToICUserByClientPrimaryApprovedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByClientPrimaryApprovedBy", Me._UpToICUserByClientPrimaryApprovedBy)
					Me._UpToICUserByClientPrimaryApprovedBy.Query.Where(Me._UpToICUserByClientPrimaryApprovedBy.Query.UserID = Me.ClientPrimaryApprovedBy)
					Me._UpToICUserByClientPrimaryApprovedBy.Query.Load()
				End If

				Return Me._UpToICUserByClientPrimaryApprovedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByClientPrimaryApprovedBy")
				

				If value Is Nothing Then
				
					Me.ClientPrimaryApprovedBy = Nothing
				
					Me._UpToICUserByClientPrimaryApprovedBy = Nothing
				Else
				
					Me.ClientPrimaryApprovedBy = value.UserID
					
					Me._UpToICUserByClientPrimaryApprovedBy = value
					Me.SetPreSave("UpToICUserByClientPrimaryApprovedBy", Me._UpToICUserByClientPrimaryApprovedBy)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByClientSecondaryApprovedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Instruction_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByClientSecondaryApprovedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByClientSecondaryApprovedBy Is Nothing _
						 AndAlso Not ClientSecondaryApprovedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByClientSecondaryApprovedBy = New ICUser()
					Me._UpToICUserByClientSecondaryApprovedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByClientSecondaryApprovedBy", Me._UpToICUserByClientSecondaryApprovedBy)
					Me._UpToICUserByClientSecondaryApprovedBy.Query.Where(Me._UpToICUserByClientSecondaryApprovedBy.Query.UserID = Me.ClientSecondaryApprovedBy)
					Me._UpToICUserByClientSecondaryApprovedBy.Query.Load()
				End If

				Return Me._UpToICUserByClientSecondaryApprovedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByClientSecondaryApprovedBy")
				

				If value Is Nothing Then
				
					Me.ClientSecondaryApprovedBy = Nothing
				
					Me._UpToICUserByClientSecondaryApprovedBy = Nothing
				Else
				
					Me.ClientSecondaryApprovedBy = value.UserID
					
					Me._UpToICUserByClientSecondaryApprovedBy = value
					Me.SetPreSave("UpToICUserByClientSecondaryApprovedBy", Me._UpToICUserByClientSecondaryApprovedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICInstructionActivityCollectionByInstructionID"
					coll = Me.ICInstructionActivityCollectionByInstructionID
					Exit Select
				Case "ICInstructionApprovalLogCollectionByInstructionID"
					coll = Me.ICInstructionApprovalLogCollectionByInstructionID
					Exit Select
				Case "ICInstructionDetailCollectionByInstructionID"
					coll = Me.ICInstructionDetailCollectionByInstructionID
					Exit Select
				Case "ICPackageChargesCollectionByInstructionID"
					coll = Me.ICPackageChargesCollectionByInstructionID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICInstructionActivityCollectionByInstructionID", GetType(ICInstructionActivityCollection), New ICInstructionActivity()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionApprovalLogCollectionByInstructionID", GetType(ICInstructionApprovalLogCollection), New ICInstructionApprovalLog()))
			props.Add(new esPropertyDescriptor(Me, "ICInstructionDetailCollectionByInstructionID", GetType(ICInstructionDetailCollection), New ICInstructionDetail()))
			props.Add(new esPropertyDescriptor(Me, "ICPackageChargesCollectionByInstructionID", GetType(ICPackageChargesCollection), New ICPackageCharges()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICGroupByGroupCode Is Nothing Then
				Me.GroupCode = Me._UpToICGroupByGroupCode.GroupCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionStatusByStatus Is Nothing Then
				Me.Status = Me._UpToICInstructionStatusByStatus.StatusID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionStatusByLastStatus Is Nothing Then
				Me.LastStatus = Me._UpToICInstructionStatusByLastStatus.StatusID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByPrintLocationCode Is Nothing Then
				Me.PrintLocationCode = Me._UpToICOfficeByPrintLocationCode.OfficeID
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICInstructionActivityCollectionByInstructionID Is Nothing Then
				Apply(Me._ICInstructionActivityCollectionByInstructionID, "InstructionID", Me.InstructionID)
			End If
			
			If Not Me._ICInstructionApprovalLogCollectionByInstructionID Is Nothing Then
				Apply(Me._ICInstructionApprovalLogCollectionByInstructionID, "InstructionID", Me.InstructionID)
			End If
			
			If Not Me._ICInstructionDetailCollectionByInstructionID Is Nothing Then
				Apply(Me._ICInstructionDetailCollectionByInstructionID, "InstructionID", Me.InstructionID)
			End If
			
			If Not Me._ICPackageChargesCollectionByInstructionID Is Nothing Then
				Apply(Me._ICPackageChargesCollectionByInstructionID, "InstructionID", Me.InstructionID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICInstructionMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.InstructionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.InstructionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Description, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Description
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Remarks, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Remarks
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CompanyCode, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CreateBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Status, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Status
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.FileID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.FileID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AcquisitionMode, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AcquisitionMode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.LastStatus, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.LastStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ErrorMessage, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ErrorMessage
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Amount, 11, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AmountInWords, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AmountInWords
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryAccountNo, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryAccountTitle, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryAccountTitle
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryAddress, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryBankCode, 16, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryBankCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryBankName, 17, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryBankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryBankAddress, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryBankAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryBranchCode, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryBranchCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryBranchName, 20, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryBranchName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryCity, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryCity
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryCNIC, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryCNIC
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryCode, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryCountry, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryCountry
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryEmail, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryEmail
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryMobile, 26, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryMobile
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryName, 27, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryPhone, 28, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryPhone
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryProvince, 29, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryProvince
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryType, 30, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryType
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientAccountNo, 31, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientAddress, 32, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientBankCode, 33, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientBankCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientBankName, 34, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientBankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientBranchAddress, 35, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientBranchAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientBranchCode, 36, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientBranchCode
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientBranchName, 37, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientBranchName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientCity, 38, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientCity
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientCountry, 39, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientCountry
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientEmail, 40, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientEmail
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientMobile, 41, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientMobile
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientName, 42, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientProvince, 43, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientProvince
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DetailAmount, 44, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DetailAmount
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DDPayableLocation, 45, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DDPayableLocation
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.InstrumentNo, 46, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.InstrumentNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.InvoiceNo, 47, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.InvoiceNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PaymentMode, 48, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PaymentMode
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PaymentNatureCode, 49, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationName, 50, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationName
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ProductTypeCode, 51, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.TransactionDate, 52, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.TransactionDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.TXNCode, 53, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.TXNCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ValueDate, 54, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ValueDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsPrinted, 55, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsPrinted
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsRePrinted, 56, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsRePrinted
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.RePrintCounter, 57, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.RePrintCounter
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReIssued, 58, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReIssued
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReValidated, 59, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReValidated
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReferenceField1, 60, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReferenceField1
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReferenceField2, 61, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReferenceField2
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReferenceField3, 62, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReferenceField3
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientAccountBranchCode, 63, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientAccountCurrency, 64, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneAccountBranchCode, 65, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneAccountCurrency, 66, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.GroupCode, 67, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.GroupCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableAccountNumber, 68, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableAccountBranchCode, 69, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableAccountCurrency, 70, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingAccountNo, 71, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingAccountBranchCode, 72, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingAccountCurrency, 73, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DDDrawnCityCode, 74, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DDDrawnCityCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationCode, 75, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CreatedOfficeCode, 76, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CreatedOfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.FileBatchNo, 77, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.FileBatchNo
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReferenceNo, 78, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReferenceNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PreviousPrintLocationName, 79, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PreviousPrintLocationName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PreviousPrintLocationCode, 80, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PreviousPrintLocationCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.VerificationDate, 81, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.VerificationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ApprovalDate, 82, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ApprovalDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StaleDate, 83, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StaleDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CancellationDate, 84, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CancellationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.LastPrintDate, 85, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.LastPrintDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.RevalidationDate, 86, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.RevalidationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DrawnOnBranchCode, 87, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DrawnOnBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedBy, 88, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovedBy, 89, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientSecondaryApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientPrimaryApprovedDate, 90, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientPrimaryApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClientSecondaryApprovalDateTime, 91, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClientSecondaryApprovalDateTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedBy, 92, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BankSecondaryApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedDate, 93, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BankPrimaryApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BankSecondaryApprovedDate, 94, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BankSecondaryApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BankPrimaryApprovedBy, 95, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BankPrimaryApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationAmendedBy, 96, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationAmendedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentDate, 97, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationAmendmentDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentApprovedBy, 98, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationAmendmentApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationApprovedAmendmentDate, 99, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationApprovedAmendmentDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentApproved, 100, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancelledBy, 101, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintLocationAmendmentCancellDate, 102, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintLocationAmendmentCancellDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsPrintLocationAmendmentCancelled, 103, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsPrintLocationAmendmentCancelled
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsPrintLocationAmended, 104, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsPrintLocationAmended
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReprintingAllowed, 105, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReprintingAllowed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReprintingAllowedDate, 106, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReprintingAllowedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReprintingAllowedBy, 107, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReprintingAllowedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsAllowReprintingApproved, 108, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsAllowReprintingApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedBy, 109, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowReprintingApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowReprintingApprovedDate, 110, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowReprintingApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsAllowReprintingCancelled, 111, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsAllowReprintingCancelled
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReprintingAllowedCancelledBy, 112, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReprintingAllowedCancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReprintingAllowCancelledDate, 113, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReprintingAllowCancelledDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReIssuanceAllowed, 114, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReIssuanceAllowed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReissuanceAlloweBy, 115, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReissuanceAlloweBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReIssuanceAllowedDate, 116, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReIssuanceAllowedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceApproved, 117, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowReIssuanceApprovedDate, 118, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowReIssuanceApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowedReIssuanceApprovedBy, 119, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowedReIssuanceApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsAllowedReIssuanceCancelled, 120, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsAllowedReIssuanceCancelled
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledDate, 121, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AllowedReIssuanceCancelledBy, 122, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AllowedReIssuanceCancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReIssuanceID, 123, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReIssuanceID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.NewInstructionID, 124, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.NewInstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PhysicalInstrumentNumber, 125, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PhysicalInstrumentNumber
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValiDatedDate, 126, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValiDatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValiDationAllowedBy, 127, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValiDationAllowedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReValiDationAllowed, 128, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReValiDationAllowed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValidationApprovalDate, 129, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValidationApprovalDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValidationApprovedBy, 130, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValidationApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReValiDationApproved, 131, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReValiDationApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValiDationCancelDate, 132, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValiDationCancelDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReValiDationCancelledBy, 133, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReValiDationCancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsReValiDationCancelled, 134, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsReValiDationCancelled
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingBankCode, 135, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingBankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingDate, 136, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingOfficeCode, 137, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingOfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearedBy, 138, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingApprovedBy, 139, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingApprovedDate, 140, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingApprovedOfficeCode, 141, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingApprovedOfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AmendedPrintLocationCode, 142, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AmendedPrintLocationCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AmendedPrintLocationName, 143, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AmendedPrintLocationName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableClientNo, 144, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableSeqNo, 145, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableProfitCentre, 146, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PayableAccountType, 147, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PayableAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingClientNo, 148, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingSeqNo, 149, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingProfitCentre, 150, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingAccountType, 151, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Creater, 152, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CreationDate, 153, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.PrintDate, 154, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.PrintDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsAmendmentComplete, 155, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsAmendmentComplete
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClubID, 156, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClubID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.SkipReason, 157, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.SkipReason
			c.CharacterMaxLength = 1000
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneBranchAddress, 158, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneBranchAddress
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusCancelledBy, 159, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusCancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusCancelledDate, 160, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusCancelledDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusReversedBy, 161, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusReversedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusReversedOfficeCode, 162, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusReversedOfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusReversedDate, 163, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusReversedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusSettledBy, 164, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusSettledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusSettledDate, 165, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusSettledDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedBy, 166, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusCancelledApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusSettledApprovedBy, 167, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusSettledApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusReversedApprovedBy, 168, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusReversedApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusCancelledApprovedDate, 169, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusCancelledApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusSettledApprovedDate, 170, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusSettledApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusReversedApprovedDate, 171, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusReversedApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusChangeRemarks, 172, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusChangeRemarks
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusApproveRemarks, 173, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusApproveRemarks
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledby, 174, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledby
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusChangeApprovalCancelledDate, 175, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusChangeApprovalCancelledDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ClearingType, 176, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ClearingType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ScheduleTransactionID, 177, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ScheduleTransactionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ScheduleTransactionTime, 178, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ScheduleTransactionTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IsCheduleTransactionProcessed, 179, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IsCheduleTransactionProcessed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryIDType, 180, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryIDType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficiaryIDNo, 181, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficiaryIDNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.Rin, 182, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.Rin
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DisbursedBy, 183, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DisbursedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DisbursedDate, 184, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DisbursedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DisbursedBranchCode, 185, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DisbursedBranchCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CancelledBy, 186, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CancelledBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CancelledApproveBy, 187, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CancelledApproveBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.CancelledApprovalDate, 188, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.CancelledApprovalDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.UBPCompanyID, 189, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.UBPCompanyID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.UBPReferenceField, 190, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.UBPReferenceField
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneficaryAccountType, 191, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneficaryAccountType
			c.CharacterMaxLength = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.BeneID, 192, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.BeneID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.ReturnReason, 193, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.ReturnReason
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AmountBeforeDueDate, 194, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AmountBeforeDueDate
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.AmountAfterDueDate, 195, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.AmountAfterDueDate
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.DueDate, 196, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.DueDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusHoldBy, 197, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusHoldBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusHoldDate, 198, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusHoldDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusHoldApprovedBy, 199, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusHoldApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusHoldApprovedDate, 200, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusHoldApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusUnHoldBy, 201, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusUnHoldBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusUnHoldDate, 202, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusUnHoldDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedBy, 203, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusUnHoldApprovedDate, 204, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusUnHoldApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusRevertBy, 205, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusRevertBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusRevertDate, 206, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusRevertDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusRevertApprovedBy, 207, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusRevertApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.StatusRevertApprovedDate, 208, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.StatusRevertApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.IBFTStan, 209, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.IBFTStan
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICInstructionMetadata.ColumnNames.FinalStatus, 210, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICInstructionMetadata.PropertyNames.FinalStatus
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICInstructionMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const InstructionID As String = "InstructionID"
			 Public Const Description As String = "Description"
			 Public Const Remarks As String = "Remarks"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Status As String = "Status"
			 Public Const FileID As String = "FileID"
			 Public Const AcquisitionMode As String = "AcquisitionMode"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const Amount As String = "Amount"
			 Public Const AmountInWords As String = "AmountInWords"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountTitle As String = "BeneficiaryAccountTitle"
			 Public Const BeneficiaryAddress As String = "BeneficiaryAddress"
			 Public Const BeneficiaryBankCode As String = "BeneficiaryBankCode"
			 Public Const BeneficiaryBankName As String = "BeneficiaryBankName"
			 Public Const BeneficiaryBankAddress As String = "BeneficiaryBankAddress"
			 Public Const BeneficiaryBranchCode As String = "BeneficiaryBranchCode"
			 Public Const BeneficiaryBranchName As String = "BeneficiaryBranchName"
			 Public Const BeneficiaryCity As String = "BeneficiaryCity"
			 Public Const BeneficiaryCNIC As String = "BeneficiaryCNIC"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const BeneficiaryCountry As String = "BeneficiaryCountry"
			 Public Const BeneficiaryEmail As String = "BeneficiaryEmail"
			 Public Const BeneficiaryMobile As String = "BeneficiaryMobile"
			 Public Const BeneficiaryName As String = "BeneficiaryName"
			 Public Const BeneficiaryPhone As String = "BeneficiaryPhone"
			 Public Const BeneficiaryProvince As String = "BeneficiaryProvince"
			 Public Const BeneficiaryType As String = "BeneficiaryType"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAddress As String = "ClientAddress"
			 Public Const ClientBankCode As String = "ClientBankCode"
			 Public Const ClientBankName As String = "ClientBankName"
			 Public Const ClientBranchAddress As String = "ClientBranchAddress"
			 Public Const ClientBranchCode As String = "ClientBranchCode"
			 Public Const ClientBranchName As String = "ClientBranchName"
			 Public Const ClientCity As String = "ClientCity"
			 Public Const ClientCountry As String = "ClientCountry"
			 Public Const ClientEmail As String = "ClientEmail"
			 Public Const ClientMobile As String = "ClientMobile"
			 Public Const ClientName As String = "ClientName"
			 Public Const ClientProvince As String = "ClientProvince"
			 Public Const DetailAmount As String = "DetailAmount"
			 Public Const DDPayableLocation As String = "DDPayableLocation"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const InvoiceNo As String = "InvoiceNo"
			 Public Const PaymentMode As String = "PaymentMode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const PrintLocationName As String = "PrintLocationName"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const TransactionDate As String = "TransactionDate"
			 Public Const TXNCode As String = "TXN_Code"
			 Public Const ValueDate As String = "ValueDate"
			 Public Const IsPrinted As String = "IsPrinted"
			 Public Const IsRePrinted As String = "IsRePrinted"
			 Public Const RePrintCounter As String = "RePrintCounter"
			 Public Const IsReIssued As String = "IsReIssued"
			 Public Const IsReValidated As String = "IsReValidated"
			 Public Const ReferenceField1 As String = "ReferenceField1"
			 Public Const ReferenceField2 As String = "ReferenceField2"
			 Public Const ReferenceField3 As String = "ReferenceField3"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const BeneAccountBranchCode As String = "BeneAccountBranchCode"
			 Public Const BeneAccountCurrency As String = "BeneAccountCurrency"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const ClearingAccountNo As String = "ClearingAccountNo"
			 Public Const ClearingAccountBranchCode As String = "ClearingAccountBranchCode"
			 Public Const ClearingAccountCurrency As String = "ClearingAccountCurrency"
			 Public Const DDDrawnCityCode As String = "DDDrawnCityCode"
			 Public Const PrintLocationCode As String = "PrintLocationCode"
			 Public Const CreatedOfficeCode As String = "CreatedOfficeCode"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const ReferenceNo As String = "ReferenceNo"
			 Public Const PreviousPrintLocationName As String = "PreviousPrintLocationName"
			 Public Const PreviousPrintLocationCode As String = "PreviousPrintLocationCode"
			 Public Const VerificationDate As String = "VerificationDate"
			 Public Const ApprovalDate As String = "ApprovalDate"
			 Public Const StaleDate As String = "StaleDate"
			 Public Const CancellationDate As String = "CancellationDate"
			 Public Const LastPrintDate As String = "LastPrintDate"
			 Public Const RevalidationDate As String = "RevalidationDate"
			 Public Const DrawnOnBranchCode As String = "DrawnOnBranchCode"
			 Public Const ClientPrimaryApprovedBy As String = "ClientPrimaryApprovedBy"
			 Public Const ClientSecondaryApprovedBy As String = "ClientSecondaryApprovedBy"
			 Public Const ClientPrimaryApprovedDate As String = "ClientPrimaryApprovedDate"
			 Public Const ClientSecondaryApprovalDateTime As String = "ClientSecondaryApprovalDateTime"
			 Public Const BankSecondaryApprovedBy As String = "BankSecondaryApprovedBy"
			 Public Const BankPrimaryApprovedDate As String = "BankPrimaryApprovedDate"
			 Public Const BankSecondaryApprovedDate As String = "BankSecondaryApprovedDate"
			 Public Const BankPrimaryApprovedBy As String = "BankPrimaryApprovedBy"
			 Public Const PrintLocationAmendedBy As String = "PrintLocationAmendedBy"
			 Public Const PrintLocationAmendmentDate As String = "PrintLocationAmendmentDate"
			 Public Const PrintLocationAmendmentApprovedBy As String = "PrintLocationAmendmentApprovedBy"
			 Public Const PrintLocationApprovedAmendmentDate As String = "PrintLocationApprovedAmendmentDate"
			 Public Const IsPrintLocationAmendmentApproved As String = "IsPrintLocationAmendmentApproved"
			 Public Const PrintLocationAmendmentCancelledBy As String = "PrintLocationAmendmentCancelledBy"
			 Public Const PrintLocationAmendmentCancellDate As String = "PrintLocationAmendmentCancellDate"
			 Public Const IsPrintLocationAmendmentCancelled As String = "IsPrintLocationAmendmentCancelled"
			 Public Const IsPrintLocationAmended As String = "IsPrintLocationAmended"
			 Public Const IsReprintingAllowed As String = "IsReprintingAllowed"
			 Public Const ReprintingAllowedDate As String = "ReprintingAllowedDate"
			 Public Const ReprintingAllowedBy As String = "ReprintingAllowedBy"
			 Public Const IsAllowReprintingApproved As String = "IsAllowReprintingApproved"
			 Public Const AllowReprintingApprovedBy As String = "AllowReprintingApprovedBy"
			 Public Const AllowReprintingApprovedDate As String = "AllowReprintingApprovedDate"
			 Public Const IsAllowReprintingCancelled As String = "IsAllowReprintingCancelled"
			 Public Const ReprintingAllowedCancelledBy As String = "ReprintingAllowedCancelledBy"
			 Public Const ReprintingAllowCancelledDate As String = "ReprintingAllowCancelledDate"
			 Public Const IsReIssuanceAllowed As String = "IsReIssuanceAllowed"
			 Public Const ReissuanceAlloweBy As String = "ReissuanceAlloweBy"
			 Public Const ReIssuanceAllowedDate As String = "ReIssuanceAllowedDate"
			 Public Const IsAllowedReIssuanceApproved As String = "IsAllowedReIssuanceApproved"
			 Public Const AllowReIssuanceApprovedDate As String = "AllowReIssuanceApprovedDate"
			 Public Const AllowedReIssuanceApprovedBy As String = "AllowedReIssuanceApprovedBy"
			 Public Const IsAllowedReIssuanceCancelled As String = "IsAllowedReIssuanceCancelled"
			 Public Const AllowedReIssuanceCancelledDate As String = "AllowedReIssuanceCancelledDate"
			 Public Const AllowedReIssuanceCancelledBy As String = "AllowedReIssuanceCancelledBy"
			 Public Const ReIssuanceID As String = "ReIssuanceID"
			 Public Const NewInstructionID As String = "NewInstructionID"
			 Public Const PhysicalInstrumentNumber As String = "PhysicalInstrumentNumber"
			 Public Const ReValiDatedDate As String = "ReValiDatedDate"
			 Public Const ReValiDationAllowedBy As String = "ReValiDationAllowedBy"
			 Public Const IsReValiDationAllowed As String = "IsReValiDationAllowed"
			 Public Const ReValidationApprovalDate As String = "ReValidationApprovalDate"
			 Public Const ReValidationApprovedBy As String = "ReValidationApprovedBy"
			 Public Const IsReValiDationApproved As String = "IsReValiDationApproved"
			 Public Const ReValiDationCancelDate As String = "ReValiDationCancelDate"
			 Public Const ReValiDationCancelledBy As String = "ReValiDationCancelledBy"
			 Public Const IsReValiDationCancelled As String = "IsReValiDationCancelled"
			 Public Const ClearingBankCode As String = "ClearingBankCode"
			 Public Const ClearingDate As String = "ClearingDate"
			 Public Const ClearingOfficeCode As String = "ClearingOfficeCode"
			 Public Const ClearedBy As String = "ClearedBy"
			 Public Const ClearingApprovedBy As String = "ClearingApprovedBy"
			 Public Const ClearingApprovedDate As String = "ClearingApprovedDate"
			 Public Const ClearingApprovedOfficeCode As String = "ClearingApprovedOfficeCode"
			 Public Const AmendedPrintLocationCode As String = "AmendedPrintLocationCode"
			 Public Const AmendedPrintLocationName As String = "AmendedPrintLocationName"
			 Public Const PayableClientNo As String = "PayableClientNo"
			 Public Const PayableSeqNo As String = "PayableSeqNo"
			 Public Const PayableProfitCentre As String = "PayableProfitCentre"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const ClearingClientNo As String = "ClearingClientNo"
			 Public Const ClearingSeqNo As String = "ClearingSeqNo"
			 Public Const ClearingProfitCentre As String = "ClearingProfitCentre"
			 Public Const ClearingAccountType As String = "ClearingAccountType"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const PrintDate As String = "PrintDate"
			 Public Const IsAmendmentComplete As String = "IsAmendmentComplete"
			 Public Const ClubID As String = "ClubID"
			 Public Const SkipReason As String = "SkipReason"
			 Public Const BeneBranchAddress As String = "BeneBranchAddress"
			 Public Const StatusCancelledBy As String = "statusCancelledBy"
			 Public Const StatusCancelledDate As String = "statusCancelledDate"
			 Public Const StatusReversedBy As String = "statusReversedBy"
			 Public Const StatusReversedOfficeCode As String = "statusReversedOfficeCode"
			 Public Const StatusReversedDate As String = "statusReversedDate"
			 Public Const StatusSettledBy As String = "statusSettledBy"
			 Public Const StatusSettledDate As String = "statusSettledDate"
			 Public Const StatusCancelledApprovedBy As String = "StatusCancelledApprovedBy"
			 Public Const StatusSettledApprovedBy As String = "StatusSettledApprovedBy"
			 Public Const StatusReversedApprovedBy As String = "StatusReversedApprovedBy"
			 Public Const StatusCancelledApprovedDate As String = "StatusCancelledApprovedDate"
			 Public Const StatusSettledApprovedDate As String = "StatusSettledApprovedDate"
			 Public Const StatusReversedApprovedDate As String = "StatusReversedApprovedDate"
			 Public Const StatusChangeRemarks As String = "statusChangeRemarks"
			 Public Const StatusApproveRemarks As String = "statusApproveRemarks"
			 Public Const StatusChangeApprovalCancelledby As String = "StatusChangeApprovalCancelledby"
			 Public Const StatusChangeApprovalCancelledDate As String = "StatusChangeApprovalCancelledDate"
			 Public Const ClearingType As String = "ClearingType"
			 Public Const ScheduleTransactionID As String = "ScheduleTransactionID"
			 Public Const ScheduleTransactionTime As String = "ScheduleTransactionTime"
			 Public Const IsCheduleTransactionProcessed As String = "IsCheduleTransactionProcessed"
			 Public Const BeneficiaryIDType As String = "BeneficiaryIDType"
			 Public Const BeneficiaryIDNo As String = "BeneficiaryIDNo"
			 Public Const Rin As String = "RIN"
			 Public Const DisbursedBy As String = "DisbursedBy"
			 Public Const DisbursedDate As String = "DisbursedDate"
			 Public Const DisbursedBranchCode As String = "DisbursedBranchCode"
			 Public Const CancelledBy As String = "CancelledBy"
			 Public Const CancelledApproveBy As String = "CancelledApproveBy"
			 Public Const CancelledApprovalDate As String = "CancelledApprovalDate"
			 Public Const UBPCompanyID As String = "UBPCompanyID"
			 Public Const UBPReferenceField As String = "UBPReferenceField"
			 Public Const BeneficaryAccountType As String = "BeneficaryAccountType"
			 Public Const BeneID As String = "BeneID"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const AmountBeforeDueDate As String = "AmountBeforeDueDate"
			 Public Const AmountAfterDueDate As String = "AmountAfterDueDate"
			 Public Const DueDate As String = "DueDate"
			 Public Const StatusHoldBy As String = "statusHoldBy"
			 Public Const StatusHoldDate As String = "statusHoldDate"
			 Public Const StatusHoldApprovedBy As String = "StatusHoldApprovedBy"
			 Public Const StatusHoldApprovedDate As String = "StatusHoldApprovedDate"
			 Public Const StatusUnHoldBy As String = "statusUnHoldBy"
			 Public Const StatusUnHoldDate As String = "statusUnHoldDate"
			 Public Const StatusUnHoldApprovedBy As String = "StatusUnHoldApprovedBy"
			 Public Const StatusUnHoldApprovedDate As String = "StatusUnHoldApprovedDate"
			 Public Const StatusRevertBy As String = "statusRevertBy"
			 Public Const StatusRevertDate As String = "statusRevertDate"
			 Public Const StatusRevertApprovedBy As String = "StatusRevertApprovedBy"
			 Public Const StatusRevertApprovedDate As String = "StatusRevertApprovedDate"
			 Public Const IBFTStan As String = "IBFTStan"
			 Public Const FinalStatus As String = "FinalStatus"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const InstructionID As String = "InstructionID"
			 Public Const Description As String = "Description"
			 Public Const Remarks As String = "Remarks"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Status As String = "Status"
			 Public Const FileID As String = "FileID"
			 Public Const AcquisitionMode As String = "AcquisitionMode"
			 Public Const LastStatus As String = "LastStatus"
			 Public Const ErrorMessage As String = "ErrorMessage"
			 Public Const Amount As String = "Amount"
			 Public Const AmountInWords As String = "AmountInWords"
			 Public Const BeneficiaryAccountNo As String = "BeneficiaryAccountNo"
			 Public Const BeneficiaryAccountTitle As String = "BeneficiaryAccountTitle"
			 Public Const BeneficiaryAddress As String = "BeneficiaryAddress"
			 Public Const BeneficiaryBankCode As String = "BeneficiaryBankCode"
			 Public Const BeneficiaryBankName As String = "BeneficiaryBankName"
			 Public Const BeneficiaryBankAddress As String = "BeneficiaryBankAddress"
			 Public Const BeneficiaryBranchCode As String = "BeneficiaryBranchCode"
			 Public Const BeneficiaryBranchName As String = "BeneficiaryBranchName"
			 Public Const BeneficiaryCity As String = "BeneficiaryCity"
			 Public Const BeneficiaryCNIC As String = "BeneficiaryCNIC"
			 Public Const BeneficiaryCode As String = "BeneficiaryCode"
			 Public Const BeneficiaryCountry As String = "BeneficiaryCountry"
			 Public Const BeneficiaryEmail As String = "BeneficiaryEmail"
			 Public Const BeneficiaryMobile As String = "BeneficiaryMobile"
			 Public Const BeneficiaryName As String = "BeneficiaryName"
			 Public Const BeneficiaryPhone As String = "BeneficiaryPhone"
			 Public Const BeneficiaryProvince As String = "BeneficiaryProvince"
			 Public Const BeneficiaryType As String = "BeneficiaryType"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAddress As String = "ClientAddress"
			 Public Const ClientBankCode As String = "ClientBankCode"
			 Public Const ClientBankName As String = "ClientBankName"
			 Public Const ClientBranchAddress As String = "ClientBranchAddress"
			 Public Const ClientBranchCode As String = "ClientBranchCode"
			 Public Const ClientBranchName As String = "ClientBranchName"
			 Public Const ClientCity As String = "ClientCity"
			 Public Const ClientCountry As String = "ClientCountry"
			 Public Const ClientEmail As String = "ClientEmail"
			 Public Const ClientMobile As String = "ClientMobile"
			 Public Const ClientName As String = "ClientName"
			 Public Const ClientProvince As String = "ClientProvince"
			 Public Const DetailAmount As String = "DetailAmount"
			 Public Const DDPayableLocation As String = "DDPayableLocation"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const InvoiceNo As String = "InvoiceNo"
			 Public Const PaymentMode As String = "PaymentMode"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const PrintLocationName As String = "PrintLocationName"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const TransactionDate As String = "TransactionDate"
			 Public Const TXNCode As String = "TXNCode"
			 Public Const ValueDate As String = "ValueDate"
			 Public Const IsPrinted As String = "IsPrinted"
			 Public Const IsRePrinted As String = "IsRePrinted"
			 Public Const RePrintCounter As String = "RePrintCounter"
			 Public Const IsReIssued As String = "IsReIssued"
			 Public Const IsReValidated As String = "IsReValidated"
			 Public Const ReferenceField1 As String = "ReferenceField1"
			 Public Const ReferenceField2 As String = "ReferenceField2"
			 Public Const ReferenceField3 As String = "ReferenceField3"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const BeneAccountBranchCode As String = "BeneAccountBranchCode"
			 Public Const BeneAccountCurrency As String = "BeneAccountCurrency"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const ClearingAccountNo As String = "ClearingAccountNo"
			 Public Const ClearingAccountBranchCode As String = "ClearingAccountBranchCode"
			 Public Const ClearingAccountCurrency As String = "ClearingAccountCurrency"
			 Public Const DDDrawnCityCode As String = "DDDrawnCityCode"
			 Public Const PrintLocationCode As String = "PrintLocationCode"
			 Public Const CreatedOfficeCode As String = "CreatedOfficeCode"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const ReferenceNo As String = "ReferenceNo"
			 Public Const PreviousPrintLocationName As String = "PreviousPrintLocationName"
			 Public Const PreviousPrintLocationCode As String = "PreviousPrintLocationCode"
			 Public Const VerificationDate As String = "VerificationDate"
			 Public Const ApprovalDate As String = "ApprovalDate"
			 Public Const StaleDate As String = "StaleDate"
			 Public Const CancellationDate As String = "CancellationDate"
			 Public Const LastPrintDate As String = "LastPrintDate"
			 Public Const RevalidationDate As String = "RevalidationDate"
			 Public Const DrawnOnBranchCode As String = "DrawnOnBranchCode"
			 Public Const ClientPrimaryApprovedBy As String = "ClientPrimaryApprovedBy"
			 Public Const ClientSecondaryApprovedBy As String = "ClientSecondaryApprovedBy"
			 Public Const ClientPrimaryApprovedDate As String = "ClientPrimaryApprovedDate"
			 Public Const ClientSecondaryApprovalDateTime As String = "ClientSecondaryApprovalDateTime"
			 Public Const BankSecondaryApprovedBy As String = "BankSecondaryApprovedBy"
			 Public Const BankPrimaryApprovedDate As String = "BankPrimaryApprovedDate"
			 Public Const BankSecondaryApprovedDate As String = "BankSecondaryApprovedDate"
			 Public Const BankPrimaryApprovedBy As String = "BankPrimaryApprovedBy"
			 Public Const PrintLocationAmendedBy As String = "PrintLocationAmendedBy"
			 Public Const PrintLocationAmendmentDate As String = "PrintLocationAmendmentDate"
			 Public Const PrintLocationAmendmentApprovedBy As String = "PrintLocationAmendmentApprovedBy"
			 Public Const PrintLocationApprovedAmendmentDate As String = "PrintLocationApprovedAmendmentDate"
			 Public Const IsPrintLocationAmendmentApproved As String = "IsPrintLocationAmendmentApproved"
			 Public Const PrintLocationAmendmentCancelledBy As String = "PrintLocationAmendmentCancelledBy"
			 Public Const PrintLocationAmendmentCancellDate As String = "PrintLocationAmendmentCancellDate"
			 Public Const IsPrintLocationAmendmentCancelled As String = "IsPrintLocationAmendmentCancelled"
			 Public Const IsPrintLocationAmended As String = "IsPrintLocationAmended"
			 Public Const IsReprintingAllowed As String = "IsReprintingAllowed"
			 Public Const ReprintingAllowedDate As String = "ReprintingAllowedDate"
			 Public Const ReprintingAllowedBy As String = "ReprintingAllowedBy"
			 Public Const IsAllowReprintingApproved As String = "IsAllowReprintingApproved"
			 Public Const AllowReprintingApprovedBy As String = "AllowReprintingApprovedBy"
			 Public Const AllowReprintingApprovedDate As String = "AllowReprintingApprovedDate"
			 Public Const IsAllowReprintingCancelled As String = "IsAllowReprintingCancelled"
			 Public Const ReprintingAllowedCancelledBy As String = "ReprintingAllowedCancelledBy"
			 Public Const ReprintingAllowCancelledDate As String = "ReprintingAllowCancelledDate"
			 Public Const IsReIssuanceAllowed As String = "IsReIssuanceAllowed"
			 Public Const ReissuanceAlloweBy As String = "ReissuanceAlloweBy"
			 Public Const ReIssuanceAllowedDate As String = "ReIssuanceAllowedDate"
			 Public Const IsAllowedReIssuanceApproved As String = "IsAllowedReIssuanceApproved"
			 Public Const AllowReIssuanceApprovedDate As String = "AllowReIssuanceApprovedDate"
			 Public Const AllowedReIssuanceApprovedBy As String = "AllowedReIssuanceApprovedBy"
			 Public Const IsAllowedReIssuanceCancelled As String = "IsAllowedReIssuanceCancelled"
			 Public Const AllowedReIssuanceCancelledDate As String = "AllowedReIssuanceCancelledDate"
			 Public Const AllowedReIssuanceCancelledBy As String = "AllowedReIssuanceCancelledBy"
			 Public Const ReIssuanceID As String = "ReIssuanceID"
			 Public Const NewInstructionID As String = "NewInstructionID"
			 Public Const PhysicalInstrumentNumber As String = "PhysicalInstrumentNumber"
			 Public Const ReValiDatedDate As String = "ReValiDatedDate"
			 Public Const ReValiDationAllowedBy As String = "ReValiDationAllowedBy"
			 Public Const IsReValiDationAllowed As String = "IsReValiDationAllowed"
			 Public Const ReValidationApprovalDate As String = "ReValidationApprovalDate"
			 Public Const ReValidationApprovedBy As String = "ReValidationApprovedBy"
			 Public Const IsReValiDationApproved As String = "IsReValiDationApproved"
			 Public Const ReValiDationCancelDate As String = "ReValiDationCancelDate"
			 Public Const ReValiDationCancelledBy As String = "ReValiDationCancelledBy"
			 Public Const IsReValiDationCancelled As String = "IsReValiDationCancelled"
			 Public Const ClearingBankCode As String = "ClearingBankCode"
			 Public Const ClearingDate As String = "ClearingDate"
			 Public Const ClearingOfficeCode As String = "ClearingOfficeCode"
			 Public Const ClearedBy As String = "ClearedBy"
			 Public Const ClearingApprovedBy As String = "ClearingApprovedBy"
			 Public Const ClearingApprovedDate As String = "ClearingApprovedDate"
			 Public Const ClearingApprovedOfficeCode As String = "ClearingApprovedOfficeCode"
			 Public Const AmendedPrintLocationCode As String = "AmendedPrintLocationCode"
			 Public Const AmendedPrintLocationName As String = "AmendedPrintLocationName"
			 Public Const PayableClientNo As String = "PayableClientNo"
			 Public Const PayableSeqNo As String = "PayableSeqNo"
			 Public Const PayableProfitCentre As String = "PayableProfitCentre"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const ClearingClientNo As String = "ClearingClientNo"
			 Public Const ClearingSeqNo As String = "ClearingSeqNo"
			 Public Const ClearingProfitCentre As String = "ClearingProfitCentre"
			 Public Const ClearingAccountType As String = "ClearingAccountType"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const PrintDate As String = "PrintDate"
			 Public Const IsAmendmentComplete As String = "IsAmendmentComplete"
			 Public Const ClubID As String = "ClubID"
			 Public Const SkipReason As String = "SkipReason"
			 Public Const BeneBranchAddress As String = "BeneBranchAddress"
			 Public Const StatusCancelledBy As String = "StatusCancelledBy"
			 Public Const StatusCancelledDate As String = "StatusCancelledDate"
			 Public Const StatusReversedBy As String = "StatusReversedBy"
			 Public Const StatusReversedOfficeCode As String = "StatusReversedOfficeCode"
			 Public Const StatusReversedDate As String = "StatusReversedDate"
			 Public Const StatusSettledBy As String = "StatusSettledBy"
			 Public Const StatusSettledDate As String = "StatusSettledDate"
			 Public Const StatusCancelledApprovedBy As String = "StatusCancelledApprovedBy"
			 Public Const StatusSettledApprovedBy As String = "StatusSettledApprovedBy"
			 Public Const StatusReversedApprovedBy As String = "StatusReversedApprovedBy"
			 Public Const StatusCancelledApprovedDate As String = "StatusCancelledApprovedDate"
			 Public Const StatusSettledApprovedDate As String = "StatusSettledApprovedDate"
			 Public Const StatusReversedApprovedDate As String = "StatusReversedApprovedDate"
			 Public Const StatusChangeRemarks As String = "StatusChangeRemarks"
			 Public Const StatusApproveRemarks As String = "StatusApproveRemarks"
			 Public Const StatusChangeApprovalCancelledby As String = "StatusChangeApprovalCancelledby"
			 Public Const StatusChangeApprovalCancelledDate As String = "StatusChangeApprovalCancelledDate"
			 Public Const ClearingType As String = "ClearingType"
			 Public Const ScheduleTransactionID As String = "ScheduleTransactionID"
			 Public Const ScheduleTransactionTime As String = "ScheduleTransactionTime"
			 Public Const IsCheduleTransactionProcessed As String = "IsCheduleTransactionProcessed"
			 Public Const BeneficiaryIDType As String = "BeneficiaryIDType"
			 Public Const BeneficiaryIDNo As String = "BeneficiaryIDNo"
			 Public Const Rin As String = "Rin"
			 Public Const DisbursedBy As String = "DisbursedBy"
			 Public Const DisbursedDate As String = "DisbursedDate"
			 Public Const DisbursedBranchCode As String = "DisbursedBranchCode"
			 Public Const CancelledBy As String = "CancelledBy"
			 Public Const CancelledApproveBy As String = "CancelledApproveBy"
			 Public Const CancelledApprovalDate As String = "CancelledApprovalDate"
			 Public Const UBPCompanyID As String = "UBPCompanyID"
			 Public Const UBPReferenceField As String = "UBPReferenceField"
			 Public Const BeneficaryAccountType As String = "BeneficaryAccountType"
			 Public Const BeneID As String = "BeneID"
			 Public Const ReturnReason As String = "ReturnReason"
			 Public Const AmountBeforeDueDate As String = "AmountBeforeDueDate"
			 Public Const AmountAfterDueDate As String = "AmountAfterDueDate"
			 Public Const DueDate As String = "DueDate"
			 Public Const StatusHoldBy As String = "StatusHoldBy"
			 Public Const StatusHoldDate As String = "StatusHoldDate"
			 Public Const StatusHoldApprovedBy As String = "StatusHoldApprovedBy"
			 Public Const StatusHoldApprovedDate As String = "StatusHoldApprovedDate"
			 Public Const StatusUnHoldBy As String = "StatusUnHoldBy"
			 Public Const StatusUnHoldDate As String = "StatusUnHoldDate"
			 Public Const StatusUnHoldApprovedBy As String = "StatusUnHoldApprovedBy"
			 Public Const StatusUnHoldApprovedDate As String = "StatusUnHoldApprovedDate"
			 Public Const StatusRevertBy As String = "StatusRevertBy"
			 Public Const StatusRevertDate As String = "StatusRevertDate"
			 Public Const StatusRevertApprovedBy As String = "StatusRevertApprovedBy"
			 Public Const StatusRevertApprovedDate As String = "StatusRevertApprovedDate"
			 Public Const IBFTStan As String = "IBFTStan"
			 Public Const FinalStatus As String = "FinalStatus"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICInstructionMetadata)
			
				If ICInstructionMetadata.mapDelegates Is Nothing Then
					ICInstructionMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICInstructionMetadata._meta Is Nothing Then
					ICInstructionMetadata._meta = New ICInstructionMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Description", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Remarks", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Status", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FileID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AcquisitionMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("LastStatus", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ErrorMessage", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Amount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("AmountInWords", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAccountTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBankAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryBranchName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCity", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCNIC", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryCountry", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryEmail", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryMobile", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryPhone", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryProvince", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientBankCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientBankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientBranchAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientBranchName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientCity", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientCountry", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientEmail", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientMobile", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientProvince", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DetailAmount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("DDPayableLocation", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InstrumentNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("InvoiceNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PrintLocationName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TransactionDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("TXNCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ValueDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsPrinted", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsRePrinted", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("RePrintCounter", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsReIssued", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsReValidated", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReferenceField1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceField3", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PayableAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DDDrawnCityCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintLocationCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FileBatchNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReferenceNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PreviousPrintLocationName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PreviousPrintLocationCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("VerificationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovalDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StaleDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CancellationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("LastPrintDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("RevalidationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DrawnOnBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientPrimaryApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClientSecondaryApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClientPrimaryApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ClientSecondaryApprovalDateTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("BankSecondaryApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BankPrimaryApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("BankSecondaryApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("BankPrimaryApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintLocationAmendedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintLocationAmendmentDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PrintLocationAmendmentApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintLocationApprovedAmendmentDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsPrintLocationAmendmentApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("PrintLocationAmendmentCancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PrintLocationAmendmentCancellDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsPrintLocationAmendmentCancelled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsPrintLocationAmended", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsReprintingAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReprintingAllowedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReprintingAllowedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsAllowReprintingApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("AllowReprintingApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AllowReprintingApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAllowReprintingCancelled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReprintingAllowedCancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReprintingAllowCancelledDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsReIssuanceAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReissuanceAlloweBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReIssuanceAllowedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAllowedReIssuanceApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("AllowReIssuanceApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AllowedReIssuanceApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsAllowedReIssuanceCancelled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("AllowedReIssuanceCancelledDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AllowedReIssuanceCancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReIssuanceID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("NewInstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PhysicalInstrumentNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ReValiDatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReValiDationAllowedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsReValiDationAllowed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReValidationApprovalDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReValidationApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsReValiDationApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ReValiDationCancelDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ReValiDationCancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsReValiDationCancelled", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ClearingBankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClearingDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ClearingOfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClearedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClearingApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClearingApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ClearingApprovedOfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AmendedPrintLocationCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AmendedPrintLocationName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingProfitCentre", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClearingAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("PrintDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAmendmentComplete", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ClubID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SkipReason", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneBranchAddress", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("StatusCancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusCancelledDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusReversedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusReversedOfficeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusReversedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusSettledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusSettledDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusCancelledApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusSettledApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusReversedApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusCancelledApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusSettledApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusReversedApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusChangeRemarks", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("StatusApproveRemarks", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("StatusChangeApprovalCancelledby", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusChangeApprovalCancelledDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ClearingType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ScheduleTransactionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ScheduleTransactionTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsCheduleTransactionProcessed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("BeneficiaryIDType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficiaryIDNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Rin", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DisbursedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("DisbursedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DisbursedBranchCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CancelledBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CancelledApproveBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CancelledApprovalDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("UBPCompanyID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UBPReferenceField", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneficaryAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReturnReason", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("AmountBeforeDueDate", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("AmountAfterDueDate", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("DueDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusHoldBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusHoldDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusHoldApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusHoldApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusUnHoldBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusUnHoldDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusUnHoldApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusUnHoldApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusRevertBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusRevertDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("StatusRevertApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("StatusRevertApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IBFTStan", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FinalStatus", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_Instruction"
				meta.Destination = "IC_Instruction"
				
				meta.spInsert = "proc_IC_InstructionInsert"
				meta.spUpdate = "proc_IC_InstructionUpdate"
				meta.spDelete = "proc_IC_InstructionDelete"
				meta.spLoadAll = "proc_IC_InstructionLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_InstructionLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICInstructionMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
