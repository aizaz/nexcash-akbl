
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Events' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICEvents))> _
	<XmlType("ICEvents")> _	
	Partial Public Class ICEvents 
		Inherits esICEvents
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICEvents()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal eventID As System.Int32)
			Dim obj As New ICEvents()
			obj.EventID = eventID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal eventID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICEvents()
			obj.EventID = eventID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICEventsCollection")> _
	Partial Public Class ICEventsCollection
		Inherits esICEventsCollection
		Implements IEnumerable(Of ICEvents)
	
		Public Function FindByPrimaryKey(ByVal eventID As System.Int32) As ICEvents
			Return MyBase.SingleOrDefault(Function(e) e.EventID.HasValue AndAlso e.EventID.Value = eventID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICEvents))> _
		Public Class ICEventsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICEventsCollection)
			
			Public Shared Widening Operator CType(packet As ICEventsCollectionWCFPacket) As ICEventsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICEventsCollection) As ICEventsCollectionWCFPacket
				Return New ICEventsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICEventsQuery 
		Inherits esICEventsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICEventsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICEventsQuery) As String
			Return ICEventsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICEventsQuery
			Return DirectCast(ICEventsQuery.SerializeHelper.FromXml(query, GetType(ICEventsQuery)), ICEventsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICEvents
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal eventID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(eventID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(eventID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal eventID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(eventID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(eventID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal eventID As System.Int32) As Boolean
		
			Dim query As New ICEventsQuery()
			query.Where(query.EventID = eventID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal eventID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("EventID", eventID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Events.EventID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EventID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICEventsMetadata.ColumnNames.EventID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICEventsMetadata.ColumnNames.EventID, value) Then
					OnPropertyChanged(ICEventsMetadata.PropertyNames.EventID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Events.EventName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EventName As System.String
			Get
				Return MyBase.GetSystemString(ICEventsMetadata.ColumnNames.EventName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICEventsMetadata.ColumnNames.EventName, value) Then
					OnPropertyChanged(ICEventsMetadata.PropertyNames.EventName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Events.EventTemplateName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EventTemplateName As System.String
			Get
				Return MyBase.GetSystemString(ICEventsMetadata.ColumnNames.EventTemplateName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICEventsMetadata.ColumnNames.EventTemplateName, value) Then
					OnPropertyChanged(ICEventsMetadata.PropertyNames.EventTemplateName)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "EventID"
							Me.str().EventID = CType(value, string)
												
						Case "EventName"
							Me.str().EventName = CType(value, string)
												
						Case "EventTemplateName"
							Me.str().EventTemplateName = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "EventID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.EventID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICEventsMetadata.PropertyNames.EventID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICEvents)
				Me.entity = entity
			End Sub				
		
	
			Public Property EventID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.EventID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EventID = Nothing
					Else
						entity.EventID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property EventName As System.String 
				Get
					Dim data_ As System.String = entity.EventName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EventName = Nothing
					Else
						entity.EventName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property EventTemplateName As System.String 
				Get
					Dim data_ As System.String = entity.EventTemplateName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EventTemplateName = Nothing
					Else
						entity.EventTemplateName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICEvents
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICEventsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICEventsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICEventsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICEventsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICEventsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICEventsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICEventsCollection
		Inherits esEntityCollection(Of ICEvents)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICEventsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICEventsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICEventsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICEventsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICEventsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICEventsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICEventsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICEventsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICEventsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICEventsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICEventsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "EventID" 
					Return Me.EventID
				Case "EventName" 
					Return Me.EventName
				Case "EventTemplateName" 
					Return Me.EventTemplateName
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property EventID As esQueryItem
			Get
				Return New esQueryItem(Me, ICEventsMetadata.ColumnNames.EventID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property EventName As esQueryItem
			Get
				Return New esQueryItem(Me, ICEventsMetadata.ColumnNames.EventName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property EventTemplateName As esQueryItem
			Get
				Return New esQueryItem(Me, ICEventsMetadata.ColumnNames.EventTemplateName, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICEvents 
		Inherits esICEvents
		
	
		#Region "ICNotificationManagementCollectionByEventID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICNotificationManagementCollectionByEventID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICEvents.ICNotificationManagementCollectionByEventID_Delegate)
				map.PropertyName = "ICNotificationManagementCollectionByEventID"
				map.MyColumnName = "EventID"
				map.ParentColumnName = "EventID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICNotificationManagementCollectionByEventID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICEventsQuery(data.NextAlias())
			
			Dim mee As ICNotificationManagementQuery = If(data.You IsNot Nothing, TryCast(data.You, ICNotificationManagementQuery), New ICNotificationManagementQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.EventID = mee.EventID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_NotificationManagement_IC_Events
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICNotificationManagementCollectionByEventID As ICNotificationManagementCollection 
		
			Get
				If Me._ICNotificationManagementCollectionByEventID Is Nothing Then
					Me._ICNotificationManagementCollectionByEventID = New ICNotificationManagementCollection()
					Me._ICNotificationManagementCollectionByEventID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICNotificationManagementCollectionByEventID", Me._ICNotificationManagementCollectionByEventID)
				
					If Not Me.EventID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICNotificationManagementCollectionByEventID.Query.Where(Me._ICNotificationManagementCollectionByEventID.Query.EventID = Me.EventID)
							Me._ICNotificationManagementCollectionByEventID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICNotificationManagementCollectionByEventID.fks.Add(ICNotificationManagementMetadata.ColumnNames.EventID, Me.EventID)
					End If
				End If

				Return Me._ICNotificationManagementCollectionByEventID
			End Get
			
			Set(ByVal value As ICNotificationManagementCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICNotificationManagementCollectionByEventID Is Nothing Then

					Me.RemovePostSave("ICNotificationManagementCollectionByEventID")
					Me._ICNotificationManagementCollectionByEventID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICNotificationManagementCollectionByEventID As ICNotificationManagementCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICNotificationManagementCollectionByEventID"
					coll = Me.ICNotificationManagementCollectionByEventID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICNotificationManagementCollectionByEventID", GetType(ICNotificationManagementCollection), New ICNotificationManagement()))
			Return props
			
		End Function
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICNotificationManagementCollectionByEventID Is Nothing Then
				Apply(Me._ICNotificationManagementCollectionByEventID, "EventID", Me.EventID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICEventsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICEventsMetadata.ColumnNames.EventID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICEventsMetadata.PropertyNames.EventID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICEventsMetadata.ColumnNames.EventName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICEventsMetadata.PropertyNames.EventName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICEventsMetadata.ColumnNames.EventTemplateName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICEventsMetadata.PropertyNames.EventTemplateName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICEventsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const EventID As String = "EventID"
			 Public Const EventName As String = "EventName"
			 Public Const EventTemplateName As String = "EventTemplateName"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const EventID As String = "EventID"
			 Public Const EventName As String = "EventName"
			 Public Const EventTemplateName As String = "EventTemplateName"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICEventsMetadata)
			
				If ICEventsMetadata.mapDelegates Is Nothing Then
					ICEventsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICEventsMetadata._meta Is Nothing Then
					ICEventsMetadata._meta = New ICEventsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("EventID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("EventName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("EventTemplateName", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_Events"
				meta.Destination = "IC_Events"
				
				meta.spInsert = "proc_IC_EventsInsert"
				meta.spUpdate = "proc_IC_EventsUpdate"
				meta.spDelete = "proc_IC_EventsDelete"
				meta.spLoadAll = "proc_IC_EventsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_EventsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICEventsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
