
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Files' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICFiles))> _
	<XmlType("ICFiles")> _	
	Partial Public Class ICFiles 
		Inherits esICFiles
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICFiles()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fileID As System.Int32)
			Dim obj As New ICFiles()
			obj.FileID = fileID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fileID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICFiles()
			obj.FileID = fileID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICFilesCollection")> _
	Partial Public Class ICFilesCollection
		Inherits esICFilesCollection
		Implements IEnumerable(Of ICFiles)
	
		Public Function FindByPrimaryKey(ByVal fileID As System.Int32) As ICFiles
			Return MyBase.SingleOrDefault(Function(e) e.FileID.HasValue AndAlso e.FileID.Value = fileID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICFiles))> _
		Public Class ICFilesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICFilesCollection)
			
			Public Shared Widening Operator CType(packet As ICFilesCollectionWCFPacket) As ICFilesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICFilesCollection) As ICFilesCollectionWCFPacket
				Return New ICFilesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICFilesQuery 
		Inherits esICFilesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICFilesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICFilesQuery) As String
			Return ICFilesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICFilesQuery
			Return DirectCast(ICFilesQuery.SerializeHelper.FromXml(query, GetType(ICFilesQuery)), ICFilesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICFiles
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fileID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fileID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fileID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fileID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fileID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fileID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fileID As System.Int32) As Boolean
		
			Dim query As New ICFilesQuery()
			query.Where(query.FileID = fileID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fileID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FileID", fileID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Files.FileID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.FileID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.FileID, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileName As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.FileName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.FileName, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileType As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.FileType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.FileType, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.RelatedId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedId As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.RelatedId)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.RelatedId, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.RelatedId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileSize
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileSize As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.FileSize)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.FileSize, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileSize)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileData
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileData As System.Byte()
			Get
				Return MyBase.GetSystemByteArray(ICFilesMetadata.ColumnNames.FileData)
			End Get
			
			Set(ByVal value As System.Byte())
				If MyBase.SetSystemByteArray(ICFilesMetadata.ColumnNames.FileData, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileData)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileMIMEType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileMIMEType As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.FileMIMEType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.FileMIMEType, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileMIMEType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileLocation
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileLocation As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.FileLocation)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.FileLocation, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileLocation)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICFilesMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.PublishDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PublishDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFilesMetadata.ColumnNames.PublishDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFilesMetadata.ColumnNames.PublishDate, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.PublishDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.AcqMode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AcqMode As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.AcqMode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.AcqMode, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.AcqMode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.OriginalFileName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OriginalFileName As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.OriginalFileName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.OriginalFileName, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.OriginalFileName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileUploadTemplateID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileUploadTemplateID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.FileUploadTemplateID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.FileUploadTemplateID, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileUploadTemplateID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileBatchNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileBatchNo As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.FileBatchNo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.FileBatchNo, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileBatchNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.Status
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Status As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.Status)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.Status, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.Status)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFilesMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFilesMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.ProcessDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProcessDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFilesMetadata.ColumnNames.ProcessDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFilesMetadata.ColumnNames.ProcessDate, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.ProcessDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.ProcessBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProcessBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.ProcessBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.ProcessBy, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.ProcessBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.TransactionCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TransactionCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.TransactionCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.TransactionCount, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.TransactionCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.TotalAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TotalAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICFilesMetadata.ColumnNames.TotalAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICFilesMetadata.ColumnNames.TotalAmount, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.TotalAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.AccountNumber, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.BranchCode, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.Currency, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.PaymentNatureCode, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.FileBatchName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileBatchName As System.String
			Get
				Return MyBase.GetSystemString(ICFilesMetadata.ColumnNames.FileBatchName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICFilesMetadata.ColumnNames.FileBatchName, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.FileBatchName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICFilesMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICFilesMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Files.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICFilesMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICFilesMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICFilesMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FileID"
							Me.str().FileID = CType(value, string)
												
						Case "FileName"
							Me.str().FileName = CType(value, string)
												
						Case "FileType"
							Me.str().FileType = CType(value, string)
												
						Case "RelatedId"
							Me.str().RelatedId = CType(value, string)
												
						Case "FileSize"
							Me.str().FileSize = CType(value, string)
												
						Case "FileMIMEType"
							Me.str().FileMIMEType = CType(value, string)
												
						Case "FileLocation"
							Me.str().FileLocation = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "PublishDate"
							Me.str().PublishDate = CType(value, string)
												
						Case "AcqMode"
							Me.str().AcqMode = CType(value, string)
												
						Case "OriginalFileName"
							Me.str().OriginalFileName = CType(value, string)
												
						Case "FileUploadTemplateID"
							Me.str().FileUploadTemplateID = CType(value, string)
												
						Case "FileBatchNo"
							Me.str().FileBatchNo = CType(value, string)
												
						Case "Status"
							Me.str().Status = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "ProcessDate"
							Me.str().ProcessDate = CType(value, string)
												
						Case "ProcessBy"
							Me.str().ProcessBy = CType(value, string)
												
						Case "TransactionCount"
							Me.str().TransactionCount = CType(value, string)
												
						Case "TotalAmount"
							Me.str().TotalAmount = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "FileBatchName"
							Me.str().FileBatchName = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FileID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.FileID)
							End If
						
						Case "FileSize"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileSize = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.FileSize)
							End If
						
						Case "FileData"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Byte()" Then
								Me.FileData = CType(value, System.Byte())
								OnPropertyChanged(ICFilesMetadata.PropertyNames.FileData)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "PublishDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.PublishDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.PublishDate)
							End If
						
						Case "FileUploadTemplateID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileUploadTemplateID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.FileUploadTemplateID)
							End If
						
						Case "FileBatchNo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FileBatchNo = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.FileBatchNo)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "ProcessDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ProcessDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.ProcessDate)
							End If
						
						Case "ProcessBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ProcessBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.ProcessBy)
							End If
						
						Case "TransactionCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TransactionCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.TransactionCount)
							End If
						
						Case "TotalAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.TotalAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.TotalAmount)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICFilesMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICFiles)
				Me.entity = entity
			End Sub				
		
	
			Public Property FileID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileID = Nothing
					Else
						entity.FileID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileName As System.String 
				Get
					Dim data_ As System.String = entity.FileName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileName = Nothing
					Else
						entity.FileName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileType As System.String 
				Get
					Dim data_ As System.String = entity.FileType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileType = Nothing
					Else
						entity.FileType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedId As System.String 
				Get
					Dim data_ As System.String = entity.RelatedId
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedId = Nothing
					Else
						entity.RelatedId = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileSize As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileSize
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileSize = Nothing
					Else
						entity.FileSize = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileMIMEType As System.String 
				Get
					Dim data_ As System.String = entity.FileMIMEType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileMIMEType = Nothing
					Else
						entity.FileMIMEType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileLocation As System.String 
				Get
					Dim data_ As System.String = entity.FileLocation
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileLocation = Nothing
					Else
						entity.FileLocation = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PublishDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.PublishDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PublishDate = Nothing
					Else
						entity.PublishDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property AcqMode As System.String 
				Get
					Dim data_ As System.String = entity.AcqMode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AcqMode = Nothing
					Else
						entity.AcqMode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OriginalFileName As System.String 
				Get
					Dim data_ As System.String = entity.OriginalFileName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OriginalFileName = Nothing
					Else
						entity.OriginalFileName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileUploadTemplateID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileUploadTemplateID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileUploadTemplateID = Nothing
					Else
						entity.FileUploadTemplateID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileBatchNo As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FileBatchNo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileBatchNo = Nothing
					Else
						entity.FileBatchNo = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Status As System.String 
				Get
					Dim data_ As System.String = entity.Status
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Status = Nothing
					Else
						entity.Status = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProcessDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ProcessDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProcessDate = Nothing
					Else
						entity.ProcessDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProcessBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ProcessBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProcessBy = Nothing
					Else
						entity.ProcessBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TransactionCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TransactionCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TransactionCount = Nothing
					Else
						entity.TransactionCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property TotalAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.TotalAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TotalAmount = Nothing
					Else
						entity.TotalAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileBatchName As System.String 
				Get
					Dim data_ As System.String = entity.FileBatchName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileBatchName = Nothing
					Else
						entity.FileBatchName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICFiles
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFilesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICFilesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFilesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICFilesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICFilesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICFilesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICFilesCollection
		Inherits esEntityCollection(Of ICFiles)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICFilesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICFilesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICFilesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICFilesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICFilesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICFilesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICFilesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICFilesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICFilesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICFilesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICFilesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FileID" 
					Return Me.FileID
				Case "FileName" 
					Return Me.FileName
				Case "FileType" 
					Return Me.FileType
				Case "RelatedId" 
					Return Me.RelatedId
				Case "FileSize" 
					Return Me.FileSize
				Case "FileData" 
					Return Me.FileData
				Case "FileMIMEType" 
					Return Me.FileMIMEType
				Case "FileLocation" 
					Return Me.FileLocation
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "PublishDate" 
					Return Me.PublishDate
				Case "AcqMode" 
					Return Me.AcqMode
				Case "OriginalFileName" 
					Return Me.OriginalFileName
				Case "FileUploadTemplateID" 
					Return Me.FileUploadTemplateID
				Case "FileBatchNo" 
					Return Me.FileBatchNo
				Case "Status" 
					Return Me.Status
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "ProcessDate" 
					Return Me.ProcessDate
				Case "ProcessBy" 
					Return Me.ProcessBy
				Case "TransactionCount" 
					Return Me.TransactionCount
				Case "TotalAmount" 
					Return Me.TotalAmount
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "FileBatchName" 
					Return Me.FileBatchName
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FileID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FileName As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedId As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.RelatedId, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileSize As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileSize, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FileData As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileData, esSystemType.ByteArray)
			End Get
		End Property 
		
		Public ReadOnly Property FileMIMEType As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileMIMEType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileLocation As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileLocation, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PublishDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.PublishDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property AcqMode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.AcqMode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OriginalFileName As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.OriginalFileName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileUploadTemplateID As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileUploadTemplateID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FileBatchNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileBatchNo, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Status As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.Status, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ProcessDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.ProcessDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ProcessBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.ProcessBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TransactionCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.TransactionCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property TotalAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.TotalAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileBatchName As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.FileBatchName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICFilesMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICFiles 
		Inherits esICFiles
		
	
		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_Files_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICFilesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileName
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileType, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.RelatedId, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.RelatedId
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileSize, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileSize
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileData, 5, GetType(System.Byte()), esSystemType.ByteArray)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileData
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileMIMEType, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileMIMEType
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileLocation, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileLocation
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.CompanyCode, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.CreatedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.PublishDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFilesMetadata.PropertyNames.PublishDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.AcqMode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.AcqMode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.OriginalFileName, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.OriginalFileName
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileUploadTemplateID, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileUploadTemplateID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileBatchNo, 14, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileBatchNo
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.Status, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.Status
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.CreatedDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFilesMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.ProcessDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFilesMetadata.PropertyNames.ProcessDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.ProcessBy, 18, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.ProcessBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.TransactionCount, 19, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.TransactionCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.TotalAmount, 20, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICFilesMetadata.PropertyNames.TotalAmount
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.AccountNumber, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.BranchCode, 22, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.Currency, 23, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.PaymentNatureCode, 24, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.FileBatchName, 25, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICFilesMetadata.PropertyNames.FileBatchName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.Creater, 26, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICFilesMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICFilesMetadata.ColumnNames.CreationDate, 27, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICFilesMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICFilesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FileID As String = "FileID"
			 Public Const FileName As String = "FileName"
			 Public Const FileType As String = "FileType"
			 Public Const RelatedId As String = "RelatedId"
			 Public Const FileSize As String = "FileSize"
			 Public Const FileData As String = "FileData"
			 Public Const FileMIMEType As String = "FileMIMEType"
			 Public Const FileLocation As String = "FileLocation"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const PublishDate As String = "PublishDate"
			 Public Const AcqMode As String = "AcqMode"
			 Public Const OriginalFileName As String = "OriginalFileName"
			 Public Const FileUploadTemplateID As String = "FileUploadTemplateID"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const Status As String = "Status"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ProcessDate As String = "ProcessDate"
			 Public Const ProcessBy As String = "ProcessBy"
			 Public Const TransactionCount As String = "TransactionCount"
			 Public Const TotalAmount As String = "TotalAmount"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FileBatchName As String = "FileBatchName"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FileID As String = "FileID"
			 Public Const FileName As String = "FileName"
			 Public Const FileType As String = "FileType"
			 Public Const RelatedId As String = "RelatedId"
			 Public Const FileSize As String = "FileSize"
			 Public Const FileData As String = "FileData"
			 Public Const FileMIMEType As String = "FileMIMEType"
			 Public Const FileLocation As String = "FileLocation"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const PublishDate As String = "PublishDate"
			 Public Const AcqMode As String = "AcqMode"
			 Public Const OriginalFileName As String = "OriginalFileName"
			 Public Const FileUploadTemplateID As String = "FileUploadTemplateID"
			 Public Const FileBatchNo As String = "FileBatchNo"
			 Public Const Status As String = "Status"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ProcessDate As String = "ProcessDate"
			 Public Const ProcessBy As String = "ProcessBy"
			 Public Const TransactionCount As String = "TransactionCount"
			 Public Const TotalAmount As String = "TotalAmount"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const FileBatchName As String = "FileBatchName"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICFilesMetadata)
			
				If ICFilesMetadata.mapDelegates Is Nothing Then
					ICFilesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICFilesMetadata._meta Is Nothing Then
					ICFilesMetadata._meta = New ICFilesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FileID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FileName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RelatedId", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileSize", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FileData", new esTypeMap("varbinary", "System.Byte()"))
				meta.AddTypeMap("FileMIMEType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileLocation", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PublishDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("AcqMode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("OriginalFileName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileUploadTemplateID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FileBatchNo", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ProcessDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ProcessBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TransactionCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("TotalAmount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileBatchName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Files"
				meta.Destination = "IC_Files"
				
				meta.spInsert = "proc_IC_FilesInsert"
				meta.spUpdate = "proc_IC_FilesUpdate"
				meta.spDelete = "proc_IC_FilesDelete"
				meta.spLoadAll = "proc_IC_FilesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_FilesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICFilesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
