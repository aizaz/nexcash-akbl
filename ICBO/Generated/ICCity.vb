
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 7:28:14 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_City' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCity))> _
	<XmlType("ICCity")> _	
	Partial Public Class ICCity 
		Inherits esICCity
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCity()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal cityCode As System.Int32)
			Dim obj As New ICCity()
			obj.CityCode = cityCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal cityCode As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCity()
			obj.CityCode = cityCode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCityCollection")> _
	Partial Public Class ICCityCollection
		Inherits esICCityCollection
		Implements IEnumerable(Of ICCity)
	
		Public Function FindByPrimaryKey(ByVal cityCode As System.Int32) As ICCity
			Return MyBase.SingleOrDefault(Function(e) e.CityCode.HasValue AndAlso e.CityCode.Value = cityCode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCity))> _
		Public Class ICCityCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCityCollection)
			
			Public Shared Widening Operator CType(packet As ICCityCollectionWCFPacket) As ICCityCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCityCollection) As ICCityCollectionWCFPacket
				Return New ICCityCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCityQuery 
		Inherits esICCityQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCityQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCityQuery) As String
			Return ICCityQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCityQuery
			Return DirectCast(ICCityQuery.SerializeHelper.FromXml(query, GetType(ICCityQuery)), ICCityQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCity
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal cityCode As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(cityCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(cityCode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal cityCode As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(cityCode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(cityCode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal cityCode As System.Int32) As Boolean
		
			Dim query As New ICCityQuery()
			query.Where(query.CityCode = cityCode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal cityCode As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("CityCode", cityCode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_City.CityCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CityCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCityMetadata.ColumnNames.CityCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCityMetadata.ColumnNames.CityCode, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.CityCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.CityName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CityName As System.String
			Get
				Return MyBase.GetSystemString(ICCityMetadata.ColumnNames.CityName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCityMetadata.ColumnNames.CityName, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.CityName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.ProvinceCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProvinceCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCityMetadata.ColumnNames.ProvinceCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCityMetadata.ColumnNames.ProvinceCode, value) Then
					Me._UpToICProvinceByProvinceCode = Nothing
					Me.OnPropertyChanged("UpToICProvinceByProvinceCode")
					OnPropertyChanged(ICCityMetadata.PropertyNames.ProvinceCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCityMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCityMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCityMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCityMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCityMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCityMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCityMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCityMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCityMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCityMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCityMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCityMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCityMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCityMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICCityMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICCityMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_City.DisplayCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DisplayCode As System.String
			Get
				Return MyBase.GetSystemString(ICCityMetadata.ColumnNames.DisplayCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCityMetadata.ColumnNames.DisplayCode, value) Then
					OnPropertyChanged(ICCityMetadata.PropertyNames.DisplayCode)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICProvinceByProvinceCode As ICProvince
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "CityCode"
							Me.str().CityCode = CType(value, string)
												
						Case "CityName"
							Me.str().CityName = CType(value, string)
												
						Case "ProvinceCode"
							Me.str().ProvinceCode = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "DisplayCode"
							Me.str().DisplayCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "CityCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CityCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCityMetadata.PropertyNames.CityCode)
							End If
						
						Case "ProvinceCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ProvinceCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCityMetadata.PropertyNames.ProvinceCode)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCityMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCityMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCityMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCityMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCityMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCityMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCityMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICCityMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCity)
				Me.entity = entity
			End Sub				
		
	
			Public Property CityCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CityCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CityCode = Nothing
					Else
						entity.CityCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CityName As System.String 
				Get
					Dim data_ As System.String = entity.CityName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CityName = Nothing
					Else
						entity.CityName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProvinceCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ProvinceCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProvinceCode = Nothing
					Else
						entity.ProvinceCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property DisplayCode As System.String 
				Get
					Dim data_ As System.String = entity.DisplayCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DisplayCode = Nothing
					Else
						entity.DisplayCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCity
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCityMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCityQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCityQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCityQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCityQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCityQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCityCollection
		Inherits esEntityCollection(Of ICCity)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCityMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCityCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCityQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCityQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCityQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCityQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCityQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCityQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCityQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCityQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCityMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "CityCode" 
					Return Me.CityCode
				Case "CityName" 
					Return Me.CityName
				Case "ProvinceCode" 
					Return Me.ProvinceCode
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "DisplayCode" 
					Return Me.DisplayCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property CityCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.CityCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CityName As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.CityName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProvinceCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.ProvinceCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property DisplayCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICCityMetadata.ColumnNames.DisplayCode, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCity 
		Inherits esICCity
		
	
		#Region "ICAreaCollectionByCityCode - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICAreaCollectionByCityCode() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCity.ICAreaCollectionByCityCode_Delegate)
				map.PropertyName = "ICAreaCollectionByCityCode"
				map.MyColumnName = "CityCode"
				map.ParentColumnName = "CityCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICAreaCollectionByCityCode_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCityQuery(data.NextAlias())
			
			Dim mee As ICAreaQuery = If(data.You IsNot Nothing, TryCast(data.You, ICAreaQuery), New ICAreaQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CityCode = mee.CityCode)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Area_IC_City
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICAreaCollectionByCityCode As ICAreaCollection 
		
			Get
				If Me._ICAreaCollectionByCityCode Is Nothing Then
					Me._ICAreaCollectionByCityCode = New ICAreaCollection()
					Me._ICAreaCollectionByCityCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICAreaCollectionByCityCode", Me._ICAreaCollectionByCityCode)
				
					If Not Me.CityCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICAreaCollectionByCityCode.Query.Where(Me._ICAreaCollectionByCityCode.Query.CityCode = Me.CityCode)
							Me._ICAreaCollectionByCityCode.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICAreaCollectionByCityCode.fks.Add(ICAreaMetadata.ColumnNames.CityCode, Me.CityCode)
					End If
				End If

				Return Me._ICAreaCollectionByCityCode
			End Get
			
			Set(ByVal value As ICAreaCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICAreaCollectionByCityCode Is Nothing Then

					Me.RemovePostSave("ICAreaCollectionByCityCode")
					Me._ICAreaCollectionByCityCode = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICAreaCollectionByCityCode As ICAreaCollection
		#End Region

		#Region "ICCompanyCollectionByCity - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCompanyCollectionByCity() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCity.ICCompanyCollectionByCity_Delegate)
				map.PropertyName = "ICCompanyCollectionByCity"
				map.MyColumnName = "City"
				map.ParentColumnName = "CityCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCompanyCollectionByCity_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCityQuery(data.NextAlias())
			
			Dim mee As ICCompanyQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCompanyQuery), New ICCompanyQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CityCode = mee.City)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Company_IC_City
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCompanyCollectionByCity As ICCompanyCollection 
		
			Get
				If Me._ICCompanyCollectionByCity Is Nothing Then
					Me._ICCompanyCollectionByCity = New ICCompanyCollection()
					Me._ICCompanyCollectionByCity.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCompanyCollectionByCity", Me._ICCompanyCollectionByCity)
				
					If Not Me.CityCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCompanyCollectionByCity.Query.Where(Me._ICCompanyCollectionByCity.Query.City = Me.CityCode)
							Me._ICCompanyCollectionByCity.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCompanyCollectionByCity.fks.Add(ICCompanyMetadata.ColumnNames.City, Me.CityCode)
					End If
				End If

				Return Me._ICCompanyCollectionByCity
			End Get
			
			Set(ByVal value As ICCompanyCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCompanyCollectionByCity Is Nothing Then

					Me.RemovePostSave("ICCompanyCollectionByCity")
					Me._ICCompanyCollectionByCity = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCompanyCollectionByCity As ICCompanyCollection
		#End Region

		#Region "ICOfficeCollectionByCityID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICOfficeCollectionByCityID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICCity.ICOfficeCollectionByCityID_Delegate)
				map.PropertyName = "ICOfficeCollectionByCityID"
				map.MyColumnName = "CityID"
				map.ParentColumnName = "CityCode"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICOfficeCollectionByCityID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICCityQuery(data.NextAlias())
			
			Dim mee As ICOfficeQuery = If(data.You IsNot Nothing, TryCast(data.You, ICOfficeQuery), New ICOfficeQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.CityCode = mee.CityID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Office_IC_City
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICOfficeCollectionByCityID As ICOfficeCollection 
		
			Get
				If Me._ICOfficeCollectionByCityID Is Nothing Then
					Me._ICOfficeCollectionByCityID = New ICOfficeCollection()
					Me._ICOfficeCollectionByCityID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICOfficeCollectionByCityID", Me._ICOfficeCollectionByCityID)
				
					If Not Me.CityCode.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICOfficeCollectionByCityID.Query.Where(Me._ICOfficeCollectionByCityID.Query.CityID = Me.CityCode)
							Me._ICOfficeCollectionByCityID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICOfficeCollectionByCityID.fks.Add(ICOfficeMetadata.ColumnNames.CityID, Me.CityCode)
					End If
				End If

				Return Me._ICOfficeCollectionByCityID
			End Get
			
			Set(ByVal value As ICOfficeCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICOfficeCollectionByCityID Is Nothing Then

					Me.RemovePostSave("ICOfficeCollectionByCityID")
					Me._ICOfficeCollectionByCityID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICOfficeCollectionByCityID As ICOfficeCollection
		#End Region

		#Region "UpToICProvinceByProvinceCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_City_IC_Province1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICProvinceByProvinceCode As ICProvince
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICProvinceByProvinceCode Is Nothing _
						 AndAlso Not ProvinceCode.Equals(Nothing)  Then
						
					Me._UpToICProvinceByProvinceCode = New ICProvince()
					Me._UpToICProvinceByProvinceCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICProvinceByProvinceCode", Me._UpToICProvinceByProvinceCode)
					Me._UpToICProvinceByProvinceCode.Query.Where(Me._UpToICProvinceByProvinceCode.Query.ProvinceCode = Me.ProvinceCode)
					Me._UpToICProvinceByProvinceCode.Query.Load()
				End If

				Return Me._UpToICProvinceByProvinceCode
			End Get
			
            Set(ByVal value As ICProvince)
				Me.RemovePreSave("UpToICProvinceByProvinceCode")
				

				If value Is Nothing Then
				
					Me.ProvinceCode = Nothing
				
					Me._UpToICProvinceByProvinceCode = Nothing
				Else
				
					Me.ProvinceCode = value.ProvinceCode
					
					Me._UpToICProvinceByProvinceCode = value
					Me.SetPreSave("UpToICProvinceByProvinceCode", Me._UpToICProvinceByProvinceCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICAreaCollectionByCityCode"
					coll = Me.ICAreaCollectionByCityCode
					Exit Select
				Case "ICCompanyCollectionByCity"
					coll = Me.ICCompanyCollectionByCity
					Exit Select
				Case "ICOfficeCollectionByCityID"
					coll = Me.ICOfficeCollectionByCityID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICAreaCollectionByCityCode", GetType(ICAreaCollection), New ICArea()))
			props.Add(new esPropertyDescriptor(Me, "ICCompanyCollectionByCity", GetType(ICCompanyCollection), New ICCompany()))
			props.Add(new esPropertyDescriptor(Me, "ICOfficeCollectionByCityID", GetType(ICOfficeCollection), New ICOffice()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICProvinceByProvinceCode Is Nothing Then
				Me.ProvinceCode = Me._UpToICProvinceByProvinceCode.ProvinceCode
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICAreaCollectionByCityCode Is Nothing Then
				Apply(Me._ICAreaCollectionByCityCode, "CityCode", Me.CityCode)
			End If
			
			If Not Me._ICCompanyCollectionByCity Is Nothing Then
				Apply(Me._ICCompanyCollectionByCity, "City", Me.CityCode)
			End If
			
			If Not Me._ICOfficeCollectionByCityID Is Nothing Then
				Apply(Me._ICOfficeCollectionByCityID, "CityID", Me.CityCode)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCityMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCityMetadata.ColumnNames.CityCode, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCityMetadata.PropertyNames.CityCode
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.CityName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCityMetadata.PropertyNames.CityName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.ProvinceCode, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCityMetadata.PropertyNames.ProvinceCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.IsActive, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCityMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.CreateBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCityMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.CreateDate, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCityMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.ApprovedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCityMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.IsApproved, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCityMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.ApprovedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCityMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCityMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICCityMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCityMetadata.ColumnNames.DisplayCode, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCityMetadata.PropertyNames.DisplayCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCityMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const CityCode As String = "CityCode"
			 Public Const CityName As String = "CityName"
			 Public Const ProvinceCode As String = "ProvinceCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const CityCode As String = "CityCode"
			 Public Const CityName As String = "CityName"
			 Public Const ProvinceCode As String = "ProvinceCode"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const DisplayCode As String = "DisplayCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCityMetadata)
			
				If ICCityMetadata.mapDelegates Is Nothing Then
					ICCityMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCityMetadata._meta Is Nothing Then
					ICCityMetadata._meta = New ICCityMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("CityCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CityName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProvinceCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("DisplayCode", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_City"
				meta.Destination = "IC_City"
				
				meta.spInsert = "proc_IC_CityInsert"
				meta.spUpdate = "proc_IC_CityUpdate"
				meta.spDelete = "proc_IC_CityDelete"
				meta.spLoadAll = "proc_IC_CityLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CityLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCityMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
