
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_MISReportUsers' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICMISReportUsers))> _
	<XmlType("ICMISReportUsers")> _	
	Partial Public Class ICMISReportUsers 
		Inherits esICMISReportUsers
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICMISReportUsers()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal reportUserID As System.Int32)
			Dim obj As New ICMISReportUsers()
			obj.ReportUserID = reportUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal reportUserID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICMISReportUsers()
			obj.ReportUserID = reportUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICMISReportUsersCollection")> _
	Partial Public Class ICMISReportUsersCollection
		Inherits esICMISReportUsersCollection
		Implements IEnumerable(Of ICMISReportUsers)
	
		Public Function FindByPrimaryKey(ByVal reportUserID As System.Int32) As ICMISReportUsers
			Return MyBase.SingleOrDefault(Function(e) e.ReportUserID.HasValue AndAlso e.ReportUserID.Value = reportUserID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICMISReportUsers))> _
		Public Class ICMISReportUsersCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICMISReportUsersCollection)
			
			Public Shared Widening Operator CType(packet As ICMISReportUsersCollectionWCFPacket) As ICMISReportUsersCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICMISReportUsersCollection) As ICMISReportUsersCollectionWCFPacket
				Return New ICMISReportUsersCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICMISReportUsersQuery 
		Inherits esICMISReportUsersQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICMISReportUsersQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICMISReportUsersQuery) As String
			Return ICMISReportUsersQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICMISReportUsersQuery
			Return DirectCast(ICMISReportUsersQuery.SerializeHelper.FromXml(query, GetType(ICMISReportUsersQuery)), ICMISReportUsersQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICMISReportUsers
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal reportUserID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(reportUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(reportUserID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal reportUserID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(reportUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(reportUserID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal reportUserID As System.Int32) As Boolean
		
			Dim query As New ICMISReportUsersQuery()
			query.Where(query.ReportUserID = reportUserID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal reportUserID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ReportUserID", reportUserID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_MISReportUsers.ReportUserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportUserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ReportUserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ReportUserID, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ReportUserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.ReportID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ReportID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ReportID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ReportID, value) Then
					Me._UpToICMISReportsByReportID = Nothing
					Me.OnPropertyChanged("UpToICMISReportsByReportID")
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ReportID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICMISReportUsersMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICMISReportUsersMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMISReportUsersMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMISReportUsersMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MISReportUsers.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMISReportUsersMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICMISReportsByReportID As ICMISReports
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ReportUserID"
							Me.str().ReportUserID = CType(value, string)
												
						Case "ReportID"
							Me.str().ReportID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ReportUserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReportUserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ReportUserID)
							End If
						
						Case "ReportID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ReportID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ReportID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.UserID)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMISReportUsersMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICMISReportUsers)
				Me.entity = entity
			End Sub				
		
	
			Public Property ReportUserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReportUserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportUserID = Nothing
					Else
						entity.ReportUserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ReportID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ReportID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ReportID = Nothing
					Else
						entity.ReportID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICMISReportUsers
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMISReportUsersMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICMISReportUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMISReportUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICMISReportUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICMISReportUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICMISReportUsersQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICMISReportUsersCollection
		Inherits esEntityCollection(Of ICMISReportUsers)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMISReportUsersMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICMISReportUsersCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICMISReportUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMISReportUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICMISReportUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICMISReportUsersQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICMISReportUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICMISReportUsersQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICMISReportUsersQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICMISReportUsersQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICMISReportUsersMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ReportUserID" 
					Return Me.ReportUserID
				Case "ReportID" 
					Return Me.ReportID
				Case "UserID" 
					Return Me.UserID
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ReportUserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.ReportUserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ReportID As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.ReportID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMISReportUsersMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICMISReportUsers 
		Inherits esICMISReportUsers
		
	
		#Region "UpToICMISReportsByReportID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MISReportUsers_IC_MISReports
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICMISReportsByReportID As ICMISReports
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICMISReportsByReportID Is Nothing _
						 AndAlso Not ReportID.Equals(Nothing)  Then
						
					Me._UpToICMISReportsByReportID = New ICMISReports()
					Me._UpToICMISReportsByReportID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICMISReportsByReportID", Me._UpToICMISReportsByReportID)
					Me._UpToICMISReportsByReportID.Query.Where(Me._UpToICMISReportsByReportID.Query.ReportID = Me.ReportID)
					Me._UpToICMISReportsByReportID.Query.Load()
				End If

				Return Me._UpToICMISReportsByReportID
			End Get
			
            Set(ByVal value As ICMISReports)
				Me.RemovePreSave("UpToICMISReportsByReportID")
				

				If value Is Nothing Then
				
					Me.ReportID = Nothing
				
					Me._UpToICMISReportsByReportID = Nothing
				Else
				
					Me.ReportID = value.ReportID
					
					Me._UpToICMISReportsByReportID = value
					Me.SetPreSave("UpToICMISReportsByReportID", Me._UpToICMISReportsByReportID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MISReportUsers_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MISReportUsers_IC_User1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICMISReportsByReportID Is Nothing Then
				Me.ReportID = Me._UpToICMISReportsByReportID.ReportID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICMISReportUsersMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.ReportUserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.ReportUserID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.ReportID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.ReportID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.UserID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.CreatedBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.CreatedDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.IsApproved, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.ApprovedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.ApprovedOn, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.Creater, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMISReportUsersMetadata.ColumnNames.CreationDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMISReportUsersMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICMISReportUsersMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ReportUserID As String = "ReportUserID"
			 Public Const ReportID As String = "ReportID"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ReportUserID As String = "ReportUserID"
			 Public Const ReportID As String = "ReportID"
			 Public Const UserID As String = "UserID"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICMISReportUsersMetadata)
			
				If ICMISReportUsersMetadata.mapDelegates Is Nothing Then
					ICMISReportUsersMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICMISReportUsersMetadata._meta Is Nothing Then
					ICMISReportUsersMetadata._meta = New ICMISReportUsersMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ReportUserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ReportID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_MISReportUsers"
				meta.Destination = "IC_MISReportUsers"
				
				meta.spInsert = "proc_IC_MISReportUsersInsert"
				meta.spUpdate = "proc_IC_MISReportUsersUpdate"
				meta.spDelete = "proc_IC_MISReportUsersDelete"
				meta.spLoadAll = "proc_IC_MISReportUsersLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_MISReportUsersLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICMISReportUsersMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
