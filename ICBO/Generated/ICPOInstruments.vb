
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_POInstruments' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPOInstruments))> _
	<XmlType("ICPOInstruments")> _	
	Partial Public Class ICPOInstruments 
		Inherits esICPOInstruments
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPOInstruments()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal pOInstrumentsID As System.Int32)
			Dim obj As New ICPOInstruments()
			obj.POInstrumentsID = pOInstrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal pOInstrumentsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPOInstruments()
			obj.POInstrumentsID = pOInstrumentsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPOInstrumentsCollection")> _
	Partial Public Class ICPOInstrumentsCollection
		Inherits esICPOInstrumentsCollection
		Implements IEnumerable(Of ICPOInstruments)
	
		Public Function FindByPrimaryKey(ByVal pOInstrumentsID As System.Int32) As ICPOInstruments
			Return MyBase.SingleOrDefault(Function(e) e.POInstrumentsID.HasValue AndAlso e.POInstrumentsID.Value = pOInstrumentsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPOInstruments))> _
		Public Class ICPOInstrumentsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPOInstrumentsCollection)
			
			Public Shared Widening Operator CType(packet As ICPOInstrumentsCollectionWCFPacket) As ICPOInstrumentsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPOInstrumentsCollection) As ICPOInstrumentsCollectionWCFPacket
				Return New ICPOInstrumentsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPOInstrumentsQuery 
		Inherits esICPOInstrumentsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPOInstrumentsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPOInstrumentsQuery) As String
			Return ICPOInstrumentsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPOInstrumentsQuery
			Return DirectCast(ICPOInstrumentsQuery.SerializeHelper.FromXml(query, GetType(ICPOInstrumentsQuery)), ICPOInstrumentsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPOInstruments
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal pOInstrumentsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(pOInstrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(pOInstrumentsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal pOInstrumentsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(pOInstrumentsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(pOInstrumentsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal pOInstrumentsID As System.Int32) As Boolean
		
			Dim query As New ICPOInstrumentsQuery()
			query.Where(query.POInstrumentsID = pOInstrumentsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal pOInstrumentsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("POInstrumentsID", pOInstrumentsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_POInstruments.POInstrumentsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property POInstrumentsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.POInstrumentsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.POInstrumentsID, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POInstrumentsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.POInstrumentNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property POInstrumentNumber As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.POInstrumentNumber)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.POInstrumentNumber, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POInstrumentNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.POMasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property POMasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.POMasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.POMasterSeriesID, value) Then
					Me._UpToICPOMasterSeriesByPOMasterSeriesID = Nothing
					Me.OnPropertyChanged("UpToICPOMasterSeriesByPOMasterSeriesID")
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POMasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.PreFix
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreFix As System.String
			Get
				Return MyBase.GetSystemString(ICPOInstrumentsMetadata.ColumnNames.PreFix)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPOInstrumentsMetadata.ColumnNames.PreFix, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.PreFix)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.IsUsed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsUsed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPOInstrumentsMetadata.ColumnNames.IsUsed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPOInstrumentsMetadata.ColumnNames.IsUsed, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.IsUsed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.UsedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UsedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.UsedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.UsedOn, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.UsedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.IsAssigned
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAssigned As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICPOInstrumentsMetadata.ColumnNames.IsAssigned)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICPOInstrumentsMetadata.ColumnNames.IsAssigned, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.IsAssigned)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPOInstrumentsMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.SubsetFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubsetFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.SubsetFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.SubsetFrom, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.SubsetFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.SubsetTo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubsetTo As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.SubsetTo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPOInstrumentsMetadata.ColumnNames.SubsetTo, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.SubsetTo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_POInstruments.OfficeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.OfficeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPOInstrumentsMetadata.ColumnNames.OfficeCode, value) Then
					OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.OfficeCode)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPOMasterSeriesByPOMasterSeriesID As ICPOMasterSeries
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "POInstrumentsID"
							Me.str().POInstrumentsID = CType(value, string)
												
						Case "POInstrumentNumber"
							Me.str().POInstrumentNumber = CType(value, string)
												
						Case "POMasterSeriesID"
							Me.str().POMasterSeriesID = CType(value, string)
												
						Case "PreFix"
							Me.str().PreFix = CType(value, string)
												
						Case "IsUsed"
							Me.str().IsUsed = CType(value, string)
												
						Case "UsedOn"
							Me.str().UsedOn = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "IsAssigned"
							Me.str().IsAssigned = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
												
						Case "SubsetFrom"
							Me.str().SubsetFrom = CType(value, string)
												
						Case "SubsetTo"
							Me.str().SubsetTo = CType(value, string)
												
						Case "OfficeCode"
							Me.str().OfficeCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "POInstrumentsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.POInstrumentsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POInstrumentsID)
							End If
						
						Case "POInstrumentNumber"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.POInstrumentNumber = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POInstrumentNumber)
							End If
						
						Case "POMasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.POMasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.POMasterSeriesID)
							End If
						
						Case "IsUsed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsUsed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.IsUsed)
							End If
						
						Case "UsedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.UsedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.UsedOn)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "IsAssigned"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAssigned = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.IsAssigned)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.CreationDate)
							End If
						
						Case "SubsetFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubsetFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.SubsetFrom)
							End If
						
						Case "SubsetTo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubsetTo = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.SubsetTo)
							End If
						
						Case "OfficeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPOInstrumentsMetadata.PropertyNames.OfficeCode)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPOInstruments)
				Me.entity = entity
			End Sub				
		
	
			Public Property POInstrumentsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.POInstrumentsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.POInstrumentsID = Nothing
					Else
						entity.POInstrumentsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property POInstrumentNumber As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.POInstrumentNumber
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.POInstrumentNumber = Nothing
					Else
						entity.POInstrumentNumber = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property POMasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.POMasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.POMasterSeriesID = Nothing
					Else
						entity.POMasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreFix As System.String 
				Get
					Dim data_ As System.String = entity.PreFix
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreFix = Nothing
					Else
						entity.PreFix = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsUsed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsUsed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsUsed = Nothing
					Else
						entity.IsUsed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property UsedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.UsedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UsedOn = Nothing
					Else
						entity.UsedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAssigned As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAssigned
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAssigned = Nothing
					Else
						entity.IsAssigned = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubsetFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubsetFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubsetFrom = Nothing
					Else
						entity.SubsetFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubsetTo As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubsetTo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubsetTo = Nothing
					Else
						entity.SubsetTo = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeCode = Nothing
					Else
						entity.OfficeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPOInstruments
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPOInstrumentsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPOInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPOInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPOInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPOInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPOInstrumentsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPOInstrumentsCollection
		Inherits esEntityCollection(Of ICPOInstruments)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPOInstrumentsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPOInstrumentsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPOInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPOInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPOInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPOInstrumentsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPOInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPOInstrumentsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPOInstrumentsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPOInstrumentsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPOInstrumentsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "POInstrumentsID" 
					Return Me.POInstrumentsID
				Case "POInstrumentNumber" 
					Return Me.POInstrumentNumber
				Case "POMasterSeriesID" 
					Return Me.POMasterSeriesID
				Case "PreFix" 
					Return Me.PreFix
				Case "IsUsed" 
					Return Me.IsUsed
				Case "UsedOn" 
					Return Me.UsedOn
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "IsAssigned" 
					Return Me.IsAssigned
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
				Case "SubsetFrom" 
					Return Me.SubsetFrom
				Case "SubsetTo" 
					Return Me.SubsetTo
				Case "OfficeCode" 
					Return Me.OfficeCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property POInstrumentsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.POInstrumentsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property POInstrumentNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.POInstrumentNumber, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property POMasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.POMasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PreFix As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.PreFix, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsUsed As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.IsUsed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property UsedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.UsedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property IsAssigned As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.IsAssigned, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SubsetFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.SubsetFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property SubsetTo As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.SubsetTo, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPOInstrumentsMetadata.ColumnNames.OfficeCode, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPOInstruments 
		Inherits esICPOInstruments
		
	
		#Region "UpToICPOMasterSeriesByPOMasterSeriesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_POInstruments_IC_POMasterSeries
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPOMasterSeriesByPOMasterSeriesID As ICPOMasterSeries
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPOMasterSeriesByPOMasterSeriesID Is Nothing _
						 AndAlso Not POMasterSeriesID.Equals(Nothing)  Then
						
					Me._UpToICPOMasterSeriesByPOMasterSeriesID = New ICPOMasterSeries()
					Me._UpToICPOMasterSeriesByPOMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPOMasterSeriesByPOMasterSeriesID", Me._UpToICPOMasterSeriesByPOMasterSeriesID)
					Me._UpToICPOMasterSeriesByPOMasterSeriesID.Query.Where(Me._UpToICPOMasterSeriesByPOMasterSeriesID.Query.POMasterSeriesID = Me.POMasterSeriesID)
					Me._UpToICPOMasterSeriesByPOMasterSeriesID.Query.Load()
				End If

				Return Me._UpToICPOMasterSeriesByPOMasterSeriesID
			End Get
			
            Set(ByVal value As ICPOMasterSeries)
				Me.RemovePreSave("UpToICPOMasterSeriesByPOMasterSeriesID")
				

				If value Is Nothing Then
				
					Me.POMasterSeriesID = Nothing
				
					Me._UpToICPOMasterSeriesByPOMasterSeriesID = Nothing
				Else
				
					Me.POMasterSeriesID = value.POMasterSeriesID
					
					Me._UpToICPOMasterSeriesByPOMasterSeriesID = value
					Me.SetPreSave("UpToICPOMasterSeriesByPOMasterSeriesID", Me._UpToICPOMasterSeriesByPOMasterSeriesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_POInstruments_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICPOMasterSeriesByPOMasterSeriesID Is Nothing Then
				Me.POMasterSeriesID = Me._UpToICPOMasterSeriesByPOMasterSeriesID.POMasterSeriesID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPOInstrumentsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.POInstrumentsID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.POInstrumentsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.POInstrumentNumber, 1, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.POInstrumentNumber
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.POMasterSeriesID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.POMasterSeriesID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.PreFix, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.PreFix
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.IsUsed, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.IsUsed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.UsedOn, 5, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.UsedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.CreatedBy, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.CreatedDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.IsAssigned, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.IsAssigned
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.SubsetFrom, 11, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.SubsetFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.SubsetTo, 12, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.SubsetTo
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPOInstrumentsMetadata.ColumnNames.OfficeCode, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPOInstrumentsMetadata.PropertyNames.OfficeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPOInstrumentsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const POInstrumentsID As String = "POInstrumentsID"
			 Public Const POInstrumentNumber As String = "POInstrumentNumber"
			 Public Const POMasterSeriesID As String = "POMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const IsUsed As String = "IsUsed"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const SubsetFrom As String = "SubsetFrom"
			 Public Const SubsetTo As String = "SubsetTo"
			 Public Const OfficeCode As String = "OfficeCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const POInstrumentsID As String = "POInstrumentsID"
			 Public Const POInstrumentNumber As String = "POInstrumentNumber"
			 Public Const POMasterSeriesID As String = "POMasterSeriesID"
			 Public Const PreFix As String = "PreFix"
			 Public Const IsUsed As String = "IsUsed"
			 Public Const UsedOn As String = "UsedOn"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const IsAssigned As String = "IsAssigned"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
			 Public Const SubsetFrom As String = "SubsetFrom"
			 Public Const SubsetTo As String = "SubsetTo"
			 Public Const OfficeCode As String = "OfficeCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPOInstrumentsMetadata)
			
				If ICPOInstrumentsMetadata.mapDelegates Is Nothing Then
					ICPOInstrumentsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPOInstrumentsMetadata._meta Is Nothing Then
					ICPOInstrumentsMetadata._meta = New ICPOInstrumentsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("POInstrumentsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("POInstrumentNumber", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("POMasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PreFix", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsUsed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("UsedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("IsAssigned", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SubsetFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("SubsetTo", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("OfficeCode", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_POInstruments"
				meta.Destination = "IC_POInstruments"
				
				meta.spInsert = "proc_IC_POInstrumentsInsert"
				meta.spUpdate = "proc_IC_POInstrumentsUpdate"
				meta.spDelete = "proc_IC_POInstrumentsDelete"
				meta.spLoadAll = "proc_IC_POInstrumentsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_POInstrumentsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPOInstrumentsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
