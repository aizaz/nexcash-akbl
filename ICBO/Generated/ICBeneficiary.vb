
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Beneficiary' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICBeneficiary))> _
	<XmlType("ICBeneficiary")> _	
	Partial Public Class ICBeneficiary 
		Inherits esICBeneficiary
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICBeneficiary()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal beneID As System.Int32)
			Dim obj As New ICBeneficiary()
			obj.BeneID = beneID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal beneID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICBeneficiary()
			obj.BeneID = beneID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICBeneficiaryCollection")> _
	Partial Public Class ICBeneficiaryCollection
		Inherits esICBeneficiaryCollection
		Implements IEnumerable(Of ICBeneficiary)
	
		Public Function FindByPrimaryKey(ByVal beneID As System.Int32) As ICBeneficiary
			Return MyBase.SingleOrDefault(Function(e) e.BeneID.HasValue AndAlso e.BeneID.Value = beneID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICBeneficiary))> _
		Public Class ICBeneficiaryCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICBeneficiaryCollection)
			
			Public Shared Widening Operator CType(packet As ICBeneficiaryCollectionWCFPacket) As ICBeneficiaryCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICBeneficiaryCollection) As ICBeneficiaryCollectionWCFPacket
				Return New ICBeneficiaryCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICBeneficiaryQuery 
		Inherits esICBeneficiaryQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICBeneficiaryQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICBeneficiaryQuery) As String
			Return ICBeneficiaryQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICBeneficiaryQuery
			Return DirectCast(ICBeneficiaryQuery.SerializeHelper.FromXml(query, GetType(ICBeneficiaryQuery)), ICBeneficiaryQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICBeneficiary
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal beneID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal beneID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(beneID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(beneID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal beneID As System.Int32) As Boolean
		
			Dim query As New ICBeneficiaryQuery()
			query.Where(query.BeneID = beneID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal beneID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("BeneID", beneID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Beneficiary.BeneID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneficiaryMetadata.ColumnNames.BeneID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneficiaryMetadata.ColumnNames.BeneID, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneIDNO
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneIDNO As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneIDNO)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneIDNO, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneIDNO)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BankName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BankName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BankName, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BankName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAccountNumber, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.Description
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Description As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.Description)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.Description, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.Description)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneCellNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneCellNumber As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneCellNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneCellNumber, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneCellNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneName As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneName, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneEmail
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneEmail As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneEmail)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneEmail, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneEmail)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneAddress1
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAddress1 As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress1)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress1, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneAddress1)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneAddress2
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAddress2 As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress2)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress2, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneAddress2)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneAddress3
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneAddress3 As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress3)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneAddress3, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneAddress3)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneProvince
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneProvince As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneProvince)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneProvince, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneProvince)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BenePostalCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BenePostalCode As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BenePostalCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BenePostalCode, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BenePostalCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.BeneCity
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BeneCity As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneCity)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.BeneCity, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneCity)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.Test
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Test As System.String
			Get
				Return MyBase.GetSystemString(ICBeneficiaryMetadata.ColumnNames.Test)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICBeneficiaryMetadata.ColumnNames.Test, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.Test)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICBeneficiaryMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICBeneficiaryMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneficiaryMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneficiaryMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneficiaryMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneficiaryMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICBeneficiaryMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICBeneficiaryMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Beneficiary.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICBeneficiaryMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICBeneficiaryMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "BeneID"
							Me.str().BeneID = CType(value, string)
												
						Case "BeneIDNO"
							Me.str().BeneIDNO = CType(value, string)
												
						Case "BankName"
							Me.str().BankName = CType(value, string)
												
						Case "BeneAccountNumber"
							Me.str().BeneAccountNumber = CType(value, string)
												
						Case "Description"
							Me.str().Description = CType(value, string)
												
						Case "BeneCellNumber"
							Me.str().BeneCellNumber = CType(value, string)
												
						Case "BeneName"
							Me.str().BeneName = CType(value, string)
												
						Case "BeneEmail"
							Me.str().BeneEmail = CType(value, string)
												
						Case "BeneAddress1"
							Me.str().BeneAddress1 = CType(value, string)
												
						Case "BeneAddress2"
							Me.str().BeneAddress2 = CType(value, string)
												
						Case "BeneAddress3"
							Me.str().BeneAddress3 = CType(value, string)
												
						Case "BeneProvince"
							Me.str().BeneProvince = CType(value, string)
												
						Case "BenePostalCode"
							Me.str().BenePostalCode = CType(value, string)
												
						Case "BeneCity"
							Me.str().BeneCity = CType(value, string)
												
						Case "Test"
							Me.str().Test = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "BeneID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BeneID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.BeneID)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreateDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICBeneficiaryMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICBeneficiary)
				Me.entity = entity
			End Sub				
		
	
			Public Property BeneID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BeneID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneID = Nothing
					Else
						entity.BeneID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneIDNO As System.String 
				Get
					Dim data_ As System.String = entity.BeneIDNO
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneIDNO = Nothing
					Else
						entity.BeneIDNO = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankName As System.String 
				Get
					Dim data_ As System.String = entity.BankName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankName = Nothing
					Else
						entity.BankName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.BeneAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAccountNumber = Nothing
					Else
						entity.BeneAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Description As System.String 
				Get
					Dim data_ As System.String = entity.Description
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Description = Nothing
					Else
						entity.Description = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneCellNumber As System.String 
				Get
					Dim data_ As System.String = entity.BeneCellNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneCellNumber = Nothing
					Else
						entity.BeneCellNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneName As System.String 
				Get
					Dim data_ As System.String = entity.BeneName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneName = Nothing
					Else
						entity.BeneName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneEmail As System.String 
				Get
					Dim data_ As System.String = entity.BeneEmail
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneEmail = Nothing
					Else
						entity.BeneEmail = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAddress1 As System.String 
				Get
					Dim data_ As System.String = entity.BeneAddress1
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAddress1 = Nothing
					Else
						entity.BeneAddress1 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAddress2 As System.String 
				Get
					Dim data_ As System.String = entity.BeneAddress2
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAddress2 = Nothing
					Else
						entity.BeneAddress2 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneAddress3 As System.String 
				Get
					Dim data_ As System.String = entity.BeneAddress3
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneAddress3 = Nothing
					Else
						entity.BeneAddress3 = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneProvince As System.String 
				Get
					Dim data_ As System.String = entity.BeneProvince
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneProvince = Nothing
					Else
						entity.BeneProvince = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BenePostalCode As System.String 
				Get
					Dim data_ As System.String = entity.BenePostalCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BenePostalCode = Nothing
					Else
						entity.BenePostalCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BeneCity As System.String 
				Get
					Dim data_ As System.String = entity.BeneCity
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BeneCity = Nothing
					Else
						entity.BeneCity = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Test As System.String 
				Get
					Dim data_ As System.String = entity.Test
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Test = Nothing
					Else
						entity.Test = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICBeneficiary
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneficiaryMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICBeneficiaryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneficiaryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICBeneficiaryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICBeneficiaryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICBeneficiaryQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICBeneficiaryCollection
		Inherits esEntityCollection(Of ICBeneficiary)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICBeneficiaryMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICBeneficiaryCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICBeneficiaryQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICBeneficiaryQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICBeneficiaryQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICBeneficiaryQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICBeneficiaryQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICBeneficiaryQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICBeneficiaryQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICBeneficiaryQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICBeneficiaryMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "BeneID" 
					Return Me.BeneID
				Case "BeneIDNO" 
					Return Me.BeneIDNO
				Case "BankName" 
					Return Me.BankName
				Case "BeneAccountNumber" 
					Return Me.BeneAccountNumber
				Case "Description" 
					Return Me.Description
				Case "BeneCellNumber" 
					Return Me.BeneCellNumber
				Case "BeneName" 
					Return Me.BeneName
				Case "BeneEmail" 
					Return Me.BeneEmail
				Case "BeneAddress1" 
					Return Me.BeneAddress1
				Case "BeneAddress2" 
					Return Me.BeneAddress2
				Case "BeneAddress3" 
					Return Me.BeneAddress3
				Case "BeneProvince" 
					Return Me.BeneProvince
				Case "BenePostalCode" 
					Return Me.BenePostalCode
				Case "BeneCity" 
					Return Me.BeneCity
				Case "Test" 
					Return Me.Test
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property BeneID As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property BeneIDNO As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneIDNO, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BankName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Description As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.Description, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneCellNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneCellNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneName As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneEmail As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneEmail, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAddress1 As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneAddress1, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAddress2 As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneAddress2, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneAddress3 As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneAddress3, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneProvince As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneProvince, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BenePostalCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BenePostalCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BeneCity As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.BeneCity, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Test As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.Test, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICBeneficiaryMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICBeneficiary 
		Inherits esICBeneficiary
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICBeneficiaryMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneIDNO, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneIDNO
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BankName, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BankName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneAccountNumber, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.Description, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.Description
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneCellNumber, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneCellNumber
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneName, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneName
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneEmail, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneEmail
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneAddress1, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneAddress1
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneAddress2, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneAddress2
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneAddress3, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneAddress3
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneProvince, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneProvince
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BenePostalCode, 12, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BenePostalCode
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.BeneCity, 13, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.BeneCity
			c.CharacterMaxLength = 150
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.Test, 14, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.Test
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.IsActive, 15, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((1))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.CreateBy, 16, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.CreateDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.Creater, 18, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICBeneficiaryMetadata.ColumnNames.CreationDate, 19, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICBeneficiaryMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICBeneficiaryMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneIDNO As String = "BeneIDNO"
			 Public Const BankName As String = "BankName"
			 Public Const BeneAccountNumber As String = "BeneAccountNumber"
			 Public Const Description As String = "Description"
			 Public Const BeneCellNumber As String = "BeneCellNumber"
			 Public Const BeneName As String = "BeneName"
			 Public Const BeneEmail As String = "BeneEmail"
			 Public Const BeneAddress1 As String = "BeneAddress1"
			 Public Const BeneAddress2 As String = "BeneAddress2"
			 Public Const BeneAddress3 As String = "BeneAddress3"
			 Public Const BeneProvince As String = "BeneProvince"
			 Public Const BenePostalCode As String = "BenePostalCode"
			 Public Const BeneCity As String = "BeneCity"
			 Public Const Test As String = "Test"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const BeneID As String = "BeneID"
			 Public Const BeneIDNO As String = "BeneIDNO"
			 Public Const BankName As String = "BankName"
			 Public Const BeneAccountNumber As String = "BeneAccountNumber"
			 Public Const Description As String = "Description"
			 Public Const BeneCellNumber As String = "BeneCellNumber"
			 Public Const BeneName As String = "BeneName"
			 Public Const BeneEmail As String = "BeneEmail"
			 Public Const BeneAddress1 As String = "BeneAddress1"
			 Public Const BeneAddress2 As String = "BeneAddress2"
			 Public Const BeneAddress3 As String = "BeneAddress3"
			 Public Const BeneProvince As String = "BeneProvince"
			 Public Const BenePostalCode As String = "BenePostalCode"
			 Public Const BeneCity As String = "BeneCity"
			 Public Const Test As String = "Test"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICBeneficiaryMetadata)
			
				If ICBeneficiaryMetadata.mapDelegates Is Nothing Then
					ICBeneficiaryMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICBeneficiaryMetadata._meta Is Nothing Then
					ICBeneficiaryMetadata._meta = New ICBeneficiaryMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("BeneID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("BeneIDNO", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Description", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneCellNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneEmail", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAddress1", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAddress2", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneAddress3", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneProvince", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BenePostalCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BeneCity", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Test", new esTypeMap("text", "System.String"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Beneficiary"
				meta.Destination = "IC_Beneficiary"
				
				meta.spInsert = "proc_IC_BeneficiaryInsert"
				meta.spUpdate = "proc_IC_BeneficiaryUpdate"
				meta.spDelete = "proc_IC_BeneficiaryDelete"
				meta.spLoadAll = "proc_IC_BeneficiaryLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_BeneficiaryLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICBeneficiaryMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
