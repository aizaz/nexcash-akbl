
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:51 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'BN_PD_SC_SCHEME' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(BnPdScScheme))> _
	<XmlType("BnPdScScheme")> _	
	Partial Public Class BnPdScScheme 
		Inherits esBnPdScScheme
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New BnPdScScheme()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String)
			Dim obj As New BnPdScScheme()
			obj.DmpProdcode = dmpProdcode
			obj.DcsSchemcode = dcsSchemcode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New BnPdScScheme()
			obj.DmpProdcode = dmpProdcode
			obj.DcsSchemcode = dcsSchemcode
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("BnPdScSchemeCollection")> _
	Partial Public Class BnPdScSchemeCollection
		Inherits esBnPdScSchemeCollection
		Implements IEnumerable(Of BnPdScScheme)
	
		Public Function FindByPrimaryKey(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String) As BnPdScScheme
			Return MyBase.SingleOrDefault(Function(e) e.DmpProdcode = dmpProdcode And e.DcsSchemcode = dcsSchemcode)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(BnPdScScheme))> _
		Public Class BnPdScSchemeCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of BnPdScSchemeCollection)
			
			Public Shared Widening Operator CType(packet As BnPdScSchemeCollectionWCFPacket) As BnPdScSchemeCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As BnPdScSchemeCollection) As BnPdScSchemeCollectionWCFPacket
				Return New BnPdScSchemeCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class BnPdScSchemeQuery 
		Inherits esBnPdScSchemeQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "BnPdScSchemeQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As BnPdScSchemeQuery) As String
			Return BnPdScSchemeQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As BnPdScSchemeQuery
			Return DirectCast(BnPdScSchemeQuery.SerializeHelper.FromXml(query, GetType(BnPdScSchemeQuery)), BnPdScSchemeQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esBnPdScScheme
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dmpProdcode, dcsSchemcode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dmpProdcode, dcsSchemcode)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dmpProdcode, dcsSchemcode)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dmpProdcode, dcsSchemcode)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String) As Boolean
		
			Dim query As New BnPdScSchemeQuery()
			query.Where(query.DmpProdcode = dmpProdcode And query.DcsSchemcode = dcsSchemcode)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal dmpProdcode As System.String, ByVal dcsSchemcode As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DmpProdcode", dmpProdcode)
						parms.Add("DcsSchemcode", dcsSchemcode)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to BN_PD_SC_SCHEME.DMP_PRODCODE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DmpProdcode As System.String
			Get
				Return MyBase.GetSystemString(BnPdScSchemeMetadata.ColumnNames.DmpProdcode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(BnPdScSchemeMetadata.ColumnNames.DmpProdcode, value) Then
					OnPropertyChanged(BnPdScSchemeMetadata.PropertyNames.DmpProdcode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to BN_PD_SC_SCHEME.DCS_SCHEMCODE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DcsSchemcode As System.String
			Get
				Return MyBase.GetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsSchemcode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsSchemcode, value) Then
					OnPropertyChanged(BnPdScSchemeMetadata.PropertyNames.DcsSchemcode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to BN_PD_SC_SCHEME.DCS_SCHEFULLNAME
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DcsSchefullname As System.String
			Get
				Return MyBase.GetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsSchefullname)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsSchefullname, value) Then
					OnPropertyChanged(BnPdScSchemeMetadata.PropertyNames.DcsSchefullname)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to BN_PD_SC_SCHEME.DCS_SCHESHORTNAME
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DcsScheshortname As System.String
			Get
				Return MyBase.GetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsScheshortname)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(BnPdScSchemeMetadata.ColumnNames.DcsScheshortname, value) Then
					OnPropertyChanged(BnPdScSchemeMetadata.PropertyNames.DcsScheshortname)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to BN_PD_SC_SCHEME.PCA_GLACCODE
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PcaGlaccode As System.String
			Get
				Return MyBase.GetSystemString(BnPdScSchemeMetadata.ColumnNames.PcaGlaccode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(BnPdScSchemeMetadata.ColumnNames.PcaGlaccode, value) Then
					OnPropertyChanged(BnPdScSchemeMetadata.PropertyNames.PcaGlaccode)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DmpProdcode"
							Me.str().DmpProdcode = CType(value, string)
												
						Case "DcsSchemcode"
							Me.str().DcsSchemcode = CType(value, string)
												
						Case "DcsSchefullname"
							Me.str().DcsSchefullname = CType(value, string)
												
						Case "DcsScheshortname"
							Me.str().DcsScheshortname = CType(value, string)
												
						Case "PcaGlaccode"
							Me.str().PcaGlaccode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esBnPdScScheme)
				Me.entity = entity
			End Sub				
		
	
			Public Property DmpProdcode As System.String 
				Get
					Dim data_ As System.String = entity.DmpProdcode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DmpProdcode = Nothing
					Else
						entity.DmpProdcode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DcsSchemcode As System.String 
				Get
					Dim data_ As System.String = entity.DcsSchemcode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DcsSchemcode = Nothing
					Else
						entity.DcsSchemcode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DcsSchefullname As System.String 
				Get
					Dim data_ As System.String = entity.DcsSchefullname
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DcsSchefullname = Nothing
					Else
						entity.DcsSchefullname = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property DcsScheshortname As System.String 
				Get
					Dim data_ As System.String = entity.DcsScheshortname
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DcsScheshortname = Nothing
					Else
						entity.DcsScheshortname = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PcaGlaccode As System.String 
				Get
					Dim data_ As System.String = entity.PcaGlaccode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PcaGlaccode = Nothing
					Else
						entity.PcaGlaccode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esBnPdScScheme
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return BnPdScSchemeMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As BnPdScSchemeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New BnPdScSchemeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As BnPdScSchemeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As BnPdScSchemeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As BnPdScSchemeQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esBnPdScSchemeCollection
		Inherits esEntityCollection(Of BnPdScScheme)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return BnPdScSchemeMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "BnPdScSchemeCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As BnPdScSchemeQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New BnPdScSchemeQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As BnPdScSchemeQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New BnPdScSchemeQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As BnPdScSchemeQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, BnPdScSchemeQuery))
		End Sub
		
		#End Region
						
		Private m_query As BnPdScSchemeQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esBnPdScSchemeQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return BnPdScSchemeMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DmpProdcode" 
					Return Me.DmpProdcode
				Case "DcsSchemcode" 
					Return Me.DcsSchemcode
				Case "DcsSchefullname" 
					Return Me.DcsSchefullname
				Case "DcsScheshortname" 
					Return Me.DcsScheshortname
				Case "PcaGlaccode" 
					Return Me.PcaGlaccode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DmpProdcode As esQueryItem
			Get
				Return New esQueryItem(Me, BnPdScSchemeMetadata.ColumnNames.DmpProdcode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DcsSchemcode As esQueryItem
			Get
				Return New esQueryItem(Me, BnPdScSchemeMetadata.ColumnNames.DcsSchemcode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DcsSchefullname As esQueryItem
			Get
				Return New esQueryItem(Me, BnPdScSchemeMetadata.ColumnNames.DcsSchefullname, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property DcsScheshortname As esQueryItem
			Get
				Return New esQueryItem(Me, BnPdScSchemeMetadata.ColumnNames.DcsScheshortname, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PcaGlaccode As esQueryItem
			Get
				Return New esQueryItem(Me, BnPdScSchemeMetadata.ColumnNames.PcaGlaccode, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class BnPdScScheme 
		Inherits esBnPdScScheme
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class BnPdScSchemeMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(BnPdScSchemeMetadata.ColumnNames.DmpProdcode, 0, GetType(System.String), esSystemType.String)	
			c.PropertyName = BnPdScSchemeMetadata.PropertyNames.DmpProdcode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(BnPdScSchemeMetadata.ColumnNames.DcsSchemcode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = BnPdScSchemeMetadata.PropertyNames.DcsSchemcode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(BnPdScSchemeMetadata.ColumnNames.DcsSchefullname, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = BnPdScSchemeMetadata.PropertyNames.DcsSchefullname
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(BnPdScSchemeMetadata.ColumnNames.DcsScheshortname, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = BnPdScSchemeMetadata.PropertyNames.DcsScheshortname
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(BnPdScSchemeMetadata.ColumnNames.PcaGlaccode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = BnPdScSchemeMetadata.PropertyNames.PcaGlaccode
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As BnPdScSchemeMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DmpProdcode As String = "DMP_PRODCODE"
			 Public Const DcsSchemcode As String = "DCS_SCHEMCODE"
			 Public Const DcsSchefullname As String = "DCS_SCHEFULLNAME"
			 Public Const DcsScheshortname As String = "DCS_SCHESHORTNAME"
			 Public Const PcaGlaccode As String = "PCA_GLACCODE"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DmpProdcode As String = "DmpProdcode"
			 Public Const DcsSchemcode As String = "DcsSchemcode"
			 Public Const DcsSchefullname As String = "DcsSchefullname"
			 Public Const DcsScheshortname As String = "DcsScheshortname"
			 Public Const PcaGlaccode As String = "PcaGlaccode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(BnPdScSchemeMetadata)
			
				If BnPdScSchemeMetadata.mapDelegates Is Nothing Then
					BnPdScSchemeMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If BnPdScSchemeMetadata._meta Is Nothing Then
					BnPdScSchemeMetadata._meta = New BnPdScSchemeMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DmpProdcode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DcsSchemcode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DcsSchefullname", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("DcsScheshortname", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PcaGlaccode", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "BN_PD_SC_SCHEME"
				meta.Destination = "BN_PD_SC_SCHEME"
				
				meta.spInsert = "proc_BN_PD_SC_SCHEMEInsert"
				meta.spUpdate = "proc_BN_PD_SC_SCHEMEUpdate"
				meta.spDelete = "proc_BN_PD_SC_SCHEMEDelete"
				meta.spLoadAll = "proc_BN_PD_SC_SCHEMELoadAll"
				meta.spLoadByPrimaryKey = "proc_BN_PD_SC_SCHEMELoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As BnPdScSchemeMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
