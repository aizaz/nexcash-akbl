
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 20-Apr-2014 12:13:13 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_MT940Files' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICMT940Files))> _
	<XmlType("ICMT940Files")> _	
	Partial Public Class ICMT940Files 
		Inherits esICMT940Files
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICMT940Files()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal mT940FileID As System.Int32)
			Dim obj As New ICMT940Files()
			obj.MT940FileID = mT940FileID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal mT940FileID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICMT940Files()
			obj.MT940FileID = mT940FileID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICMT940FilesCollection")> _
	Partial Public Class ICMT940FilesCollection
		Inherits esICMT940FilesCollection
		Implements IEnumerable(Of ICMT940Files)
	
		Public Function FindByPrimaryKey(ByVal mT940FileID As System.Int32) As ICMT940Files
			Return MyBase.SingleOrDefault(Function(e) e.MT940FileID.HasValue AndAlso e.MT940FileID.Value = mT940FileID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICMT940Files))> _
		Public Class ICMT940FilesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICMT940FilesCollection)
			
			Public Shared Widening Operator CType(packet As ICMT940FilesCollectionWCFPacket) As ICMT940FilesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICMT940FilesCollection) As ICMT940FilesCollectionWCFPacket
				Return New ICMT940FilesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICMT940FilesQuery 
		Inherits esICMT940FilesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICMT940FilesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICMT940FilesQuery) As String
			Return ICMT940FilesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICMT940FilesQuery
			Return DirectCast(ICMT940FilesQuery.SerializeHelper.FromXml(query, GetType(ICMT940FilesQuery)), ICMT940FilesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICMT940Files
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal mT940FileID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(mT940FileID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(mT940FileID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal mT940FileID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(mT940FileID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(mT940FileID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal mT940FileID As System.Int32) As Boolean
		
			Dim query As New ICMT940FilesQuery()
			query.Where(query.MT940FileID = mT940FileID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal mT940FileID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("MT940FileID", mT940FileID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_MT940Files.MT940FileID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940FileID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMT940FilesMetadata.ColumnNames.MT940FileID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMT940FilesMetadata.ColumnNames.MT940FileID, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.MT940FileID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ClientAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountNo, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ClientAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ClientAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountBranchCode, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ClientAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ClientAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ClientAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.ClientAccountCurrency, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ClientAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.MT940FileData
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940FileData As System.Byte()
			Get
				Return MyBase.GetSystemByteArray(ICMT940FilesMetadata.ColumnNames.MT940FileData)
			End Get
			
			Set(ByVal value As System.Byte())
				If MyBase.SetSystemByteArray(ICMT940FilesMetadata.ColumnNames.MT940FileData, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.MT940FileData)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.MT940FileMIMEType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MT940FileMIMEType As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.MT940FileMIMEType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.MT940FileMIMEType, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.MT940FileMIMEType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.FileLocation
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileLocation As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.FileLocation)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.FileLocation, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.FileLocation)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.FileName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FileName As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.FileName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.FileName, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.FileName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.OriginalFileName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OriginalFileName As System.String
			Get
				Return MyBase.GetSystemString(ICMT940FilesMetadata.ColumnNames.OriginalFileName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICMT940FilesMetadata.ColumnNames.OriginalFileName, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.OriginalFileName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMT940FilesMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMT940FilesMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMT940FilesMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMT940FilesMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ExportDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExportDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportDate, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ExportedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExportedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMT940FilesMetadata.ColumnNames.ExportedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMT940FilesMetadata.ColumnNames.ExportedBy, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.NoOfTxns
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property NoOfTxns As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICMT940FilesMetadata.ColumnNames.NoOfTxns)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICMT940FilesMetadata.ColumnNames.NoOfTxns, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.NoOfTxns)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ExportPeriodFromDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExportPeriodFromDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportPeriodFromDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportPeriodFromDate, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportPeriodFromDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_MT940Files.ExportPeriodToDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ExportPeriodToDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportPeriodToDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICMT940FilesMetadata.ColumnNames.ExportPeriodToDate, value) Then
					OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportPeriodToDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "MT940FileID"
							Me.str().MT940FileID = CType(value, string)
												
						Case "ClientAccountNo"
							Me.str().ClientAccountNo = CType(value, string)
												
						Case "ClientAccountBranchCode"
							Me.str().ClientAccountBranchCode = CType(value, string)
												
						Case "ClientAccountCurrency"
							Me.str().ClientAccountCurrency = CType(value, string)
												
						Case "MT940FileMIMEType"
							Me.str().MT940FileMIMEType = CType(value, string)
												
						Case "FileLocation"
							Me.str().FileLocation = CType(value, string)
												
						Case "FileName"
							Me.str().FileName = CType(value, string)
												
						Case "OriginalFileName"
							Me.str().OriginalFileName = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "ExportDate"
							Me.str().ExportDate = CType(value, string)
												
						Case "ExportedBy"
							Me.str().ExportedBy = CType(value, string)
												
						Case "NoOfTxns"
							Me.str().NoOfTxns = CType(value, string)
												
						Case "ExportPeriodFromDate"
							Me.str().ExportPeriodFromDate = CType(value, string)
												
						Case "ExportPeriodToDate"
							Me.str().ExportPeriodToDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "MT940FileID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.MT940FileID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.MT940FileID)
							End If
						
						Case "MT940FileData"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Byte()" Then
								Me.MT940FileData = CType(value, System.Byte())
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.MT940FileData)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.CreateDate)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "ExportDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ExportDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportDate)
							End If
						
						Case "ExportedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ExportedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportedBy)
							End If
						
						Case "NoOfTxns"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.NoOfTxns = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.NoOfTxns)
							End If
						
						Case "ExportPeriodFromDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ExportPeriodFromDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportPeriodFromDate)
							End If
						
						Case "ExportPeriodToDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ExportPeriodToDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICMT940FilesMetadata.PropertyNames.ExportPeriodToDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICMT940Files)
				Me.entity = entity
			End Sub				
		
	
			Public Property MT940FileID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.MT940FileID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MT940FileID = Nothing
					Else
						entity.MT940FileID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountNo = Nothing
					Else
						entity.ClientAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountBranchCode = Nothing
					Else
						entity.ClientAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ClientAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ClientAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ClientAccountCurrency = Nothing
					Else
						entity.ClientAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MT940FileMIMEType As System.String 
				Get
					Dim data_ As System.String = entity.MT940FileMIMEType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MT940FileMIMEType = Nothing
					Else
						entity.MT940FileMIMEType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileLocation As System.String 
				Get
					Dim data_ As System.String = entity.FileLocation
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileLocation = Nothing
					Else
						entity.FileLocation = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FileName As System.String 
				Get
					Dim data_ As System.String = entity.FileName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FileName = Nothing
					Else
						entity.FileName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property OriginalFileName As System.String 
				Get
					Dim data_ As System.String = entity.OriginalFileName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OriginalFileName = Nothing
					Else
						entity.OriginalFileName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExportDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ExportDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExportDate = Nothing
					Else
						entity.ExportDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExportedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ExportedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExportedBy = Nothing
					Else
						entity.ExportedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property NoOfTxns As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.NoOfTxns
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.NoOfTxns = Nothing
					Else
						entity.NoOfTxns = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExportPeriodFromDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ExportPeriodFromDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExportPeriodFromDate = Nothing
					Else
						entity.ExportPeriodFromDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ExportPeriodToDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ExportPeriodToDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ExportPeriodToDate = Nothing
					Else
						entity.ExportPeriodToDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICMT940Files
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMT940FilesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICMT940FilesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMT940FilesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICMT940FilesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICMT940FilesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICMT940FilesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICMT940FilesCollection
		Inherits esEntityCollection(Of ICMT940Files)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICMT940FilesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICMT940FilesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICMT940FilesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICMT940FilesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICMT940FilesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICMT940FilesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICMT940FilesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICMT940FilesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICMT940FilesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICMT940FilesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICMT940FilesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "MT940FileID" 
					Return Me.MT940FileID
				Case "ClientAccountNo" 
					Return Me.ClientAccountNo
				Case "ClientAccountBranchCode" 
					Return Me.ClientAccountBranchCode
				Case "ClientAccountCurrency" 
					Return Me.ClientAccountCurrency
				Case "MT940FileData" 
					Return Me.MT940FileData
				Case "MT940FileMIMEType" 
					Return Me.MT940FileMIMEType
				Case "FileLocation" 
					Return Me.FileLocation
				Case "FileName" 
					Return Me.FileName
				Case "OriginalFileName" 
					Return Me.OriginalFileName
				Case "CreateDate" 
					Return Me.CreateDate
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "ExportDate" 
					Return Me.ExportDate
				Case "ExportedBy" 
					Return Me.ExportedBy
				Case "NoOfTxns" 
					Return Me.NoOfTxns
				Case "ExportPeriodFromDate" 
					Return Me.ExportPeriodFromDate
				Case "ExportPeriodToDate" 
					Return Me.ExportPeriodToDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property MT940FileID As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.MT940FileID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ClientAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ClientAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ClientAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ClientAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MT940FileData As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.MT940FileData, esSystemType.ByteArray)
			End Get
		End Property 
		
		Public ReadOnly Property MT940FileMIMEType As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.MT940FileMIMEType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileLocation As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.FileLocation, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FileName As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.FileName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property OriginalFileName As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.OriginalFileName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ExportDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ExportDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ExportedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ExportedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property NoOfTxns As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.NoOfTxns, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ExportPeriodFromDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ExportPeriodFromDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ExportPeriodToDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICMT940FilesMetadata.ColumnNames.ExportPeriodToDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICMT940Files 
		Inherits esICMT940Files
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICMT940FilesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.MT940FileID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.MT940FileID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ClientAccountNo, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ClientAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ClientAccountBranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ClientAccountBranchCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ClientAccountCurrency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ClientAccountCurrency
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.MT940FileData, 4, GetType(System.Byte()), esSystemType.ByteArray)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.MT940FileData
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.MT940FileMIMEType, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.MT940FileMIMEType
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.FileLocation, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.FileLocation
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.FileName, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.FileName
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.OriginalFileName, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.OriginalFileName
			c.CharacterMaxLength = 500
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.CreateDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.CompanyCode, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ExportDate, 11, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ExportDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ExportedBy, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ExportedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.NoOfTxns, 13, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.NoOfTxns
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ExportPeriodFromDate, 14, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ExportPeriodFromDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICMT940FilesMetadata.ColumnNames.ExportPeriodToDate, 15, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICMT940FilesMetadata.PropertyNames.ExportPeriodToDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICMT940FilesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const MT940FileID As String = "MT940FileID"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const MT940FileData As String = "MT940FileData"
			 Public Const MT940FileMIMEType As String = "MT940FileMIMEType"
			 Public Const FileLocation As String = "FileLocation"
			 Public Const FileName As String = "FileName"
			 Public Const OriginalFileName As String = "OriginalFileName"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ExportDate As String = "ExportDate"
			 Public Const ExportedBy As String = "ExportedBy"
			 Public Const NoOfTxns As String = "NoOfTxns"
			 Public Const ExportPeriodFromDate As String = "ExportPeriodFromDate"
			 Public Const ExportPeriodToDate As String = "ExportPeriodToDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const MT940FileID As String = "MT940FileID"
			 Public Const ClientAccountNo As String = "ClientAccountNo"
			 Public Const ClientAccountBranchCode As String = "ClientAccountBranchCode"
			 Public Const ClientAccountCurrency As String = "ClientAccountCurrency"
			 Public Const MT940FileData As String = "MT940FileData"
			 Public Const MT940FileMIMEType As String = "MT940FileMIMEType"
			 Public Const FileLocation As String = "FileLocation"
			 Public Const FileName As String = "FileName"
			 Public Const OriginalFileName As String = "OriginalFileName"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const ExportDate As String = "ExportDate"
			 Public Const ExportedBy As String = "ExportedBy"
			 Public Const NoOfTxns As String = "NoOfTxns"
			 Public Const ExportPeriodFromDate As String = "ExportPeriodFromDate"
			 Public Const ExportPeriodToDate As String = "ExportPeriodToDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICMT940FilesMetadata)
			
				If ICMT940FilesMetadata.mapDelegates Is Nothing Then
					ICMT940FilesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICMT940FilesMetadata._meta Is Nothing Then
					ICMT940FilesMetadata._meta = New ICMT940FilesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("MT940FileID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ClientAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ClientAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MT940FileData", new esTypeMap("varbinary", "System.Byte()"))
				meta.AddTypeMap("MT940FileMIMEType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileLocation", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FileName", new esTypeMap("nchar", "System.String"))
				meta.AddTypeMap("OriginalFileName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ExportDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ExportedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("NoOfTxns", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ExportPeriodFromDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ExportPeriodToDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_MT940Files"
				meta.Destination = "IC_MT940Files"
				
				meta.spInsert = "proc_IC_MT940FilesInsert"
				meta.spUpdate = "proc_IC_MT940FilesUpdate"
				meta.spDelete = "proc_IC_MT940FilesDelete"
				meta.spLoadAll = "proc_IC_MT940FilesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_MT940FilesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICMT940FilesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
