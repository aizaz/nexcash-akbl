
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 10/16/2014 11:29:58 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CollectionValueDateClearingInstruments' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCollectionValueDateClearingInstruments))> _
	<XmlType("ICCollectionValueDateClearingInstruments")> _	
	Partial Public Class ICCollectionValueDateClearingInstruments 
		Inherits esICCollectionValueDateClearingInstruments
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCollectionValueDateClearingInstruments()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal valueDateClearingInstrumentID As System.Int32)
			Dim obj As New ICCollectionValueDateClearingInstruments()
			obj.ValueDateClearingInstrumentID = valueDateClearingInstrumentID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal valueDateClearingInstrumentID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCollectionValueDateClearingInstruments()
			obj.ValueDateClearingInstrumentID = valueDateClearingInstrumentID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCollectionValueDateClearingInstrumentsCollection")> _
	Partial Public Class ICCollectionValueDateClearingInstrumentsCollection
		Inherits esICCollectionValueDateClearingInstrumentsCollection
		Implements IEnumerable(Of ICCollectionValueDateClearingInstruments)
	
		Public Function FindByPrimaryKey(ByVal valueDateClearingInstrumentID As System.Int32) As ICCollectionValueDateClearingInstruments
			Return MyBase.SingleOrDefault(Function(e) e.ValueDateClearingInstrumentID.HasValue AndAlso e.ValueDateClearingInstrumentID.Value = valueDateClearingInstrumentID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCollectionValueDateClearingInstruments))> _
		Public Class ICCollectionValueDateClearingInstrumentsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCollectionValueDateClearingInstrumentsCollection)
			
			Public Shared Widening Operator CType(packet As ICCollectionValueDateClearingInstrumentsCollectionWCFPacket) As ICCollectionValueDateClearingInstrumentsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCollectionValueDateClearingInstrumentsCollection) As ICCollectionValueDateClearingInstrumentsCollectionWCFPacket
				Return New ICCollectionValueDateClearingInstrumentsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCollectionValueDateClearingInstrumentsQuery 
		Inherits esICCollectionValueDateClearingInstrumentsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCollectionValueDateClearingInstrumentsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCollectionValueDateClearingInstrumentsQuery) As String
			Return ICCollectionValueDateClearingInstrumentsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCollectionValueDateClearingInstrumentsQuery
			Return DirectCast(ICCollectionValueDateClearingInstrumentsQuery.SerializeHelper.FromXml(query, GetType(ICCollectionValueDateClearingInstrumentsQuery)), ICCollectionValueDateClearingInstrumentsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCollectionValueDateClearingInstruments
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal valueDateClearingInstrumentID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(valueDateClearingInstrumentID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(valueDateClearingInstrumentID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal valueDateClearingInstrumentID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(valueDateClearingInstrumentID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(valueDateClearingInstrumentID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal valueDateClearingInstrumentID As System.Int32) As Boolean
		
			Dim query As New ICCollectionValueDateClearingInstrumentsQuery()
			query.Where(query.ValueDateClearingInstrumentID = valueDateClearingInstrumentID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal valueDateClearingInstrumentID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ValueDateClearingInstrumentID", valueDateClearingInstrumentID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingInstruments.ValueDateClearingInstrumentID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ValueDateClearingInstrumentID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.ValueDateClearingInstrumentID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.ValueDateClearingInstrumentID, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.ValueDateClearingInstrumentID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingInstruments.InstrumentNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstrumentNo As System.String
			Get
				Return MyBase.GetSystemString(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.InstrumentNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.InstrumentNo, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.InstrumentNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingInstruments.IsProcessed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsProcessed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.IsProcessed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.IsProcessed, value) Then
					OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.IsProcessed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingInstruments.HistoryID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HistoryID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.HistoryID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.HistoryID, value) Then
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID = Nothing
					Me.OnPropertyChanged("UpToICCollectionValueDateClearingHistoryByHistoryID")
					OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.HistoryID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CollectionValueDateClearingInstruments.CollectionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CollectionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.CollectionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.CollectionID, value) Then
					Me._UpToICCollectionsByCollectionID = Nothing
					Me.OnPropertyChanged("UpToICCollectionsByCollectionID")
					OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.CollectionID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionsByCollectionID As ICCollections
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCollectionValueDateClearingHistoryByHistoryID As ICCollectionValueDateClearingHistory
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ValueDateClearingInstrumentID"
							Me.str().ValueDateClearingInstrumentID = CType(value, string)
												
						Case "InstrumentNo"
							Me.str().InstrumentNo = CType(value, string)
												
						Case "IsProcessed"
							Me.str().IsProcessed = CType(value, string)
												
						Case "HistoryID"
							Me.str().HistoryID = CType(value, string)
												
						Case "CollectionID"
							Me.str().CollectionID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ValueDateClearingInstrumentID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ValueDateClearingInstrumentID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.ValueDateClearingInstrumentID)
							End If
						
						Case "IsProcessed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsProcessed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.IsProcessed)
							End If
						
						Case "HistoryID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.HistoryID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.HistoryID)
							End If
						
						Case "CollectionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CollectionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.CollectionID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCollectionValueDateClearingInstruments)
				Me.entity = entity
			End Sub				
		
	
			Public Property ValueDateClearingInstrumentID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ValueDateClearingInstrumentID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ValueDateClearingInstrumentID = Nothing
					Else
						entity.ValueDateClearingInstrumentID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstrumentNo As System.String 
				Get
					Dim data_ As System.String = entity.InstrumentNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstrumentNo = Nothing
					Else
						entity.InstrumentNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsProcessed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsProcessed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsProcessed = Nothing
					Else
						entity.IsProcessed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property HistoryID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.HistoryID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HistoryID = Nothing
					Else
						entity.HistoryID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CollectionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CollectionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CollectionID = Nothing
					Else
						entity.CollectionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCollectionValueDateClearingInstruments
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingInstrumentsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCollectionValueDateClearingInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionValueDateClearingInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCollectionValueDateClearingInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCollectionValueDateClearingInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCollectionValueDateClearingInstrumentsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCollectionValueDateClearingInstrumentsCollection
		Inherits esEntityCollection(Of ICCollectionValueDateClearingInstruments)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingInstrumentsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCollectionValueDateClearingInstrumentsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCollectionValueDateClearingInstrumentsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCollectionValueDateClearingInstrumentsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCollectionValueDateClearingInstrumentsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCollectionValueDateClearingInstrumentsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCollectionValueDateClearingInstrumentsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCollectionValueDateClearingInstrumentsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCollectionValueDateClearingInstrumentsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCollectionValueDateClearingInstrumentsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCollectionValueDateClearingInstrumentsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ValueDateClearingInstrumentID" 
					Return Me.ValueDateClearingInstrumentID
				Case "InstrumentNo" 
					Return Me.InstrumentNo
				Case "IsProcessed" 
					Return Me.IsProcessed
				Case "HistoryID" 
					Return Me.HistoryID
				Case "CollectionID" 
					Return Me.CollectionID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ValueDateClearingInstrumentID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.ValueDateClearingInstrumentID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property InstrumentNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.InstrumentNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsProcessed As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.IsProcessed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property HistoryID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.HistoryID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CollectionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.CollectionID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCollectionValueDateClearingInstruments 
		Inherits esICCollectionValueDateClearingInstruments
		
	
		#Region "UpToICCollectionsByCollectionID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionValueDateClearingInstruments_IC_Collections
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionsByCollectionID As ICCollections
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionsByCollectionID Is Nothing _
						 AndAlso Not CollectionID.Equals(Nothing)  Then
						
					Me._UpToICCollectionsByCollectionID = New ICCollections()
					Me._UpToICCollectionsByCollectionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionsByCollectionID", Me._UpToICCollectionsByCollectionID)
					Me._UpToICCollectionsByCollectionID.Query.Where(Me._UpToICCollectionsByCollectionID.Query.CollectionID = Me.CollectionID)
					Me._UpToICCollectionsByCollectionID.Query.Load()
				End If

				Return Me._UpToICCollectionsByCollectionID
			End Get
			
            Set(ByVal value As ICCollections)
				Me.RemovePreSave("UpToICCollectionsByCollectionID")
				

				If value Is Nothing Then
				
					Me.CollectionID = Nothing
				
					Me._UpToICCollectionsByCollectionID = Nothing
				Else
				
					Me.CollectionID = value.CollectionID
					
					Me._UpToICCollectionsByCollectionID = value
					Me.SetPreSave("UpToICCollectionsByCollectionID", Me._UpToICCollectionsByCollectionID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCollectionValueDateClearingHistoryByHistoryID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_CollectionValueDateClearingInstruments_IC_CollectionValueDateClearingHistory
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCollectionValueDateClearingHistoryByHistoryID As ICCollectionValueDateClearingHistory
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCollectionValueDateClearingHistoryByHistoryID Is Nothing _
						 AndAlso Not HistoryID.Equals(Nothing)  Then
						
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID = New ICCollectionValueDateClearingHistory()
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCollectionValueDateClearingHistoryByHistoryID", Me._UpToICCollectionValueDateClearingHistoryByHistoryID)
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID.Query.Where(Me._UpToICCollectionValueDateClearingHistoryByHistoryID.Query.HistoryID = Me.HistoryID)
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID.Query.Load()
				End If

				Return Me._UpToICCollectionValueDateClearingHistoryByHistoryID
			End Get
			
            Set(ByVal value As ICCollectionValueDateClearingHistory)
				Me.RemovePreSave("UpToICCollectionValueDateClearingHistoryByHistoryID")
				

				If value Is Nothing Then
				
					Me.HistoryID = Nothing
				
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID = Nothing
				Else
				
					Me.HistoryID = value.HistoryID
					
					Me._UpToICCollectionValueDateClearingHistoryByHistoryID = value
					Me.SetPreSave("UpToICCollectionValueDateClearingHistoryByHistoryID", Me._UpToICCollectionValueDateClearingHistoryByHistoryID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCollectionsByCollectionID Is Nothing Then
				Me.CollectionID = Me._UpToICCollectionsByCollectionID.CollectionID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICCollectionValueDateClearingHistoryByHistoryID Is Nothing Then
				Me.HistoryID = Me._UpToICCollectionValueDateClearingHistoryByHistoryID.HistoryID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICCollectionValueDateClearingInstrumentsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.ValueDateClearingInstrumentID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.ValueDateClearingInstrumentID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.InstrumentNo, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.InstrumentNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.IsProcessed, 2, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.IsProcessed
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.HistoryID, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.HistoryID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCollectionValueDateClearingInstrumentsMetadata.ColumnNames.CollectionID, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCollectionValueDateClearingInstrumentsMetadata.PropertyNames.CollectionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCollectionValueDateClearingInstrumentsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ValueDateClearingInstrumentID As String = "ValueDateClearingInstrumentID"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const HistoryID As String = "HistoryID"
			 Public Const CollectionID As String = "CollectionID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ValueDateClearingInstrumentID As String = "ValueDateClearingInstrumentID"
			 Public Const InstrumentNo As String = "InstrumentNo"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const HistoryID As String = "HistoryID"
			 Public Const CollectionID As String = "CollectionID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCollectionValueDateClearingInstrumentsMetadata)
			
				If ICCollectionValueDateClearingInstrumentsMetadata.mapDelegates Is Nothing Then
					ICCollectionValueDateClearingInstrumentsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCollectionValueDateClearingInstrumentsMetadata._meta Is Nothing Then
					ICCollectionValueDateClearingInstrumentsMetadata._meta = New ICCollectionValueDateClearingInstrumentsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ValueDateClearingInstrumentID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("InstrumentNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsProcessed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("HistoryID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CollectionID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_CollectionValueDateClearingInstruments"
				meta.Destination = "IC_CollectionValueDateClearingInstruments"
				
				meta.spInsert = "proc_IC_CollectionValueDateClearingInstrumentsInsert"
				meta.spUpdate = "proc_IC_CollectionValueDateClearingInstrumentsUpdate"
				meta.spDelete = "proc_IC_CollectionValueDateClearingInstrumentsDelete"
				meta.spLoadAll = "proc_IC_CollectionValueDateClearingInstrumentsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CollectionValueDateClearingInstrumentsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCollectionValueDateClearingInstrumentsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
