
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_GlobalCharges' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICGlobalCharges))> _
	<XmlType("ICGlobalCharges")> _	
	Partial Public Class ICGlobalCharges 
		Inherits esICGlobalCharges
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICGlobalCharges()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal globalChargeID As System.Int32)
			Dim obj As New ICGlobalCharges()
			obj.GlobalChargeID = globalChargeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal globalChargeID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICGlobalCharges()
			obj.GlobalChargeID = globalChargeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICGlobalChargesCollection")> _
	Partial Public Class ICGlobalChargesCollection
		Inherits esICGlobalChargesCollection
		Implements IEnumerable(Of ICGlobalCharges)
	
		Public Function FindByPrimaryKey(ByVal globalChargeID As System.Int32) As ICGlobalCharges
			Return MyBase.SingleOrDefault(Function(e) e.GlobalChargeID.HasValue AndAlso e.GlobalChargeID.Value = globalChargeID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICGlobalCharges))> _
		Public Class ICGlobalChargesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICGlobalChargesCollection)
			
			Public Shared Widening Operator CType(packet As ICGlobalChargesCollectionWCFPacket) As ICGlobalChargesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICGlobalChargesCollection) As ICGlobalChargesCollectionWCFPacket
				Return New ICGlobalChargesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICGlobalChargesQuery 
		Inherits esICGlobalChargesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICGlobalChargesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICGlobalChargesQuery) As String
			Return ICGlobalChargesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICGlobalChargesQuery
			Return DirectCast(ICGlobalChargesQuery.SerializeHelper.FromXml(query, GetType(ICGlobalChargesQuery)), ICGlobalChargesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICGlobalCharges
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal globalChargeID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(globalChargeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(globalChargeID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal globalChargeID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(globalChargeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(globalChargeID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal globalChargeID As System.Int32) As Boolean
		
			Dim query As New ICGlobalChargesQuery()
			query.Where(query.GlobalChargeID = globalChargeID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal globalChargeID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("GlobalChargeID", globalChargeID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_GlobalCharges.GlobalChargeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GlobalChargeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGlobalChargesMetadata.ColumnNames.GlobalChargeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGlobalChargesMetadata.ColumnNames.GlobalChargeID, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.GlobalChargeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.Title
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Title As System.String
			Get
				Return MyBase.GetSystemString(ICGlobalChargesMetadata.ColumnNames.Title)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICGlobalChargesMetadata.ColumnNames.Title, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.Title)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.ChargeAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICGlobalChargesMetadata.ColumnNames.ChargeAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICGlobalChargesMetadata.ColumnNames.ChargeAmount, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.ChargeAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.ChargePercentage
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargePercentage As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICGlobalChargesMetadata.ColumnNames.ChargePercentage)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICGlobalChargesMetadata.ColumnNames.ChargePercentage, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.ChargePercentage)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.ChargeType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeType As System.String
			Get
				Return MyBase.GetSystemString(ICGlobalChargesMetadata.ColumnNames.ChargeType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICGlobalChargesMetadata.ColumnNames.ChargeType, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.ChargeType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.PrincipalBankAccountID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PrincipalBankAccountID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGlobalChargesMetadata.ColumnNames.PrincipalBankAccountID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGlobalChargesMetadata.ColumnNames.PrincipalBankAccountID, value) Then
					Me._UpToICBankAccountsByPrincipalBankAccountID = Nothing
					Me.OnPropertyChanged("UpToICBankAccountsByPrincipalBankAccountID")
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.PrincipalBankAccountID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.Active
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Active As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICGlobalChargesMetadata.ColumnNames.Active)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICGlobalChargesMetadata.ColumnNames.Active, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.Active)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICGlobalChargesMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICGlobalChargesMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_GlobalCharges.CreatedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICGlobalChargesMetadata.ColumnNames.CreatedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICGlobalChargesMetadata.ColumnNames.CreatedOn, value) Then
					OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.CreatedOn)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICBankAccountsByPrincipalBankAccountID As ICBankAccounts
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "GlobalChargeID"
							Me.str().GlobalChargeID = CType(value, string)
												
						Case "Title"
							Me.str().Title = CType(value, string)
												
						Case "ChargeAmount"
							Me.str().ChargeAmount = CType(value, string)
												
						Case "ChargePercentage"
							Me.str().ChargePercentage = CType(value, string)
												
						Case "ChargeType"
							Me.str().ChargeType = CType(value, string)
												
						Case "PrincipalBankAccountID"
							Me.str().PrincipalBankAccountID = CType(value, string)
												
						Case "Active"
							Me.str().Active = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedOn"
							Me.str().CreatedOn = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "GlobalChargeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GlobalChargeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.GlobalChargeID)
							End If
						
						Case "ChargeAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.ChargeAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.ChargeAmount)
							End If
						
						Case "ChargePercentage"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.ChargePercentage = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.ChargePercentage)
							End If
						
						Case "PrincipalBankAccountID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PrincipalBankAccountID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.PrincipalBankAccountID)
							End If
						
						Case "Active"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.Active = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.Active)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICGlobalChargesMetadata.PropertyNames.CreatedOn)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICGlobalCharges)
				Me.entity = entity
			End Sub				
		
	
			Public Property GlobalChargeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GlobalChargeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GlobalChargeID = Nothing
					Else
						entity.GlobalChargeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Title As System.String 
				Get
					Dim data_ As System.String = entity.Title
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Title = Nothing
					Else
						entity.Title = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.ChargeAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeAmount = Nothing
					Else
						entity.ChargeAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargePercentage As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.ChargePercentage
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargePercentage = Nothing
					Else
						entity.ChargePercentage = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeType As System.String 
				Get
					Dim data_ As System.String = entity.ChargeType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeType = Nothing
					Else
						entity.ChargeType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PrincipalBankAccountID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PrincipalBankAccountID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PrincipalBankAccountID = Nothing
					Else
						entity.PrincipalBankAccountID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Active As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.Active
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Active = Nothing
					Else
						entity.Active = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedOn = Nothing
					Else
						entity.CreatedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICGlobalCharges
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICGlobalChargesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICGlobalChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICGlobalChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICGlobalChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICGlobalChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICGlobalChargesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICGlobalChargesCollection
		Inherits esEntityCollection(Of ICGlobalCharges)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICGlobalChargesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICGlobalChargesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICGlobalChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICGlobalChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICGlobalChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICGlobalChargesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICGlobalChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICGlobalChargesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICGlobalChargesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICGlobalChargesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICGlobalChargesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "GlobalChargeID" 
					Return Me.GlobalChargeID
				Case "Title" 
					Return Me.Title
				Case "ChargeAmount" 
					Return Me.ChargeAmount
				Case "ChargePercentage" 
					Return Me.ChargePercentage
				Case "ChargeType" 
					Return Me.ChargeType
				Case "PrincipalBankAccountID" 
					Return Me.PrincipalBankAccountID
				Case "Active" 
					Return Me.Active
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedOn" 
					Return Me.CreatedOn
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property GlobalChargeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.GlobalChargeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Title As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.Title, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.ChargeAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ChargePercentage As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.ChargePercentage, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeType As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.ChargeType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PrincipalBankAccountID As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.PrincipalBankAccountID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Active As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.Active, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICGlobalChargesMetadata.ColumnNames.CreatedOn, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICGlobalCharges 
		Inherits esICGlobalCharges
		
	
		#Region "ICPackageChargesCollectionByGlobalChargeId - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackageChargesCollectionByGlobalChargeId() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGlobalCharges.ICPackageChargesCollectionByGlobalChargeId_Delegate)
				map.PropertyName = "ICPackageChargesCollectionByGlobalChargeId"
				map.MyColumnName = "GlobalChargeId"
				map.ParentColumnName = "GlobalChargeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackageChargesCollectionByGlobalChargeId_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGlobalChargesQuery(data.NextAlias())
			
			Dim mee As ICPackageChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackageChargesQuery), New ICPackageChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GlobalChargeID = mee.GlobalChargeId)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PackageCharges_IC_GlobalCharges
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackageChargesCollectionByGlobalChargeId As ICPackageChargesCollection 
		
			Get
				If Me._ICPackageChargesCollectionByGlobalChargeId Is Nothing Then
					Me._ICPackageChargesCollectionByGlobalChargeId = New ICPackageChargesCollection()
					Me._ICPackageChargesCollectionByGlobalChargeId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackageChargesCollectionByGlobalChargeId", Me._ICPackageChargesCollectionByGlobalChargeId)
				
					If Not Me.GlobalChargeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackageChargesCollectionByGlobalChargeId.Query.Where(Me._ICPackageChargesCollectionByGlobalChargeId.Query.GlobalChargeId = Me.GlobalChargeID)
							Me._ICPackageChargesCollectionByGlobalChargeId.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackageChargesCollectionByGlobalChargeId.fks.Add(ICPackageChargesMetadata.ColumnNames.GlobalChargeId, Me.GlobalChargeID)
					End If
				End If

				Return Me._ICPackageChargesCollectionByGlobalChargeId
			End Get
			
			Set(ByVal value As ICPackageChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackageChargesCollectionByGlobalChargeId Is Nothing Then

					Me.RemovePostSave("ICPackageChargesCollectionByGlobalChargeId")
					Me._ICPackageChargesCollectionByGlobalChargeId = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackageChargesCollectionByGlobalChargeId As ICPackageChargesCollection
		#End Region

		#Region "UpToICPackageCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_MasterPackage_IC_GlobalCharges
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICPackageCollection As ICPackageCollection
		
			Get
				If Me._UpToICPackageCollection Is Nothing Then
					Me._UpToICPackageCollection = New ICPackageCollection()
					Me._UpToICPackageCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICPackageCollection", Me._UpToICPackageCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.GlobalChargeID.Equals(Nothing) Then 
				
						Dim m As New ICPackageQuery("m")
						Dim j As New ICPackagesGlobalChargesQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.PackageID = j.PackageID)
                        m.Where(j.GlobalChargeID = Me.GlobalChargeID)

						Me._UpToICPackageCollection.Load(m)

					End If
				End If

				Return Me._UpToICPackageCollection
			End Get
			
			Set(ByVal value As ICPackageCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICPackageCollection Is Nothing Then

					Me.RemovePostSave("UpToICPackageCollection")
					Me._UpToICPackageCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_MasterPackage_IC_GlobalCharges
		''' </summary>
		Public Sub AssociateICPackageCollection(entity As ICPackage)
			If Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Me._ICPackagesGlobalChargesCollection = New ICPackagesGlobalChargesCollection()
				Me._ICPackagesGlobalChargesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICPackagesGlobalChargesCollection", Me._ICPackagesGlobalChargesCollection)
			End If
			
			Dim obj As ICPackagesGlobalCharges = Me._ICPackagesGlobalChargesCollection.AddNew()
			obj.GlobalChargeID = Me.GlobalChargeID
			obj.PackageID = entity.PackageID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_MasterPackage_IC_GlobalCharges
		''' </summary>
		Public Sub DissociateICPackageCollection(entity As ICPackage)
			If Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Me._ICPackagesGlobalChargesCollection = new ICPackagesGlobalChargesCollection()
				Me._ICPackagesGlobalChargesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICPackagesGlobalChargesCollection", Me._ICPackagesGlobalChargesCollection)
			End If

			Dim obj As ICPackagesGlobalCharges = Me._ICPackagesGlobalChargesCollection.AddNew()
			obj.GlobalChargeID = Me.GlobalChargeID
            obj.PackageID = entity.PackageID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICPackageCollection As ICPackageCollection
		Private _ICPackagesGlobalChargesCollection As ICPackagesGlobalChargesCollection
		#End Region

		#Region "ICPackagesGlobalChargesCollectionByGlobalChargeID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPackagesGlobalChargesCollectionByGlobalChargeID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICGlobalCharges.ICPackagesGlobalChargesCollectionByGlobalChargeID_Delegate)
				map.PropertyName = "ICPackagesGlobalChargesCollectionByGlobalChargeID"
				map.MyColumnName = "GlobalChargeID"
				map.ParentColumnName = "GlobalChargeID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPackagesGlobalChargesCollectionByGlobalChargeID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICGlobalChargesQuery(data.NextAlias())
			
			Dim mee As ICPackagesGlobalChargesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPackagesGlobalChargesQuery), New ICPackagesGlobalChargesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.GlobalChargeID = mee.GlobalChargeID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_MasterPackage_IC_GlobalCharges
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPackagesGlobalChargesCollectionByGlobalChargeID As ICPackagesGlobalChargesCollection 
		
			Get
				If Me._ICPackagesGlobalChargesCollectionByGlobalChargeID Is Nothing Then
					Me._ICPackagesGlobalChargesCollectionByGlobalChargeID = New ICPackagesGlobalChargesCollection()
					Me._ICPackagesGlobalChargesCollectionByGlobalChargeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPackagesGlobalChargesCollectionByGlobalChargeID", Me._ICPackagesGlobalChargesCollectionByGlobalChargeID)
				
					If Not Me.GlobalChargeID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPackagesGlobalChargesCollectionByGlobalChargeID.Query.Where(Me._ICPackagesGlobalChargesCollectionByGlobalChargeID.Query.GlobalChargeID = Me.GlobalChargeID)
							Me._ICPackagesGlobalChargesCollectionByGlobalChargeID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPackagesGlobalChargesCollectionByGlobalChargeID.fks.Add(ICPackagesGlobalChargesMetadata.ColumnNames.GlobalChargeID, Me.GlobalChargeID)
					End If
				End If

				Return Me._ICPackagesGlobalChargesCollectionByGlobalChargeID
			End Get
			
			Set(ByVal value As ICPackagesGlobalChargesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPackagesGlobalChargesCollectionByGlobalChargeID Is Nothing Then

					Me.RemovePostSave("ICPackagesGlobalChargesCollectionByGlobalChargeID")
					Me._ICPackagesGlobalChargesCollectionByGlobalChargeID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPackagesGlobalChargesCollectionByGlobalChargeID As ICPackagesGlobalChargesCollection
		#End Region

		#Region "UpToICBankAccountsByPrincipalBankAccountID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_GlobalCharges_IC_BankAccounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICBankAccountsByPrincipalBankAccountID As ICBankAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICBankAccountsByPrincipalBankAccountID Is Nothing _
						 AndAlso Not PrincipalBankAccountID.Equals(Nothing)  Then
						
					Me._UpToICBankAccountsByPrincipalBankAccountID = New ICBankAccounts()
					Me._UpToICBankAccountsByPrincipalBankAccountID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICBankAccountsByPrincipalBankAccountID", Me._UpToICBankAccountsByPrincipalBankAccountID)
					Me._UpToICBankAccountsByPrincipalBankAccountID.Query.Where(Me._UpToICBankAccountsByPrincipalBankAccountID.Query.PrincipalBankAccountID = Me.PrincipalBankAccountID)
					Me._UpToICBankAccountsByPrincipalBankAccountID.Query.Load()
				End If

				Return Me._UpToICBankAccountsByPrincipalBankAccountID
			End Get
			
            Set(ByVal value As ICBankAccounts)
				Me.RemovePreSave("UpToICBankAccountsByPrincipalBankAccountID")
				

				If value Is Nothing Then
				
					Me.PrincipalBankAccountID = Nothing
				
					Me._UpToICBankAccountsByPrincipalBankAccountID = Nothing
				Else
				
					Me.PrincipalBankAccountID = value.PrincipalBankAccountID
					
					Me._UpToICBankAccountsByPrincipalBankAccountID = value
					Me.SetPreSave("UpToICBankAccountsByPrincipalBankAccountID", Me._UpToICBankAccountsByPrincipalBankAccountID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICPackageChargesCollectionByGlobalChargeId"
					coll = Me.ICPackageChargesCollectionByGlobalChargeId
					Exit Select
				Case "ICPackagesGlobalChargesCollectionByGlobalChargeID"
					coll = Me.ICPackagesGlobalChargesCollectionByGlobalChargeID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICPackageChargesCollectionByGlobalChargeId", GetType(ICPackageChargesCollection), New ICPackageCharges()))
			props.Add(new esPropertyDescriptor(Me, "ICPackagesGlobalChargesCollectionByGlobalChargeID", GetType(ICPackagesGlobalChargesCollection), New ICPackagesGlobalCharges()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICBankAccountsByPrincipalBankAccountID Is Nothing Then
				Me.PrincipalBankAccountID = Me._UpToICBankAccountsByPrincipalBankAccountID.PrincipalBankAccountID
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICPackageChargesCollectionByGlobalChargeId Is Nothing Then
				Apply(Me._ICPackageChargesCollectionByGlobalChargeId, "GlobalChargeId", Me.GlobalChargeID)
			End If
			
			If Not Me._ICPackagesGlobalChargesCollection Is Nothing Then
				Apply(Me._ICPackagesGlobalChargesCollection, "GlobalChargeID", Me.GlobalChargeID)
			End If
			
			If Not Me._ICPackagesGlobalChargesCollectionByGlobalChargeID Is Nothing Then
				Apply(Me._ICPackagesGlobalChargesCollectionByGlobalChargeID, "GlobalChargeID", Me.GlobalChargeID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICGlobalChargesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.GlobalChargeID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.GlobalChargeID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.Title, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.Title
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.ChargeAmount, 2, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.ChargeAmount
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.ChargePercentage, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.ChargePercentage
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.ChargeType, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.ChargeType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.PrincipalBankAccountID, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.PrincipalBankAccountID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.Active, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.Active
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.CreatedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICGlobalChargesMetadata.ColumnNames.CreatedOn, 8, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICGlobalChargesMetadata.PropertyNames.CreatedOn
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICGlobalChargesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const GlobalChargeID As String = "GlobalChargeID"
			 Public Const Title As String = "Title"
			 Public Const ChargeAmount As String = "ChargeAmount"
			 Public Const ChargePercentage As String = "ChargePercentage"
			 Public Const ChargeType As String = "ChargeType"
			 Public Const PrincipalBankAccountID As String = "PrincipalBankAccountID"
			 Public Const Active As String = "Active"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const GlobalChargeID As String = "GlobalChargeID"
			 Public Const Title As String = "Title"
			 Public Const ChargeAmount As String = "ChargeAmount"
			 Public Const ChargePercentage As String = "ChargePercentage"
			 Public Const ChargeType As String = "ChargeType"
			 Public Const PrincipalBankAccountID As String = "PrincipalBankAccountID"
			 Public Const Active As String = "Active"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedOn As String = "CreatedOn"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICGlobalChargesMetadata)
			
				If ICGlobalChargesMetadata.mapDelegates Is Nothing Then
					ICGlobalChargesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICGlobalChargesMetadata._meta Is Nothing Then
					ICGlobalChargesMetadata._meta = New ICGlobalChargesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("GlobalChargeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Title", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ChargeAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("ChargePercentage", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("ChargeType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PrincipalBankAccountID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Active", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedOn", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_GlobalCharges"
				meta.Destination = "IC_GlobalCharges"
				
				meta.spInsert = "proc_IC_GlobalChargesInsert"
				meta.spUpdate = "proc_IC_GlobalChargesUpdate"
				meta.spDelete = "proc_IC_GlobalChargesDelete"
				meta.spLoadAll = "proc_IC_GlobalChargesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_GlobalChargesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICGlobalChargesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
