
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Packages_GlobalCharges' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPackagesGlobalCharges))> _
	<XmlType("ICPackagesGlobalCharges")> _	
	Partial Public Class ICPackagesGlobalCharges 
		Inherits esICPackagesGlobalCharges
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPackagesGlobalCharges()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32)
			Dim obj As New ICPackagesGlobalCharges()
			obj.PackageID = packageID
			obj.GlobalChargeID = globalChargeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPackagesGlobalCharges()
			obj.PackageID = packageID
			obj.GlobalChargeID = globalChargeID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPackagesGlobalChargesCollection")> _
	Partial Public Class ICPackagesGlobalChargesCollection
		Inherits esICPackagesGlobalChargesCollection
		Implements IEnumerable(Of ICPackagesGlobalCharges)
	
		Public Function FindByPrimaryKey(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32) As ICPackagesGlobalCharges
			Return MyBase.SingleOrDefault(Function(e) e.PackageID.HasValue AndAlso e.PackageID.Value = packageID And e.GlobalChargeID.HasValue AndAlso e.GlobalChargeID.Value = globalChargeID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPackagesGlobalCharges))> _
		Public Class ICPackagesGlobalChargesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPackagesGlobalChargesCollection)
			
			Public Shared Widening Operator CType(packet As ICPackagesGlobalChargesCollectionWCFPacket) As ICPackagesGlobalChargesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPackagesGlobalChargesCollection) As ICPackagesGlobalChargesCollectionWCFPacket
				Return New ICPackagesGlobalChargesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPackagesGlobalChargesQuery 
		Inherits esICPackagesGlobalChargesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPackagesGlobalChargesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPackagesGlobalChargesQuery) As String
			Return ICPackagesGlobalChargesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPackagesGlobalChargesQuery
			Return DirectCast(ICPackagesGlobalChargesQuery.SerializeHelper.FromXml(query, GetType(ICPackagesGlobalChargesQuery)), ICPackagesGlobalChargesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPackagesGlobalCharges
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(packageID, globalChargeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(packageID, globalChargeID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(packageID, globalChargeID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(packageID, globalChargeID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32) As Boolean
		
			Dim query As New ICPackagesGlobalChargesQuery()
			query.Where(query.PackageID = packageID And query.GlobalChargeID = globalChargeID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal packageID As System.Int32, ByVal globalChargeID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("PackageID", packageID)
						parms.Add("GlobalChargeID", globalChargeID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Packages_GlobalCharges.PackageID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PackageID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackagesGlobalChargesMetadata.ColumnNames.PackageID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackagesGlobalChargesMetadata.ColumnNames.PackageID, value) Then
					Me._UpToICPackageByPackageID = Nothing
					Me.OnPropertyChanged("UpToICPackageByPackageID")
					OnPropertyChanged(ICPackagesGlobalChargesMetadata.PropertyNames.PackageID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Packages_GlobalCharges.GlobalChargeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GlobalChargeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackagesGlobalChargesMetadata.ColumnNames.GlobalChargeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackagesGlobalChargesMetadata.ColumnNames.GlobalChargeID, value) Then
					Me._UpToICGlobalChargesByGlobalChargeID = Nothing
					Me.OnPropertyChanged("UpToICGlobalChargesByGlobalChargeID")
					OnPropertyChanged(ICPackagesGlobalChargesMetadata.PropertyNames.GlobalChargeID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICGlobalChargesByGlobalChargeID As ICGlobalCharges
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPackageByPackageID As ICPackage
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "PackageID"
							Me.str().PackageID = CType(value, string)
												
						Case "GlobalChargeID"
							Me.str().GlobalChargeID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "PackageID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PackageID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackagesGlobalChargesMetadata.PropertyNames.PackageID)
							End If
						
						Case "GlobalChargeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GlobalChargeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackagesGlobalChargesMetadata.PropertyNames.GlobalChargeID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPackagesGlobalCharges)
				Me.entity = entity
			End Sub				
		
	
			Public Property PackageID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PackageID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PackageID = Nothing
					Else
						entity.PackageID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GlobalChargeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GlobalChargeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GlobalChargeID = Nothing
					Else
						entity.GlobalChargeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPackagesGlobalCharges
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackagesGlobalChargesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPackagesGlobalChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackagesGlobalChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPackagesGlobalChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPackagesGlobalChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPackagesGlobalChargesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPackagesGlobalChargesCollection
		Inherits esEntityCollection(Of ICPackagesGlobalCharges)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackagesGlobalChargesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPackagesGlobalChargesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPackagesGlobalChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackagesGlobalChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPackagesGlobalChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPackagesGlobalChargesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPackagesGlobalChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPackagesGlobalChargesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPackagesGlobalChargesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPackagesGlobalChargesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPackagesGlobalChargesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "PackageID" 
					Return Me.PackageID
				Case "GlobalChargeID" 
					Return Me.GlobalChargeID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property PackageID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackagesGlobalChargesMetadata.ColumnNames.PackageID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GlobalChargeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackagesGlobalChargesMetadata.ColumnNames.GlobalChargeID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPackagesGlobalCharges 
		Inherits esICPackagesGlobalCharges
		
	
		#Region "UpToICGlobalChargesByGlobalChargeID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MasterPackage_IC_GlobalCharges
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICGlobalChargesByGlobalChargeID As ICGlobalCharges
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICGlobalChargesByGlobalChargeID Is Nothing _
						 AndAlso Not GlobalChargeID.Equals(Nothing)  Then
						
					Me._UpToICGlobalChargesByGlobalChargeID = New ICGlobalCharges()
					Me._UpToICGlobalChargesByGlobalChargeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICGlobalChargesByGlobalChargeID", Me._UpToICGlobalChargesByGlobalChargeID)
					Me._UpToICGlobalChargesByGlobalChargeID.Query.Where(Me._UpToICGlobalChargesByGlobalChargeID.Query.GlobalChargeID = Me.GlobalChargeID)
					Me._UpToICGlobalChargesByGlobalChargeID.Query.Load()
				End If

				Return Me._UpToICGlobalChargesByGlobalChargeID
			End Get
			
            Set(ByVal value As ICGlobalCharges)
				Me.RemovePreSave("UpToICGlobalChargesByGlobalChargeID")
				

				If value Is Nothing Then
				
					Me.GlobalChargeID = Nothing
				
					Me._UpToICGlobalChargesByGlobalChargeID = Nothing
				Else
				
					Me.GlobalChargeID = value.GlobalChargeID
					
					Me._UpToICGlobalChargesByGlobalChargeID = value
					Me.SetPreSave("UpToICGlobalChargesByGlobalChargeID", Me._UpToICGlobalChargesByGlobalChargeID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPackageByPackageID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_MasterPackage_IC_PackageManagement
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPackageByPackageID As ICPackage
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPackageByPackageID Is Nothing _
						 AndAlso Not PackageID.Equals(Nothing)  Then
						
					Me._UpToICPackageByPackageID = New ICPackage()
					Me._UpToICPackageByPackageID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPackageByPackageID", Me._UpToICPackageByPackageID)
					Me._UpToICPackageByPackageID.Query.Where(Me._UpToICPackageByPackageID.Query.PackageID = Me.PackageID)
					Me._UpToICPackageByPackageID.Query.Load()
				End If

				Return Me._UpToICPackageByPackageID
			End Get
			
            Set(ByVal value As ICPackage)
				Me.RemovePreSave("UpToICPackageByPackageID")
				

				If value Is Nothing Then
				
					Me.PackageID = Nothing
				
					Me._UpToICPackageByPackageID = Nothing
				Else
				
					Me.PackageID = value.PackageID
					
					Me._UpToICPackageByPackageID = value
					Me.SetPreSave("UpToICPackageByPackageID", Me._UpToICPackageByPackageID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICGlobalChargesByGlobalChargeID Is Nothing Then
				Me.GlobalChargeID = Me._UpToICGlobalChargesByGlobalChargeID.GlobalChargeID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICPackageByPackageID Is Nothing Then
				Me.PackageID = Me._UpToICPackageByPackageID.PackageID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPackagesGlobalChargesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPackagesGlobalChargesMetadata.ColumnNames.PackageID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackagesGlobalChargesMetadata.PropertyNames.PackageID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackagesGlobalChargesMetadata.ColumnNames.GlobalChargeID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackagesGlobalChargesMetadata.PropertyNames.GlobalChargeID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPackagesGlobalChargesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const PackageID As String = "PackageID"
			 Public Const GlobalChargeID As String = "GlobalChargeID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const PackageID As String = "PackageID"
			 Public Const GlobalChargeID As String = "GlobalChargeID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPackagesGlobalChargesMetadata)
			
				If ICPackagesGlobalChargesMetadata.mapDelegates Is Nothing Then
					ICPackagesGlobalChargesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPackagesGlobalChargesMetadata._meta Is Nothing Then
					ICPackagesGlobalChargesMetadata._meta = New ICPackagesGlobalChargesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("PackageID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GlobalChargeID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_Packages_GlobalCharges"
				meta.Destination = "IC_Packages_GlobalCharges"
				
				meta.spInsert = "proc_IC_Packages_GlobalChargesInsert"
				meta.spUpdate = "proc_IC_Packages_GlobalChargesUpdate"
				meta.spDelete = "proc_IC_Packages_GlobalChargesDelete"
				meta.spLoadAll = "proc_IC_Packages_GlobalChargesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_Packages_GlobalChargesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPackagesGlobalChargesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
