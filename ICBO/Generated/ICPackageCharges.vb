
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:59 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_PackageCharges' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICPackageCharges))> _
	<XmlType("ICPackageCharges")> _	
	Partial Public Class ICPackageCharges 
		Inherits esICPackageCharges
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICPackageCharges()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal chargeId As System.Int64)
			Dim obj As New ICPackageCharges()
			obj.ChargeId = chargeId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal chargeId As System.Int64, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICPackageCharges()
			obj.ChargeId = chargeId
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICPackageChargesCollection")> _
	Partial Public Class ICPackageChargesCollection
		Inherits esICPackageChargesCollection
		Implements IEnumerable(Of ICPackageCharges)
	
		Public Function FindByPrimaryKey(ByVal chargeId As System.Int64) As ICPackageCharges
			Return MyBase.SingleOrDefault(Function(e) e.ChargeId.HasValue AndAlso e.ChargeId.Value = chargeId)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICPackageCharges))> _
		Public Class ICPackageChargesCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICPackageChargesCollection)
			
			Public Shared Widening Operator CType(packet As ICPackageChargesCollectionWCFPacket) As ICPackageChargesCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICPackageChargesCollection) As ICPackageChargesCollectionWCFPacket
				Return New ICPackageChargesCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICPackageChargesQuery 
		Inherits esICPackageChargesQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICPackageChargesQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICPackageChargesQuery) As String
			Return ICPackageChargesQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICPackageChargesQuery
			Return DirectCast(ICPackageChargesQuery.SerializeHelper.FromXml(query, GetType(ICPackageChargesQuery)), ICPackageChargesQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICPackageCharges
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal chargeId As System.Int64) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(chargeId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(chargeId)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal chargeId As System.Int64) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(chargeId)
			Else
				Return LoadByPrimaryKeyStoredProcedure(chargeId)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal chargeId As System.Int64) As Boolean
		
			Dim query As New ICPackageChargesQuery()
			query.Where(query.ChargeId = chargeId)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal chargeId As System.Int64) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ChargeId", chargeId)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeId As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICPackageChargesMetadata.ColumnNames.ChargeId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICPackageChargesMetadata.ColumnNames.ChargeId, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.PacakgeId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PacakgeId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageChargesMetadata.ColumnNames.PacakgeId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageChargesMetadata.ColumnNames.PacakgeId, value) Then
					Me._UpToICPackageByPacakgeId = Nothing
					Me.OnPropertyChanged("UpToICPackageByPacakgeId")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.PacakgeId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeType As System.String
			Get
				Return MyBase.GetSystemString(ICPackageChargesMetadata.ColumnNames.ChargeType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICPackageChargesMetadata.ColumnNames.ChargeType, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeAmount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeAmount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICPackageChargesMetadata.ColumnNames.ChargeAmount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICPackageChargesMetadata.ColumnNames.ChargeAmount, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeAmount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeDate, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.InstructionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InstructionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageChargesMetadata.ColumnNames.InstructionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageChargesMetadata.ColumnNames.InstructionID, value) Then
					Me._UpToICInstructionByInstructionID = Nothing
					Me.OnPropertyChanged("UpToICInstructionByInstructionID")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.InstructionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.GlobalChargeId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GlobalChargeId As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICPackageChargesMetadata.ColumnNames.GlobalChargeId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICPackageChargesMetadata.ColumnNames.GlobalChargeId, value) Then
					Me._UpToICGlobalChargesByGlobalChargeId = Nothing
					Me.OnPropertyChanged("UpToICGlobalChargesByGlobalChargeId")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.GlobalChargeId)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeFromDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeFromDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeFromDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeFromDate, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeFromDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.ChargeToDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ChargeToDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeToDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICPackageChargesMetadata.ColumnNames.ChargeToDate, value) Then
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeToDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_PackageCharges.InvoiceId
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property InvoiceId As Nullable(Of System.Int64)
			Get
				Return MyBase.GetSystemInt64(ICPackageChargesMetadata.ColumnNames.InvoiceId)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int64))
				If MyBase.SetSystemInt64(ICPackageChargesMetadata.ColumnNames.InvoiceId, value) Then
					Me._UpToICPackageInvoiceByInvoiceId = Nothing
					Me.OnPropertyChanged("UpToICPackageInvoiceByInvoiceId")
					OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.InvoiceId)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICGlobalChargesByGlobalChargeId As ICGlobalCharges
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICInstructionByInstructionID As ICInstruction
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPackageByPacakgeId As ICPackage
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPackageInvoiceByInvoiceId As ICPackageInvoice
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ChargeId"
							Me.str().ChargeId = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "PacakgeId"
							Me.str().PacakgeId = CType(value, string)
												
						Case "ChargeType"
							Me.str().ChargeType = CType(value, string)
												
						Case "ChargeAmount"
							Me.str().ChargeAmount = CType(value, string)
												
						Case "ChargeDate"
							Me.str().ChargeDate = CType(value, string)
												
						Case "InstructionID"
							Me.str().InstructionID = CType(value, string)
												
						Case "GlobalChargeId"
							Me.str().GlobalChargeId = CType(value, string)
												
						Case "ChargeFromDate"
							Me.str().ChargeFromDate = CType(value, string)
												
						Case "ChargeToDate"
							Me.str().ChargeToDate = CType(value, string)
												
						Case "InvoiceId"
							Me.str().InvoiceId = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ChargeId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.ChargeId = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeId)
							End If
						
						Case "PacakgeId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PacakgeId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.PacakgeId)
							End If
						
						Case "ChargeAmount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.ChargeAmount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeAmount)
							End If
						
						Case "ChargeDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ChargeDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeDate)
							End If
						
						Case "InstructionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.InstructionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.InstructionID)
							End If
						
						Case "GlobalChargeId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GlobalChargeId = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.GlobalChargeId)
							End If
						
						Case "ChargeFromDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ChargeFromDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeFromDate)
							End If
						
						Case "ChargeToDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ChargeToDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.ChargeToDate)
							End If
						
						Case "InvoiceId"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int64" Then
								Me.InvoiceId = CType(value, Nullable(Of System.Int64))
								OnPropertyChanged(ICPackageChargesMetadata.PropertyNames.InvoiceId)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICPackageCharges)
				Me.entity = entity
			End Sub				
		
	
			Public Property ChargeId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.ChargeId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeId = Nothing
					Else
						entity.ChargeId = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PacakgeId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PacakgeId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PacakgeId = Nothing
					Else
						entity.PacakgeId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeType As System.String 
				Get
					Dim data_ As System.String = entity.ChargeType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeType = Nothing
					Else
						entity.ChargeType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeAmount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.ChargeAmount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeAmount = Nothing
					Else
						entity.ChargeAmount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ChargeDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeDate = Nothing
					Else
						entity.ChargeDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property InstructionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.InstructionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InstructionID = Nothing
					Else
						entity.InstructionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GlobalChargeId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GlobalChargeId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GlobalChargeId = Nothing
					Else
						entity.GlobalChargeId = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeFromDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ChargeFromDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeFromDate = Nothing
					Else
						entity.ChargeFromDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ChargeToDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ChargeToDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ChargeToDate = Nothing
					Else
						entity.ChargeToDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property InvoiceId As System.String 
				Get
					Dim data_ As Nullable(Of System.Int64) = entity.InvoiceId
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.InvoiceId = Nothing
					Else
						entity.InvoiceId = Convert.ToInt64(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICPackageCharges
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageChargesMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICPackageChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICPackageChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICPackageChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICPackageChargesQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICPackageChargesCollection
		Inherits esEntityCollection(Of ICPackageCharges)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICPackageChargesMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICPackageChargesCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICPackageChargesQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICPackageChargesQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICPackageChargesQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICPackageChargesQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICPackageChargesQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICPackageChargesQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICPackageChargesQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICPackageChargesQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICPackageChargesMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ChargeId" 
					Return Me.ChargeId
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "PacakgeId" 
					Return Me.PacakgeId
				Case "ChargeType" 
					Return Me.ChargeType
				Case "ChargeAmount" 
					Return Me.ChargeAmount
				Case "ChargeDate" 
					Return Me.ChargeDate
				Case "InstructionID" 
					Return Me.InstructionID
				Case "GlobalChargeId" 
					Return Me.GlobalChargeId
				Case "ChargeFromDate" 
					Return Me.ChargeFromDate
				Case "ChargeToDate" 
					Return Me.ChargeToDate
				Case "InvoiceId" 
					Return Me.InvoiceId
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ChargeId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeId, esSystemType.Int64)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PacakgeId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.PacakgeId, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeType As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeAmount As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeAmount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property InstructionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.InstructionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GlobalChargeId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.GlobalChargeId, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeFromDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeFromDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ChargeToDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.ChargeToDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property InvoiceId As esQueryItem
			Get
				Return New esQueryItem(Me, ICPackageChargesMetadata.ColumnNames.InvoiceId, esSystemType.Int64)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICPackageCharges 
		Inherits esICPackageCharges
		
	
		#Region "UpToICAccountsPaymentNatureProductTypeByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsPaymentNatureProductTypeByAccountNumber As ICAccountsPaymentNatureProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  AndAlso Not PaymentNatureCode.Equals(Nothing)  AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = New ICAccountsPaymentNatureProductType()
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Where(Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber
			End Get
			
            Set(ByVal value As ICAccountsPaymentNatureProductType)
				Me.RemovePreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me.PaymentNatureCode = Nothing
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber = value
					Me.SetPreSave("UpToICAccountsPaymentNatureProductTypeByAccountNumber", Me._UpToICAccountsPaymentNatureProductTypeByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICGlobalChargesByGlobalChargeId - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageCharges_IC_GlobalCharges
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICGlobalChargesByGlobalChargeId As ICGlobalCharges
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICGlobalChargesByGlobalChargeId Is Nothing _
						 AndAlso Not GlobalChargeId.Equals(Nothing)  Then
						
					Me._UpToICGlobalChargesByGlobalChargeId = New ICGlobalCharges()
					Me._UpToICGlobalChargesByGlobalChargeId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICGlobalChargesByGlobalChargeId", Me._UpToICGlobalChargesByGlobalChargeId)
					Me._UpToICGlobalChargesByGlobalChargeId.Query.Where(Me._UpToICGlobalChargesByGlobalChargeId.Query.GlobalChargeID = Me.GlobalChargeId)
					Me._UpToICGlobalChargesByGlobalChargeId.Query.Load()
				End If

				Return Me._UpToICGlobalChargesByGlobalChargeId
			End Get
			
            Set(ByVal value As ICGlobalCharges)
				Me.RemovePreSave("UpToICGlobalChargesByGlobalChargeId")
				

				If value Is Nothing Then
				
					Me.GlobalChargeId = Nothing
				
					Me._UpToICGlobalChargesByGlobalChargeId = Nothing
				Else
				
					Me.GlobalChargeId = value.GlobalChargeID
					
					Me._UpToICGlobalChargesByGlobalChargeId = value
					Me.SetPreSave("UpToICGlobalChargesByGlobalChargeId", Me._UpToICGlobalChargesByGlobalChargeId)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICInstructionByInstructionID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageCharges_IC_Instruction
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICInstructionByInstructionID As ICInstruction
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICInstructionByInstructionID Is Nothing _
						 AndAlso Not InstructionID.Equals(Nothing)  Then
						
					Me._UpToICInstructionByInstructionID = New ICInstruction()
					Me._UpToICInstructionByInstructionID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
					Me._UpToICInstructionByInstructionID.Query.Where(Me._UpToICInstructionByInstructionID.Query.InstructionID = Me.InstructionID)
					Me._UpToICInstructionByInstructionID.Query.Load()
				End If

				Return Me._UpToICInstructionByInstructionID
			End Get
			
            Set(ByVal value As ICInstruction)
				Me.RemovePreSave("UpToICInstructionByInstructionID")
				

				If value Is Nothing Then
				
					Me.InstructionID = Nothing
				
					Me._UpToICInstructionByInstructionID = Nothing
				Else
				
					Me.InstructionID = value.InstructionID
					
					Me._UpToICInstructionByInstructionID = value
					Me.SetPreSave("UpToICInstructionByInstructionID", Me._UpToICInstructionByInstructionID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPackageByPacakgeId - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges1
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPackageByPacakgeId As ICPackage
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPackageByPacakgeId Is Nothing _
						 AndAlso Not PacakgeId.Equals(Nothing)  Then
						
					Me._UpToICPackageByPacakgeId = New ICPackage()
					Me._UpToICPackageByPacakgeId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPackageByPacakgeId", Me._UpToICPackageByPacakgeId)
					Me._UpToICPackageByPacakgeId.Query.Where(Me._UpToICPackageByPacakgeId.Query.PackageID = Me.PacakgeId)
					Me._UpToICPackageByPacakgeId.Query.Load()
				End If

				Return Me._UpToICPackageByPacakgeId
			End Get
			
            Set(ByVal value As ICPackage)
				Me.RemovePreSave("UpToICPackageByPacakgeId")
				

				If value Is Nothing Then
				
					Me.PacakgeId = Nothing
				
					Me._UpToICPackageByPacakgeId = Nothing
				Else
				
					Me.PacakgeId = value.PackageID
					
					Me._UpToICPackageByPacakgeId = value
					Me.SetPreSave("UpToICPackageByPacakgeId", Me._UpToICPackageByPacakgeId)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPackageInvoiceByInvoiceId - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_PackageCharges_IC_PackageCharges2
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPackageInvoiceByInvoiceId As ICPackageInvoice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPackageInvoiceByInvoiceId Is Nothing _
						 AndAlso Not InvoiceId.Equals(Nothing)  Then
						
					Me._UpToICPackageInvoiceByInvoiceId = New ICPackageInvoice()
					Me._UpToICPackageInvoiceByInvoiceId.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPackageInvoiceByInvoiceId", Me._UpToICPackageInvoiceByInvoiceId)
					Me._UpToICPackageInvoiceByInvoiceId.Query.Where(Me._UpToICPackageInvoiceByInvoiceId.Query.InvoiceId = Me.InvoiceId)
					Me._UpToICPackageInvoiceByInvoiceId.Query.Load()
				End If

				Return Me._UpToICPackageInvoiceByInvoiceId
			End Get
			
            Set(ByVal value As ICPackageInvoice)
				Me.RemovePreSave("UpToICPackageInvoiceByInvoiceId")
				

				If value Is Nothing Then
				
					Me.InvoiceId = Nothing
				
					Me._UpToICPackageInvoiceByInvoiceId = Nothing
				Else
				
					Me.InvoiceId = value.InvoiceId
					
					Me._UpToICPackageInvoiceByInvoiceId = value
					Me.SetPreSave("UpToICPackageInvoiceByInvoiceId", Me._UpToICPackageInvoiceByInvoiceId)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICGlobalChargesByGlobalChargeId Is Nothing Then
				Me.GlobalChargeId = Me._UpToICGlobalChargesByGlobalChargeId.GlobalChargeID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICInstructionByInstructionID Is Nothing Then
				Me.InstructionID = Me._UpToICInstructionByInstructionID.InstructionID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICPackageByPacakgeId Is Nothing Then
				Me.PacakgeId = Me._UpToICPackageByPacakgeId.PackageID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICPackageInvoiceByInvoiceId Is Nothing Then
				Me.InvoiceId = Me._UpToICPackageInvoiceByInvoiceId.InvoiceId
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICPackageChargesMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeId, 0, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeId
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 19
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.AccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.BranchCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.Currency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.PaymentNatureCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.PaymentNatureCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ProductTypeCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.PacakgeId, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.PacakgeId
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeType, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeAmount, 8, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeAmount
			c.NumericPrecision = 18
			c.NumericScale = 3
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.InstructionID, 10, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.InstructionID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.GlobalChargeId, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.GlobalChargeId
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeFromDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeFromDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.ChargeToDate, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.ChargeToDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICPackageChargesMetadata.ColumnNames.InvoiceId, 14, GetType(System.Int64), esSystemType.Int64)	
			c.PropertyName = ICPackageChargesMetadata.PropertyNames.InvoiceId
			c.NumericPrecision = 19
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICPackageChargesMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ChargeId As String = "ChargeId"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const PacakgeId As String = "PacakgeId"
			 Public Const ChargeType As String = "ChargeType"
			 Public Const ChargeAmount As String = "ChargeAmount"
			 Public Const ChargeDate As String = "ChargeDate"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const GlobalChargeId As String = "GlobalChargeId"
			 Public Const ChargeFromDate As String = "ChargeFromDate"
			 Public Const ChargeToDate As String = "ChargeToDate"
			 Public Const InvoiceId As String = "InvoiceId"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ChargeId As String = "ChargeId"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const PacakgeId As String = "PacakgeId"
			 Public Const ChargeType As String = "ChargeType"
			 Public Const ChargeAmount As String = "ChargeAmount"
			 Public Const ChargeDate As String = "ChargeDate"
			 Public Const InstructionID As String = "InstructionID"
			 Public Const GlobalChargeId As String = "GlobalChargeId"
			 Public Const ChargeFromDate As String = "ChargeFromDate"
			 Public Const ChargeToDate As String = "ChargeToDate"
			 Public Const InvoiceId As String = "InvoiceId"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICPackageChargesMetadata)
			
				If ICPackageChargesMetadata.mapDelegates Is Nothing Then
					ICPackageChargesMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICPackageChargesMetadata._meta Is Nothing Then
					ICPackageChargesMetadata._meta = New ICPackageChargesMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ChargeId", new esTypeMap("bigint", "System.Int64"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PacakgeId", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChargeType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ChargeAmount", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("ChargeDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("InstructionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GlobalChargeId", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ChargeFromDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ChargeToDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("InvoiceId", new esTypeMap("bigint", "System.Int64"))			
				
				
				 
				meta.Source = "IC_PackageCharges"
				meta.Destination = "IC_PackageCharges"
				
				meta.spInsert = "proc_IC_PackageChargesInsert"
				meta.spUpdate = "proc_IC_PackageChargesUpdate"
				meta.spDelete = "proc_IC_PackageChargesDelete"
				meta.spLoadAll = "proc_IC_PackageChargesLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_PackageChargesLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICPackageChargesMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
