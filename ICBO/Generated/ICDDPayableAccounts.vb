
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/24/2014 12:31:41 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_DDPayableAccounts' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICDDPayableAccounts))> _
	<XmlType("ICDDPayableAccounts")> _	
	Partial Public Class ICDDPayableAccounts 
		Inherits esICDDPayableAccounts
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICDDPayableAccounts()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal dDPayableAccountID As System.Int32)
			Dim obj As New ICDDPayableAccounts()
			obj.DDPayableAccountID = dDPayableAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal dDPayableAccountID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICDDPayableAccounts()
			obj.DDPayableAccountID = dDPayableAccountID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICDDPayableAccountsCollection")> _
	Partial Public Class ICDDPayableAccountsCollection
		Inherits esICDDPayableAccountsCollection
		Implements IEnumerable(Of ICDDPayableAccounts)
	
		Public Function FindByPrimaryKey(ByVal dDPayableAccountID As System.Int32) As ICDDPayableAccounts
			Return MyBase.SingleOrDefault(Function(e) e.DDPayableAccountID.HasValue AndAlso e.DDPayableAccountID.Value = dDPayableAccountID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICDDPayableAccounts))> _
		Public Class ICDDPayableAccountsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICDDPayableAccountsCollection)
			
			Public Shared Widening Operator CType(packet As ICDDPayableAccountsCollectionWCFPacket) As ICDDPayableAccountsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICDDPayableAccountsCollection) As ICDDPayableAccountsCollectionWCFPacket
				Return New ICDDPayableAccountsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICDDPayableAccountsQuery 
		Inherits esICDDPayableAccountsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICDDPayableAccountsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICDDPayableAccountsQuery) As String
			Return ICDDPayableAccountsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICDDPayableAccountsQuery
			Return DirectCast(ICDDPayableAccountsQuery.SerializeHelper.FromXml(query, GetType(ICDDPayableAccountsQuery)), ICDDPayableAccountsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICDDPayableAccounts
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal dDPayableAccountID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDPayableAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDPayableAccountID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal dDPayableAccountID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(dDPayableAccountID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(dDPayableAccountID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal dDPayableAccountID As System.Int32) As Boolean
		
			Dim query As New ICDDPayableAccountsQuery()
			query.Where(query.DDPayableAccountID = dDPayableAccountID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal dDPayableAccountID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("DDPayableAccountID", dDPayableAccountID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_DDPayableAccounts.DDPayableAccountID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property DDPayableAccountID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.DDPayableAccountID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.DDPayableAccountID, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.DDPayableAccountID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICProductTypeByProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICProductTypeByProductTypeCode")
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.BankCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BankCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.BankCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.BankCode, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.BankCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountNumber, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountBranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountBranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountBranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountBranchCode, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountBranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountCurrency, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PreferenceOrder
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PreferenceOrder As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.PreferenceOrder)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICDDPayableAccountsMetadata.ColumnNames.PreferenceOrder, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PreferenceOrder)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountType, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountSeqNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountSeqNo As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountSeqNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountSeqNo, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountSeqNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountClientNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountClientNo As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountClientNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountClientNo, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountClientNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_DDPayableAccounts.PayableAccountProfitCentre
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PayableAccountProfitCentre As System.String
			Get
				Return MyBase.GetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountProfitCentre)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountProfitCentre, value) Then
					OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PayableAccountProfitCentre)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICProductTypeByProductTypeCode As ICProductType
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "DDPayableAccountID"
							Me.str().DDPayableAccountID = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "BankCode"
							Me.str().BankCode = CType(value, string)
												
						Case "PayableAccountNumber"
							Me.str().PayableAccountNumber = CType(value, string)
												
						Case "PayableAccountBranchCode"
							Me.str().PayableAccountBranchCode = CType(value, string)
												
						Case "PayableAccountCurrency"
							Me.str().PayableAccountCurrency = CType(value, string)
												
						Case "PreferenceOrder"
							Me.str().PreferenceOrder = CType(value, string)
												
						Case "PayableAccountType"
							Me.str().PayableAccountType = CType(value, string)
												
						Case "PayableAccountSeqNo"
							Me.str().PayableAccountSeqNo = CType(value, string)
												
						Case "PayableAccountClientNo"
							Me.str().PayableAccountClientNo = CType(value, string)
												
						Case "PayableAccountProfitCentre"
							Me.str().PayableAccountProfitCentre = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "DDPayableAccountID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.DDPayableAccountID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.DDPayableAccountID)
							End If
						
						Case "BankCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.BankCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.BankCode)
							End If
						
						Case "PreferenceOrder"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.PreferenceOrder = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICDDPayableAccountsMetadata.PropertyNames.PreferenceOrder)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICDDPayableAccounts)
				Me.entity = entity
			End Sub				
		
	
			Public Property DDPayableAccountID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.DDPayableAccountID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.DDPayableAccountID = Nothing
					Else
						entity.DDPayableAccountID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BankCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.BankCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BankCode = Nothing
					Else
						entity.BankCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountNumber = Nothing
					Else
						entity.PayableAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountBranchCode As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountBranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountBranchCode = Nothing
					Else
						entity.PayableAccountBranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountCurrency = Nothing
					Else
						entity.PayableAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PreferenceOrder As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.PreferenceOrder
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PreferenceOrder = Nothing
					Else
						entity.PreferenceOrder = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountType As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountType = Nothing
					Else
						entity.PayableAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountSeqNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountSeqNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountSeqNo = Nothing
					Else
						entity.PayableAccountSeqNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountClientNo As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountClientNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountClientNo = Nothing
					Else
						entity.PayableAccountClientNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property PayableAccountProfitCentre As System.String 
				Get
					Dim data_ As System.String = entity.PayableAccountProfitCentre
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PayableAccountProfitCentre = Nothing
					Else
						entity.PayableAccountProfitCentre = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICDDPayableAccounts
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDPayableAccountsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICDDPayableAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDPayableAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICDDPayableAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICDDPayableAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICDDPayableAccountsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICDDPayableAccountsCollection
		Inherits esEntityCollection(Of ICDDPayableAccounts)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICDDPayableAccountsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICDDPayableAccountsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICDDPayableAccountsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICDDPayableAccountsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICDDPayableAccountsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICDDPayableAccountsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICDDPayableAccountsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICDDPayableAccountsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICDDPayableAccountsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICDDPayableAccountsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICDDPayableAccountsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "DDPayableAccountID" 
					Return Me.DDPayableAccountID
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "BankCode" 
					Return Me.BankCode
				Case "PayableAccountNumber" 
					Return Me.PayableAccountNumber
				Case "PayableAccountBranchCode" 
					Return Me.PayableAccountBranchCode
				Case "PayableAccountCurrency" 
					Return Me.PayableAccountCurrency
				Case "PreferenceOrder" 
					Return Me.PreferenceOrder
				Case "PayableAccountType" 
					Return Me.PayableAccountType
				Case "PayableAccountSeqNo" 
					Return Me.PayableAccountSeqNo
				Case "PayableAccountClientNo" 
					Return Me.PayableAccountClientNo
				Case "PayableAccountProfitCentre" 
					Return Me.PayableAccountProfitCentre
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property DDPayableAccountID As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.DDPayableAccountID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BankCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.BankCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountBranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountBranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PreferenceOrder As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PreferenceOrder, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountSeqNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountSeqNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountClientNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountClientNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property PayableAccountProfitCentre As esQueryItem
			Get
				Return New esQueryItem(Me, ICDDPayableAccountsMetadata.ColumnNames.PayableAccountProfitCentre, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICDDPayableAccounts 
		Inherits esICDDPayableAccounts
		
	
		#Region "UpToICProductTypeByProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_DDPayableAccounts_IC_ProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICProductTypeByProductTypeCode As ICProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICProductTypeByProductTypeCode Is Nothing _
						 AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICProductTypeByProductTypeCode = New ICProductType()
					Me._UpToICProductTypeByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Where(Me._UpToICProductTypeByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Load()
				End If

				Return Me._UpToICProductTypeByProductTypeCode
			End Get
			
            Set(ByVal value As ICProductType)
				Me.RemovePreSave("UpToICProductTypeByProductTypeCode")
				

				If value Is Nothing Then
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICProductTypeByProductTypeCode = Nothing
				Else
				
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICProductTypeByProductTypeCode = value
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICDDPayableAccountsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.DDPayableAccountID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.DDPayableAccountID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.ProductTypeCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.BankCode, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.BankCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountNumber, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountBranchCode, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountBranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountCurrency, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PreferenceOrder, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PreferenceOrder
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountType, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountType
			c.CharacterMaxLength = 25
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountSeqNo, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountSeqNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountClientNo, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountClientNo
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICDDPayableAccountsMetadata.ColumnNames.PayableAccountProfitCentre, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICDDPayableAccountsMetadata.PropertyNames.PayableAccountProfitCentre
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICDDPayableAccountsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const DDPayableAccountID As String = "DDPayableAccountID"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const PreferenceOrder As String = "PreferenceOrder"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const PayableAccountSeqNo As String = "PayableAccountSeqNo"
			 Public Const PayableAccountClientNo As String = "PayableAccountClientNo"
			 Public Const PayableAccountProfitCentre As String = "PayableAccountProfitCentre"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const DDPayableAccountID As String = "DDPayableAccountID"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const BankCode As String = "BankCode"
			 Public Const PayableAccountNumber As String = "PayableAccountNumber"
			 Public Const PayableAccountBranchCode As String = "PayableAccountBranchCode"
			 Public Const PayableAccountCurrency As String = "PayableAccountCurrency"
			 Public Const PreferenceOrder As String = "PreferenceOrder"
			 Public Const PayableAccountType As String = "PayableAccountType"
			 Public Const PayableAccountSeqNo As String = "PayableAccountSeqNo"
			 Public Const PayableAccountClientNo As String = "PayableAccountClientNo"
			 Public Const PayableAccountProfitCentre As String = "PayableAccountProfitCentre"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICDDPayableAccountsMetadata)
			
				If ICDDPayableAccountsMetadata.mapDelegates Is Nothing Then
					ICDDPayableAccountsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICDDPayableAccountsMetadata._meta Is Nothing Then
					ICDDPayableAccountsMetadata._meta = New ICDDPayableAccountsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("DDPayableAccountID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BankCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PayableAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountBranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PreferenceOrder", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PayableAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountSeqNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountClientNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("PayableAccountProfitCentre", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_DDPayableAccounts"
				meta.Destination = "IC_DDPayableAccounts"
				
				meta.spInsert = "proc_IC_DDPayableAccountsInsert"
				meta.spUpdate = "proc_IC_DDPayableAccountsUpdate"
				meta.spDelete = "proc_IC_DDPayableAccountsDelete"
				meta.spLoadAll = "proc_IC_DDPayableAccountsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_DDPayableAccountsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICDDPayableAccountsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
