
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:56 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_HolidayManagement' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICHolidayManagement))> _
	<XmlType("ICHolidayManagement")> _	
	Partial Public Class ICHolidayManagement 
		Inherits esICHolidayManagement
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICHolidayManagement()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal holidayID As System.Int32)
			Dim obj As New ICHolidayManagement()
			obj.HolidayID = holidayID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal holidayID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICHolidayManagement()
			obj.HolidayID = holidayID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICHolidayManagementCollection")> _
	Partial Public Class ICHolidayManagementCollection
		Inherits esICHolidayManagementCollection
		Implements IEnumerable(Of ICHolidayManagement)
	
		Public Function FindByPrimaryKey(ByVal holidayID As System.Int32) As ICHolidayManagement
			Return MyBase.SingleOrDefault(Function(e) e.HolidayID.HasValue AndAlso e.HolidayID.Value = holidayID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICHolidayManagement))> _
		Public Class ICHolidayManagementCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICHolidayManagementCollection)
			
			Public Shared Widening Operator CType(packet As ICHolidayManagementCollectionWCFPacket) As ICHolidayManagementCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICHolidayManagementCollection) As ICHolidayManagementCollectionWCFPacket
				Return New ICHolidayManagementCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICHolidayManagementQuery 
		Inherits esICHolidayManagementQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICHolidayManagementQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICHolidayManagementQuery) As String
			Return ICHolidayManagementQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICHolidayManagementQuery
			Return DirectCast(ICHolidayManagementQuery.SerializeHelper.FromXml(query, GetType(ICHolidayManagementQuery)), ICHolidayManagementQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICHolidayManagement
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal holidayID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(holidayID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(holidayID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal holidayID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(holidayID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(holidayID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal holidayID As System.Int32) As Boolean
		
			Dim query As New ICHolidayManagementQuery()
			query.Where(query.HolidayID = holidayID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal holidayID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("HolidayID", holidayID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_HolidayManagement.HolidayID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICHolidayManagementMetadata.ColumnNames.HolidayID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICHolidayManagementMetadata.ColumnNames.HolidayID, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.HolidayName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayName As System.String
			Get
				Return MyBase.GetSystemString(ICHolidayManagementMetadata.ColumnNames.HolidayName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICHolidayManagementMetadata.ColumnNames.HolidayName, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.HolidayDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.HolidayDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.HolidayDate, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.HolidayDescription
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property HolidayDescription As System.String
			Get
				Return MyBase.GetSystemString(ICHolidayManagementMetadata.ColumnNames.HolidayDescription)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICHolidayManagementMetadata.ColumnNames.HolidayDescription, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayDescription)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICHolidayManagementMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICHolidayManagementMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICHolidayManagementMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICHolidayManagementMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_HolidayManagement.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICHolidayManagementMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "HolidayID"
							Me.str().HolidayID = CType(value, string)
												
						Case "HolidayName"
							Me.str().HolidayName = CType(value, string)
												
						Case "HolidayDate"
							Me.str().HolidayDate = CType(value, string)
												
						Case "HolidayDescription"
							Me.str().HolidayDescription = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "HolidayID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.HolidayID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayID)
							End If
						
						Case "HolidayDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.HolidayDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.HolidayDate)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreateDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICHolidayManagementMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICHolidayManagement)
				Me.entity = entity
			End Sub				
		
	
			Public Property HolidayID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.HolidayID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayID = Nothing
					Else
						entity.HolidayID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property HolidayName As System.String 
				Get
					Dim data_ As System.String = entity.HolidayName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayName = Nothing
					Else
						entity.HolidayName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property HolidayDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.HolidayDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayDate = Nothing
					Else
						entity.HolidayDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property HolidayDescription As System.String 
				Get
					Dim data_ As System.String = entity.HolidayDescription
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.HolidayDescription = Nothing
					Else
						entity.HolidayDescription = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICHolidayManagement
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICHolidayManagementMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICHolidayManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICHolidayManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICHolidayManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICHolidayManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICHolidayManagementQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICHolidayManagementCollection
		Inherits esEntityCollection(Of ICHolidayManagement)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICHolidayManagementMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICHolidayManagementCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICHolidayManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICHolidayManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICHolidayManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICHolidayManagementQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICHolidayManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICHolidayManagementQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICHolidayManagementQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICHolidayManagementQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICHolidayManagementMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "HolidayID" 
					Return Me.HolidayID
				Case "HolidayName" 
					Return Me.HolidayName
				Case "HolidayDate" 
					Return Me.HolidayDate
				Case "HolidayDescription" 
					Return Me.HolidayDescription
				Case "CreateDate" 
					Return Me.CreateDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property HolidayID As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.HolidayID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property HolidayName As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.HolidayName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property HolidayDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.HolidayDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property HolidayDescription As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.HolidayDescription, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICHolidayManagementMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICHolidayManagement 
		Inherits esICHolidayManagement
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICHolidayManagementMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.HolidayID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.HolidayID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.HolidayName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.HolidayName
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.HolidayDate, 2, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.HolidayDate
			c.CharacterMaxLength = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.HolidayDescription, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.HolidayDescription
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.CreateDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.CreatedBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.Creater, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICHolidayManagementMetadata.ColumnNames.CreationDate, 7, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICHolidayManagementMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICHolidayManagementMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const HolidayID As String = "HolidayID"
			 Public Const HolidayName As String = "HolidayName"
			 Public Const HolidayDate As String = "HolidayDate"
			 Public Const HolidayDescription As String = "HolidayDescription"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const HolidayID As String = "HolidayID"
			 Public Const HolidayName As String = "HolidayName"
			 Public Const HolidayDate As String = "HolidayDate"
			 Public Const HolidayDescription As String = "HolidayDescription"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICHolidayManagementMetadata)
			
				If ICHolidayManagementMetadata.mapDelegates Is Nothing Then
					ICHolidayManagementMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICHolidayManagementMetadata._meta Is Nothing Then
					ICHolidayManagementMetadata._meta = New ICHolidayManagementMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("HolidayID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("HolidayName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("HolidayDate", new esTypeMap("date", "System.DateTime"))
				meta.AddTypeMap("HolidayDescription", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_HolidayManagement"
				meta.Destination = "IC_HolidayManagement"
				
				meta.spInsert = "proc_IC_HolidayManagementInsert"
				meta.spUpdate = "proc_IC_HolidayManagementUpdate"
				meta.spDelete = "proc_IC_HolidayManagementDelete"
				meta.spLoadAll = "proc_IC_HolidayManagementLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_HolidayManagementLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICHolidayManagementMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
