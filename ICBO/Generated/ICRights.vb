
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Rights' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICRights))> _
	<XmlType("ICRights")> _	
	Partial Public Class ICRights 
		Inherits esICRights
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICRights()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal rightName As System.String, ByVal rightDetailName As System.String)
			Dim obj As New ICRights()
			obj.RightName = rightName
			obj.RightDetailName = rightDetailName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal rightName As System.String, ByVal rightDetailName As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICRights()
			obj.RightName = rightName
			obj.RightDetailName = rightDetailName
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICRightsCollection")> _
	Partial Public Class ICRightsCollection
		Inherits esICRightsCollection
		Implements IEnumerable(Of ICRights)
	
		Public Function FindByPrimaryKey(ByVal rightName As System.String, ByVal rightDetailName As System.String) As ICRights
			Return MyBase.SingleOrDefault(Function(e) e.RightName = rightName And e.RightDetailName = rightDetailName)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICRights))> _
		Public Class ICRightsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICRightsCollection)
			
			Public Shared Widening Operator CType(packet As ICRightsCollectionWCFPacket) As ICRightsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICRightsCollection) As ICRightsCollectionWCFPacket
				Return New ICRightsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICRightsQuery 
		Inherits esICRightsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICRightsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICRightsQuery) As String
			Return ICRightsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICRightsQuery
			Return DirectCast(ICRightsQuery.SerializeHelper.FromXml(query, GetType(ICRightsQuery)), ICRightsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICRights
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal rightName As System.String, ByVal rightDetailName As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(rightName, rightDetailName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(rightName, rightDetailName)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal rightName As System.String, ByVal rightDetailName As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(rightName, rightDetailName)
			Else
				Return LoadByPrimaryKeyStoredProcedure(rightName, rightDetailName)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal rightName As System.String, ByVal rightDetailName As System.String) As Boolean
		
			Dim query As New ICRightsQuery()
			query.Where(query.RightName = rightName And query.RightDetailName = rightDetailName)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal rightName As System.String, ByVal rightDetailName As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("RightName", rightName)
						parms.Add("RightDetailName", rightDetailName)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Rights.RightID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRightsMetadata.ColumnNames.RightID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRightsMetadata.ColumnNames.RightID, value) Then
					OnPropertyChanged(ICRightsMetadata.PropertyNames.RightID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Rights.RightName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightName As System.String
			Get
				Return MyBase.GetSystemString(ICRightsMetadata.ColumnNames.RightName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRightsMetadata.ColumnNames.RightName, value) Then
					OnPropertyChanged(ICRightsMetadata.PropertyNames.RightName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Rights.TabID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TabID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRightsMetadata.ColumnNames.TabID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRightsMetadata.ColumnNames.TabID, value) Then
					OnPropertyChanged(ICRightsMetadata.PropertyNames.TabID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Rights.RightDetailName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RightDetailName As System.String
			Get
				Return MyBase.GetSystemString(ICRightsMetadata.ColumnNames.RightDetailName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRightsMetadata.ColumnNames.RightDetailName, value) Then
					OnPropertyChanged(ICRightsMetadata.PropertyNames.RightDetailName)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RightID"
							Me.str().RightID = CType(value, string)
												
						Case "RightName"
							Me.str().RightName = CType(value, string)
												
						Case "TabID"
							Me.str().TabID = CType(value, string)
												
						Case "RightDetailName"
							Me.str().RightDetailName = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RightID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RightID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRightsMetadata.PropertyNames.RightID)
							End If
						
						Case "TabID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TabID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRightsMetadata.PropertyNames.TabID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICRights)
				Me.entity = entity
			End Sub				
		
	
			Public Property RightID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RightID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightID = Nothing
					Else
						entity.RightID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightName As System.String 
				Get
					Dim data_ As System.String = entity.RightName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightName = Nothing
					Else
						entity.RightName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TabID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TabID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TabID = Nothing
					Else
						entity.TabID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RightDetailName As System.String 
				Get
					Dim data_ As System.String = entity.RightDetailName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RightDetailName = Nothing
					Else
						entity.RightDetailName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICRights
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRightsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICRightsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICRightsCollection
		Inherits esEntityCollection(Of ICRights)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRightsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICRightsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICRightsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRightsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICRightsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICRightsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICRightsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICRightsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICRightsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICRightsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICRightsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RightID" 
					Return Me.RightID
				Case "RightName" 
					Return Me.RightName
				Case "TabID" 
					Return Me.TabID
				Case "RightDetailName" 
					Return Me.RightDetailName
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RightID As esQueryItem
			Get
				Return New esQueryItem(Me, ICRightsMetadata.ColumnNames.RightID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RightName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRightsMetadata.ColumnNames.RightName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TabID As esQueryItem
			Get
				Return New esQueryItem(Me, ICRightsMetadata.ColumnNames.TabID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RightDetailName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRightsMetadata.ColumnNames.RightDetailName, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICRights 
		Inherits esICRights
		
	
		#Region "ICRoleRightsCollectionByRightName - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICRoleRightsCollectionByRightName() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRights.ICRoleRightsCollectionByRightName_Delegate)
				map.PropertyName = "ICRoleRightsCollectionByRightName"
				map.MyColumnName = "RightName,RightDetailName"
				map.ParentColumnName = "RightName,RightDetailName"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICRoleRightsCollectionByRightName_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRightsQuery(data.NextAlias())
			
			Dim mee As ICRoleRightsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICRoleRightsQuery), New ICRoleRightsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RightName = mee.RightName And parent.RightDetailName = mee.RightDetailName)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_RoleRights_IC_Rights
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICRoleRightsCollectionByRightName As ICRoleRightsCollection 
		
			Get
				If Me._ICRoleRightsCollectionByRightName Is Nothing Then
					Me._ICRoleRightsCollectionByRightName = New ICRoleRightsCollection()
					Me._ICRoleRightsCollectionByRightName.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICRoleRightsCollectionByRightName", Me._ICRoleRightsCollectionByRightName)
				
					If Not Me.RightName.Equals(Nothing) AndAlso Not Me.RightDetailName.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICRoleRightsCollectionByRightName.Query.Where(Me._ICRoleRightsCollectionByRightName.Query.RightName = Me.RightName)
							Me._ICRoleRightsCollectionByRightName.Query.Where(Me._ICRoleRightsCollectionByRightName.Query.RightDetailName = Me.RightDetailName)
							Me._ICRoleRightsCollectionByRightName.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICRoleRightsCollectionByRightName.fks.Add(ICRoleRightsMetadata.ColumnNames.RightName, Me.RightName)
						Me._ICRoleRightsCollectionByRightName.fks.Add(ICRoleRightsMetadata.ColumnNames.RightDetailName, Me.RightDetailName)
					End If
				End If

				Return Me._ICRoleRightsCollectionByRightName
			End Get
			
			Set(ByVal value As ICRoleRightsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICRoleRightsCollectionByRightName Is Nothing Then

					Me.RemovePostSave("ICRoleRightsCollectionByRightName")
					Me._ICRoleRightsCollectionByRightName = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICRoleRightsCollectionByRightName As ICRoleRightsCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICRoleRightsCollectionByRightName"
					coll = Me.ICRoleRightsCollectionByRightName
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICRoleRightsCollectionByRightName", GetType(ICRoleRightsCollection), New ICRoleRights()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICRightsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICRightsMetadata.ColumnNames.RightID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRightsMetadata.PropertyNames.RightID
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRightsMetadata.ColumnNames.RightName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRightsMetadata.PropertyNames.RightName
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 250
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRightsMetadata.ColumnNames.TabID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRightsMetadata.PropertyNames.TabID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRightsMetadata.ColumnNames.RightDetailName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRightsMetadata.PropertyNames.RightDetailName
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICRightsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RightID As String = "RightID"
			 Public Const RightName As String = "RightName"
			 Public Const TabID As String = "TabID"
			 Public Const RightDetailName As String = "RightDetailName"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RightID As String = "RightID"
			 Public Const RightName As String = "RightName"
			 Public Const TabID As String = "TabID"
			 Public Const RightDetailName As String = "RightDetailName"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICRightsMetadata)
			
				If ICRightsMetadata.mapDelegates Is Nothing Then
					ICRightsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICRightsMetadata._meta Is Nothing Then
					ICRightsMetadata._meta = New ICRightsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RightID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RightName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TabID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RightDetailName", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_Rights"
				meta.Destination = "IC_Rights"
				
				meta.spInsert = "proc_IC_RightsInsert"
				meta.spUpdate = "proc_IC_RightsUpdate"
				meta.spDelete = "proc_IC_RightsDelete"
				meta.spLoadAll = "proc_IC_RightsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_RightsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICRightsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
