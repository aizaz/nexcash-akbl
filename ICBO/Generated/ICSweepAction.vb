
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 3/4/2014 10:53:54 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_SweepAction' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICSweepAction))> _
	<XmlType("ICSweepAction")> _	
	Partial Public Class ICSweepAction 
		Inherits esICSweepAction
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICSweepAction()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal sweepActionID As System.Int32)
			Dim obj As New ICSweepAction()
			obj.SweepActionID = sweepActionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal sweepActionID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICSweepAction()
			obj.SweepActionID = sweepActionID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICSweepActionCollection")> _
	Partial Public Class ICSweepActionCollection
		Inherits esICSweepActionCollection
		Implements IEnumerable(Of ICSweepAction)
	
		Public Function FindByPrimaryKey(ByVal sweepActionID As System.Int32) As ICSweepAction
			Return MyBase.SingleOrDefault(Function(e) e.SweepActionID.HasValue AndAlso e.SweepActionID.Value = sweepActionID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICSweepAction))> _
		Public Class ICSweepActionCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICSweepActionCollection)
			
			Public Shared Widening Operator CType(packet As ICSweepActionCollectionWCFPacket) As ICSweepActionCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICSweepActionCollection) As ICSweepActionCollectionWCFPacket
				Return New ICSweepActionCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICSweepActionQuery 
		Inherits esICSweepActionQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICSweepActionQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICSweepActionQuery) As String
			Return ICSweepActionQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICSweepActionQuery
			Return DirectCast(ICSweepActionQuery.SerializeHelper.FromXml(query, GetType(ICSweepActionQuery)), ICSweepActionQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICSweepAction
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal sweepActionID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(sweepActionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(sweepActionID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal sweepActionID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(sweepActionID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(sweepActionID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal sweepActionID As System.Int32) As Boolean
		
			Dim query As New ICSweepActionQuery()
			query.Where(query.SweepActionID = sweepActionID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal sweepActionID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SweepActionID", sweepActionID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionMetadata.ColumnNames.SweepActionID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionMetadata.ColumnNames.SweepActionID, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.FromAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountNo, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.FromAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.FromAccountBrCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountBrCode As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountBrCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountBrCode, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.FromAccountBrCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.FromAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountCurrency, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.FromAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.ToAccountNo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNo As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountNo)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountNo, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.ToAccountNo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.ToAccountBrCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountBrCode As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountBrCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountBrCode, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.ToAccountBrCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.ToAccountCurrency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountCurrency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountCurrency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountCurrency, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.ToAccountCurrency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionCreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionCreationDate As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionCreationDate)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionCreationDate, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionCreationDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionCreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionCreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionMetadata.ColumnNames.SweepActionCreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionMetadata.ColumnNames.SweepActionCreatedBy, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionCreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.AmountPerInstruction
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmountPerInstruction As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICSweepActionMetadata.ColumnNames.AmountPerInstruction)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICSweepActionMetadata.ColumnNames.AmountPerInstruction, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.AmountPerInstruction)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.FromAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.FromAccountType, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.FromAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.ToAccountType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.ToAccountType, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.ToAccountType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.IsProcessed
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsProcessed As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsProcessed)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsProcessed, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsProcessed)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.IsActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.IsApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICSweepActionMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.CurrentStatus
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CurrentStatus As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.CurrentStatus)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.CurrentStatus, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.CurrentStatus)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionFromDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionFromDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionFromDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionFromDate, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionFromDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionToDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionToDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionToDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionToDate, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionToDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionFrequency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionFrequency As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionFrequency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionFrequency, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionFrequency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionTitle
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionTitle As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionTitle)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionTitle, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionTitle)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionTime
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionTime As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionTime)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSweepActionMetadata.ColumnNames.SweepActionTime, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionTime)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.SweepActionDayType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SweepActionDayType As System.String
			Get
				Return MyBase.GetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionDayType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSweepActionMetadata.ColumnNames.SweepActionDayType, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionDayType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.TotalInstructions
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property TotalInstructions As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionMetadata.ColumnNames.TotalInstructions)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionMetadata.ColumnNames.TotalInstructions, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.TotalInstructions)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.GroupCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property GroupCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionMetadata.ColumnNames.GroupCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionMetadata.ColumnNames.GroupCode, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.GroupCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SweepAction.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSweepActionMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSweepActionMetadata.ColumnNames.CompanyCode, value) Then
					OnPropertyChanged(ICSweepActionMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SweepActionID"
							Me.str().SweepActionID = CType(value, string)
												
						Case "FromAccountNo"
							Me.str().FromAccountNo = CType(value, string)
												
						Case "FromAccountBrCode"
							Me.str().FromAccountBrCode = CType(value, string)
												
						Case "FromAccountCurrency"
							Me.str().FromAccountCurrency = CType(value, string)
												
						Case "ToAccountNo"
							Me.str().ToAccountNo = CType(value, string)
												
						Case "ToAccountBrCode"
							Me.str().ToAccountBrCode = CType(value, string)
												
						Case "ToAccountCurrency"
							Me.str().ToAccountCurrency = CType(value, string)
												
						Case "SweepActionCreationDate"
							Me.str().SweepActionCreationDate = CType(value, string)
												
						Case "SweepActionCreatedBy"
							Me.str().SweepActionCreatedBy = CType(value, string)
												
						Case "AmountPerInstruction"
							Me.str().AmountPerInstruction = CType(value, string)
												
						Case "FromAccountType"
							Me.str().FromAccountType = CType(value, string)
												
						Case "ToAccountType"
							Me.str().ToAccountType = CType(value, string)
												
						Case "IsProcessed"
							Me.str().IsProcessed = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "CurrentStatus"
							Me.str().CurrentStatus = CType(value, string)
												
						Case "SweepActionFromDate"
							Me.str().SweepActionFromDate = CType(value, string)
												
						Case "SweepActionToDate"
							Me.str().SweepActionToDate = CType(value, string)
												
						Case "SweepActionFrequency"
							Me.str().SweepActionFrequency = CType(value, string)
												
						Case "SweepActionTitle"
							Me.str().SweepActionTitle = CType(value, string)
												
						Case "SweepActionTime"
							Me.str().SweepActionTime = CType(value, string)
												
						Case "SweepActionDayType"
							Me.str().SweepActionDayType = CType(value, string)
												
						Case "TotalInstructions"
							Me.str().TotalInstructions = CType(value, string)
												
						Case "GroupCode"
							Me.str().GroupCode = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SweepActionID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepActionID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionID)
							End If
						
						Case "SweepActionCreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SweepActionCreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionCreatedBy)
							End If
						
						Case "AmountPerInstruction"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.AmountPerInstruction = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.AmountPerInstruction)
							End If
						
						Case "IsProcessed"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsProcessed = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsProcessed)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsActive)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.IsApproved)
							End If
						
						Case "SweepActionFromDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionFromDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionFromDate)
							End If
						
						Case "SweepActionToDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionToDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionToDate)
							End If
						
						Case "SweepActionTime"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.SweepActionTime = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.SweepActionTime)
							End If
						
						Case "TotalInstructions"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.TotalInstructions = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.TotalInstructions)
							End If
						
						Case "GroupCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.GroupCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.GroupCode)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSweepActionMetadata.PropertyNames.CompanyCode)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICSweepAction)
				Me.entity = entity
			End Sub				
		
	
			Public Property SweepActionID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepActionID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionID = Nothing
					Else
						entity.SweepActionID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNo = Nothing
					Else
						entity.FromAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountBrCode As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountBrCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountBrCode = Nothing
					Else
						entity.FromAccountBrCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountCurrency = Nothing
					Else
						entity.FromAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNo As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNo
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNo = Nothing
					Else
						entity.ToAccountNo = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountBrCode As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountBrCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountBrCode = Nothing
					Else
						entity.ToAccountBrCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountCurrency As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountCurrency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountCurrency = Nothing
					Else
						entity.ToAccountCurrency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionCreationDate As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionCreationDate
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionCreationDate = Nothing
					Else
						entity.SweepActionCreationDate = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionCreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SweepActionCreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionCreatedBy = Nothing
					Else
						entity.SweepActionCreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AmountPerInstruction As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.AmountPerInstruction
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmountPerInstruction = Nothing
					Else
						entity.AmountPerInstruction = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountType As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountType = Nothing
					Else
						entity.FromAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountType As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountType = Nothing
					Else
						entity.ToAccountType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsProcessed As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsProcessed
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsProcessed = Nothing
					Else
						entity.IsProcessed = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CurrentStatus As System.String 
				Get
					Dim data_ As System.String = entity.CurrentStatus
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CurrentStatus = Nothing
					Else
						entity.CurrentStatus = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionFromDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionFromDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionFromDate = Nothing
					Else
						entity.SweepActionFromDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionToDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionToDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionToDate = Nothing
					Else
						entity.SweepActionToDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionFrequency As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionFrequency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionFrequency = Nothing
					Else
						entity.SweepActionFrequency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionTitle As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionTitle
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionTitle = Nothing
					Else
						entity.SweepActionTitle = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionTime As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.SweepActionTime
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionTime = Nothing
					Else
						entity.SweepActionTime = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property SweepActionDayType As System.String 
				Get
					Dim data_ As System.String = entity.SweepActionDayType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SweepActionDayType = Nothing
					Else
						entity.SweepActionDayType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property TotalInstructions As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.TotalInstructions
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.TotalInstructions = Nothing
					Else
						entity.TotalInstructions = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property GroupCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.GroupCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.GroupCode = Nothing
					Else
						entity.GroupCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICSweepAction
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSweepActionMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICSweepActionQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSweepActionQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICSweepActionQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICSweepActionQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICSweepActionQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICSweepActionCollection
		Inherits esEntityCollection(Of ICSweepAction)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSweepActionMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICSweepActionCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICSweepActionQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSweepActionQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICSweepActionQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICSweepActionQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICSweepActionQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICSweepActionQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICSweepActionQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICSweepActionQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICSweepActionMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SweepActionID" 
					Return Me.SweepActionID
				Case "FromAccountNo" 
					Return Me.FromAccountNo
				Case "FromAccountBrCode" 
					Return Me.FromAccountBrCode
				Case "FromAccountCurrency" 
					Return Me.FromAccountCurrency
				Case "ToAccountNo" 
					Return Me.ToAccountNo
				Case "ToAccountBrCode" 
					Return Me.ToAccountBrCode
				Case "ToAccountCurrency" 
					Return Me.ToAccountCurrency
				Case "SweepActionCreationDate" 
					Return Me.SweepActionCreationDate
				Case "SweepActionCreatedBy" 
					Return Me.SweepActionCreatedBy
				Case "AmountPerInstruction" 
					Return Me.AmountPerInstruction
				Case "FromAccountType" 
					Return Me.FromAccountType
				Case "ToAccountType" 
					Return Me.ToAccountType
				Case "IsProcessed" 
					Return Me.IsProcessed
				Case "IsActive" 
					Return Me.IsActive
				Case "IsApproved" 
					Return Me.IsApproved
				Case "CurrentStatus" 
					Return Me.CurrentStatus
				Case "SweepActionFromDate" 
					Return Me.SweepActionFromDate
				Case "SweepActionToDate" 
					Return Me.SweepActionToDate
				Case "SweepActionFrequency" 
					Return Me.SweepActionFrequency
				Case "SweepActionTitle" 
					Return Me.SweepActionTitle
				Case "SweepActionTime" 
					Return Me.SweepActionTime
				Case "SweepActionDayType" 
					Return Me.SweepActionDayType
				Case "TotalInstructions" 
					Return Me.TotalInstructions
				Case "GroupCode" 
					Return Me.GroupCode
				Case "CompanyCode" 
					Return Me.CompanyCode
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SweepActionID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.FromAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountBrCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.FromAccountBrCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.FromAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNo As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.ToAccountNo, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountBrCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.ToAccountBrCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountCurrency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.ToAccountCurrency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionCreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionCreationDate, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionCreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionCreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AmountPerInstruction As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.AmountPerInstruction, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.FromAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.ToAccountType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsProcessed As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.IsProcessed, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CurrentStatus As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.CurrentStatus, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionFromDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionFromDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionToDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionToDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionFrequency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionFrequency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionTitle As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionTitle, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionTime As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionTime, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property SweepActionDayType As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.SweepActionDayType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property TotalInstructions As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.TotalInstructions, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property GroupCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.GroupCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSweepActionMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICSweepAction 
		Inherits esICSweepAction
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICSweepActionMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.FromAccountNo, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.FromAccountNo
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.FromAccountBrCode, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.FromAccountBrCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.FromAccountCurrency, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.FromAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.ToAccountNo, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.ToAccountNo
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.ToAccountBrCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.ToAccountBrCode
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.ToAccountCurrency, 6, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.ToAccountCurrency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionCreationDate, 7, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionCreationDate
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionCreatedBy, 8, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionCreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.AmountPerInstruction, 9, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.AmountPerInstruction
			c.NumericPrecision = 18
			c.NumericScale = 2
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.FromAccountType, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.FromAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.ToAccountType, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.ToAccountType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.IsProcessed, 12, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.IsProcessed
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.IsActive, 13, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.IsActive
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.IsApproved, 14, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.IsApproved
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.CurrentStatus, 15, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.CurrentStatus
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionFromDate, 16, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionFromDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionToDate, 17, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionToDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionFrequency, 18, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionFrequency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionTitle, 19, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionTitle
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionTime, 20, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionTime
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.SweepActionDayType, 21, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.SweepActionDayType
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.TotalInstructions, 22, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.TotalInstructions
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.GroupCode, 23, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.GroupCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSweepActionMetadata.ColumnNames.CompanyCode, 24, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSweepActionMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICSweepActionMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SweepActionID As String = "SweepActionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBrCode As String = "FromAccountBrCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBrCode As String = "ToAccountBrCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const SweepActionCreationDate As String = "SweepActionCreationDate"
			 Public Const SweepActionCreatedBy As String = "SweepActionCreatedBy"
			 Public Const AmountPerInstruction As String = "AmountPerInstruction"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CurrentStatus As String = "CurrentStatus"
			 Public Const SweepActionFromDate As String = "SweepActionFromDate"
			 Public Const SweepActionToDate As String = "SweepActionToDate"
			 Public Const SweepActionFrequency As String = "SweepActionFrequency"
			 Public Const SweepActionTitle As String = "SweepActionTitle"
			 Public Const SweepActionTime As String = "SweepActionTime"
			 Public Const SweepActionDayType As String = "SweepActionDayType"
			 Public Const TotalInstructions As String = "TotalInstructions"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyCode As String = "CompanyCode"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SweepActionID As String = "SweepActionID"
			 Public Const FromAccountNo As String = "FromAccountNo"
			 Public Const FromAccountBrCode As String = "FromAccountBrCode"
			 Public Const FromAccountCurrency As String = "FromAccountCurrency"
			 Public Const ToAccountNo As String = "ToAccountNo"
			 Public Const ToAccountBrCode As String = "ToAccountBrCode"
			 Public Const ToAccountCurrency As String = "ToAccountCurrency"
			 Public Const SweepActionCreationDate As String = "SweepActionCreationDate"
			 Public Const SweepActionCreatedBy As String = "SweepActionCreatedBy"
			 Public Const AmountPerInstruction As String = "AmountPerInstruction"
			 Public Const FromAccountType As String = "FromAccountType"
			 Public Const ToAccountType As String = "ToAccountType"
			 Public Const IsProcessed As String = "IsProcessed"
			 Public Const IsActive As String = "IsActive"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const CurrentStatus As String = "CurrentStatus"
			 Public Const SweepActionFromDate As String = "SweepActionFromDate"
			 Public Const SweepActionToDate As String = "SweepActionToDate"
			 Public Const SweepActionFrequency As String = "SweepActionFrequency"
			 Public Const SweepActionTitle As String = "SweepActionTitle"
			 Public Const SweepActionTime As String = "SweepActionTime"
			 Public Const SweepActionDayType As String = "SweepActionDayType"
			 Public Const TotalInstructions As String = "TotalInstructions"
			 Public Const GroupCode As String = "GroupCode"
			 Public Const CompanyCode As String = "CompanyCode"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICSweepActionMetadata)
			
				If ICSweepActionMetadata.mapDelegates Is Nothing Then
					ICSweepActionMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICSweepActionMetadata._meta Is Nothing Then
					ICSweepActionMetadata._meta = New ICSweepActionMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SweepActionID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FromAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountBrCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FromAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNo", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountBrCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountCurrency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionCreationDate", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionCreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AmountPerInstruction", new esTypeMap("decimal", "System.Decimal"))
				meta.AddTypeMap("FromAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsProcessed", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CurrentStatus", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionFromDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionToDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionFrequency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionTitle", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("SweepActionTime", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("SweepActionDayType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("TotalInstructions", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("GroupCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_SweepAction"
				meta.Destination = "IC_SweepAction"
				
				meta.spInsert = "proc_IC_SweepActionInsert"
				meta.spUpdate = "proc_IC_SweepActionUpdate"
				meta.spDelete = "proc_IC_SweepActionDelete"
				meta.spLoadAll = "proc_IC_SweepActionLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_SweepActionLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICSweepActionMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
