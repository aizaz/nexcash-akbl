
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_SubSet' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICSubSet))> _
	<XmlType("ICSubSet")> _	
	Partial Public Class ICSubSet 
		Inherits esICSubSet
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICSubSet()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal subSetID As System.Int32)
			Dim obj As New ICSubSet()
			obj.SubSetID = subSetID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal subSetID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICSubSet()
			obj.SubSetID = subSetID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICSubSetCollection")> _
	Partial Public Class ICSubSetCollection
		Inherits esICSubSetCollection
		Implements IEnumerable(Of ICSubSet)
	
		Public Function FindByPrimaryKey(ByVal subSetID As System.Int32) As ICSubSet
			Return MyBase.SingleOrDefault(Function(e) e.SubSetID.HasValue AndAlso e.SubSetID.Value = subSetID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICSubSet))> _
		Public Class ICSubSetCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICSubSetCollection)
			
			Public Shared Widening Operator CType(packet As ICSubSetCollectionWCFPacket) As ICSubSetCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICSubSetCollection) As ICSubSetCollectionWCFPacket
				Return New ICSubSetCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICSubSetQuery 
		Inherits esICSubSetQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICSubSetQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICSubSetQuery) As String
			Return ICSubSetQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICSubSetQuery
			Return DirectCast(ICSubSetQuery.SerializeHelper.FromXml(query, GetType(ICSubSetQuery)), ICSubSetQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICSubSet
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal subSetID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(subSetID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(subSetID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal subSetID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(subSetID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(subSetID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal subSetID As System.Int32) As Boolean
		
			Dim query As New ICSubSetQuery()
			query.Where(query.SubSetID = subSetID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal subSetID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("SubSetID", subSetID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_SubSet.SubSetID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubSetID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.SubSetID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.SubSetID, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICSubSetMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSubSetMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICSubSetMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSubSetMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICSubSetMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSubSetMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.MasterSeriesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MasterSeriesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.MasterSeriesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.MasterSeriesID, value) Then
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
					Me.OnPropertyChanged("UpToICMasterSeriesByMasterSeriesID")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.MasterSeriesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.OfficeID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property OfficeID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.OfficeID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.OfficeID, value) Then
					Me._UpToICOfficeByOfficeID = Nothing
					Me.OnPropertyChanged("UpToICOfficeByOfficeID")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.OfficeID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.SubSetFrom
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubSetFrom As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICSubSetMetadata.ColumnNames.SubSetFrom)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICSubSetMetadata.ColumnNames.SubSetFrom, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetFrom)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.SubSetTo
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property SubSetTo As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICSubSetMetadata.ColumnNames.SubSetTo)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICSubSetMetadata.ColumnNames.SubSetTo, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetTo)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSubSetMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSubSetMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.Remarks
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Remarks As System.String
			Get
				Return MyBase.GetSystemString(ICSubSetMetadata.ColumnNames.Remarks)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICSubSetMetadata.ColumnNames.Remarks, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.Remarks)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICSubSetMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICSubSetMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_SubSet.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICSubSetMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICSubSetMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICOfficeByOfficeID As ICOffice
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "SubSetID"
							Me.str().SubSetID = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "MasterSeriesID"
							Me.str().MasterSeriesID = CType(value, string)
												
						Case "OfficeID"
							Me.str().OfficeID = CType(value, string)
												
						Case "SubSetFrom"
							Me.str().SubSetFrom = CType(value, string)
												
						Case "SubSetTo"
							Me.str().SubSetTo = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "Remarks"
							Me.str().Remarks = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "SubSetID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.SubSetID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "MasterSeriesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.MasterSeriesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.MasterSeriesID)
							End If
						
						Case "OfficeID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.OfficeID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.OfficeID)
							End If
						
						Case "SubSetFrom"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubSetFrom = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetFrom)
							End If
						
						Case "SubSetTo"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.SubSetTo = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.SubSetTo)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICSubSetMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICSubSet)
				Me.entity = entity
			End Sub				
		
	
			Public Property SubSetID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.SubSetID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubSetID = Nothing
					Else
						entity.SubSetID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property MasterSeriesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.MasterSeriesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MasterSeriesID = Nothing
					Else
						entity.MasterSeriesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property OfficeID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.OfficeID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.OfficeID = Nothing
					Else
						entity.OfficeID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubSetFrom As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubSetFrom
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubSetFrom = Nothing
					Else
						entity.SubSetFrom = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property SubSetTo As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.SubSetTo
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.SubSetTo = Nothing
					Else
						entity.SubSetTo = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Remarks As System.String 
				Get
					Dim data_ As System.String = entity.Remarks
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Remarks = Nothing
					Else
						entity.Remarks = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICSubSet
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSubSetMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICSubSetQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSubSetQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICSubSetQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICSubSetQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICSubSetQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICSubSetCollection
		Inherits esEntityCollection(Of ICSubSet)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICSubSetMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICSubSetCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICSubSetQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICSubSetQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICSubSetQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICSubSetQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICSubSetQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICSubSetQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICSubSetQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICSubSetQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICSubSetMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "SubSetID" 
					Return Me.SubSetID
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "MasterSeriesID" 
					Return Me.MasterSeriesID
				Case "OfficeID" 
					Return Me.OfficeID
				Case "SubSetFrom" 
					Return Me.SubSetFrom
				Case "SubSetTo" 
					Return Me.SubSetTo
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "Remarks" 
					Return Me.Remarks
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property SubSetID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.SubSetID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property MasterSeriesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.MasterSeriesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property OfficeID As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.OfficeID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property SubSetFrom As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.SubSetFrom, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property SubSetTo As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.SubSetTo, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Remarks As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.Remarks, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICSubSetMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICSubSet 
		Inherits esICSubSet
		
	
		#Region "ICInstrumentsCollectionBySubSetID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICInstrumentsCollectionBySubSetID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICSubSet.ICInstrumentsCollectionBySubSetID_Delegate)
				map.PropertyName = "ICInstrumentsCollectionBySubSetID"
				map.MyColumnName = "SubSetID"
				map.ParentColumnName = "SubSetID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICInstrumentsCollectionBySubSetID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICSubSetQuery(data.NextAlias())
			
			Dim mee As ICInstrumentsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICInstrumentsQuery), New ICInstrumentsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.SubSetID = mee.SubSetID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_Instruments_IC_SubSet
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICInstrumentsCollectionBySubSetID As ICInstrumentsCollection 
		
			Get
				If Me._ICInstrumentsCollectionBySubSetID Is Nothing Then
					Me._ICInstrumentsCollectionBySubSetID = New ICInstrumentsCollection()
					Me._ICInstrumentsCollectionBySubSetID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICInstrumentsCollectionBySubSetID", Me._ICInstrumentsCollectionBySubSetID)
				
					If Not Me.SubSetID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICInstrumentsCollectionBySubSetID.Query.Where(Me._ICInstrumentsCollectionBySubSetID.Query.SubSetID = Me.SubSetID)
							Me._ICInstrumentsCollectionBySubSetID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICInstrumentsCollectionBySubSetID.fks.Add(ICInstrumentsMetadata.ColumnNames.SubSetID, Me.SubSetID)
					End If
				End If

				Return Me._ICInstrumentsCollectionBySubSetID
			End Get
			
			Set(ByVal value As ICInstrumentsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICInstrumentsCollectionBySubSetID Is Nothing Then

					Me.RemovePostSave("ICInstrumentsCollectionBySubSetID")
					Me._ICInstrumentsCollectionBySubSetID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICInstrumentsCollectionBySubSetID As ICInstrumentsCollection
		#End Region

		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSet_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSet_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICMasterSeriesByMasterSeriesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSet_IC_MasterSeries
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICMasterSeriesByMasterSeriesID As ICMasterSeries
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICMasterSeriesByMasterSeriesID Is Nothing _
						 AndAlso Not MasterSeriesID.Equals(Nothing)  Then
						
					Me._UpToICMasterSeriesByMasterSeriesID = New ICMasterSeries()
					Me._UpToICMasterSeriesByMasterSeriesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Where(Me._UpToICMasterSeriesByMasterSeriesID.Query.MasterSeriesID = Me.MasterSeriesID)
					Me._UpToICMasterSeriesByMasterSeriesID.Query.Load()
				End If

				Return Me._UpToICMasterSeriesByMasterSeriesID
			End Get
			
            Set(ByVal value As ICMasterSeries)
				Me.RemovePreSave("UpToICMasterSeriesByMasterSeriesID")
				

				If value Is Nothing Then
				
					Me.MasterSeriesID = Nothing
				
					Me._UpToICMasterSeriesByMasterSeriesID = Nothing
				Else
				
					Me.MasterSeriesID = value.MasterSeriesID
					
					Me._UpToICMasterSeriesByMasterSeriesID = value
					Me.SetPreSave("UpToICMasterSeriesByMasterSeriesID", Me._UpToICMasterSeriesByMasterSeriesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICOfficeByOfficeID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSet_IC_Office
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICOfficeByOfficeID As ICOffice
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICOfficeByOfficeID Is Nothing _
						 AndAlso Not OfficeID.Equals(Nothing)  Then
						
					Me._UpToICOfficeByOfficeID = New ICOffice()
					Me._UpToICOfficeByOfficeID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
					Me._UpToICOfficeByOfficeID.Query.Where(Me._UpToICOfficeByOfficeID.Query.OfficeID = Me.OfficeID)
					Me._UpToICOfficeByOfficeID.Query.Load()
				End If

				Return Me._UpToICOfficeByOfficeID
			End Get
			
            Set(ByVal value As ICOffice)
				Me.RemovePreSave("UpToICOfficeByOfficeID")
				

				If value Is Nothing Then
				
					Me.OfficeID = Nothing
				
					Me._UpToICOfficeByOfficeID = Nothing
				Else
				
					Me.OfficeID = value.OfficeID
					
					Me._UpToICOfficeByOfficeID = value
					Me.SetPreSave("UpToICOfficeByOfficeID", Me._UpToICOfficeByOfficeID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_SubSet_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICInstrumentsCollectionBySubSetID"
					coll = Me.ICInstrumentsCollectionBySubSetID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICInstrumentsCollectionBySubSetID", GetType(ICInstrumentsCollection), New ICInstruments()))
			Return props
			
		End Function	
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICMasterSeriesByMasterSeriesID Is Nothing Then
				Me.MasterSeriesID = Me._UpToICMasterSeriesByMasterSeriesID.MasterSeriesID
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICOfficeByOfficeID Is Nothing Then
				Me.OfficeID = Me._UpToICOfficeByOfficeID.OfficeID
			End If
			
		End Sub
		
		''' <summary>
		''' Called by ApplyPostSaveKeys 
		''' </summary>
		''' <param name="coll">The collection to enumerate over</param>
		''' <param name="key">"The column name</param>
		''' <param name="value">The column value</param>
		Private Sub Apply(coll As esEntityCollectionBase, key As String, value As Object)
			For Each obj As esEntity In coll
				If obj.es.IsAdded Then
					obj.SetProperty(key, value)
				End If
			Next
		End Sub
		
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PostSave.
		''' </summary>
		Protected Overrides Sub ApplyPostSaveKeys()
		
			If Not Me._ICInstrumentsCollectionBySubSetID Is Nothing Then
				Apply(Me._ICInstrumentsCollectionBySubSetID, "SubSetID", Me.SubSetID)
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICSubSetMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.SubSetID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.SubSetID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.CompanyCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.AccountNumber, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.AccountNumber
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.BranchCode, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.BranchCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.Currency, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.Currency
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.MasterSeriesID, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.MasterSeriesID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.OfficeID, 6, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.OfficeID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.SubSetFrom, 7, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.SubSetFrom
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.SubSetTo, 8, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.SubSetTo
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.CreatedBy, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.CreatedDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.Remarks, 11, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.Remarks
			c.CharacterMaxLength = 250
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.Creater, 12, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICSubSetMetadata.ColumnNames.CreationDate, 13, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICSubSetMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICSubSetMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const SubSetID As String = "SubSetID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const SubSetFrom As String = "SubSetFrom"
			 Public Const SubSetTo As String = "SubSetTo"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Remarks As String = "Remarks"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const SubSetID As String = "SubSetID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const MasterSeriesID As String = "MasterSeriesID"
			 Public Const OfficeID As String = "OfficeID"
			 Public Const SubSetFrom As String = "SubSetFrom"
			 Public Const SubSetTo As String = "SubSetTo"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const Remarks As String = "Remarks"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICSubSetMetadata)
			
				If ICSubSetMetadata.mapDelegates Is Nothing Then
					ICSubSetMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICSubSetMetadata._meta Is Nothing Then
					ICSubSetMetadata._meta = New ICSubSetMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("SubSetID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("MasterSeriesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("OfficeID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("SubSetFrom", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("SubSetTo", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Remarks", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_SubSet"
				meta.Destination = "IC_SubSet"
				
				meta.spInsert = "proc_IC_SubSetInsert"
				meta.spUpdate = "proc_IC_SubSetUpdate"
				meta.spDelete = "proc_IC_SubSetDelete"
				meta.spLoadAll = "proc_IC_SubSetLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_SubSetLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICSubSetMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
