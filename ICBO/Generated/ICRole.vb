
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:00 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_Role' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICRole))> _
	<XmlType("ICRole")> _	
	Partial Public Class ICRole 
		Inherits esICRole
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICRole()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal roleID As System.Int32)
			Dim obj As New ICRole()
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal roleID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICRole()
			obj.RoleID = roleID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICRoleCollection")> _
	Partial Public Class ICRoleCollection
		Inherits esICRoleCollection
		Implements IEnumerable(Of ICRole)
	
		Public Function FindByPrimaryKey(ByVal roleID As System.Int32) As ICRole
			Return MyBase.SingleOrDefault(Function(e) e.RoleID.HasValue AndAlso e.RoleID.Value = roleID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICRole))> _
		Public Class ICRoleCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICRoleCollection)
			
			Public Shared Widening Operator CType(packet As ICRoleCollectionWCFPacket) As ICRoleCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICRoleCollection) As ICRoleCollectionWCFPacket
				Return New ICRoleCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICRoleQuery 
		Inherits esICRoleQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICRoleQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICRoleQuery) As String
			Return ICRoleQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICRoleQuery
			Return DirectCast(ICRoleQuery.SerializeHelper.FromXml(query, GetType(ICRoleQuery)), ICRoleQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICRole
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal roleID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(roleID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal roleID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(roleID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(roleID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal roleID As System.Int32) As Boolean
		
			Dim query As New ICRoleQuery()
			query.Where(query.RoleID = roleID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal roleID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("RoleID", roleID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_Role.RoleID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleMetadata.ColumnNames.RoleID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleMetadata.ColumnNames.RoleID, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.RoleID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.RoleName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleName As System.String
			Get
				Return MyBase.GetSystemString(ICRoleMetadata.ColumnNames.RoleName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRoleMetadata.ColumnNames.RoleName, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.RoleName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.RoleType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RoleType As System.String
			Get
				Return MyBase.GetSystemString(ICRoleMetadata.ColumnNames.RoleType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRoleMetadata.ColumnNames.RoleType, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.RoleType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.isAutoAssign
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAutoAssign As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICRoleMetadata.ColumnNames.IsAutoAssign)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICRoleMetadata.ColumnNames.IsAutoAssign, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.IsAutoAssign)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.isActive
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsActive As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICRoleMetadata.ColumnNames.IsActive)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICRoleMetadata.ColumnNames.IsActive, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.IsActive)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICRoleMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICRoleMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.ApprovedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleMetadata.ColumnNames.ApprovedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleMetadata.ColumnNames.ApprovedDate, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.ApprovedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.Description
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Description As System.String
			Get
				Return MyBase.GetSystemString(ICRoleMetadata.ColumnNames.Description)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICRoleMetadata.ColumnNames.Description, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.Description)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICRoleMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICRoleMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_Role.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICRoleMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICRoleMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICRoleMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RoleID"
							Me.str().RoleID = CType(value, string)
												
						Case "RoleName"
							Me.str().RoleName = CType(value, string)
												
						Case "RoleType"
							Me.str().RoleType = CType(value, string)
												
						Case "IsAutoAssign"
							Me.str().IsAutoAssign = CType(value, string)
												
						Case "IsActive"
							Me.str().IsActive = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedDate"
							Me.str().ApprovedDate = CType(value, string)
												
						Case "Description"
							Me.str().Description = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RoleID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RoleID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.RoleID)
							End If
						
						Case "IsAutoAssign"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAutoAssign = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.IsAutoAssign)
							End If
						
						Case "IsActive"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsActive = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.IsActive)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.ApprovedDate)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICRoleMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICRole)
				Me.entity = entity
			End Sub				
		
	
			Public Property RoleID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RoleID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleID = Nothing
					Else
						entity.RoleID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleName As System.String 
				Get
					Dim data_ As System.String = entity.RoleName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleName = Nothing
					Else
						entity.RoleName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RoleType As System.String 
				Get
					Dim data_ As System.String = entity.RoleType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RoleType = Nothing
					Else
						entity.RoleType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAutoAssign As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAutoAssign
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAutoAssign = Nothing
					Else
						entity.IsAutoAssign = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsActive As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsActive
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsActive = Nothing
					Else
						entity.IsActive = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedDate = Nothing
					Else
						entity.ApprovedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property Description As System.String 
				Get
					Dim data_ As System.String = entity.Description
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Description = Nothing
					Else
						entity.Description = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICRole
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRoleMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICRoleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRoleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICRoleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICRoleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICRoleQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICRoleCollection
		Inherits esEntityCollection(Of ICRole)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICRoleMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICRoleCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICRoleQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICRoleQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICRoleQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICRoleQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICRoleQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICRoleQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICRoleQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICRoleQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICRoleMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RoleID" 
					Return Me.RoleID
				Case "RoleName" 
					Return Me.RoleName
				Case "RoleType" 
					Return Me.RoleType
				Case "IsAutoAssign" 
					Return Me.IsAutoAssign
				Case "IsActive" 
					Return Me.IsActive
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedDate" 
					Return Me.ApprovedDate
				Case "Description" 
					Return Me.Description
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RoleID As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.RoleID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property RoleName As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.RoleName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RoleType As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.RoleType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property IsAutoAssign As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.IsAutoAssign, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsActive As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.IsActive, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.ApprovedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property Description As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.Description, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICRoleMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICRole 
		Inherits esICRole
		
	
		#Region "ICBankRoleRightsCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICBankRoleRightsCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICBankRoleRightsCollectionByRoleID_Delegate)
				map.PropertyName = "ICBankRoleRightsCollectionByRoleID"
				map.MyColumnName = "RoleID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICBankRoleRightsCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICBankRoleRightsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICBankRoleRightsQuery), New ICBankRoleRightsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RoleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_BankRoleRights_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICBankRoleRightsCollectionByRoleID As ICBankRoleRightsCollection 
		
			Get
				If Me._ICBankRoleRightsCollectionByRoleID Is Nothing Then
					Me._ICBankRoleRightsCollectionByRoleID = New ICBankRoleRightsCollection()
					Me._ICBankRoleRightsCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICBankRoleRightsCollectionByRoleID", Me._ICBankRoleRightsCollectionByRoleID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICBankRoleRightsCollectionByRoleID.Query.Where(Me._ICBankRoleRightsCollectionByRoleID.Query.RoleID = Me.RoleID)
							Me._ICBankRoleRightsCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICBankRoleRightsCollectionByRoleID.fks.Add(ICBankRoleRightsMetadata.ColumnNames.RoleID, Me.RoleID)
					End If
				End If

				Return Me._ICBankRoleRightsCollectionByRoleID
			End Get
			
			Set(ByVal value As ICBankRoleRightsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICBankRoleRightsCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICBankRoleRightsCollectionByRoleID")
					Me._ICBankRoleRightsCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICBankRoleRightsCollectionByRoleID As ICBankRoleRightsCollection
		#End Region

		#Region "ICCompanyRoleRightsCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICCompanyRoleRightsCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICCompanyRoleRightsCollectionByRoleID_Delegate)
				map.PropertyName = "ICCompanyRoleRightsCollectionByRoleID"
				map.MyColumnName = "RoleID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICCompanyRoleRightsCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICCompanyRoleRightsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICCompanyRoleRightsQuery), New ICCompanyRoleRightsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RoleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_CompanyRoleRights_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICCompanyRoleRightsCollectionByRoleID As ICCompanyRoleRightsCollection 
		
			Get
				If Me._ICCompanyRoleRightsCollectionByRoleID Is Nothing Then
					Me._ICCompanyRoleRightsCollectionByRoleID = New ICCompanyRoleRightsCollection()
					Me._ICCompanyRoleRightsCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICCompanyRoleRightsCollectionByRoleID", Me._ICCompanyRoleRightsCollectionByRoleID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICCompanyRoleRightsCollectionByRoleID.Query.Where(Me._ICCompanyRoleRightsCollectionByRoleID.Query.RoleID = Me.RoleID)
							Me._ICCompanyRoleRightsCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICCompanyRoleRightsCollectionByRoleID.fks.Add(ICCompanyRoleRightsMetadata.ColumnNames.RoleID, Me.RoleID)
					End If
				End If

				Return Me._ICCompanyRoleRightsCollectionByRoleID
			End Get
			
			Set(ByVal value As ICCompanyRoleRightsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICCompanyRoleRightsCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICCompanyRoleRightsCollectionByRoleID")
					Me._ICCompanyRoleRightsCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICCompanyRoleRightsCollectionByRoleID As ICCompanyRoleRightsCollection
		#End Region

		#Region "ICPaymentNatureAndRolesCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICPaymentNatureAndRolesCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICPaymentNatureAndRolesCollectionByRoleID_Delegate)
				map.PropertyName = "ICPaymentNatureAndRolesCollectionByRoleID"
				map.MyColumnName = "RoleID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICPaymentNatureAndRolesCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICPaymentNatureAndRolesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICPaymentNatureAndRolesQuery), New ICPaymentNatureAndRolesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RoleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_PaymentNatureAndRoles_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICPaymentNatureAndRolesCollectionByRoleID As ICPaymentNatureAndRolesCollection 
		
			Get
				If Me._ICPaymentNatureAndRolesCollectionByRoleID Is Nothing Then
					Me._ICPaymentNatureAndRolesCollectionByRoleID = New ICPaymentNatureAndRolesCollection()
					Me._ICPaymentNatureAndRolesCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICPaymentNatureAndRolesCollectionByRoleID", Me._ICPaymentNatureAndRolesCollectionByRoleID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICPaymentNatureAndRolesCollectionByRoleID.Query.Where(Me._ICPaymentNatureAndRolesCollectionByRoleID.Query.RoleID = Me.RoleID)
							Me._ICPaymentNatureAndRolesCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICPaymentNatureAndRolesCollectionByRoleID.fks.Add(ICPaymentNatureAndRolesMetadata.ColumnNames.RoleID, Me.RoleID)
					End If
				End If

				Return Me._ICPaymentNatureAndRolesCollectionByRoleID
			End Get
			
			Set(ByVal value As ICPaymentNatureAndRolesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICPaymentNatureAndRolesCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICPaymentNatureAndRolesCollectionByRoleID")
					Me._ICPaymentNatureAndRolesCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICPaymentNatureAndRolesCollectionByRoleID As ICPaymentNatureAndRolesCollection
		#End Region

		#Region "ICRoleRightsCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICRoleRightsCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICRoleRightsCollectionByRoleID_Delegate)
				map.PropertyName = "ICRoleRightsCollectionByRoleID"
				map.MyColumnName = "RoleID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICRoleRightsCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICRoleRightsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICRoleRightsQuery), New ICRoleRightsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RoleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_RoleRights_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICRoleRightsCollectionByRoleID As ICRoleRightsCollection 
		
			Get
				If Me._ICRoleRightsCollectionByRoleID Is Nothing Then
					Me._ICRoleRightsCollectionByRoleID = New ICRoleRightsCollection()
					Me._ICRoleRightsCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICRoleRightsCollectionByRoleID", Me._ICRoleRightsCollectionByRoleID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICRoleRightsCollectionByRoleID.Query.Where(Me._ICRoleRightsCollectionByRoleID.Query.RoleID = Me.RoleID)
							Me._ICRoleRightsCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICRoleRightsCollectionByRoleID.fks.Add(ICRoleRightsMetadata.ColumnNames.RoleID, Me.RoleID)
					End If
				End If

				Return Me._ICRoleRightsCollectionByRoleID
			End Get
			
			Set(ByVal value As ICRoleRightsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICRoleRightsCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICRoleRightsCollectionByRoleID")
					Me._ICRoleRightsCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICRoleRightsCollectionByRoleID As ICRoleRightsCollection
		#End Region

		#Region "UpToICUserCollection - Many To Many"
		''' <summary>
		''' Many to Many
		''' Foreign Key Name - FK_IC_UserRoles_IC_Role
		''' </summary>

		<XmlIgnore()> _
		Public Property UpToICUserCollection As ICUserCollection
		
			Get
				If Me._UpToICUserCollection Is Nothing Then
					Me._UpToICUserCollection = New ICUserCollection()
					Me._UpToICUserCollection.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("UpToICUserCollection", Me._UpToICUserCollection)
					If Not Me.es.IsLazyLoadDisabled And Not Me.RoleID.Equals(Nothing) Then 
				
						Dim m As New ICUserQuery("m")
						Dim j As New ICUserRolesQuery("j")
						m.Select(m)
						m.InnerJoin(j).On(m.UserID = j.UserID)
                        m.Where(j.RoleID = Me.RoleID)

						Me._UpToICUserCollection.Load(m)

					End If
				End If

				Return Me._UpToICUserCollection
			End Get
			
			Set(ByVal value As ICUserCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._UpToICUserCollection Is Nothing Then

					Me.RemovePostSave("UpToICUserCollection")
					Me._UpToICUserCollection = Nothing
					

				End If
			End Set	
			
		End Property

		''' <summary>
		''' Many to Many Associate
		''' Foreign Key Name - FK_IC_UserRoles_IC_Role
		''' </summary>
		Public Sub AssociateICUserCollection(entity As ICUser)
			If Me._ICUserRolesCollection Is Nothing Then
				Me._ICUserRolesCollection = New ICUserRolesCollection()
				Me._ICUserRolesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICUserRolesCollection", Me._ICUserRolesCollection)
			End If
			
			Dim obj As ICUserRoles = Me._ICUserRolesCollection.AddNew()
			obj.RoleID = Me.RoleID
			obj.UserID = entity.UserID			
			
		End Sub

		''' <summary>
		''' Many to Many Dissociate
		''' Foreign Key Name - FK_IC_UserRoles_IC_Role
		''' </summary>
		Public Sub DissociateICUserCollection(entity As ICUser)
			If Me._ICUserRolesCollection Is Nothing Then
				Me._ICUserRolesCollection = new ICUserRolesCollection()
				Me._ICUserRolesCollection.es.Connection.Name = Me.es.Connection.Name
				Me.SetPostSave("ICUserRolesCollection", Me._ICUserRolesCollection)
			End If

			Dim obj As ICUserRoles = Me._ICUserRolesCollection.AddNew()
			obj.RoleID = Me.RoleID
            obj.UserID = entity.UserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
		End Sub

		Private _UpToICUserCollection As ICUserCollection
		Private _ICUserRolesCollection As ICUserRolesCollection
		#End Region

		#Region "ICUserRolesCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICUserRolesCollectionByRoleID_Delegate)
				map.PropertyName = "ICUserRolesCollectionByRoleID"
				map.MyColumnName = "RoleID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICUserRolesQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesQuery), New ICUserRolesQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RoleID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRoles_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesCollectionByRoleID As ICUserRolesCollection 
		
			Get
				If Me._ICUserRolesCollectionByRoleID Is Nothing Then
					Me._ICUserRolesCollectionByRoleID = New ICUserRolesCollection()
					Me._ICUserRolesCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesCollectionByRoleID", Me._ICUserRolesCollectionByRoleID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesCollectionByRoleID.Query.Where(Me._ICUserRolesCollectionByRoleID.Query.RoleID = Me.RoleID)
							Me._ICUserRolesCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesCollectionByRoleID.fks.Add(ICUserRolesMetadata.ColumnNames.RoleID, Me.RoleID)
					End If
				End If

				Return Me._ICUserRolesCollectionByRoleID
			End Get
			
			Set(ByVal value As ICUserRolesCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICUserRolesCollectionByRoleID")
					Me._ICUserRolesCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesCollectionByRoleID As ICUserRolesCollection
		#End Region

		#Region "ICUserRolesAccountAndPaymentNatureCollectionByRolesID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesAccountAndPaymentNatureCollectionByRolesID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICRole.ICUserRolesAccountAndPaymentNatureCollectionByRolesID_Delegate)
				map.PropertyName = "ICUserRolesAccountAndPaymentNatureCollectionByRolesID"
				map.MyColumnName = "RolesID"
				map.ParentColumnName = "RoleID"
				map.IsMultiPartKey = false
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesAccountAndPaymentNatureCollectionByRolesID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICRoleQuery(data.NextAlias())
			
			Dim mee As ICUserRolesAccountAndPaymentNatureQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesAccountAndPaymentNatureQuery), New ICUserRolesAccountAndPaymentNatureQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RoleID = mee.RolesID)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_Role
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesAccountAndPaymentNatureCollectionByRolesID As ICUserRolesAccountAndPaymentNatureCollection 
		
			Get
				If Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID Is Nothing Then
					Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID = New ICUserRolesAccountAndPaymentNatureCollection()
					Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesAccountAndPaymentNatureCollectionByRolesID", Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID)
				
					If Not Me.RoleID.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID.Query.Where(Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID.Query.RolesID = Me.RoleID)
							Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID.fks.Add(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.RolesID, Me.RoleID)
					End If
				End If

				Return Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID
			End Get
			
			Set(ByVal value As ICUserRolesAccountAndPaymentNatureCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID Is Nothing Then

					Me.RemovePostSave("ICUserRolesAccountAndPaymentNatureCollectionByRolesID")
					Me._ICUserRolesAccountAndPaymentNatureCollectionByRolesID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesAccountAndPaymentNatureCollectionByRolesID As ICUserRolesAccountAndPaymentNatureCollection
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICBankRoleRightsCollectionByRoleID"
					coll = Me.ICBankRoleRightsCollectionByRoleID
					Exit Select
				Case "ICCompanyRoleRightsCollectionByRoleID"
					coll = Me.ICCompanyRoleRightsCollectionByRoleID
					Exit Select
				Case "ICPaymentNatureAndRolesCollectionByRoleID"
					coll = Me.ICPaymentNatureAndRolesCollectionByRoleID
					Exit Select
				Case "ICRoleRightsCollectionByRoleID"
					coll = Me.ICRoleRightsCollectionByRoleID
					Exit Select
				Case "ICUserRolesCollectionByRoleID"
					coll = Me.ICUserRolesCollectionByRoleID
					Exit Select
				Case "ICUserRolesAccountAndPaymentNatureCollectionByRolesID"
					coll = Me.ICUserRolesAccountAndPaymentNatureCollectionByRolesID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICBankRoleRightsCollectionByRoleID", GetType(ICBankRoleRightsCollection), New ICBankRoleRights()))
			props.Add(new esPropertyDescriptor(Me, "ICCompanyRoleRightsCollectionByRoleID", GetType(ICCompanyRoleRightsCollection), New ICCompanyRoleRights()))
			props.Add(new esPropertyDescriptor(Me, "ICPaymentNatureAndRolesCollectionByRoleID", GetType(ICPaymentNatureAndRolesCollection), New ICPaymentNatureAndRoles()))
			props.Add(new esPropertyDescriptor(Me, "ICRoleRightsCollectionByRoleID", GetType(ICRoleRightsCollection), New ICRoleRights()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesCollectionByRoleID", GetType(ICUserRolesCollection), New ICUserRoles()))
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesAccountAndPaymentNatureCollectionByRolesID", GetType(ICUserRolesAccountAndPaymentNatureCollection), New ICUserRolesAccountAndPaymentNature()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICRoleMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.RoleID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleMetadata.PropertyNames.RoleID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.RoleName, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRoleMetadata.PropertyNames.RoleName
			c.CharacterMaxLength = 50
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.RoleType, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRoleMetadata.PropertyNames.RoleType
			c.CharacterMaxLength = 100
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.IsAutoAssign, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICRoleMetadata.PropertyNames.IsAutoAssign
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.IsActive, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICRoleMetadata.PropertyNames.IsActive
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.CreateBy, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.CreateDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.ApprovedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.IsApproved, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICRoleMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.ApprovedDate, 9, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleMetadata.PropertyNames.ApprovedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.Description, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICRoleMetadata.PropertyNames.Description
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.Creater, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICRoleMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICRoleMetadata.ColumnNames.CreationDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICRoleMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICRoleMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RoleID As String = "RoleID"
			 Public Const RoleName As String = "RoleName"
			 Public Const RoleType As String = "RoleType"
			 Public Const IsAutoAssign As String = "isAutoAssign"
			 Public Const IsActive As String = "isActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "isApproved"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const Description As String = "Description"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RoleID As String = "RoleID"
			 Public Const RoleName As String = "RoleName"
			 Public Const RoleType As String = "RoleType"
			 Public Const IsAutoAssign As String = "IsAutoAssign"
			 Public Const IsActive As String = "IsActive"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedDate As String = "ApprovedDate"
			 Public Const Description As String = "Description"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICRoleMetadata)
			
				If ICRoleMetadata.mapDelegates Is Nothing Then
					ICRoleMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICRoleMetadata._meta Is Nothing Then
					ICRoleMetadata._meta = New ICRoleMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RoleID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("RoleName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RoleType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("IsAutoAssign", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsActive", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("Description", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_Role"
				meta.Destination = "IC_Role"
				
				meta.spInsert = "proc_IC_RoleInsert"
				meta.spUpdate = "proc_IC_RoleUpdate"
				meta.spDelete = "proc_IC_RoleDelete"
				meta.spLoadAll = "proc_IC_RoleLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_RoleLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICRoleMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
