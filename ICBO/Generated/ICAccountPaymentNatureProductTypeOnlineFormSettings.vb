
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:52 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AccountPaymentNatureProductTypeOnlineFormSettings' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAccountPaymentNatureProductTypeOnlineFormSettings))> _
	<XmlType("ICAccountPaymentNatureProductTypeOnlineFormSettings")> _	
	Partial Public Class ICAccountPaymentNatureProductTypeOnlineFormSettings 
		Inherits esICAccountPaymentNatureProductTypeOnlineFormSettings
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAccountPaymentNatureProductTypeOnlineFormSettings()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32)
			Dim obj As New ICAccountPaymentNatureProductTypeOnlineFormSettings()
			obj.AccountPaymentNatureProductTypeOnlineFormSettingID = accountPaymentNatureProductTypeOnlineFormSettingID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAccountPaymentNatureProductTypeOnlineFormSettings()
			obj.AccountPaymentNatureProductTypeOnlineFormSettingID = accountPaymentNatureProductTypeOnlineFormSettingID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection")> _
	Partial Public Class ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection
		Inherits esICAccountPaymentNatureProductTypeOnlineFormSettingsCollection
		Implements IEnumerable(Of ICAccountPaymentNatureProductTypeOnlineFormSettings)
	
		Public Function FindByPrimaryKey(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32) As ICAccountPaymentNatureProductTypeOnlineFormSettings
			Return MyBase.SingleOrDefault(Function(e) e.AccountPaymentNatureProductTypeOnlineFormSettingID.HasValue AndAlso e.AccountPaymentNatureProductTypeOnlineFormSettingID.Value = accountPaymentNatureProductTypeOnlineFormSettingID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAccountPaymentNatureProductTypeOnlineFormSettings))> _
		Public Class ICAccountPaymentNatureProductTypeOnlineFormSettingsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection)
			
			Public Shared Widening Operator CType(packet As ICAccountPaymentNatureProductTypeOnlineFormSettingsCollectionWCFPacket) As ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection) As ICAccountPaymentNatureProductTypeOnlineFormSettingsCollectionWCFPacket
				Return New ICAccountPaymentNatureProductTypeOnlineFormSettingsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery 
		Inherits esICAccountPaymentNatureProductTypeOnlineFormSettingsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery) As String
			Return ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery
			Return DirectCast(ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery.SerializeHelper.FromXml(query, GetType(ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery)), ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeOnlineFormSettings
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountPaymentNatureProductTypeOnlineFormSettingID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountPaymentNatureProductTypeOnlineFormSettingID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(accountPaymentNatureProductTypeOnlineFormSettingID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(accountPaymentNatureProductTypeOnlineFormSettingID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32) As Boolean
		
			Dim query As New ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery()
			query.Where(query.AccountPaymentNatureProductTypeOnlineFormSettingID = accountPaymentNatureProductTypeOnlineFormSettingID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal accountPaymentNatureProductTypeOnlineFormSettingID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AccountPaymentNatureProductTypeOnlineFormSettingID", accountPaymentNatureProductTypeOnlineFormSettingID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.AccountPaymentNatureProductTypeOnlineFormSettingID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountPaymentNatureProductTypeOnlineFormSettingID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeOnlineFormSettingID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeOnlineFormSettingID, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeOnlineFormSettingID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.AccountPaymentNatureProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountPaymentNatureProductTypeCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeCode, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldID, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.FieldName
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldName As System.String
			Get
				Return MyBase.GetSystemString(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldName)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldName, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FieldName)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.FixLength
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FixLength As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FixLength)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FixLength, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FixLength)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.IsRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsRequired, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.MustRequired
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property MustRequired As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.MustRequired)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.MustRequired, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.MustRequired)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AccountPaymentNatureProductTypeOnlineFormSettings.IsVisible
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsVisible As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsVisible)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsVisible, value) Then
					OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsVisible)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AccountPaymentNatureProductTypeOnlineFormSettingID"
							Me.str().AccountPaymentNatureProductTypeOnlineFormSettingID = CType(value, string)
												
						Case "AccountPaymentNatureProductTypeCode"
							Me.str().AccountPaymentNatureProductTypeCode = CType(value, string)
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "FieldName"
							Me.str().FieldName = CType(value, string)
												
						Case "FixLength"
							Me.str().FixLength = CType(value, string)
												
						Case "IsRequired"
							Me.str().IsRequired = CType(value, string)
												
						Case "MustRequired"
							Me.str().MustRequired = CType(value, string)
												
						Case "IsVisible"
							Me.str().IsVisible = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "AccountPaymentNatureProductTypeOnlineFormSettingID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AccountPaymentNatureProductTypeOnlineFormSettingID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeOnlineFormSettingID)
							End If
						
						Case "AccountPaymentNatureProductTypeCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AccountPaymentNatureProductTypeCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeCode)
							End If
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FieldID)
							End If
						
						Case "FixLength"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FixLength = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FixLength)
							End If
						
						Case "IsRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsRequired)
							End If
						
						Case "MustRequired"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.MustRequired = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.MustRequired)
							End If
						
						Case "IsVisible"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsVisible = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsVisible)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAccountPaymentNatureProductTypeOnlineFormSettings)
				Me.entity = entity
			End Sub				
		
	
			Public Property AccountPaymentNatureProductTypeOnlineFormSettingID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AccountPaymentNatureProductTypeOnlineFormSettingID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountPaymentNatureProductTypeOnlineFormSettingID = Nothing
					Else
						entity.AccountPaymentNatureProductTypeOnlineFormSettingID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountPaymentNatureProductTypeCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AccountPaymentNatureProductTypeCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountPaymentNatureProductTypeCode = Nothing
					Else
						entity.AccountPaymentNatureProductTypeCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldName As System.String 
				Get
					Dim data_ As System.String = entity.FieldName
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldName = Nothing
					Else
						entity.FieldName = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property FixLength As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FixLength
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FixLength = Nothing
					Else
						entity.FixLength = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsRequired = Nothing
					Else
						entity.IsRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property MustRequired As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.MustRequired
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.MustRequired = Nothing
					Else
						entity.MustRequired = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsVisible As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsVisible
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsVisible = Nothing
					Else
						entity.IsVisible = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAccountPaymentNatureProductTypeOnlineFormSettings
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeOnlineFormSettingsCollection
		Inherits esEntityCollection(Of ICAccountPaymentNatureProductTypeOnlineFormSettings)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAccountPaymentNatureProductTypeOnlineFormSettingsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAccountPaymentNatureProductTypeOnlineFormSettingsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAccountPaymentNatureProductTypeOnlineFormSettingsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AccountPaymentNatureProductTypeOnlineFormSettingID" 
					Return Me.AccountPaymentNatureProductTypeOnlineFormSettingID
				Case "AccountPaymentNatureProductTypeCode" 
					Return Me.AccountPaymentNatureProductTypeCode
				Case "FieldID" 
					Return Me.FieldID
				Case "FieldName" 
					Return Me.FieldName
				Case "FixLength" 
					Return Me.FixLength
				Case "IsRequired" 
					Return Me.IsRequired
				Case "MustRequired" 
					Return Me.MustRequired
				Case "IsVisible" 
					Return Me.IsVisible
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AccountPaymentNatureProductTypeOnlineFormSettingID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeOnlineFormSettingID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountPaymentNatureProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldName As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldName, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property FixLength As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FixLength, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property MustRequired As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.MustRequired, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsVisible As esQueryItem
			Get
				Return New esQueryItem(Me, ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsVisible, esSystemType.Boolean)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAccountPaymentNatureProductTypeOnlineFormSettings 
		Inherits esICAccountPaymentNatureProductTypeOnlineFormSettings
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeOnlineFormSettingID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeOnlineFormSettingID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.AccountPaymentNatureProductTypeCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.AccountPaymentNatureProductTypeCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FieldName, 3, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FieldName
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.FixLength, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.FixLength
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsRequired, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.MustRequired, 6, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.MustRequired
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.ColumnNames.IsVisible, 7, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.PropertyNames.IsVisible
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AccountPaymentNatureProductTypeOnlineFormSettingID As String = "AccountPaymentNatureProductTypeOnlineFormSettingID"
			 Public Const AccountPaymentNatureProductTypeCode As String = "AccountPaymentNatureProductTypeCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsVisible As String = "IsVisible"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AccountPaymentNatureProductTypeOnlineFormSettingID As String = "AccountPaymentNatureProductTypeOnlineFormSettingID"
			 Public Const AccountPaymentNatureProductTypeCode As String = "AccountPaymentNatureProductTypeCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const FieldName As String = "FieldName"
			 Public Const FixLength As String = "FixLength"
			 Public Const IsRequired As String = "IsRequired"
			 Public Const MustRequired As String = "MustRequired"
			 Public Const IsVisible As String = "IsVisible"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata)
			
				If ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.mapDelegates Is Nothing Then
					ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata._meta Is Nothing Then
					ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata._meta = New ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AccountPaymentNatureProductTypeOnlineFormSettingID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountPaymentNatureProductTypeCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldName", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("FixLength", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("MustRequired", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsVisible", new esTypeMap("bit", "System.Boolean"))			
				
				
				 
				meta.Source = "IC_AccountPaymentNatureProductTypeOnlineFormSettings"
				meta.Destination = "IC_AccountPaymentNatureProductTypeOnlineFormSettings"
				
				meta.spInsert = "proc_IC_AccountPaymentNatureProductTypeOnlineFormSettingsInsert"
				meta.spUpdate = "proc_IC_AccountPaymentNatureProductTypeOnlineFormSettingsUpdate"
				meta.spDelete = "proc_IC_AccountPaymentNatureProductTypeOnlineFormSettingsDelete"
				meta.spLoadAll = "proc_IC_AccountPaymentNatureProductTypeOnlineFormSettingsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AccountPaymentNatureProductTypeOnlineFormSettingsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAccountPaymentNatureProductTypeOnlineFormSettingsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
