
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:23:01 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_UserRolesAccountAndPaymentNature' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICUserRolesAccountAndPaymentNature))> _
	<XmlType("ICUserRolesAccountAndPaymentNature")> _	
	Partial Public Class ICUserRolesAccountAndPaymentNature 
		Inherits esICUserRolesAccountAndPaymentNature
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICUserRolesAccountAndPaymentNature()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String)
			Dim obj As New ICUserRolesAccountAndPaymentNature()
			obj.RolesID = rolesID
			obj.PaymentNatureCode = paymentNatureCode
			obj.UserID = userID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICUserRolesAccountAndPaymentNature()
			obj.RolesID = rolesID
			obj.PaymentNatureCode = paymentNatureCode
			obj.UserID = userID
			obj.AccountNumber = accountNumber
			obj.BranchCode = branchCode
			obj.Currency = currency
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICUserRolesAccountAndPaymentNatureCollection")> _
	Partial Public Class ICUserRolesAccountAndPaymentNatureCollection
		Inherits esICUserRolesAccountAndPaymentNatureCollection
		Implements IEnumerable(Of ICUserRolesAccountAndPaymentNature)
	
		Public Function FindByPrimaryKey(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As ICUserRolesAccountAndPaymentNature
			Return MyBase.SingleOrDefault(Function(e) e.RolesID.HasValue AndAlso e.RolesID.Value = rolesID And e.PaymentNatureCode = paymentNatureCode And e.UserID.HasValue AndAlso e.UserID.Value = userID And e.AccountNumber = accountNumber And e.BranchCode = branchCode And e.Currency = currency)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICUserRolesAccountAndPaymentNature))> _
		Public Class ICUserRolesAccountAndPaymentNatureCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICUserRolesAccountAndPaymentNatureCollection)
			
			Public Shared Widening Operator CType(packet As ICUserRolesAccountAndPaymentNatureCollectionWCFPacket) As ICUserRolesAccountAndPaymentNatureCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICUserRolesAccountAndPaymentNatureCollection) As ICUserRolesAccountAndPaymentNatureCollectionWCFPacket
				Return New ICUserRolesAccountAndPaymentNatureCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICUserRolesAccountAndPaymentNatureQuery 
		Inherits esICUserRolesAccountAndPaymentNatureQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICUserRolesAccountAndPaymentNatureQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICUserRolesAccountAndPaymentNatureQuery) As String
			Return ICUserRolesAccountAndPaymentNatureQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICUserRolesAccountAndPaymentNatureQuery
			Return DirectCast(ICUserRolesAccountAndPaymentNatureQuery.SerializeHelper.FromXml(query, GetType(ICUserRolesAccountAndPaymentNatureQuery)), ICUserRolesAccountAndPaymentNatureQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesAccountAndPaymentNature
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(rolesID, paymentNatureCode, userID, accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(rolesID, paymentNatureCode, userID, accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(rolesID, paymentNatureCode, userID, accountNumber, branchCode, currency)
			Else
				Return LoadByPrimaryKeyStoredProcedure(rolesID, paymentNatureCode, userID, accountNumber, branchCode, currency)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim query As New ICUserRolesAccountAndPaymentNatureQuery()
			query.Where(query.RolesID = rolesID And query.PaymentNatureCode = paymentNatureCode And query.UserID = userID And query.AccountNumber = accountNumber And query.BranchCode = branchCode And query.Currency = currency)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal rolesID As System.Int32, ByVal paymentNatureCode As System.String, ByVal userID As System.Int32, ByVal accountNumber As System.String, ByVal branchCode As System.String, ByVal currency As System.String) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("RolesID", rolesID)
						parms.Add("PaymentNatureCode", paymentNatureCode)
						parms.Add("UserID", userID)
						parms.Add("AccountNumber", accountNumber)
						parms.Add("BranchCode", branchCode)
						parms.Add("Currency", currency)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.RolesID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RolesID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.RolesID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.RolesID, value) Then
					Me._UpToICRoleByRolesID = Nothing
					Me.OnPropertyChanged("UpToICRoleByRolesID")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.RolesID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.PaymentNatureCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property PaymentNatureCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.PaymentNatureCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.PaymentNatureCode, value) Then
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
					Me.OnPropertyChanged("UpToICPaymentNatureByPaymentNatureCode")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.PaymentNatureCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.CreateBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateBy, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.CreateDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreateDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateDate, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.ApprovedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedBy, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.isApproved
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsApproved As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.IsApproved)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.IsApproved, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.IsApproved)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.ApprovedOn
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovedOn As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedOn)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedOn, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedOn)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.UserID, value) Then
					Me._UpToICUserByUserID = Nothing
					Me.OnPropertyChanged("UpToICUserByUserID")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.AccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.AccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.AccountNumber, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.AccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.BranchCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property BranchCode As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.BranchCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.BranchCode, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.BranchCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.Currency
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Currency As System.String
			Get
				Return MyBase.GetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Currency)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Currency, value) Then
					Me._UpToICAccountsByAccountNumber = Nothing
					Me.OnPropertyChanged("UpToICAccountsByAccountNumber")
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.Currency)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_UserRolesAccountAndPaymentNature.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICAccountsByAccountNumber As ICAccounts
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICRoleByRolesID As ICRole
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByUserID As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "RolesID"
							Me.str().RolesID = CType(value, string)
												
						Case "PaymentNatureCode"
							Me.str().PaymentNatureCode = CType(value, string)
												
						Case "CreateBy"
							Me.str().CreateBy = CType(value, string)
												
						Case "CreateDate"
							Me.str().CreateDate = CType(value, string)
												
						Case "ApprovedBy"
							Me.str().ApprovedBy = CType(value, string)
												
						Case "IsApproved"
							Me.str().IsApproved = CType(value, string)
												
						Case "ApprovedOn"
							Me.str().ApprovedOn = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "AccountNumber"
							Me.str().AccountNumber = CType(value, string)
												
						Case "BranchCode"
							Me.str().BranchCode = CType(value, string)
												
						Case "Currency"
							Me.str().Currency = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "RolesID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.RolesID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.RolesID)
							End If
						
						Case "CreateBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreateBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateBy)
							End If
						
						Case "CreateDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreateDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateDate)
							End If
						
						Case "ApprovedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedBy)
							End If
						
						Case "IsApproved"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsApproved = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.IsApproved)
							End If
						
						Case "ApprovedOn"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.ApprovedOn = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedOn)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.UserID)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICUserRolesAccountAndPaymentNature)
				Me.entity = entity
			End Sub				
		
	
			Public Property RolesID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.RolesID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RolesID = Nothing
					Else
						entity.RolesID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property PaymentNatureCode As System.String 
				Get
					Dim data_ As System.String = entity.PaymentNatureCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.PaymentNatureCode = Nothing
					Else
						entity.PaymentNatureCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreateBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateBy = Nothing
					Else
						entity.CreateBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreateDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreateDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreateDate = Nothing
					Else
						entity.CreateDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedBy = Nothing
					Else
						entity.ApprovedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsApproved As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsApproved
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsApproved = Nothing
					Else
						entity.IsApproved = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovedOn As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.ApprovedOn
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovedOn = Nothing
					Else
						entity.ApprovedOn = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property AccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.AccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AccountNumber = Nothing
					Else
						entity.AccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property BranchCode As System.String 
				Get
					Dim data_ As System.String = entity.BranchCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.BranchCode = Nothing
					Else
						entity.BranchCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Currency As System.String 
				Get
					Dim data_ As System.String = entity.Currency
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Currency = Nothing
					Else
						entity.Currency = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICUserRolesAccountAndPaymentNature
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountAndPaymentNatureMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICUserRolesAccountAndPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesAccountAndPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICUserRolesAccountAndPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICUserRolesAccountAndPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICUserRolesAccountAndPaymentNatureQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICUserRolesAccountAndPaymentNatureCollection
		Inherits esEntityCollection(Of ICUserRolesAccountAndPaymentNature)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountAndPaymentNatureMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICUserRolesAccountAndPaymentNatureCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICUserRolesAccountAndPaymentNatureQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICUserRolesAccountAndPaymentNatureQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICUserRolesAccountAndPaymentNatureQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICUserRolesAccountAndPaymentNatureQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICUserRolesAccountAndPaymentNatureQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICUserRolesAccountAndPaymentNatureQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICUserRolesAccountAndPaymentNatureQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICUserRolesAccountAndPaymentNatureQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICUserRolesAccountAndPaymentNatureMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "RolesID" 
					Return Me.RolesID
				Case "PaymentNatureCode" 
					Return Me.PaymentNatureCode
				Case "CreateBy" 
					Return Me.CreateBy
				Case "CreateDate" 
					Return Me.CreateDate
				Case "ApprovedBy" 
					Return Me.ApprovedBy
				Case "IsApproved" 
					Return Me.IsApproved
				Case "ApprovedOn" 
					Return Me.ApprovedOn
				Case "UserID" 
					Return Me.UserID
				Case "AccountNumber" 
					Return Me.AccountNumber
				Case "BranchCode" 
					Return Me.BranchCode
				Case "Currency" 
					Return Me.Currency
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property RolesID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.RolesID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property PaymentNatureCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.PaymentNatureCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreateBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreateDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsApproved As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.IsApproved, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovedOn As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedOn, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property AccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.AccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property BranchCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.BranchCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Currency As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Currency, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICUserRolesAccountAndPaymentNature 
		Inherits esICUserRolesAccountAndPaymentNature
		
	
		#Region "ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID - Zero To Many"
		
		Public Shared ReadOnly Property Prefetch_ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID() As esPrefetchMap
			Get
				Dim map As New esPrefetchMap()
				map.PrefetchDelegate = New esPrefetchDelegate(AddressOf IC.ICUserRolesAccountAndPaymentNature.ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID_Delegate)
				map.PropertyName = "ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID"
				map.MyColumnName = "RoleID,PaymentNatureCode,UserID,AccountNumber,BranchCode,Currency"
				map.ParentColumnName = "RolesID,PaymentNatureCode,UserID,AccountNumber,BranchCode,Currency"
				map.IsMultiPartKey = true
				Return map
			End Get
		End Property

		Private Shared Sub ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID_Delegate(ByVal data As esPrefetchParameters)
		
			Dim parent As New ICUserRolesAccountAndPaymentNatureQuery(data.NextAlias())
			
			Dim mee As ICUserRolesAccountPaymentNatureAndLocationsQuery = If(data.You IsNot Nothing, TryCast(data.You, ICUserRolesAccountPaymentNatureAndLocationsQuery), New ICUserRolesAccountPaymentNatureAndLocationsQuery(data.NextAlias()))

			If data.Root Is Nothing Then
				data.Root = mee
			End If
			
			data.Root.InnerJoin(parent).On(parent.RolesID = mee.RoleID And parent.PaymentNatureCode = mee.PaymentNatureCode And parent.UserID = mee.UserID And parent.AccountNumber = mee.AccountNumber And parent.BranchCode = mee.BranchCode And parent.Currency = mee.Currency)

			data.You = parent
			
		End Sub		

		''' <summary>
		''' Zero to Many
		''' Foreign Key Name - FK_IC_UserRolesAccountPaymentNatureAndLocations_IC_UserRolesAccountAndPaymentNature
		''' </summary>

		<XmlIgnore()> _ 
		Public Property ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID As ICUserRolesAccountPaymentNatureAndLocationsCollection 
		
			Get
				If Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID Is Nothing Then
					Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID = New ICUserRolesAccountPaymentNatureAndLocationsCollection()
					Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPostSave("ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID", Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID)
				
					If Not Me.RolesID.Equals(Nothing) AndAlso Not Me.PaymentNatureCode.Equals(Nothing) AndAlso Not Me.UserID.Equals(Nothing) AndAlso Not Me.AccountNumber.Equals(Nothing) AndAlso Not Me.BranchCode.Equals(Nothing) AndAlso Not Me.Currency.Equals(Nothing) Then
					
						If Not Me.es.IsLazyLoadDisabled Then
						
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.RoleID = Me.RolesID)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.PaymentNatureCode = Me.PaymentNatureCode)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.UserID = Me.UserID)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.AccountNumber = Me.AccountNumber)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.BranchCode = Me.BranchCode)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Where(Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Currency = Me.Currency)
							Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.Query.Load()
						End If

						' Auto-hookup Foreign Keys
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.RoleID, Me.RolesID)
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.PaymentNatureCode, Me.PaymentNatureCode)
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.UserID, Me.UserID)
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.AccountNumber, Me.AccountNumber)
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.BranchCode, Me.BranchCode)
						Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID.fks.Add(ICUserRolesAccountPaymentNatureAndLocationsMetadata.ColumnNames.Currency, Me.Currency)
					End If
				End If

				Return Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID
			End Get
			
			Set(ByVal value As ICUserRolesAccountPaymentNatureAndLocationsCollection)
				If Not value Is Nothing Then Throw New Exception("'value' Must be null")

				If Not Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID Is Nothing Then

					Me.RemovePostSave("ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID")
					Me._ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID = Nothing
					

				End If
			End Set				
			
		End Property
		

		private _ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID As ICUserRolesAccountPaymentNatureAndLocationsCollection
		#End Region

		#Region "UpToICAccountsByAccountNumber - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_Accounts
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICAccountsByAccountNumber As ICAccounts
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICAccountsByAccountNumber Is Nothing _
						 AndAlso Not AccountNumber.Equals(Nothing)  AndAlso Not BranchCode.Equals(Nothing)  AndAlso Not Currency.Equals(Nothing)  Then
						
					Me._UpToICAccountsByAccountNumber = New ICAccounts()
					Me._UpToICAccountsByAccountNumber.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.AccountNumber = Me.AccountNumber)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.BranchCode = Me.BranchCode)
					Me._UpToICAccountsByAccountNumber.Query.Where(Me._UpToICAccountsByAccountNumber.Query.Currency = Me.Currency)
					Me._UpToICAccountsByAccountNumber.Query.Load()
				End If

				Return Me._UpToICAccountsByAccountNumber
			End Get
			
            Set(ByVal value As ICAccounts)
				Me.RemovePreSave("UpToICAccountsByAccountNumber")
				

				If value Is Nothing Then
				
					Me.AccountNumber = Nothing
				
					Me.BranchCode = Nothing
				
					Me.Currency = Nothing
				
					Me._UpToICAccountsByAccountNumber = Nothing
				Else
				
					Me.AccountNumber = value.AccountNumber
					
					Me.BranchCode = value.BranchCode
					
					Me.Currency = value.Currency
					
					Me._UpToICAccountsByAccountNumber = value
					Me.SetPreSave("UpToICAccountsByAccountNumber", Me._UpToICAccountsByAccountNumber)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICPaymentNatureByPaymentNatureCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_PaymentNature
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICPaymentNatureByPaymentNatureCode As ICPaymentNature
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICPaymentNatureByPaymentNatureCode Is Nothing _
						 AndAlso Not PaymentNatureCode.Equals(Nothing)  Then
						
					Me._UpToICPaymentNatureByPaymentNatureCode = New ICPaymentNature()
					Me._UpToICPaymentNatureByPaymentNatureCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Where(Me._UpToICPaymentNatureByPaymentNatureCode.Query.PaymentNatureCode = Me.PaymentNatureCode)
					Me._UpToICPaymentNatureByPaymentNatureCode.Query.Load()
				End If

				Return Me._UpToICPaymentNatureByPaymentNatureCode
			End Get
			
            Set(ByVal value As ICPaymentNature)
				Me.RemovePreSave("UpToICPaymentNatureByPaymentNatureCode")
				

				If value Is Nothing Then
				
					Me.PaymentNatureCode = Nothing
				
					Me._UpToICPaymentNatureByPaymentNatureCode = Nothing
				Else
				
					Me.PaymentNatureCode = value.PaymentNatureCode
					
					Me._UpToICPaymentNatureByPaymentNatureCode = value
					Me.SetPreSave("UpToICPaymentNatureByPaymentNatureCode", Me._UpToICPaymentNatureByPaymentNatureCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICRoleByRolesID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_Role
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICRoleByRolesID As ICRole
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICRoleByRolesID Is Nothing _
						 AndAlso Not RolesID.Equals(Nothing)  Then
						
					Me._UpToICRoleByRolesID = New ICRole()
					Me._UpToICRoleByRolesID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICRoleByRolesID", Me._UpToICRoleByRolesID)
					Me._UpToICRoleByRolesID.Query.Where(Me._UpToICRoleByRolesID.Query.RoleID = Me.RolesID)
					Me._UpToICRoleByRolesID.Query.Load()
				End If

				Return Me._UpToICRoleByRolesID
			End Get
			
            Set(ByVal value As ICRole)
				Me.RemovePreSave("UpToICRoleByRolesID")
				

				If value Is Nothing Then
				
					Me.RolesID = Nothing
				
					Me._UpToICRoleByRolesID = Nothing
				Else
				
					Me.RolesID = value.RoleID
					
					Me._UpToICRoleByRolesID = value
					Me.SetPreSave("UpToICRoleByRolesID", Me._UpToICRoleByRolesID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByUserID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_UserRolesAndPaymentNature_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByUserID As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByUserID Is Nothing _
						 AndAlso Not UserID.Equals(Nothing)  Then
						
					Me._UpToICUserByUserID = New ICUser()
					Me._UpToICUserByUserID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
					Me._UpToICUserByUserID.Query.Where(Me._UpToICUserByUserID.Query.UserID = Me.UserID)
					Me._UpToICUserByUserID.Query.Load()
				End If

				Return Me._UpToICUserByUserID
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByUserID")
				

				If value Is Nothing Then
				
					Me.UserID = Nothing
				
					Me._UpToICUserByUserID = Nothing
				Else
				
					Me.UserID = value.UserID
					
					Me._UpToICUserByUserID = value
					Me.SetPreSave("UpToICUserByUserID", Me._UpToICUserByUserID)
				End If
				
			End Set	

		End Property
		#End Region

		
		
		
		Protected Overrides Function CreateCollectionForPrefetch(name As String) As esEntityCollectionBase
			Dim coll As esEntityCollectionBase = Nothing

			Select Case name
			
				Case "ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID"
					coll = Me.ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID
					Exit Select	
			End Select

			Return coll
		End Function
					
		''' <summary>
		''' Used internally by the entity's hierarchical properties.
		''' </summary>
		Protected Overrides Function GetHierarchicalProperties() As List(Of esPropertyDescriptor)
		
			Dim props As New List(Of esPropertyDescriptor)
			props.Add(new esPropertyDescriptor(Me, "ICUserRolesAccountPaymentNatureAndLocationsCollectionByRoleID", GetType(ICUserRolesAccountPaymentNatureAndLocationsCollection), New ICUserRolesAccountPaymentNatureAndLocations()))
			Return props
			
		End Function
	End Class
		



	<Serializable> _
	Partial Public Class ICUserRolesAccountAndPaymentNatureMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.RolesID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.RolesID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.PaymentNatureCode, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.PaymentNatureCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateBy, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreateDate, 3, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreateDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedBy, 4, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.IsApproved, 5, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.IsApproved
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.ApprovedOn, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.ApprovedOn
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.UserID, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.UserID
			c.IsInPrimaryKey = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.AccountNumber, 8, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.AccountNumber
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 100
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.BranchCode, 9, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.BranchCode
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 20
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Currency, 10, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.Currency
			c.IsInPrimaryKey = True
			c.CharacterMaxLength = 50
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.Creater, 11, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICUserRolesAccountAndPaymentNatureMetadata.ColumnNames.CreationDate, 12, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICUserRolesAccountAndPaymentNatureMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICUserRolesAccountAndPaymentNatureMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const RolesID As String = "RolesID"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "isApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const RolesID As String = "RolesID"
			 Public Const PaymentNatureCode As String = "PaymentNatureCode"
			 Public Const CreateBy As String = "CreateBy"
			 Public Const CreateDate As String = "CreateDate"
			 Public Const ApprovedBy As String = "ApprovedBy"
			 Public Const IsApproved As String = "IsApproved"
			 Public Const ApprovedOn As String = "ApprovedOn"
			 Public Const UserID As String = "UserID"
			 Public Const AccountNumber As String = "AccountNumber"
			 Public Const BranchCode As String = "BranchCode"
			 Public Const Currency As String = "Currency"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICUserRolesAccountAndPaymentNatureMetadata)
			
				If ICUserRolesAccountAndPaymentNatureMetadata.mapDelegates Is Nothing Then
					ICUserRolesAccountAndPaymentNatureMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICUserRolesAccountAndPaymentNatureMetadata._meta Is Nothing Then
					ICUserRolesAccountAndPaymentNatureMetadata._meta = New ICUserRolesAccountAndPaymentNatureMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("RolesID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("PaymentNatureCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreateBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreateDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsApproved", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ApprovedOn", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("AccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("BranchCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Currency", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_UserRolesAccountAndPaymentNature"
				meta.Destination = "IC_UserRolesAccountAndPaymentNature"
				
				meta.spInsert = "proc_IC_UserRolesAccountAndPaymentNatureInsert"
				meta.spUpdate = "proc_IC_UserRolesAccountAndPaymentNatureUpdate"
				meta.spDelete = "proc_IC_UserRolesAccountAndPaymentNatureDelete"
				meta.spLoadAll = "proc_IC_UserRolesAccountAndPaymentNatureLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_UserRolesAccountAndPaymentNatureLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICUserRolesAccountAndPaymentNatureMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
