
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:54 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_CBFTDetails' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICCBFTDetails))> _
	<XmlType("ICCBFTDetails")> _	
	Partial Public Class ICCBFTDetails 
		Inherits esICCBFTDetails
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICCBFTDetails()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal fTDetailID As System.Int32)
			Dim obj As New ICCBFTDetails()
			obj.FTDetailID = fTDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal fTDetailID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICCBFTDetails()
			obj.FTDetailID = fTDetailID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICCBFTDetailsCollection")> _
	Partial Public Class ICCBFTDetailsCollection
		Inherits esICCBFTDetailsCollection
		Implements IEnumerable(Of ICCBFTDetails)
	
		Public Function FindByPrimaryKey(ByVal fTDetailID As System.Int32) As ICCBFTDetails
			Return MyBase.SingleOrDefault(Function(e) e.FTDetailID.HasValue AndAlso e.FTDetailID.Value = fTDetailID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICCBFTDetails))> _
		Public Class ICCBFTDetailsCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICCBFTDetailsCollection)
			
			Public Shared Widening Operator CType(packet As ICCBFTDetailsCollectionWCFPacket) As ICCBFTDetailsCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICCBFTDetailsCollection) As ICCBFTDetailsCollectionWCFPacket
				Return New ICCBFTDetailsCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICCBFTDetailsQuery 
		Inherits esICCBFTDetailsQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICCBFTDetailsQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICCBFTDetailsQuery) As String
			Return ICCBFTDetailsQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICCBFTDetailsQuery
			Return DirectCast(ICCBFTDetailsQuery.SerializeHelper.FromXml(query, GetType(ICCBFTDetailsQuery)), ICCBFTDetailsQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICCBFTDetails
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal fTDetailID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fTDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fTDetailID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal fTDetailID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(fTDetailID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(fTDetailID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal fTDetailID As System.Int32) As Boolean
		
			Dim query As New ICCBFTDetailsQuery()
			query.Where(query.FTDetailID = fTDetailID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal fTDetailID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("FTDetailID", fTDetailID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_CBFTDetails.FTDetailID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FTDetailID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICCBFTDetailsMetadata.ColumnNames.FTDetailID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICCBFTDetailsMetadata.ColumnNames.FTDetailID, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.FTDetailID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBFTDetails.FromAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FromAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBFTDetailsMetadata.ColumnNames.FromAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBFTDetailsMetadata.ColumnNames.FromAccountNumber, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.FromAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBFTDetails.ToAccountNumber
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ToAccountNumber As System.String
			Get
				Return MyBase.GetSystemString(ICCBFTDetailsMetadata.ColumnNames.ToAccountNumber)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBFTDetailsMetadata.ColumnNames.ToAccountNumber, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.ToAccountNumber)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBFTDetails.Amount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Amount As Nullable(Of System.Decimal)
			Get
				Return MyBase.GetSystemDecimal(ICCBFTDetailsMetadata.ColumnNames.Amount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Decimal))
				If MyBase.SetSystemDecimal(ICCBFTDetailsMetadata.ColumnNames.Amount, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.Amount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBFTDetails.EntityType
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property EntityType As System.String
			Get
				Return MyBase.GetSystemString(ICCBFTDetailsMetadata.ColumnNames.EntityType)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBFTDetailsMetadata.ColumnNames.EntityType, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.EntityType)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_CBFTDetails.RelatedID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property RelatedID As System.String
			Get
				Return MyBase.GetSystemString(ICCBFTDetailsMetadata.ColumnNames.RelatedID)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICCBFTDetailsMetadata.ColumnNames.RelatedID, value) Then
					OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.RelatedID)
				End If
			End Set
		End Property	
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "FTDetailID"
							Me.str().FTDetailID = CType(value, string)
												
						Case "FromAccountNumber"
							Me.str().FromAccountNumber = CType(value, string)
												
						Case "ToAccountNumber"
							Me.str().ToAccountNumber = CType(value, string)
												
						Case "Amount"
							Me.str().Amount = CType(value, string)
												
						Case "EntityType"
							Me.str().EntityType = CType(value, string)
												
						Case "RelatedID"
							Me.str().RelatedID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "FTDetailID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FTDetailID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.FTDetailID)
							End If
						
						Case "Amount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Decimal" Then
								Me.Amount = CType(value, Nullable(Of System.Decimal))
								OnPropertyChanged(ICCBFTDetailsMetadata.PropertyNames.Amount)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICCBFTDetails)
				Me.entity = entity
			End Sub				
		
	
			Public Property FTDetailID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FTDetailID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FTDetailID = Nothing
					Else
						entity.FTDetailID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FromAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.FromAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FromAccountNumber = Nothing
					Else
						entity.FromAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property ToAccountNumber As System.String 
				Get
					Dim data_ As System.String = entity.ToAccountNumber
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ToAccountNumber = Nothing
					Else
						entity.ToAccountNumber = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property Amount As System.String 
				Get
					Dim data_ As Nullable(Of System.Decimal) = entity.Amount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Amount = Nothing
					Else
						entity.Amount = Convert.ToDecimal(Value)
					End If
				End Set
			End Property
		  	
			Public Property EntityType As System.String 
				Get
					Dim data_ As System.String = entity.EntityType
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.EntityType = Nothing
					Else
						entity.EntityType = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property RelatedID As System.String 
				Get
					Dim data_ As System.String = entity.RelatedID
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.RelatedID = Nothing
					Else
						entity.RelatedID = Convert.ToString(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICCBFTDetails
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBFTDetailsMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICCBFTDetailsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBFTDetailsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICCBFTDetailsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICCBFTDetailsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICCBFTDetailsQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICCBFTDetailsCollection
		Inherits esEntityCollection(Of ICCBFTDetails)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICCBFTDetailsMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICCBFTDetailsCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICCBFTDetailsQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICCBFTDetailsQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICCBFTDetailsQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICCBFTDetailsQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICCBFTDetailsQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICCBFTDetailsQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICCBFTDetailsQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICCBFTDetailsQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICCBFTDetailsMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "FTDetailID" 
					Return Me.FTDetailID
				Case "FromAccountNumber" 
					Return Me.FromAccountNumber
				Case "ToAccountNumber" 
					Return Me.ToAccountNumber
				Case "Amount" 
					Return Me.Amount
				Case "EntityType" 
					Return Me.EntityType
				Case "RelatedID" 
					Return Me.RelatedID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property FTDetailID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.FTDetailID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FromAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.FromAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property ToAccountNumber As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.ToAccountNumber, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property Amount As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.Amount, esSystemType.Decimal)
			End Get
		End Property 
		
		Public ReadOnly Property EntityType As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.EntityType, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property RelatedID As esQueryItem
			Get
				Return New esQueryItem(Me, ICCBFTDetailsMetadata.ColumnNames.RelatedID, esSystemType.String)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICCBFTDetails 
		Inherits esICCBFTDetails
		
	
		
		
	End Class
		



	<Serializable> _
	Partial Public Class ICCBFTDetailsMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.FTDetailID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.FTDetailID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.FromAccountNumber, 1, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.FromAccountNumber
			c.CharacterMaxLength = 300
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.ToAccountNumber, 2, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.ToAccountNumber
			c.CharacterMaxLength = 300
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.Amount, 3, GetType(System.Decimal), esSystemType.Decimal)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.Amount
			c.NumericPrecision = 18
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.EntityType, 4, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.EntityType
			c.CharacterMaxLength = 2147483647
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICCBFTDetailsMetadata.ColumnNames.RelatedID, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICCBFTDetailsMetadata.PropertyNames.RelatedID
			c.CharacterMaxLength = 200
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICCBFTDetailsMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const FTDetailID As String = "FTDetailID"
			 Public Const FromAccountNumber As String = "FromAccountNumber"
			 Public Const ToAccountNumber As String = "ToAccountNumber"
			 Public Const Amount As String = "Amount"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedID As String = "RelatedID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const FTDetailID As String = "FTDetailID"
			 Public Const FromAccountNumber As String = "FromAccountNumber"
			 Public Const ToAccountNumber As String = "ToAccountNumber"
			 Public Const Amount As String = "Amount"
			 Public Const EntityType As String = "EntityType"
			 Public Const RelatedID As String = "RelatedID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICCBFTDetailsMetadata)
			
				If ICCBFTDetailsMetadata.mapDelegates Is Nothing Then
					ICCBFTDetailsMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICCBFTDetailsMetadata._meta Is Nothing Then
					ICCBFTDetailsMetadata._meta = New ICCBFTDetailsMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("FTDetailID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FromAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("ToAccountNumber", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("Amount", new esTypeMap("numeric", "System.Decimal"))
				meta.AddTypeMap("EntityType", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("RelatedID", new esTypeMap("varchar", "System.String"))			
				
				
				 
				meta.Source = "IC_CBFTDetails"
				meta.Destination = "IC_CBFTDetails"
				
				meta.spInsert = "proc_IC_CBFTDetailsInsert"
				meta.spUpdate = "proc_IC_CBFTDetailsUpdate"
				meta.spDelete = "proc_IC_CBFTDetailsDelete"
				meta.spLoadAll = "proc_IC_CBFTDetailsLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_CBFTDetailsLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICCBFTDetailsMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
