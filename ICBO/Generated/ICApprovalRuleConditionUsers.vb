
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 5/21/2015 11:37:52 AM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_ApprovalRuleConditionUsers' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICApprovalRuleConditionUsers))> _
	<XmlType("ICApprovalRuleConditionUsers")> _	
	Partial Public Class ICApprovalRuleConditionUsers 
		Inherits esICApprovalRuleConditionUsers
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICApprovalRuleConditionUsers()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal approvalRuleConditionUserID As System.Int32)
			Dim obj As New ICApprovalRuleConditionUsers()
			obj.ApprovalRuleConditionUserID = approvalRuleConditionUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal approvalRuleConditionUserID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICApprovalRuleConditionUsers()
			obj.ApprovalRuleConditionUserID = approvalRuleConditionUserID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICApprovalRuleConditionUsersCollection")> _
	Partial Public Class ICApprovalRuleConditionUsersCollection
		Inherits esICApprovalRuleConditionUsersCollection
		Implements IEnumerable(Of ICApprovalRuleConditionUsers)
	
		Public Function FindByPrimaryKey(ByVal approvalRuleConditionUserID As System.Int32) As ICApprovalRuleConditionUsers
			Return MyBase.SingleOrDefault(Function(e) e.ApprovalRuleConditionUserID.HasValue AndAlso e.ApprovalRuleConditionUserID.Value = approvalRuleConditionUserID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICApprovalRuleConditionUsers))> _
		Public Class ICApprovalRuleConditionUsersCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICApprovalRuleConditionUsersCollection)
			
			Public Shared Widening Operator CType(packet As ICApprovalRuleConditionUsersCollectionWCFPacket) As ICApprovalRuleConditionUsersCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICApprovalRuleConditionUsersCollection) As ICApprovalRuleConditionUsersCollectionWCFPacket
				Return New ICApprovalRuleConditionUsersCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICApprovalRuleConditionUsersQuery 
		Inherits esICApprovalRuleConditionUsersQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICApprovalRuleConditionUsersQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICApprovalRuleConditionUsersQuery) As String
			Return ICApprovalRuleConditionUsersQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICApprovalRuleConditionUsersQuery
			Return DirectCast(ICApprovalRuleConditionUsersQuery.SerializeHelper.FromXml(query, GetType(ICApprovalRuleConditionUsersQuery)), ICApprovalRuleConditionUsersQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRuleConditionUsers
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal approvalRuleConditionUserID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleConditionUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleConditionUserID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal approvalRuleConditionUserID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(approvalRuleConditionUserID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(approvalRuleConditionUserID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal approvalRuleConditionUserID As System.Int32) As Boolean
		
			Dim query As New ICApprovalRuleConditionUsersQuery()
			query.Where(query.ApprovalRuleConditionUserID = approvalRuleConditionUserID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal approvalRuleConditionUserID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("ApprovalRuleConditionUserID", approvalRuleConditionUserID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.ApprovalRuleConditionUserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovalRuleConditionUserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovalRuleConditionUserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovalRuleConditionUserID, value) Then
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovalRuleConditionUserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.UserID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserID, value) Then
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.UserID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.UserCount
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property UserCount As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserCount)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserCount, value) Then
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.UserCount)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedBy, value) Then
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_ApprovalRuleConditionUsers.ApprovaRuleCondID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ApprovaRuleCondID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovaRuleCondID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovaRuleCondID, value) Then
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID = Nothing
					Me.OnPropertyChanged("UpToICApprovalRuleConditionsByApprovaRuleCondID")
					OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovaRuleCondID)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICApprovalRuleConditionsByApprovaRuleCondID As ICApprovalRuleConditions
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "ApprovalRuleConditionUserID"
							Me.str().ApprovalRuleConditionUserID = CType(value, string)
												
						Case "UserID"
							Me.str().UserID = CType(value, string)
												
						Case "UserCount"
							Me.str().UserCount = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "ApprovaRuleCondID"
							Me.str().ApprovaRuleCondID = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "ApprovalRuleConditionUserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovalRuleConditionUserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovalRuleConditionUserID)
							End If
						
						Case "UserID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.UserID)
							End If
						
						Case "UserCount"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.UserCount = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.UserCount)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "ApprovaRuleCondID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.ApprovaRuleCondID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovaRuleCondID)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICApprovalRuleConditionUsers)
				Me.entity = entity
			End Sub				
		
	
			Public Property ApprovalRuleConditionUserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovalRuleConditionUserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovalRuleConditionUserID = Nothing
					Else
						entity.ApprovalRuleConditionUserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserID = Nothing
					Else
						entity.UserID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property UserCount As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.UserCount
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.UserCount = Nothing
					Else
						entity.UserCount = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property ApprovaRuleCondID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.ApprovaRuleCondID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ApprovaRuleCondID = Nothing
					Else
						entity.ApprovaRuleCondID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICApprovalRuleConditionUsers
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionUsersMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICApprovalRuleConditionUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleConditionUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICApprovalRuleConditionUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICApprovalRuleConditionUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICApprovalRuleConditionUsersQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICApprovalRuleConditionUsersCollection
		Inherits esEntityCollection(Of ICApprovalRuleConditionUsers)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionUsersMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICApprovalRuleConditionUsersCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICApprovalRuleConditionUsersQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICApprovalRuleConditionUsersQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICApprovalRuleConditionUsersQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICApprovalRuleConditionUsersQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICApprovalRuleConditionUsersQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICApprovalRuleConditionUsersQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICApprovalRuleConditionUsersQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICApprovalRuleConditionUsersQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICApprovalRuleConditionUsersMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "ApprovalRuleConditionUserID" 
					Return Me.ApprovalRuleConditionUserID
				Case "UserID" 
					Return Me.UserID
				Case "UserCount" 
					Return Me.UserCount
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "ApprovaRuleCondID" 
					Return Me.ApprovaRuleCondID
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property ApprovalRuleConditionUserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovalRuleConditionUserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.UserID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property UserCount As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.UserCount, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property ApprovaRuleCondID As esQueryItem
			Get
				Return New esQueryItem(Me, ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovaRuleCondID, esSystemType.Int32)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICApprovalRuleConditionUsers 
		Inherits esICApprovalRuleConditionUsers
		
	
		#Region "UpToICApprovalRuleConditionsByApprovaRuleCondID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_ApprovalRuleConditionUsers_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICApprovalRuleConditionsByApprovaRuleCondID As ICApprovalRuleConditions
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICApprovalRuleConditionsByApprovaRuleCondID Is Nothing _
						 AndAlso Not ApprovaRuleCondID.Equals(Nothing)  Then
						
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID = New ICApprovalRuleConditions()
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICApprovalRuleConditionsByApprovaRuleCondID", Me._UpToICApprovalRuleConditionsByApprovaRuleCondID)
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID.Query.Where(Me._UpToICApprovalRuleConditionsByApprovaRuleCondID.Query.ApprovalRuleConditionID = Me.ApprovaRuleCondID)
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID.Query.Load()
				End If

				Return Me._UpToICApprovalRuleConditionsByApprovaRuleCondID
			End Get
			
            Set(ByVal value As ICApprovalRuleConditions)
				Me.RemovePreSave("UpToICApprovalRuleConditionsByApprovaRuleCondID")
				

				If value Is Nothing Then
				
					Me.ApprovaRuleCondID = Nothing
				
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID = Nothing
				Else
				
					Me.ApprovaRuleCondID = value.ApprovalRuleConditionID
					
					Me._UpToICApprovalRuleConditionsByApprovaRuleCondID = value
					Me.SetPreSave("UpToICApprovalRuleConditionsByApprovaRuleCondID", Me._UpToICApprovalRuleConditionsByApprovaRuleCondID)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICApprovalRuleConditionsByApprovaRuleCondID Is Nothing Then
				Me.ApprovaRuleCondID = Me._UpToICApprovalRuleConditionsByApprovaRuleCondID.ApprovalRuleConditionID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICApprovalRuleConditionUsersMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovalRuleConditionUserID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovalRuleConditionUserID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserID, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.UserID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.UserCount, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.UserCount
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedBy, 3, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.CreatedDate, 4, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICApprovalRuleConditionUsersMetadata.ColumnNames.ApprovaRuleCondID, 5, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICApprovalRuleConditionUsersMetadata.PropertyNames.ApprovaRuleCondID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICApprovalRuleConditionUsersMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const ApprovalRuleConditionUserID As String = "ApprovalRuleConditionUserID"
			 Public Const UserID As String = "UserID"
			 Public Const UserCount As String = "UserCount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ApprovaRuleCondID As String = "ApprovaRuleCondID"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const ApprovalRuleConditionUserID As String = "ApprovalRuleConditionUserID"
			 Public Const UserID As String = "UserID"
			 Public Const UserCount As String = "UserCount"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const ApprovaRuleCondID As String = "ApprovaRuleCondID"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICApprovalRuleConditionUsersMetadata)
			
				If ICApprovalRuleConditionUsersMetadata.mapDelegates Is Nothing Then
					ICApprovalRuleConditionUsersMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICApprovalRuleConditionUsersMetadata._meta Is Nothing Then
					ICApprovalRuleConditionUsersMetadata._meta = New ICApprovalRuleConditionUsersMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("ApprovalRuleConditionUserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("UserCount", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("ApprovaRuleCondID", new esTypeMap("int", "System.Int32"))			
				
				
				 
				meta.Source = "IC_ApprovalRuleConditionUsers"
				meta.Destination = "IC_ApprovalRuleConditionUsers"
				
				meta.spInsert = "proc_IC_ApprovalRuleConditionUsersInsert"
				meta.spUpdate = "proc_IC_ApprovalRuleConditionUsersUpdate"
				meta.spDelete = "proc_IC_ApprovalRuleConditionUsersDelete"
				meta.spLoadAll = "proc_IC_ApprovalRuleConditionUsersLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_ApprovalRuleConditionUsersLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICApprovalRuleConditionUsersMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
