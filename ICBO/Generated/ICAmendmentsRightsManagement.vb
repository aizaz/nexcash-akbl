
'===============================================================================
'                   EntitySpaces Studio by EntitySpaces, LLC
'            Persistence Layer and Business Objects for Microsoft .NET
'            EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
'                         http://www.entityspaces.net
'===============================================================================
' EntitySpaces Version : 2012.1.0930.0
' EntitySpaces Driver  : SQL
' Date Generated       : 2/17/2014 2:22:53 PM
'===============================================================================

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Text
Imports System.Linq
Imports System.Data
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Runtime.Serialization

Imports EntitySpaces.Core
Imports EntitySpaces.Interfaces
Imports EntitySpaces.DynamicQuery



Namespace IC

	' <summary>
	' Encapsulates the 'IC_AmendmentsRightsManagement' table
	' </summary>

	<System.Diagnostics.DebuggerDisplay("Data = {Debug}")> _ 
	<Serializable> _
	<DataContract> _
	<KnownType(GetType(ICAmendmentsRightsManagement))> _
	<XmlType("ICAmendmentsRightsManagement")> _	
	Partial Public Class ICAmendmentsRightsManagement 
		Inherits esICAmendmentsRightsManagement
		
		<DebuggerBrowsable(DebuggerBrowsableState.RootHidden Or DebuggerBrowsableState.Never)> _		
		Protected Overrides ReadOnly Property Debug() As esEntityDebuggerView()
			Get
				Return MyBase.Debug
			End Get
		End Property
		
		Public Overrides Function CreateInstance() as esEntity
			Return New ICAmendmentsRightsManagement()
		End Function
		
		#Region "Static Quick Access Methods"
		Public Shared Sub Delete(ByVal amendmentsRightsID As System.Int32)
			Dim obj As New ICAmendmentsRightsManagement()
			obj.AmendmentsRightsID = amendmentsRightsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save()
		End Sub

		Public Shared Sub Delete(ByVal amendmentsRightsID As System.Int32, ByVal sqlAccessType As esSqlAccessType)
			Dim obj As New ICAmendmentsRightsManagement()
			obj.AmendmentsRightsID = amendmentsRightsID
			obj.AcceptChanges()
			obj.MarkAsDeleted()
			obj.Save(sqlAccessType)
		End Sub
		#End Region		
	
		
			
	End Class


 
	<DebuggerDisplay("Count = {Count}")> _ 
	<Serializable> _
	<CollectionDataContract> _
	<XmlType("ICAmendmentsRightsManagementCollection")> _
	Partial Public Class ICAmendmentsRightsManagementCollection
		Inherits esICAmendmentsRightsManagementCollection
		Implements IEnumerable(Of ICAmendmentsRightsManagement)
	
		Public Function FindByPrimaryKey(ByVal amendmentsRightsID As System.Int32) As ICAmendmentsRightsManagement
			Return MyBase.SingleOrDefault(Function(e) e.AmendmentsRightsID.HasValue AndAlso e.AmendmentsRightsID.Value = amendmentsRightsID)
		End Function


				
		#Region "WCF Service Class"

		<DataContract> _
		<KnownType(GetType(ICAmendmentsRightsManagement))> _
		Public Class ICAmendmentsRightsManagementCollectionWCFPacket
			Inherits esCollectionWCFPacket(Of ICAmendmentsRightsManagementCollection)
			
			Public Shared Widening Operator CType(packet As ICAmendmentsRightsManagementCollectionWCFPacket) As ICAmendmentsRightsManagementCollection
				Return packet.Collection
			End Operator

			Public Shared Widening Operator CType(collection As ICAmendmentsRightsManagementCollection) As ICAmendmentsRightsManagementCollectionWCFPacket
				Return New ICAmendmentsRightsManagementCollectionWCFPacket()  With {.Collection = collection}
			End Operator
			
		End Class

		#End Region
		
			
		
	End Class




	<DebuggerDisplay("Query = {Parse()}")> _ 
	<Serializable> _ 
	Partial Public Class ICAmendmentsRightsManagementQuery 
		Inherits esICAmendmentsRightsManagementQuery
		
		Public Sub New(ByVal joinAlias As String)
			Me.es.JoinAlias = joinAlias
		End Sub	
		
		Protected Overrides Function GetQueryName() As String
			Return "ICAmendmentsRightsManagementQuery"
		End Function	
		
		#Region "Explicit Casts"

		Public Shared Narrowing Operator CType(ByVal query As ICAmendmentsRightsManagementQuery) As String
			Return ICAmendmentsRightsManagementQuery.SerializeHelper.ToXml(query)
		End Operator

		Public Shared Narrowing Operator CType(ByVal query As String) As ICAmendmentsRightsManagementQuery
			Return DirectCast(ICAmendmentsRightsManagementQuery.SerializeHelper.FromXml(query, GetType(ICAmendmentsRightsManagementQuery)), ICAmendmentsRightsManagementQuery)
		End Operator

		#End Region
			
	End Class

	
	<DataContract> _
	<Serializable()> _
	MustInherit Public Partial Class esICAmendmentsRightsManagement
		Inherits esEntity
		Implements INotifyPropertyChanged
	
		Public Sub New()
		
		End Sub
		
#Region "LoadByPrimaryKey"		
		Public Overridable Function LoadByPrimaryKey(ByVal amendmentsRightsID As System.Int32) As Boolean
		
			If Me.es.Connection.SqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(amendmentsRightsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(amendmentsRightsID)
			End If
			
		End Function
	
		Public Overridable Function LoadByPrimaryKey(ByVal sqlAccessType As esSqlAccessType, ByVal amendmentsRightsID As System.Int32) As Boolean
		
			If sqlAccessType = esSqlAccessType.DynamicSQL
				Return LoadByPrimaryKeyDynamic(amendmentsRightsID)
			Else
				Return LoadByPrimaryKeyStoredProcedure(amendmentsRightsID)
			End If
			
		End Function
	
		Private Function LoadByPrimaryKeyDynamic(ByVal amendmentsRightsID As System.Int32) As Boolean
		
			Dim query As New ICAmendmentsRightsManagementQuery()
			query.Where(query.AmendmentsRightsID = amendmentsRightsID)
			Return Me.Load(query)
			
		End Function
	
		Private Function LoadByPrimaryKeyStoredProcedure(ByVal amendmentsRightsID As System.Int32) As Boolean
		
			Dim parms As esParameters = New esParameters()
			parms.Add("AmendmentsRightsID", amendmentsRightsID)
			
			Return MyBase.Load(esQueryType.StoredProcedure, Me.es.spLoadByPrimaryKey, parms)
			
		End Function
#End Region
		
#Region "Properties"
		
		
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.AmendmentsRightsID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property AmendmentsRightsID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.AmendmentsRightsID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.AmendmentsRightsID, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.AmendmentsRightsID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.CompanyCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CompanyCode As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.CompanyCode)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.CompanyCode, value) Then
					Me._UpToICCompanyByCompanyCode = Nothing
					Me.OnPropertyChanged("UpToICCompanyByCompanyCode")
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CompanyCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.FieldID
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property FieldID As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.FieldID)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.FieldID, value) Then
					Me._UpToICFieldsListByFieldID = Nothing
					Me.OnPropertyChanged("UpToICFieldsListByFieldID")
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.FieldID)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.IsAmendmentAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsAmendmentAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.IsAmendmentAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.IsAmendmentAllow, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.IsAmendmentAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.IsBulkAmendmentAllow
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property IsBulkAmendmentAllow As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.IsBulkAmendmentAllow)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.IsBulkAmendmentAllow, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.IsBulkAmendmentAllow)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.ProductTypeCode
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property ProductTypeCode As System.String
			Get
				Return MyBase.GetSystemString(ICAmendmentsRightsManagementMetadata.ColumnNames.ProductTypeCode)
			End Get
			
			Set(ByVal value As System.String)
				If MyBase.SetSystemString(ICAmendmentsRightsManagementMetadata.ColumnNames.ProductTypeCode, value) Then
					Me._UpToICProductTypeByProductTypeCode = Nothing
					Me.OnPropertyChanged("UpToICProductTypeByProductTypeCode")
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.ProductTypeCode)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.CreatedDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedDate, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedDate)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.CreatedBy
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreatedBy As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedBy)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedBy, value) Then
					Me._UpToICUserByCreatedBy = Nothing
					Me.OnPropertyChanged("UpToICUserByCreatedBy")
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedBy)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.CanView
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CanView As Nullable(Of System.Boolean)
			Get
				Return MyBase.GetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.CanView)
			End Get
			
			Set(ByVal value As Nullable(Of System.Boolean))
				If MyBase.SetSystemBoolean(ICAmendmentsRightsManagementMetadata.ColumnNames.CanView, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CanView)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.Creater
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property Creater As Nullable(Of System.Int32)
			Get
				Return MyBase.GetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.Creater)
			End Get
			
			Set(ByVal value As Nullable(Of System.Int32))
				If MyBase.SetSystemInt32(ICAmendmentsRightsManagementMetadata.ColumnNames.Creater, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.Creater)
				End If
			End Set
		End Property	
			
		' <summary>
		' Maps to IC_AmendmentsRightsManagement.CreationDate
		' </summary>
		<DataMember(EmitDefaultValue:=False)> _
		Public Overridable Property CreationDate As Nullable(Of System.DateTime)
			Get
				Return MyBase.GetSystemDateTime(ICAmendmentsRightsManagementMetadata.ColumnNames.CreationDate)
			End Get
			
			Set(ByVal value As Nullable(Of System.DateTime))
				If MyBase.SetSystemDateTime(ICAmendmentsRightsManagementMetadata.ColumnNames.CreationDate, value) Then
					OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreationDate)
				End If
			End Set
		End Property	
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICCompanyByCompanyCode As ICCompany
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICFieldsListByFieldID As ICFieldsList
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICProductTypeByProductTypeCode As ICProductType
		
		<CLSCompliant(false)> _
		Dim Friend Protected _UpToICUserByCreatedBy As ICUser
		
#End Region	

#Region ".str() Properties"

		Public Overrides Sub SetProperties(values as IDictionary)

		Dim propertyName As String
			For Each propertyName In values.Keys
				Me.SetProperty(propertyName, values(propertyName))
			Next

		End Sub

		Public Overrides Sub SetProperty(name as string, value as object)

			Dim col As esColumnMetadata = Me.Meta.Columns.FindByPropertyName(name)
			If Not col Is Nothing Then

				If value Is Nothing OrElse value.GetType().ToString() = "System.String" Then

					' Use the strongly typed property
					Select Case name
												
						Case "AmendmentsRightsID"
							Me.str().AmendmentsRightsID = CType(value, string)
												
						Case "CompanyCode"
							Me.str().CompanyCode = CType(value, string)
												
						Case "FieldID"
							Me.str().FieldID = CType(value, string)
												
						Case "IsAmendmentAllow"
							Me.str().IsAmendmentAllow = CType(value, string)
												
						Case "IsBulkAmendmentAllow"
							Me.str().IsBulkAmendmentAllow = CType(value, string)
												
						Case "ProductTypeCode"
							Me.str().ProductTypeCode = CType(value, string)
												
						Case "CreatedDate"
							Me.str().CreatedDate = CType(value, string)
												
						Case "CreatedBy"
							Me.str().CreatedBy = CType(value, string)
												
						Case "CanView"
							Me.str().CanView = CType(value, string)
												
						Case "Creater"
							Me.str().Creater = CType(value, string)
												
						Case "CreationDate"
							Me.str().CreationDate = CType(value, string)
					
					End Select
					
				Else
				
					Select Case name
						
						Case "AmendmentsRightsID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.AmendmentsRightsID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.AmendmentsRightsID)
							End If
						
						Case "CompanyCode"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CompanyCode = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CompanyCode)
							End If
						
						Case "FieldID"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.FieldID = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.FieldID)
							End If
						
						Case "IsAmendmentAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsAmendmentAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.IsAmendmentAllow)
							End If
						
						Case "IsBulkAmendmentAllow"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.IsBulkAmendmentAllow = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.IsBulkAmendmentAllow)
							End If
						
						Case "CreatedDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreatedDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedDate)
							End If
						
						Case "CreatedBy"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.CreatedBy = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedBy)
							End If
						
						Case "CanView"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Boolean" Then
								Me.CanView = CType(value, Nullable(Of System.Boolean))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CanView)
							End If
						
						Case "Creater"
						
							If value Is Nothing Or value.GetType().ToString() = "System.Int32" Then
								Me.Creater = CType(value, Nullable(Of System.Int32))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.Creater)
							End If
						
						Case "CreationDate"
						
							If value Is Nothing Or value.GetType().ToString() = "System.DateTime" Then
								Me.CreationDate = CType(value, Nullable(Of System.DateTime))
								OnPropertyChanged(ICAmendmentsRightsManagementMetadata.PropertyNames.CreationDate)
							End If
						
					
						Case Else
						
					End Select
				End If

			Else If Me.ContainsColumn(name) Then
				Me.SetColumn(name, value)
			Else
				throw New Exception("SetProperty Error: '" + name + "' not found")
			End If	

		End Sub

		Public Function str() As esStrings
		
			If _esstrings Is Nothing Then
				_esstrings = New esStrings(Me)
			End If
			Return _esstrings
			
		End Function

		NotInheritable Public Class esStrings
		
			Public Sub New(ByVal entity As esICAmendmentsRightsManagement)
				Me.entity = entity
			End Sub				
		
	
			Public Property AmendmentsRightsID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.AmendmentsRightsID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.AmendmentsRightsID = Nothing
					Else
						entity.AmendmentsRightsID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CompanyCode As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CompanyCode
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CompanyCode = Nothing
					Else
						entity.CompanyCode = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property FieldID As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.FieldID
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.FieldID = Nothing
					Else
						entity.FieldID = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsAmendmentAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsAmendmentAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsAmendmentAllow = Nothing
					Else
						entity.IsAmendmentAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property IsBulkAmendmentAllow As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.IsBulkAmendmentAllow
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.IsBulkAmendmentAllow = Nothing
					Else
						entity.IsBulkAmendmentAllow = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property ProductTypeCode As System.String 
				Get
					Dim data_ As System.String = entity.ProductTypeCode
					
					if data_ Is Nothing Then
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.ProductTypeCode = Nothing
					Else
						entity.ProductTypeCode = Convert.ToString(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreatedDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedDate = Nothing
					Else
						entity.CreatedDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreatedBy As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.CreatedBy
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreatedBy = Nothing
					Else
						entity.CreatedBy = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CanView As System.String 
				Get
					Dim data_ As Nullable(Of System.Boolean) = entity.CanView
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CanView = Nothing
					Else
						entity.CanView = Convert.ToBoolean(Value)
					End If
				End Set
			End Property
		  	
			Public Property Creater As System.String 
				Get
					Dim data_ As Nullable(Of System.Int32) = entity.Creater
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.Creater = Nothing
					Else
						entity.Creater = Convert.ToInt32(Value)
					End If
				End Set
			End Property
		  	
			Public Property CreationDate As System.String 
				Get
					Dim data_ As Nullable(Of System.DateTime) = entity.CreationDate
					
					If Not data_.HasValue Then
					
						Return String.Empty
					Else
						Return Convert.ToString(data_)
					End If
				End Get

				Set(ByVal Value as System.String)
					If String.IsNullOrEmpty(value) Then
						entity.CreationDate = Nothing
					Else
						entity.CreationDate = Convert.ToDateTime(Value)
					End If
				End Set
			End Property
		  

			Private entity As esICAmendmentsRightsManagement
		End Class
		

        <NonSerialized> _
        <IgnoreDataMember> _		
		Private _esstrings As esStrings
		
#End Region

#Region "Housekeeping methods"

		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAmendmentsRightsManagementMetadata.Meta()
			End Get
		End Property

#End Region

#Region "Query Logic"

		Public ReadOnly Property Query() As ICAmendmentsRightsManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAmendmentsRightsManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property

		Public Overloads Function Load(ByVal query As ICAmendmentsRightsManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Me.Query.Load()
		End Function

		Protected Sub InitQuery(ByVal query As ICAmendmentsRightsManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntity).Connection
			End If
		End Sub

#End Region

        <IgnoreDataMember> _
        Private m_query As ICAmendmentsRightsManagementQuery

	End Class



	<Serializable()> _
	MustInherit Public Partial Class esICAmendmentsRightsManagementCollection
		Inherits esEntityCollection(Of ICAmendmentsRightsManagement)
		
		#Region "Housekeeping methods"
		Protected Overloads Overrides ReadOnly Property Meta() As IMetadata
			Get
				Return ICAmendmentsRightsManagementMetadata.Meta()
			End Get
		End Property
		
		Protected Overloads Overrides Function GetCollectionName() As String
			Return "ICAmendmentsRightsManagementCollection"
		End Function
		
		#End Region
		
		#Region "Query Logic"
		

		<BrowsableAttribute(False)> _ 
		Public ReadOnly Property Query() As ICAmendmentsRightsManagementQuery
			Get
				If Me.m_query Is Nothing Then
					Me.m_query = New ICAmendmentsRightsManagementQuery()
					InitQuery(Me.m_query)
				End If
				
				Return Me.m_query
			End Get
		End Property
		
		Public Overloads Function Load(ByVal query As ICAmendmentsRightsManagementQuery) As Boolean
			Me.m_query = query
			InitQuery(Me.m_query)
			Return Query.Load()
		End Function
		
		Protected Overloads Overrides Function GetDynamicQuery() As esDynamicQuery
			If Me.m_query Is Nothing Then
				Me.m_query = New ICAmendmentsRightsManagementQuery()
				Me.InitQuery(m_query)
			End If
			Return Me.m_query
		End Function
		
		Protected Sub InitQuery(ByVal query As ICAmendmentsRightsManagementQuery)
			query.OnLoadDelegate = AddressOf OnQueryLoaded
			
			If Not query.es2.HasConnection Then
				query.es2.Connection = DirectCast(Me, IEntityCollection).Connection
			End If
		End Sub
		
		Protected Overloads Overrides Sub HookupQuery(ByVal query As esDynamicQuery)
			Me.InitQuery(DirectCast(query, ICAmendmentsRightsManagementQuery))
		End Sub
		
		#End Region
						
		Private m_query As ICAmendmentsRightsManagementQuery
	End Class



	<Serializable> _
	MustInherit Public Partial Class esICAmendmentsRightsManagementQuery 
		Inherits esDynamicQuery 
	
		Protected ReadOnly Overrides Property Meta() As IMetadata
			Get
				Return ICAmendmentsRightsManagementMetadata.Meta()
			End Get
		End Property
		
#Region "QueryItemFromName"

        Protected Overrides Function QueryItemFromName(ByVal name As String) As esQueryItem
            Select Case name
				Case "AmendmentsRightsID" 
					Return Me.AmendmentsRightsID
				Case "CompanyCode" 
					Return Me.CompanyCode
				Case "FieldID" 
					Return Me.FieldID
				Case "IsAmendmentAllow" 
					Return Me.IsAmendmentAllow
				Case "IsBulkAmendmentAllow" 
					Return Me.IsBulkAmendmentAllow
				Case "ProductTypeCode" 
					Return Me.ProductTypeCode
				Case "CreatedDate" 
					Return Me.CreatedDate
				Case "CreatedBy" 
					Return Me.CreatedBy
				Case "CanView" 
					Return Me.CanView
				Case "Creater" 
					Return Me.Creater
				Case "CreationDate" 
					Return Me.CreationDate
                Case Else
                    Return Nothing
            End Select
        End Function

#End Region		
		
#Region "esQueryItems"


		Public ReadOnly Property AmendmentsRightsID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.AmendmentsRightsID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CompanyCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.CompanyCode, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property FieldID As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.FieldID, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property IsAmendmentAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.IsAmendmentAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property IsBulkAmendmentAllow As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.IsBulkAmendmentAllow, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property ProductTypeCode As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.ProductTypeCode, esSystemType.String)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedDate, esSystemType.DateTime)
			End Get
		End Property 
		
		Public ReadOnly Property CreatedBy As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedBy, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CanView As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.CanView, esSystemType.Boolean)
			End Get
		End Property 
		
		Public ReadOnly Property Creater As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.Creater, esSystemType.Int32)
			End Get
		End Property 
		
		Public ReadOnly Property CreationDate As esQueryItem
			Get
				Return New esQueryItem(Me, ICAmendmentsRightsManagementMetadata.ColumnNames.CreationDate, esSystemType.DateTime)
			End Get
		End Property 
		
#End Region	
		
	End Class


	
	Partial Public Class ICAmendmentsRightsManagement 
		Inherits esICAmendmentsRightsManagement
		
	
		#Region "UpToICCompanyByCompanyCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_Company
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICCompanyByCompanyCode As ICCompany
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICCompanyByCompanyCode Is Nothing _
						 AndAlso Not CompanyCode.Equals(Nothing)  Then
						
					Me._UpToICCompanyByCompanyCode = New ICCompany()
					Me._UpToICCompanyByCompanyCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Where(Me._UpToICCompanyByCompanyCode.Query.CompanyCode = Me.CompanyCode)
					Me._UpToICCompanyByCompanyCode.Query.Load()
				End If

				Return Me._UpToICCompanyByCompanyCode
			End Get
			
            Set(ByVal value As ICCompany)
				Me.RemovePreSave("UpToICCompanyByCompanyCode")
				

				If value Is Nothing Then
				
					Me.CompanyCode = Nothing
				
					Me._UpToICCompanyByCompanyCode = Nothing
				Else
				
					Me.CompanyCode = value.CompanyCode
					
					Me._UpToICCompanyByCompanyCode = value
					Me.SetPreSave("UpToICCompanyByCompanyCode", Me._UpToICCompanyByCompanyCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICFieldsListByFieldID - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_FieldsList
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICFieldsListByFieldID As ICFieldsList
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICFieldsListByFieldID Is Nothing _
						 AndAlso Not FieldID.Equals(Nothing)  Then
						
					Me._UpToICFieldsListByFieldID = New ICFieldsList()
					Me._UpToICFieldsListByFieldID.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICFieldsListByFieldID", Me._UpToICFieldsListByFieldID)
					Me._UpToICFieldsListByFieldID.Query.Where(Me._UpToICFieldsListByFieldID.Query.FieldID = Me.FieldID)
					Me._UpToICFieldsListByFieldID.Query.Load()
				End If

				Return Me._UpToICFieldsListByFieldID
			End Get
			
            Set(ByVal value As ICFieldsList)
				Me.RemovePreSave("UpToICFieldsListByFieldID")
				

				If value Is Nothing Then
				
					Me.FieldID = Nothing
				
					Me._UpToICFieldsListByFieldID = Nothing
				Else
				
					Me.FieldID = value.FieldID
					
					Me._UpToICFieldsListByFieldID = value
					Me.SetPreSave("UpToICFieldsListByFieldID", Me._UpToICFieldsListByFieldID)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICProductTypeByProductTypeCode - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_ProductType
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICProductTypeByProductTypeCode As ICProductType
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICProductTypeByProductTypeCode Is Nothing _
						 AndAlso Not ProductTypeCode.Equals(Nothing)  Then
						
					Me._UpToICProductTypeByProductTypeCode = New ICProductType()
					Me._UpToICProductTypeByProductTypeCode.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Where(Me._UpToICProductTypeByProductTypeCode.Query.ProductTypeCode = Me.ProductTypeCode)
					Me._UpToICProductTypeByProductTypeCode.Query.Load()
				End If

				Return Me._UpToICProductTypeByProductTypeCode
			End Get
			
            Set(ByVal value As ICProductType)
				Me.RemovePreSave("UpToICProductTypeByProductTypeCode")
				

				If value Is Nothing Then
				
					Me.ProductTypeCode = Nothing
				
					Me._UpToICProductTypeByProductTypeCode = Nothing
				Else
				
					Me.ProductTypeCode = value.ProductTypeCode
					
					Me._UpToICProductTypeByProductTypeCode = value
					Me.SetPreSave("UpToICProductTypeByProductTypeCode", Me._UpToICProductTypeByProductTypeCode)
				End If
				
			End Set	

		End Property
		#End Region

		#Region "UpToICUserByCreatedBy - Many To One"
		''' <summary>
		''' Many to One
		''' Foreign Key Name - FK_IC_AmendmentsRightsManagement_IC_User
		''' </summary>

		<XmlIgnore()> _		
		Public Property UpToICUserByCreatedBy As ICUser
		
			Get
				If Me.es.IsLazyLoadDisabled Then return Nothing
				
				If Me._UpToICUserByCreatedBy Is Nothing _
						 AndAlso Not CreatedBy.Equals(Nothing)  Then
						
					Me._UpToICUserByCreatedBy = New ICUser()
					Me._UpToICUserByCreatedBy.es.Connection.Name = Me.es.Connection.Name
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
					Me._UpToICUserByCreatedBy.Query.Where(Me._UpToICUserByCreatedBy.Query.UserID = Me.CreatedBy)
					Me._UpToICUserByCreatedBy.Query.Load()
				End If

				Return Me._UpToICUserByCreatedBy
			End Get
			
            Set(ByVal value As ICUser)
				Me.RemovePreSave("UpToICUserByCreatedBy")
				

				If value Is Nothing Then
				
					Me.CreatedBy = Nothing
				
					Me._UpToICUserByCreatedBy = Nothing
				Else
				
					Me.CreatedBy = value.UserID
					
					Me._UpToICUserByCreatedBy = value
					Me.SetPreSave("UpToICUserByCreatedBy", Me._UpToICUserByCreatedBy)
				End If
				
			End Set	

		End Property
		#End Region

		
			
		''' <summary>
		''' Used internally for retrieving AutoIncrementing keys
		''' during hierarchical PreSave.
		''' </summary>
		Protected Overrides Sub ApplyPreSaveKeys()
		
			If Not Me.es.IsDeleted And Not Me._UpToICCompanyByCompanyCode Is Nothing Then
				Me.CompanyCode = Me._UpToICCompanyByCompanyCode.CompanyCode
			End If
			
			If Not Me.es.IsDeleted And Not Me._UpToICFieldsListByFieldID Is Nothing Then
				Me.FieldID = Me._UpToICFieldsListByFieldID.FieldID
			End If
			
		End Sub
	End Class
		



	<Serializable> _
	Partial Public Class ICAmendmentsRightsManagementMetadata 
		Inherits esMetadata
		Implements IMetadata
		
#Region "Protected Constructor"
		Protected Sub New()
			m_columns = New esColumnMetadataCollection()
			Dim c as esColumnMetadata

			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.AmendmentsRightsID, 0, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.AmendmentsRightsID
			c.IsInPrimaryKey = True
			c.IsAutoIncrement = True
			c.NumericPrecision = 10
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.CompanyCode, 1, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.CompanyCode
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.FieldID, 2, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.FieldID
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.IsAmendmentAllow, 3, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.IsAmendmentAllow
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.IsBulkAmendmentAllow, 4, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.IsBulkAmendmentAllow
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.ProductTypeCode, 5, GetType(System.String), esSystemType.String)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.ProductTypeCode
			c.CharacterMaxLength = 20
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedDate, 6, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedDate
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.CreatedBy, 7, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.CreatedBy
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.CanView, 8, GetType(System.Boolean), esSystemType.Boolean)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.CanView
			c.HasDefault = True
			c.Default = "((0))"
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.Creater, 9, GetType(System.Int32), esSystemType.Int32)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.Creater
			c.NumericPrecision = 10
			c.IsNullable = True
			m_columns.Add(c)
				
			c = New esColumnMetadata(ICAmendmentsRightsManagementMetadata.ColumnNames.CreationDate, 10, GetType(System.DateTime), esSystemType.DateTime)	
			c.PropertyName = ICAmendmentsRightsManagementMetadata.PropertyNames.CreationDate
			c.IsNullable = True
			m_columns.Add(c)
				
		End Sub
#End Region	
	
		Shared Public Function Meta() As ICAmendmentsRightsManagementMetadata
			Return _meta
		End Function
		
		Public ReadOnly Property DataID() As System.Guid Implements IMetadata.DataID
			Get
				Return MyBase.m_dataID
			End Get
		End Property

		Public ReadOnly Property MultiProviderMode() As Boolean Implements IMetadata.MultiProviderMode
			Get
				Return false
			End Get
		End Property

		Public ReadOnly Property Columns() As esColumnMetadataCollection Implements IMetadata.Columns
			Get
				Return MyBase.m_columns
			End Get
		End Property

#Region "ColumnNames"
		Public Class ColumnNames
			 Public Const AmendmentsRightsID As String = "AmendmentsRightsID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const IsAmendmentAllow As String = "IsAmendmentAllow"
			 Public Const IsBulkAmendmentAllow As String = "IsBulkAmendmentAllow"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CanView As String = "CanView"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	
		
#Region "PropertyNames"
		Public Class  PropertyNames
			 Public Const AmendmentsRightsID As String = "AmendmentsRightsID"
			 Public Const CompanyCode As String = "CompanyCode"
			 Public Const FieldID As String = "FieldID"
			 Public Const IsAmendmentAllow As String = "IsAmendmentAllow"
			 Public Const IsBulkAmendmentAllow As String = "IsBulkAmendmentAllow"
			 Public Const ProductTypeCode As String = "ProductTypeCode"
			 Public Const CreatedDate As String = "CreatedDate"
			 Public Const CreatedBy As String = "CreatedBy"
			 Public Const CanView As String = "CanView"
			 Public Const Creater As String = "Creater"
			 Public Const CreationDate As String = "CreationDate"
		End Class
#End Region	

		Public Function GetProviderMetadata(ByVal mapName As String) As esProviderSpecificMetadata _
			Implements IMetadata.GetProviderMetadata

			Dim mapMethod As MapToMeta = mapDelegates(mapName)

			If (Not mapMethod = Nothing) Then
				Return mapMethod(mapName)
			Else
				Return Nothing
			End If

		End Function
		
#Region "MAP esDefault"

		Private Shared Function RegisterDelegateesDefault() As Integer
		
			' This is only executed once per the life of the application
			SyncLock GetType(ICAmendmentsRightsManagementMetadata)
			
				If ICAmendmentsRightsManagementMetadata.mapDelegates Is Nothing Then
					ICAmendmentsRightsManagementMetadata.mapDelegates = New Dictionary(Of String, MapToMeta)
				End If			

				If ICAmendmentsRightsManagementMetadata._meta Is Nothing Then
					ICAmendmentsRightsManagementMetadata._meta = New ICAmendmentsRightsManagementMetadata()
				End If

				Dim mapMethod As New MapToMeta(AddressOf _meta.esDefault)
				mapDelegates.Add("esDefault", mapMethod)
				mapMethod("esDefault")
				Return 0

			End SyncLock
			
		End Function

		Private Function esDefault(ByVal mapName As String) As esProviderSpecificMetadata

			If (Not m_providerMetadataMaps.ContainsKey(mapName)) Then
			
				Dim meta As esProviderSpecificMetadata = New esProviderSpecificMetadata()
				


				meta.AddTypeMap("AmendmentsRightsID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CompanyCode", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("FieldID", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("IsAmendmentAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("IsBulkAmendmentAllow", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("ProductTypeCode", new esTypeMap("varchar", "System.String"))
				meta.AddTypeMap("CreatedDate", new esTypeMap("datetime", "System.DateTime"))
				meta.AddTypeMap("CreatedBy", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CanView", new esTypeMap("bit", "System.Boolean"))
				meta.AddTypeMap("Creater", new esTypeMap("int", "System.Int32"))
				meta.AddTypeMap("CreationDate", new esTypeMap("datetime", "System.DateTime"))			
				
				
				 
				meta.Source = "IC_AmendmentsRightsManagement"
				meta.Destination = "IC_AmendmentsRightsManagement"
				
				meta.spInsert = "proc_IC_AmendmentsRightsManagementInsert"
				meta.spUpdate = "proc_IC_AmendmentsRightsManagementUpdate"
				meta.spDelete = "proc_IC_AmendmentsRightsManagementDelete"
				meta.spLoadAll = "proc_IC_AmendmentsRightsManagementLoadAll"
				meta.spLoadByPrimaryKey = "proc_IC_AmendmentsRightsManagementLoadByPrimaryKey"
				
				Me.m_providerMetadataMaps.Add("esDefault", meta)

			End If

			Return Me.m_providerMetadataMaps("esDefault")

		End Function
		
#End Region	
		
        Private Shared _meta As ICAmendmentsRightsManagementMetadata
        Protected Shared mapDelegates As Dictionary(Of String, MapToMeta)
		Private Shared _esDefault As Integer = RegisterDelegateesDefault()	
		
	End Class
	
End Namespace
